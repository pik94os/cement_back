﻿using Castle.MicroKernel.Registration;
using Castle.Windsor;
using Cement.Core.Auth;
using Cement.Core.Entities;
using Cement.Core.Services.EntityService;
using DataAccess.Repository;
using DataAccess.Repository.Impl;
using DataAccess.SessionProvider;
using DataAccess.SessionProvider.Impl;
using DataAccess.Transaction.Impl;
using NHibernate;
using NUnit.Framework;

namespace Cement.WebUI.Tests
{
    //[SetUpFixture]
    public class TestFixture
    {
        public static IWindsorContainer Container;

        [SetUp]
        public void Setup()
        {
            Container = new WindsorContainer();
            Container.Register(Component.For<IWindsorContainer>().Instance(Container));

            var configuration = new NHibernateSQLiteConfiguration(Container);

            Container.Register(Component.For<ISessionFactory>().UsingFactoryMethod(configuration.BuildSessionFactory));

            Container.Register(Component.For<ISessionProvider>()
                .ImplementedBy<SessionProvider>());

            Container.Register(Component.For<DataAccess.Transaction.ITransactionFactory>()
                .ImplementedBy<ComplexTransactionFactory>());

            Container.Register(Component.For<ITransaction>()
                .LifeStyle.Transient
                .UsingFactoryMethod(kernel =>
                {
                    var session = kernel.Resolve<ISessionProvider>().GetCurrentSession();

                    return kernel.Resolve<DataAccess.Transaction.ITransactionFactory>().BeginTransaction(session);
                }));
            
            Container.Register(Component.For(typeof(IRepository<>)).ImplementedBy(typeof(NHibernateRepository<>)).LifestyleTransient());
            
            Container.Install(new Core.ModuleInstaller());
            Container.Install(new Protocol.ModuleInstaller());

            PrepareUsers();
        }

        protected void  PrepareUsers()
        {
            var employee = new Employee {Name = "Тест"};

            Container.Resolve<IRepository<Employee>>().Save(employee);

            Container.Resolve<IRepository<Employee>>().Save(new Employee { Name = "Тест2" });

            var user = new User { Employee = employee, Login = "Test", Name = "Test" };

            user.SetNewPassword(Container.Resolve<IPasswordHashProvider>(), "Test");

            Container.Resolve<IEntityService<User>>().Save(user);

            var userPrincipal = Container.Resolve<IAuthenticationProvider>().AuthenticateUser("Test", "Test");
            
            Container.Register(Component.For<IUserPrincipal>().Instance(userPrincipal));
        }

        [TearDown]
        public void TearDown()
        {
            
        }
    }
}