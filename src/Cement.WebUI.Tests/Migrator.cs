﻿using Cement.Core;
using Cement.Core.Migrations;
using ECM7.Migrator.Framework;
using Npgsql;
using NUnit.Framework;
namespace Cement.WebUI.Tests
{
    public class Migrator
    {
        //private string Provider
        //{
        //    get
        //    {
        //        //return "ECM7.Migrator.Providers.Oracle.OracleTransformationProvider, ECM7.Migrator.Providers.Oracle";
        //        return "ECM7.Migrator.Providers.PostgreSQL.PostgreSQLTransformationProvider, ECM7.Migrator.Providers.PostgreSQL";
        //    }
        //}

        private ITransformationProvider Provider
        {
            get
            {
                return new PostgreSqlTransformationProvider(new NpgsqlConnection(ConnectionString), 6000);
            }
        }

        private string ConnectionString
        {
            get
            {
                return Global.GetConnString();
            }
        }

        [Ignore]
        [Test]
        public void UpdateViews()
        {
            DbMigrator.UpdateViews(Provider);
        }

        [Ignore]
        [Test]
        public void MigrateToLast()
        {
            DbMigrator.MigrateToLast(Provider);
        }

        [Ignore]
        [Test]
        public void MigrateToPrevious()
        {
            DbMigrator.MigrateToPrev(Provider);
        }

        [Ignore]
        [Test]
        public void MigrateToVersion()
        {
            DbMigrator.MigrateTo(Provider, 2015031600);
        }

    }
}