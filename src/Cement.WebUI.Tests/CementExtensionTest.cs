﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using Castle.Windsor;
using Cement.Core.Extensions;
using NUnit.Framework;

namespace Cement.WebUI.Tests
{
    public class CementExtensionTest 
    {
        protected IWindsorContainer Container { get; set; }

        public CementExtensionTest()
        {
            Container = TestFixture.Container;
        }

        [Test]
        public void CementDateTimeIsString()
        {
            var date = new DateTime?();
            date = new DateTime(2008, 11, 27);

            var cementDateString = date.ToCementDateTime();

            Assert.AreEqual(cementDateString, "27.11.08");
        }

        [Test]
        public void CementDateTimeIsEmptyString()
        {
            var date = new DateTime?();

            var cementDateString = date.ToCementDateTime();

            Assert.AreEqual(cementDateString, string.Empty);
        }

        [Test]
        public void GetBytesString()
        {
            var str = "test";
            var bytes = Encoding.UTF8.GetBytes(str);
            var bytes2 = str.GetBytes();

            Assert.AreEqual(bytes, bytes2);
        }

        [Test]
        public void GetString()
        {
            var bytes = new byte[]{1, 2, 3, 4};

            var str = Encoding.UTF8.GetString(bytes);
            var str2 = bytes.GetString();

            Assert.AreEqual(str, str2);
        }
        
        [Test]
        public void EnumDisplayValue()
        {
            var str = "Test display value";
            var enumStr = TestEnum.Test.GetDisplayValue();

            Assert.AreEqual(str, enumStr);
        }

        [Test]
        public void StringToLongList()
        {
            var str = "1,2,3,4,5";
            var list = new List<long> {1, 2, 3, 4, 5};
            var list2 = str.ToLongList();

            Assert.AreEqual(list, list2);
        }

        [Test]
        public void EnumAsDictionary()
        {
            var dict = new Dictionary<int, string>
            {
                {10, "Test display value"},
                {20, "Test 2"}
            };

            var dict2 = typeof (TestEnum).AsDictionary();

            Assert.That(dict, Is.EquivalentTo(dict2));
            Assert.AreEqual(dict, dict2);
        }
    }

    enum TestEnum
    {
        [Display(Name = "Test display value")]
        Test = 10,

        [Display(Name = "Test 2")]
        Test2 = 20,
    }
}