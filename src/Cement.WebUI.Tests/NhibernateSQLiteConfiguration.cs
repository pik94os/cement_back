﻿using System;
using System.IO;
using System.Linq;
using System.Reflection;
using Castle.Core.Internal;
using Castle.Windsor;
using DataAccess;
using NHibernate;
using NHibernate.Bytecode;
using NHibernate.Cfg;
using NHibernate.Cfg.Loquacious;
using NHibernate.Cfg.MappingSchema;
using NHibernate.Connection;
using NHibernate.Dialect;
using NHibernate.Driver;
using NHibernate.Mapping.ByCode;
using Environment = NHibernate.Cfg.Environment;

namespace Cement.WebUI.Tests
{
    public sealed class NHibernateSQLiteConfiguration
    {
        private readonly string _connectionString;

        public IWindsorContainer _container { get; set; }
        
        public NHibernateSQLiteConfiguration(IWindsorContainer container)
        {
            var directory = Directory.Exists("R:/") ? "R:/" : Path.GetTempPath();
            
            var dbName = Path.Combine(directory, Guid.NewGuid().ToString());

            _connectionString = string.Format("Data Source={0};Version=3;New=True;", dbName);
            _container = container;
        }

        /// <summary>
        /// Создание фабрики сессий
        /// </summary>
        public ISessionFactory BuildSessionFactory()
        {
            var configuration = LoadConfiguration();

            var sessionFactory = configuration.BuildSessionFactory();

            var schemaUpdate = new NHibernate.Tool.hbm2ddl.SchemaUpdate(configuration);
            schemaUpdate.Execute(true, true);

            return sessionFactory;
        }

        /// <summary>
        /// Создание конифуграции
        /// </summary>
        private NHibernate.Cfg.Configuration LoadConfiguration()
        {
            var configuration = CreateConfiguration();

            return configuration;
        }

        /// <summary>
        /// Создание конифуграции
        /// </summary>
        private NHibernate.Cfg.Configuration CreateConfiguration()
        {
            var configuration = new NHibernate.Cfg.Configuration();
            configuration.DataBaseIntegration(FillDataBaseConfiguration);

            SetGeneralConfiguration(configuration);

            configuration.AddMapping(GetMappings());

            return configuration;
        }

        static bool IsSubclassOfRawGeneric(Type generic, Type toCheck)
        {
            while (toCheck != null && toCheck != typeof(object))
            {
                var cur = toCheck.IsGenericType ? toCheck.GetGenericTypeDefinition() : toCheck;
                if (generic == cur)
                {
                    return true;
                }
                toCheck = toCheck.BaseType;
            }
            return false;
        }

        private HbmMapping GetMappings()
        {
            var assemblies = _container.ResolveAll<IEntityModule>()
                .SelectMany(x => x.GetAssemblies())
                .Distinct()
                .ToList();

            // для начала получаем список маппингов
            var mappings = assemblies
                .SelectMany(x => x.GetTypes())
                .Where(x => x.Is<IEntityAttributesMapper>())
                .Where(x => x.IsClass && !x.IsAbstract && !x.IsInterface);

            var mapper = new ModelMapper();

            mapper.AddMappings(mappings);
            var mapping = mapper.CompileMappingForAllExplicitlyAddedEntities();

            return mapping;
        }

        /// <summary>
        /// Заполняем данные базы.
        /// </summary>
        /// <param name="db"></param>
        private void FillDataBaseConfiguration(IDbIntegrationConfigurationProperties db)
        {
            db.ConnectionString = _connectionString;
            db.Dialect<SQLiteDialect>();
            db.Driver<SQLite20Driver>();
            db.ConnectionProvider<DriverConnectionProvider>();
            db.LogSqlInConsole = true;
            db.LogFormattedSql = true;
        }

        /// <summary>
        /// Задание общей конфигурации
        /// </summary>
        private void SetGeneralConfiguration(NHibernate.Cfg.Configuration configuration)
        {
            Environment.UseReflectionOptimizer = true;

          
            configuration.Properties[Environment.PrepareSql] = "false";
            configuration.Properties[Environment.WrapResultSets] = "true";
            configuration.Properties[Environment.UseProxyValidator] = "false";
            configuration.Properties[Environment.GenerateStatistics] = "false";
            configuration.Properties[Environment.BatchSize] = "1000";
            configuration.Properties[Environment.CommandTimeout] = "6000";
            configuration.Properties[Environment.UseSecondLevelCache] = "true";
            configuration.Properties[Environment.UseQueryCache] = "true";
            configuration.Properties[Environment.ShowSql] = "false";
            configuration.Properties[Environment.FormatSql] = "false";
            //configuration.Properties[Environment.Hbm2ddlKeyWords] = "auto-quote";
            
            configuration.Proxy(p => p.ProxyFactoryFactory<DefaultProxyFactoryFactory>());
        }

    }
}