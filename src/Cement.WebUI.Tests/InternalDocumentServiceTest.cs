﻿using System;
using System.Linq;
using Castle.Windsor;
using Cement.Core.Auth;
using Cement.Core.Entities;
using Cement.Core.Enums;
using Cement.Core.Extensions;
using Cement.Core.Services;
using Cement.Core.Services.EntityService;
using DataAccess.Repository;
using NUnit.Framework;

namespace Cement.WebUI.Tests
{
    public class InternalDocumentServiceTest
    {
        protected IWindsorContainer Container { get; set; }

        protected IInternalDocumentService InternalDocumentService { get; set; }

        protected IEntityService<InternalDocument> InternalDocumentEntityService { get; set; }
        
        protected IEntityService<Employee> EmployeeEntityService { get; set; }

        protected IEntityService<InternalDocumentMovement> InternalDocumentMovementEntityService { get; set; }

        protected IEntityService<InternalDocumentRoute> InternalDocumentRouteEntityService { get; set; }
        
        public InternalDocumentServiceTest()
        {
            Container = TestFixture.Container;
            InternalDocumentService = Container.Resolve<IInternalDocumentService>();
            InternalDocumentEntityService = Container.Resolve<IEntityService<InternalDocument>>();
            EmployeeEntityService = Container.Resolve<IEntityService<Employee>>();
            InternalDocumentMovementEntityService = Container.Resolve<IEntityService<InternalDocumentMovement>>();
            InternalDocumentRouteEntityService = Container.Resolve<IEntityService<InternalDocumentRoute>>();
        }

        protected InternalDocument CreateNewInternalDocument()
        {
            var internalDoc = new InternalDocument
            {
                InternalDocumentType = InternalDocumentType.Instruction,
                Date = DateTime.Now,
                No = "1"
            };

            InternalDocumentEntityService.Save(internalDoc);

            Assert.That(internalDoc.DocumentState, Is.EqualTo(DocumentState.Draft));

            return internalDoc;
        }

        [Test]
        public void CreateNewDocAndSendTest()
        {
            CreateNewDocAndSend();
        }

        public long CreateNewDocAndSend()
        {
            var internalDoc = CreateNewInternalDocument();

            var from = Container.Resolve<IUserPrincipal>().Employee;

            var to = EmployeeEntityService.GetAll().ToList().Last();

            var commentText = Guid.NewGuid().ToString();

            InternalDocumentService.SendTo(internalDoc.Id, from, to, commentText);

            Assert.That(internalDoc.DocumentState, Is.EqualTo(DocumentState.OnAgreement));

            var routes = InternalDocumentRouteEntityService.GetAll().Where(x => x.Document == internalDoc).ToList();

            Assert.That(routes.Count, Is.EqualTo(2));

            var movement = InternalDocumentMovementEntityService.GetAll().Where(x => x.Document == internalDoc).ToList();

            Assert.That(movement.Count, Is.EqualTo(2));

            Assert.That(movement.Count(x => x.MovementType == DocumentMovementType.Incoming), Is.EqualTo(1));
            Assert.That(movement.Count(x => x.MovementType == DocumentMovementType.Outcoming), Is.EqualTo(1));

            Assert.That(movement.Where(x => x.Employee == to).Count(x => x.MovementType == DocumentMovementType.Incoming), Is.EqualTo(1));
            Assert.That(movement.Where(x => x.Employee == from).Count(x => x.MovementType == DocumentMovementType.Outcoming), Is.EqualTo(1));

            var incomingRoute = movement.Single(x => x.Employee == to).Route;

            Assert.That(incomingRoute, Is.EqualTo(routes.Last()));

            Assert.That(routes.First().Comment.GetString(), Is.EqualTo(commentText));

            return movement.Single(x => x.Employee == to).Id;
        }

        [Test]
        public void Sign()
        {
            var incomingMovementId = CreateNewDocAndSend();

            var commentText = Guid.NewGuid().ToString();

            InternalDocumentService.ProcessDocument(incomingMovementId, DocumentProcessingResult.Signed, commentText);

            var movement = InternalDocumentMovementEntityService.GetAll().Single(x => x.Id == incomingMovementId);

            var route = movement.Route;

            Assert.That(route, Is.Not.Null);
            Assert.That(route.DateOut, Is.Not.Null);
            Assert.That(route.ProcessingResult, Is.EqualTo(DocumentProcessingResult.Signed));
            Assert.That(route.Comment.GetString(), Is.EqualTo(commentText));
            Assert.That(movement.Document.DocumentState, Is.EqualTo(DocumentState.Signed));
        }

        [Test]
        public void Reject()
        {
            var incomingMovementId = CreateNewDocAndSend();

            var commentText = Guid.NewGuid().ToString();

            InternalDocumentService.ProcessDocument(incomingMovementId, DocumentProcessingResult.Rejected, commentText);

            var movement = InternalDocumentMovementEntityService.GetAll().Single(x => x.Id == incomingMovementId);

            var route = movement.Route;

            Assert.That(route, Is.Not.Null);
            Assert.That(route.DateOut, Is.Not.Null);
            Assert.That(route.ProcessingResult, Is.EqualTo(DocumentProcessingResult.Rejected));
            Assert.That(route.Comment.GetString(), Is.EqualTo(commentText));
            Assert.That(movement.Document.DocumentState, Is.EqualTo(DocumentState.Rejected));
        }

        [Test]
        public void AgreeAndSendNext()
        {
            var incomingMovementId = CreateNewDocAndSend();

            var nextToAgreeEmployee = new Employee {Name = "Next to agree"};

            Container.Resolve<IRepository<Employee>>().Save(nextToAgreeEmployee);

            var commentText = Guid.NewGuid().ToString();

            InternalDocumentService.ProcessDocument(incomingMovementId, DocumentProcessingResult.Agreed, commentText, nextToAgreeEmployee);

            var movement = InternalDocumentMovementEntityService.GetAll().Single(x => x.Id == incomingMovementId);

            var route = movement.Route;

            Assert.That(route, Is.Not.Null);
            Assert.That(route.DateOut, Is.Not.Null);
            Assert.That(route.ProcessingResult, Is.EqualTo(DocumentProcessingResult.Agreed));
            Assert.That(route.Comment.GetString(), Is.EqualTo(commentText));
            Assert.That(movement.Document.DocumentState, Is.EqualTo(DocumentState.OnAgreement));
        }
    }
}