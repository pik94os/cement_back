﻿using System.Reflection;
using ECM7.Migrator.Framework;
using ECM7.Migrator.Providers;
using Npgsql;

namespace Cement.WebUI.Tests
{
    /// <summary>
    /// Модифицированная версия ECM7.Migrator.Providers.PostgreSQL.PostgreSQLTransformationProvider
    /// Которая переводит наименования всех объектов в LowerCase
    /// </summary>
    public class PostgreSqlTransformationProvider : ECM7.Migrator.Providers.PostgreSQL.PostgreSQLTransformationProvider, ITransformationProvider
    {
        public PostgreSqlTransformationProvider(NpgsqlConnection connection, int commandTimeout)
            : base(connection, commandTimeout)
        {
            var formatProvider = new SqlFormatter(obj => string.Format(NamesQuoteTemplate, obj).ToLower());

            GetType().GetField("sqlFormatProvider", BindingFlags.Instance | BindingFlags.NonPublic).SetValue(this, formatProvider);
        }

        protected void UpdateColumnName(Column column)
        {
            column.Name = column.Name.ToLower();
        }
        
        public override string GetSqlColumnDef(Column column, bool compoundPrimaryKey)
        {
            UpdateColumnName(column);

            return base.GetSqlColumnDef(column, compoundPrimaryKey);
        }

        protected void UpdateTableName(SchemaQualifiedObjectName table)
        {
            table.Name = table.Name.ToLower();
        }

        public override bool TableExists(SchemaQualifiedObjectName table)
        {
            UpdateTableName(table);

            return base.TableExists(table);
        }

        public override bool ColumnExists(SchemaQualifiedObjectName table, string column)
        {
            UpdateTableName(table);

            return base.ColumnExists(table, column.ToLower());
        }
    }
}