﻿using System.Linq;
using Castle.Windsor;
using Cement.Core.Entities;
using Cement.Core.Services.EntityService;
using DataAccess.Repository;
using NUnit.Framework;

namespace Cement.WebUI.Tests
{
    public class Test1 
    {
        protected IWindsorContainer Container { get; set; }

        public Test1 ()
        {
            Container = TestFixture.Container;
        }

        [Test]
        public void TestRepo()
        {
            var repo = Container.Resolve<IRepository<Employee>>();

            var countBefore = repo.GetAll().Count();

            repo.Save(new Employee());

            var countAfter = repo.GetAll().Count();

            Assert.That(countAfter, Is.EqualTo(countBefore + 1));
        }

        [Test]
        public void OnSavePriceListCreateInternalDocument()
        {
            var internalDocEntityService = Container.Resolve<IEntityService<InternalDocument>>();
            var priceListEntityService = Container.Resolve<IEntityService<PriceList>>();

            var internalDocCountBefore = internalDocEntityService.GetAll().Count();
            var priceListCountBefore = priceListEntityService.GetAll().Count();
            
            priceListEntityService.Save(new PriceList());

            var internalDocCountAfter = internalDocEntityService.GetAll().Count();
            var priceListCountAfter = priceListEntityService.GetAll().Count();
            
            Assert.That(internalDocCountAfter, Is.EqualTo(internalDocCountBefore + 1));
            Assert.That(priceListCountAfter, Is.EqualTo(priceListCountBefore + 1));
        }
    }
}