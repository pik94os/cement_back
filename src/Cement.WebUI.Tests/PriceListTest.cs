﻿using System;
using System.Linq;
using Castle.Windsor;
using Cement.Core.Entities;
using Cement.Core.Enums;
using Cement.Core.Services.EntityService;
using DataAccess.Repository;
using NUnit.Framework;

namespace Cement.WebUI.Tests
{
    public class PriceListTest 
    {
        protected IWindsorContainer Container { get; set; }

        public PriceListTest()
        {
            Container = TestFixture.Container;
        }

        [Test]
        public void ArchivingReplacedPriceList()
        {
            var priceListEntityService = Container.Resolve<IEntityService<PriceList>>();

            var priceList = new PriceList();
            priceListEntityService.Save(priceList);

            var newPriceList = new PriceList
            {
                DateStart = new DateTime(2020, 11, 13),
                ReplacementPriceList = priceList
            };

            priceListEntityService.Save(newPriceList);

            Assert.AreEqual(priceList.DateEnd, newPriceList.DateStart);
            Assert.AreEqual(priceList.Status, PriceListStatus.Archive);
        }
    }
}