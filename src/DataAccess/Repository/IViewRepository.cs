﻿using System.Linq;

namespace DataAccess.Repository
{
    public interface IViewRepository<T> where T : IViewEntity
    {
        IQueryable<T> GetAll();
    }
}