﻿using System.Linq;

namespace DataAccess.Repository
{
    public interface IRepository<T> where T : IEntity
    {
        void Evict(T entity);

        void Delete(long id);

        T Get(long id);

        IQueryable<T> GetAll();

        T Load(long id);

        void Save(T value);

        void Update(T value);
    }
}