﻿using System.Linq;
using Castle.Windsor;
using DataAccess.SessionProvider;
using DataAccess.Transaction;
using NHibernate;
using NHibernate.Linq;

namespace DataAccess.Repository.Impl
{
    public class NHibernateRepository<T> : IRepository<T> where T : class, IEntity
    {
        public IWindsorContainer Container { get; set; }

        public virtual void Evict(T entity)
        {
            OpenSession().Evict(entity);
        }

        public virtual void Delete(long id)
        {
            var session = OpenSession();
            using (var transaction = BeginTransaction(session))
            {
                var obj = id == 0 ? null : session.Load<T>(id);
                session.Delete(obj);
                transaction.Commit();
            }
        }

        public virtual T Get(long id)
        {
            T returnVal;

            var session = OpenSession();
            using (var transaction = BeginTransaction(session))
            {
                returnVal = session.Get<T>(id);
                transaction.Commit();
            }

            return returnVal;
        }

        public virtual IQueryable<T> GetAll()
        {
            return OpenSession().Query<T>();
        }

        public virtual T Load(long id)
        {
            return id == 0 ? null : OpenSession().Load<T>(id);
        }

        public virtual void Save(T value)
        {
            var session = OpenSession();
            using (var transaction = BeginTransaction(session))
            {
                session.Save(value);
                transaction.Commit();
            }
        }

        public virtual void Update(T value)
        {
            var session = OpenSession();
            using (var transaction = BeginTransaction(session))
            {
                session.Update(value);
                transaction.Commit();
            }
        }
        
        protected ISession OpenSession()
        {
            return Container.Resolve<ISessionProvider>().GetCurrentSession();
        }

        /// <summary>Открыть транзакцию</summary>
        /// <returns>Экземпляр ITransaction</returns>
        protected ITransaction BeginTransaction(ISession session)
        {
            return Container.Resolve<ITransactionFactory>().BeginTransaction(session);
        }
    } 
}