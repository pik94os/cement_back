﻿using System.Linq;
using Castle.Windsor;
using DataAccess.SessionProvider;
using NHibernate;
using NHibernate.Linq;

namespace DataAccess.Repository.Impl
{
    public class NHibernateViewRepository<T> : IViewRepository<T> where T : IViewEntity
    {
        public IWindsorContainer Container { get; set; }
        
        public virtual IQueryable<T> GetAll()
        {
            return OpenSession().Query<T>();
        }
        
        protected ISession OpenSession()
        {
            return Container.Resolve<ISessionProvider>().GetCurrentSession();
        }
    } 
}