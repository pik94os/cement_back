﻿using System;
using Castle.MicroKernel.Registration;
using Castle.MicroKernel.SubSystems.Configuration;
using Castle.Windsor;
using DataAccess.Config;
using DataAccess.CustomLifeStyles;
using DataAccess.Repository;
using DataAccess.Repository.Impl;
using DataAccess.SessionProvider;
using DataAccess.Transaction;
using DataAccess.Transaction.Impl;
using NHibernate;

namespace DataAccess
{
    public class ModuleInstaller : IWindsorInstaller
    {
        public void Install(IWindsorContainer container, IConfigurationStore store)
        {
            var config = container.Resolve<ConfigProvider>();
            
            var connectionString = config.ConnectionString;

            if (string.IsNullOrWhiteSpace(connectionString))
            {
                throw new Exception("No connection string");
            }

            var configuration = new NHibernateConfiguration(connectionString, container);

            container.Register(Component.For<ISessionFactory>().UsingFactoryMethod(configuration.BuildSessionFactory));

            container.Register(Component.For<ISessionProvider>()
                       .ImplementedBy<SessionProvider.Impl.SessionProvider>()
                       .LifeStyle.Scoped<HybridPerWebRequestTransientScopeAccessor>());

            container.Register(Component.For<ITransactionFactory>()
                .ImplementedBy<ComplexTransactionFactory>()
                .LifeStyle.Scoped<HybridPerWebRequestTransientScopeAccessor>());

            container.Register(Component.For<ITransaction>()
                .LifeStyle.Transient
                .UsingFactoryMethod(kernel =>
                {
                    var session = kernel.Resolve<ISessionProvider>().GetCurrentSession();

                    return kernel.Resolve<ITransactionFactory>().BeginTransaction(session);
                }));

            //var transaction = Component.For<IDataTransaction>()
            //    .UsingFactoryMethod(kernel =>
            //    {
            //        var session = container.Resolve<ISessionProvider>().GetCurrentSession();
            //        var innerTransaction = session.BeginTransaction();
            //        return new DataTransaction(innerTransaction);
            //    })
            //    .LifestyleTransient();

            //container.Register(transaction);

            container.Register(Component.For(typeof(IRepository<>)).ImplementedBy(typeof(NHibernateRepository<>)).LifestyleTransient());
            container.Register(Component.For(typeof(IViewRepository<>)).ImplementedBy(typeof(NHibernateViewRepository<>)).LifestyleTransient());
        }
    }
}