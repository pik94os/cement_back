﻿using System.Collections.Generic;
using System.Reflection;

namespace DataAccess
{
    public interface IEntityModule
    {
        List<Assembly> GetAssemblies();
    }
}