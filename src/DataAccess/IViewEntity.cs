﻿namespace DataAccess
{
    public interface IViewEntity
    {
        /// <summary>
        /// Идентификатор
        /// </summary>
        long Id { get; set; } 
    }
}