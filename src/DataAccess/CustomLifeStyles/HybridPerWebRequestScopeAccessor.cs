﻿using System.Web;
using Castle.MicroKernel.Lifestyle;
using Castle.MicroKernel.Lifestyle.Scoped;

namespace DataAccess.CustomLifeStyles 
{
    public class HybridPerWebRequestScopeAccessor: IScopeAccessor {
        private readonly IScopeAccessor webRequestScopeAccessor = new WebRequestScopeAccessor();
        private readonly IScopeAccessor secondaryScopeAccessor;

        public HybridPerWebRequestScopeAccessor(IScopeAccessor secondaryScopeAccessor) {
            this.secondaryScopeAccessor = secondaryScopeAccessor;
        }

        public ILifetimeScope GetScope(Castle.MicroKernel.Context.CreationContext context) {
            if (HttpContext.Current != null && PerWebRequestLifestyleModuleUtils.IsInitialized)
                return this.webRequestScopeAccessor.GetScope(context);
            return this.secondaryScopeAccessor.GetScope(context);
        }

        public void Dispose() {
            this.webRequestScopeAccessor.Dispose();
            this.secondaryScopeAccessor.Dispose();
        }
    }
}
