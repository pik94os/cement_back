﻿using Castle.MicroKernel.Lifestyle.Scoped;

namespace DataAccess.CustomLifeStyles 
{
    public class TransientScopeAccessor: IScopeAccessor {
        public ILifetimeScope GetScope(Castle.MicroKernel.Context.CreationContext context) {
            return new DefaultLifetimeScope();
        }

        public void Dispose() {
        }
    }
}
