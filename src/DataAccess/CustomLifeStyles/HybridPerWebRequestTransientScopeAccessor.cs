﻿namespace DataAccess.CustomLifeStyles 
{
    public class HybridPerWebRequestTransientScopeAccessor : HybridPerWebRequestScopeAccessor {
        public HybridPerWebRequestTransientScopeAccessor() : 
            base(new TransientScopeAccessor()) {}
    }
}
