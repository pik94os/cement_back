﻿using NHibernate.Cfg.Loquacious;

namespace DataAccess.Batching
{
    public interface IBatcherApplier
    {
        void ApplyBatcher(IDbIntegrationConfigurationProperties db);
    }
}