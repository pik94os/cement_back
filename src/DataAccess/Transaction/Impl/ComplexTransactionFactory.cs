﻿using System;
using NHibernate;

namespace DataAccess.Transaction.Impl
{
    public class ComplexTransactionFactory : ITransactionFactory
    {
        private Guid _guid;

        public ComplexTransactionFactory()
        {
            ResolveCount = 0;
        }

        /// <summary>Идентификатор транзакции</summary>
        public Guid Guid
        {
            get { return _guid; }
        }

        public int ResolveCount { get; set; }

        /// <summary>Открыть транзакцию</summary>
        /// <param name="session">Сессия</param>
        /// <returns>Транзакция</returns>
        public ITransaction BeginTransaction(ISession session)
        {
            ITransaction innerTransaction = null;
            
            if (ResolveCount == 0)
            {
                _guid = Guid.NewGuid();
                innerTransaction = session.BeginTransaction();
            }

            var transaction = new ComplexTransaction(innerTransaction);

            transaction.OnCommit += TransactionRelease;
            transaction.OnRollback += TransactionRelease;
            transaction.OnDispose += TransactionRelease;

            ResolveCount++;

            return transaction;
        }

        private void TransactionRelease(object sender, EventArgs e)
        {
            var transaction = sender as ComplexTransaction;

            if (transaction != null)
            {
                // На всякий случай
                if (ResolveCount > 0)
                {
                    ResolveCount--;
                }

                // Отписываем события
                transaction.OnCommit -= TransactionRelease;
                transaction.OnRollback -= TransactionRelease;
                transaction.OnDispose -= TransactionRelease;
            }
        }
    }
}