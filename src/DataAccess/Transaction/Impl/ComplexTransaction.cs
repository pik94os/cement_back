﻿using System;
using System.Data;
using NHibernate;
using NHibernate.Transaction;

namespace DataAccess.Transaction.Impl
{
    /// <summary>
    /// Реализация интерфейса NHibernate.ITransaction.
    /// </summary>
    public class ComplexTransaction : ITransaction
    {
        /// <summary>
        /// Вложенная транзакция.
        /// </summary>
        public ITransaction InnerTransaction { get; set; }
        
        public event EventHandler OnCommit;

        public event EventHandler OnRollback;

        public event EventHandler OnDispose;

        /// <summary>
        /// Конструктор.
        /// </summary>
        /// <param name="innerTransaction">
        /// Вложенная транзакция.
        /// </param>
        public ComplexTransaction(ITransaction innerTransaction)
        {
            InnerTransaction = innerTransaction;
        }

        public bool IsActive
        {
            get
            {
                return this.InnerTransaction.IsActive;
            }
        }

        public bool WasCommitted
        {
            get
            {
                return this.InnerTransaction.WasCommitted;
            }
        }

        public bool WasRolledBack
        {
            get
            {
                return this.InnerTransaction.WasRolledBack;
            }
        }

        public void Begin()
        {
            throw new NotImplementedException();
        }

        public void Begin(IsolationLevel isolationLevel)
        {
            throw new NotImplementedException();
        }

        public void Commit()
        {
            if (InnerTransaction != null)
            {
                if (OnCommit != null)
                {
                    OnCommit(this, new EventArgs());
                }

                InnerTransaction.Commit();
            }
        }

        public void Dispose()
        {
            if (OnDispose != null)
            {
                OnDispose(this, new EventArgs());
            }

            if (InnerTransaction != null)
            {
                InnerTransaction.Dispose();
            }
        }

        public void Enlist(IDbCommand command)
        {
            InnerTransaction.Enlist(command);
        }

        public void RegisterSynchronization(ISynchronization synchronization)
        {
            this.InnerTransaction.RegisterSynchronization(synchronization);
        }

        public void Rollback()
        {
            if (OnRollback != null)
            {
                OnRollback(this, new EventArgs());
            }

            if (this.InnerTransaction != null)
            {
                InnerTransaction.Rollback();
            }
        }
    }
}