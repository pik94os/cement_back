using System;
using NHibernate;

namespace DataAccess.Transaction
{
    /// <summary>������� ����������</summary>
    public interface ITransactionFactory
    {
        /// <summary>������������� ����������</summary>
        Guid Guid { get; }

        /// <summary>������� ����������</summary>
        /// <param name="session">������</param>
        /// <returns>����������</returns>
        ITransaction BeginTransaction(ISession session);
    }
}