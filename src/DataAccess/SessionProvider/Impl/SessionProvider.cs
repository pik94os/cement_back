﻿using Castle.Windsor;
using NHibernate;

namespace DataAccess.SessionProvider.Impl
{
    /// <summary>Провайдер сессий NHibernate</summary>
    public class SessionProvider : ISessionProvider
    {
        private readonly object _lockObject;
        private ISession _currentSession;

        /// <summary>Конструктор</summary>
        public SessionProvider()
        {
            this._lockObject = new object();
        }

        /// <summary>Получить текущую сессию (Внимание! Используется только для проверки наличия сессии или получения доступа к текущей. Служит индиктаром наличия открытой сессии)</summary>
        /// <returns>Текущая сессия</returns>
        public virtual ISession CurrentSession
        {
            get { return this._currentSession; }
        }

        public IWindsorContainer Container { get; set; }

        /// <summary>Открыть "независимую/тихую" сессию (Не вызываются различные события  NHibernate)</summary>
        /// <returns>Сессия</returns>
        public virtual IStatelessSession OpenStatelessSession()
        {
            return Container.Resolve<ISessionFactory>().OpenStatelessSession();
        }

        /// <summary>Получить текущую сессию (в случае отсутствия, сессия создается)</summary>
        /// <returns>Текущая сессия</returns>
        public virtual ISession GetCurrentSession()
        {
            if (this._currentSession == null)
            {
                lock (this._lockObject)
                {
                    if (this._currentSession == null)
                    {
                        this._currentSession = this.OpenSession();
                    }
                }
            }

            return this._currentSession;
        }

        /// <summary>Закрыть текущую сессию (Внимание!!! Вызывать вне транзакции. При вызове внутри открытой транзакции можем получить различные непрогнозируемые ошибки!!!)</summary>
        public void CloseCurrentSession()
        {
            if (this._currentSession != null)
            {
                lock (this._lockObject)
                {
                    if (this._currentSession != null)
                    {
                        this._currentSession.Dispose();
                        this._currentSession = null;
                    }
                }
            }
        }

        /// <summary>Создать новую сессию (Внимание!!! Текущая сессия будет закрыта!!! Вызывать вне транзакции. При вызове внутри открытой транзакции можем получить различные непрогнозируемые ошибки!!!)</summary>
        /// <returns>Новая сессия</returns>
        public virtual ISession CreateNewSession()
        {
            lock (this._lockObject)
            {
                if (this._currentSession != null)
                {
                    this._currentSession.Dispose();
                }

                this._currentSession = this.OpenSession();
            }

            return this._currentSession;
        }

        /// <summary>Освобождаем ресурсы</summary>
        public virtual void Dispose()
        {
            if (this._currentSession != null)
            {
                this._currentSession.Dispose();
            }
        }

        protected virtual ISession OpenSession()
        {
            return Container.Resolve<ISessionFactory>().OpenSession();
        }
    }
}