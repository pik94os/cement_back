﻿using System;
using NHibernate;

namespace DataAccess.SessionProvider
{
    /// <summary>Провайдер сессий NHibernate</summary>
    public interface ISessionProvider : IDisposable
    {
        /// <summary>Получить текущую сессию (Внимание! Используется только для проверки наличия сессии или получения доступа к текущей. Служит индиктаром наличия открытой сессии)</summary>
        /// <returns>Текущая сессия</returns>
        ISession CurrentSession { get; }

        /// <summary>Открыть "независимую/тихую" сессию (Не вызываются различные события  NHibernate)</summary>
        /// <returns>Сессия</returns>
        IStatelessSession OpenStatelessSession();

        /// <summary>Получить текущую сессию (в случае отсутствия, сессия создается)</summary>
        /// <returns>Текущая сессия</returns>
        ISession GetCurrentSession();

        /// <summary>Закрыть текущую сессию (Внимание!!! Вызывать вне транзакции. При вызове внутри открытой транзакции можем получить различные непрогнозируемые ошибки!!!)</summary>
        void CloseCurrentSession();

        /// <summary>Создать новую сессию (Внимание!!! Текущая сессия будет закрыта!!! Вызывать вне транзакции. При вызове внутри открытой транзакции можем получить различные непрогнозируемые ошибки!!!)</summary>
        /// <returns>Новая сессия</returns>
        ISession CreateNewSession();
    }
}