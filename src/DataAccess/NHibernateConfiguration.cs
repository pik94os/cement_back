﻿using System.Linq;
using System.Reflection;
using Castle.Core.Internal;
using Castle.Windsor;
using DataAccess.Batching;
using NHibernate;
using NHibernate.Bytecode;
using NHibernate.Cfg;
using NHibernate.Cfg.Loquacious;
using NHibernate.Cfg.MappingSchema;
using NHibernate.Mapping.ByCode;

namespace DataAccess
{
    public sealed class NHibernateConfiguration
    {
        private readonly string _connectionString;

        public IWindsorContainer _container { get; set; }

        public NHibernateConfiguration(string connectionString, IWindsorContainer container)
        {
            _connectionString = connectionString;
            _container = container;
        }

        /// <summary>
        /// Создание фабрики сессий
        /// </summary>
        public ISessionFactory BuildSessionFactory()
        {
            var configuration = LoadConfiguration();

            var sessionFactory = configuration.BuildSessionFactory();

            return sessionFactory;
        }

        /// <summary>
        /// Создание конифуграции
        /// </summary>
        private NHibernate.Cfg.Configuration LoadConfiguration()
        {
            var configuration = CreateConfiguration();

            return configuration;
        }

        /// <summary>
        /// Создание конифуграции
        /// </summary>
        private NHibernate.Cfg.Configuration CreateConfiguration()
        {
            var configuration = new NHibernate.Cfg.Configuration();
            configuration.DataBaseIntegration(FillDataBaseConfiguration);

            SetGeneralConfiguration(configuration);

            configuration.AddMapping(GetMappings());

            return configuration;
        }

        private HbmMapping GetMappings()
        {
            var assemblies = _container.ResolveAll<IEntityModule>()
                .SelectMany(x => x.GetAssemblies())
                .Distinct()
                .ToList();

            // для начала получаем список маппингов
            var mappings = assemblies
                .SelectMany(x => x.GetTypes())
                .Where(x => x.Is<IEntityAttributesMapper>())
                .Where(x => x.IsClass && !x.IsAbstract && !x.IsInterface);

            var mapper = new ModelMapper();

            mapper.AddMappings(mappings);
            var mapping = mapper.CompileMappingForAllExplicitlyAddedEntities();

            return mapping;
        }

        /// <summary>
        /// Заполняем данные базы.
        /// </summary>
        /// <param name="db"></param>
        private void FillDataBaseConfiguration(IDbIntegrationConfigurationProperties db)
        {
            db.ConnectionString = _connectionString;
            //db.Dialect<NHibernate.Dialect.Oracle10gDialect>();
            //db.Driver<NHibernate.Driver.OracleClientDriver>();
            db.Dialect<NHibernate.Dialect.PostgreSQLDialect>();
            db.Driver<NHibernate.Driver.NpgsqlDriver>();
            db.ConnectionProvider<NHibernate.Connection.DriverConnectionProvider>();
            
            if(_container.Kernel.HasComponent(typeof(IBatcherApplier)))
            {
                _container.Resolve<IBatcherApplier>().ApplyBatcher(db);
            }
        }

        /// <summary>
        /// Задание общей конфигурации
        /// </summary>
        private void SetGeneralConfiguration(NHibernate.Cfg.Configuration configuration)
        {
            Environment.UseReflectionOptimizer = true;
            
            configuration.Properties[Environment.PrepareSql] = "false";
            configuration.Properties[Environment.WrapResultSets] = "true";
            configuration.Properties[Environment.UseProxyValidator] = "false";
            configuration.Properties[Environment.GenerateStatistics] = "false";
            configuration.Properties[Environment.BatchSize] = "1000";
            configuration.Properties[Environment.CommandTimeout] = "6000";
            configuration.Properties[Environment.UseSecondLevelCache] = "true";
            configuration.Properties[Environment.UseQueryCache] = "true";
            configuration.Properties[Environment.ShowSql] = "false";
            configuration.Properties[Environment.FormatSql] = "false";
            configuration.Properties[Environment.PropertyUseReflectionOptimizer] = "true";
            //configuration.Properties[Environment.Hbm2ddlKeyWords] = "auto-quote";

            configuration.Proxy(p => p.ProxyFactoryFactory<DefaultProxyFactoryFactory>());
        }

    }
}