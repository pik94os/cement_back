﻿using System.Collections.Specialized;
using System.IO;

namespace DataAccess.Config
{
    public class ConfigProvider
    {
        public ConfigProvider(NameValueCollection appSettings, string connectionString, string appDomainAppPath)
        {
            ConnectionString = connectionString;

            AppDomainAppPath = appDomainAppPath;

            _fileDirectoryPath = appSettings["FileDirectory"] ?? Path.Combine(AppDomainAppPath, "FileDirectory");
        }

        public readonly string AppDomainAppPath;

        public readonly string ConnectionString;

        #region FileDirectory

        private readonly string _fileDirectoryPath;

        private DirectoryInfo _filesDirectory;

        public DirectoryInfo FilesDirectory
        {
            get
            {
                if (_filesDirectory != null)
                {
                    return _filesDirectory;
                }

                if (_filesDirectory == null)
                {
                    _filesDirectory = new DirectoryInfo(_fileDirectoryPath);
                }

                if (!_filesDirectory.Exists)
                {
                    _filesDirectory.Create();
                }

                return _filesDirectory;
            }
        }

        #endregion FileDirectory
    }
}