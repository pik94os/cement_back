﻿using System;
using Cement.Core.Entities;
using Newtonsoft.Json;

namespace Cement.Protocol
{
    public class TransportUnitArendProxy : TransportUnitArend, IDataResult
    {
        [JsonProperty(PropertyName = "p_transport_unit")]
        public override TransportUnit TransportUnit { get; set; }

        [JsonProperty(PropertyName = "p_renter")]
        public override Client Arendator { get; set; }

        [JsonProperty(PropertyName = "p_date_start")]
        public override DateTime? DateStart { get; set; }

        [JsonProperty(PropertyName = "p_date_end")]
        public override DateTime? DateEnd { get; set; }
    }

    public class TransportUnitArendDetailProxy : IDataResult
    {
        public object auto { get; set; }

        public object trailer { get; set; }

        public object driver { get; set; }

        public object firm { get; set; }
    }
}