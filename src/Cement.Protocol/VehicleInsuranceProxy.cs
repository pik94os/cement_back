﻿using System;
using Cement.Core.Entities;
using Newtonsoft.Json;

namespace Cement.Protocol
{
    public class VehicleInsuranceProxy : VehicleInsurance, IDataResult
    {
        [JsonProperty(PropertyName = "id")]
        public override long Id { get; set; }

        [JsonProperty(PropertyName = "p_transport_unit")]
        public override Vehicle Vehicle { get; set; }

        [JsonProperty(PropertyName = "p_name")]
        public override string Name { get; set; }

        [JsonProperty(PropertyName = "p_number")]
        public override string No { get; set; }

        [JsonProperty(PropertyName = "p_company")]
        public override Client InsuranceCompany { get; set; }

        [JsonProperty(PropertyName = "p_contact")]
        public override string ContactPerson { get; set; }

        [JsonProperty(PropertyName = "p_phone")]
        public override string Phone { get; set; }

        [JsonProperty(PropertyName = "p_date_start")]
        public override DateTime? DateStart { get; set; }

        [JsonProperty(PropertyName = "p_date_end")]
        public override DateTime? DateEnd { get; set; }

        [JsonProperty(PropertyName = "p_sum")]
        public override decimal? InsuranceSum { get; set; }

        [JsonProperty(PropertyName = "p_document_file")]
        public override EntityFile Document { get; set; }
    }

    public class VehicleInsuranceOutProxy : VehicleInsuranceProxy
    {
        [JsonIgnore]
        public override Vehicle Vehicle { get; set; }

        [JsonIgnore]
        public override DateTime? DateStart { get; set; }

        [JsonIgnore]
        public override DateTime? DateEnd { get; set; }

        [JsonIgnore]
        public override Client InsuranceCompany { get; set; }

        [JsonProperty(PropertyName = "p_address")]
        public string Address { get; set; }

        [JsonProperty(PropertyName = "p_date_start")]
        public string DateStartStr { get; set; }

        [JsonProperty(PropertyName = "p_date_end")]
        public string DateEndStr { get; set; }

        [JsonProperty(PropertyName = "p_transport_unit")]
        public long? VehicleId { get; set; }

        [JsonProperty(PropertyName = "p_transport_unit_display")]
        public string VehicleDisplay { get; set; }

        [JsonProperty(PropertyName = "p_company")]
        public string InsuranceCompanyDisplay { get; set; }
    }
}