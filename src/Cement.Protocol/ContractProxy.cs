﻿using System.Collections.Generic;
using Newtonsoft.Json;

namespace Cement.Protocol
{
    public class ContractProxy
    {
         
    }

    public class ContractServiceTreeElement
    {
        [JsonProperty(PropertyName = "id")]
        public long Id { get; set; }

        [JsonProperty(PropertyName = "text")]
        public string Name { get; set; }

        [JsonProperty(PropertyName = "leaf")]
        public bool Leaf { get; set; }

        [JsonProperty(PropertyName = "expanded")]
        public bool Expanded { get; set; }

        [JsonProperty(PropertyName = "children")]
        public List<ContractServiceTreeElement> Children { get; set; }

        [JsonProperty(PropertyName = "p_group")]
        public string GroupName { get; set; }

        [JsonProperty(PropertyName = "p_subgroup")]
        public string SubGroupName { get; set; }

        [JsonProperty(PropertyName = "p_manufacturer")]
        public string ManufacturerName { get; set; }

        [JsonProperty(PropertyName = "p_unit")]
        public string UnitName { get; set; }

        [JsonProperty(PropertyName = "p_tax")]
        public string Tax { get; set; }

        [JsonProperty(PropertyName = "p_price")]
        public decimal Price { get; set; }
    }

    public class ContractRequestProxy
    {
        public long p_contract_id { get; set; }
    }
}