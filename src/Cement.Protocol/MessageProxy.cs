﻿using System;
using Cement.Core.Entities;
using Cement.Core.Enums;
using Newtonsoft.Json;

namespace Cement.Protocol
{
    public class MessageProxy : Message, IDataResult
    {
        [JsonProperty(PropertyName = "p_date")]
        public override DateTime? Date { get; set; }

        [JsonProperty(PropertyName = "p_kind")]
        public override MessageType MessageType { get; set; }

        [JsonProperty(PropertyName = "p_theme")]
        public override string Subject { get; set; }

        [JsonProperty(PropertyName = "p_author")]
        public override Employee Author { get; set; }

        [JsonProperty(PropertyName = "p_importance")]
        public override ImportanceType Importance { get; set; }

        [JsonProperty(PropertyName = "p_security")]
        public override SecrecyType Secrecy { get; set; }

        [JsonProperty(PropertyName = "p_file")]
        public override EntityFile File { get; set; }

        [JsonProperty(PropertyName = "p_repeat_display")]
        public override string RepeatDisplay { get; set; }

        [JsonProperty(PropertyName = "p_repeat")]
        public override string RepeatDetails { get; set; }
    }
}