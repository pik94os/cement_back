﻿using Newtonsoft.Json;

namespace Cement.Protocol
{
    public class TransportCategory : TreeElement
    {
        [JsonProperty(PropertyName = "p_show_char")]
        public bool HasСharacteristics { get; set; }
    }
}