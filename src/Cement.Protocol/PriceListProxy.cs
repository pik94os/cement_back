﻿using System;
using System.Collections.Generic;
using Cement.Core.Entities;
using Cement.Core.Enums;
using Newtonsoft.Json;

namespace Cement.Protocol
{
    public class PriceListProxy : PriceList, IDataResult
    {
        [JsonProperty(PropertyName = "id")]
        public override long Id { get; set; }

        [JsonProperty(PropertyName = "p_name")]
        public override string Name { get; set; }

        [JsonProperty(PropertyName = "p_replace")]
        public override PriceList ReplacementPriceList { get; set; }

        [JsonProperty(PropertyName = "p_date_start")]
        public override DateTime? DateStart { get; set; }

        [JsonProperty(PropertyName = "p_date_end")]
        public override DateTime? DateEnd { get; set; }

        [JsonProperty(PropertyName = "p_comment")]
        public override string Description { get; set; }

        [JsonProperty(PropertyName = "p_products")]
        public override string ProductsJsonString { get; set; }

        [JsonProperty(PropertyName = "p_author")]
        public override Employee Author { get; set; }

        [JsonProperty(PropertyName = "p_sign_for")]
        public override Employee Signer { get; set; }

        [JsonProperty(PropertyName = "p_documents")]
        public override List<string> RelatedDocumentIds { get; set; }

        [JsonProperty(PropertyName = "p_deadline")]
        public override DateTime? ReviewBefore { get; set; }

        [JsonProperty(PropertyName = "p_importance")]
        public override ImportanceType Importance { get; set; }

        [JsonProperty(PropertyName = "p_security")]
        public override SecrecyType Secrecy { get; set; }

        [JsonProperty(PropertyName = "p_import")]
        public override EntityFile File { get; set; }
    }
}