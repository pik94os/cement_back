﻿using System;
using Cement.Core.Entities;
using Newtonsoft.Json;

namespace Cement.Protocol
{
    public class VehicleSparePartServiceProxy : VehicleSparePartService, IDataResult
    {
        [JsonProperty(PropertyName = "p_transport_unit")]
        public override Vehicle Vehicle { get; set; }

        [JsonProperty(PropertyName = "p_date")]
        public override DateTime? Date { get; set; }

        [JsonProperty(PropertyName = "p_odometer")]
        public override decimal? Mileage { get; set; }

        [JsonProperty(PropertyName = "p_name")]
        public override string Name { get; set; }

        [JsonProperty(PropertyName = "p_autoservice")]
        public override Client CarService { get; set; }

        [JsonProperty(PropertyName = "p_master")]
        public override string Master { get; set; }

        [JsonProperty(PropertyName = "p_unit")]
        public override MeasureUnit MeasureUnit { get; set; }

        [JsonProperty(PropertyName = "p_count")]
        public override decimal? Amount { get; set; }

        [JsonProperty(PropertyName = "p_date_start")]
        public override DateTime? DateStart { get; set; }

        [JsonProperty(PropertyName = "p_date_end")]
        public override DateTime? DateEnd { get; set; }

        [JsonProperty(PropertyName = "p_cost")]
        public override decimal? Cost { get; set; }

        [JsonProperty(PropertyName = "p_driver")]
        public override Employee Driver { get; set; }

        [JsonProperty(PropertyName = "p_document")]
        public override string DocName { get; set; }

        [JsonProperty(PropertyName = "p_document_file")]
        public override EntityFile DocFile { get; set; }
    }
}