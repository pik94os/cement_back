﻿using System;
using Cement.Core.Entities;
using Cement.Core.Enums;
using Cement.Protocol.JsonConverters;
using Newtonsoft.Json;

namespace Cement.Protocol
{
    public class VehicleProxy : Vehicle, IDataResult
    {
        [JsonProperty(PropertyName = "id")]
        public override long Id { get; set; }

        #region Общие сведения

        [JsonProperty(PropertyName = "p_category")]
        public override VehicleCategory Category { get; set; }

        [JsonProperty(PropertyName = "p_sub_category")]
        public override VehicleSubCategory SubCategory { get; set; }

        [JsonProperty(PropertyName = "p_model")]
        public override VehicleModel Model { get; set; }

        [JsonProperty(PropertyName = "p_mark_model_spec")]
        public override VehicleModification Modification { get; set; }

        [JsonProperty(PropertyName = "p_year")]
        public override int? ReleaseYear { get; set; }

        [JsonProperty(PropertyName = "p_number")]
        public override string Number { get; set; }

        [JsonProperty(PropertyName = "p_number_country_code")]
        public override CountryCode NumCountryCode { get; set; }

        [JsonProperty(PropertyName = "p_number_region_code")]
        public override RegionCode NumRegionCode { get; set; }

        [JsonProperty(PropertyName = "p_img")]
        public override EntityFile Photo { get; set; }

        #endregion Общие сведения

        #region Регистрационные данные

        [JsonProperty(PropertyName = "p_reg_vin")]
        public override string Vin { get; set; }

        [JsonProperty(PropertyName = "p_reg_engine")]
        public override string EngineSerialNum { get; set; }

        [JsonProperty(PropertyName = "p_reg_chasis")]
        public override string СhassisSerialNum { get; set; }

        [JsonProperty(PropertyName = "p_reg_body")]
        public override string Body { get; set; }

        [JsonProperty(PropertyName = "p_reg_wheel_formula")]
        public override VehicleWheelBase WheelBase { get; set; }

        [JsonProperty(PropertyName = "p_reg_category")]
        public override DriverCategory DriverCategory { get; set; }

        [JsonProperty(PropertyName = "p_reg_eco_class")]
        public override decimal? EcoClass { get; set; }

        [JsonProperty(PropertyName = "p_reg_color")]
        public override string Color { get; set; }

        [JsonProperty(PropertyName = "p_reg_power")]
        public override decimal? Power { get; set; }

        [JsonProperty(PropertyName = "p_reg_engine_volume")]
        public override decimal? EngineVolume { get; set; }

        [JsonProperty(PropertyName = "p_reg_max_mass")]
        public override decimal? MaxMass { get; set; }

        [JsonProperty(PropertyName = "p_reg_truck_mass")]
        public override decimal? VehicleMass { get; set; }

        #endregion Регистрационные данные

        #region ПТС
        [JsonProperty(PropertyName = "p_pts_serie")]
        public override string PassportSerie { get; set; }

        [JsonProperty(PropertyName = "p_pts_number")]
        public override string PassportNumber { get; set; }

        [JsonProperty(PropertyName = "p_pts_manufacturer")]
        public override string PassportManufacturer { get; set; }

        [JsonProperty(PropertyName = "p_pts_date")]
        public override DateTime? PassportDate { get; set; }

        [JsonProperty(PropertyName = "p_pts_page1")]
        public override EntityFile PasspostScan1 { get; set; }

        [JsonProperty(PropertyName = "p_pts_page2")]
        public override EntityFile PasspostScan2 { get; set; }
        #endregion ПТС

        #region Свидетельство
        [JsonProperty(PropertyName = "p_document_serie")]
        public override string DocumentSerie { get; set; }

        [JsonProperty(PropertyName = "p_document_number")]
        public override string DocumentNumber { get; set; }

        [JsonProperty(PropertyName = "p_document_owner")]
        public override string DocumentOwner { get; set; }

        [JsonProperty(PropertyName = "p_document_date")]
        public override DateTime? DocumentDate { get; set; }

        [JsonProperty(PropertyName = "p_document_page1")]
        public override EntityFile DocumentScan1 { get; set; }

        [JsonProperty(PropertyName = "p_document_page2")]
        public override EntityFile DocumentScan2 { get; set; }
        #endregion Свидетельство

        #region Характеристика

        [JsonProperty(PropertyName = "p_char_class")]
        public override VehicleClass Class { get; set; }

        [JsonProperty(PropertyName = "p_char_ops")]
        public override VehicleType Type { get; set; }

        [JsonProperty(PropertyName = "p_char_type")]
        public override VehicleBodyType BodyType { get; set; }

        [JsonProperty(PropertyName = "p_char_length")]
        public override decimal? Length { get; set; }

        [JsonProperty(PropertyName = "p_char_width")]
        public override decimal? Width { get; set; }

        [JsonProperty(PropertyName = "p_char_height")]
        public override decimal? Height { get; set; }

        [JsonProperty(PropertyName = "p_char_load_back")]
        public override bool IsRearLoading { get; set; }

        [JsonProperty(PropertyName = "p_char_load_left")]
        public override bool IsSideLoading { get; set; }

        [JsonProperty(PropertyName = "p_char_load_top")]
        public override bool IsOverLoading { get; set; }

        [JsonProperty(PropertyName = "p_char_capacity")]
        public override decimal? Capacity { get; set; }

        [JsonProperty(PropertyName = "p_char_volume")]
        public override decimal? Volume { get; set; }

        [JsonProperty(PropertyName = "p_char_additional")]
        public override VehicleAdditionalOptions Option { get; set; }

        [JsonProperty(PropertyName = "p_add_additional")]
        public override string ExtraCharacteristic { get; set; }

        #endregion Характеристика

        #region Дополнительно
        [JsonProperty(PropertyName = "p_add_subdivision")]
        public override VehicleDepartment Division { get; set; }

        [JsonProperty(PropertyName = "p_add_column")]
        public override VehicleColumn Column { get; set; }

        [JsonProperty(PropertyName = "p_add_garage_number")]
        public override string GarageNumber { get; set; }
        #endregion Дополнительно
    }

    public class TreeElemendVehicle : IDataResult
    {
        [JsonProperty(PropertyName = "id")]
        public long Id { get; set; }

        [JsonProperty(PropertyName = "p_name")]
        public string Name { get; set; }

        [JsonProperty(PropertyName = "leaf")]
        public bool Leaf { get; set; }

        [JsonProperty(PropertyName = "p_category_display")]
        public string CategoryName { get; set; }

        [JsonProperty(PropertyName = "p_sub_category_display")]
        public string SubCategoryName { get; set; }

        [JsonProperty(PropertyName = "p_mark_model_spec_display")]
        public string BrandModelModificationText { get; set; }

        [JsonProperty(PropertyName = "p_number_display")]
        public string Number { get; set; }

        [JsonProperty(PropertyName = "p_class_display")]
        public string ClassName { get; set; }

        [JsonProperty(PropertyName = "p_size_display")]
        public string SizeText { get; set; }

        [JsonProperty(PropertyName = "p_go_display")]
        public string GoText { get; set; }

        [JsonProperty(PropertyName = "p_char_type_display")]
        public string BodyTypeName { get; set; }

        [JsonProperty(PropertyName = "p_char_ops")]
        public VehicleType? VehicleType { get; set; }

        [JsonProperty(PropertyName = "p_load_back")]
        public bool IsRearLoading { get; set; }

        [JsonProperty(PropertyName = "p_load_top")]
        public bool IsOverLoading { get; set; }

        [JsonProperty(PropertyName = "p_load_left")]
        public bool IsSideLoading { get; set; }

        [JsonProperty(PropertyName = "p_additional_options_display")]
        public string OptionName { get; set; }

        [JsonProperty(PropertyName = "p_additional_chars_display")]
        public string ExtraCharacteristic { get; set; }

        [JsonProperty(PropertyName = "p_department_display")]
        public string DepartmentName { get; set; }

        [JsonProperty(PropertyName = "p_column_display")]
        public string ColumnName { get; set; }

        [JsonProperty(PropertyName = "p_garage_number")]
        public string GarageNumber { get; set; }

        [JsonProperty(PropertyName = "show_char")]
        public bool HasСharacteristics { get; set; }
    }

    public class VehicleDetailProxy : VehicleProxy
    {
        [JsonIgnore]
        public override VehicleCategory Category { get; set; }

        [JsonProperty(PropertyName = "p_category")]
        public long? CategoryId { get; set; }

        [JsonIgnore]
        public override VehicleSubCategory SubCategory { get; set; }

        [JsonProperty(PropertyName = "p_sub_category")]
        public long? SubCategoryId { get; set; }

        [JsonIgnore]
        public override VehicleModel Model { get; set; }

        [JsonProperty(PropertyName = "p_model")]
        public long? ModelId { get; set; }

        [JsonIgnore]
        public override VehicleModification Modification { get; set; }

        [JsonProperty(PropertyName = "p_mark_model_spec")]
        public long? ModificationId { get; set; }

        [JsonProperty(PropertyName = "p_mark_model_spec_display")]
        public override string Name { get; set; }

        [JsonProperty(PropertyName = "p_mark_display")]
        public string BrandName { get; set; }

        [JsonProperty(PropertyName = "p_model_display")]
        public string ModelName { get; set; }

        [JsonProperty(PropertyName = "p_modification_display")]
        public string ModificationName { get; set; }
        
        [JsonIgnore]
        public override CountryCode NumCountryCode { get; set; }

        [JsonProperty(PropertyName = "p_number_country_code")]
        public long? NumCountryCodeId { get; set; }

        [JsonIgnore]
        public override RegionCode NumRegionCode { get; set; }

        [JsonProperty(PropertyName = "p_number_region_code")]
        public long? NumRegionCodeId { get; set; }

        [JsonConverterAttribute(typeof(DownloadableConverter))]
        [JsonProperty(PropertyName = "p_img")]
        public long? PhotoLink { get; set; }

        [JsonProperty(PropertyName = "p_add_subdivision_display")]
        public string DepartmentName { get; set; }

        [JsonProperty(PropertyName = "p_add_column_display")]
        public string ColumnName { get; set; }

        [JsonProperty(PropertyName = "p_add_subdivision")]
        public long? DepartmentId { get; set; }

        [JsonProperty(PropertyName = "p_add_column")]
        public long? ColumnId { get; set; }

        [JsonIgnore]
        public override VehicleWheelBase WheelBase { get; set; }

        [JsonProperty(PropertyName = "p_reg_wheel_formula")]
        public long? WheelBaseId { get; set; }

        [JsonProperty(PropertyName = "p_reg_category")]
        public long? DriverCategoryId { get; set; }


        [JsonProperty(PropertyName = "p_reg_wheel_formula_display")]
        public string WheelFormulaName { get; set; }

        [JsonProperty(PropertyName = "p_number_display")]
        public override string NumberText { get; set; }

        [JsonProperty(PropertyName = "p_char_size")]
        public override string SizeText { get; set; }

        [JsonIgnore]
        public override DriverCategory DriverCategory { get; set; }

        [JsonProperty(PropertyName = "p_reg_category_display")]
        public string DriverCategoryName { get; set; }


        [JsonIgnore]
        public override VehicleClass Class { get; set; }

        [JsonProperty(PropertyName = "p_char_class")]
        public long? ClassId { get; set; }

        [JsonIgnore]
        public override VehicleBodyType BodyType { get; set; }
        
        [JsonProperty(PropertyName = "p_char_type")]
        public long? BodyTypeId { get; set; }

        [JsonIgnore]
        public override EntityFile PasspostScan1 { get; set; }

        [JsonConverterAttribute(typeof(DownloadableConverter))]
        [JsonProperty(PropertyName = "p_pts_page1")]
        public long? PasspostScan1Id { get; set; }

        [JsonIgnore]
        public override EntityFile PasspostScan2 { get; set; }

        [JsonConverterAttribute(typeof(DownloadableConverter))]
        [JsonProperty(PropertyName = "p_pts_page2")]
        public long? PasspostScan2Id { get; set; }


        [JsonIgnore]
        public override EntityFile DocumentScan1 { get; set; }

        [JsonConverterAttribute(typeof(DownloadableConverter))]
        [JsonProperty(PropertyName = "p_document_page1")]
        public long? DocumentScan1Id { get; set; }

        [JsonIgnore]
        public override EntityFile DocumentScan2 { get; set; }

        [JsonConverterAttribute(typeof(DownloadableConverter))]
        [JsonProperty(PropertyName = "p_document_page2")]
        public long? DocumentScan2Id { get; set; }

        [JsonProperty(PropertyName = "show_char")]
        public bool? HasСharacteristics { get; set; }
        
        [JsonIgnore]
        public override VehicleAdditionalOptions Option { get; set; }

        [JsonProperty(PropertyName = "p_char_additional")]
        public long? OptionId { get; set; }
        
        [JsonProperty(PropertyName = "p_char_additional_display")]
        public string OptionName { get; set; }
        
        [JsonProperty(PropertyName = "p_char_type_display")]
        public string BodyTypeName { get; set; }

        [JsonProperty(PropertyName = "p_char_class_display")]
        public string ClassName { get; set; }



        [JsonIgnore]
        public override VehicleType Type { get; set; }

        [JsonProperty(PropertyName = "p_char_ops")]
        public VehicleType? TypeNullable { get; set; }
    }
}