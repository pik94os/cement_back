﻿using System;
using System.Collections.Generic;
using System.Linq;
using Cement.Core.Enums;
using Newtonsoft.Json;

namespace Cement.Protocol
{
    public class AgreeRequest
    {
        public class Doc
        {
            public long Id { get; set; }

            public DocumentType DocumentType { get; set; }
        }

        public List<Doc> Docs { get; set; }

        public long EmployeeId { get; set; }

        public string Comment { get; set; }

        public bool Send { get; set; }
    }

    public class AgreementProxy
    {
        [JsonProperty(PropertyName = "p_item")]
        public long ItemId { get; set; }

        [JsonProperty(PropertyName = "p_data")]
        public string Data { get; set; }

        /// <summary>
        /// Отправить дальше
        /// </summary>
        [JsonProperty(PropertyName = "p_send")]
        public bool Send { get; set; }

        public Tuple<long?, string> GetEmployeeCommentTuple()
        {
            long? employeeId = null;
            string comment = null;
            
            if (!string.IsNullOrWhiteSpace(Data))
            {
                var dataStr = Data.Trim('[', ']');

                try
                {
                    var data = JsonConvert.DeserializeObject<AgreementDataProxy>(dataStr);

                    if (data != null)
                    {
                        employeeId = data.EmployeeId;
                        comment = data.Comment;
                    }
                }
                catch { }
            }

            return new Tuple<long?, string>(employeeId, comment);
        }

        public List<long> GetRecipients()
        {
            if (!string.IsNullOrWhiteSpace(Data))
            {
                try
                {
                    var data = JsonConvert.DeserializeObject<List<AgreementDataProxy>>(Data);

                    if (data != null)
                    {
                        return data.Where(x => x.EmployeeId.HasValue).Select(x => x.EmployeeId.Value).ToList();
                    }
                }
                catch { }
            }

            return new List<long>();
        }
    }

    public class AgreementDataProxy
    {
        [JsonProperty(PropertyName = "p_employee_id")]
        public long? EmployeeId { get; set; }

        [JsonProperty(PropertyName = "p_comment")]
        public string Comment { get; set; }
    }
}