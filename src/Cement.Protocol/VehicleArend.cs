﻿using System;
using Cement.Core.Entities;
using Newtonsoft.Json;

namespace Cement.Protocol
{
    public class VehicleArendProxy : VehicleArend, IDataResult
    {
        [JsonProperty(PropertyName = "p_transport")]
        public override Vehicle Vehicle { get; set; }

        [JsonProperty(PropertyName = "p_renter")]
        public override Client Arendator { get; set; }

        [JsonProperty(PropertyName = "p_date_start")]
        public override DateTime? DateStart { get; set; }

        [JsonProperty(PropertyName = "p_date_end")]
        public override DateTime? DateEnd { get; set; }
    }


    public class VehicleArendDetailProxy : IDataResult
    {
        public object auto { get; set; }
        public object firm { get; set; }
        public object service { get; set; }
    }
}