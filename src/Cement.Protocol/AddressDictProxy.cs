﻿using Newtonsoft.Json;

namespace Cement.Protocol
{
    public class AddressDictProxy
    {
        public long Id { get; set; }

        [JsonProperty(PropertyName = "p_index")]
        public string Index { get; set; }

        [JsonProperty(PropertyName = "p_country")]
        public string Country { get; set; }

        [JsonProperty(PropertyName = "p_region")]
        public string Region { get; set; }

        [JsonProperty(PropertyName = "p_district")]
        public string Area { get; set; }

        [JsonProperty(PropertyName = "p_city")]
        public string Locality { get; set; }

        [JsonProperty(PropertyName = "p_street")]
        public string Street { get; set; } 
    }
}