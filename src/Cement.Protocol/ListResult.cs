﻿using System.Collections.Generic;
using Cement.Protocol.JsonConverters;
using Newtonsoft.Json;

namespace Cement.Protocol
{
    public class ListResult<T> : IDataResult
    {
        [JsonProperty(PropertyName = "data")]
        public virtual List<T> Data { get; set; }
            
        [JsonProperty(PropertyName = "total")]
        public virtual long Count { get; set; }
    }

    public class ObjectListResult : IDataResult
    {
        [JsonProperty(PropertyName = "data")]
        public object Data { get; set; }

        [JsonProperty(PropertyName = "total")]
        public long Count { get; set; }
    }

    [JsonConverterAttribute(typeof(ObjectResultConverter))]
    public class ObjectResult : IDataResult
    {
        public Dictionary<string, object> Data;

        public ObjectResult()
        {
        }

        public ObjectResult(Dictionary<string, object> data)
        {
            Data = data;
        }
    }
}