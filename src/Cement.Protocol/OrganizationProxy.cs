﻿using System;
using System.Collections.Generic;
using Cement.Core.Entities;
using Cement.Core.Enums;
using Newtonsoft.Json;

namespace Cement.Protocol
{
    public class OrganizationProxy : Organization, IDataResult
    {
        [JsonProperty(PropertyName = "id")]
        public override long Id { get; set; }

        [JsonProperty(PropertyName = "p_client_display")]
        public override string Name { get; set; }

        [JsonProperty(PropertyName = "p_client")]
        public override Client Client { get; set; }

        [JsonProperty(PropertyName = "p_logo")]
        public override EntityFile Logo { get; set; }

        [JsonProperty(PropertyName = "p_email")]
        public string Email { get; set; }

        [JsonProperty(PropertyName = "p_pass")]
        public string Pass { get; set; }
    }
}