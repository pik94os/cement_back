﻿using System;
using Cement.Core.Entities;
using Newtonsoft.Json;

namespace Cement.Protocol
{
    public class SignatureProxy : Signature, IDataResult
    {
        [JsonProperty(PropertyName = "p_employee")]
        public override Employee Recipient { get; set; }

        [JsonProperty(PropertyName = "p_sign_reason")]
        public override InternalDocument Reason { get; set; }

        [JsonProperty(PropertyName = "p_sign_for_who")]
        public override Employee Truster { get; set; }

        [JsonProperty(PropertyName = "p_sign_date_from")]
        public override DateTime? DateStart { get; set; }

        [JsonProperty(PropertyName = "p_sign_date_end")]
        public override DateTime? DateEnd { get; set; }
    }
}