﻿namespace Cement.Protocol
{
    public class DictionaryData
    {
        public string DictionaryName { get; set; }

        public string DataString { get; set; }
    }
}