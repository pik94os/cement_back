﻿using System;
using System.Collections.Generic;
using Cement.Core.Entities;
using Cement.Core.Enums;
using Newtonsoft.Json;

namespace Cement.Protocol
{
    public class InternalDocumentProxy : InternalDocument, IDataResult
    {
        [JsonProperty(PropertyName = "p_number")]
        public override string No { get; set; }

        [JsonProperty(PropertyName = "p_date")]
        public override DateTime? Date { get; set; }

        [JsonProperty(PropertyName = "p_kind")]
        public override InternalDocumentType InternalDocumentType { get; set; }

        [JsonProperty(PropertyName = "p_nomenklature")]
        public override Nomenclature Nomenclature { get; set; }

        [JsonProperty(PropertyName = "p_theme")]
        public override string Subject { get; set; }

        [JsonProperty(PropertyName = "p_content")]
        public override string ContentString { get; set; }

        [JsonProperty(PropertyName = "p_author")]
        public override Employee Author { get; set; }

        [JsonProperty(PropertyName = "p_sign_for")]
        public override Employee Signer { get; set; }

        [JsonProperty(PropertyName = "p_documents")]
        public override List<string> RelatedDocumentIds { get; set; }

        [JsonProperty(PropertyName = "p_deadline")]
        public override DateTime? ReviewBefore { get; set; }

        [JsonProperty(PropertyName = "p_importance")]
        public override ImportanceType Importance { get; set; }

        [JsonProperty(PropertyName = "p_security")]
        public override SecrecyType Secrecy { get; set; }

        [JsonProperty(PropertyName = "p_import")]
        public override EntityFile File { get; set; }
    }
}