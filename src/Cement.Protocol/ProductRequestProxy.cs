﻿using System;
using System.Collections.Generic;
using Cement.Core.Entities;
using Cement.Core.Enums;
using Newtonsoft.Json;

namespace Cement.Protocol
{
    public class ProductRequestProxy : ProductRequest, IDataResult
    {
        [JsonProperty(PropertyName = "id")]
        public override long Id { get; set; }

        [JsonProperty(PropertyName = "p_number")]
        public override string Number { get; set; }

        [JsonProperty(PropertyName = "p_date")]
        public override DateTime? Date { get; set; }

        #region Общие данные

        [JsonProperty(PropertyName = "p_load_kind")]
        public override ShipmentType ShipmentType { get; set; }

        [JsonProperty(PropertyName = "p_load_date")]
        public override DateTime? ShipmentDateTime { get; set; }

        [JsonProperty(PropertyName = "p_unload_date")]
        public override DateTime? UnshipmentDateTime { get; set; }

        [JsonProperty(PropertyName = "p_load_time")]
        public override DateTime? ShipmentTime { get; set; }

        [JsonProperty(PropertyName = "p_unload_time")]
        public override DateTime? UnshipmentTime { get; set; }

        #endregion Общие данные

        #region Поставщик

        [JsonProperty(PropertyName = "p_supplier")]
        public override Organization Provider { get; set; }

        [JsonProperty(PropertyName = "p_contract")]
        public override Contract ProviderContract { get; set; }

        [JsonProperty(PropertyName = "p_supplier_contact")]
        public override Employee ProviderContactPerson { get; set; }

        #endregion Поставщик

        #region Товар

        [JsonProperty(PropertyName = "p_product")]
        public override Product Product { get; set; }

        [JsonProperty(PropertyName = "p_product_unit")]
        public override MeasureUnit ProductMeasureUnit { get; set; }

        [JsonProperty(PropertyName = "p_product_count")]
        public override int ProductCount { get; set; }

        [JsonProperty(PropertyName = "p_product_price")]
        public override decimal? ProductPrice { get; set; }

        [JsonProperty(PropertyName = "p_product_tax")]
        public override decimal? ProductTax { get; set; }

        [JsonProperty(PropertyName = "p_product_fact_count")]
        public override int? ProductFactCount { get; set; }

        #endregion Товар

        #region Транспортная единица

        [JsonProperty(PropertyName = "p_char_ops")]
        public override VehicleType VehicleType { get; set; }

        [JsonProperty(PropertyName = "p_char_type")]
        public override VehicleBodyType VehicleBodyType { get; set; }

        [JsonProperty(PropertyName = "p_char_load_top")]
        public override bool VehicleIsRearLoading { get; set; }

        [JsonProperty(PropertyName = "p_char_load_left")]
        public override bool VehicleIsSideLoading { get; set; }

        [JsonProperty(PropertyName = "p_char_load_back")]
        public override bool VehicleIsOverLoading { get; set; }

        [JsonProperty(PropertyName = "p_char_additional")]
        public override VehicleAdditionalOptions VehicleOption { get; set; }

        [JsonProperty(PropertyName = "p_add_additional")]
        public override string VehicleExtraCharacteristic { get; set; }

        #endregion Транспортная единица

        #region Грузополучатель

        [JsonProperty(PropertyName = "p_getter")]
        public override Organization Consignee { get; set; }

        [JsonProperty(PropertyName = "p_getter_contact")]
        public override Employee ConsigneeContactPerson { get; set; }

        [JsonProperty(PropertyName = "p_warehouse")]
        public override Warehouse Warehouse { get; set; }

        #endregion Грузополучатель

        #region Плательщик

        [JsonProperty(PropertyName = "p_payer")]
        public override Organization Payer { get; set; }

        [JsonProperty(PropertyName = "p_payer_contact")]
        public override Employee PayerContactPerson { get; set; }

        #endregion Плательщик

        #region Дополнительные данные

        [JsonProperty(PropertyName = "p_comment")]
        public override string ContentString { get; set; }

        [JsonProperty(PropertyName = "p_importance")]
        public override ImportanceType Importance { get; set; }

        [JsonProperty(PropertyName = "p_security")]
        public override SecrecyType Secrecy { get; set; }

        #endregion Дополнительные данные
    }

}