﻿using Newtonsoft.Json;

namespace Cement.Protocol
{
    public class DictionaryObject : IDataResult
    {
        [JsonProperty(PropertyName = "id")]
        public long Id { get; set; }

        [JsonProperty(PropertyName = "p_name")]
        public string Name { get; set; }
    }

    public class LeafDictionaryObject : DictionaryObject
    {
        [JsonProperty(PropertyName = "leaf")]
        public bool Leaf { get { return true; } }
    }

    public class DictionaryFlagObject : IDataResult
    {
        [JsonProperty(PropertyName = "id")]
        public long Id { get; set; }

        [JsonProperty(PropertyName = "p_name")]
        public string Name { get; set; }

        [JsonProperty(PropertyName = "flag")]
        public string Flag { get; set; }
    }
}