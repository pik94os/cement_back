﻿using System;
using System.Collections.Generic;
using Cement.Core.Entities;
using Cement.Core.Enums;
using Newtonsoft.Json;

namespace Cement.Protocol
{
    public class ProductProxy : Product, IDataResult
    {
        [JsonProperty(PropertyName = "id")]
        public override long Id { get; set; }

        [JsonProperty(PropertyName = "p_name")]
        public override string Name { get; set; }

        [JsonProperty(PropertyName = "p_group")]
        public override ProductGroup Group { get; set; }

        [JsonProperty(PropertyName = "p_subgroup")]
        public override ProductSubGroup SubGroup { get; set; }

        [JsonProperty(PropertyName = "p_trade_mark")]
        public override string Brand { get; set; }

        [JsonProperty(PropertyName = "p_unit")]
        public override MeasureUnit MeasureUnit { get; set; }

        [JsonProperty(PropertyName = "p_img")]
        public override EntityFile Photo { get; set; }
     
        #region Происхождение

        [JsonProperty(PropertyName = "p_found_country")]
        public override Country Country { get; set; }

        [JsonProperty(PropertyName = "p_found_manufacturer")]
        public override Client Manufacturer { get; set; }

        [JsonProperty(PropertyName = "p_found_packer")]
        public override Client Packer { get; set; }

        [JsonProperty(PropertyName = "p_found_importer")]
        public override Client Importer { get; set; }

        #endregion

        #region Коды

        [JsonProperty(PropertyName = "p_code_okp")]
        public override string Okp { get; set; }

        [JsonProperty(PropertyName = "p_code_th_ved")]
        public override string TnVed { get; set; }

        [JsonProperty(PropertyName = "p_code_zd")]
        public override string CargoTrainCode { get; set; }

        [JsonProperty(PropertyName = "p_code_bar")]
        public override string Barcode { get; set; }

        [JsonProperty(PropertyName = "p_code_okun")]
        public override string Okun { get; set; }

        [JsonProperty(PropertyName = "p_code_partnumber")]
        public override string Article { get; set; }

        #endregion

        #region Технические условия

        [JsonProperty(PropertyName = "p_tu_gost")]
        public override string Gost { get; set; }

        [JsonProperty(PropertyName = "p_tu_gost_r")]
        public override string GostR { get; set; }

        [JsonProperty(PropertyName = "p_tu_ost")]
        public override string Ost { get; set; }

        [JsonProperty(PropertyName = "p_tu_stp")]
        public override string Stp { get; set; }

        [JsonProperty(PropertyName = "p_tu_tu")]
        public override string Tu { get; set; }

        #endregion

        #region Сертификаты

        [JsonProperty(PropertyName = "p_sert_soot_number")]
        public override string ConformityCertificateNumber { get; set; }

        [JsonProperty(PropertyName = "p_sert_soot_img1")]
        public override EntityFile ConformityCertificateScan { get; set; }

        [JsonProperty(PropertyName = "p_sert_soot_img2")]
        public override EntityFile ConformityCertificateScan2 { get; set; }

        [JsonProperty(PropertyName = "p_sert_soot_start")]
        public override DateTime? ConformityCertificateStartDate { get; set; }

        [JsonProperty(PropertyName = "p_sert_soot_end")]
        public override DateTime? ConformityCertificateEndDate { get; set; }

        [JsonProperty(PropertyName = "p_sert_deklar_number")]
        public override string ConformityDeclarationNumber { get; set; }

        [JsonProperty(PropertyName = "p_sert_deklar_img")]
        public override EntityFile ConformityDeclarationScan { get; set; }

        [JsonProperty(PropertyName = "p_sert_deklar_start")]
        public override DateTime? ConformityDeclarationStartDate { get; set; }

        [JsonProperty(PropertyName = "p_sert_deklar_end")]
        public override DateTime? ConformityDeclarationEndDate { get; set; }

        [JsonProperty(PropertyName = "p_sert_san_number")]
        public override string HealthCertificateNumber { get; set; }

        [JsonProperty(PropertyName = "p_sert_san_img")]
        public override EntityFile HealthCertificateScan { get; set; }

        [JsonProperty(PropertyName = "p_sert_san_start")]
        public override DateTime? HealthCertificateStartDate { get; set; }

        [JsonProperty(PropertyName = "p_sert_san_end")]
        public override DateTime? HealthCertificateEndDate { get; set; }

        [JsonProperty(PropertyName = "p_sert_fire_number")]
        public override string FireSafetyCertificateNumber { get; set; }

        [JsonProperty(PropertyName = "p_sert_fire_img")]
        public override EntityFile FireSafetyCertificateScan { get; set; }

        [JsonProperty(PropertyName = "p_sert_fire_start")]
        public override DateTime? FireSafetyCertificateStartDate { get; set; }

        [JsonProperty(PropertyName = "p_sert_fire_end")]
        public override DateTime? FireSafetyCertificateEndDate { get; set; }

        [JsonProperty(PropertyName = "p_sert_fire_deklar")]
        public override string FireSafetyDeclarationNumber { get; set; }

        [JsonProperty(PropertyName = "p_sert_fire_deklar_img")]
        public override EntityFile FireSafetyDeclarationScan { get; set; }

        [JsonProperty(PropertyName = "p_sert_fire_deklar_start")]
        public override DateTime? FireSafetyDeclarationStartDate { get; set; }

        [JsonProperty(PropertyName = "p_sert_fire_deklar_end")]
        public override DateTime? FireSafetyDeclarationEndDate { get; set; }

        #endregion

        #region Класс опасности (ADR)

        [JsonProperty(PropertyName = "p_danger_class")]
        public override DangerClass DangerClass { get; set; }

        [JsonProperty(PropertyName = "p_danger_sub_class")]
        public override DangerSubClass DangerSubClass { get; set; }

        [JsonProperty(PropertyName = "p_danger_oon")]
        public override DangerUnCode DangerUnCode { get; set; }

        #endregion

        #region Тара/упаковка
        
        [JsonProperty(PropertyName = "p_package_display")]
        public override string PackageDisplayString { get; set; }

        [JsonProperty(PropertyName = "p_package")]
        public override string PackageJsonString { get; set; }

        #endregion

        [JsonProperty(PropertyName = "p_complect_display")]
        public override string ComplectString { get; set; }

        [JsonProperty(PropertyName = "p_complect")]
        public override List<long> ComplectProductsIds { get; set; }
    }

    public class ProductTreeElement
    {
        [JsonProperty(PropertyName = "id")]
        public long Id { get; set; }

        [JsonProperty(PropertyName = "leaf")]
        public bool Leaf { get; set; }

        [JsonProperty(PropertyName = "p_name")]
        public string Name { get; set; }
        
        [JsonProperty(PropertyName = "p_complect")]
        public bool Complect { get; set; }

        [JsonProperty(PropertyName = "p_manufacturer")]
        public string Manufacturer { get; set; }

        [JsonProperty(PropertyName = "p_trade_mark")]
        public string Brand { get; set; }

        [JsonProperty(PropertyName = "p_unit")]
        public string Unit { get; set; }
    }

}