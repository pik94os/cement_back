﻿using System;
using System.Collections.Generic;
using Cement.Core.Entities;
using Cement.Core.Enums;
using Newtonsoft.Json;

namespace Cement.Protocol
{
    public class PaymentAccountProxy : PaymentAccount, IDataResult
    {
        [JsonProperty(PropertyName = "id")]
        public override long Id { get; set; }

        [JsonProperty(PropertyName = "p_number")]
        public override string Number { get; set; }

        [JsonProperty(PropertyName = "p_replace_name")]
        public override Bank Bank { get; set; }

        [JsonProperty(PropertyName = "p_bik")]
        public override string Bik { get; set; }
    }
}