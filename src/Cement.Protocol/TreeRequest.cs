﻿using Cement.Core;

namespace Cement.Protocol
{
    public class TreeRequest
    {
        public long Node { get; set; }

        public TreeLevel Level { get; set; }

        //[JsonProperty(PropertyName = "p_firm_id")]
        //public long OrganizationId { get; set; } 
    }

    public class TreeRequestStringNode
    {
        public string Node { get; set; }
    }

    public class TreeRequestWithFirmId : TreeRequest
    {
        public long p_firm_id { get; set; }

        public long OrganizationId {
            get { return p_firm_id; }
        }
    }

    public class TreeRequestWithWarehouseId : TreeRequest
    {
        public long p_warehouse_id { get; set; }

        public long WarehouseId
        {
            get { return p_warehouse_id; }
        }
    }

    public class TreeRequestStringNodeWithFirmId : TreeRequestStringNode
    {
        public long p_firm_id { get; set; }

        public long request_id { get; set; }

        public long OrganizationId
        {
            get { return p_firm_id; }
        }
    }

    public class VehicleModelTreeRequest : TreeRequest
    {
        public long p_category { get; set; }

        public long p_sub_category { get; set; }
        
        public long CategoryId
        {
            get { return p_category; }
        }

        public long SubCategoryId
        {
            get { return p_sub_category; }
        }

        public bool IsBrandRequest { get; set; }
    }
}