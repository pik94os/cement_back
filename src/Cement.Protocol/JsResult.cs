﻿using Newtonsoft.Json;

namespace Cement.Protocol
{
    public class JsResult
    {
        public JsResult(string message = null)
        {
            Success = true;
            Msg = message;
        }

        public JsResult(long id, string message = null)
        {
            Id = id;
            Success = true;
            Msg = message;
        }

        [JsonProperty(PropertyName = "success")]
        public bool Success { get; set; }

        [JsonProperty(PropertyName = "msg")]
        public string Msg { get; set; }

        [JsonProperty(PropertyName = "id")]
        public long Id { get; set; }

        [JsonProperty(PropertyName = "data", DefaultValueHandling = DefaultValueHandling.Ignore, NullValueHandling = NullValueHandling.Ignore)]
        public object Data { get; set; }

        public static JsResult Successful(string message = null)
        {
            return new JsResult(message);
        }
    }
}