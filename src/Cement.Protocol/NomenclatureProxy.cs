﻿using System;
using System.Collections.Generic;
using Cement.Core.Entities;
using Cement.Core.Enums;
using Newtonsoft.Json;

namespace Cement.Protocol
{
    public class NomenclatureProxy : Nomenclature, IDataResult
    {
        [JsonProperty(PropertyName = "id")]
        public override long Id { get; set; }

        [JsonProperty(PropertyName = "p_index")]
        public override string CaseIndex { get; set; }

        [JsonProperty(PropertyName = "p_title")]
        public override string CaseTitle { get; set; }

        [JsonProperty(PropertyName = "p_count")]
        public override int? StorageUnitsCount { get; set; }

        [JsonProperty(PropertyName = "p_deadline")]
        public override string StorageLifeAndArticleNumbers { get; set; }

        [JsonProperty(PropertyName = "p_comment")]
        public override string Description { get; set; }

        [JsonProperty(PropertyName = "p_year")]
        public override int? Year { get; set; }
    }
}