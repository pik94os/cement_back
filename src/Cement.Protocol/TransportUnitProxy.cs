﻿using System;
using Cement.Core.Entities;
using Newtonsoft.Json;

namespace Cement.Protocol
{
    public class TransportUnitProxy : TransportUnit, IDataResult
    {
        [JsonProperty(PropertyName = "p_transport")]
        public override Vehicle Vehicle { get; set; }

        [JsonProperty(PropertyName = "p_trailer")]
        public override Vehicle Trailer { get; set; }

        [JsonProperty(PropertyName = "p_name")]
        public override string Name { get; set; }

        [JsonProperty(PropertyName = "p_date_start")]
        public override DateTime? DateStart { get; set; }
        
        [JsonProperty(PropertyName = "p_driver")]
        public override Employee Driver { get; set; }
    }

    public class TransportUnitOutProxy : IDataResult
    {
        [JsonProperty(PropertyName = "auto")]
        public object Vehicle { get; set; }

        [JsonProperty(PropertyName = "trailer")]
        public object Trailer { get; set; }

        [JsonProperty(PropertyName = "driver")]
        public object Driver { get; set; }
    }
}