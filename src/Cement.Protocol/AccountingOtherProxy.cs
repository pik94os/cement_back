﻿using System;
using Cement.Core.Entities;
using Newtonsoft.Json;

namespace Cement.Protocol
{
    public class AccountingOtherProxy : AccountingOtherBase, IDataResult
    {
        [JsonProperty(PropertyName = "p_number")]
        public override string No { get; set; }

        [JsonProperty(PropertyName = "p_date")]
        public override DateTime? Date { get; set; }
        
        [JsonProperty(PropertyName = "p_type")]
        public override AccountingOtherDocumentType AccountingOtherDocumentType { get; set; }

        [JsonProperty(PropertyName = "p_movementbill_kind")]
        public AccountingOtherMovementBillKind AccountingOtherMovementBillKind { get; set; }

        [JsonProperty(PropertyName = "p_writeoffact_kind")]
        public AccountingOtherWriteoffActKind AccountingOtherWriteoffActKind { get; set; }

        
    }
}