﻿using System;
using Cement.Core.Entities;
using Newtonsoft.Json;

namespace Cement.Protocol
{
    public class VehicleMileageProxy : VehicleMileage, IDataResult
    {
        [JsonProperty(PropertyName = "id")]
        public override long Id { get; set; }

        [JsonProperty(PropertyName = "p_date")]
        public override DateTime? Date { get; set; }

        [JsonProperty(PropertyName = "p_transport_unit")]
        public override Vehicle Vehicle { get; set; }

        [JsonProperty(PropertyName = "p_value")]
        public override decimal? Mileage { get; set; }
    }

    public class VehicleMileageOutProxy : VehicleMileageProxy
    {
        [JsonIgnore]
        public override Vehicle Vehicle { get; set; }

        [JsonIgnore]
        public override DateTime? Date { get; set; }

        [JsonProperty(PropertyName = "p_date")]
        public string DateStr { get; set; }

        [JsonProperty(PropertyName = "p_transport_unit")]
        public long? VehicleId { get; set; }

        [JsonProperty(PropertyName = "p_transport_unit_display")]
        public string VehicleDisplay { get; set; }
    }
}