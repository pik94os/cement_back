﻿using System.Linq;
using Cement.Core.Entities;
using Cement.Core.Services.EntityService;

namespace Cement.Protocol.DetailsServices.Impl
{
    public class ClientDetailsService : BaseDetailsService<Client>
    {
        protected override IDataResult GetDetails(IEntityService<Client> entityService, long id)
        {
            var details = entityService.GetAll()
                .Where(x => x.Id == id)
                .Select(x => new ClientDetailsProxy
                {
                    Id = x.Id,
                    Name = x.Name,

                    Okpo = x.Okpo,
                    Kpp = x.Kpp,
                    Inn = x.Inn,

                    JurAddressIndex = "423832",
                    JurAddressCountry = "Россия",
                    JurAddressRegion = "Респ. Татарстан",
                    JurAddressArea = "",
                    JurAddressLocality = "г. Наб.Челны",
                    JurAddressStreet = "пр.Сююмбике",
                    JurAddressHouse = "21",
                    JurAddressHousing = "33",
                    JurAddressBuilding = "А",
                    JurAddressOffice = "100"
                })
                .FirstOrDefault();

            return details;
        }
    }
}