﻿using Castle.Windsor;
using Cement.Core.Services.EntityService;
using DataAccess;

namespace Cement.Protocol.DetailsServices.Impl
{
    public class BaseDetailsService<T> : IDetailsService<T> where T: IEntity
    {
        public IWindsorContainer Container { get; set; }

        public IDataResult GetDetails(long? id)
        {
            if (id == null)
            {
                return null;
            }

            var entityService = Container.Resolve<IEntityService<T>>();

            try
            {
                return GetDetails(entityService, id.Value);
            }
            finally
            {
                Container.Release(entityService);
            }
        }

        public virtual IDataResult GetDetails(T obj)
        {
            return null;
        }

        protected virtual IDataResult GetDetails(IEntityService<T> entityService, long id)
        {
            var obj = entityService.Get(id);

            return GetDetails(obj);
        }
    }
}