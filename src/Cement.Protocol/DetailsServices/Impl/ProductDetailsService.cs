﻿using Cement.Core.Entities;

namespace Cement.Protocol.DetailsServices.Impl
{
    public class ProductDetailsService : BaseDetailsService<Product>
    {
        protected override IDataResult GetDetails(Core.Services.EntityService.IEntityService<Product> entityService, long id)
        {
            return new ProductProxy();
        }
    }
}