﻿using System.Linq;
using Cement.Core.Entities;
using Cement.Core.Enums;
using Cement.Core.Services.EntityService;

namespace Cement.Protocol.DetailsServices.Impl
{
    public class EmployeeDetailsService : BaseDetailsService<Employee>
    {
        public IEntityService<EmployeeDrivingCategory> EmployeeDrivingCategoryEntityService { get; set; }

        protected override IDataResult GetDetails(IEntityService<Employee> entityService, long id)
        {
            var details = entityService.GetAll()
                .Where(x => x.Id == id)
                .Select(x => new EmployeeOutProxy
                {
                    Id = x.Id,
                    Name = x.Name,

                    Surname = x.Surname,
                    FirstName = x.FirstName,
                    Patronymic = x.Patronymic,
                    PositionId = x.Position.Id,
                    PositionDisplay = x.Position.Name,
                    DepartmentId = x.Department.Id,
                    DepartmentDisplay = x.Department.Name,
                    Inn = x.Inn,

                    PassportSerie = x.PassportSerie,
                    PassportNo = x.PassportNo,
                    PassportGivenPlace = x.PassportGivenPlace,
                    PassportDate = x.PassportDate,
                    BirthDate = x.BirthDate,
                    BirthPlace = x.BirthPlace,

                    DriveLicSerie = x.DriveLicSerie,
                    DriveLicNo = x.DriveLicNo,
                    DriveLicGivenPlace = x.DriveLicGivenPlace,
                    DriveLicDate = x.DriveLicDate,
                    DriveLicValidDate = x.DriveLicValidDate,
                    DriveExperienceId = x.DriveExperience.Id,
                    DriveExperienceDisplay = x.DriveExperience.Name,

                    TractorDriveLicSerie = x.TractorDriveLicSerie,
                    TractorDriveLicNo = x.TractorDriveLicNo,
                    TractorDriveLicCode = x.TractorDriveLicCode,
                    TractorDriveLicDate = x.TractorDriveLicDate,
                    TractorDriveLicValidDate = x.TractorDriveLicValidDate,

                    Index = x.Index,
                    Country = x.Country,
                    Region = x.Region,
                    Area = x.Area,
                    Locality = x.Locality,
                    Street = x.Street,
                    House = x.House,
                    Apartment = x.Apartment,

                    Phone1 = x.Phone1,
                    Phone2 = x.Phone2,
                    Phone3 = x.Phone3,
                    Fax = x.Fax,
                    MobilePhone1 = x.MobilePhone1,
                    MobilePhone2 = x.MobilePhone2,
                    MobilePhone3 = x.MobilePhone3,
                    Email1 = x.Email1,
                    Email2 = x.Email2,
                    Email3 = x.Email3
                })
                .FirstOrDefault();

            if (details == null)
            {
                return null;
            }

            var driveCats = EmployeeDrivingCategoryEntityService.GetAll()
                .Where(x => x.Employee.Id == details.Id)
                .ToList();

            details.DriveCategoriesDisplay = string.Join(", ", driveCats.Where(x => x.Category.Type == DriverCategoryType.AutoMoto).Select(x => x.Category.Name));
            details.TractorCategoriesDisplay = string.Join(", ", driveCats.Where(x => x.Category.Type == DriverCategoryType.Tractor).Select(x => x.Category.Name));

            return details;
        }
    }
}