﻿namespace Cement.Protocol.DetailsServices
{
    public interface IDetailsService<T> 
    {
        IDataResult GetDetails(long? id);

        IDataResult GetDetails(T obj);
    }
}