﻿using System.Collections.Generic;
using Newtonsoft.Json;

namespace Cement.Protocol
{
    public class CommonDictionaries
    {
        [JsonProperty(PropertyName = "transport.auxiliary.TransportDrivingCategories")]
        public List<DictionaryObject> TransportDrivingCategories { get; set; }

        [JsonProperty(PropertyName = "corporate.auxiliary.DrivingCategories")]
        public List<DictionaryObject> DrivingCategories { get; set; }

        [JsonProperty(PropertyName = "corporate.auxiliary.TractorCategories")]
        public List<DictionaryObject> TractorDrivingCategories { get; set; }

        [JsonProperty(PropertyName = "corporate.auxiliary.EmployeePositions")]
        public List<DictionaryObject> EmployeePositions { get; set; }

        [JsonProperty(PropertyName = "corporate.auxiliary.DriveFrom")]
        public List<DictionaryObject> DrivingExperience { get; set; }

        [JsonProperty(PropertyName = "transport.auxiliary.RegionCodes")]
        public List<DictionaryObject> TransportRegionCodes { get; set; }

        [JsonProperty(PropertyName = "transport.auxiliary.CountryCodes")]
        public List<DictionaryObject> TransportCountryCodes { get; set; }

        [JsonProperty(PropertyName = "transport.auxiliary.CharClasses")]
        public List<DictionaryObject> TransportCharClasses { get; set; }

        [JsonProperty(PropertyName = "warehouse.inventory.auxiliary.InventoryKinds")]
        public List<DictionaryObject> InventoryKinds { get; set; }

        /// <summary>
        /// Типы кузова
        /// </summary>
        [JsonProperty(PropertyName = "transport.auxiliary.CharTypes")]
        public List<DictionaryObject> TransportCharTypes { get; set; }

        [JsonProperty(PropertyName = "transport.auxiliary.CarOptions")]
        public List<DictionaryObject> TransportCarOptions { get; set; }
        
        [JsonProperty(PropertyName = "transport.auxiliary.TransportCategories")]
        public List<TransportCategory> TransportCategories { get; set; }

        [JsonProperty(PropertyName = "transport.auxiliary.WheelFormulas")]
        public List<DictionaryObject> WheelFormulas { get; set; }

        #region Товары/Услуги

        [JsonProperty(PropertyName = "products.auxiliary.AllUnits")]
        public List<DictionaryObject> ProductAllUnits { get; set; }

        [JsonProperty(PropertyName = "products.auxiliary.Countries")]
        public List<DictionaryObject> ProductCountries { get; set; }

        [JsonProperty(PropertyName = "products.auxiliary.PackageKinds")]
        public List<DictionaryObject> ProductPackageKinds { get; set; }

        [JsonProperty(PropertyName = "products.auxiliary.PackageMaterials")]
        public List<DictionaryObject> ProductPackageMaterials { get; set; }

        [JsonProperty(PropertyName = "products.auxiliary.ProductDangerClasses")]
        public List<DictionaryObject> ProductDangerClasses { get; set; }

        [JsonProperty(PropertyName = "products.auxiliary.ProductImporters")]
        public List<DictionaryObject> ProductImporters { get; set; }

        [JsonProperty(PropertyName = "products.auxiliary.ProductManufacturers")]
        public List<DictionaryObject> ProductManufacturers { get; set; }

        [JsonProperty(PropertyName = "products.auxiliary.ProductPackers")]
        public List<DictionaryObject> ProductPackers { get; set; }

        [JsonProperty(PropertyName = "products.auxiliary.ProductUnCodes")]
        public List<DictionaryObject> ProductUnCodes { get; set; }

        [JsonProperty(PropertyName = "products.auxiliary.ServiceManufacturers")]
        public List<DictionaryObject> ServiceManufacturers { get; set; }

        [JsonProperty(PropertyName = "products.auxiliary.ServiceUnits")]
        public List<DictionaryObject> ServiceUnits { get; set; }

        [JsonProperty(PropertyName = "products.auxiliary.SizeUnits")]
        public List<DictionaryObject> ProductSizeUnits { get; set; }

        [JsonProperty(PropertyName = "products.auxiliary.Taxes")]
        public List<DictionaryObject> ProductTaxes { get; set; }

        [JsonProperty(PropertyName = "products.auxiliary.VolumeUnits")]
        public List<DictionaryObject> ProductVolumeUnits { get; set; }

        [JsonProperty(PropertyName = "products.auxiliary.WeightUnits")]
        public List<DictionaryObject> ProductWeightUnits { get; set; }

        [JsonProperty(PropertyName = "goods.auto.auxiliary.LoadKinds")]
        public List<DictionaryObject> LoadKinds { get; set; }

        [JsonProperty(PropertyName = "goods.auto.auxiliary.TransportUnitType")]
        public List<DictionaryObject> TransportUnitType { get; set; }

        #endregion

        #region Делопроизводство

        [JsonProperty(PropertyName = "office_work.auxiliary.Kinds")]
        public List<DictionaryObject> InnerDocumentKinds { get; set; }

        [JsonProperty(PropertyName = "office_work.auxiliary.Importances")]
        public List<DictionaryObject> InnerDocumentImportances { get; set; }

        [JsonProperty(PropertyName = "office_work.auxiliary.Securities")]
        public List<DictionaryObject> InnerDocumentSecrecies { get; set; }

        [JsonProperty(PropertyName = "goods.auxiliary.Importances")]
        public List<DictionaryObject> GoodsImportances { get; set; }

        [JsonProperty(PropertyName = "goods.auxiliary.Securities")]
        public List<DictionaryObject> GoodsSecrecies { get; set; }

        [JsonProperty(PropertyName = "goods.auxiliary.ImportancesColored")]
        public List<DictionaryFlagObject> GoodsImportancesColored { get; set; }

        [JsonProperty(PropertyName = "goods.auxiliary.SecuritiesColored")]
        public List<DictionaryFlagObject> GoodsSecreciesColored { get; set; }

        #endregion Делопроизводство

        [JsonProperty(PropertyName = "personal.messages.auxiliary.Kinds")]
        public List<DictionaryObject> MessageKinds { get; set; }
        
        [JsonProperty(PropertyName = "clients.Groups")]
        public List<TreeElement> ClientGroups { get; set; }

        [JsonProperty(PropertyName = "clients.auxiliary.EventReminderTypes")]
        public List<DictionaryObject> ClientEventReminderTypes { get; set; }

        [JsonProperty(PropertyName = "products.auxiliary.ServiceGroups")]
        public List<TreeElement> ServiceGroups { get; set; }

        [JsonProperty(PropertyName = "products.auxiliary.ProductGroups")]
        public List<TreeElement> ProductGroups { get; set; }

        [JsonProperty(PropertyName = "accounting.other.auxiliary.MovementbillKinds")]
        public List<DictionaryObject> AccountingOtherMovementBillKinds { get; set; }

        [JsonProperty(PropertyName = "accounting.other.auxiliary.WriteoffActKinds")]
        public List<DictionaryObject> AccountingOtherWriteoffActKinds { get; set; }

        [JsonProperty(PropertyName = "accounting.receiptexpense.auxiliary.SalebillKinds")]
        public List<DictionaryObject> AccountingInOutDocWayBillSaleKinds { get; set; }

        [JsonProperty(PropertyName = "accounting.receiptexpense.auxiliary.SalebillShippingKinds")]
        public List<DictionaryObject> AccountingInOutDocWayBillSaleShippingKinds { get; set; }
    }
}