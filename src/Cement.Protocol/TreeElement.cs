﻿using System.Collections.Generic;
using Newtonsoft.Json;

namespace Cement.Protocol
{
    public class TreeElement
    {
        [JsonProperty(PropertyName = "id")]
        public long Id { get; set; }

        [JsonProperty(PropertyName = "leaf")]
        public bool Leaf {
            get { return Children == null || Children.Count == 0; }
        }

        [JsonProperty(PropertyName = "text")]
        public string Text { get; set; }

        [JsonProperty(PropertyName = "children", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.Ignore)]
        public List<TreeElement> Children { get; set; } 
    }

    public class NamedTreeElement
    {
        [JsonProperty(PropertyName = "id")]
        public object Id { get; set; }

        [JsonIgnore]
        public bool? Leaf { get; set; }

        [JsonProperty(PropertyName = "leaf")]
        public bool leaf
        {
            get { return Leaf ??  Children == null || Children.Count == 0; }
        }

        [JsonProperty(PropertyName = "p_name")]
        public string Name { get; set; }

        [JsonProperty(PropertyName = "expanded")]
        public bool Expanded { get; set; }

        [JsonProperty(PropertyName = "children", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.Ignore)]
        public List<TreeElement> Children { get; set; }
    }

    public class NamedFirmTreeElement : NamedTreeElement
    {
        [JsonProperty(PropertyName = "p_address")]
        public string Address { get; set; }
    }

    public class NamedFirmContractTreeElement : NamedFirmTreeElement
    {
        [JsonProperty(PropertyName = "p_contract_id")]
        public object ContractId { get; set; }

        [JsonProperty(PropertyName = "p_supplier")]
        public string ProviderName { get; set; }
    }
}