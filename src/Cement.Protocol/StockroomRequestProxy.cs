﻿using System;
using Cement.Core.Entities;
using Cement.Core.Entities.Stockroom;
using Newtonsoft.Json;

namespace Cement.Protocol
{
    public class StockroomRequestProxy : StockroomBaseRequest, IDataResult
    {
        [JsonProperty(PropertyName = "p_description")]
        public override string ExtraData { get; set; }

        [JsonProperty(PropertyName = "p_number")]
        public override string No { get; set; }

        [JsonProperty(PropertyName = "p_date")]
        public override DateTime? Date { get; set; }

        [JsonProperty(PropertyName = "p_count")]
        public override decimal Amount { get; set; }

        [JsonProperty(PropertyName = "p_stockroom_product")]
        public override StockroomProduct StockroomProduct { get; set; }

        [JsonProperty(PropertyName = "p_type")]
        public override StockroomRequestType StockroomRequestType { get; set; }

        [JsonProperty(PropertyName = "p_reason")]
        public string Reason { get; set; }

        [JsonProperty(PropertyName = "p_warehouse")]
        public override Warehouse Warehouse { get; set; }

        [JsonProperty(PropertyName = "p_warehouse_to")]
        public Warehouse WarehouseTo { get; set; }

        [JsonProperty(PropertyName = "p_warehouse_contact")]
        public Employee WarehouseContactPerson { get; set; }

        [JsonProperty(PropertyName = "p_pricelist_product")]
        public PriceListProduct PriceListProduct { get; set; }
    }
}