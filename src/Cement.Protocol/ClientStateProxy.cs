﻿using Cement.Core.Entities;
using Newtonsoft.Json;

namespace Cement.Protocol
{
    public class ClientStateProxy : ClientState, IDataResult
    {
        [JsonProperty(PropertyName = "p_name")]
        public override string Name { get; set; }

        [JsonProperty(PropertyName = "p_description")]
        public override string Description { get; set; }
    }
}