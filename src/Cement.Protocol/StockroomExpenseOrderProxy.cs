﻿using System;
using Cement.Core.Entities;
using Cement.Core.Entities.Stockroom;
using Newtonsoft.Json;

namespace Cement.Protocol
{
    public class StockroomExpenseOrderProxy : StockroomExpenseOrderBase, IDataResult
    {
        [JsonProperty(PropertyName = "p_number")]
        public override string No { get; set; }

        [JsonProperty(PropertyName = "p_date")]
        public override DateTime? Date { get; set; }

        [JsonProperty(PropertyName = "p_count_fact")]
        public override decimal Amount { get; set; }

        [JsonProperty(PropertyName = "p_type")]
        public override StockroomExpenseOrderType StockroomExpenseOrderType { get; set; }

        [JsonProperty(PropertyName = "p_productrequest_id")]
        public ProductRequest ProductRequest { get; set; }

        [JsonProperty(PropertyName = "p_stockmovementrequest_id")]
        public StockroomMovementRequest StockroomMovementRequest { get; set; }

        [JsonProperty(PropertyName = "p_stockwriteoffrequest_id")]
        public StockroomWriteoffRequest StockroomWriteoffRequest { get; set; }

        [JsonProperty(PropertyName = "p_warehouse")]
        public override Warehouse Warehouse { get; set; }
    }
}