﻿using System;
using Cement.Core.Entities;
using Cement.Core.Entities.Stockroom;
using Newtonsoft.Json;

namespace Cement.Protocol
{
    public class StockroomReceiptOrderProxy : StockroomReceiptOrderBase, IDataResult
    {
        [JsonProperty(PropertyName = "p_number")]
        public override string No { get; set; }

        [JsonProperty(PropertyName = "p_date")]
        public override DateTime? Date { get; set; }

        [JsonProperty(PropertyName = "p_type")]
        public override StockroomReceiptOrderType StockroomReceiptOrderType { get; set; }

        [JsonProperty(PropertyName = "p_order_item_outer")]
        public StockroomReceiptOrderItemOuter StockroomReceiptOrderItemOuter { get; set; }

        [JsonProperty(PropertyName = "p_order_item_move")]
        public StockroomReceiptOrderItemInnerMove StockroomReceiptOrderItemMove { get; set; }

        [JsonProperty(PropertyName = "p_order_item_posting")]
        public StockroomReceiptOrderItemInnerPosting StockroomReceiptOrderItemPosting { get; set; }

        [JsonProperty(PropertyName = "p_warehouse")]
        public override Warehouse Warehouse { get; set; }
    }
}