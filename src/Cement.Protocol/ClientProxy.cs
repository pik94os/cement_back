﻿using System.Collections.Generic;
using Cement.Core;
using Newtonsoft.Json;

namespace Cement.Protocol
{
    public class ClientDetailsProxy : IDataResult
    {
        [JsonProperty(PropertyName = "id")]
        public long Id { get; set; }

        [JsonProperty(PropertyName = "p_name")]
        public string Name { get; set; }

        [JsonProperty(PropertyName = "p_inn")]
        public string Inn { get; set; }
        
        [JsonProperty(PropertyName = "p_kpp")]
        public string Kpp { get; set; }

        [JsonProperty(PropertyName = "p_okpo")]
        public string Okpo { get; set; }
        
        [JsonProperty(PropertyName = "p_law_index")]
        public string JurAddressIndex { get; set; }

        [JsonProperty(PropertyName = "p_law_country")]
        public string JurAddressCountry { get; set; }

        [JsonProperty(PropertyName = "p_law_region")]
        public string JurAddressRegion { get; set; }

        [JsonProperty(PropertyName = "p_law_district")]
        public string JurAddressArea { get; set; }

        [JsonProperty(PropertyName = "p_law_city")]
        public string JurAddressLocality { get; set; }

        [JsonProperty(PropertyName = "p_law_street")]
        public string JurAddressStreet { get; set; }

        [JsonProperty(PropertyName = "p_law_house")]
        public string JurAddressHouse { get; set; }

        [JsonProperty(PropertyName = "p_law_building")]
        public string JurAddressBuilding { get; set; }

        [JsonProperty(PropertyName = "p_law_housepart")]
        public string JurAddressHousing { get; set; }

        [JsonProperty(PropertyName = "p_law_office")]
        public string JurAddressOffice { get; set; }
    }

    public class ClientTreeElement : TreeElement
    {
        [JsonProperty(PropertyName = "address")]
        public string Address { get; set; }
    }

    public class ClientExtendedTreeElement
    {
        [JsonProperty(PropertyName = "id")]
        public long Id { get; set; }

        [JsonProperty(PropertyName = "leaf")]
        public bool Leaf
        {
            get { return Children == null || Children.Count == 0; }
        }

        [JsonProperty(PropertyName = "p_level")]
        public TreeLevel Level { get; set; }

        [JsonProperty(PropertyName = "p_name")]
        public string Name { get; set; }

        [JsonProperty(PropertyName = "children", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.Ignore)]
        public List<TreeElement> Children { get; set; } 


        [JsonProperty(PropertyName = "p_address")]
        public string Address { get; set; }

        [JsonProperty(PropertyName = "p_inn")]
        public string Inn { get; set; }

        [JsonProperty(PropertyName = "p_kpp")]
        public string Kpp { get; set; }
    }

    public class OrganizationClientExtendedTreeElement : ClientExtendedTreeElement
    {
        [JsonProperty(PropertyName = "p_client")]
        public long ClientId { get; set; }
    }
}