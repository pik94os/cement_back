﻿using System;
using System.Collections.Generic;
using Cement.Core.Entities;
using Cement.Core.Enums;
using Newtonsoft.Json;

namespace Cement.Protocol
{
    public class PersonalClientEventProxy : PersonalClientEvent, IDataResult
    {
        [JsonProperty(PropertyName = "p_personal_client")]
        public override PersonalClient PersonalClient { get; set; }

        [JsonProperty(PropertyName = "p_date")]
        public override DateTime? EventDateTime { get; set; }

        [JsonProperty(PropertyName = "p_time")]
        public override string TimeString { get; set; }

        [JsonProperty(PropertyName = "p_theme")]
        public override string Theme { get; set; }

        [JsonProperty(PropertyName = "p_description")]
        public override string Description { get; set; }

        [JsonProperty(PropertyName = "p_reminder")]
        public override PersonalClientEventReminderType ReminderType { get; set; }

        [JsonProperty(PropertyName = "p_documents")]
        public override List<string> RelatedDocumentIds { get; set; }
    }
}