﻿using System;
using Cement.Core.Entities;
using Newtonsoft.Json;

namespace Cement.Protocol
{
    public class AccountingInOutDocProxy : AccountingInOutDocBase, IDataResult
    {
        [JsonProperty(PropertyName = "p_number")]
        public override string No { get; set; }

        [JsonProperty(PropertyName = "p_date")]
        public override DateTime? Date { get; set; }
        
        [JsonProperty(PropertyName = "p_type")]
        public override AccountingInOutDocType AccountingInOutDocType { get; set; }

        [JsonProperty(PropertyName = "p_kind")]
        public AccountingInOutDocWayBillSaleKind AccountingInOutDocWayBillSaleKind { get; set; }

        [JsonProperty(PropertyName = "p_shipping_kind")]
        public AccountingInOutDocWayBillSaleShippingKind AccountingInOutDocWayBillSaleShippingKind { get; set; }
    }
}