﻿using System;
using Cement.Core.Entities;
using Newtonsoft.Json;

namespace Cement.Protocol
{
    public class VehicleAccidentProxy : VehicleAccident, IDataResult
    {
        [JsonProperty(PropertyName = "p_transport_unit")]
        public override Vehicle Vehicle { get; set; }

        [JsonProperty(PropertyName = "p_date")]
        public override DateTime? Date { get; set; }

        [JsonProperty(PropertyName = "p_count")]
        public override int? Count { get; set; }

        [JsonProperty(PropertyName = "p_damage")]
        public override decimal? Damage { get; set; }

        [JsonProperty(PropertyName = "p_comment")]
        public override string Description { get; set; }
        
        [JsonProperty(PropertyName = "p_sum")]
        public override decimal? Cost { get; set; }

        [JsonProperty(PropertyName = "p_driver")]
        public override Employee Driver { get; set; }
    }
}