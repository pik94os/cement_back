﻿using System.Collections.Generic;
using Cement.Core;
using Newtonsoft.Json;

namespace Cement.Protocol
{
    public class RequestClientProxy : IDataResult
    {
        [JsonProperty(PropertyName = "id")]
        public long Id { get; set; }

        [JsonProperty(PropertyName = "p_name")]
        public string Name { get; set; }

        [JsonProperty(PropertyName = "p_address")]
        public string Address { get; set; }

        [JsonProperty(PropertyName = "p_contact")]
        public long EmployeeId { get; set; }

        [JsonProperty(PropertyName = "p_contact_display")]
        public string EmployeeName { get; set; }
    }
}