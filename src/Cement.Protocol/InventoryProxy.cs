﻿using System;
using Cement.Core.Entities;
using Cement.Core.Entities.Stockroom;
using Newtonsoft.Json;

namespace Cement.Protocol
{
    public class InventoryProxy : Inventory, IDataResult
    {
        [JsonProperty(PropertyName = "p_warehouse")]
        public override Warehouse Warehouse { get; set; }

        [JsonProperty(PropertyName = "p_date")]
        public override DateTime? Date { get; set; }

        [JsonProperty(PropertyName = "p_number")]
        public override string No { get; set; }
        
        [JsonProperty(PropertyName = "p_kind")]
        public override InventoryKind Kind { get; set; }

        [JsonProperty(PropertyName = "p_director")]
        public override Employee Director { get; set; }

        [JsonProperty(PropertyName = "p_chairman")]
        public override Employee Chairman { get; set; }
    }
}