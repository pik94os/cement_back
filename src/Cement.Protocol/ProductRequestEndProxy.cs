﻿using System;
using System.Collections.Generic;
using Cement.Core.Entities;
using Cement.Core.Enums;
using Newtonsoft.Json;

namespace Cement.Protocol
{
    public class ProductRequestEndProxy : IDataResult
    {
        public long Id { get; set; }

        public DateTime ShipmentDate { get; set; }

        public DateTime ShipmentTime { get; set; }
    }

}