﻿using System;
using System.Collections.Generic;
using Cement.Core.Entities;
using Cement.Core.Enums;
using Newtonsoft.Json;

namespace Cement.Protocol
{
    public class CatalogueProxy : Catalogue, IDataResult
    {
        [JsonProperty(PropertyName = "id")]
        public override long Id { get; set; }

        [JsonProperty(PropertyName = "p_price")]
        public override PriceList PriceList { get; set; }

        [JsonProperty(PropertyName = "p_comment")]
        public override string Description { get; set; }
    }
}