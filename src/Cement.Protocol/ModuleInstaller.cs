﻿using Castle.MicroKernel.Registration;
using Castle.MicroKernel.SubSystems.Configuration;
using Castle.Windsor;
using Cement.Core.Entities;
using Cement.Protocol.DetailsServices;
using Cement.Protocol.DetailsServices.Impl;

namespace Cement.Protocol
{
    public class ModuleInstaller : IWindsorInstaller
    {
        public void Install(IWindsorContainer container, IConfigurationStore store)
        {
            container.Register(Component.For(typeof(IDetailsService<>)).ImplementedBy(typeof(BaseDetailsService<>)).LifestyleTransient());

            container.Register(Component.For<IDetailsService<Employee>>().ImplementedBy<EmployeeDetailsService>().LifestyleTransient());
            container.Register(Component.For<IDetailsService<Client>>().ImplementedBy<ClientDetailsService>().LifestyleTransient());
            container.Register(Component.For<IDetailsService<Product>>().ImplementedBy<ProductDetailsService>().LifestyleTransient());
        }
    }
}