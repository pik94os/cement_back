﻿using System;
using System.Collections.Generic;
using Cement.Core.Entities;
using Cement.Core.Enums;
using Newtonsoft.Json;

namespace Cement.Protocol
{
    public class WarehouseProxy : Warehouse, IDataResult
    {
        [JsonProperty(PropertyName = "id")]
        public override long Id { get; set; }

        [JsonProperty(PropertyName = "p_kind")]
        public override WarehouseType Type { get; set; }

        [JsonProperty(PropertyName = "p_name")]
        public override string Name { get; set; }

        [JsonProperty(PropertyName = "p_railway_is_own")]
        public override bool IsOwnDriveways { get; set; }

        [JsonProperty(PropertyName = "p_railway_owner")]
        public override Client DrivewaysOwner { get; set; }

        [JsonProperty(PropertyName = "p_index")]
        public override string AddressIndex { get; set; }

        [JsonProperty(PropertyName = "p_country")]
        public override string AddressCountry { get; set; }

        [JsonProperty(PropertyName = "p_region")]
        public override string AddressRegion { get; set; }

        [JsonProperty(PropertyName = "p_district")]
        public override string AddressArea { get; set; }

        [JsonProperty(PropertyName = "p_city")]
        public override string AddressLocality { get; set; }

        [JsonProperty(PropertyName = "p_street")]
        public override string AddressStreet { get; set; }

        [JsonProperty(PropertyName = "p_house")]
        public override string AddressHouse { get; set; }

        [JsonProperty(PropertyName = "p_housepart")]
        public override string AddressHousing { get; set; }

        [JsonProperty(PropertyName = "p_building")]
        public override string AddressBuilding { get; set; }

        [JsonProperty(PropertyName = "p_office")]
        public override string AddressOffice { get; set; }

        [JsonProperty(PropertyName = "p_address_comment")]
        public override string AddressComment { get; set; }

        [JsonProperty(PropertyName = "p_firm_code")]
        public override string CompanyCode { get; set; }

        [JsonProperty(PropertyName = "p_longitude")]
        public override decimal Longitude { get; set; }

        [JsonProperty(PropertyName = "p_latitude")]
        public override decimal Latitude { get; set; }

        [JsonProperty(PropertyName = "p_station")]
        public override RailwayStation RailwayStation { get; set; }
    }

    public class WarehouseTreeElement : IDataResult
    {
        [JsonProperty(PropertyName = "id")]
        public long Id { get; set; }

        [JsonProperty(PropertyName = "leaf")]
        public bool Leaf { get; set; }

        [JsonProperty(PropertyName = "p_name")]
        public virtual string Name { get; set; }

        [JsonProperty(PropertyName = "p_address")]
        public virtual string Address { get; set; }
    }
}