﻿using System;
using Newtonsoft.Json;

namespace Cement.Protocol.JsonConverters
{
    public class ObjectResultConverter : JsonConverter
    {
        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            if (value is ObjectResult)
            {
                //var data = (value as ObjectResult).Data;
                //serializer.Serialize(writer, data ?? new object());
                serializer.Serialize(writer, (value as ObjectResult).Data);
            }
        }

        public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
        {
            throw new NotImplementedException();
        }

        public override bool CanConvert(Type objectType)
        {
            return objectType == typeof(ObjectResult);
        }

        public override bool CanRead
        {
            get { return false; }
        }

        public override bool CanWrite
        {
            get { return true; }
        }
    }
}