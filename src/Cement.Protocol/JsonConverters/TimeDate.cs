﻿using System;
using Newtonsoft.Json;

namespace Cement.Protocol.JsonConverters
{

    [JsonConverter(typeof(TimeDateConverter))]
    public class TimeDate
    {
        private DateTime? _dateTime;

        public TimeDate(DateTime? value) //constructor
        {
            _dateTime = value;
        }

        public static implicit operator DateTime?(TimeDate timeDate)
        {
            return timeDate._dateTime;
        }

        public DateTime? DateTime { get { return _dateTime; } }
    }
}