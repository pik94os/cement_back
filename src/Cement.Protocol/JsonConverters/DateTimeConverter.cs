﻿using System;
using Newtonsoft.Json;

namespace Cement.Protocol.JsonConverters
{
    public class DateTimeConverter : JsonConverter
    {
        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            if (value is DateTime?)
            {
                var dateTime = value as DateTime?;

                serializer.Serialize(writer, dateTime.HasValue ? dateTime.Value.ToString("dd.MM.yy") : string.Empty);
            }
            else if (value is DateTime)
            {
                serializer.Serialize(writer,((DateTime)value).ToString("dd.MM.yy"));
            }
            else
            {
                serializer.Serialize(writer, value);
            }
        }

        public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
        {
            throw new NotImplementedException();
        }

        public override bool CanConvert(Type objectType)
        {
            return objectType == typeof(DateTime) || objectType == typeof(DateTime?);
        }

        public override bool CanRead
        {
            get { return false; }
        }

        public override bool CanWrite
        {
            get { return true; }
        }
    }
}