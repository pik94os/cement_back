﻿using System;
using Newtonsoft.Json;

namespace Cement.Protocol.JsonConverters
{
    public class TimeDateConverter : JsonConverter
    {
        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            if (value is TimeDate)
            {
                var dateTime = (value as TimeDate).DateTime;

                serializer.Serialize(writer, dateTime.HasValue ? dateTime.Value.ToString("dd.MM.yyyy HH:mm:ss") : string.Empty);
            }
            else
            {
                serializer.Serialize(writer, value);
            }
        }

        public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
        {
            throw new NotImplementedException();
        }

        public override bool CanConvert(Type objectType)
        {
            return objectType == typeof(TimeDate);
        }

        public override bool CanRead
        {
            get { return false; }
        }

        public override bool CanWrite
        {
            get { return true; }
        }
    }
}