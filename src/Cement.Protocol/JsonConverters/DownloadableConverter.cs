﻿using System;
using Newtonsoft.Json;

namespace Cement.Protocol.JsonConverters
{
    public class DownloadableConverter : JsonConverter
    {
        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            if (value is long?)
            {
                serializer.Serialize(writer, GetLink(value as long?));
            }
            else if(value is Downloadable)
            {
                serializer.Serialize(writer, GetLink((value as Downloadable).Id));
            }
            else
            {
                serializer.Serialize(writer, null);
            }
        }

        private string GetLink(long? id)
        {
            return id > 0 ? "/file/download/" + id.Value : null;
        }

        public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
        {
            throw new NotImplementedException();
        }

        public override bool CanConvert(Type objectType)
        {
            return objectType == typeof(long) || objectType == typeof(long?);
        }

        public override bool CanRead
        {
            get { return false; }
        }

        public override bool CanWrite
        {
            get { return true; }
        }
    }
}