﻿using Newtonsoft.Json;

namespace Cement.Protocol.JsonConverters
{
    [JsonConverter(typeof(DownloadableConverter))]
    public class Downloadable
    {
        private long? id;

        public Downloadable(long? value) //constructor
        {
            id = value;
        }
        
        public static implicit operator long?(Downloadable downloadable)
        {
            return downloadable.id;
        }

        public long? Id { get { return id; } }
    }
}
