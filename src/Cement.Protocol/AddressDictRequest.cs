﻿namespace Cement.Protocol
{
    public class AddressDictRequest
    {
        public string Query { get; set; }
    }
}