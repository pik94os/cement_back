﻿using Cement.Core.Entities;
using Newtonsoft.Json;

namespace Cement.Protocol
{
    public class PersonalClientProxy : PersonalClient, IDataResult
    {
        [JsonProperty(PropertyName = "p_group")]
        public override ClientGroup Group { get; set; }

        [JsonProperty(PropertyName = "p_sub_group")]
        public override ClientGroup Subgroup { get; set; }

        [JsonProperty(PropertyName = "p_status")]
        public override ClientState State { get; set; }

        [JsonProperty(PropertyName = "p_from")]
        public override string From { get; set; }

        [JsonProperty(PropertyName = "p_comment")]
        public override string Comment { get; set; }

        [JsonProperty(PropertyName = "p_client")]
        public override Client Client { get; set; }
    }
}