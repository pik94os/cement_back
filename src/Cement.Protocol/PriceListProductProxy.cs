﻿using Newtonsoft.Json;

namespace Cement.Protocol
{
    public class PriceListProductProxy :  IDataResult
    {
        [JsonProperty(PropertyName = "id")]
        public long Id { get; set; }

        [JsonProperty(PropertyName = "p_name")]
        public string Name { get; set; }

        [JsonProperty(PropertyName = "p_product_unit_display")]
        public string MeasureUnit { get; set; }

        [JsonProperty(PropertyName = "p_product_unit")]
        public long? MeasureUnitId { get; set; }

        [JsonProperty(PropertyName = "p_price")]
        public decimal Price { get; set; }

        [JsonProperty(PropertyName = "p_tax")]
        public decimal? Tax { get; set; }

        [JsonProperty(PropertyName = "p_tax_id")]
        public long? TaxId { get; set; }

        [JsonProperty(PropertyName = "p_pricelist_product")]
        public long? PriceListProduct { get; set; }

        public long p_contract_id { get; set; }

        public long p_pricelist_id { get; set; }

    }

    public class PriceListProductTreeElement
    {
        [JsonProperty(PropertyName = "id")]
        public long Id { get; set; }

        [JsonProperty(PropertyName = "leaf")]
        public bool Leaf { get; set; }

        [JsonProperty(PropertyName = "p_name")]
        public string Name { get; set; }
    }
}