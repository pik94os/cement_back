﻿using System;
using System.Collections.Generic;
using Cement.Core.Entities;
using Cement.Core.Entities.Dict;
using Newtonsoft.Json;

namespace Cement.Protocol
{
    public class EmployeeProxy : Employee, IDataResult
    {
        #region Общие сведения
        [JsonProperty(PropertyName = "p_fio")]
        public override string Name { get; set; }

        [JsonProperty(PropertyName = "p_surname")]
        public override string Surname { get; set; }

        [JsonProperty(PropertyName = "p_name")]
        public override string FirstName { get; set; }

        [JsonProperty(PropertyName = "p_middlename")]
        public override string Patronymic { get; set; }

        [JsonProperty(PropertyName = "p_position")]
        public override Position Position { get; set; }

        [JsonProperty(PropertyName = "p_department")]
        public override Department Department { get; set; }

        [JsonProperty(PropertyName = "p_inn")]
        public override string Inn { get; set; }

        #endregion Общие сведения

        #region Паспорт
        [JsonProperty(PropertyName = "p_passport_serie")]
        public override string PassportSerie { get; set; }

        [JsonProperty(PropertyName = "p_passport_number")]
        public override string PassportNo { get; set; }

        [JsonProperty(PropertyName = "p_passport_get")]
        public override string PassportGivenPlace { get; set; }

        [JsonProperty(PropertyName = "p_passport_date")]
        public override DateTime? PassportDate { get; set; }

        [JsonProperty(PropertyName = "p_passport_birth")]
        public override DateTime? BirthDate { get; set; }

        [JsonProperty(PropertyName = "p_passport_place")]
        public override string BirthPlace { get; set; }

        #endregion Паспорт

        #region Водительское удостоверение

        [JsonProperty(PropertyName = "p_drive_serie")]
        public override string DriveLicSerie { get; set; }

        [JsonProperty(PropertyName = "p_drive_number")]
        public override string DriveLicNo { get; set; }

        [JsonProperty(PropertyName = "p_drive_get")]
        public override string DriveLicGivenPlace { get; set; }

        [JsonProperty(PropertyName = "p_drive_category")]
        public override List<long> DriveCategories { get; set; }

        [JsonProperty(PropertyName = "p_drive_date")]
        public override DateTime? DriveLicDate { get; set; }

        [JsonProperty(PropertyName = "p_drive_date_end")]
        public override DateTime? DriveLicValidDate { get; set; }

        [JsonProperty(PropertyName = "p_drive_age")]
        public override Experience DriveExperience { get; set; }

        #endregion Водительское удостоверение

        #region Удостоверение тракториста

        [JsonProperty(PropertyName = "p_tractor_serie")]
        public override string TractorDriveLicSerie { get; set; }

        [JsonProperty(PropertyName = "p_tractor_number")]
        public override string TractorDriveLicNo { get; set; }

        [JsonProperty(PropertyName = "p_tractor_code")]
        public override string TractorDriveLicCode { get; set; }

        [JsonProperty(PropertyName = "p_tractor_category")]
        public override List<long> TractorCategories { get; set; }

        [JsonProperty(PropertyName = "p_tractor_date")]
        public override DateTime? TractorDriveLicDate { get; set; }

        [JsonProperty(PropertyName = "p_tractor_date_end")]
        public override DateTime? TractorDriveLicValidDate { get; set; }

        #endregion Удостоверение тракториста

        #region Прописка

        [JsonProperty(PropertyName = "p_index")]
        public override string Index { get; set; }

        [JsonProperty(PropertyName = "p_country")]
        public override string Country { get; set; }

        [JsonProperty(PropertyName = "p_region")]
        public override string Region { get; set; }

        [JsonProperty(PropertyName = "p_district")]
        public override string Area { get; set; }

        [JsonProperty(PropertyName = "p_city")]
        public override string Locality { get; set; }

        [JsonProperty(PropertyName = "p_street")]
        public override string Street { get; set; }

        [JsonProperty(PropertyName = "p_house")]
        public override string House { get; set; }

        [JsonProperty(PropertyName = "p_flat")]
        public override string Apartment { get; set; }

        #endregion Прописка

        #region Связь

        [JsonProperty(PropertyName = "p_connect_phone1")]
        public override string Phone1 { get; set; }

        [JsonProperty(PropertyName = "p_connect_phone2")]
        public override string Phone2 { get; set; }

        [JsonProperty(PropertyName = "p_connect_phone3")]
        public override string Phone3 { get; set; }

        [JsonProperty(PropertyName = "p_connect_fax")]
        public override string Fax { get; set; }

        [JsonProperty(PropertyName = "p_connect_mobile1")]
        public override string MobilePhone1 { get; set; }

        [JsonProperty(PropertyName = "p_connect_mobile2")]
        public override string MobilePhone2 { get; set; }

        [JsonProperty(PropertyName = "p_connect_mobile3")]
        public override string MobilePhone3 { get; set; }

        [JsonProperty(PropertyName = "p_connect_email1")]
        public override string Email1 { get; set; }

        [JsonProperty(PropertyName = "p_connect_email2")]
        public override string Email2 { get; set; }

        [JsonProperty(PropertyName = "p_connect_email3")]
        public override string Email3 { get; set; }

        #endregion Связь
    }

    public class EmployeeOutProxy : EmployeeProxy
    {
        [JsonIgnore]
        public override Position Position { get; set; }

        [JsonProperty(PropertyName = "p_position")]
        public long? PositionId { get; set; }

        [JsonProperty(PropertyName = "p_position_display")]
        public string PositionDisplay { get; set; }

        [JsonIgnore]
        public override Department Department { get; set; }

        [JsonProperty(PropertyName = "p_department")]
        public long? DepartmentId { get; set; }

        [JsonProperty(PropertyName = "p_department_display")]
        public string DepartmentDisplay { get; set; }

        [JsonIgnore]
        public override Experience DriveExperience { get; set; }

        [JsonProperty(PropertyName = "p_drive_age")]
        public long? DriveExperienceId { get; set; }

        [JsonProperty(PropertyName = "p_drive_age_display")]
        public string DriveExperienceDisplay { get; set; }

        [JsonProperty(PropertyName = "p_drive_category_display")]
        public string DriveCategoriesDisplay { get; set; }

        [JsonProperty(PropertyName = "p_tractor_category_display")]
        public string TractorCategoriesDisplay { get; set; }
    }

    public class TreeElemendDriver : IDataResult
    {
        [JsonProperty(PropertyName = "id")]
        public long Id { get; set; }

        [JsonProperty(PropertyName = "p_name")]
        public string Name { get; set; }
    
        [JsonProperty(PropertyName = "leaf")]
        public bool Leaf { get; set; }

        [JsonProperty(PropertyName = "p_department")]
        public string DepartmentName { get; set; }

        [JsonProperty(PropertyName = "p_position")]
        public string PositionName { get; set; }
    }

    public class BaseEmployeeTreeElement : IDataResult
    {
        [JsonProperty(PropertyName = "id")]
        public long Id { get; set; }

        [JsonProperty(PropertyName = "leaf")]
        public bool Leaf { get; set; }

        public virtual string Name { get; set; }

        public virtual string Inn { get; set; }

        public virtual string PositionName { get; set; }

        public virtual string DepartmentName { get; set; }

    }

    public class EmployeeTreeElement : BaseEmployeeTreeElement
    {
        [JsonProperty(PropertyName = "text")]
        public override string Name { get; set; }

        [JsonProperty(PropertyName = "inn")]
        public override string Inn { get; set; }

        [JsonProperty(PropertyName = "position")]
        public override string PositionName { get; set; }
    }

    public class ExternalEmployeeTreeElement : EmployeeTreeElement
    {
        [JsonProperty(PropertyName = "text")]
        public override string Name { get; set; }

        [JsonProperty(PropertyName = "inn")]
        public override string Inn { get; set; }

        [JsonProperty(PropertyName = "data")]
        public List<ExternalEmployeeTreeElement> Children { get; set; }
    }

    public class EmployeeNamedTreeElement : BaseEmployeeTreeElement
    {
        [JsonProperty(PropertyName = "p_name")]
        public override string Name { get; set; }

        [JsonProperty(PropertyName = "inn")]
        public override string Inn { get; set; }

        [JsonProperty(PropertyName = "p_position")]
        public override string PositionName { get; set; }

        [JsonProperty(PropertyName = "p_department")]
        public override string DepartmentName { get; set; }
    }
}