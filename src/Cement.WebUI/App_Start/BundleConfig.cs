﻿using System.Web;
using System.Web.Optimization;
using SenchaMinify;

namespace Cement.WebUI
{
    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            var ctx = new HttpContextWrapper(HttpContext.Current);
            var context = new BundleContext(ctx, bundles, string.Empty);

            var scriptBundle = new ScriptBundle("~/js")
                .Include("~/Content/lib/OpenLayers.js")
                .Include("~/scripts/Config.js")
                .Include("~/scripts/extjs/ext-all-dev.js")
                .Include("~/scripts/extjs/ext-lang-ru.js");

            bundles.Add(new StaticBundle("~/js", scriptBundle.GenerateBundleResponse(context)));

#if DEBUG
            var senchaBundle = new ScriptBundle("~/app")
                .Include("~/scripts/app/Start.js")
                .Include("~/scripts/app/Application.js");
#else
            var senchaBundle = new SenchaBundle("~/app")
                .Include("~/scripts/app/Start.js")
                .IncludeDirectory("~/scripts/app/model", "*.js", true)
                .IncludeDirectory("~/scripts/app/store", "*.js", true)
                .IncludeDirectory("~/scripts/app/view", "*.js", true)
                .IncludeDirectory("~/scripts/app/controller", "*.js", true)
                .Include("~/scripts/app/Application.js");
#endif

            bundles.Add(new StaticBundle("~/app", senchaBundle.GenerateBundleResponse(context)));
            
            bundles.Add(new StyleBundle("~/css").Include("~/Content/Cement-all.css", new CssRewriteUrlTransform()));

            // Set EnableOptimizations to false for debugging. For more information,
            // visit http://go.microsoft.com/fwlink/?LinkId=301862

#if DEBUG
            BundleTable.EnableOptimizations = false;
#else
            BundleTable.EnableOptimizations = true;
#endif
        }
    }

    internal class StaticBundle : Bundle
    {
        private readonly BundleResponse _response;

        public StaticBundle(string virtualPath, BundleResponse response)
            : base(virtualPath)
        {
            _response = response;
        }

        public override BundleResponse CacheLookup(BundleContext context)
        {
            return _response;
        }

        public override void UpdateCache(BundleContext context, BundleResponse response)
        {
            
        }
    }
}