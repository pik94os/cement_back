﻿using System.Net.Http.Formatting;
using System.Web.Http;
using System.Web.Http.Filters;
using Cement.Protocol.JsonConverters;
using Cement.WebUI.Framework;
using Cement.WebUI.Framework.ActionFilters.Http;

namespace Cement.WebUI
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            config.EnableCors();
            // Web API configuration and services

            // Web API routes
            config.MapHttpAttributeRoutes(new CustomDirectRouteProvider());

            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );
        }

        public static void SetFilters(HttpFilterCollection filters)
        {
            filters.Add(new UnhandledExceptionFilter());
            filters.Add(new CementExceptionHandler());
        }

        public static void SetFormatters(MediaTypeFormatterCollection formatters)
        {
            //formatters.JsonFormatter.SerializerSettings.ContractResolver = new CamelCasePropertyNamesContractResolver();
            //formatters.JsonFormatter.SerializerSettings.Converters.Add(new StringEnumConverter { CamelCaseText = true });
            ////formatters.JsonFormatter.SerializerSettings.Converters.Add(new EntityListConverter());
            //formatters.JsonFormatter.SerializerSettings.NullValueHandling = NullValueHandling.Include;
            formatters.JsonFormatter.SerializerSettings.Converters.Add(new DateTimeConverter());
        }
    }
}
