﻿using System;
using System.Configuration;
using System.Net;
using System.Net.Http;
using System.Web.Configuration;
using System.Web.Http;
using System.Web.Http.Filters;
using Cement.Core.Exceptions;

namespace Cement.WebUI.Framework.ActionFilters.Http
{
    public static class Compilation
    {
        public static readonly CompilationSection Current = (CompilationSection)ConfigurationManager.GetSection("system.web/compilation");
    }

    public class CementError
    {
        public string Message { get; set; }

        public string Code { get; set; }

        public string StackTrace { get; set; }
    }

    public class CementExceptionHandler : ExceptionFilterAttribute
    {
        public override void OnException(HttpActionExecutedContext actionExecutedContext)
        {
            if (ExceptionIs<HttpResponseException>(actionExecutedContext, exception => CreateResponseFromHttpResponseException(actionExecutedContext, exception)) ||
                ExceptionIs<SystemException>(actionExecutedContext, exception => CreateResponseFromSystemException(actionExecutedContext, exception)) ||
                ExceptionIs<ValidationException>(actionExecutedContext, exception => CreateResponseFromRequestValidationException(actionExecutedContext, exception)))
            {
                return;
            }

            actionExecutedContext.Response = CreateResponseFromGeneralException(actionExecutedContext, actionExecutedContext.Exception);
        }

        private bool ExceptionIs<TException>(HttpActionExecutedContext actionExecutedContext, Func<TException, HttpResponseMessage> getResponse) where TException : Exception
        {
            var exception = actionExecutedContext.Exception as TException;
            if (exception != null)
            {
                actionExecutedContext.Response = getResponse(exception);
                return true;
            }
            return false;
        }

        private HttpResponseMessage CreateResponseFromHttpResponseException(HttpActionExecutedContext actionExecutedContext, HttpResponseException exception)
        {
            return actionExecutedContext.Request.CreateResponse(exception.Response.StatusCode, new CementError
            {
                Message = exception.Response.ReasonPhrase,
                Code = "General",
                StackTrace = Compilation.Current.Debug ? exception.StackTrace : null
            });
        }

        private HttpResponseMessage CreateResponseFromGeneralException(HttpActionExecutedContext actionExecutedContext, Exception exception)
        {
            return actionExecutedContext.Request.CreateResponse(HttpStatusCode.InternalServerError, new CementError
            {
                Message = exception.Message,
                Code = exception.GetType().Name,
                StackTrace = Compilation.Current.Debug ? exception.StackTrace : null
            });
        }

        private HttpResponseMessage CreateResponseFromSystemException(HttpActionExecutedContext actionExecutedContext, SystemException systemException)
        {
            return actionExecutedContext.Request.CreateResponse(HttpStatusCode.InternalServerError, new CementError
            {
                Message = systemException.Message,
                Code = GetTypeName(systemException.GetType()),
                StackTrace = Compilation.Current.Debug ? systemException.StackTrace : null
            });
        }

        private HttpResponseMessage CreateResponseFromRequestValidationException(HttpActionExecutedContext actionExecutedContext, ValidationException validationException)
        {
            return actionExecutedContext.Request.CreateResponse(HttpStatusCode.InternalServerError, new CementError
            {
                Message = validationException.Message,
                Code = GetTypeName(validationException.GetType()),
                StackTrace = Compilation.Current.Debug ? validationException.StackTrace : null
            });
        }

        private static string GetTypeName(Type type)
        {
            string typeName = type.Name;
            if (type.IsGenericType)
            {
                typeName = typeName.Remove(typeName.IndexOf('`'));
            }
            return typeName;
        }
    }
}