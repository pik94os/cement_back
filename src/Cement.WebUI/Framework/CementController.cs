﻿using System;
using System.Security.Principal;
using System.Web.Mvc;
using Castle.Windsor;
using Cement.Core.Auth;

namespace Cement.WebUI.Framework
{
    public class CementController : Controller
    {
        public IWindsorContainer Container { get; set; }

        protected IUserPrincipal UserPrincipal { get; set; }

        protected override void Initialize(System.Web.Routing.RequestContext requestContext)
        {
            base.Initialize(requestContext);

            if (Container != null)
            {
                try
                {
                    var authenticationState = Container.Resolve<IAuthenticationState>(new { principal = User });
                    UserPrincipal = authenticationState.CurrentUser;
                }
                catch 
                {
                    // Для тех случаев, когда аутентифицированный пользователь уже заблокирован/удален
                    Container.Resolve<IAuthenticator>().SignOut();
                }
            }
        }
         
    }
}