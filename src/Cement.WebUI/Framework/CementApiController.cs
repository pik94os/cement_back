﻿using System.Web.Http;
using System.Web.Http.Controllers;
using System.Web.Http.Cors;
using Castle.Windsor;
using Cement.Core.Auth;

namespace Cement.WebUI.Framework
{
    //[EnableCors("*", "*", "*")]
    [Authorize]
    public class CementApiController : ApiController
    {
        public IWindsorContainer Container { get; set; }
        
        protected IUserPrincipal UserPrincipal { get; set; }

        protected override void Initialize(HttpControllerContext controllerContext)
        {
            base.Initialize(controllerContext);

            if (Container != null)
            {
                try
                {
                    var authenticationState = Container.Resolve<IAuthenticationState>(new { principal = User });
                    UserPrincipal = authenticationState.CurrentUser;
                }
                catch
                {
                    // Для тех случаев, когда аутентифицированный пользователь уже заблокирован/удален
                    Container.Resolve<IAuthenticator>().SignOut();
                }
            }
        }
    }
}