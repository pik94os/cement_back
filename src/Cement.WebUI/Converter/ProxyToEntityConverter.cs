﻿using Cement.Core.Services;

namespace Cement.WebUI.Converter
{
    public class ProxyToEntityConverter : IProxyToEntityConverter
    {
        public T ConvertToEntity<T, TP>(TP proxy)
        {
            return AutoMapper.Mapper.DynamicMap<T>(proxy);
        }
    }
}