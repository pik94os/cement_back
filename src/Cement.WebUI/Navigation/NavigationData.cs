﻿using System.Collections.Generic;
using Cement.Core.Entities;
using Cement.Core.Enums;
using Newtonsoft.Json;

namespace Cement.WebUI.Navigation
{
    public class NavigationData
    {
        [JsonProperty(PropertyName = "name")]
        public virtual string Name { get; set; }

        [JsonIgnore]
        public string Permission { get; set; }

        public string ChildPermission(string permission)
        {
            return string.Format("{0}_{1}", Permission, permission);
        }
    }

    public class NavigationLevel1 : NavigationData
    {
        [JsonIgnore]
        public int Order { get; set; }

        [JsonProperty(PropertyName = "groupName")]
        public override string Name { get; set; }

        [JsonProperty(PropertyName = "items")]
        public List<NavigationLevel2> Items = new List<NavigationLevel2>();
    }

    public class NavigationLevel2 : NavigationData
    {
        [JsonProperty(PropertyName = "text")]
        public override string Name { get; set; }

        [JsonProperty(PropertyName = "master")]
        public string Master { get; set; }

        [JsonProperty(PropertyName = "detail")]
        public string Detail = "";

        [JsonProperty(PropertyName = "accessType")]
        public AccessType AccessType { get; set; }

        [JsonProperty(PropertyName = "tabs")]
        public List<NavigationLevel3> Items = new List<NavigationLevel3>();

        [JsonIgnore]
        public UserRole Role { get; set; }

        public NavigationLevel2()
        {
            Role = UserRole.Employee;
        }
    }

    public class NavigationLevel3 : NavigationData
    {
        [JsonProperty(PropertyName = "xtype")]
        public string Xtype { get; set; }

        [JsonProperty(PropertyName = "hidden")]
        public bool Hidden { get; set; }

        [JsonProperty(PropertyName = "title")]
        public override string Name { get; set; }

        [JsonProperty(PropertyName = "detailType")]
        public string DetailType = "";

        [JsonProperty(PropertyName = "accessType")]
        public AccessType AccessType { get; set; }
    }
}