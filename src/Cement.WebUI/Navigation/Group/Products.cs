﻿namespace Cement.WebUI.Navigation.Group
{
    public class Products : INavigationGroup
    {
        public NavigationLevel1 GetGroup()
        {
            var root = new NavigationLevel1
            {
                Name = "Товары (услуги)",
                Order = 40,
                Permission = "Products"
            };

            AddProduct(root);
            AddService(root);
            AddPriceList(root);
            AddPackType(root);

            return root;
        }

        private void AddProduct(NavigationLevel1 root)
        {
            var data = new NavigationLevel2
            {
                Name = "Товары",
                Master = "Cement.view.products.products.Total",
                Detail = "Cement.view.products.products.View",
                Permission = root.ChildPermission("Product")
            };

            data.Items.Add(new NavigationLevel3
            {
                Name = "На модерацию",
                Permission = data.ChildPermission("Moderation"),
                Xtype = "products_products_complex_moderation"
            });

            data.Items.Add(new NavigationLevel3
            {
                Name = "Общий каталог",
                Permission = data.ChildPermission("General"),
                Xtype = "products_products_complex_general"
            });

            data.Items.Add(new NavigationLevel3
            {
                Name = "Персональный каталог",
                Permission = data.ChildPermission("Personal"),
                Xtype = "products_products_complex_personal"
            });

            data.Items.Add(new NavigationLevel3
            {
                Name = "Архив",
                Permission = data.ChildPermission("Archive"),
                Xtype = "products_products_complex_archive"
            });

            root.Items.Add(data);
        }

        private void AddService(NavigationLevel1 root)
        {
            var data = new NavigationLevel2
            {
                Name = "Услуги",
                Master = "Cement.view.products.services.Total",
                Detail = "Cement.view.products.services.View",
                Permission = root.ChildPermission("Service")
            };

            data.Items.Add(new NavigationLevel3
            {
                Name = "На модерацию",
                Permission = data.ChildPermission("Moderation"),
                Xtype = "products_services_complex_moderation"
            });

            data.Items.Add(new NavigationLevel3
            {
                Name = "Общий каталог",
                Permission = data.ChildPermission("General"),
                Xtype = "products_services_complex_general"
            });

            data.Items.Add(new NavigationLevel3
            {
                Name = "Персональный каталог",
                Permission = data.ChildPermission("Personal"),
                Xtype = "products_services_complex_personal"
            });

            data.Items.Add(new NavigationLevel3
            {
                Name = "Архив",
                Permission = data.ChildPermission("Archive"),
                Xtype = "products_services_complex_archive"
            });

            root.Items.Add(data);
        }

        private void AddPriceList(NavigationLevel1 root)
        {
            var data = new NavigationLevel2
            {
                Name = "Прайс-лист",
                Master = "Cement.view.products.price.Total",
                Detail = "Cement.view.products.price.View",
                Permission = root.ChildPermission("PriceList")
            };

            data.Items.Add(new NavigationLevel3
            {
                Name = "Новые",
                Permission = data.ChildPermission("New"),
                Xtype = "products_price_new_complex"
            });

            data.Items.Add(new NavigationLevel3
            {
                Name = "Действующие",
                Permission = data.ChildPermission("Current"),
                Xtype = "products_price_current_complex"
            });

            data.Items.Add(new NavigationLevel3
            {
                Name = "Архив",
                Permission = data.ChildPermission("Archive"),
                Xtype = "products_price_archive_complex"
            });

            root.Items.Add(data);
        }

        private void AddPackType(NavigationLevel1 root)
        {
            var data = new NavigationLevel2
            {
                Name = "Тип упаковки",
                Master = "Cement.view.products.packtype.List",
                Detail = "Cement.view.products.packtype.View",
                Permission = root.ChildPermission("PackType"),
                Items = null
            };

            root.Items.Add(data);
        }
    }
}