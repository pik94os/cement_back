﻿namespace Cement.WebUI.Navigation.Group
{
    public class OfficeWork : INavigationGroup
    {
        public NavigationLevel1 GetGroup()
        {
            var root = new NavigationLevel1
            {
                Name = "Делопроизводство",
                Order = 100,
                Permission = "OfficeWork"
            };

            AddInternalDocument(root);
            AddCorrespondence(root);
            AddDisposal(root);
            
            return root;
        }

        private void AddInternalDocument(NavigationLevel1 root)
        {
            var data = new NavigationLevel2
            {
                Name = "Внутренние документы",
                Master = "Cement.view.office_work.documents.Total",
                Detail = "Cement.view.office_work.documents.View",
                Permission = root.ChildPermission("Documents")
            };

            data.Items.Add(new NavigationLevel3
            {
                Name = "Новые",
                Permission = data.ChildPermission("New"),
                Xtype = "office_work_documents_complex_new"
            });

            data.Items.Add(new NavigationLevel3
            {
                Name = "Входящие",
                Permission = data.ChildPermission("Incoming"),
                Xtype = "office_work_documents_complex_incoming"
            });

            data.Items.Add(new NavigationLevel3
            {
                Name = "Исходящие",
                Permission = data.ChildPermission("Outcoming"),
                Xtype = "office_work_documents_complex_outcoming"
            });

            data.Items.Add(new NavigationLevel3
            {
                Name = "Архив",
                Permission = data.ChildPermission("Archive"),
                Xtype = "office_work_documents_complex_archive"
            });

            root.Items.Add(data);
        }

        private void AddCorrespondence(NavigationLevel1 root)
        {
            var data = new NavigationLevel2
            {
                Name = "Корреспонденция",
                Master = "Cement.view.office_work.correspondention.Total",
                Detail = "Cement.view.office_work.correspondention.View",
                Permission = root.ChildPermission("Correspondence")
            };

            data.Items.Add(new NavigationLevel3
            {
                Name = "Новые",
                Permission = data.ChildPermission("New"),
                Xtype = "office_work_correspondention_complex_new"
            });

            data.Items.Add(new NavigationLevel3
            {
                Name = "Входящие",
                Permission = data.ChildPermission("Incoming"),
                Xtype = "office_work_correspondention_complex_incoming"
            });

            data.Items.Add(new NavigationLevel3
            {
                Name = "Исходящие",
                Permission = data.ChildPermission("Outcoming"),
                Xtype = "office_work_correspondention_complex_outcoming"
            });

            data.Items.Add(new NavigationLevel3
            {
                Name = "Архив",
                Permission = data.ChildPermission("Archive"),
                Xtype = "office_work_correspondention_complex_archive"
            });

            root.Items.Add(data);
        }

        private void AddDisposal(NavigationLevel1 root)
        {
            var data = new NavigationLevel2
            {
                Name = "Приказы",
                Master = "Cement.view.office_work.disposal.Total",
                Detail = "Cement.view.office_work.disposal.View",
                Permission = root.ChildPermission("Disposal")
            };

            data.Items.Add(new NavigationLevel3
            {
                Name = "Новые",
                Permission = data.ChildPermission("New"),
                Xtype = "office_work_disposal_complex_new"
            });

            data.Items.Add(new NavigationLevel3
            {
                Name = "Входящие",
                Permission = data.ChildPermission("Incoming"),
                Xtype = "office_work_disposal_complex_incoming"
            });

            data.Items.Add(new NavigationLevel3
            {
                Name = "Исходящие",
                Permission = data.ChildPermission("Outcoming"),
                Xtype = "office_work_disposal_complex_outcoming"
            });

            data.Items.Add(new NavigationLevel3
            {
                Name = "Архив",
                Permission = data.ChildPermission("Archive"),
                Xtype = "office_work_disposal_complex_archive"
            });

            root.Items.Add(data);
        }
    }
}