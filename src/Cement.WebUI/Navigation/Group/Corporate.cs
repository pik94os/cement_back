﻿using System.Collections.Generic;
using Cement.Core.Entities;

namespace Cement.WebUI.Navigation.Group
{
    public class Corporate : INavigationGroup
    {
        public NavigationLevel1 GetGroup()
        {
            var root = new NavigationLevel1
            {
                Name = "Корпоративные данные",
                Order = 20,
                Permission = "Corporate"
            };
            
            root.Items.AddRange(new List<NavigationLevel2>
            {
                new NavigationLevel2
                {
                    Name = "Реквизиты",
                    Master = "Cement.view.corporate.firm.ClientView",
                    Detail = "Cement.view.corporate.firm.View",
                    Permission = root.ChildPermission("Requisite")
                },
                new NavigationLevel2
                {
                    Name = "Реквизиты счетов",
                    Master = "Cement.view.corporate.bank_account.List",
                    Detail = "Cement.view.corporate.bank_account.View",
                    Permission = root.ChildPermission("PaymentAccounts")
                },
                new NavigationLevel2
                {
                    Name = "Номенклатура дел",
                    Master = "Cement.view.corporate.nomenklature.List",
                    Detail = "Cement.view.corporate.nomenklature.View",
                    Permission = root.ChildPermission("Nomenklature")
                },
                new NavigationLevel2
                {
                    Name = "Сотрудники",
                    Master = "Cement.view.corporate.employee.List",
                    Detail = "Cement.view.corporate.employee.View",
                    Permission = root.ChildPermission("Employee"),
                    Role = UserRole.Admin | UserRole.Employee
                },
                new NavigationLevel2
                {
                    Name = "Подписи",
                    Master = "Cement.view.corporate.signature.List",
                    Detail = "Cement.view.corporate.signature.View",
                    Permission = root.ChildPermission("Signature")
                },
                new NavigationLevel2
                {
                    Name = "Администрирование",
                    Master = "Cement.view.corporate.administration.List",
                    Detail = "Cement.view.corporate.administration.View",
                    Permission = root.ChildPermission("Administration"),
                    Role = UserRole.Admin
                }
            });
            
            return root;
        }
    }
}