﻿using System.Collections.Generic;

namespace Cement.WebUI.Navigation.Group
{
    public class Warehouse : INavigationGroup
    {
        public NavigationLevel1 GetGroup()
        {
            var root = new NavigationLevel1
            {
                Name = "Склад",
                Order = 85,
                Permission = "Warehouse"
            };
            
            root.Items.AddRange(new List<NavigationLevel2>
            {
                new NavigationLevel2
                {
                    Name = "Склады",
                    Master = "Cement.view.corporate.warehouse.List",
                    Detail = "Cement.view.corporate.warehouse.View",
                    Permission = root.ChildPermission("Warehouse")
                }
            });

            AddMovement(root);
            AddReceipt(root);
            AddExpense(root);
            AddInternal(root);
            AddInventory(root);
            
            return root;
        }

        private void AddInternal(NavigationLevel1 root)
        {
            var data = new NavigationLevel2
            {
                Name = "Внутренние заявки",
                Master = "Cement.view.warehouse.internal.Total",
                Detail = "Cement.view.warehouse.internal.view.View",
                Permission = root.ChildPermission("Internal")
            };

            data.Items.Add(new NavigationLevel3
            {
                Name = "Новые",
                Permission = data.ChildPermission("New"),
                Xtype = "warehouse_internal_complex_new"
            });

            data.Items.Add(new NavigationLevel3
            {
                Name = "Входящие",
                Permission = data.ChildPermission("Incoming"),
                Xtype = "warehouse_internal_complex_incoming"
            });

            data.Items.Add(new NavigationLevel3
            {
                Name = "Исходящие",
                Permission = data.ChildPermission("Outcoming"),
                Xtype = "warehouse_internal_complex_outcoming"
            });

            data.Items.Add(new NavigationLevel3
            {
                Name = "Архив",
                Permission = data.ChildPermission("Archive"),
                Xtype = "warehouse_internal_complex_archive"
            });

            root.Items.Add(data);
        }

        private void AddInventory(NavigationLevel1 root)
        {
            var data = new NavigationLevel2
            {
                Name = "Инвентаризация",
                Master = "Cement.view.warehouse.inventory.Total",
                Detail = "Cement.view.warehouse.inventory.view.View",
                Permission = root.ChildPermission("Inventory")
            };

            data.Items.Add(new NavigationLevel3
            {
                Name = "Новые",
                Permission = data.ChildPermission("New"),
                Xtype = "warehouse_inventory_complex_new"
            });

            data.Items.Add(new NavigationLevel3
            {
                Name = "Входящие",
                Permission = data.ChildPermission("Incoming"),
                Xtype = "warehouse_inventory_complex_incoming"
            });

            data.Items.Add(new NavigationLevel3
            {
                Name = "Исходящие",
                Permission = data.ChildPermission("Outcoming"),
                Xtype = "warehouse_inventory_complex_outcoming"
            });

            data.Items.Add(new NavigationLevel3
            {
                Name = "Архив",
                Permission = data.ChildPermission("Archive"),
                Xtype = "warehouse_inventory_complex_archive"
            });

            root.Items.Add(data);
        }

        private void AddMovement(NavigationLevel1 root)
        {
            var data = new NavigationLevel2
            {
                Name = "Движение товара",
                //Master = "Cement.view.warehouse.movement.Total",
                Master = "Cement.view.warehouse.movement.complex.New",
                Detail = "Cement.view.warehouse.movement.view.View",
                Permission = root.ChildPermission("Movement")
            };

            //data.Items.Add(new NavigationLevel3
            //{
            //    Name = "Новые",
            //    Permission = data.ChildPermission("New"),
            //    Xtype = "warehouse_movement_complex_new"
            //});

            //data.Items.Add(new NavigationLevel3
            //{
            //    Name = "Входящие",
            //    Permission = data.ChildPermission("Incoming"),
            //    Xtype = "warehouse_movement_complex_incoming"
            //});

            //data.Items.Add(new NavigationLevel3
            //{
            //    Name = "Исходящие",
            //    Permission = data.ChildPermission("Outcoming"),
            //    Xtype = "warehouse_movement_complex_outcoming"
            //});

            //data.Items.Add(new NavigationLevel3
            //{
            //    Name = "Архив",
            //    Permission = data.ChildPermission("Archive"),
            //    Xtype = "warehouse_movement_complex_archive"
            //});

            root.Items.Add(data);
        }

        private void AddReceipt(NavigationLevel1 root)
        {
            var data = new NavigationLevel2
            {
                Name = "Приходный складской ордер",
                Master = "Cement.view.warehouse.receipt.Total",
                Detail = "Cement.view.warehouse.receipt.view.View",
                Permission = root.ChildPermission("Receipt")
            };

            data.Items.Add(new NavigationLevel3
            {
                Name = "Новые",
                Permission = data.ChildPermission("New"),
                Xtype = "warehouse_receipt_complex_new"
            });

            data.Items.Add(new NavigationLevel3
            {
                Name = "Входящие",
                Permission = data.ChildPermission("Incoming"),
                Xtype = "warehouse_receipt_complex_incoming"
            });

            data.Items.Add(new NavigationLevel3
            {
                Name = "Исходящие",
                Permission = data.ChildPermission("Outcoming"),
                Xtype = "warehouse_receipt_complex_outcoming"
            });

            data.Items.Add(new NavigationLevel3
            {
                Name = "Архив",
                Permission = data.ChildPermission("Archive"),
                Xtype = "warehouse_receipt_complex_archive"
            });

            root.Items.Add(data);
        }

        private void AddExpense(NavigationLevel1 root)
        {
            var data = new NavigationLevel2
            {
                Name = "Расходный складской ордер",
                Master = "Cement.view.warehouse.expense.Total",
                Detail = "Cement.view.warehouse.expense.view.View",
                Permission = root.ChildPermission("Expense")
            };

            data.Items.Add(new NavigationLevel3
            {
                Name = "Новые",
                Permission = data.ChildPermission("New"),
                Xtype = "warehouse_expense_complex_new"
            });

            data.Items.Add(new NavigationLevel3
            {
                Name = "Входящие",
                Permission = data.ChildPermission("Incoming"),
                Xtype = "warehouse_expense_complex_incoming"
            });

            data.Items.Add(new NavigationLevel3
            {
                Name = "Исходящие",
                Permission = data.ChildPermission("Outcoming"),
                Xtype = "warehouse_expense_complex_outcoming"
            });

            data.Items.Add(new NavigationLevel3
            {
                Name = "Архив",
                Permission = data.ChildPermission("Archive"),
                Xtype = "warehouse_expense_complex_archive"
            });

            root.Items.Add(data);
        }
    }
}