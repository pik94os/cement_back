﻿namespace Cement.WebUI.Navigation.Group
{
    public class Transport : INavigationGroup
    {
        public NavigationLevel1 GetGroup()
        {
            var root = new NavigationLevel1
            {
                Name = "Транспорт",
                Order = 30,
                Permission = "Transport"
            };

            AddAuto(root);
            AddAutoControl(root);
            AddTransportUnit(root);
            AddTimesheet(root);
            //AddRailwayTransport(root);

            return root;
        }

        private void AddAuto(NavigationLevel1 root)
        {
            var data = new NavigationLevel2
            {
                Name = "Автомобильный транспорт",
                Master = "Cement.view.transport.auto.Complex",
                Detail = "Cement.view.transport.auto.View",
                Permission = root.ChildPermission("Auto")
            };

            data.Items.Add(new NavigationLevel3
            {
                Name = "Собственные",
                Permission = data.ChildPermission("Owned"),
                Xtype = "transport_auto_owned",
                DetailType = "Cement.view.transport.auto.View"
            });

            data.Items.Add(new NavigationLevel3
            {
                Name = "Переданные в аренду",
                Permission = data.ChildPermission("SentToRent"),
                Xtype = "transport_auto_sent_to_rent",
                DetailType = "Cement.view.transport.auto.ViewSentToRent"
            });

            data.Items.Add(new NavigationLevel3
            {
                Name = "Полученные в аренду",
                Permission = data.ChildPermission("ViewGotInRent"),
                Xtype = "transport_auto_got_in_rent",
                DetailType = "Cement.view.transport.auto.ViewGotInRent"
            });

            root.Items.Add(data);
        }

        private void AddAutoControl(NavigationLevel1 root)
        {
            var data = new NavigationLevel2
            {
                Name = "Управление АТС",
                Master = "Cement.view.transport.auto_control.Complex",
                Detail = "Cement.view.transport.auto_control.view.Maintenance",
                Permission = root.ChildPermission("AutoControl")
            };

            data.Items.Add(new NavigationLevel3
            {
                Name = "Техобслуживание",
                Permission = data.ChildPermission("Maintenance"),
                Xtype = "transport_auto_control_maintenance",
                DetailType = "Cement.view.transport.auto_control.view.Maintenance"
            });

            //data.Items.Add(new NavigationLevel3
            //{
            //    Name = "Регламент",
            //    Permission = data.ChildPermission("Regulation"),
            //    Xtype = "transport_auto_control_regulation",
            //    DetailType = "Cement.view.transport.auto_control.view.Regulation"
            //});

            data.Items.Add(new NavigationLevel3
            {
                Name = "Запчасти/услуги",
                Permission = data.ChildPermission("Part"),
                Xtype = "transport_auto_control_part",
                DetailType = "Cement.view.transport.auto_control.view.Part"
            });

            data.Items.Add(new NavigationLevel3
            {
                Name = "Топливо",
                Permission = data.ChildPermission("Fuel"),
                Xtype = "transport_auto_control_fuel",
                DetailType = "Cement.view.transport.auto_control.view.Fuel"
            });
            
            data.Items.Add(new NavigationLevel3
            {
                Name = "Пробег",
                Permission = data.ChildPermission("Odometer"),
                Xtype = "transport_auto_control_odometer",
                DetailType = "Cement.view.transport.auto_control.view.Odometer"
            });

            data.Items.Add(new NavigationLevel3
            {
                Name = "Страховки",
                Permission = data.ChildPermission("Insurance"),
                Xtype = "transport_auto_control_insurance",
                DetailType = "Cement.view.transport.auto_control.view.Insurance"
            });

            data.Items.Add(new NavigationLevel3
            {
                Name = "ДТП",
                Permission = data.ChildPermission("Crash"),
                Xtype = "transport_auto_control_crash",
                DetailType = "Cement.view.transport.auto_control.view.Crash"
            });

            root.Items.Add(data);
        }

        private void AddTransportUnit(NavigationLevel1 root)
        {
            var data = new NavigationLevel2
            {
                Name = "Транспортные единицы",
                Master = "Cement.view.transport.units.Complex",
                Detail = "Cement.view.transport.units.View",
                Permission = root.ChildPermission("TransportUnit")
            };

            data.Items.Add(new NavigationLevel3
            {
                Name = "Собственные",
                Permission = data.ChildPermission("Owned"),
                Xtype = "transport_units_lists_owned",
                DetailType = "Cement.view.transport.units.View"
            });

            data.Items.Add(new NavigationLevel3
            {
                Name = "Переданные в управление",
                Permission = data.ChildPermission("SentToRent"),
                Xtype = "transport_auto_sent_to_rent",
                DetailType = "Cement.view.transport.units.ViewSentToRent"
            });

            data.Items.Add(new NavigationLevel3
            {
                Name = "Полученные в управление",
                Permission = data.ChildPermission("ViewGotInRent"),
                Xtype = "transport_auto_got_in_rent",
                DetailType = "Cement.view.transport.units.ViewGotInRent"
            });

            root.Items.Add(data);
        }

        private void AddRailwayTransport(NavigationLevel1 root)
        {
            var data = new NavigationLevel2
            {
                Name = "ЖД Транспорт",
                Master = "Cement.view.transport.railway.Complex",
                Detail = "Cement.view.transport.railway.View",
                Permission = root.ChildPermission("RailwayTransport"),
                Items = null
            };

            root.Items.Add(data);
        }


        private void AddTimesheet(NavigationLevel1 root)
        {
            var data = new NavigationLevel2
            {
                Name = "Табель учета",
                Master = "Cement.view.transport.timesheet.Total",
                Detail = "Cement.view.transport.units.View",
                Permission = root.ChildPermission("Timesheet")
            };

            root.Items.Add(data);
        }
    }
}