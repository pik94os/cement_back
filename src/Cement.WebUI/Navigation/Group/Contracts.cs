﻿namespace Cement.WebUI.Navigation.Group
{
    public class Contracts : INavigationGroup
    {
        public NavigationLevel1 GetGroup()
        {
            var root = new NavigationLevel1
            {
                Name = "Договора",
                Order = 60,
                Permission = "Contracts"
            };

            AddContract(root);
            AddContractsAnnex(root);

            return root;
        }

        private void AddContract(NavigationLevel1 root)
        {
            var data = new NavigationLevel2
            {
                Name = "Договор",
                Master = "Cement.view.contracts.contracts.Total",
                Detail = "Cement.view.contracts.contracts.view.Contract",
                Permission = root.ChildPermission("Contract")
            };

            data.Items.Add(new NavigationLevel3
            {
                Name = "Новые",
                Permission = data.ChildPermission("New"),
                Xtype = "contracts_contracts_complex_new"
            });

            data.Items.Add(new NavigationLevel3
            {
                Name = "Входящие",
                Permission = data.ChildPermission("Incoming"),
                Xtype = "contracts_contracts_complex_incoming"
            });

            data.Items.Add(new NavigationLevel3
            {
                Name = "Исходящие",
                Permission = data.ChildPermission("Outcoming"),
                Xtype = "contracts_contracts_complex_outcoming"
            });

            data.Items.Add(new NavigationLevel3
            {
                Name = "Архив",
                Permission = data.ChildPermission("Archive"),
                Xtype = "contracts_contracts_complex_archive"
            });

            root.Items.Add(data);
        }

        private void AddContractsAnnex(NavigationLevel1 root)
        {
            var data = new NavigationLevel2
            {
                Name = "Приложения",
                Master = "Cement.view.contracts.addons.Total",
                Detail = "Cement.view.contracts.contracts.view.Contract",
                Permission = root.ChildPermission("ContractAnnex")
            };

            data.Items.Add(new NavigationLevel3
            {
                Name = "Новые",
                Permission = data.ChildPermission("New"),
                Xtype = "contracts_addons_complex_new"
            });

            data.Items.Add(new NavigationLevel3
            {
                Name = "Входящие",
                Permission = data.ChildPermission("Incoming"),
                Xtype = "contracts_addons_complex_incoming"
            });

            data.Items.Add(new NavigationLevel3
            {
                Name = "Исходящие",
                Permission = data.ChildPermission("Outcoming"),
                Xtype = "contracts_addons_complex_outcoming"
            });

            data.Items.Add(new NavigationLevel3
            {
                Name = "Архив",
                Permission = data.ChildPermission("Archive"),
                Xtype = "contracts_addons_complex_archive"
            });

            root.Items.Add(data);
        }
    }
}