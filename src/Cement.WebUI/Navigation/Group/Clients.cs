﻿namespace Cement.WebUI.Navigation.Group
{
    public class Clients : INavigationGroup
    {
        public NavigationLevel1 GetGroup()
        {
            var root = new NavigationLevel1
            {
                Name = "Клиенты",
                Order = 50,
                Permission = "Clients"
            };

            AddClients(root);
            AddManagement(root);
            AddGroups(root);
            AddStatuses(root);

            return root;
        }

        private void AddClients(NavigationLevel1 root)
        {
            var data = new NavigationLevel2
            {
                Name = "Клиенты",
                Master = "Cement.view.clients.clients.Total",
                Detail = "Cement.view.clients.View",
                Permission = root.ChildPermission("Clients")
            };

            data.Items.Add(new NavigationLevel3
            {
                Name = "Общий каталог",
                Permission = data.ChildPermission("Current"),
                Xtype = "clients_current_complex"
            });

            data.Items.Add(new NavigationLevel3
            {
                Name = "Корпоративный каталог",
                Permission = data.ChildPermission("Corporate"),
                Xtype = "clients_corporate_complex"
            });

            data.Items.Add(new NavigationLevel3
            {
                Name = "Персональный каталог",
                Permission = data.ChildPermission("Personal"),
                Xtype = "clients_personal_complex"
            });

            data.Items.Add(new NavigationLevel3
            {
                Name = "Архив",
                Permission = data.ChildPermission("Archive"),
                Xtype = "clients_archive_complex"
            });

            root.Items.Add(data);
        }

        private void AddManagement(NavigationLevel1 root)
        {
            var data = new NavigationLevel2
            {
                Name = "Управление клиентами",
                Master = "Cement.view.clients.management.Total",
                Detail = "Cement.view.clients.management.View",
                Permission = root.ChildPermission("Management")
            };

            data.Items.Add(new NavigationLevel3
            {
                Name = "События",
                Permission = data.ChildPermission("Events"),
                Xtype = "clients_management_complex_events"
            });

            data.Items.Add(new NavigationLevel3
            {
                Name = "Архив",
                Permission = data.ChildPermission("Archive"),
                Xtype = "clients_management_complex_archive"
            });

            root.Items.Add(data);
        }

        private void AddGroups(NavigationLevel1 root)
        {
            var data = new NavigationLevel2
            {
                Name = "Группы/подгруппы",
                Master = "Cement.view.clients.groups.Total",
                Detail = "Cement.view.clients.groups.view.Group",
                Permission = root.ChildPermission("Groups")
            };

            data.Items.Add(new NavigationLevel3
            {
                Name = "Группы",
                Permission = data.ChildPermission("Group"),
                Xtype = "clients_groups_list_groups",
                DetailType = "Cement.view.clients.groups.view.Group"
            });

            data.Items.Add(new NavigationLevel3
            {
                Name = "Подгруппы",
                Permission = data.ChildPermission("SubGroup"),
                Xtype = "clients_groups_list_sub_groups",
                DetailType = "Cement.view.clients.groups.view.SubGroup"
            });

            root.Items.Add(data);
        }

        private void AddStatuses(NavigationLevel1 root)
        {
            var data = new NavigationLevel2
            {
                Name = "Статус",
                Master = "Cement.view.clients.statuses.list.Statuses",
                Detail = "Cement.view.clients.statuses.View",
                Permission = root.ChildPermission("Statuses"),
                Items = null
            };

            root.Items.Add(data);
        }
    }
}