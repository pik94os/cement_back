﻿namespace Cement.WebUI.Navigation.Group
{
    public class Goods : INavigationGroup
    {
        public NavigationLevel1 GetGroup()
        {
            var root = new NavigationLevel1
            {
                Name = "Товарооборот",
                Order = 70,
                Permission = "Goods"
            };

            AddCatalogue(root);
            AddProductRequest(root);
            AddServiceRequest(root);

            return root;
        }

        private void AddCatalogue(NavigationLevel1 root)
        {
            var data = new NavigationLevel2
            {
                Name = "Каталог",
                Master = "Cement.view.goods.catalog.Complex",
                Detail = "Cement.view.goods.catalog.ProductView",
                Permission = root.ChildPermission("Catalogue")
            };

            data.Items.Add(new NavigationLevel3
            {
                Name = "Товары",
                Permission = data.ChildPermission("Products"),
                Xtype = "goods_catalog_products",
                DetailType = "Cement.view.goods.catalog.ProductView"
            });

            data.Items.Add(new NavigationLevel3
            {
                Name = "Услуги",
                Permission = data.ChildPermission("Services"),
                Xtype = "goods_catalog_services",
                DetailType = "Cement.view.goods.catalog.ServiceView"
            });

            root.Items.Add(data);
        }

        private void AddProductRequest(NavigationLevel1 root)
        {
            var data = new NavigationLevel2
            {
                Name = "Заявки на товар",
                Master = "Cement.view.goods.auto.Total",
                Detail = "Cement.view.goods.auto.view.TemplateView",
                Permission = root.ChildPermission("ProductRequest")
            };

            //data.Items.Add(new NavigationLevel3
            //{
            //    Name = "Шаблоны",
            //    Permission = data.ChildPermission("Templates"),
            //    Xtype = "goods_auto_complex_templates"
            //});

            data.Items.Add(new NavigationLevel3
            {
                Name = "Новые",
                Permission = data.ChildPermission("New"),
                Xtype = "goods_auto_complex_new"
            });

            data.Items.Add(new NavigationLevel3
            {
                Name = "Входящие",
                Permission = data.ChildPermission("Incoming"),
                Xtype = "goods_auto_complex_incoming"
            });

            data.Items.Add(new NavigationLevel3
            {
                Name = "Исходящие",
                Permission = data.ChildPermission("Outcoming"),
                Xtype = "goods_auto_complex_outcoming"
            });

            data.Items.Add(new NavigationLevel3
            {
                Name = "Архив",
                Permission = data.ChildPermission("Archive"),
                Xtype = "goods_auto_complex_archive"
            });

            root.Items.Add(data);
        }

        private void AddServiceRequest(NavigationLevel1 root)
        {
            var data = new NavigationLevel2
            {
                Name = "Заявки на услуги",
                Master = "Cement.view.goods.services.Total",
                Detail = "Cement.view.goods.services.view.TemplateView",
                Permission = root.ChildPermission("Groups")
            };

            data.Items.Add(new NavigationLevel3
            {
                Name = "Шаблоны",
                Permission = data.ChildPermission("Templates"),
                Xtype = "goods_services_complex_templates"
            });

            data.Items.Add(new NavigationLevel3
            {
                Name = "Новые",
                Permission = data.ChildPermission("New"),
                Xtype = "goods_services_complex_new"
            });

            data.Items.Add(new NavigationLevel3
            {
                Name = "Входящие",
                Permission = data.ChildPermission("Incoming"),
                Xtype = "goods_services_complex_incoming"
            });

            data.Items.Add(new NavigationLevel3
            {
                Name = "Исходящие",
                Permission = data.ChildPermission("Outcoming"),
                Xtype = "goods_services_complex_outcoming"
            });

            data.Items.Add(new NavigationLevel3
            {
                Name = "Архив",
                Permission = data.ChildPermission("Archive"),
                Xtype = "goods_services_complex_archive"
            });

            root.Items.Add(data);
        }
    }
}