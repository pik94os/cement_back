﻿using System.Collections.Generic;
using Cement.Core.Entities;

namespace Cement.WebUI.Navigation.Group
{
    public class Administration : INavigationGroup
    {
        public NavigationLevel1 GetGroup()
        {
            var root = new NavigationLevel1
            {
                Name = "Администрирование",
                Permission = "Administration"
            };

            root.Items.AddRange(new List<NavigationLevel2>
            {
                new NavigationLevel2
                {
                    Name = "Организации",
                    Master = "Cement.view.administration.organizations.Grid",
                    Role = UserRole.Root,
                    Permission = root.ChildPermission("Organizations"),
                    Items = null
                },
                new NavigationLevel2
                {
                    Name = "Справочники",
                    Master = "Cement.view.administration.dictionaries.List",
                    Role = UserRole.Root,
                    Permission = root.ChildPermission("Dictionaries"),
                    Items = null
                },
                new NavigationLevel2
                {
                    Name = "Суперпользователи",
                    Master = "Cement.view.administration.superusers.List",
                    Role = UserRole.Root,
                    Permission = root.ChildPermission("Superusers"),
                    Items = null
                }
            });

            return root;
        }
    }
}