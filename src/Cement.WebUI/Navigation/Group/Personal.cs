﻿namespace Cement.WebUI.Navigation.Group
{
    public class Personal : INavigationGroup
    {
        public NavigationLevel1 GetGroup()
        {
            var root = new NavigationLevel1
            {
                Name = "Персональные данные",
                Order = 10,
                Permission = "Personal"
            };

            AddDocuments(root);
            AddCorresspondence(root);
            AddCalendar(root);
            AddMessages(root);
            AddAdverts(root);

            return root;
        }

        private void AddDocuments(NavigationLevel1 root)
        {
            var data = new NavigationLevel2
            {
                Name = "Внутренние документы",
                Master = "Cement.view.personal.documents.Total",
                Permission = root.ChildPermission("Documents")
            };

            data.Items.Add(new NavigationLevel3
            {
                Name = "Входящие",
                Permission = data.ChildPermission("Incoming"),
                Xtype = "personal_documents_lists_incoming"
            });

            data.Items.Add(new NavigationLevel3
            {
                Name = "Исходящие",
                Permission = data.ChildPermission("Outcoming"),
                Xtype = "personal_documents_lists_outcoming"
            });

            root.Items.Add(data);
        }

        private void AddCorresspondence(NavigationLevel1 root)
        {
            var data = new NavigationLevel2
            {
                Name = "Корреспонденция",
                Master = "Cement.view.personal.correspondention.Total",
                Permission = root.ChildPermission("Corresspondence")
            };

            data.Items.Add(new NavigationLevel3
            {
                Name = "Входящие",
                Permission = data.ChildPermission("Incoming"),
                Xtype = "personal_correspondention_lists_incoming"
            });

            data.Items.Add(new NavigationLevel3
            {
                Name = "Исходящие",
                Permission = data.ChildPermission("Outcoming"),
                Xtype = "personal_correspondention_lists_outcoming"
            });

            root.Items.Add(data);
        }

        private void AddCalendar(NavigationLevel1 root)
        {
            var data = new NavigationLevel2
            {
                Name = "Календарь",
                Master = "Cement.view.personal.calendar.Total",
                Detail = "Cement.view.personal.calendar.view.DatePicker",
                Permission = root.ChildPermission("Calendar")
            };

            root.Items.Add(data);
        }

        private void AddMessages(NavigationLevel1 root)
        {
            var data = new NavigationLevel2
            {
                Name = "Сообщения",
                Master = "Cement.view.personal.messages.Total",
                Detail = "Cement.view.personal.messages.view.View",
                Permission = root.ChildPermission("Messages")
            };

            data.Items.Add(new NavigationLevel3
            {
                Name = "Новые",
                Permission = data.ChildPermission("New"),
                Xtype = "personal_messages_complex_new"
            });

            data.Items.Add(new NavigationLevel3
            {
                Name = "Входящие",
                Permission = data.ChildPermission("Incoming"),
                Xtype = "personal_messages_complex_incoming"
            });

            data.Items.Add(new NavigationLevel3
            {
                Name = "Исходящие",
                Permission = data.ChildPermission("Outcoming"),
                Xtype = "personal_messages_complex_outcoming"
            });

            data.Items.Add(new NavigationLevel3
            {
                Name = "Архив",
                Permission = data.ChildPermission("Archive"),
                Xtype = "personal_messages_complex_archive"
            });

            root.Items.Add(data);
        }

        private void AddAdverts(NavigationLevel1 root)
        {
            var data = new NavigationLevel2
            {
                Name = "Объявления",
                Master = "Cement.view.personal.adverts.Total",
                Detail = "Cement.view.personal.adverts.view.View",
                Permission = root.ChildPermission("Adverts")
            };

            data.Items.Add(new NavigationLevel3
            {
                Name = "Новые",
                Permission = data.ChildPermission("New"),
                Xtype = "personal_adverts_complex_new"
            });

            data.Items.Add(new NavigationLevel3
            {
                Name = "Входящие",
                Permission = data.ChildPermission("Incoming"),
                Xtype = "personal_adverts_complex_incoming"
            });

            data.Items.Add(new NavigationLevel3
            {
                Name = "Исходящие",
                Permission = data.ChildPermission("Outcoming"),
                Xtype = "personal_adverts_complex_outcoming"
            });

            data.Items.Add(new NavigationLevel3
            {
                Name = "Архив",
                Permission = data.ChildPermission("Archive"),
                Xtype = "personal_adverts_complex_archive"
            });

            root.Items.Add(data);
        }
    }
}