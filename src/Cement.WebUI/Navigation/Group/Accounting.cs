﻿using System.Collections.Generic;

namespace Cement.WebUI.Navigation.Group
{
    public class Accounting : INavigationGroup
    {
        public NavigationLevel1 GetGroup()
        {
            var root = new NavigationLevel1
            {
                Name = "Бухгалтерия",
                Order = 90,
                Permission = "Accounting"
            };

            //root.Items.AddRange(new List<NavigationLevel2>
            //{
            //    new NavigationLevel2
            //    {
            //        Name = "Документы",
            //        Master = "Cement.view.accounting.documents.Total",
            //        Detail = "Cement.view.accounting.documents.view.View",
            //        Permission = root.ChildPermission("Documents"),
            //        Items = null
            //    },
            //    new NavigationLevel2
            //    {
            //        Name = "Индикаторы",
            //        Master = "Cement.view.accounting.indicators.Total",
            //        Detail = "Cement.view.accounting.indicators.view.View",
            //        Permission = root.ChildPermission("Indicators"),
            //        Items = null
            //    }
            //});

            AddReceiptexpense(root);
            AddOther(root);

            return root;
        }

        private void AddOther(NavigationLevel1 root)
        {
            var data = new NavigationLevel2
            {
                Name = "Иные документы",
                Master = "Cement.view.accounting.other.Total",
                Detail = "Cement.view.accounting.other.view.View",
                Permission = root.ChildPermission("Other")
            };

            data.Items.Add(new NavigationLevel3
            {
                Name = "Новые",
                Permission = data.ChildPermission("New"),
                Xtype = "accounting_other_complex_new"
            });

            data.Items.Add(new NavigationLevel3
            {
                Name = "Входящие",
                Permission = data.ChildPermission("Incoming"),
                Xtype = "accounting_other_complex_incoming"
            });

            data.Items.Add(new NavigationLevel3
            {
                Name = "Исходящие",
                Permission = data.ChildPermission("Outcoming"),
                Xtype = "accounting_other_complex_outcoming"
            });

            data.Items.Add(new NavigationLevel3
            {
                Name = "Архив",
                Permission = data.ChildPermission("Archive"),
                Xtype = "accounting_other_complex_archive"
            });

            root.Items.Add(data);
        }

        private void AddReceiptexpense(NavigationLevel1 root)
        {
            var data = new NavigationLevel2
            {
                Name = "Документы (приход/расход)",
                Master = "Cement.view.accounting.receiptexpense.Total",
                Detail = "Cement.view.accounting.receiptexpense.view.View",
                Permission = root.ChildPermission("Receiptexpense")
            };

            data.Items.Add(new NavigationLevel3
            {
                Name = "Новые",
                Permission = data.ChildPermission("New"),
                Xtype = "accounting_receiptexpense_complex_new"
            });

            data.Items.Add(new NavigationLevel3
            {
                Name = "Входящие",
                Permission = data.ChildPermission("Incoming"),
                Xtype = "accounting_receiptexpense_complex_incoming"
            });

            data.Items.Add(new NavigationLevel3
            {
                Name = "Исходящие",
                Permission = data.ChildPermission("Outcoming"),
                Xtype = "accounting_receiptexpense_complex_outcoming"
            });

            data.Items.Add(new NavigationLevel3
            {
                Name = "Архив",
                Permission = data.ChildPermission("Archive"),
                Xtype = "accounting_receiptexpense_complex_archive"
            });

            root.Items.Add(data);
        }
    }
}