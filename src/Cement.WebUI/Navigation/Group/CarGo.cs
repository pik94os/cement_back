﻿using System.Collections.Generic;

namespace Cement.WebUI.Navigation.Group
{
    public class CarGo : INavigationGroup
    {
        public NavigationLevel1 GetGroup()
        {
            var root = new NavigationLevel1
            {
                Name = "Логистика",
                Order = 80,
                Permission = "CarGo"
            };

            root.Items.AddRange(new List<NavigationLevel2>
            {
                new NavigationLevel2
                {
                    Name = "Лот на груз",
                    Master = "Cement.view.cargo.goods_lot.Total",
                    Detail = "Cement.view.cargo.goods_lot.view.View",
                    Permission = root.ChildPermission("GoodsLot"),
                    Items = null
                },
                new NavigationLevel2
                {
                    Name = "Лот на транспорт",
                    Master = "Cement.view.cargo.transport_lot.Total",
                    Detail = "Cement.view.cargo.goods_lot.view.View",
                    Permission = root.ChildPermission("TransportLot"),
                    Items = null
                },
                new NavigationLevel2
                {
                    Name = "Биржа грузов",
                    Master = "Cement.view.cargo.goods_exchange.Total",
                    Permission = root.ChildPermission("GoodsExchange"),
                    Items = null
                },
                new NavigationLevel2
                {
                    Name = "Биржа транспорта",
                    Master = "Cement.view.cargo.transport_exchange.Total",
                    Permission = root.ChildPermission("TransportExchange"),
                    Items = null
                },
                new NavigationLevel2
                {
                    Name = "Участники",
                    Master = "Cement.view.cargo.participants.Total",
                    Permission = root.ChildPermission("Participants"),
                    Items = null
                }
            });

            return root;
        }
    }
}