﻿namespace Cement.WebUI.Navigation
{
    public interface INavigationGroup
    {
        NavigationLevel1 GetGroup();
    }
}