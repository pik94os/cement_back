﻿using System.Collections.Generic;
using Cement.Core.Auth;

namespace Cement.WebUI.Navigation
{
    public interface INavigationProvider
    {
        List<NavigationLevel1> GetNavigation(IUserPrincipal userPrincipal);

        List<NavigationLevel1> GetFullNavigation();

        List<string> GetPermissionNameList();
    }
}