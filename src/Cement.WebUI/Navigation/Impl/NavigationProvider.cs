﻿using System.Collections.Generic;
using System.Linq;
using Castle.Windsor;
using Cement.Core.Auth;
using Cement.Core.Entities;
using Cement.Core.Enums;
using Cement.Core.Extensions;
using Cement.Core.Services.EntityService;

namespace Cement.WebUI.Navigation.Impl
{
    public class NavigationProvider : INavigationProvider
    {
        public IWindsorContainer Container { get; set; }

        public IEntityService<UserPermission> UserPermissionEntityService { get; set; }

        public List<NavigationLevel1> GetNavigation(IUserPrincipal userPrincipal)
        {
            var groups = Container.ResolveAll<INavigationGroup>();

            var navigation = groups.Select(x => x.GetGroup()).OrderBy(x => x.Order).ToList();

            var permissions = new List<string>();

            foreach (var navigationLevel1 in navigation)
            {
                permissions.Add(navigationLevel1.Permission);
                
                if (navigationLevel1.Items != null)
                foreach (var navigationLevel2 in navigationLevel1.Items)
                {
                    permissions.Add(navigationLevel2.Permission);

                    if (navigationLevel2.Items != null)
                    foreach (var navigationLevel3 in navigationLevel2.Items)
                    {
                        permissions.Add(navigationLevel3.Permission);
                    }
                }
            }

            // Чтобы вылетало исключение при дублирующих или null permission'ах
            permissions.ToDictionary(x => x);

#if DEBUG
            TraceNavigationData(navigation);
#endif

            var userPermissionsDict = UserPermissionEntityService.GetAll()
                .Where(x => x.User.Id == userPrincipal.UserId)
                .Select(x => new
                {
                    x.Permission,
                    x.AccessType
                })
                .ToDictionary(x => x.Permission, x => x.AccessType);

            var result = new List<NavigationLevel1>();

            foreach (var navigationLevel1 in navigation)
            {
                var items = new List<NavigationLevel2>();

                if (navigationLevel1.Items != null)
                foreach (var navigationLevel2 in navigationLevel1.Items)
                {
                    var accessType = userPermissionsDict.Get(navigationLevel2.Permission, AccessType.Denied);

                    if (userPrincipal.IsInRole(UserRole.Employee))
                    {
                        if (accessType != AccessType.Denied && navigationLevel2.Role.HasFlag(UserRole.Employee))
                        {
                            navigationLevel2.AccessType = accessType;
                            if (navigationLevel2.Items != null)
                            {
                                navigationLevel2.Items.ForEach(x => x.AccessType = accessType);
                            }

                            items.Add(navigationLevel2);
                        }

                    }
                    else if (userPrincipal.IsInRole(UserRole.Admin) && navigationLevel2.Role.HasFlag(UserRole.Admin))
                    {
                        navigationLevel2.AccessType = AccessType.ReadWrite;
                        if (navigationLevel2.Items != null)
                        {
                            navigationLevel2.Items.ForEach(x => x.AccessType = AccessType.ReadWrite);
                        }

                        items.Add(navigationLevel2);
                    }
                    else if (userPrincipal.IsInRole(UserRole.Root) && navigationLevel2.Role.HasFlag(UserRole.Root))
                    {
                        navigationLevel2.AccessType = AccessType.ReadWrite;
                        if (navigationLevel2.Items != null)
                        {
                            navigationLevel2.Items.ForEach(x => x.AccessType = AccessType.ReadWrite);
                        }

                        items.Add(navigationLevel2);
                    }
                }

                if (items.Any())
                {
                    result.Add(new NavigationLevel1
                    {
                        Name = navigationLevel1.Name,
                        Items = items
                    });
                }
            }

            return result;
        }

        public List<NavigationLevel1> GetFullNavigation()
        {
            var groups = Container.ResolveAll<INavigationGroup>();

            var navigation = groups.Select(x => x.GetGroup()).OrderBy(x => x.Order).ToList();

            return navigation;
        }

        public List<string> GetPermissionNameList()
        {
            var groups = Container.ResolveAll<INavigationGroup>();

            var navigation = groups.Select(x => x.GetGroup()).OrderBy(x => x.Order).ToList();

            var permissions = navigation
                .Where(x => x.Items != null)
                .SelectMany(x => x.Items)
                .Select(x => x.Permission)
                .ToList();

            return permissions;
        }

        private void TraceNavigationData(List<NavigationLevel1> navigation)
        {
            System.Diagnostics.Trace.WriteLine("Model Fields:");
            foreach (var navigationLevel1 in navigation)
            {
                if (navigationLevel1.Items != null)
                foreach (var navigationLevel2 in navigationLevel1.Items)
                    {
                        System.Diagnostics.Trace.WriteLine(navigationLevel2.Permission);
                    }
            }

            var shortNameDict = new Dictionary<string, string>();

            shortNameDict["Внутренние документы"] = "В";
            shortNameDict["Корреспонденция"] = "Кр";
            shortNameDict["Календарь"] = "Кл";
            shortNameDict["Сообщения"] = "С";
            shortNameDict["Объявления"] = "О";
            shortNameDict["Реквизиты"] = "Рк";
            shortNameDict["Реквизиты счетов"] = "Рс";
            shortNameDict["Номенклатура дел"] = "Н";
            shortNameDict["Склады"] = "Ск";
            shortNameDict["Сотрудники"] = "Ст";
            shortNameDict["Подписи"] = "П";
            shortNameDict["Администрирование"] = "А";
            shortNameDict["Автомобильный транспорт"] = "Ат";
            shortNameDict["Управление АТС"] = "У";
            shortNameDict["Транспортные единицы"] = "ТЕ";
            shortNameDict["Табель учета"] = "Ту";
            shortNameDict["Товары"] = "Т";
            shortNameDict["Услуги"] = "У";
            shortNameDict["Прайс-лист"] = "Пр";
            shortNameDict["Тип упаковки"] = "Ту";
            shortNameDict["Клиенты"] = "Кл";
            shortNameDict["Управление клиентами"] = "У";
            shortNameDict["Группы/подгруппы"] = "Гр";
            shortNameDict["Статус"] = "Ст";
            shortNameDict["Договор"] = "Д";
            shortNameDict["Приложения"] = "П";
            shortNameDict["Каталог"] = "К";
            shortNameDict["Заявки на товар"] = "Зт";
            shortNameDict["Заявки на услуги"] = "Зу";
            shortNameDict["Лот на груз"] = "ЛГ";
            shortNameDict["Лот на транспорт"] = "ЛТ";
            shortNameDict["Биржа грузов"] = "БГ";
            shortNameDict["Биржа транспорта"] = "БТ";
            shortNameDict["Участники"] = "У";
            shortNameDict["Документы"] = "Д";
            shortNameDict["Индикаторы"] = "И";
            shortNameDict["Внутренние документы"] = "В";
            shortNameDict["Корреспонденция"] = "К";
            shortNameDict["Приказы"] = "П";

            System.Diagnostics.Trace.WriteLine("Grid Columns:");

            foreach (var navigationLevel1 in navigation)
            {
                var str = string.Format("{{ \n\ttext: '{0}', \n\tcolumns: [", navigationLevel1.Name);

                System.Diagnostics.Trace.WriteLine(str);
                
                if (navigationLevel1.Items != null)
                foreach (var navigationLevel2 in navigationLevel1.Items)
                {
                    var s2 = string.Format("\t\t{{ text: '{0}', dataIndex: '{1}' }},", shortNameDict.Get(navigationLevel2.Name), navigationLevel2.Permission);
                    System.Diagnostics.Trace.WriteLine(s2);
                }

                System.Diagnostics.Trace.WriteLine("\t]\n},");
            }
        }
    }
}