﻿using System.Collections.Specialized;
using System.IO;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using Cement.Core.Extensions;
using Cement.Core.Services.StoreParams;

namespace Cement.WebUI.Extensions
{
    public static class RequestExtension
    {
        public static StoreParams GetStoreParams(this HttpRequestMessage request)
        {
            if (request.Content.IsFormData())
            {
                var formData = request.Content.ReadAsFormDataAsync().Result;

                var rawStoreParams = Core.Extensions.RequestExtension.ProcessFormData<RawStoreParams>(formData);

                return rawStoreParams.ToStoreParams();
            }

            return new StoreParams();
        }

        public static NameValueCollection ToNameValueCollection(this HttpRequestMessage request)
        {
            if (request.Content.IsFormData())
            {
                return request.Content.ReadAsFormDataAsync().Result;
            }
            
            if (request.Content.IsMimeMultipartContent())
            {
                var root = Path.GetTempPath();

                var provider = new MultipartFormDataStreamProvider(root);

                Task.Factory
                    .StartNew(() =>
                        request.Content.ReadAsMultipartAsync(provider).Wait(),
                        CancellationToken.None,
                        TaskCreationOptions.LongRunning,
                        TaskScheduler.Default)
                    .Wait();

                return provider.FormData;
            }

            return new NameValueCollection();
        }

        public static T ToType<T>(this HttpRequestMessage request) where T: class, new()
        {
            return request.ConvertToType<T>();
        }
    }
}