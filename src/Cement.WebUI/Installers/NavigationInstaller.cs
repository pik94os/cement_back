﻿using Castle.MicroKernel.Registration;
using Castle.MicroKernel.SubSystems.Configuration;
using Castle.Windsor;
using Cement.WebUI.Navigation;
using Cement.WebUI.Navigation.Group;
using Cement.WebUI.Navigation.Impl;

namespace Cement.WebUI.Installers
{
    public class NavigationInstaller : IWindsorInstaller
    {
        public void Install(IWindsorContainer container, IConfigurationStore store)
        {
            container.Register(Component.For<INavigationProvider>().ImplementedBy<NavigationProvider>());

            container.Register(Component.For<INavigationGroup>().ImplementedBy<Personal>());
            container.Register(Component.For<INavigationGroup>().ImplementedBy<Corporate>());
            container.Register(Component.For<INavigationGroup>().ImplementedBy<Transport>());
            container.Register(Component.For<INavigationGroup>().ImplementedBy<Products>());
            container.Register(Component.For<INavigationGroup>().ImplementedBy<Clients>());
            container.Register(Component.For<INavigationGroup>().ImplementedBy<Contracts>());
            container.Register(Component.For<INavigationGroup>().ImplementedBy<Goods>());
            container.Register(Component.For<INavigationGroup>().ImplementedBy<CarGo>());
            container.Register(Component.For<INavigationGroup>().ImplementedBy<Warehouse>());
            container.Register(Component.For<INavigationGroup>().ImplementedBy<Accounting>());
            container.Register(Component.For<INavigationGroup>().ImplementedBy<OfficeWork>());
            container.Register(Component.For<INavigationGroup>().ImplementedBy<Administration>());
            
        }
    }
}