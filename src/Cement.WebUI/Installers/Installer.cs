﻿using Castle.MicroKernel.Registration;
using Castle.MicroKernel.SubSystems.Configuration;
using Castle.Windsor;
using Cement.Core.Services;
using Cement.WebUI.Converter;
using DataAccess.Batching;

namespace Cement.WebUI.Installers
{
    public class Installer : IWindsorInstaller
    {
        public void Install(IWindsorContainer container, IConfigurationStore store)
        {
            container.Register(Component.For<IProxyToEntityConverter>().ImplementedBy<ProxyToEntityConverter>());
            container.Register(Component.For<IBatcherApplier>().ImplementedBy<DataAccessExtensions.PostgresBatcher>());
        }
    }
}