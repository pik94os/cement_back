﻿using System;
using System.Configuration;
using System.Security.Principal;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using System.Web.Http;
using Castle.MicroKernel.Registration;
using Castle.MicroKernel.Resolvers.SpecializedResolvers;
using Castle.Windsor;
using Castle.Windsor.Installer;
using Cement.Core.Auth;
using Cement.Core.Auth.Impl;
using Cement.Core.Extensions;
using Cement.WebUI.Installers;
using Newtonsoft.Json.Serialization;

namespace Cement.WebUI
{
    public class Global : HttpApplication
    {
        private static IWindsorContainer _container;

        void Application_Start(object sender, EventArgs e)
        {
#if DEBUG
            log4net.Config.XmlConfigurator.Configure();
#endif

            ConfigureWindsor(GlobalConfiguration.Configuration);
            // Code that runs on application startup
            AreaRegistration.RegisterAllAreas();
            GlobalConfiguration.Configure(WebApiConfig.Register);
            RouteConfig.RegisterRoutes(RouteTable.Routes);

            WebApiConfig.SetFilters(GlobalConfiguration.Configuration.Filters);
            WebApiConfig.SetFormatters(GlobalConfiguration.Configuration.Formatters);

            BundleConfig.RegisterBundles(BundleTable.Bundles);

            GlobalConfiguration.Configuration.Formatters.Remove(GlobalConfiguration.Configuration.Formatters.XmlFormatter);
        }

        public static void ConfigureWindsor(HttpConfiguration configuration)
        {
            _container = new WindsorContainer();
            _container.Install(FromAssembly.This());
            _container.Kernel.Resolver.AddSubResolver(new CollectionResolver(_container.Kernel, true));
            _container.Register(Component.For<IWindsorContainer>().Instance(_container));
            var dependencyResolver = new WindsorDependencyResolver(_container);
            configuration.DependencyResolver = dependencyResolver;

            var controllerFactory = new WindsorControllerFactory(_container.Kernel);
            ControllerBuilder.Current.SetControllerFactory(controllerFactory);
            
            var connStr = GetConnString(false);

            var configProvider = new DataAccess.Config.ConfigProvider(ConfigurationManager.AppSettings, connStr, HttpRuntime.AppDomainAppPath);
            _container.Register(Component.For<DataAccess.Config.ConfigProvider>().Instance(configProvider));

            _container.Register(Component.For<IUserPrincipal>()
                .UsingFactoryMethod(() =>
                {
                    if (HttpContext.Current == null)
                    {
                        return new NotAuthorizedPrincipal();
                    }

                    return HttpContext.Current.User as IUserPrincipal ?? new NotAuthorizedPrincipal();
                })
                .LifestyleTransient());

            _container.Install(new DataAccess.ModuleInstaller());
            _container.Install(new Core.ModuleInstaller());
            _container.Install(new Protocol.ModuleInstaller());

            ManageRootUser();
        }

        protected static void ManageRootUser()
        {
            var systemUserPassword = ConfigurationManager.AppSettings["SystemUserPassword"];
            var systemUserIsEnabled = ConfigurationManager.AppSettings["SystemUserIsEnabled"].ToBool();

            _container.Resolve<IRootUserManager>().ManageRootUser(systemUserIsEnabled, systemUserPassword);
        }

        public static string GetConnString(bool externCall = true)
        {
            const string dbKey = "Main";

            if (!externCall)
            {
                return System.Configuration.ConfigurationManager.ConnectionStrings[dbKey].ConnectionString;
            }

            try
            {
                var configFileMap = new ExeConfigurationFileMap { ExeConfigFilename = "../../../Cement.WebUI/web.config" };

                var config = ConfigurationManager.OpenMappedExeConfiguration(configFileMap, ConfigurationUserLevel.None);

                return config.ConnectionStrings.ConnectionStrings[dbKey].ConnectionString;
            }
            catch (Exception)
            {
                return string.Empty;
            }
        }
    }
}