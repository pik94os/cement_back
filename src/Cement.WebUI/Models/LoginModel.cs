﻿using System.ComponentModel.DataAnnotations;

namespace Cement.WebUI.Models
{
    public class LoginModel
    {
        [Required]
        public string Login { get; set; }

        [Required]
        [DataType(DataType.Password)]
        public string Password { get; set; }
    }

    public class ConfirmModel
    {
        [Required]
        public string Code { get; set; }

        public string ReturnUrl { get; set; }

        public bool IsRemember { get; set; }
    }
}