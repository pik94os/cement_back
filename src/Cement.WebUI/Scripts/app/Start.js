Ext.require([
    'Ext.layout.container.Accordion',
    'Ext.state.LocalStorageProvider',
    'Ext.layout.container.Anchor',
    'Ext.layout.container.Column',
    'Ext.form.ComboBox',
    'Ext.form.FieldContainer',
    'Ext.form.FieldSet',
    'Ext.form.field.Hidden',
    'Ext.form.field.Base',
    'Ext.form.field.File',
    'Ext.form.field.Date',
    'Ext.form.field.HtmlEditor',
    'Ext.form.RadioGroup',
    'Ext.data.proxy.Rest',
    'Ext.data.TreeStore',
    'Ext.form.field.ComboBox',
    'Ext.grid.ColumnLayout',
    'Ext.grid.Panel',
    'Ext.grid.feature.Grouping',
    'Ext.grid.plugin.RowExpander',
    'Ext.util.MixedCollection',
    'Ext.chart.*'
]);

CondExpr = {
    operands: {
        //and: 'and',
        //or: 'or',
        eq: '_eql',
        neq: '_not_eql',
        gt: '_much',
        lt: '_less',
        gte: '_much_or_eql',
        lte: '_less_or_eql',
        contains: '_contains',
        start: '_starts_with',
        end: '_ends_with'
    }
};

Ext.define('Ext.ux.TreePicker', {
    extend: 'Ext.form.field.Picker',
    xtype: 'treepicker',

    uses: [
        'Ext.tree.Panel'
    ],

    triggerCls: Ext.baseCSSPrefix + 'form-arrow-trigger',

    config: {   
        /**
         * @cfg {Ext.data.TreeStore} store
         * A tree store that the tree picker will be bound to
         */
        store: null,

        /**
         * @cfg {String} displayField
         * The field inside the model that will be used as the node's text.
         * Defaults to the default value of {@link Ext.tree.Panel}'s `displayField` configuration.
         */
        displayField: null,

        /**
         * @cfg {Array} columns
         * An optional array of columns for multi-column trees
         */
        columns: null,

        /**
         * @cfg {Boolean} selectOnTab
         * Whether the Tab key should select the currently highlighted item. Defaults to `true`.
         */
        selectOnTab: true,

        /**
         * @cfg {Number} maxPickerHeight
         * The maximum height of the tree dropdown. Defaults to 300.
         */
        maxPickerHeight: 300,

        /**
         * @cfg {Number} minPickerHeight
         * The minimum height of the tree dropdown. Defaults to 100.
         */
        minPickerHeight: 300
    },

    editable: false,

    initComponent: function () {
        var me = this;
        me.callParent(arguments);

        me.addEvents(
            /**
             * @event select
             * Fires when a tree node is selected
             * @param {Ext.ux.TreePicker} picker        This tree picker
             * @param {Ext.data.Model} record           The selected record
             */
            'select'
        );

        me.mon(me.store, {
            scope: me,
            load: me.onLoad,
            update: me.onUpdate
        });
    },

    /**
     * Creates and returns the tree panel to be used as this field's picker.
     */
    createPicker: function () {
        var me = this,
            picker = new Ext.tree.Panel({
                shrinkWrapDock: 2,
                store: me.store,
                floating: true,
                displayField: me.displayField,
                columns: me.columns,
                minHeight: me.minPickerHeight,
                maxHeight: me.maxPickerHeight,
                rootVisible: me.rootVisible,
                manageHeight: false,
                shadow: false,
                listeners: {
                    scope: me,
                    itemclick: me.onItemClick
                },
                viewConfig: {
                    listeners: {
                        scope: me,
                        render: me.onViewRender
                    }
                }
            }),
            view = picker.getView();

        if (Ext.isIE9 && Ext.isStrict) {
            // In IE9 strict mode, the tree view grows by the height of the horizontal scroll bar when the items are highlighted or unhighlighted.
            // Also when items are collapsed or expanded the height of the view is off. Forcing a repaint fixes the problem.
            view.on({
                scope: me,
                highlightitem: me.repaintPickerView,
                unhighlightitem: me.repaintPickerView,
                afteritemexpand: me.repaintPickerView,
                afteritemcollapse: me.repaintPickerView
            });
        }
        return picker;
    },

    onViewRender: function (view) {
        view.getEl().on('keypress', this.onPickerKeypress, this);
    },

    /**
     * repaints the tree view
     */
    repaintPickerView: function () {
        var style = this.picker.getView().getEl().dom.style;

        // can't use Element.repaint because it contains a setTimeout, which results in a flicker effect
        style.display = style.display;
    },

    /**
     * Aligns the picker to the input element
     */
    alignPicker: function () {
        var me = this,
            picker;

        if (me.isExpanded) {
            picker = me.getPicker();
            if (me.matchFieldWidth) {
                // Auto the height (it will be constrained by max height)
                picker.setWidth(me.bodyEl.getWidth());
            }
            if (picker.isFloating()) {
                me.doAlign();
            }
        }
    },

    /**
     * Handles a click even on a tree node
     * @private
     * @param {Ext.tree.View} view
     * @param {Ext.data.Model} record
     * @param {HTMLElement} node
     * @param {Number} rowIndex
     * @param {Ext.EventObject} e
     */
    onItemClick: function (view, record, node, rowIndex, e) {
        this.selectItem(record);
    },

    /**
     * Handles a keypress event on the picker element
     * @private
     * @param {Ext.EventObject} e
     * @param {HTMLElement} el
     */
    onPickerKeypress: function (e, el) {
        var key = e.getKey();

        if (key === e.ENTER || (key === e.TAB && this.selectOnTab)) {
            this.selectItem(this.picker.getSelectionModel().getSelection()[0]);
        }
    },

    /**
     * Changes the selection to a given record and closes the picker
     * @private
     * @param {Ext.data.Model} record
     */
    selectItem: function (record) {
        var me = this;
        me.setValue(record.getId());
        me.picker.hide();
        me.inputEl.focus();
        me.fireEvent('select', me, record)

    },

    /**
     * Runs when the picker is expanded.  Selects the appropriate tree node based on the value of the input element,
     * and focuses the picker so that keyboard navigation will work.
     * @private
     */
    onExpand: function () {
        var me = this,
            picker = me.picker,
            store = picker.store,
            value = me.value,
            node;


        if (value) {
            node = store.getNodeById(value);
        }

        if (!node) {
            node = store.getRootNode();
        }

        picker.selectPath(node.getPath());

        Ext.defer(function () {
            picker.getView().focus();
        }, 1);
    },

    /**
     * Sets the specified value into the field
     * @param {Mixed} value
     * @return {Ext.ux.TreePicker} this
     */
    setValue: function (value) {
        var me = this,
            record;

        me.value = value;

        if (me.store.loading) {
            // Called while the Store is loading. Ensure it is processed by the onLoad method.
            return me;
        }

        // try to find a record in the store that matches the value
        record = value ? me.store.getNodeById(value) : me.store.getRootNode();
        if (value === undefined) {
            record = me.store.getRootNode();
            me.value = record.getId();
        } else {
            record = me.store.getNodeById(value);
        }

        // set the raw value to the record's display field if a record was found
        me.setRawValue(record ? record.get(me.displayField) : '');

        return me;
    },

    getSubmitValue: function () {
        return this.value;
    },

    /**
     * Returns the current data value of the field (the idProperty of the record)
     * @return {Number}
     */
    getValue: function () {
        return this.value;
    },

    /**
     * Handles the store's load event.
     * @private
     */
    onLoad: function () {
        var value = this.value;

        if (value) {
            this.setValue(value);
        }
    },

    onUpdate: function (store, rec, type, modifiedFieldNames) {
        var display = this.displayField;

        if (type === 'edit' && modifiedFieldNames && Ext.Array.contains(modifiedFieldNames, display) && this.value === rec.getId()) {
            this.setRawValue(rec.get(display));
        }
    }

});
Ext.define('Ext.ux.IconCombo', {
    extend: 'Ext.form.ComboBox',
    //alias: 'widget.iconcombo',
    xtype: 'iconcombo',
    initComponent: function () {

        Ext.apply(this, {
            tpl: '<tpl for=".">'
                + '<div class="{' + this.iconClsField + '}"></div>'
                + '{' + this.displayField + '}'
                + '</tpl>'
        });

        // call parent initComponent
        Ext.ux.IconCombo.superclass.initComponent.call(this);

    }, // end of function initComponent

    onRender: function (ct, position) {
        // call parent onRender
        Ext.ux.IconCombo.superclass.onRender.call(this, ct, position);

        // adjust styles
        //this.wrap.applyStyles({ position: 'relative' });
        this.el.addClass('ux-icon-combo-input');

        // add div for icon
        this.icon = Ext.DomHelper.append(this.el.up('div.x-form-field-wrap'), {
            tag: 'div', style: 'position:absolute'
        });
    }, // end of function onRender

    setIconCls: function () {
        //var rec = this.store.query(this.valueField, this.getValue()).itemAt(0);
        //if (rec) {
        //    this.icon.className = 'ux-icon-combo-icon ' + rec.get(this.iconClsField);
        //}
    }, // end of function setIconCls

    setValue: function (value) {
        Ext.ux.IconCombo.superclass.setValue.call(this, value);
        this.setIconCls();
    } // end of function setValue
});

Ext.define('Cement.Creators', {
    singleton: true,

    corporate: {
        text: 'Корпроративные данные',
        leaf: false,
        treeId: 'corporate',
        expanded: true,
        children: [
            {
                text: 'Реквизиты счета',
                xtype: 'bankaccountform',
                model: 'Cement.model.BankAccount',
                master: 'Cement.view.corporate.bank_account.List',
                topLevel: 'corporate',
                sidebarCollapsed: true,
                leaf: true
            }, {
                text: 'Номенклатура дел',
                xtype: 'nomeklature_form',
                model: 'Cement.model.Nomenklature',
                master: 'Cement.view.corporate.nomenklature.List',
                topLevel: 'corporate',
                sidebarCollapsed: true,
                leaf: true
            }, {
                text: 'Сотрудник',
                xtype: 'new_employeeform',
                model: 'Cement.model.Employee',
                master: 'Cement.view.corporate.employee.List',
                topLevel: 'corporate',
                sidebarCollapsed: true,
                leaf: true
            }, {
                text: 'Подпись',
                xtype: 'new_signatureform',
                model: 'Cement.model.Signature',
                master: 'Cement.view.corporate.signature.List',
                topLevel: 'corporate',
                sidebarCollapsed: true,
                leaf: true
            }, {
                text: 'Администрирование',
                xtype: 'administrationform',
                model: 'Cement.model.Permission',
                master: 'Cement.view.corporate.administration.List',
                topLevel: 'corporate',
                sidebarCollapsed: true,
                leaf: true,
                removePadding: true
            }
        ]
    },

    transport: {
        text: 'Транспорт',
        treeId: 'transport',
        leaf: false,
        expanded: true,
        children: [
            {
                text: 'Автомобильное ТС',
                leaf: false,
                children: [
                    {
                        text: 'Транспортное средство',
                        xtype: 'transport_auto_form',
                        topLevel: 'transport',
                        model: 'Cement.model.transport.Auto',
                        master: 'Cement.view.transport.auto.Complex#Cement.view.transport.auto.Owned',
                        sidebarCollapsed: true,
                        leaf: true
                    }, {
                        text: 'Передать в аренду',
                        xtype: 'transport_auto_rent_form',
                        topLevel: 'transport',
                        model: 'Cement.model.transport.Rent',
                        master: 'Cement.view.transport.auto.Rent',
                        sidebarCollapsed: true,
                        leaf: true
                    }
                ]
            },
            // , {
            // 	name: 'Контейнер',
            // 	leaf: false,
            // 	items: [{
            // 		name: 'Контейнер',
            // 		xtype: 'new_trucksform',
            // 		master: 'Cement.view.truck.List'
            // 	}]
            // }
            {
                text: 'Управление транспортом',
                leaf: false,
                children: [
                    {
                        text: 'Техобслуживание',
                        xtype: 'transport_auto_control_form_maintenance',
                        model: 'Cement.model.transport.Maintenance',
                        topLevel: 'transport',
                        master: 'Cement.view.transport.auto_control.Maintenance',
                        sidebarCollapsed: true,
                        leaf: true
                    }, {
                        text: 'Регламент',
                        xtype: 'transport_auto_control_form_regulation',
                        model: 'Cement.model.transport.Regulation',
                        topLevel: 'transport',
                        master: 'Cement.view.transport.auto_control.Regulation',
                        sidebarCollapsed: true,
                        leaf: true
                    }, {
                        text: 'Запчасти/услуги',
                        xtype: 'transport_auto_control_form_part',
                        model: 'Cement.model.transport.Part',
                        topLevel: 'transport',
                        master: 'Cement.view.transport.auto_control.Part',
                        sidebarCollapsed: true,
                        leaf: true
                    }, {
                        text: 'Топливо',
                        xtype: 'transport_auto_control_form_fuel',
                        model: 'Cement.model.transport.Fuel',
                        topLevel: 'transport',
                        master: 'Cement.view.transport.auto_control.Fuel',
                        sidebarCollapsed: true,
                        leaf: true
                    }, {
                        text: 'Пробег',
                        xtype: 'transport_auto_control_form_odometer',
                        model: 'Cement.model.transport.Odometer',
                        topLevel: 'transport',
                        master: 'Cement.view.transport.auto_control.Odometer',
                        sidebarCollapsed: true,
                        leaf: true
                    }, {
                        text: 'Страховки',
                        xtype: 'transport_auto_control_form_insurance',
                        model: 'Cement.model.transport.Insurance',
                        topLevel: 'transport',
                        master: 'Cement.view.transport.auto_control.Insurance',
                        sidebarCollapsed: true,
                        leaf: true
                    }, {
                        text: 'ДТП',
                        xtype: 'transport_auto_control_form_crash',
                        model: 'Cement.model.transport.Crash',
                        topLevel: 'transport',
                        master: 'Cement.view.transport.auto_control.Crash',
                        sidebarCollapsed: true,
                        leaf: true
                    }
                ]
            }, {
                text: 'Транспортные единицы',
                leaf: false,
                children: [
                    {
                        text: 'Транспортная единица',
                        xtype: 'transport_units_form',
                        model: 'Cement.view.transport.Unit',
                        topLevel: 'transport',
                        master: 'Cement.view.transport.units.lists.New',
                        sidebarCollapsed: true,
                        leaf: true
                    }, {
                        text: 'Передать в управление',
                        xtype: 'transport_units_rent_form',
                        topLevel: 'transport',
                        model: 'Cement.view.transport.RentUnit',
                        master: 'Cement.view.transport.units.lists.SendToRent',
                        sidebarCollapsed: true,
                        leaf: true
                    }
                ]
            }, {
                text: 'Транспортное средство ЖД',
                leaf: false,
                children: [
                    {
                        text: 'Транспортное средство',
                        xtype: 'transport_railway_form',
                        topLevel: 'transport',
                        model: 'Cement.model.transport.Railway',
                        master: 'Cement.view.transport.railway.Owned',
                        sidebarCollapsed: true,
                        leaf: true
                    }, {
                        text: 'Передать в аренду',
                        xtype: 'transport_railway_rent_form',
                        topLevel: 'transport',
                        model: 'Cement.model.transport.RailwayRent',
                        master: 'Cement.view.transport.railway.Rent',
                        sidebarCollapsed: true,
                        leaf: true
                    }
                ]
            },
            {
                text: 'Табель учёта',
                xtype: 'transport_timesheet_form',
                model: 'Cement.model.transport.Timesheet',
                topLevel: 'transport',
                master: 'Cement.view.transport.timesheet.Total',
                sidebarCollapsed: true,
                leaf: true,
                layout: 'fit',
                removePadding: true
            }]
    },

    products: {
        text: 'Товары (услуги)',
        leaf: false,
        expanded: true,
        treeId: 'products',
        children: [
            {
                text: 'Товар',
                xtype: 'products_products_form',
                model: 'Cement.model.products.Product',
                topLevel: 'products',
                master: 'Cement.view.products.products.Total',
                sidebarCollapsed: true,
                leaf: true
            }, {
                text: 'Услуга',
                xtype: 'products_services_form',
                model: 'Cement.model.products.Service',
                topLevel: 'products',
                master: 'Cement.view.products.services.Total',
                sidebarCollapsed: true,
                leaf: true
            }, {
                text: 'Прайс-лист',
                xtype: 'products_price_form',
                model: 'Cement.model.products.PriceList',
                topLevel: 'products',
                master: 'Cement.view.products.price.Total',
                sidebarCollapsed: true,
                leaf: true
            }
        ]
    },

    clients: {
        text: 'Клиенты',
        treeId: 'clients',
        expanded: true,
        leaf: false,
        children: [
            {
                text: 'Клиент',
                xtype: 'clientform',
                model: 'Cement.model.Client',
                topLevel: 'clients',
                master: 'Cement.view.clients.clients.Total#Cement.view.clients.current.list.Clients',
                sidebarCollapsed: true,
                leaf: true
            }, {
                text: 'Управление клиентом',
                xtype: 'clients_management_form',
                model: 'Cement.model.clients.Management',
                topLevel: 'clients',
                master: 'Cement.view.clients.management.Total',
                sidebarCollapsed: true,
                leaf: true
            }, {
                text: 'Статус',
                xtype: 'clients_statuses_form',
                model: 'Cement.model.clients.Status',
                topLevel: 'clients',
                master: 'Cement.view.clients.statuses.list.Statuses',
                sidebarCollapsed: true,
                leaf: true
            }, {
                text: 'Группа',
                xtype: 'clients_groups_form_group',
                model: 'Cement.model.clients.Group',
                topLevel: 'clients',
                master: 'Cement.view.clients.groups.Total#tab-0',
                sidebarCollapsed: true,
                leaf: true
            }, {
                text: 'Подгруппа',
                xtype: 'clients_groups_form_sub_group',
                model: 'Cement.model.clients.SubGroup',
                topLevel: 'clients',
                master: 'Cement.view.clients.groups.Total#tab-1',
                sidebarCollapsed: true,
                leaf: true

            }
        ]
    },

    goods: {
        text: 'Товарооборот',
        treeId: 'goods',
        expanded: true,
        leaf: false,
        children: [
        //{
        //    text: 'Заявка на товар (Авто)',
        //    xtype: 'goods_auto_form_form',
        //    topLevel: 'goods',
        //    model: 'Cement.model.goods.Auto',
        //    master: 'Cement.view.goods.auto.Total',
        //    sidebarCollapsed: true,
        //    leaf: true
        //},
        {
            text: 'Заявка на товар (АВТО)',
            xtype: 'goods_auto_form_form',
            topLevel: 'goods',
            model: 'Cement.model.goods.Auto',
            master: 'Cement.view.goods.auto.Total',
            sidebarCollapsed: true,
            leaf: true
        },
        {
            text: 'Заявка на товар (ЖД)',
            xtype: 'goods_railway_form_form',
            topLevel: 'goods',
            model: 'Cement.model.goods.Railway',
            master: 'Cement.view.goods.railway.Total',
            sidebarCollapsed: true,
            leaf: true
        }, {
            text: 'Заявка на оказание услуг',
            xtype: 'goods_services_form_form',
            topLevel: 'goods',
            leaf: true,
            sidebarCollapsed: true,
            master: 'Cement.view.goods.services.Total',
            model: 'Cement.model.goods.Service'
        }, {
            text: 'Каталог',
            xtype: 'goods_catalog_form',
            topLevel: 'goods',
            leaf: true,
            sidebarCollapsed: true,
            master: 'Cement.view.goods.catalog.Products',
            model: 'Cement.model.goods.Catalog'
        }, {
            text: 'План',
            xtype: 'goods_plans_form_form',
            topLevel: 'goods',
            leaf: true,
            sidebarCollapsed: true,
            master: 'Cement.view.goods.plans.lists.New',
            model: 'Cement.model.goods.Plan'
        }]
    },

    cargo: {
        text: 'Грузооборот',
        treeId: 'cargo',
        leaf: false,
        expanded: true,
        children: [{
            text: 'Лот на груз',
            xtype: 'cargo_goods_lot_form_form',
            model: 'Cement.model.cargo.GoodsLot',
            topLevel: 'cargo',
            master: 'Cement.view.cargo.goods_lot.Total',
            sidebarCollapsed: true,
            leaf: true
        }, {
            text: 'Лот на транспорт',
            xtype: 'cargo_transport_lot_form_form',
            model: 'Cement.model.cargo.TransportLot',
            topLevel: 'cargo',
            master: 'Cement.view.cargo.transport_lot.Total',
            sidebarCollapsed: true,
            leaf: true
        }, {
            text: 'Участник',
            xtype: 'cargo_participants_form_form',
            model: 'Cement.model.cargo.Participant',
            topLevel: 'cargo',
            master: 'Cement.view.cargo.participants.lists.Current',
            sidebarCollapsed: true,
            leaf: true
        }]
    },

    contracts: {
        text: 'Договора',
        leaf: false,
        treeId: 'contracts',
        expanded: true,
        children: [{
            text: 'ОАО "Мордовцемент"',
            leaf: false,
            expanded: true,
            children: [{
                text: 'Договор поставки ЖД',
                leaf: false,
                expanded: true,
                children: [{
                    text: 'Договор',
                    xtype: 'contacts_contracts_form_contract',
                    model: 'Cement.model.contracts.Contract',
                    topLevel: 'contracts',
                    master: 'Cement.view.contract.lists.New',
                    sidebarCollapsed: true,
                    contractKind: 1,
                    leaf: true
                }, {
                    text: 'Приложение',
                    xtype: 'contracts_contracts_form_addon',
                    model: 'Cement.model.contracts.ContractAnnex',
                    topLevel: 'contracts',
                    master: 'Cement.view.contracts.addons.Total',
                    sidebarCollapsed: true,
                    form_kind: 10,
                    contractKind: 1,
                    leaf: true
                }, {
                    text: 'Дополнительное соглашение',
                    xtype: 'contracts_contracts_form_agreement',
                    topLevel: 'contracts',
                    model: 'Cement.model.contracts.ContractAnnex',
                    master: 'Cement.view.contracts.addons.Total',
                    sidebarCollapsed: true,
                    form_kind: 20,
                    contractKind: 1,
                    leaf: true
                }, {
                    text: 'Спецификация',
                    xtype: 'contracts_contracts_form_spec',
                    topLevel: 'contracts',
                    model: 'Cement.model.contracts.ContractAnnex',
                    master: 'Cement.view.contracts.addons.Total',
                    sidebarCollapsed: true,
                    form_kind: 30,
                    contractKind: 1,
                    leaf: true
                }]
            }]
        }]
    },

    document_flow: {
        text: 'Документооборот',
        leaf: false,
        treeId: 'document_flow',
        expanded: true,
        children: [{
            text: 'Входящий документ',
            xtype: 'document_flow_incoming_form',
            model: 'Cement.model.DocumentFlow',
            topLevel: 'document_flow',
            master: 'Cement.view.document_flow.incoming.list.New',
            sidebarCollapsed: true,
            leaf: true
        }, {
            text: 'Исходящий документ',
            xtype: 'document_flow_outcoming_form',
            model: 'Cement.model.DocumentFlowOutcoming',
            topLevel: 'document_flow',
            master: 'Cement.view.document_flow.outcoming.list.New',
            sidebarCollapsed: true,
            leaf: true
        }]
    },

    office_work: {
        text: 'Делопроизводство',
        leaf: false,
        treeId: 'office_work',
        expanded: true,
        children: [{
            text: 'Внутренний документ',
            xtype: 'office_work_documents_form_form',
            model: 'Cement.model.office_work.Document',
            topLevel: 'office_work',
            master: 'Cement.view.office_work.documents.Total',
            sidebarCollapsed: true,
            leaf: true
        }, {
            text: 'Исходящая корреспонденция',
            xtype: 'office_work_correspondention_form_form',
            model: 'Cement.model.office_work.Correspondention',
            topLevel: 'office_work',
            master: 'Cement.view.office_work.correspondention.Total',
            sidebarCollapsed: true,
            leaf: true
        }, {
            text: 'Приказ',
            xtype: 'office_work_disposal_form_form',
            model: 'Cement.model.office_work.Disposal',
            topLevel: 'office_work',
            master: 'Cement.view.office_work.disposal.Total',
            sidebarCollapsed: true,
            leaf: true
        }
        ]
    },

    personal: {
        text: 'Персональные данные',
        leaf: false,
        treeId: 'personal',
        expanded: true,
        children: [{
            text: 'Сообщение',
            xtype: 'personal_messages_form_form',
            model: 'Cement.model.personal.Message',
            topLevel: 'personal',
            master: 'Cement.view.personal.messages.lists.New',
            sidebarCollapsed: true,
            leaf: true
        }, {
            text: 'Объявление',
            xtype: 'personal_adverts_form_form',
            model: 'Cement.model.personal.Advert',
            topLevel: 'personal',
            master: 'Cement.view.personal.adverts.lists.New',
            sidebarCollapsed: true,
            leaf: true
        }]
    },

    navigation: {
        text: 'Навигация',
        leaf: false,
        treeId: 'navigation',
        expanded: true,
        children: [{
            text: 'Терминал',
            xtype: 'navigation_terminals_form_form',
            model: 'Cement.model.navigation.Terminal',
            topLevel: 'navigation',
            master: 'Cement.view.navigation.terminals.lists.New',
            sidebarCollapsed: true,
            leaf: true
        }, {
            text: 'Геозона',
            xtype: 'navigation_geozones_form_form',
            model: 'Cement.model.navigation.Geozone',
            topLevel: 'navigation',
            master: 'Cement.view.navigation.geozones.lists.New',
            sidebarCollapsed: true,
            leaf: true
        }]
    },

    accounting: {
        text: 'Бухгалтерия',
        leaf: false,
        treeId: 'accounting',
        expanded: true,
        otherDocumentIndex: 0,
        receiptexpenseDocumentIndex: 1,
        children: [
            {
                text: 'Иной бухгалтерский документ',
                leaf: false,
                children: [
                    {
                        text: 'Накладная (перемещение)',
                        xtype: 'accounting_other_form_movementbill',
                        topLevel: 'accounting',
                        model: 'Cement.model.accounting.Other',
                        master: 'Cement.view.accounting.other.Total',
                        sidebarCollapsed: true,
                        form_kind: Cement.Config.accounting.other_kinds.movementbill,
                        layout: 'fit',
                        removePadding: true,
                        leaf: true
                    },
                    {
                        text: 'Акт на списание',
                        xtype: 'accounting_other_form_writeoffact',
                        topLevel: 'accounting',
                        model: 'Cement.model.accounting.Other',
                        master: 'Cement.view.accounting.other.Total',
                        sidebarCollapsed: true,
                        form_kind: Cement.Config.accounting.other_kinds.writeoffact,
                        layout: 'fit',
                        removePadding: true,
                        leaf: true
                    },
                    {
                        text: 'Акт на оприходование',
                        xtype: 'accounting_other_form_postingact',
                        topLevel: 'accounting',
                        model: 'Cement.model.accounting.Other',
                        master: 'Cement.view.accounting.other.Total',
                        sidebarCollapsed: true,
                        form_kind: Cement.Config.accounting.other_kinds.postingact,
                        layout: 'fit',
                        removePadding: true,
                        leaf: true
                    }
                ]
            },
            {
                text: 'Документы (приход/расход)',
                leaf: false,
                children: [
                    {
                        text: 'Накладная (продажа)',
                        xtype: 'accounting_receiptexpense_form_salebill',
                        topLevel: 'accounting',
                        model: 'Cement.model.accounting.Receiptexpense',
                        master: 'Cement.view.accounting.receiptexpense.Total',
                        sidebarCollapsed: true,
                        form_kind: Cement.Config.accounting.receiptexpense_kinds.salebill,
                        layout: 'fit',
                        removePadding: true,
                        leaf: true
                    },
                    {
                        text: 'Акт выполненных работ',
                        xtype: 'accounting_receiptexpense_form_completionact',
                        topLevel: 'accounting',
                        model: 'Cement.model.accounting.Receiptexpense',
                        master: 'Cement.view.accounting.receiptexpense.Total',
                        sidebarCollapsed: true,
                        form_kind: Cement.Config.accounting.receiptexpense_kinds.completionact,
                        layout: 'fit',
                        removePadding: true,
                        leaf: true
                    },
                    {
                        text: 'Счет-фактура',
                        xtype: 'accounting_receiptexpense_form_invoice',
                        topLevel: 'accounting',
                        model: 'Cement.model.accounting.Receiptexpense',
                        master: 'Cement.view.accounting.receiptexpense.Total',
                        sidebarCollapsed: true,
                        form_kind: Cement.Config.accounting.receiptexpense_kinds.invoice,
                        layout: 'fit',
                        removePadding: true,
                        leaf: true
                    }
                ]
            },
            {
            text: 'Платежное поручение',
            xtype: 'accounting_documents_form_payment',
            model: 'Cement.model.accounting.Document' + Cement.Config.accounting.document_kinds.payment,
            topLevel: 'accounting',
            master: 'Cement.view.accounting.documents.lists.New',
            sidebarCollapsed: true,
            leaf: true
        }, {
            text: 'Счет',
            xtype: 'accounting_documents_form_bill',
            model: 'Cement.model.accounting.Document' + Cement.Config.accounting.document_kinds.bill,
            topLevel: 'accounting',
            master: 'Cement.view.accounting.documents.lists.New1',
            removePadding: true,
            sidebarCollapsed: true,
            leaf: true
        }, {
            text: 'Акт выполненных работ',
            xtype: 'accounting_documents_form_act',
            model: 'Cement.model.accounting.Document' + Cement.Config.accounting.document_kinds.act,
            topLevel: 'accounting',
            master: 'Cement.view.accounting.documents.lists.New2',
            removePadding: true,
            sidebarCollapsed: true,
            leaf: true
        }, {
            text: 'Доверенность',
            xtype: 'accounting_documents_form_proxy',
            model: 'Cement.model.accounting.Document' + Cement.Config.accounting.document_kinds.proxy,
            topLevel: 'accounting',
            master: 'Cement.view.accounting.documents.lists.New3',
            removePadding: true,
            sidebarCollapsed: true,
            leaf: true
        }, {
            text: 'Накладная',
            xtype: 'accounting_documents_form_waybill',
            model: 'Cement.model.accounting.Document' + Cement.Config.accounting.document_kinds.waybill,
            topLevel: 'accounting',
            master: 'Cement.view.accounting.documents.lists.New4',
            removePadding: true,
            sidebarCollapsed: true,
            leaf: true
        }, {
            text: 'Счет-фактура',
            xtype: 'accounting_documents_form_invoice',
            model: 'Cement.model.accounting.Document' + Cement.Config.accounting.document_kinds.invoice,
            topLevel: 'accounting',
            master: 'Cement.view.accounting.documents.lists.New5',
            removePadding: true,
            sidebarCollapsed: true,
            leaf: true
        }, {
            text: 'Товарный чек',
            xtype: 'accounting_documents_form_goods_check',
            model: 'Cement.model.accounting.Document' + Cement.Config.accounting.document_kinds.goods_check,
            topLevel: 'accounting',
            master: 'Cement.view.accounting.documents.lists.New6',
            removePadding: true,
            sidebarCollapsed: true,
            leaf: true
        }, {
            text: 'Универсальный передаточный документ',
            xtype: 'accounting_documents_form_universal_document',
            model: 'Cement.model.accounting.Document' + Cement.Config.accounting.document_kinds.universal_document,
            topLevel: 'accounting',
            master: 'Cement.view.accounting.documents.lists.New7',
            removePadding: true,
            sidebarCollapsed: true,
            leaf: true
        }, {
            text: 'Индикатор',
            xtype: 'accounting_indicators_form_form',
            model: 'Cement.model.accounting.Indicator',
            topLevel: 'accounting',
            master: 'Cement.view.accounting.indicators.lists.New',
            sidebarCollapsed: true,
            leaf: true
        }]
    },

    warehouse: {
        text: 'Склад',
        treeId: 'warehouse',
        leaf: false,
        expanded: true,
        warehouseIndex: 0,
        internalRequestIndex: 1,
        receiptIndex: 3,
        expenseIndex: 4,
        children: [
            {
                text: 'Склад',
                leaf: false,
                children: [
                    {
                        text: 'Склад (авто)',
                        xtype: 'warehouseformauto',
                        topLevel: 'warehouse',
                        model: 'Cement.model.Warehouse',
                        master: 'Cement.view.corporate.warehouse.List',
                        sidebarCollapsed: true,
                        form_kind: Cement.Config.warehouse.warehouse_kinds.auto,
                        //layout: 'fit',
                        removePadding: true,
                        leaf: true
                    },
                    {
                        text: 'Склад (Ж/Д)',
                        xtype: 'warehouseformrailway',
                        topLevel: 'warehouse',
                        model: 'Cement.model.Warehouse',
                        master: 'Cement.view.corporate.warehouse.List',
                        sidebarCollapsed: true,
                        form_kind: Cement.Config.warehouse.warehouse_kinds.railway,
                        //layout: 'fit',
                        removePadding: true,
                        leaf: true
                    }
                ]
            },
            {
                text: 'Внутренняя заявка',
                leaf: false,
                children: [
                    {
                        text: 'Заявка на перемещение',
                        xtype: 'warehouse_internal_form_movement',
                        topLevel: 'warehouse',
                        model: 'Cement.model.warehouse.InternalRequest',
                        master: 'Cement.view.warehouse.internal.Total',
                        sidebarCollapsed: true,
                        form_kind: Cement.Config.warehouse.request_kinds.movement,
                        leaf: true
                    },
                    {
                        text: 'Заявка на списание',
                        xtype: 'warehouse_internal_form_writeoff',
                        topLevel: 'warehouse',
                        model: 'Cement.model.warehouse.InternalRequest',
                        master: 'Cement.view.warehouse.internal.Total',
                        sidebarCollapsed: true,
                        form_kind: Cement.Config.warehouse.request_kinds.writeoff,
                        leaf: true
                    },
                    {
                        text: 'Заявка на оприходование (инвентаризация)',
                        xtype: 'warehouse_internal_form_posting',
                        topLevel: 'warehouse',
                        model: 'Cement.model.warehouse.InternalRequest',
                        master: 'Cement.view.warehouse.internal.Total',
                        sidebarCollapsed: true,
                        form_kind: Cement.Config.warehouse.request_kinds.posting,
                        leaf: true
                    },
                    {
                        text: 'Заявка на оприходование (производство)',
                        xtype: 'warehouse_internal_form_making_posting',
                        topLevel: 'warehouse',
                        model: 'Cement.model.warehouse.InternalRequest',
                        master: 'Cement.view.warehouse.internal.Total',
                        sidebarCollapsed: true,
                        form_kind: Cement.Config.warehouse.request_kinds.making_posting,
                        leaf: true
                    }
                ]
            },
            {
                text: 'Инвентаризация',
                xtype: 'warehouse_inventory_form_inventory',
                model: 'Cement.model.warehouse.Inventory',
                topLevel: 'warehouse',
                master: 'Cement.view.warehouse.inventory.lists.New',
                sidebarCollapsed: true,
                layout: 'fit',
                removePadding: true,
                leaf: true
            },
            {
                text: 'Приходный складской ордер',
                leaf: false,
                children: [
                    {
                        text: 'Приходный складской ордер (покупка)',
                        xtype: 'warehouse_receipt_form_receipt_buy',
                        topLevel: 'warehouse',
                        model: 'Cement.model.warehouse.Receipt',
                        master: 'Cement.view.warehouse.receipt.Total',
                        sidebarCollapsed: true,
                        form_kind: Cement.Config.warehouse.receipt_kinds.buy,
                        layout: 'fit',
                        removePadding: true,
                        leaf: true
                    },
                    {
                        text: 'Приходный складской ордер (перемещение)',
                        xtype: 'warehouse_receipt_form_receipt_movement',
                        topLevel: 'warehouse',
                        model: 'Cement.model.warehouse.Receipt',
                        master: 'Cement.view.warehouse.receipt.Total',
                        sidebarCollapsed: true,
                        form_kind: Cement.Config.warehouse.receipt_kinds.movement,
                        layout: 'fit',
                        removePadding: true,
                        leaf: true
                    },
                    {
                        text: 'Приходный складской ордер (оприходование)',
                        xtype: 'warehouse_receipt_form_receipt_posting',
                        topLevel: 'warehouse',
                        model: 'Cement.model.warehouse.Receipt',
                        master: 'Cement.view.warehouse.receipt.Total',
                        sidebarCollapsed: true,
                        form_kind: Cement.Config.warehouse.receipt_kinds.posting,
                        layout: 'fit',
                        removePadding: true,
                        leaf: true
                    }
                ]
            },
            {
                text: 'Расходный складской ордер',
                leaf: false,
                children: [
                    {
                        text: 'Расходный складской ордер (продажа)',
                        xtype: 'warehouse_expense_form_expense_sale',
                        topLevel: 'warehouse',
                        model: 'Cement.model.warehouse.Expense',
                        master: 'Cement.view.warehouse.expense.Total',
                        sidebarCollapsed: true,
                        form_kind: Cement.Config.warehouse.expense_kinds.sale,
                        layout: 'fit',
                        removePadding: true,
                        leaf: true
                    },
                    {
                        text: 'Расходный складской ордер (перемещение)',
                        xtype: 'warehouse_expense_form_expense_movement',
                        topLevel: 'warehouse',
                        model: 'Cement.model.warehouse.Expense',
                        master: 'Cement.view.warehouse.expense.Total',
                        sidebarCollapsed: true,
                        form_kind: Cement.Config.warehouse.expense_kinds.movement,
                        layout: 'fit',
                        removePadding: true,
                        leaf: true
                    },
                    {
                        text: 'Расходный складской ордер (списание)',
                        xtype: 'warehouse_expense_form_expense_writeoff',
                        topLevel: 'warehouse',
                        model: 'Cement.model.warehouse.Expense',
                        master: 'Cement.view.warehouse.expense.Total',
                        sidebarCollapsed: true,
                        form_kind: Cement.Config.warehouse.expense_kinds.writeoff,
                        layout: 'fit',
                        removePadding: true,
                        leaf: true
                    }
                ]
            }
        ]
    }
});

Ext.define('Cement.util.Animation', {
    singleton: true,
    createWindowAnimation: function (window) {
        //Ext.create('Ext.fx.Anim', {
        //    target: window,
        //    duration: 500,
        //    from: {
        //        opacity: 0
        //    },
        //    to: {
        //        opacity: 1
        //    }
        //});
    }
});

Ext.override(Ext.form.field.Date, {
    startDay: 1
});

Ext.override(Ext.form.field.Base, {
    getSubmitData: function () {
        var me = this,
                data = null,
                val;
        if (me.submitValue && !me.isFileUpload())
        {
            val = me.getSubmitValue();
            if (val !== null) {
                data = {};
                data[me.getName()] = val;
            }
        }
        return data;
    }
});