Ext.define('Cement.model.cargo.GoodsRight', {
    extend: 'Ext.data.Model',
    fields: [
        "p_name",
        "p_auto_mark_model",
        "p_auto_number",
        "p_auto_trailer",
        "p_auto_trailer_number",
        "p_kind",
        "p_transport_name",
        "p_transport_number",
        "p_driver_surname",
        "p_driver_name",
        "p_driver_lastname",
        "p_transport_size_display",
        "p_transport_go_display",
        "p_transport_char_ops",
        "p_transport_char_type_display",
        "p_transport_load_back",
        "p_transport_load_left",
        "p_transport_load_top",
        "p_transport_additional_options_display",
        "p_transport_additional_chars_display"
    ]
});