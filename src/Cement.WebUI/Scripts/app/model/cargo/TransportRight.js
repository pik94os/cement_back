Ext.define('Cement.model.cargo.TransportRight', {
    extend: 'Ext.data.Model',
    fields: [
        "p_number",
        "p_product_group",
        "p_product_subgroup",
        "p_product_display",
        "p_product_package",
        "p_product_unit_display",
        "p_product_quantity",
        "p_transport_char_ops",
        "p_transport_char_type_display",
        "p_transport_load_back",
        "p_transport_load_left",
        "p_transport_load_top",
        "p_transport_additional_options_display"
    ]
});