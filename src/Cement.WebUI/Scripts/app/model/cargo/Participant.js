Ext.define('Cement.model.cargo.Participant', {
    extend: 'Ext.data.Model',
    fields: [
        'p_grid_row_cls',
        'p_name',
        "p_session_display",
        "p_session",
        "p_group",
        "p_group_display",
        "p_address",
        "p_kpp",
        "p_inn",
        "p_ogrn",
        "p_date_start",
        "p_date_end",
        "p_base_doc"
    ]
});