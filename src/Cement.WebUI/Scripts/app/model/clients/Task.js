Ext.define('Cement.model.clients.Task', {
  extend: 'Ext.data.Model',
  fields: [
      'id',
      'p_client_display',
      'p_client',
      'p_theme',
      'p_content',
      'p_repeat_display',
      'p_repeat',
      'p_date',
      'p_grid_row_cls'
  ]
});