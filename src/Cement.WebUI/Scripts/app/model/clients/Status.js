Ext.define('Cement.model.clients.Status', {
  extend: 'Cement.model.Employee',
  fields: [
    'p_grid_row_cls',
    'p_name',
    'p_description'
  ]
});