Ext.define('Cement.model.clients.Management', {
  extend: 'Cement.model.Client',
  fields: [
    'p_grid_row_cls',
    'p_client_display',
    'p_client',
    'p_personal_client',
    'p_personal_client_display',
    'p_address_display',
    'p_date',
    'p_time',
    'p_reminder_display',
    'p_reminder',
    'p_theme',
    'p_description',
    'p_comments'
  ]
});