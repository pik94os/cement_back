Ext.define('Cement.model.clients.Group', {
  extend: 'Ext.data.Model',
  fields: [
    'p_grid_row_cls',
    'p_name',
    'p_group',
    'p_group_display',
    'p_description'
  ]
});