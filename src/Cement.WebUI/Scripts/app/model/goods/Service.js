Ext.define('Cement.model.goods.Service', {
    extend: 'Ext.data.Model',
    fields: [
        'p_grid_row_cls',
        'p_name',
        'p_request',
        'p_request_display',

        'p_number',
        'p_date',
        'p_kind',
        'p_comment',
        'p_status_display',
        'p_start_time',
        'p_end_time',
        'p_fact_start_time',
        'p_fact_end_time',

        'p_seller_name',
        'p_seller_address',
        'p_seller_work_time',

        'p_buyer_name',
        'p_buyer_address',
        'p_buyer_work_time',

        'p_service',
        'p_service_display',
        'p_service_unit_display',
        'p_service_count',
        'p_service_fact_count',
        'p_service_price',
        'p_service_tax',
        'p_service_sum',

        'p_carriage_kind',
        'p_carriage_owner',
        'p_carriage_number',
        'p_carriage_renter',
        'p_base_doc',
        'p_code_okun',
        'p_code_partnumber'
    ]
});