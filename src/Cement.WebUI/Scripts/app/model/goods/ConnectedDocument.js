Ext.define('Cement.model.goods.ConnectedDocument', {
    extend: 'Ext.data.Model',
    fields: [ 'id', 'p_number', 'p_name', 'p_date', 'p_kind', 'p_status', 'p_grid_row_cls' ]
});