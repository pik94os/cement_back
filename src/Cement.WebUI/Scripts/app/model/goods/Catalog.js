Ext.define('Cement.model.goods.Catalog', {
    extend: 'Ext.data.Model',
    fields: [
      'p_grid_row_cls',
        'p_price',
        'p_price_display',
        'p_comment'
    ]
});