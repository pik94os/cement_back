Ext.define('Cement.model.ProductSetItem', {
  extend: 'Ext.data.Model',
  fields: [
      'id',
      'p_product_id',
      'p_product_display',
      'p_is_product',
      'p_service_id',
      'p_service_display',
      'p_grid_row_cls',

      {
        name: 'p_name',
        mapping: 'p_name',
        convert: function(v, record) {
          if (record.get('p_is_product')) {
            return record.get('p_product_display');
          }
          return record.get('p_service_display');
        }
      }
  ]
});