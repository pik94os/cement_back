Ext.define('Cement.model.Firm', {
  extend: 'Ext.data.Model',
  fields: [
      'p_grid_row_cls',
      'p_activity_kind_display',
      'p_activity_kind',
      'p_group',
      'p_address',
      'p_fact_address',
      /*===============================*/
      'id',
      'p_name',
      'p_full_name',
      'p_brand',
      'p_ownership',
      'p_status',
      'p_law_index',
      'p_law_country',
      'p_law_region',
      'p_law_district',
      'p_law_city',
      'p_law_street',
      'p_law_house',
      'p_law_housepart',
      'p_law_building',
      'p_law_office',

      'p_fact_index',
      'p_fact_country',
      'p_fact_region',
      'p_fact_district',
      'p_fact_city',
      'p_fact_street',
      'p_fact_house',
      'p_fact_housepart',
      'p_fact_building',
      'p_fact_office',

      'p_phone',
      'p_fax',
      'p_email',
      'p_web',

      'p_ogrn',
      'p_ogrn_file',
      'p_inn',
      'p_inn_file',

      'p_kpp',
      'p_okpo',
      'p_okved',
      'p_okato',
      'p_okonh',
      'p_charter',

      'p_bank_active',

      'p_firm_code',
      'p_station',
      'p_station_code',
      'p_road',
      'p_img'
  ]
});