Ext.define('Cement.model.office_work.Disposal', {
    extend: 'Cement.model.office_work.Document',
    fields: [
        'p_employees',
        'p_employees_display'
    ]
});