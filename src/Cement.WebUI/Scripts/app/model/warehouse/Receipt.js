Ext.define('Cement.model.warehouse.Receipt', {
    extend: 'Ext.data.Model',
    fields: [
        'id',
        'p_base_doc',
        'p_type',
        'p_type_display',
        'p_number',
        'p_date',
        'p_warehouse',
        'p_warehouse_display',
        'p_warehouse_address',
        'p_provider',
        'p_provider_display',
        'p_product',
        'p_product_display',
        'p_measure_unit',
        'p_measure_unit_display',
        'p_count',
        'p_count_fact',
        'p_cause',
        'p_cause_display',
        'p_state',
        'p_state_display',
        'p_description'
    ]
});