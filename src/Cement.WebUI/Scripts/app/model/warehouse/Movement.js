Ext.define('Cement.model.warehouse.Movement', {
    extend: 'Ext.data.Model',
    fields: [
        'id',
        'p_name',
        'p_warehouse',
        'p_warehouse_display',
        'p_group',
        'p_group_display',
        'p_subgroup',
        'p_trade_mark',
        'p_manufacturer',
        'p_manufacturer_display',
        'p_measure_unit',
        'p_measure_unit_display',
        'p_initial_balance',
        'p_final_balance',
        'p_incoming',
        'p_rate',
        'stockroomProductHistoryFilterData'
    ]
});