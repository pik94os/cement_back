Ext.define('Cement.model.warehouse.Inventory', {
    extend: 'Ext.data.Model',
    fields: [
        'id',
        'p_base_doc',
        'p_kind',
        'p_type',
        'p_date',
        'p_kind_display',
        'p_number',
        'p_name',
        'p_warehouse',
        'p_warehouse_display',
        'p_warehouse_address',
        'p_chairman',
        'p_chairman_display',
        'p_director',
        'p_director_display',
        'p_members',
        'p_members_display',
        'p_state',
        'p_state_display',
        'p_description',
        'p_stockroomproducts',
        'p_existing_stockroomproducts'
    ]
});