Ext.define('Cement.model.BankAccount', {
    extend: 'Ext.data.Model',
    fields: [
        'id',
        'p_name',
        'p_number',
        'p_corr',
        'p_grid_row_cls',
        'p_bik'
    ]
});