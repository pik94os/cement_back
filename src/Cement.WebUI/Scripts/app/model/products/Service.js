Ext.define('Cement.model.products.Service', {
    extend: 'Ext.data.Model',
    fields: [
        'p_grid_row_cls',
        'p_name',       //Для дерева
        'p_group',   //Категория
        'p_group_display',
        'p_subgroup',  //Подкатегория
        'p_subgroup_display',
        'p_trade_mark',
        'p_manufacturer',
        'p_manufacturer_display',
        'p_text',
        'p_img',

        //Происхождение
        'p_found_country',
        'p_found_country_display',
        'p_found_manufacturer',
        'p_found_manufacturer_display',
        //Коды
        'p_code',
        'p_partnumber',
        //Технические условия
        'p_tu_gost',
        'p_tu_gost_r',
        'p_tu_ost',
        'p_tu_stp',
        'p_tu_tu',
        //Сертификаты
        'p_sert_soot_number',
        'p_sert_soot_img1',
        'p_sert_soot_img2',
        'p_sert_soot_start',
        'p_sert_soot_end',
        'p_sert_deklar_number',
        'p_sert_deklar_img',
        'p_sert_deklar_start',
        'p_sert_deklar_end',
        'p_sert_san_number',
        'p_sert_san_img',
        'p_sert_san_start',
        'p_sert_san_end',
        'p_sert_fire_number',
        'p_sert_fire_img',
        'p_sert_fire_start',
        'p_sert_fire_end',
        'p_sert_fire_deklar',
        'p_sert_fire_deklar_img',
        'p_sert_fire_deklar_start',
        'p_sert_fire_deklar_end',

        'p_unit',
        'p_unit_display',

        'p_complect',
        'p_complect_display',
        'p_code_okun',
        'p_code_partnumber'
    ]
});