Ext.define('Cement.model.products.PackType', {
    extend: 'Ext.data.Model',
    fields: [
        'p_name',
        'p_text',
        'p_img'
    ]
});