Ext.define('Cement.model.products.PriceList', {
    extend: 'Ext.data.Model',
    fields: [
        'p_grid_row_cls',
        'p_name',
        'p_date_start',
        'p_date_end',
        'p_replace_name',
        'p_replace_display',
        'p_replace_date_start',
        'p_replace_date_end',
        'p_comment',
        'p_products',
        'p_products_display',
        'p_author',
        'p_author_display',
        'p_sign_for',
        'p_sign_for_display',
        'p_documents',
        'p_documents_display',
        'p_deadline',
        'p_importance',
        'p_security',
        'p_import',
        'p_status'
    ]
});