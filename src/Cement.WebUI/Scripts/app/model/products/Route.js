Ext.define('Cement.model.products.Route', {
    extend: 'Ext.data.Model',
    fields: [
        'p_grid_row_cls',
        'p_org_name',
        'p_name',
        'p_position',
        'p_department',
        'p_status',
        'p_date_in',
        'p_date_out',
        'p_time_spent',
        'p_comment'
    ]
});