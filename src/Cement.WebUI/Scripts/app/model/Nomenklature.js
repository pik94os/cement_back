Ext.define('Cement.model.Nomenklature', {
  extend: 'Ext.data.Model',
  fields: [
      'id',
      'p_grid_row_cls',
      'p_index',
      'p_title',
      'p_count',
      'p_deadline',
      'p_comment',
      'p_year'
  ]
});