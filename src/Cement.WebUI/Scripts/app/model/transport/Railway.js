Ext.define('Cement.model.transport.Railway', {
    extend: 'Ext.data.Model',
    fields: [
        'p_model_display',
        'p_number_display',
        'p_feature_display',
        'p_grid_row_cls',

        'p_photo',
        'p_name',       //Название
        'p_img',        //Фото
        'p_made_year',  //Год выпуска
        'p_kind',       //Род
        'p_cargo_max',  //Грузоподъемность
        'p_package_mass_min',  //Масса тары min, т.
        'p_package_mass_max',  //Масса тары max, т.
        'p_axis_length',       //Длина по осям автосцепки, мм.
        'p_axis_count',        //Количество осей, шт
        'p_axis_load',         //Осевая нагрузка, т.
        'p_gangway',           //Переходная площадка
        'p_volume',            //Объем кузова, куб. м
        'p_trolley_model',     //Модель тележки
        'p_size',              //Габарит
        'p_manufacturer',      //Изготовитель

        //Для переданных и полученных в аренду:
        'p_rent_name',                     //Арендатор наименование
        'p_rent_address',                  //Арендатор адрес
        'p_rent_from',                     //Арендатор действует с
        'p_rent_to',                       //Арендатор действует до
        'p_rent_service_name',             //Услуга наименование
        'p_rent_service_unit_display',     //Услуга ед изм
        'p_rent_service_count',            //Услуга кол-во
        'p_rent_service_fact_count',       //Услуга факт кол-во
        'p_rent_service_price',            //Услуга цена
        'p_rent_service_tax',              //Услуга налог
        'p_rent_service_sum',              //Услуга сумма

        //Подробное инфо
        'show_char'                        //Показывать характеристику
    ]
});