Ext.define('Cement.model.transport.Regulation', {
  extend: 'Ext.data.Model',
  fields: [
      'id',
      'p_grid_row_cls',
      'p_transport_unit',
      'p_transport_unit_display',
      'p_name',
      'p_period',
      'p_odometer',
      'p_count',
      'p_comment',
      'p_items',
      'p_items_display',
      'p_work_name',
      'p_work_unit',
      'p_work_name_display',
      'p_work_unit_display'
  ]
});