Ext.define('Cement.model.transport.Maintenance', {
  extend: 'Ext.data.Model',
  fields: [
      'id',
      'p_grid_row_cls',
      'p_transport_unit',
      'p_transport_unit_display',
      'p_date',
      'p_odometer',
      'p_autoservice',
      'p_address',
      'p_master',
      'p_made_date',
      'p_cost',
      'p_name',
      'p_comment',
      'p_driver_display',
      'p_driver',
      'p_document',
      'p_document_file'
  ]
});