Ext.define('Cement.model.transport.Part', {
  extend: 'Ext.data.Model',
  fields: [
      'id',
      'p_grid_row_cls',
      'p_transport_unit',
      'p_transport_unit_display',
      'p_date',
      'p_name',
      'p_odometer',
      'p_autoservice',
      'p_address',
      'p_master',
      'p_unit',
      'p_count',
      'p_date_start',
      'p_date_end',
      'p_cost',
      'p_document',
      'p_document_file',
      'p_driver',
      'p_driver_display'
  ]
});