Ext.define('Cement.model.transport.Unit', {
    extend: 'Ext.data.Model',
    fields: [
        'p_name',
        'p_grid_row_cls',
        'p_auto_mark_model',
        'p_auto_number',
        'p_auto_trailer',
        'p_auto_trailer_number',
        'p_driver_name',
        'p_driver_surname',
        'p_driver_lastname',
        'p_class_display',
        'p_size_display',
        'p_go_display',
        'p_char_ops',            //ОПС
        'p_char_type_display',  //Тип кузова
        'p_load_back',
        'p_load_top',
        'p_load_left',
        'p_additional_options_display',   //Доп опции
        'p_additional_chars_display',     //Доп характеристики
        'p_date_start',
        'p_date_end',

        //Передача в управление
        'p_control_got_name',
        'p_control_got_address',
        'p_control_sent_name',
        'p_control_sent_address',
        'p_control_date_start',
        'p_control_date_end',

        //Для архива
        'p_owner_display',
        'p_type_display',

        //Для грузооборота
        'p_from_display',
        'p_from',
        'p_to_display',
        'p_to',

        //Для табеля
        'p_accounting',
        'p_unit_id',
        'p_date',
    ]
});