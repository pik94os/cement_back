Ext.define('Cement.model.transport.Timesheet', {
    extend: 'Ext.data.Model',
    fields: [
        'p_name',
        'p_indicators',
        'p_number',
        'p_base_doc'
    ]
});