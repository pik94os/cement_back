Ext.define('Cement.model.transport.FullInfo', {
  extend: 'Ext.data.Model',
  fields: [
    'p_grid_row_cls',
    'id',
    'show_char',
    'p_name', //Для показа в форме создания транспортной единицы
    'p_img',
    'p_mark',
    'p_model',
    'p_modification',
    'p_mark_display', //Марка в удобочитаемом виде, далее аналогично для модели и модификации
    'p_model_display',
    'p_modification_display',
    'p_year',
    'p_truck_display',
    'p_number',
    'p_number_country_code',
    'p_number_region_code',

    'p_category',
    'p_category_display',
    'p_sub_category',
    'p_sub_category_display',
    'p_transport',

    'p_reg_vin',
    'p_reg_engine',
    'p_reg_chasis',
    'p_reg_body',
    'p_reg_wheel_base',
    'p_reg_category',
    'p_reg_category_display',
    'p_reg_eco_class',
    'p_reg_color',
    'p_reg_power',
    'p_reg_engine_volume',
    'p_reg_max_mass',
    'p_reg_truck_mass',
    'p_reg_wheel_formula_display',
    'p_reg_wheel_formula',

    'p_pts_serie',
    'p_pts_number',
    'p_pts_manufacturer',
    'p_pts_date',
    'p_pts_page1',
    'p_pts_page2',

    'p_document_serie',
    'p_document_number',
    'p_document_owner',
    'p_document_date',
    'p_document_page1',
    'p_document_page2',

    'p_char_ops',
    'p_char_width',
    'p_char_height',
    'p_char_length',
    'p_char_capacity',
    'p_char_volume',
    'p_char_europalet',
    'p_char_additional',
    'p_char_additional_display',
    'p_char_load_back',
    'p_char_load_left',
    'p_char_load_top',
    'p_char_load_tir',
    'p_char_load_ekmt',

    'p_add_subdivision',
    'p_add_subdivision_display',
    'p_add_column',
    'p_add_column_display',
    'p_add_garage_number',
    'p_add_additional',


    'p_number_display',
    'p_char_type',
    'p_char_type_display',
    'p_char_class',
    'p_char_class_display',
    'p_char_size'


    // {
    //   name: 'p_gos_number',
    //   mapping: 'p_gos_number',
    //   convert: function(v, record) {
    //     var data = [];
    //     if (record.data.p_number && record.data.p_number != '') {
    //       data.push(record.data.p_number);
    //     }
    //     if (record.data.p_number_region_code && record.data.p_number_region_code != '') {
    //       var store = Ext.data.StoreManager.lookup('region_codes_Store');
    //         i = store.find('id', record.data.p_number_region_code);
    //       if (i >= 0) {
    //         data.push(store.getAt(i).get('p_name'));
    //       }
    //     }
    //     if (record.data.p_number_country_code && record.data.p_number_country_code != '') {
    //       var store = Ext.data.StoreManager.lookup('country_codes_Store');
    //         i = store.find('id', record.data.p_number_country_code);
    //       if (i >= 0) {
    //         data.push(store.getAt(i).get('p_name'));
    //       }
    //     }
    //     return data.join(' ');
    //   }
    // },

    // {
    //   name: 'p_reg_color_display',
    //   mapping: 'p_reg_color_display',
    //   convert: function (v, record) {
    //     if (record.data.p_reg_color) {
    //       var store = Ext.data.StoreManager.lookup('car_colors_Store'),
    //         rec_num = store.find('id', record.data.p_reg_color);
    //       if (rec_num >= 0) {
    //         return store.getAt(rec_num).get('p_name');
    //       }
    //       return '';
    //     }
    //     return '';
    //   }
    // },

    // {
    //   name: 'p_reg_eco_class_display',
    //   mapping: 'p_reg_eco_class_display',
    //   convert: function (v, record) {
    //     if (record.data.p_reg_eco_class) {
    //       var store = Ext.data.StoreManager.lookup('car_eco_classes_Store'),
    //         rec_num = store.find('id', record.data.p_reg_eco_class);
    //       if (rec_num >= 0) {
    //         return store.getAt(rec_num).get('p_name');
    //       }
    //       return '';
    //     }
    //     return '';
    //   }
    // },

    // {
    //   name: 'p_char_mass_volume',
    //   mapping: 'p_char_mass_volume',
    //   convert: function (v, record) {
    //     var c = record.get('p_char_capacity') || '',
    //       v = record.get('p_char_volume') || '';
    //     if (c == '' && v == '') {
    //       return '';
    //     }
    //     return c + '/' + v;
    //   }
    // },

    // {
    //   name: 'p_char_type_display',
    //   mapping: 'p_char_type_display',
    //   convert: function (v, record) {
    //     if (record.data.p_char_type) {
    //       var store = Ext.data.StoreManager.lookup('car_types_Store'),
    //         rec_num = store.find('id', record.data.p_char_type);
    //       if (rec_num >= 0) {
    //         return store.getAt(rec_num).get('p_name');
    //       }
    //       return '';
    //     }
    //     return '';
    //   }
    // },

    // {
    //   name: 'p_char_additional_display',
    //   mapping: 'p_char_additional_display',
    //   convert: function (v, record) {
    //     if (record.data.p_char_additional) {
    //       var store = Ext.data.StoreManager.lookup('car_options_Store'),
    //         rec_num = store.find('id', record.data.p_char_additional);
    //       if (rec_num >= 0) {
    //         return store.getAt(rec_num).get('p_name');
    //       }
    //       return '';
    //     }
    //     return '';
    //   }
    // },

    // {
    //   name: 'p_char_size_display',
    //   mapping: 'p_char_size_display',
    //   convert: function (v, record) {
    //     var data = [];
    //     if (record.data.p_char_length && record.data.p_char_length != '') {
    //       data.push(record.data.p_char_length);
    //     }
    //     if (record.data.p_char_width && record.data.p_char_width != '') {
    //       data.push(record.data.p_char_width);
    //     }
    //     if (record.data.p_char_height && record.data.p_char_height != '') {
    //       data.push(record.data.p_char_height);
    //     }
    //     return data.join('x');
    //   }
    // }
]
});