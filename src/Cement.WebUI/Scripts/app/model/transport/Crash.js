Ext.define('Cement.model.transport.Crash', {
  extend: 'Ext.data.Model',
  fields: [
    'p_grid_row_cls',
      'id',
      'p_transport_unit',
      'p_transport_unit_display',
      'p_date',
      'p_count',
      'p_damage',
      'p_comment',
      'p_sum',
      'p_driver',
      'p_driver_display'
  ]
});