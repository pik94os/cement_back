Ext.define('Cement.model.transport.Fuel', {
  extend: 'Ext.data.Model',
  fields: [
      'p_grid_row_cls',
      'id',
      'p_transport_unit',
      'p_transport_unit_display',
      'p_date',
      'p_name',
      'p_odometer',
      'p_station',
      'p_address',
      'p_unit',
      'p_count',
      'p_cost',
      'p_document',
      'p_document_file',
      'p_driver',
      'p_driver_display'
  ]
});