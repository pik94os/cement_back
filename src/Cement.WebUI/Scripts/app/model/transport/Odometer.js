Ext.define('Cement.model.transport.Odometer', {
  extend: 'Ext.data.Model',
  fields: [
      'id',
      'p_grid_row_cls',
      'p_transport_unit',
      'p_transport_unit_display',
      'p_date',
      'p_value'
  ]
});