Ext.define('Cement.model.transport.Auto', {
    extend: 'Ext.data.Model',
    fields: [
        'p_grid_row_cls',
        'p_name',       //Для дерева
        'p_category',   //Категория
        'p_category_display',
        'p_sub_category',  //Подкатегория
        'p_sub_category_display',
        'p_mark_model_spec',
        'p_mark_model_spec_display',  //Марка, модель, спецификация
        'p_number_display', //Гос номер
        'p_class_display',  //Класс
        'p_size_display',   //ДхШхВ
        'p_go_display',     //Г/О
        'p_char_ops',            //ОПС
        'p_char_type_display',  //Тип кузова
        'p_load_back',
        'p_load_top',
        'p_load_left',
        'p_additional_options_display',   //Доп опции
        'p_additional_chars_display',     //Доп характеристики
        'p_department_display',           //Подразделение
        'p_column_display',               //Колонна
        'p_column',
        'p_garage_number',                 //Гаражный номер

        //Для переданных и полученных в аренду:
        'p_rent_name',                     //Арендатор наименование
        'p_rent_address',                  //Арендатор адрес
        'p_rent_from',                     //Арендатор действует с
        'p_rent_to',                       //Арендатор действует до
        'p_rent_service_name',             //Услуга наименование
        'p_rent_service_unit_display',     //Услуга ед изм
        'p_rent_service_count',            //Услуга кол-во
        'p_rent_service_fact_count',       //Услуга факт кол-во
        'p_rent_service_price',            //Услуга цена
        'p_rent_service_tax',              //Услуга налог
        'p_rent_service_sum',              //Услуга сумма

        //Подробное инфо
        'show_char',                        //Показывать характеристику
        'p_name', //Для показа в форме создания транспортной единицы
        'p_img',
        'p_mark',
        'p_model',
        'p_modification',
        'p_mark_display', //Марка в удобочитаемом виде, далее аналогично для модели и модификации
        'p_model_display',
        'p_modification_display',
        'p_year',
        'p_truck_display',
        'p_number',
        'p_number_country_code',
        'p_number_region_code',

        'p_category',
        'p_category_display',
        'p_sub_category',
        'p_sub_category_display',
        'p_transport',

        'p_reg_vin',
        'p_reg_engine',
        'p_reg_chasis',
        'p_reg_body',
        'p_reg_wheel_base',
        'p_reg_category',
        'p_reg_category_display',
        'p_reg_eco_class',
        'p_reg_color',
        'p_reg_power',
        'p_reg_engine_volume',
        'p_reg_max_mass',
        'p_reg_truck_mass',
        'p_reg_wheel_formula_display',
        'p_reg_wheel_formula',

        'p_pts_serie',
        'p_pts_number',
        'p_pts_manufacturer',
        'p_pts_date',
        'p_pts_page1',
        'p_pts_page2',

        'p_document_serie',
        'p_document_number',
        'p_document_owner',
        'p_document_date',
        'p_document_page1',
        'p_document_page2',

        'p_char_ops',
        'p_char_width',
        'p_char_height',
        'p_char_length',
        'p_char_capacity',
        'p_char_volume',
        'p_char_europalet',
        'p_char_additional',
        'p_char_additional_display',
        'p_char_load_back',
        'p_char_load_left',
        'p_char_load_top',
        'p_char_load_tir',
        'p_char_load_ekmt',

        'p_add_subdivision',
        'p_add_subdivision_display',
        'p_add_column',
        'p_add_column_display',
        'p_add_garage_number',
        'p_add_additional',

        'p_number_display',
        'p_char_type',
        'p_char_type_display',
        'p_char_class',
        'p_char_class_display',
        'p_char_size'

    ]
});