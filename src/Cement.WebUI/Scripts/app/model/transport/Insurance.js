Ext.define('Cement.model.transport.Insurance', {
  extend: 'Ext.data.Model',
  fields: [
    'p_grid_row_cls',
      'id',
      'p_transport_unit',
      'p_transport_unit_display',
      'p_name',
      'p_number',
      'p_company',
      'p_address',
      'p_contact',
      'p_phone',
      'p_date_start',
      'p_date_end',
      'p_sum',
      'p_document_file'
  ]
});