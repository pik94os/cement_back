Ext.define('Cement.model.navigation.Geozone', {
    extend: 'Ext.data.Model',
    fields: [
      'p_grid_row_cls',
      'p_geometry',
      'p_name',
      'p_kind',
      'p_kind_display',
      'p_points',
      'p_base_doc'
    ]
});