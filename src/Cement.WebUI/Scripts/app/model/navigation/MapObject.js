Ext.define('Cement.model.navigation.MapObject', {
    extend: 'Ext.data.Model',
    fields: [
        'p_name',
        'p_is_cluster',
        'p_is_truck',
        'p_lat',
        'p_lng',
        'p_cluster_count',
        'p_base_doc'
    ]
});