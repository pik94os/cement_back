Ext.define('Cement.model.personal.Message', {
    extend: 'Ext.data.Model',
    fields: [
        'id',
        'p_grid_row_cls',
        'p_kind',
        'p_kind_display',
        'p_type',
        'p_type_display',
        'p_date',
        'p_author_display',
        'p_author',
        'p_employees_display',
        'p_employees',
        'p_repeat_display',
        'p_repeat',
        'p_theme',
        'p_security',
        'p_importance',
        'p_content',
        'p_hide_sign',
        'p_base_doc'
    ]
});