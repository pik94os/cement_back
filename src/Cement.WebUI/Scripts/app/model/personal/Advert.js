Ext.define('Cement.model.personal.Advert', {
    extend: 'Ext.data.Model',
    fields: [
        'id',
        'p_grid_row_cls',
        'p_kind',
        'p_kind_display',
        'p_date',
        'p_theme',
        'p_security',
        'p_importance',
        'p_content',
        'p_repeat_display',
        'p_repeat',
        'p_employees_display',
        'p_employees',
        'p_author_display',
        'p_author',
        'p_hide_sign',
        'p_base_doc'
    ]
});