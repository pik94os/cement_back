Ext.define('Cement.model.personal.Document', {
    extend: 'Ext.data.Model',
    fields: [
        'id',
        'p_grid_row_cls',
        'p_task',
        'p_kind',
        'p_number',
        'p_date',
        'p_author_name',
        'p_packet',
        'p_name',
        { name: 'p_selected', type: 'boolean', defaultValue: false },
        "p_hide_reg",
        "p_hide_sign",
        "p_hide_agree",
        "p_hide_cancel",
        'DocumentType'
    ]
});