Ext.define('Cement.model.accounting.Indicator', {
    extend: 'Ext.data.Model',
    fields: [
        'id',
        'p_grid_row_cls',
        "p_kind",
        "p_name",
        "p_name_display",
        "p_loaded",
        "p_requested",
        "p_total",
        "p_limit",
        "p_delay",
        "p_plus_minus",
        "p_date_start",
        "p_date_end",
        "p_payment_rules",
        "p_payment_rules_display",
        "p_basis",
        "p_basis_display",
        "p_basis_file",
        "p_comment",
        "p_base_doc"
    ]
});