Ext.define('Cement.model.accounting.Other', {
    extend: 'Ext.data.Model',
    fields: [
        'id',
        'p_name',
        'p_number',
        'p_form_kind',
        'p_base_doc',
        'p_request',
        'p_type',
        'p_type_display',
        'p_kind',
        'p_movementbill_kind',
        'p_writeoffact_kind',
        'p_kind_display',
        'p_date',
        'p_sum',
        'p_count',
        'p_cause',
        'p_cause_display',
        'p_state',
        'p_state_display',
        'p_description',
        'p_expenses',
        'p_expense_orders',
        'p_src',
        'p_dst'
    ]
});