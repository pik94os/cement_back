Ext.define('Cement.model.contracts.ContractAnnex', {
      extend: 'Ext.data.Model',
      fields: [
            'id',
            'p_grid_row_cls',
            'p_document_kind_display',
            'p_number',
            'p_date',
            'p_name',
            'p_sign_employee_display',
            'p_name_2',
            'p_sign_employee_2_display',
            'p_date_start',
            'p_date_end',
            'p_type_display',
            'p_document_to_display',

            'p_client_display',
            'p_client',
            'p_client_inn',
            'p_client_kpp',
            'p_client_address',

            'p_addon1_client',
            'p_addon2_client',
            'p_addon3_client',

            'p_addon_price2',
            'p_addon_price2_display',
            'p_addon_price1',
            'p_addon_price1_display',

            'p_payment_rules_display',
            'p_payment_rules',

            'p_spec_price_display',
            'p_spec_price',


            'p_contract_to',
            'p_contract_to_display',
            'p_price',
            'p_price_display',

            'p_price2_display',
            'p_price2',
            'p_price1_display',
            'p_price1',

            'p_form_kind',
            'p_contract_kind'
      ]
});