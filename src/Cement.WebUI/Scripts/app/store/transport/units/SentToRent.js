Ext.define('Cement.store.transport.units.SentToRent', {
    extend: 'Ext.data.Store',
    model: 'Cement.model.transport.Unit',
    autoLoad: false,
    pageSize: Cement.Config.defaultPageSize,
    proxy: Cement.Config.url.transport.units.sent_to_rent.proxy,
    remoteFilter: true,
    remoteSort: true
});