Ext.define('Cement.store.transport.units.Owned', {
    extend: 'Ext.data.Store',
    model: 'Cement.model.transport.Unit',
    autoLoad: false,
    pageSize: Cement.Config.defaultPageSize,
    proxy: Cement.Config.url.transport.units.owned.proxy,
    remoteFilter: true,
    remoteSort: true
});