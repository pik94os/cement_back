Ext.define('Cement.store.transport.units.GotInRent', {
    extend: 'Ext.data.Store',
    model: 'Cement.model.transport.Unit',
    autoLoad: false,
    pageSize: Cement.Config.defaultPageSize,
    proxy: Cement.Config.url.transport.units.got_in_rent.proxy,
    remoteFilter: true,
    remoteSort: true
});