Ext.define('Cement.store.transport.units.New', {
    extend: 'Ext.data.Store',
    model: 'Cement.model.transport.Unit',
    autoLoad: false,
    pageSize: Cement.Config.defaultPageSize,
    proxy: Cement.Config.url.transport.units.new_units.proxy,
    remoteFilter: true,
    remoteSort: true
});