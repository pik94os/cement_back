Ext.define('Cement.store.transport.units.Archive', {
    extend: 'Ext.data.Store',
    model: 'Cement.model.transport.Unit',
    autoLoad: false,
    pageSize: Cement.Config.defaultPageSize,
    proxy: Cement.Config.url.transport.units.archive.proxy,
    remoteFilter: true,
    remoteSort: true
});