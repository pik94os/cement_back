Ext.define('Cement.store.transport.auto.Owned', {
    extend: 'Ext.data.Store',
    model: 'Cement.model.transport.Auto',
    autoLoad: false,
    pageSize: Cement.Config.defaultPageSize,
    proxy: Cement.Config.url.transport.auto.owned.proxy,
    remoteFilter: true,
    remoteSort: true
});