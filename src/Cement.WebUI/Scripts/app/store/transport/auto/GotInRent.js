Ext.define('Cement.store.transport.auto.GotInRent', {
    extend: 'Ext.data.Store',
    model: 'Cement.model.transport.Auto',
    autoLoad: false,
    pageSize: Cement.Config.defaultPageSize,
    proxy: Cement.Config.url.transport.auto.got_in_rent.proxy,
    remoteFilter: true,
    remoteSort: true
});