Ext.define('Cement.store.transport.auto.SentToRent', {
    extend: 'Ext.data.Store',
    model: 'Cement.model.transport.Auto',
    autoLoad: false,
    pageSize: Cement.Config.defaultPageSize,
    proxy: Cement.Config.url.transport.auto.sent_to_rent.proxy,
    remoteFilter: true,
    remoteSort: true
});