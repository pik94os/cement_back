Ext.define('Cement.store.transport.Timesheet', {
    extend: 'Ext.data.Store',
    model: 'Cement.model.transport.Timesheet',
    autoLoad: false,
    pageSize: Cement.Config.defaultPageSize,
    proxy: Cement.Config.url.transport.timesheet.proxy,
    remoteFilter: true,
    remoteSort: true
});