Ext.define('Cement.store.transport.auxiliary.RailwayManufacturers', {
	extend: 'Ext.data.Store',
    autoLoad: false,
    fields: ['id', 'p_name'],
    proxy: getProxy(Cement.Config.url.transport.railway.manufacturers_dict, '')
});