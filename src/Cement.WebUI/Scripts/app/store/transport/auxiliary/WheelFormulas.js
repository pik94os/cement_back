Ext.define('Cement.store.transport.auxiliary.WheelFormulas', {
    extend: 'Ext.data.Store',
    fields: [
        'id', 'p_name'
    ],
    autoLoad: false
});