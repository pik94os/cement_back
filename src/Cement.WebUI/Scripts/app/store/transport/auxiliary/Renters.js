var proxy = getProxy(Cement.Config.url.transport.renters_structure, '');
proxy.nodeParam = 'p_parent';

Ext.define('Cement.store.transport.auxiliary.Renters', {
	extend: 'Ext.data.TreeStore',
    fields: ['id', 'text', 'address'],
    autoLoad: false,
    proxy: proxy,
    root: {
    	text: '',
    	id: 0
    }
});