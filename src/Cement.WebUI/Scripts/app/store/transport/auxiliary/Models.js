﻿Ext.define('Cement.store.transport.auxiliary.Models', {
	extend: 'Ext.data.TreeStore',
  fields: ['id', 'text'],
  autoLoad: false,
  proxy: getProxy(Cement.Config.url.transport.auto.models_dict, ''),
  root: {
    text: 'Все марки',
  	id: 0
  },
  remoteFilter: true
});