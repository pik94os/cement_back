Ext.define('Cement.store.transport.auxiliary.CarOptions', {
    extend: 'Ext.data.Store',
    fields: [
        'id', 'p_name'
    ],
    autoLoad: false
});