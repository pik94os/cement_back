var proxy = getProxy(Cement.Config.url.transport.all_cars_dict, '');
proxy.nodeParam = 'p_parent';
Ext.define('Cement.store.transport.auxiliary.AllCars', {
	extend: 'Ext.data.TreeStore',
    model: 'Cement.model.transport.Auto',
    autoLoad: false,
    proxy: proxy,
    root: {
    	text: '',
    	id: 0
    }
});