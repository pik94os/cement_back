var proxy = getProxy(Cement.Config.url.transport.all_transport_dict, '');
proxy.nodeParam = 'p_parent';

Ext.define('Cement.store.transport.auxiliary.AllTransport', {
	extend: 'Ext.data.TreeStore',
    model: 'Cement.model.transport.Auto',
    autoLoad: false,
    proxy: proxy,
    root: {
    	text: '',
    	id: 0
    }
});