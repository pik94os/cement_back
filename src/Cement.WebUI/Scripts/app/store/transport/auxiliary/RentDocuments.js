var proxy = getProxy(Cement.Config.url.transport.rent_documents_structure, '');
proxy.nodeParam = 'p_parent';

Ext.define('Cement.store.transport.auxiliary.RentDocuments', {
	extend: 'Ext.data.TreeStore',
    fields: ['id', 'text', 'p_group', 'p_subgroup', 'p_trademark', 'p_manufacturer', 'p_unit', 'p_tax', 'p_price' ],
    autoLoad: false,
    proxy: proxy,
    root: {
    	text: '',
    	id: 0
    }
});