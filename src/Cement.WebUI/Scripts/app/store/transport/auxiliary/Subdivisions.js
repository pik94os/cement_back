Ext.define('Cement.store.transport.auxiliary.Subdivisions', {
    extend: 'Ext.data.Store',
    fields: [ 'id', 'p_name' ],
    autoLoad: false,
    pageSize: Cement.Config.defaultPageSize,
    proxy: Cement.Config.url.transport.auto.subdivisions_proxy,
    remoteFilter: true
});