Ext.define('Cement.store.transport.auxiliary.CharTypes', {
    extend: 'Ext.data.Store',
    fields: [
        'id', 'p_name'
    ],
    autoLoad: false
});