var proxy = getProxy(Cement.Config.url.transport.all_drivers_dict, '');
proxy.nodeParam = 'p_parent';

Ext.define('Cement.store.transport.auxiliary.AllDrivers', {
	extend: 'Ext.data.TreeStore',
    fields: ['id', 'p_name', 'p_position', 'p_department' ],
    autoLoad: false,
    proxy: proxy,
    root: {
    	text: '',
    	id: 0
    }
});