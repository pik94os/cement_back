Ext.define('Cement.store.transport.auxiliary.OwnedCarriages', {
	extend: 'Ext.data.Store',
    autoLoad: false,
    fields: ['id', 'p_name'],
    proxy: getProxy(Cement.Config.url.transport.railway.owned_carriages_dict, '')
});