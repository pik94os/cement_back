var proxy = getProxy(Cement.Config.url.transport.all_units_dict, '');
proxy.nodeParam = 'p_parent';

Ext.define('Cement.store.transport.auxiliary.AllUnits', {
	extend: 'Ext.data.TreeStore',
    model: 'Cement.model.transport.Unit',
    autoLoad: false,
    proxy: proxy,
    root: {
    	text: '',
    	id: 0
    }
});