Ext.define('Cement.store.transport.auxiliary.TransportCategories', {
    extend: 'Ext.data.TreeStore',
    autoLoad: false,
    fields: ['id', 'text', 'p_show_char'],
    root: {
    	text: '',
    	id: 0,
    	expanded: true
    }
});