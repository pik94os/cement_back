Ext.define('Cement.store.transport.auxiliary.RegionCodes', {
	extend: 'Ext.data.Store',
	autoLoad: false,
    fields: ['id', 'p_name']
});