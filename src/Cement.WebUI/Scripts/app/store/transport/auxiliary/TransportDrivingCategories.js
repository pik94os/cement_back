Ext.define('Cement.store.transport.auxiliary.TransportDrivingCategories', {
	extend: 'Ext.data.Store',
    fields: [
        'id', 'p_name'
    ],
    autoLoad: false
});