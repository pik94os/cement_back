Ext.define('Cement.store.transport.auxiliary.RailwayModels', {
	extend: 'Ext.data.Store',
    autoLoad: false,
    fields: ['id', 'p_name'],
    proxy: getProxy(Cement.Config.url.transport.railway.models_dict, '')
});