Ext.define('Cement.store.transport.auxiliary.CharClasses', {
    extend: 'Ext.data.Store',
    fields: [
        'id', 'p_name'
    ],
    autoLoad: false
});