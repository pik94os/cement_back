Ext.define('Cement.store.transport.auxiliary.CountryCodes', {
	extend: 'Ext.data.Store',
    fields: [
        'id', 'p_name'
    ],
    autoLoad: false
});