Ext.define('Cement.store.transport.auxiliary.Columns', {
    extend: 'Ext.data.Store',
    fields: [ 'id', 'p_name' ],
    autoLoad: false,
    pageSize: Cement.Config.defaultPageSize,
    proxy: Cement.Config.url.transport.auto.columns_proxy,
    remoteFilter: true
});