Ext.define('Cement.store.transport.timesheet.Units', {
    extend: 'Ext.data.Store',
    model: 'Cement.model.transport.Unit',
    autoLoad: false,
    proxy: Cement.Config.url.transport.timesheet.transport_units_proxy,
    remoteFilter: true,
    remoteSort: true,
    listeners: {
        beforeload: function(self, operation) {
            return !!(operation && operation.params && operation.params.p_filter);
        }
    }
});