Ext.define('Cement.store.transport.railway.GotInRent', {
    extend: 'Ext.data.Store',
    model: 'Cement.model.transport.Railway',
    autoLoad: false,
    pageSize: Cement.Config.defaultPageSize,
    proxy: Cement.Config.url.transport.railway.got_in_rent.proxy,
    remoteFilter: true,
    remoteSort: true
});