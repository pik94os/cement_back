Ext.define('Cement.store.transport.railway.SentToRent', {
    extend: 'Ext.data.Store',
    model: 'Cement.model.transport.Railway',
    autoLoad: false,
    pageSize: Cement.Config.defaultPageSize,
    proxy: Cement.Config.url.transport.railway.sent_to_rent.proxy,
    remoteFilter: true,
    remoteSort: true
});