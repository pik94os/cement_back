Ext.define('Cement.store.transport.railway.Owned', {
    extend: 'Ext.data.Store',
    model: 'Cement.model.transport.Railway',
    autoLoad: false,
    pageSize: Cement.Config.defaultPageSize,
    proxy: Cement.Config.url.transport.railway.owned.proxy,
    remoteFilter: true,
    remoteSort: true
});