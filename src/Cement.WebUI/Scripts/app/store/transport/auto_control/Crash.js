Ext.define('Cement.store.transport.auto_control.Crash', {
    extend: 'Ext.data.Store',
    model: 'Cement.model.transport.Crash',
    autoLoad: false,
    pageSize: Cement.Config.defaultPageSize,
    proxy: Cement.Config.url.transport.auto_control.crash.proxy,
    remoteFilter: true,
    remoteSort: true
});