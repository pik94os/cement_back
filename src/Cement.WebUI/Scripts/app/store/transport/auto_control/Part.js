Ext.define('Cement.store.transport.auto_control.Part', {
    extend: 'Ext.data.Store',
    model: 'Cement.model.transport.Part',
    autoLoad: false,
    pageSize: Cement.Config.defaultPageSize,
    proxy: Cement.Config.url.transport.auto_control.parts.proxy,
    remoteFilter: true,
    remoteSort: true
});