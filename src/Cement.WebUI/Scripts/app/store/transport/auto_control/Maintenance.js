Ext.define('Cement.store.transport.auto_control.Maintenance', {
    extend: 'Ext.data.Store',
    model: 'Cement.model.transport.Maintenance',
    autoLoad: false,
    pageSize: Cement.Config.defaultPageSize,
    proxy: Cement.Config.url.transport.auto_control.maintenance.proxy,
    remoteFilter: true,
    remoteSort: true
});