Ext.define('Cement.store.transport.auto_control.Odometer', {
    extend: 'Ext.data.Store',
    model: 'Cement.model.transport.Odometer',
    autoLoad: false,
    pageSize: Cement.Config.defaultPageSize,
    proxy: Cement.Config.url.transport.auto_control.odometer.proxy,
    remoteFilter: true,
    remoteSort: true
});