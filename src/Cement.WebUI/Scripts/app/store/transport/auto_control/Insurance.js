Ext.define('Cement.store.transport.auto_control.Insurance', {
    extend: 'Ext.data.Store',
    model: 'Cement.model.transport.Insurance',
    autoLoad: false,
    pageSize: Cement.Config.defaultPageSize,
    proxy: Cement.Config.url.transport.auto_control.insurance.proxy,
    remoteFilter: true,
    remoteSort: true
});