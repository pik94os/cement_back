Ext.define('Cement.store.transport.auto_control.Regulation', {
    extend: 'Ext.data.Store',
    model: 'Cement.model.transport.Regulation',
    autoLoad: false,
    pageSize: Cement.Config.defaultPageSize,
    proxy: Cement.Config.url.transport.auto_control.regulation.proxy,
    remoteFilter: true,
    remoteSort: true
});