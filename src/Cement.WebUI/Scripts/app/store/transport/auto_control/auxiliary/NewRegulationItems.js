Ext.define('Cement.store.transport.auto_control.auxiliary.NewRegulationItems', {
    extend: 'Ext.data.Store',
    model: 'Cement.model.ProductSetItem',
    autoLoad: false,
    pageSize: Cement.Config.defaultPageSize,
    proxy: Cement.Config.url.transport.auto_control.regulation.items,
    remoteFilter: true
});