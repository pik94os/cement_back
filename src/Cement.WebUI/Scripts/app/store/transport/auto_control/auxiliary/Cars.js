Ext.define('Cement.store.transport.auto_control.auxiliary.Cars', {
    extend: 'Ext.data.Store',
    fields: [
        'id', 'p_name'
    ],
    autoLoad: false,
    proxy: getProxy(Cement.Config.url.transport.auto_control.cars_dict, ''),
    remoteFilter: true
});