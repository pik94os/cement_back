Ext.define('Cement.store.transport.auto_control.auxiliary.WorkNames', {
    extend: 'Ext.data.Store',
    fields: [
        'id', 'p_name'
    ],
    autoLoad: false,
    proxy: getProxy(Cement.Config.url.transport.auto_control.work_names_dict, ''),
    remoteFilter: true
});