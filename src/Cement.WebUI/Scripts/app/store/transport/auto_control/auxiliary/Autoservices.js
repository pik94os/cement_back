Ext.define('Cement.store.transport.auto_control.auxiliary.Autoservices', {
    extend: 'Ext.data.TreeStore',
    fields: ['id', 'p_name', 'p_address', 'p_work_time', 'p_autoservice'],
    autoLoad: false,
    pageSize: Cement.Config.defaultPageSize,
    proxy: getProxy(Cement.Config.url.transport.auto_control.autoservices_dict, ''),
    remoteFilter: true,
    root: {
    	text: '',
    	id: 0,
    	expanded: false
    }
});