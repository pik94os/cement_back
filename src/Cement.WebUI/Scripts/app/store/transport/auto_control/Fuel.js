Ext.define('Cement.store.transport.auto_control.Fuel', {
    extend: 'Ext.data.Store',
    model: 'Cement.model.transport.Fuel',
    autoLoad: false,
    pageSize: Cement.Config.defaultPageSize,
    proxy: Cement.Config.url.transport.auto_control.fuel.proxy,
    remoteFilter: true,
    remoteSort: true
});