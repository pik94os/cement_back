Ext.define('Cement.store.ExternalFirmStructure', {
    extend: 'Ext.data.TreeStore',
    fields: ['id', 'text', 'position', 'inn', 'p_comment'],
    autoLoad: false,
    proxy: Cement.Config.url.firm_structure_external,
    root: {
        text: '',
        id: 0,
        expanded: false
    }
});