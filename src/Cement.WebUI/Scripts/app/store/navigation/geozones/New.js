Ext.define('Cement.store.navigation.geozones.New', {
    extend: 'Ext.data.Store',
    model: 'Cement.model.navigation.Geozone',
    autoLoad: false,
    pageSize: Cement.Config.defaultPageSize,
    proxy: Cement.Config.url.navigation.geozones.new_g.proxy,
    remoteFilter: true,
    remoteSort: true
});