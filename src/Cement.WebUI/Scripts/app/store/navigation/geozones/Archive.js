Ext.define('Cement.store.navigation.geozones.Archive', {
    extend: 'Ext.data.Store',
    model: 'Cement.model.navigation.Geozone',
    autoLoad: false,
    pageSize: Cement.Config.defaultPageSize,
    proxy: Cement.Config.url.navigation.geozones.archive.proxy,
    remoteFilter: true,
    remoteSort: true
});