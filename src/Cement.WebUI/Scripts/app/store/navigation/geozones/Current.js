Ext.define('Cement.store.navigation.geozones.Current', {
    extend: 'Ext.data.Store',
    model: 'Cement.model.navigation.Geozone',
    autoLoad: false,
    pageSize: Cement.Config.defaultPageSize,
    proxy: Cement.Config.url.navigation.geozones.current.proxy,
    remoteFilter: true,
    remoteSort: true
});