Ext.define('Cement.store.navigation.geozones.bottom.archive.Routes', {
    extend: 'Ext.data.Store',
    model: 'Cement.model.products.Route',
    autoLoad: false,
    pageSize: Cement.Config.defaultPageSize,
    proxy: Cement.Config.url.navigation.geozones.routes.proxy,
    remoteFilter: true,
    remoteSort: true
});