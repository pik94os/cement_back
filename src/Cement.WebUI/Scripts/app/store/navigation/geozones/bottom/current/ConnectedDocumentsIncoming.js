Ext.define('Cement.store.navigation.geozones.bottom.current.ConnectedDocumentsIncoming', {
    extend: 'Ext.data.Store',
    model: 'Cement.model.goods.ConnectedDocument',
    autoLoad: false,
    pageSize: Cement.Config.defaultPageSize,
    proxy: Cement.Config.url.navigation.geozones.documents.proxy,
    remoteFilter: true,
    remoteSort: true
});