Ext.define('Cement.store.navigation.MapObjects', {
    extend: 'Ext.data.Store',
    model: 'Cement.model.navigation.MapObject',
    autoLoad: false,
    proxy: Cement.Config.url.navigation.map.proxy,
    remoteFilter: true,
    remoteSort: true
});