Ext.define('Cement.store.navigation.Indicators', {
    extend: 'Ext.data.Store',
    model: 'Cement.model.navigation.Indicator',
    autoLoad: false,
    proxy: Cement.Config.url.navigation.indicators.proxy,
    remoteFilter: true,
    remoteSort: true
});