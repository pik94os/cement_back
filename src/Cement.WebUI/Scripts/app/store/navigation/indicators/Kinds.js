Ext.define('Cement.store.navigation.indicators.Kinds', {
    extend: 'Ext.data.Store',
    fields: [ 'id', 'p_name', 'p_has_detailed_version' ],
    autoLoad: false
});