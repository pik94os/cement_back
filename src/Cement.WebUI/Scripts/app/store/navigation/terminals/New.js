Ext.define('Cement.store.navigation.terminals.New', {
    extend: 'Ext.data.Store',
    model: 'Cement.model.navigation.Terminal',
    autoLoad: false,
    pageSize: Cement.Config.defaultPageSize,
    proxy: Cement.Config.url.navigation.terminals.new_t.proxy,
    remoteFilter: true,
    remoteSort: true
});