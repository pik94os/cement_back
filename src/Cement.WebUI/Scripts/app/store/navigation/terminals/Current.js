Ext.define('Cement.store.navigation.terminals.Current', {
    extend: 'Ext.data.Store',
    model: 'Cement.model.navigation.Terminal',
    autoLoad: false,
    pageSize: Cement.Config.defaultPageSize,
    proxy: Cement.Config.url.navigation.terminals.current.proxy,
    remoteFilter: true,
    remoteSort: true
});