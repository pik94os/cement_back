Ext.define('Cement.store.navigation.terminals.Archive', {
    extend: 'Ext.data.Store',
    model: 'Cement.model.navigation.Terminal',
    autoLoad: false,
    pageSize: Cement.Config.defaultPageSize,
    proxy: Cement.Config.url.navigation.terminals.archive.proxy,
    remoteFilter: true,
    remoteSort: true
});