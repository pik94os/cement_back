Ext.define('Cement.store.navigation.terminals.auxiliary.TransportUnits', {
    extend: 'Ext.data.Store',
    model: 'Cement.model.transport.Unit',
    proxy: getProxy(Cement.Config.url.navigation.terminals.transport_units_dict, ''),
    remoteFilter: false,
    autoLoad: false
});