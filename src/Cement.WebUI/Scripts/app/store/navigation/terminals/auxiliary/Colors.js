Ext.define('Cement.store.navigation.terminals.auxiliary.Colors', {
    extend: 'Ext.data.Store',
    fields: [ 'id', 'p_name', 'p_color_code' ],
    autoLoad: false
});