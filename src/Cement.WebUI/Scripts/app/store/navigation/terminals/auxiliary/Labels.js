Ext.define('Cement.store.navigation.terminals.auxiliary.Labels', {
    extend: 'Ext.data.Store',
    fields: [ 'id', 'p_name', 'p_label_src' ],
    autoLoad: false
});