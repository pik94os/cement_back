Ext.define('Cement.store.navigation.terminals.auxiliary.ServiceSuppliers', {
    extend: 'Ext.data.Store',
    fields: [ 'id', 'p_name', 'p_ip', 'p_port' ],
    proxy: getProxy(Cement.Config.url.navigation.terminals.service_suppliers_dict, ''),
    remoteFilter: false,
    autoLoad: false
});