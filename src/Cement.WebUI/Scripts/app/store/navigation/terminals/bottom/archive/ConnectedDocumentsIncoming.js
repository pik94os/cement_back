Ext.define('Cement.store.navigation.terminals.bottom.archive.ConnectedDocumentsIncoming', {
    extend: 'Ext.data.Store',
    model: 'Cement.model.goods.ConnectedDocument',
    autoLoad: false,
    pageSize: Cement.Config.defaultPageSize,
    proxy: Cement.Config.url.navigation.terminals.documents.proxy,
    remoteFilter: true,
    remoteSort: true
});