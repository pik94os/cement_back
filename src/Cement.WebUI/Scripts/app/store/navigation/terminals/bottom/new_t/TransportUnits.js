Ext.define('Cement.store.navigation.terminals.bottom.new_t.TransportUnits', {
    extend: 'Ext.data.Store',
    model: 'Cement.model.transport.Unit',
    autoLoad: false,
    pageSize: Cement.Config.defaultPageSize,
    proxy: Cement.Config.url.navigation.terminals.transport_units.proxy,
    remoteFilter: true,
    remoteSort: true
});