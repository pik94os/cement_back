Ext.define('Cement.store.personal.correspondention.Outcoming', {
    extend: 'Ext.data.Store',
    model: 'Cement.model.personal.Document',
    autoLoad: false,
    pageSize: Cement.Config.defaultPageSize,
    proxy: Cement.Config.url.personal.correspondention.outcoming.proxy,
    remoteFilter: true,
    remoteSort: true
});