Ext.define('Cement.store.personal.correspondention.Incoming', {
    extend: 'Ext.data.Store',
    model: 'Cement.model.personal.Document',
    autoLoad: false,
    pageSize: Cement.Config.defaultPageSize,
    proxy: Cement.Config.url.personal.correspondention.incoming.proxy,
    remoteFilter: true,
    remoteSort: true
});