Ext.define('Cement.store.personal.adverts.bottom.incoming.ConnectedDocumentsIncoming', {
    extend: 'Ext.data.Store',
    model: 'Cement.model.goods.ConnectedDocument',
    autoLoad: false,
    pageSize: Cement.Config.defaultPageSize,
    proxy: Cement.Config.url.personal.adverts.documents.proxy,
    remoteFilter: true,
    remoteSort: true
});