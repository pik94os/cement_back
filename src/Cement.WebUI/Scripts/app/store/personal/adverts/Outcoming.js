Ext.define('Cement.store.personal.adverts.Outcoming', {
    extend: 'Ext.data.Store',
    model: 'Cement.model.personal.Advert',
    autoLoad: false,
    pageSize: Cement.Config.defaultPageSize,
    proxy: Cement.Config.url.personal.adverts.outcoming.proxy,
    remoteFilter: true,
    remoteSort: true
});