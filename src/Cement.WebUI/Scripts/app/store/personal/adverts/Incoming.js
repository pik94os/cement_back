Ext.define('Cement.store.personal.adverts.Incoming', {
    extend: 'Ext.data.Store',
    model: 'Cement.model.personal.Advert',
    autoLoad: false,
    pageSize: Cement.Config.defaultPageSize,
    proxy: Cement.Config.url.personal.adverts.incoming.proxy,
    remoteFilter: true,
    remoteSort: true
});