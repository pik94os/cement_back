Ext.define('Cement.store.personal.adverts.Archive', {
    extend: 'Ext.data.Store',
    model: 'Cement.model.personal.Advert',
    autoLoad: false,
    pageSize: Cement.Config.defaultPageSize,
    proxy: Cement.Config.url.personal.adverts.archive.proxy,
    remoteFilter: true,
    remoteSort: true
});