Ext.define('Cement.store.personal.adverts.New', {
    extend: 'Ext.data.Store',
    model: 'Cement.model.personal.Advert',
    autoLoad: false,
    pageSize: Cement.Config.defaultPageSize,
    proxy: Cement.Config.url.personal.adverts.new_a.proxy,
    remoteFilter: true,
    remoteSort: true
});