Ext.define('Cement.store.personal.adverts.auxiliary.Kinds', {
    extend: 'Ext.data.Store',
    fields: [
        'id', 'p_name'
    ],
    autoLoad: false
});