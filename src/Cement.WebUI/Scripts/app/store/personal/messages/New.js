Ext.define('Cement.store.personal.messages.New', {
    extend: 'Ext.data.Store',
    model: 'Cement.model.personal.Message',
    autoLoad: false,
    pageSize: Cement.Config.defaultPageSize,
    proxy: Cement.Config.url.personal.messages.new_m.proxy,
    remoteFilter: true,
    remoteSort: true
});