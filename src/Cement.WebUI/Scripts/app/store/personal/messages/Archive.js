Ext.define('Cement.store.personal.messages.Archive', {
    extend: 'Ext.data.Store',
    model: 'Cement.model.personal.Message',
    autoLoad: false,
    pageSize: Cement.Config.defaultPageSize,
    proxy: Cement.Config.url.personal.messages.archive.proxy,
    remoteFilter: true,
    remoteSort: true
});