Ext.define('Cement.store.personal.messages.Outcoming', {
    extend: 'Ext.data.Store',
    model: 'Cement.model.personal.Message',
    autoLoad: false,
    pageSize: Cement.Config.defaultPageSize,
    proxy: Cement.Config.url.personal.messages.outcoming.proxy,
    remoteFilter: true,
    remoteSort: true
});