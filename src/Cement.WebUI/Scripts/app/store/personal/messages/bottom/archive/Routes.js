Ext.define('Cement.store.personal.messages.bottom.archive.Routes', {
    extend: 'Ext.data.Store',
    model: 'Cement.model.products.Route',
    autoLoad: false,
    pageSize: Cement.Config.defaultPageSize,
    proxy: Cement.Config.url.personal.messages.routes.proxy,
    remoteFilter: true,
    remoteSort: true
});