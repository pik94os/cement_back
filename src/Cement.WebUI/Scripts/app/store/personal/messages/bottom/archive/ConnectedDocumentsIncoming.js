Ext.define('Cement.store.personal.messages.bottom.archive.ConnectedDocumentsIncoming', {
    extend: 'Ext.data.Store',
    model: 'Cement.model.goods.ConnectedDocument',
    autoLoad: false,
    pageSize: Cement.Config.defaultPageSize,
    proxy: Cement.Config.url.personal.messages.documents.proxy,
    remoteFilter: true,
    remoteSort: true
});