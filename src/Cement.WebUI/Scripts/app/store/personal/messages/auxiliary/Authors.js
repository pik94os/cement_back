Ext.define('Cement.store.personal.messages.auxiliary.Authors', {
    extend: 'Ext.data.TreeStore',
    fields: ['id', 'p_name', 'p_position', 'p_department'],
    autoLoad: false,
    pageSize: Cement.Config.defaultPageSize,
    proxy: getProxy(Cement.Config.url.personal.messages.authors_dict, ''),
    remoteFilter: true,
    root: {
      text: '',
      id: 0,
      expanded: false
    }
});