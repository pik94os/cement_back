Ext.define('Cement.store.personal.messages.auxiliary.Kinds', {
    extend: 'Ext.data.Store',
    fields: [
        'id', 'p_name'
    ],
    autoLoad: false
});