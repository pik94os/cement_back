Ext.define('Cement.store.personal.messages.Incoming', {
    extend: 'Ext.data.Store',
    model: 'Cement.model.personal.Message',
    autoLoad: false,
    pageSize: Cement.Config.defaultPageSize,
    proxy: Cement.Config.url.personal.messages.incoming.proxy,
    remoteFilter: true,
    remoteSort: true
});