Ext.define('Cement.store.personal.documents.Outcoming', {
    extend: 'Ext.data.Store',
    model: 'Cement.model.personal.Document',
    autoLoad: false,
    pageSize: Cement.Config.defaultPageSize,
    proxy: Cement.Config.url.personal.documents.outcoming.proxy,
    remoteFilter: true,
    remoteSort: true
});