Ext.define('Cement.store.personal.documents.Incoming', {
    extend: 'Ext.data.Store',
    model: 'Cement.model.personal.Document',
    autoLoad: false,
    pageSize: Cement.Config.defaultPageSize,
    proxy: Cement.Config.url.personal.documents.incoming.proxy,
    remoteFilter: true,
    remoteSort: true
});