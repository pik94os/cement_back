Ext.define('Cement.store.administration.Dictionaries', {
    extend: 'Ext.data.Store',
    fields: [
        'id',
        'Name',
        'Description',
        'FullName',
        'Bik',
        'Ks',
        'Model',
        'Brand',
        'Category',
        'SubCategory',
        'ProductType',
        'ProductGroup',
        'Rate',
        'StringValue',
        'Class',
        'Num',
        'ShortName',
        'Code',
        'RegionName',
        'RailwayName',
        'RailwayShortName',
        'MeasureUnitGroup',
        'IsNational',
        'IsNotInEskk',
        'Photo',
        { name: 'ProductGroupType', mapping: 'ProductGroup.ProductType' },
        { name: 'ModelBrand', mapping: 'Model.Brand.Name' },
        { name: 'ModelCategory', mapping: 'Model.Category.Name' },
        { name: 'ModelSubCategory', mapping: 'Model.SubCategory.Name' },
        {
            name: 'PhotoLink',
            convert: function (value, record) {
                var val = record.get('Photo');
                if (val && val.id) {
                    return "/file/download/" + val.id;
                }

                return null;
            }
        },
    ],
    autoLoad: false,
    pageSize: Cement.Config.defaultPageSize,
    proxy: Cement.Config.url.administration.dictionaries.proxy,
    remoteFilter: true,
    remoteSort: true,
    listeners: {
        beforeload: function (self, operation) {
            return !!(operation && operation.filters && operation.filters.length);
        }
    }
});