Ext.define('Cement.store.administration.Superusers', {
    extend: 'Ext.data.Store',
    //model: 'Cement.model.products.Product',
    fields: [
        'id',
        'p_login',
        'p_state',
        'p_is_deletable',
        'p_is_blockable',
        'p_is_unblockable'
    ],
    autoLoad: false,
    pageSize: Cement.Config.defaultPageSize,
    proxy: Cement.Config.url.administration.superusers.proxy,
    remoteFilter: true,
    remoteSort: true
});