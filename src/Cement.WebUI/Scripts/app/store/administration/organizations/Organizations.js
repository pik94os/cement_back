Ext.define('Cement.store.administration.organizations.Organizations', {
    extend: 'Ext.data.Store',
    fields: ['id', 'p_name', 'p_inn'],
    autoLoad: false,
    pageSize: Cement.Config.defaultPageSize,
    proxy: Cement.Config.url.administration.organizations.organizations_dict,
    remoteFilter: true
});