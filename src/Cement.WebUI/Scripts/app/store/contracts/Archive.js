Ext.define('Cement.store.contracts.Archive', {
    extend: 'Ext.data.Store',
    model: 'Cement.model.contracts.Contract',
    autoLoad: false,
    pageSize: Cement.Config.defaultPageSize,
    proxy: Cement.Config.url.contracts.contracts.archive.proxy,
    remoteFilter: true,
    remoteSort: true
});