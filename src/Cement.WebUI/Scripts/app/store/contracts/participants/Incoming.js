Ext.define('Cement.store.contracts.participants.Incoming', {
    extend: 'Ext.data.Store',
    fields: ['id', 'p_name', 'p_call_name', 'p_address', 'p_inn', 'p_kpp', 'p_signer', 'p_signer_position', 'p_signer_reason'],
    autoLoad: false,
    pageSize: Cement.Config.defaultPageSize,
    proxy: Cement.Config.url.contracts.contracts.participants.proxy,
    remoteFilter: true,
    remoteSort: true
});