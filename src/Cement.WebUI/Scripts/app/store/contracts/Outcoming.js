Ext.define('Cement.store.contracts.Outcoming', {
    extend: 'Ext.data.Store',
    model: 'Cement.model.contracts.Contract',
    autoLoad: false,
    pageSize: Cement.Config.defaultPageSize,
    proxy: Cement.Config.url.contracts.contracts.outcoming.proxy,
    remoteFilter: true,
    remoteSort: true
});