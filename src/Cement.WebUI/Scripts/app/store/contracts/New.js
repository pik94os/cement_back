Ext.define('Cement.store.contracts.New', {
    extend: 'Ext.data.Store',
    model: 'Cement.model.contracts.Contract',
    autoLoad: false,
    pageSize: Cement.Config.defaultPageSize,
    proxy: Cement.Config.url.contracts.contracts.contracts_new.proxy,
    remoteFilter: true,
    remoteSort: true
});