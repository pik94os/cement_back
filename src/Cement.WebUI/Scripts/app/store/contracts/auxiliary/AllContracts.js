Ext.define('Cement.store.contracts.auxiliary.AllContracts', {
    extend: 'Ext.data.TreeStore',
    fields: ['id', 'text', 'address', 'kpp', 'inn'],
    proxy: Cement.Config.url.contracts.contracts.structure,
    root: {
    	text: '',
    	id: 0,
    	expanded: false
    },
    remoteFilter: false,
    autoLoad: false
});