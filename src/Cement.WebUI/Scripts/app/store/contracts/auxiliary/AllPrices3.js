Ext.define('Cement.store.contracts.auxiliary.AllPrices3', {
    extend: 'Ext.data.Store',
    model: 'Cement.model.products.PriceList',
    proxy: getProxy(Cement.Config.url.contracts.contracts.all_prices_dict, ''),
    remoteFilter: false,
    autoLoad: false
});