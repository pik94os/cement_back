Ext.define('Cement.store.contracts.addons.New', {
    extend: 'Ext.data.Store',
    model: 'Cement.model.contracts.ContractAnnex',
    autoLoad: false,
    pageSize: Cement.Config.defaultPageSize,
    proxy: Cement.Config.url.contracts.contracts.addons.proxy,
    remoteFilter: true,
    remoteSort: true
});