Ext.define('Cement.store.contracts.Incoming', {
    extend: 'Ext.data.Store',
    model: 'Cement.model.contracts.Contract',
    autoLoad: false,
    pageSize: Cement.Config.defaultPageSize,
    proxy: Cement.Config.url.contracts.contracts.incoming.proxy,
    remoteFilter: true,
    remoteSort: true
});