Ext.define('Cement.store.addons.Outcoming', {
    extend: 'Ext.data.Store',
    model: 'Cement.model.contracts.Contract',
    autoLoad: false,
    pageSize: Cement.Config.defaultPageSize,
    proxy: Cement.Config.url.contracts.addons.outcoming.proxy,
    remoteFilter: true,
    remoteSort: true
});