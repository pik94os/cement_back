Ext.define('Cement.store.addons.Incoming', {
    extend: 'Ext.data.Store',
    model: 'Cement.model.contracts.Contract',
    autoLoad: false,
    pageSize: Cement.Config.defaultPageSize,
    proxy: Cement.Config.url.contracts.addons.incoming.proxy,
    remoteFilter: true,
    remoteSort: true
});