Ext.define('Cement.store.addons.Archive', {
    extend: 'Ext.data.Store',
    model: 'Cement.model.contracts.Contract',
    autoLoad: false,
    pageSize: Cement.Config.defaultPageSize,
    proxy: Cement.Config.url.contracts.addons.archive.proxy,
    remoteFilter: true,
    remoteSort: true
});