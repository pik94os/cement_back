Ext.define('Cement.store.addons.New', {
    extend: 'Ext.data.Store',
    model: 'Cement.model.contracts.ContractAnnex',
    autoLoad: false,
    pageSize: Cement.Config.defaultPageSize,
    proxy: Cement.Config.url.contracts.addons.addons_new.proxy,
    remoteFilter: true,
    remoteSort: true
});