Ext.define('Cement.store.addons.addons.New', {
    extend: 'Ext.data.Store',
    model: 'Cement.model.contracts.Contract',
    autoLoad: false,
    pageSize: Cement.Config.defaultPageSize,
    proxy: Cement.Config.url.contracts.addons.addons.proxy,
    remoteFilter: true,
    remoteSort: true
});