Ext.define('Cement.store.SignerStatuses', {
    extend: 'Ext.data.Store',
    autoLoad: false,
    fields: ['id', 'p_name']
});