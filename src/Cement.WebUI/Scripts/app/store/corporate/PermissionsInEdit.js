Ext.define('Cement.store.corporate.PermissionsInEdit', {
    extend: 'Cement.store.corporate.Permissions',
    model: 'Cement.model.Permission'
});