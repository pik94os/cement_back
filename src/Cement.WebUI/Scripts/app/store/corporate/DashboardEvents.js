Ext.define('Cement.store.corporate.DashboardEvents', {
    extend: 'Ext.data.TreeStore',
    fields: ['id', 'p_name'],
    root: {
        expanded: true
    }
});