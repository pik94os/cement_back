Ext.define('Cement.store.corporate.Nomenklatures', {
    extend: 'Ext.data.Store',
    model: 'Cement.model.Nomenklature',
    autoLoad: false,
    pageSize: Cement.Config.defaultPageSize,
    proxy: Cement.Config.url.corporate.nomenklatures.proxy,
    remoteFilter: true,
    remoteSort: true
});