Ext.define('Cement.store.corporate.Permissions', {
    extend: 'Ext.data.Store',
    model: 'Cement.model.Permission',
    autoLoad: false,
    pageSize: Cement.Config.defaultPageSize,
    proxy: Cement.Config.url.corporate.administration.proxy,
    remoteFilter: true,
    remoteSort: true
});