Ext.define('Cement.store.corporate.auxiliary.DrivingCategories', {
    extend: 'Ext.data.Store',
    fields: [
        'id', 'p_name'
    ],
    autoLoad: false
});