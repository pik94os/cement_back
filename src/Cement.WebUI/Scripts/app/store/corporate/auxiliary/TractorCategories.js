Ext.define('Cement.store.corporate.auxiliary.TractorCategories', {
    extend: 'Ext.data.Store',
    fields: [
        'id', 'p_name'
    ],
    autoLoad: false
});