Ext.define('Cement.store.corporate.auxiliary.WarehouseStations', {
    extend: 'Ext.data.Store',
    fields: [
        'id', 'p_name', 'p_station_code', 'p_road'
    ],
    proxy: getProxy(Cement.Config.url.corporate.warehouses.stations, ''),
    autoLoad: false,
    remoteFilter: true
});