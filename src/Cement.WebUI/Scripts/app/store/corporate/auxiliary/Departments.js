Ext.define('Cement.store.corporate.auxiliary.Departments', {
    extend: 'Ext.data.Store',
    fields: [ 'id', 'p_name' ],
    autoLoad: false,
    pageSize: Cement.Config.defaultPageSize,
    proxy: Cement.Config.url.corporate.departments.proxy,
    remoteFilter: true
});