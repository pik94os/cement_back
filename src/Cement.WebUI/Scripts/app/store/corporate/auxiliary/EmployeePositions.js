Ext.define('Cement.store.corporate.auxiliary.EmployeePositions', {
    extend: 'Ext.data.Store',
    fields: [ 'id', 'p_name' ],
    autoLoad: false
});