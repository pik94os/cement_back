Ext.define('Cement.store.corporate.auxiliary.Streets', {
    extend: 'Ext.data.Store',
    fields: [
        'id', 'p_name'
    ],
    proxy: getProxy(Cement.Config.url.streets_list, ''),
    autoLoad: false,
    remoteFilter: true
});