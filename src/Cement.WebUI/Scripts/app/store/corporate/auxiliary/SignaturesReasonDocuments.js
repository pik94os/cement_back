Ext.define('Cement.store.corporate.auxiliary.SignaturesReasonDocuments', {
    extend: 'Ext.data.TreeStore',
    autoLoad: false,
    fields: ['id', 'text'],
    proxy: Cement.Config.url.corporate.signatures.documents,
    root: {
    	text: '',
    	id: 0,
    	expanded: false
    }
});