Ext.define('Cement.store.corporate.auxiliary.DriveFrom', {
    extend: 'Ext.data.Store',
    fields: [ 'id', 'p_name' ],
    autoLoad: false
});