Ext.define('Cement.store.accounting.receiptexpense.Archive', {
    extend: 'Ext.data.Store',
    model: 'Cement.model.accounting.Receiptexpense',
    autoLoad: false,
    pageSize: Cement.Config.defaultPageSize,
    proxy: Cement.Config.url.accounting.receiptexpense.archive.proxy,
    remoteFilter: true,
    remoteSort: true
});