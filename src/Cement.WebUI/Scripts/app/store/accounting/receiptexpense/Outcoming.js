Ext.define('Cement.store.accounting.receiptexpense.Outcoming', {
    extend: 'Ext.data.Store',
    model: 'Cement.model.accounting.Receiptexpense',
    autoLoad: false,
    pageSize: Cement.Config.defaultPageSize,
    proxy: Cement.Config.url.accounting.receiptexpense.outcoming.proxy,
    remoteFilter: true,
    remoteSort: true
});