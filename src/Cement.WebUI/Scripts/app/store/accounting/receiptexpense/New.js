Ext.define('Cement.store.accounting.receiptexpense.New', {
    extend: 'Ext.data.Store',
    model: 'Cement.model.accounting.Receiptexpense',
    autoLoad: false,
    pageSize: Cement.Config.defaultPageSize,
    proxy: Cement.Config.url.accounting.receiptexpense.new_re.proxy,
    remoteFilter: true,
    remoteSort: true
});