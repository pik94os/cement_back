Ext.define('Cement.store.accounting.receiptexpense.Incoming', {
    extend: 'Ext.data.Store',
    model: 'Cement.model.accounting.Receiptexpense',
    autoLoad: false,
    pageSize: Cement.Config.defaultPageSize,
    proxy: Cement.Config.url.accounting.receiptexpense.incoming.proxy,
    remoteFilter: true,
    remoteSort: true
});