Ext.define('Cement.store.accounting.receiptexpense.auxiliary.PaymentDocuments', {
    extend: 'Ext.data.TreeStore',
    fields: ['id', 'p_name'],
    autoLoad: false,
    pageSize: Cement.Config.defaultPageSize,
    proxy: Cement.Config.url.accounting.receiptexpense.paymentdoc_dict.proxy,
    remoteFilter: true,
    root: {
    	text: '',
    	id: 0,
    	expanded: false
    }
});