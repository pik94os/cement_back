Ext.define('Cement.store.accounting.receiptexpense.auxiliary.SalebillKinds', {
    extend: 'Ext.data.Store',
    fields: [
        'id', 'p_name'
    ],
    autoLoad: false
});