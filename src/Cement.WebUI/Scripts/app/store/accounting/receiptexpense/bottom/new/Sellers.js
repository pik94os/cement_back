Ext.define('Cement.store.accounting.receiptexpense.bottom.new.Sellers', {
    extend: 'Ext.data.Store',
    model: 'Cement.model.Firm',
    autoLoad: false,
    pageSize: Cement.Config.defaultPageSize,
    proxy: Cement.Config.url.accounting.receiptexpense.sellers.proxy,
    remoteFilter: true,
    remoteSort: true
});