Ext.define('Cement.store.accounting.receiptexpense.bottom.new.Suppliers', {
    extend: 'Ext.data.Store',
    model: 'Cement.model.Firm',
    autoLoad: false,
    pageSize: Cement.Config.defaultPageSize,
    proxy: Cement.Config.url.accounting.receiptexpense.suppliers.proxy,
    remoteFilter: true,
    remoteSort: true
});