Ext.define('Cement.store.accounting.receiptexpense.bottom.new.Products', {
    extend: 'Ext.data.Store',
    model: 'Cement.model.products.Product',
    autoLoad: false,
    pageSize: Cement.Config.defaultPageSize,
    proxy: Cement.Config.url.accounting.receiptexpense.products.proxy,
    remoteFilter: true,
    remoteSort: true
});