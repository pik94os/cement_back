Ext.define('Cement.store.accounting.receiptexpense.bottom.new.Payers', {
    extend: 'Ext.data.Store',
    model: 'Cement.model.Firm',
    autoLoad: false,
    pageSize: Cement.Config.defaultPageSize,
    proxy: Cement.Config.url.accounting.receiptexpense.payers.proxy,
    remoteFilter: true,
    remoteSort: true
});