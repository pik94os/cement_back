Ext.define('Cement.store.accounting.receiptexpense.bottom.new.Routes', {
    extend: 'Ext.data.Store',
    model: 'Cement.model.products.Route',
    autoLoad: false,
    pageSize: Cement.Config.defaultPageSize,
    proxy: Cement.Config.url.accounting.receiptexpense.routes.proxy,
    remoteFilter: true,
    remoteSort: true
});