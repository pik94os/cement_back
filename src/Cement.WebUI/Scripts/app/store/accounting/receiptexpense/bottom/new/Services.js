Ext.define('Cement.store.accounting.receiptexpense.bottom.new.Services', {
    extend: 'Ext.data.Store',
    model: 'Cement.model.products.Product',
    autoLoad: false,
    pageSize: Cement.Config.defaultPageSize,
    proxy: Cement.Config.url.accounting.receiptexpense.services.proxy,
    remoteFilter: true,
    remoteSort: true
});