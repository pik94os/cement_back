Ext.define('Cement.store.accounting.documents.Incoming', {
    extend: 'Ext.data.Store',
    model: 'Cement.model.accounting.Document',
    autoLoad: false,
    pageSize: Cement.Config.defaultPageSize,
    proxy: Cement.Config.url.accounting.documents.incoming.proxy,
    remoteFilter: true,
    remoteSort: true
});