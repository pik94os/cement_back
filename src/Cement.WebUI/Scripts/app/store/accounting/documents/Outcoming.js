Ext.define('Cement.store.accounting.documents.Outcoming', {
    extend: 'Ext.data.Store',
    model: 'Cement.model.accounting.Document',
    autoLoad: false,
    pageSize: Cement.Config.defaultPageSize,
    proxy: Cement.Config.url.accounting.documents.outcoming.proxy,
    remoteFilter: true,
    remoteSort: true
});