Ext.define('Cement.store.accounting.documents.New', {
    extend: 'Ext.data.Store',
    model: 'Cement.model.accounting.Document',
    autoLoad: false,
    pageSize: Cement.Config.defaultPageSize,
    proxy: Cement.Config.url.accounting.documents.new_d.proxy,
    remoteFilter: true,
    remoteSort: true
});