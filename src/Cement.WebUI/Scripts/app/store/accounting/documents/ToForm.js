Ext.define('Cement.store.accounting.documents.ToForm', {
    extend: 'Ext.data.Store',
    model: 'Cement.model.accounting.Document',
    autoLoad: false,
    pageSize: Cement.Config.defaultPageSize,
    proxy: Cement.Config.url.accounting.documents.to_form.proxy,
    remoteFilter: true,
    remoteSort: true
});