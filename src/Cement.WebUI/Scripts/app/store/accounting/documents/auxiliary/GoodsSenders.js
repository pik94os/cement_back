Ext.define('Cement.store.accounting.documents.auxiliary.GoodsSenders', {
    extend: 'Ext.data.TreeStore',
    fields: ['id', 'p_name', 'p_address', 'p_inn'],
    autoLoad: false,
    pageSize: Cement.Config.defaultPageSize,
    proxy: getProxy(Cement.Config.url.accounting.documents.goods_senders_dict, ''),
    remoteFilter: true,
    root: {
      text: '',
      id: 0,
      expanded: false
    }
});