Ext.define('Cement.store.accounting.documents.auxiliary.Buyers', {
    extend: 'Ext.data.TreeStore',
    fields: ['id', 'p_name', 'p_address', 'p_inn'],
    autoLoad: false,
    pageSize: Cement.Config.defaultPageSize,
    proxy: getProxy(Cement.Config.url.accounting.documents.buyers_dict, ''),
    remoteFilter: true,
    root: {
      text: '',
      id: 0,
      expanded: false
    }
});