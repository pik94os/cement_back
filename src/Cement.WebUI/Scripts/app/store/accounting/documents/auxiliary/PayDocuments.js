Ext.define('Cement.store.accounting.documents.auxiliary.PayDocuments', {
    extend: 'Ext.data.TreeStore',
    fields: ['id', 'p_name'],
    autoLoad: false,
    pageSize: Cement.Config.defaultPageSize,
    proxy: getProxy(Cement.Config.url.accounting.documents.pay_documents_dict, ''),
    remoteFilter: true,
    root: {
        text: '',
        id: 0,
        expanded: false
    }
});