Ext.define('Cement.store.accounting.documents.auxiliary.FillByDocuments', {
    extend: 'Ext.data.TreeStore',
    // fields: ['id', 'p_name'],
    model: 'Cement.model.accounting.Document',
    autoLoad: false,
    pageSize: Cement.Config.defaultPageSize,
    proxy: getProxy(Cement.Config.url.accounting.documents.fill_by_documents_dict, ''),
    remoteFilter: true,
    root: {
        text: '',
        id: 0,
        expanded: false
    }
});