Ext.define('Cement.store.accounting.documents.bottom.outcoming.Routes', {
    extend: 'Ext.data.Store',
    model: 'Cement.model.products.Route',
    autoLoad: false,
    pageSize: Cement.Config.defaultPageSize,
    proxy: Cement.Config.url.accounting.documents.routes.proxy,
    remoteFilter: true,
    remoteSort: true
});