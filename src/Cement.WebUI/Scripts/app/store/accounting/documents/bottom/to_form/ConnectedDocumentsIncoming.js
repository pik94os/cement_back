Ext.define('Cement.store.accounting.documents.bottom.to_form.ConnectedDocumentsIncoming', {
    extend: 'Ext.data.Store',
    model: 'Cement.model.goods.ConnectedDocument',
    autoLoad: false,
    pageSize: Cement.Config.defaultPageSize,
    proxy: Cement.Config.url.accounting.documents.documents.proxy,
    remoteFilter: true,
    remoteSort: true
});