Ext.define('Cement.store.accounting.other.bottom.new.Warehouses', {
    extend: 'Ext.data.Store',
    model: 'Cement.model.Warehouse',
    autoLoad: false,
    pageSize: Cement.Config.defaultPageSize,
    proxy: Cement.Config.url.accounting.other.warehouses.proxy,
    remoteFilter: true,
    remoteSort: true
});