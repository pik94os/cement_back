Ext.define('Cement.store.accounting.other.bottom.new.Services', {
    extend: 'Ext.data.Store',
    model: 'Cement.model.products.Product',
    autoLoad: false,
    pageSize: Cement.Config.defaultPageSize,
    proxy: Cement.Config.url.accounting.other.services.proxy,
    remoteFilter: true,
    remoteSort: true
});