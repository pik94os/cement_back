Ext.define('Cement.store.accounting.other.bottom.new.Routes', {
    extend: 'Ext.data.Store',
    model: 'Cement.model.products.Route',
    autoLoad: false,
    pageSize: Cement.Config.defaultPageSize,
    proxy: Cement.Config.url.accounting.other.routes.proxy,
    remoteFilter: true,
    remoteSort: true
});