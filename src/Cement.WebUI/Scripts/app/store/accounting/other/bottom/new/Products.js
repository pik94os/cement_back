Ext.define('Cement.store.accounting.other.bottom.new.Products', {
    extend: 'Ext.data.Store',
    model: 'Cement.model.products.Product',
    autoLoad: false,
    pageSize: Cement.Config.defaultPageSize,
    proxy: Cement.Config.url.accounting.other.products.proxy,
    remoteFilter: true,
    remoteSort: true
});