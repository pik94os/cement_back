Ext.define('Cement.store.accounting.other.bottom.ConnectedDocuments', {
    extend: 'Ext.data.Store',
    model: 'Cement.model.accounting.ConnectedDocument',
    autoLoad: false,
    pageSize: Cement.Config.defaultPageSize,
    proxy: Cement.Config.url.accounting.other.documents.proxy,
    remoteFilter: true,
    remoteSort: true
});