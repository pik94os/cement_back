Ext.define('Cement.store.accounting.other.Outcoming', {
    extend: 'Ext.data.Store',
    model: 'Cement.model.accounting.Other',
    autoLoad: false,
    pageSize: Cement.Config.defaultPageSize,
    proxy: Cement.Config.url.accounting.other.outcoming.proxy,
    remoteFilter: true,
    remoteSort: true
});