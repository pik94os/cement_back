Ext.define('Cement.store.accounting.other.Archive', {
    extend: 'Ext.data.Store',
    model: 'Cement.model.accounting.Other',
    autoLoad: false,
    pageSize: Cement.Config.defaultPageSize,
    proxy: Cement.Config.url.accounting.other.archive.proxy,
    remoteFilter: true,
    remoteSort: true
});