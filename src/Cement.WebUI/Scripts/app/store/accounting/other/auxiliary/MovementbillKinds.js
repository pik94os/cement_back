Ext.define('Cement.store.accounting.other.auxiliary.MovementbillKinds', {
    extend: 'Ext.data.Store',
    fields: [
        'id', 'p_name'
    ],
    autoLoad: false
});