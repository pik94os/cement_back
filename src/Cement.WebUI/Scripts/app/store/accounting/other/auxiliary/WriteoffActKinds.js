Ext.define('Cement.store.accounting.other.auxiliary.WriteoffActKinds', {
    extend: 'Ext.data.Store',
    fields: [
        'id', 'p_name'
    ],
    autoLoad: false
});