Ext.define('Cement.store.accounting.other.Incoming', {
    extend: 'Ext.data.Store',
    model: 'Cement.model.accounting.Other',
    autoLoad: false,
    pageSize: Cement.Config.defaultPageSize,
    proxy: Cement.Config.url.accounting.other.incoming.proxy,
    remoteFilter: true,
    remoteSort: true
});