Ext.define('Cement.store.accounting.other.New', {
    extend: 'Ext.data.Store',
    model: 'Cement.model.accounting.Other',
    autoLoad: false,
    pageSize: Cement.Config.defaultPageSize,
    proxy: Cement.Config.url.accounting.other.new_o.proxy,
    remoteFilter: true,
    remoteSort: true
});