Ext.define('Cement.store.accounting.indicators.Outcoming', {
    extend: 'Ext.data.Store',
    model: 'Cement.model.accounting.Indicator',
    autoLoad: false,
    pageSize: Cement.Config.defaultPageSize,
    proxy: Cement.Config.url.accounting.indicators.outcoming.proxy,
    remoteFilter: true,
    remoteSort: true
});