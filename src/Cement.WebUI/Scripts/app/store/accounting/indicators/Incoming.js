Ext.define('Cement.store.accounting.indicators.Incoming', {
    extend: 'Ext.data.Store',
    model: 'Cement.model.accounting.Indicator',
    autoLoad: false,
    pageSize: Cement.Config.defaultPageSize,
    proxy: Cement.Config.url.accounting.indicators.incoming.proxy,
    remoteFilter: true,
    remoteSort: true
});