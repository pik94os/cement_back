Ext.define('Cement.store.accounting.indicators.New', {
    extend: 'Ext.data.Store',
    model: 'Cement.model.accounting.Indicator',
    autoLoad: false,
    pageSize: Cement.Config.defaultPageSize,
    proxy: Cement.Config.url.accounting.indicators.new_i.proxy,
    remoteFilter: true,
    remoteSort: true
});