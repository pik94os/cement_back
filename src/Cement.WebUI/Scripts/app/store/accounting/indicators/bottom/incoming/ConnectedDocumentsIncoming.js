Ext.define('Cement.store.accounting.indicators.bottom.incoming.ConnectedDocumentsIncoming', {
    extend: 'Ext.data.Store',
    model: 'Cement.model.goods.ConnectedDocument',
    autoLoad: false,
    pageSize: Cement.Config.defaultPageSize,
    proxy: Cement.Config.url.accounting.indicators.documents.proxy,
    remoteFilter: true,
    remoteSort: true
});