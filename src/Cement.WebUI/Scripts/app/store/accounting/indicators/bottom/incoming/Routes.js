Ext.define('Cement.store.accounting.indicators.bottom.incoming.Routes', {
    extend: 'Ext.data.Store',
    model: 'Cement.model.products.Route',
    autoLoad: false,
    pageSize: Cement.Config.defaultPageSize,
    proxy: Cement.Config.url.personal.adverts.routes.proxy,
    remoteFilter: true,
    remoteSort: true
});