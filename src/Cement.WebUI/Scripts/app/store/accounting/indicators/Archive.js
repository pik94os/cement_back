Ext.define('Cement.store.accounting.indicators.Archive', {
    extend: 'Ext.data.Store',
    model: 'Cement.model.accounting.Indicator',
    autoLoad: false,
    pageSize: Cement.Config.defaultPageSize,
    proxy: Cement.Config.url.accounting.indicators.archive.proxy,
    remoteFilter: true,
    remoteSort: true
});