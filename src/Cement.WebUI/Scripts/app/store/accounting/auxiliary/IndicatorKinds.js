Ext.define('Cement.store.accounting.auxiliary.IndicatorKinds', {
    extend: 'Ext.data.Store',
    fields: [
        'id', 'p_name'
    ],
    autoLoad: false
});