Ext.define('Cement.store.accounting.auxiliary.PaymentKind', {
    extend: 'Ext.data.Store',
    fields: [
        'id', 'p_name'
    ],
    autoLoad: false
});