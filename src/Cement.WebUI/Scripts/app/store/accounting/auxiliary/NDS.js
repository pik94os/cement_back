Ext.define('Cement.store.accounting.auxiliary.NDS', {
    extend: 'Ext.data.Store',
    fields: [
        'id', 'p_name'
    ],
    autoLoad: false
});