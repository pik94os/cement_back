Ext.define('Cement.store.accounting.auxiliary.DocumentKinds', {
    extend: 'Ext.data.Store',
    fields: [
        'id', 'p_name'
    ],
    autoLoad: false
});