Ext.define('Cement.store.products.Services', {
    extend: 'Ext.data.Store',
    model: 'Cement.model.products.Service',
    autoLoad: false,
    pageSize: Cement.Config.defaultPageSize,
    proxy: Cement.Config.url.products.services.proxy,
    remoteFilter: true,
    remoteSort: true
});