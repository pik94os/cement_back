Ext.define('Cement.store.products.products.ProductsGeneral', {
    extend: 'Ext.data.Store',
    model: 'Cement.model.products.Product',
    autoLoad: false,
    pageSize: Cement.Config.defaultPageSize,
    proxy: Cement.Config.url.products.products.general.proxy,
    remoteFilter: true,
    remoteSort: true
});