Ext.define('Cement.store.products.products.ProductsModeration', {
    extend: 'Ext.data.Store',
    model: 'Cement.model.products.Product',
    autoLoad: false,
    pageSize: Cement.Config.defaultPageSize,
    proxy: Cement.Config.url.products.products.moderation.proxy,
    remoteFilter: true,
    remoteSort: true
});