Ext.define('Cement.store.products.products.bottom.Products', {
    extend: 'Ext.data.Store',
    model: 'Cement.model.products.Product',
    autoLoad: false,
    pageSize: Cement.Config.defaultPageSize,
    proxy: Cement.Config.url.products.products.complect_list,
    remoteFilter: true,
    remoteSort: true
});