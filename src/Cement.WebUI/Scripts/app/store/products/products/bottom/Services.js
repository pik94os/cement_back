Ext.define('Cement.store.products.products.bottom.Services', {
    extend: 'Ext.data.Store',
    model: 'Cement.model.products.Product',
    autoLoad: false,
    pageSize: Cement.Config.defaultPageSize,
    proxy: Cement.Config.url.products.services.complect_list,
    remoteFilter: true,
    remoteSort: true
});