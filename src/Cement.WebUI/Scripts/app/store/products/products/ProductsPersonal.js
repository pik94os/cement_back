Ext.define('Cement.store.products.products.ProductsPersonal', {
    extend: 'Ext.data.Store',
    model: 'Cement.model.products.Product',
    autoLoad: false,
    pageSize: Cement.Config.defaultPageSize,
    proxy: Cement.Config.url.products.products.personal.proxy,
    remoteFilter: true,
    remoteSort: true
});