Ext.define('Cement.store.products.products.ProductsArchive', {
    extend: 'Ext.data.Store',
    model: 'Cement.model.products.Product',
    autoLoad: false,
    pageSize: Cement.Config.defaultPageSize,
    proxy: Cement.Config.url.products.products.archive.proxy,
    remoteFilter: true,
    remoteSort: true
});