Ext.define('Cement.store.products.PackType', {
    extend: 'Ext.data.Store',
    model: 'Cement.model.products.PackType',
    autoLoad: false,
    pageSize: Cement.Config.defaultPageSize,
    proxy: Cement.Config.url.products.packtype.proxy,
    remoteFilter: true,
    remoteSort: true
});