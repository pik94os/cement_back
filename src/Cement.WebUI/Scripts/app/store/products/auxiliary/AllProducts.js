Ext.define('Cement.store.products.auxiliary.AllProducts', {
    extend: 'Ext.data.Store',
    model: 'Cement.model.products.Product',
    proxy: getProxy(Cement.Config.url.products.price.all_products_dict, ''),
    remoteFilter: false,
    autoLoad: false
});