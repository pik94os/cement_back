Ext.define('Cement.store.products.auxiliary.ServiceManufacturers', {
    extend: 'Ext.data.Store',
    fields: [
        'id', 'p_name'
    ],
    autoLoad: false
});