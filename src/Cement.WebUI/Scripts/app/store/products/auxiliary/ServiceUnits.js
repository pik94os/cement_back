Ext.define('Cement.store.products.auxiliary.ServiceUnits', {
    extend: 'Ext.data.Store',
    fields: [
        'id', 'p_name'
    ],
    autoLoad: false
});