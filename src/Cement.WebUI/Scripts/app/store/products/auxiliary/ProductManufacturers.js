Ext.define('Cement.store.products.auxiliary.ProductManufacturers', {
    extend: 'Ext.data.Store',
    fields: [
        'id', 'p_name'
    ],
    autoLoad: false
});