Ext.define('Cement.store.products.auxiliary.ServiceGroups', {
    extend: 'Ext.data.TreeStore',
    autoLoad: false,
    fields: ['id', 'text'],
    root: {
      text: '',
      id: 0,
      expanded: true
    }
});