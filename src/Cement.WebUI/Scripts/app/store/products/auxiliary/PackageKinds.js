Ext.define('Cement.store.products.auxiliary.PackageKinds', {
    extend: 'Ext.data.Store',
    fields: [
        'id', 'p_name'
    ],
    autoLoad: false
});