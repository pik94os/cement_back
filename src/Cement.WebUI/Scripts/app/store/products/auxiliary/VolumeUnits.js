Ext.define('Cement.store.products.auxiliary.VolumeUnits', {
    extend: 'Ext.data.Store',
    fields: [
        'id', 'p_name'
    ],
    autoLoad: false
});