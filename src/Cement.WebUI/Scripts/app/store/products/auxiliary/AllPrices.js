Ext.define('Cement.store.products.auxiliary.AllPrices', {
    extend: 'Ext.data.Store',
    model: 'Cement.model.products.PriceList',
    proxy: getProxy(Cement.Config.url.products.price.all_prices_dict, ''),
    remoteFilter: false,
    autoLoad: false
});