Ext.define('Cement.store.products.auxiliary.PackageMaterials', {
    extend: 'Ext.data.Store',
    fields: [
        'id', 'p_name'
    ],
    autoLoad: false
});