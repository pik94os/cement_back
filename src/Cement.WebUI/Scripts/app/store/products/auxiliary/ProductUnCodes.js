Ext.define('Cement.store.products.auxiliary.ProductUnCodes', {
    extend: 'Ext.data.Store',
    fields: [
        'id', 'p_name'
    ],
    autoLoad: false
});