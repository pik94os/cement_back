Ext.define('Cement.store.products.auxiliary.ProductDangerSubClasses', {
    extend: 'Ext.data.Store',
    fields: [
    	'id', 'p_name', 'p_danger_class_id'
    ],
    proxy: getProxy(Cement.Config.url.products.danger_sub_classes_dict, ''),
    remoteFilter: true,
    autoLoad: false
});