Ext.define('Cement.store.products.auxiliary.SizeUnits', {
    extend: 'Ext.data.Store',
    fields: [
        'id', 'p_name'
    ],
    autoLoad: false
});