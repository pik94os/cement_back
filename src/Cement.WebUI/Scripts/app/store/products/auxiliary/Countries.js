Ext.define('Cement.store.products.auxiliary.Countries', {
    extend: 'Ext.data.Store',
    fields: [
        'id', 'p_name'
    ],
    autoLoad: false
});