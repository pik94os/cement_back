Ext.define('Cement.store.products.auxiliary.WeightUnits', {
    extend: 'Ext.data.Store',
    fields: [
        'id', 'p_name'
    ],
    autoLoad: false
});