﻿Ext.define('Cement.store.products.auxiliary.MeasureUnits', {
    extend: 'Ext.data.Store',
    fields: ['id', 'name'],
    data: [
        { "id": 10, "name": 'Единица длины' },
        { "id": 20, "name": 'Единица площади' },
        { "id": 30, "name": 'Единица объема' },
        { "id": 40, "name": 'Единица массы' },
        { "id": 50, "name": 'Техническая единица' },
        { "id": 60, "name": 'Единица времени' },
        { "id": 70, "name": 'Экономическая единица' }
    ]
});