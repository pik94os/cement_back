Ext.define('Cement.store.products.auxiliary.Taxes', {
    extend: 'Ext.data.Store',
    fields: [
        'id', 'p_name'
    ],
    autoLoad: false
});