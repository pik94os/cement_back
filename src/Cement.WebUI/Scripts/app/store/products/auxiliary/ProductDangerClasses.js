Ext.define('Cement.store.products.auxiliary.ProductDangerClasses', {
    extend: 'Ext.data.Store',
    fields: [
        'id', 'p_name'
    ],
    autoLoad: false
});