Ext.define('Cement.store.products.auxiliary.ProductPackers', {
    extend: 'Ext.data.Store',
    fields: [
        'id', 'p_name'
    ],
    autoLoad: false
});