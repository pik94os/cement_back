Ext.define('Cement.store.products.auxiliary.Complect', {
	extend: 'Ext.data.TreeStore',
    fields: [ 'p_name', 'p_trade_mark', 'p_manufacturer', 'p_unit', 'p_complect' ],
    autoLoad: false,
    proxy: getProxy(Cement.Config.url.products.complect_dict, ''),
    root: {
    	text: '',
    	id: 0
    }
});