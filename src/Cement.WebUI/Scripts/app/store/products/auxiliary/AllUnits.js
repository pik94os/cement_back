Ext.define('Cement.store.products.auxiliary.AllUnits', {
    extend: 'Ext.data.Store',
    fields: [
        'id', 'p_name'
    ],
    autoLoad: false
});