Ext.define('Cement.store.products.auxiliary.ProductGroups', {
    extend: 'Ext.data.TreeStore',
    autoLoad: false,
    fields: ['id', 'text'],
    root: {
      text: '',
      id: 0,
      expanded: true
    }
});