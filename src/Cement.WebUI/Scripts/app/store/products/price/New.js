Ext.define('Cement.store.products.price.New', {
    extend: 'Ext.data.Store',
    model: 'Cement.model.products.PriceList',
    autoLoad: false,
    pageSize: Cement.Config.defaultPageSize,
    proxy: Cement.Config.url.products.price.new_price.proxy,
    remoteFilter: true,
    remoteSort: true
});