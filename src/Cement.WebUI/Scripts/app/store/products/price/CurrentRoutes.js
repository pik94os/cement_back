Ext.define('Cement.store.products.price.CurrentRoutes', {
    extend: 'Ext.data.Store',
    model: 'Cement.model.products.Route',
    autoLoad: false,
    pageSize: Cement.Config.defaultPageSize,
    proxy: Cement.Config.url.products.price.routes.proxy,
    remoteFilter: true,
    remoteSort: true
});