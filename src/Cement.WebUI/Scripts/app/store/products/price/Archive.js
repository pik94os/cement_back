Ext.define('Cement.store.products.price.Archive', {
    extend: 'Ext.data.Store',
    model: 'Cement.model.products.PriceList',
    autoLoad: false,
    pageSize: Cement.Config.defaultPageSize,
    proxy: Cement.Config.url.products.price.archive.proxy,
    remoteFilter: true,
    remoteSort: true
});