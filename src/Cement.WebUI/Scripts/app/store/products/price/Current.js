Ext.define('Cement.store.products.price.Current', {
    extend: 'Ext.data.Store',
    model: 'Cement.model.products.PriceList',
    autoLoad: false,
    pageSize: Cement.Config.defaultPageSize,
    proxy: Cement.Config.url.products.price.current.proxy,
    remoteFilter: true,
    remoteSort: true
});