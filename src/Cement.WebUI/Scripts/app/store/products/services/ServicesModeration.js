Ext.define('Cement.store.products.services.ServicesModeration', {
    extend: 'Ext.data.Store',
    model: 'Cement.model.products.Service',
    autoLoad: false,
    pageSize: Cement.Config.defaultPageSize,
    proxy: Cement.Config.url.products.services.moderation.proxy,
    remoteFilter: true,
    remoteSort: true
});