Ext.define('Cement.store.products.services.ServicesPersonal', {
    extend: 'Ext.data.Store',
    model: 'Cement.model.products.Service',
    autoLoad: false,
    pageSize: Cement.Config.defaultPageSize,
    proxy: Cement.Config.url.products.services.personal.proxy,
    remoteFilter: true,
    remoteSort: true
});