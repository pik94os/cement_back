Ext.define('Cement.store.products.services.ServicesGeneral', {
    extend: 'Ext.data.Store',
    model: 'Cement.model.products.Service',
    autoLoad: false,
    pageSize: Cement.Config.defaultPageSize,
    proxy: Cement.Config.url.products.services.general.proxy,
    remoteFilter: true,
    remoteSort: true
});