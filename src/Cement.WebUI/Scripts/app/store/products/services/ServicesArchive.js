Ext.define('Cement.store.products.services.ServicesArchive', {
    extend: 'Ext.data.Store',
    model: 'Cement.model.products.Service',
    autoLoad: false,
    pageSize: Cement.Config.defaultPageSize,
    proxy: Cement.Config.url.products.services.archive.proxy,
    remoteFilter: true,
    remoteSort: true
});