Ext.define('Cement.store.office_work.documents.New', {
    extend: 'Ext.data.Store',
    model: 'Cement.model.office_work.Document',
    autoLoad: false,
    pageSize: Cement.Config.defaultPageSize,
    proxy: Cement.Config.url.office_work.documents.new_d.proxy,
    remoteFilter: true,
    remoteSort: true
});