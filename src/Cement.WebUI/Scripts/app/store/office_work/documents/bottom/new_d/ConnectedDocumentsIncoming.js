Ext.define('Cement.store.office_work.documents.bottom.new_d.ConnectedDocumentsIncoming', {
    extend: 'Ext.data.Store',
    model: 'Cement.model.goods.ConnectedDocument',
    autoLoad: false,
    pageSize: Cement.Config.defaultPageSize,
    proxy: Cement.Config.url.office_work.documents.documents.proxy,
    remoteFilter: true,
    remoteSort: true
});