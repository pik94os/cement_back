Ext.define('Cement.store.office_work.documents.Incoming', {
    extend: 'Ext.data.Store',
    model: 'Cement.model.office_work.Document',
    autoLoad: false,
    pageSize: Cement.Config.defaultPageSize,
    proxy: Cement.Config.url.office_work.documents.incoming.proxy,
    remoteFilter: true,
    remoteSort: true
});