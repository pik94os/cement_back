Ext.define('Cement.store.office_work.disposal.bottom.archive.ConnectedDocuments', {
    extend: 'Ext.data.Store',
    model: 'Cement.model.goods.ConnectedDocument',
    autoLoad: false,
    pageSize: Cement.Config.defaultPageSize,
    proxy: Cement.Config.url.office_work.disposal.documents.proxy,
    remoteFilter: true,
    remoteSort: true
});