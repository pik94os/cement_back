Ext.define('Cement.store.office_work.disposal.bottom.archive.Routes', {
    extend: 'Ext.data.Store',
    model: 'Cement.model.products.Route',
    autoLoad: false,
    pageSize: Cement.Config.defaultPageSize,
    proxy: Cement.Config.url.office_work.disposal.routes.proxy,
    remoteFilter: true,
    remoteSort: true
});