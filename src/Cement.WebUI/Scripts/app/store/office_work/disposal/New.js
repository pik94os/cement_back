Ext.define('Cement.store.office_work.disposal.New', {
    extend: 'Ext.data.Store',
    model: 'Cement.model.office_work.Disposal',
    autoLoad: false,
    pageSize: Cement.Config.defaultPageSize,
    proxy: Cement.Config.url.office_work.disposal.new_d.proxy,
    remoteFilter: true,
    remoteSort: true
});