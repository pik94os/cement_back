Ext.define('Cement.store.office_work.disposal.Outcoming', {
    extend: 'Ext.data.Store',
    model: 'Cement.model.office_work.Disposal',
    autoLoad: false,
    pageSize: Cement.Config.defaultPageSize,
    proxy: Cement.Config.url.office_work.disposal.outcoming.proxy,
    remoteFilter: true,
    remoteSort: true
});