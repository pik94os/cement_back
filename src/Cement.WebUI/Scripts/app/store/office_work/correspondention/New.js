Ext.define('Cement.store.office_work.correspondention.New', {
    extend: 'Ext.data.Store',
    model: 'Cement.model.office_work.Correspondention',
    autoLoad: false,
    pageSize: Cement.Config.defaultPageSize,
    proxy: Cement.Config.url.office_work.correspondention.new_c.proxy,
    remoteFilter: true,
    remoteSort: true
});