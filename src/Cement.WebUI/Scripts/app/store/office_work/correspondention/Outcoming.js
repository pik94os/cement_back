Ext.define('Cement.store.office_work.correspondention.Outcoming', {
    extend: 'Ext.data.Store',
    model: 'Cement.model.office_work.Correspondention',
    autoLoad: false,
    pageSize: Cement.Config.defaultPageSize,
    proxy: Cement.Config.url.office_work.correspondention.outcoming.proxy,
    remoteFilter: true,
    remoteSort: true
});