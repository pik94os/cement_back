Ext.define('Cement.store.office_work.correspondention.Archive', {
    extend: 'Ext.data.Store',
    model: 'Cement.model.office_work.Correspondention',
    autoLoad: false,
    pageSize: Cement.Config.defaultPageSize,
    proxy: Cement.Config.url.office_work.correspondention.archive.proxy,
    remoteFilter: true,
    remoteSort: true
});