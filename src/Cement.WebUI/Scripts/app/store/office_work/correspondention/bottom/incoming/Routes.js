Ext.define('Cement.store.office_work.correspondention.bottom.incoming.Routes', {
    extend: 'Ext.data.Store',
    model: 'Cement.model.products.Route',
    autoLoad: false,
    pageSize: Cement.Config.defaultPageSize,
    proxy: Cement.Config.url.office_work.correspondention.routes.proxy,
    remoteFilter: true,
    remoteSort: true
});