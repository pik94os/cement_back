Ext.define('Cement.store.office_work.correspondention.bottom.new_c.ConnectedDocumentsIncoming', {
    extend: 'Ext.data.Store',
    model: 'Cement.model.goods.ConnectedDocument',
    autoLoad: false,
    pageSize: Cement.Config.defaultPageSize,
    proxy: Cement.Config.url.office_work.correspondention.documents.proxy,
    remoteFilter: true,
    remoteSort: true
});