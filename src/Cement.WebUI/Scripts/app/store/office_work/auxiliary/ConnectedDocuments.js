Ext.define('Cement.store.office_work.auxiliary.ConnectedDocuments', {
    extend: 'Ext.data.TreeStore',
    fields: ['id', 'p_name'],
    autoLoad: false,
    proxy: getProxy(Cement.Config.url.office_work.documents.documents_dict, ''),
    remoteFilter: true,
    root: {
        text: '',
        id: 0,
        expanded: false
    }
});