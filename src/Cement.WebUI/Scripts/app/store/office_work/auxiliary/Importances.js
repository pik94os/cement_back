Ext.define('Cement.store.office_work.auxiliary.Importances', {
    extend: 'Ext.data.Store',
    fields: [
        'id', 'p_name'
    ],
    autoLoad: false
});