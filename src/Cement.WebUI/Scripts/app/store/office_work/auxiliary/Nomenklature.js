Ext.define('Cement.store.office_work.auxiliary.Nomenklature', {
    extend: 'Ext.data.TreeStore',
    fields: ['id', 'p_name'],
    autoLoad: false,
    proxy: getProxy(Cement.Config.url.office_work.documents.nomenklatures_dict, ''),
    remoteFilter: true,
    root: {
        text: '',
        id: 0,
        expanded: false
    }
});