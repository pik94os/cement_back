Ext.define('Cement.store.office_work.auxiliary.Securities', {
    extend: 'Ext.data.Store',
    fields: [
        'id', 'p_name'
    ],
    autoLoad: false
});