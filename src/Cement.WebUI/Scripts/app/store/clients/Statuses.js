Ext.define('Cement.store.clients.Statuses', {
    extend: 'Ext.data.Store',
    fields: [ 'id', 'p_name' ],
    autoLoad: false,
    pageSize: Cement.Config.defaultPageSize,
    proxy: Cement.Config.url.clients.statuses_dict,
    remoteFilter: true
});