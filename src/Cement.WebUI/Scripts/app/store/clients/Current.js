Ext.define('Cement.store.clients.Current', {
    extend: 'Ext.data.Store',
    model: 'Cement.model.Client',
    autoLoad: false,
    pageSize: Cement.Config.defaultPageSize,
    proxy: Cement.Config.url.clients.current.proxy,
    remoteFilter: true,
    remoteSort: true
});