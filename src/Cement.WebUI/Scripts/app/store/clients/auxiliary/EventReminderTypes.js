Ext.define('Cement.store.clients.auxiliary.EventReminderTypes', {
    extend: 'Ext.data.Store',
    fields: [
        'id', 'p_name'
    ],
    autoLoad: false
});