Ext.define('Cement.store.clients.groups.List', {
    extend: 'Ext.data.Store',
    model: 'Cement.model.clients.Group',
    autoLoad: false,
    pageSize: Cement.Config.defaultPageSize,
    proxy: Cement.Config.url.clients.groups.proxy,
    remoteFilter: true,
    remoteSort: true
});