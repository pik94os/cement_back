Ext.define('Cement.store.clients.subgroups.List', {
    extend: 'Ext.data.Store',
    model: 'Cement.model.clients.SubGroup',
    autoLoad: false,
    pageSize: Cement.Config.defaultPageSize,
    proxy: Cement.Config.url.clients.sub_groups.proxy,
    remoteFilter: true,
    remoteSort: true
});