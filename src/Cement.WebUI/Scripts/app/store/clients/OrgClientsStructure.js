Ext.define('Cement.store.clients.OrgClientsStructure', {
    extend: 'Ext.data.TreeStore',
    fields: ['id', 'p_client', 'p_name', 'p_address', 'p_kpp', 'p_inn', 'p_level'],
    autoLoad: false,
    pageSize: Cement.Config.defaultPageSize,
    proxy: Cement.Config.url.clients.orgClientsStructure,
    remoteFilter: true,
    root: {
    	text: '',
    	id: 0,
    	expanded: false
    }
});