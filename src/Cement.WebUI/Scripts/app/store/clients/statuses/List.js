Ext.define('Cement.store.clients.statuses.List', {
    extend: 'Ext.data.Store',
    model: 'Cement.model.clients.Status',
    autoLoad: false,
    pageSize: Cement.Config.defaultPageSize,
    proxy: Cement.Config.url.clients.statuses.proxy,
    remoteFilter: true,
    remoteSort: true
});