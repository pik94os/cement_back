Ext.define('Cement.store.clients.Corporate', {
    extend: 'Ext.data.Store',
    model: 'Cement.model.Client',
    autoLoad: false,
    pageSize: Cement.Config.defaultPageSize,
    proxy: Cement.Config.url.clients.corporate.proxy,
    remoteFilter: true,
    remoteSort: true
});