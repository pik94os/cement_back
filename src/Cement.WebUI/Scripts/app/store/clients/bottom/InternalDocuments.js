Ext.define('Cement.store.clients.bottom.InternalDocuments', {
    extend: 'Ext.data.Store',
    model: 'Cement.model.office_work.Document',
    autoLoad: false,
    pageSize: Cement.Config.defaultPageSize,
    proxy: Cement.Config.url.office_work.documents.new_d.proxy,
    remoteFilter: true,
    remoteSort: true
});