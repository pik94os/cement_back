Ext.define('Cement.store.clients.archive.bottom.Warehouses', {
    extend: 'Ext.data.Store',
    model: 'Cement.model.Warehouse',
    autoLoad: false,
    pageSize: Cement.Config.defaultPageSize,
    proxy: Cement.Config.url.corporate.warehouses.proxy,
    remoteFilter: true,
    remoteSort: true
});