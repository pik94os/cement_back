Ext.define('Cement.store.clients.archive.Firms', {
    extend: 'Ext.data.Store',
    model: 'Cement.model.Firm',
    autoLoad: false,
    pageSize: Cement.Config.defaultPageSize,
    proxy: Cement.Config.url.clients.firms.proxy,
    remoteFilter: true,
    remoteSort: true
});