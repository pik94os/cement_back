Ext.define('Cement.store.clients.archive.People', {
    extend: 'Ext.data.Store',
    model: 'Cement.model.clients.Employee',
    autoLoad: false,
    pageSize: Cement.Config.defaultPageSize,
    proxy: Cement.Config.url.clients.people.proxy,
    remoteFilter: true,
    remoteSort: true
});