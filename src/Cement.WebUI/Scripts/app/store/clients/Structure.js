Ext.define('Cement.store.clients.Structure', {
    extend: 'Ext.data.TreeStore',
    fields: ['id', 'p_name', 'p_address', 'p_kpp', 'p_inn'],
    autoLoad: false,
    pageSize: Cement.Config.defaultPageSize,
    proxy: Cement.Config.url.clients.structure,
    remoteFilter: true,
    root: {
    	text: '',
    	id: 0,
    	expanded: false
    }
});