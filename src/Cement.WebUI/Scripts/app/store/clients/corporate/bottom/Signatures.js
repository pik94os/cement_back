Ext.define('Cement.store.clients.corporate.bottom.Signatures', {
    extend: 'Ext.data.Store',
    model: 'Cement.model.Signature',
    autoLoad: false,
    pageSize: Cement.Config.defaultPageSize,
    proxy: Cement.Config.url.corporate.signatures.proxy,
    remoteFilter: true,
    remoteSort: true
});