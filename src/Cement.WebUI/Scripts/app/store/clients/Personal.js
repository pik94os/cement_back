Ext.define('Cement.store.clients.Personal', {
    extend: 'Ext.data.Store',
    model: 'Cement.model.Client',
    autoLoad: false,
    pageSize: Cement.Config.defaultPageSize,
    proxy: Cement.Config.url.clients.personal.proxy,
    remoteFilter: true,
    remoteSort: true
});