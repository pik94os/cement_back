Ext.define('Cement.store.clients.current.bottom.Employees', {
    extend: 'Ext.data.Store',
    model: 'Cement.model.Employee',
    autoLoad: false,
    pageSize: Cement.Config.defaultPageSize,
    proxy: Cement.Config.url.corporate.employes.proxy,
    remoteFilter: true,
    remoteSort: true
});