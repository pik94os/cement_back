Ext.define('Cement.store.clients.Groups', {
    extend: 'Ext.data.TreeStore',
    autoLoad: false,
    fields: ['id', 'text', 'p_name'],
    proxy: Cement.Config.url.clients.groups_tree,
    root: {
      text: '',
      id: 0,
      expanded: true
    }
});