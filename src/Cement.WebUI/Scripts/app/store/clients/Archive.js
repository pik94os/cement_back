Ext.define('Cement.store.clients.Archive', {
    extend: 'Ext.data.Store',
    model: 'Cement.model.Client',
    autoLoad: false,
    pageSize: Cement.Config.defaultPageSize,
    proxy: Cement.Config.url.clients.archive.proxy,
    remoteFilter: true,
    remoteSort: true
});