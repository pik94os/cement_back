Ext.define('Cement.store.clients.management.ConnectedDocuments', {
    extend: 'Ext.data.Store',
    model: 'Cement.model.goods.ConnectedDocument',
    autoLoad: false,
    pageSize: Cement.Config.defaultPageSize,
    proxy: Cement.Config.url.clients.management.documents.proxy,
    remoteFilter: true,
    remoteSort: true
});