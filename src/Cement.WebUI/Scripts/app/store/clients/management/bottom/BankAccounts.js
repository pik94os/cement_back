Ext.define('Cement.store.clients.management.bottom.BankAccounts', {
    extend: 'Ext.data.Store',
    model: 'Cement.model.BankAccount',
    autoLoad: false,
    pageSize: Cement.Config.defaultPageSize,
    proxy: Cement.Config.url.corporate.bank_accounts.proxy,
    remoteFilter: true,
    remoteSort: true
});