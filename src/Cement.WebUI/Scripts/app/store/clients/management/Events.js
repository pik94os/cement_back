Ext.define('Cement.store.clients.management.events', {
    extend: 'Ext.data.Store',
    model: 'Cement.model.clients.Management',
    autoLoad: false,
    pageSize: Cement.Config.defaultPageSize,
    proxy: Cement.Config.url.clients.management.events.proxy,
    remoteFilter: true,
    remoteSort: true
});