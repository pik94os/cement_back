Ext.define('Cement.store.warehouse.internal.Outcoming', {
    extend: 'Ext.data.Store',
    model: 'Cement.model.warehouse.InternalRequest',
    autoLoad: false,
    pageSize: Cement.Config.defaultPageSize,
    proxy: Cement.Config.url.warehouse.internal.outcoming.proxy,
    remoteFilter: true,
    remoteSort: true
});