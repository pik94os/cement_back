Ext.define('Cement.store.warehouse.internal.Archive', {
    extend: 'Ext.data.Store',
    model: 'Cement.model.warehouse.InternalRequest',
    autoLoad: false,
    pageSize: Cement.Config.defaultPageSize,
    proxy: Cement.Config.url.warehouse.internal.archive.proxy,
    remoteFilter: true,
    remoteSort: true
});