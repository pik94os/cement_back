Ext.define('Cement.store.warehouse.internal.Incoming', {
    extend: 'Ext.data.Store',
    model: 'Cement.model.warehouse.InternalRequest',
    autoLoad: false,
    pageSize: Cement.Config.defaultPageSize,
    proxy: Cement.Config.url.warehouse.internal.incoming.proxy,
    remoteFilter: true,
    remoteSort: true
});