Ext.define('Cement.store.warehouse.internal.bottom.new.Services', {
    extend: 'Ext.data.Store',
    model: 'Cement.model.products.Product',
    autoLoad: false,
    pageSize: Cement.Config.defaultPageSize,
    proxy: Cement.Config.url.warehouse.internal.services.proxy,
    remoteFilter: true,
    remoteSort: true
});