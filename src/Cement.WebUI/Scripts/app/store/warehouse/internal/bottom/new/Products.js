Ext.define('Cement.store.warehouse.internal.bottom.new.Products', {
    extend: 'Ext.data.Store',
    model: 'Cement.model.products.Product',
    autoLoad: false,
    pageSize: Cement.Config.defaultPageSize,
    proxy: Cement.Config.url.warehouse.internal.products.proxy,
    remoteFilter: true,
    remoteSort: true
});