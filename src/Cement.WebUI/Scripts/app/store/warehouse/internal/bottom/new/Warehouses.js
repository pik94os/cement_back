Ext.define('Cement.store.warehouse.internal.bottom.new.Warehouses', {
    extend: 'Ext.data.Store',
    model: 'Cement.model.Warehouse',
    autoLoad: false,
    pageSize: Cement.Config.defaultPageSize,
    proxy: Cement.Config.url.warehouse.internal.warehouses.proxy,
    remoteFilter: true,
    remoteSort: true
});