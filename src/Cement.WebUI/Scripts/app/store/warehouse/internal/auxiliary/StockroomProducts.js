Ext.define('Cement.store.warehouse.internal.auxiliary.StockroomProducts', {
    extend: 'Ext.data.Store',
    fields: ['Id', 'Name', 'UnitName'],
    autoLoad: false,
    pageSize: Cement.Config.defaultPageSize,
    proxy: Cement.Config.url.warehouse.internal.stockroom_products_proxy,
    remoteFilter: true,
    remoteSort: true
});