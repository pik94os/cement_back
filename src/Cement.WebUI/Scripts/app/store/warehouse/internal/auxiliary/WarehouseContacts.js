Ext.define('Cement.store.warehouse.internal.auxiliary.WarehouseContacts', {
    extend: 'Ext.data.TreeStore',
    fields: ['id', 'p_name', 'p_position', 'p_department'],
    autoLoad: false,
    pageSize: Cement.Config.defaultPageSize,
    proxy: getProxy(Cement.Config.url.warehouse.internal.warehouse_contacts_dict, ''),
    remoteFilter: true,
    root: {
    	text: '',
    	id: 0,
    	expanded: false
    }
});