Ext.define('Cement.store.warehouse.internal.auxiliary.Products', {
    extend: 'Ext.data.TreeStore',
    fields: ['id', 'p_name', 'p_product_unit', 'p_price', 'p_tax', 'p_pricelist_product_id', 'p_product_unit_display', 'p_pricelist_product'],
    autoLoad: false,
    pageSize: Cement.Config.defaultPageSize,
    proxy: getProxy(Cement.Config.url.warehouse.internal.pricelist_dict, ''),
    remoteFilter: true,
    root: {
        text: '',
        id: 0,
        expanded: false
    }
});