Ext.define('Cement.store.warehouse.internal.New', {
    extend: 'Ext.data.Store',
    model: 'Cement.model.warehouse.InternalRequest',
    autoLoad: false,
    pageSize: Cement.Config.defaultPageSize,
    proxy: Cement.Config.url.warehouse.internal.new_i.proxy,
    remoteFilter: true,
    remoteSort: true
});