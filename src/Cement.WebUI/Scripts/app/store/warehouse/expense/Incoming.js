Ext.define('Cement.store.warehouse.expense.Incoming', {
    extend: 'Ext.data.Store',
    model: 'Cement.model.warehouse.Expense',
    autoLoad: false,
    pageSize: Cement.Config.defaultPageSize,
    proxy: Cement.Config.url.warehouse.expense.incoming.proxy,
    remoteFilter: true,
    remoteSort: true
});