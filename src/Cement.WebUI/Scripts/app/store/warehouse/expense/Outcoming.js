Ext.define('Cement.store.warehouse.expense.Outcoming', {
    extend: 'Ext.data.Store',
    model: 'Cement.model.warehouse.Expense',
    autoLoad: false,
    pageSize: Cement.Config.defaultPageSize,
    proxy: Cement.Config.url.warehouse.expense.outcoming.proxy,
    remoteFilter: true,
    remoteSort: true
});