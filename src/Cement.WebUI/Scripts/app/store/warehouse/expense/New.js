Ext.define('Cement.store.warehouse.expense.New', {
    extend: 'Ext.data.Store',
    model: 'Cement.model.warehouse.Expense',
    autoLoad: false,
    pageSize: Cement.Config.defaultPageSize,
    proxy: Cement.Config.url.warehouse.expense.new_i.proxy,
    remoteFilter: true,
    remoteSort: true
});