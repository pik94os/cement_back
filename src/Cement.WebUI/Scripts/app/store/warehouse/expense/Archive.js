Ext.define('Cement.store.warehouse.expense.Archive', {
    extend: 'Ext.data.Store',
    model: 'Cement.model.warehouse.Expense',
    autoLoad: false,
    pageSize: Cement.Config.defaultPageSize,
    proxy: Cement.Config.url.warehouse.expense.archive.proxy,
    remoteFilter: true,
    remoteSort: true
});