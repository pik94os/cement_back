Ext.define('Cement.store.warehouse.expense.bottom.new.Providers', {
    extend: 'Ext.data.Store',
    model: 'Cement.model.Firm',
    autoLoad: false,
    pageSize: Cement.Config.defaultPageSize,
    proxy: Cement.Config.url.warehouse.expense.providers.proxy,
    remoteFilter: true,
    remoteSort: true
});