Ext.define('Cement.store.warehouse.expense.bottom.new.Routes', {
    extend: 'Ext.data.Store',
    model: 'Cement.model.products.Route',
    autoLoad: false,
    pageSize: Cement.Config.defaultPageSize,
    proxy: Cement.Config.url.warehouse.expense.routes.proxy,
    remoteFilter: true,
    remoteSort: true
});