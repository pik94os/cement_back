Ext.define('Cement.store.warehouse.expense.bottom.ConnectedDocuments', {
    extend: 'Ext.data.Store',
    model: 'Cement.model.warehouse.ConnectedDocument',
    autoLoad: false,
    pageSize: Cement.Config.defaultPageSize,
    proxy: Cement.Config.url.warehouse.expense.documents.proxy,
    remoteFilter: true,
    remoteSort: true
});