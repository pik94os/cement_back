Ext.define('Cement.store.warehouse.inventory.New', {
    extend: 'Ext.data.Store',
    model: 'Cement.model.warehouse.Inventory',
    autoLoad: false,
    pageSize: Cement.Config.defaultPageSize,
    proxy: Cement.Config.url.warehouse.inventory.new_i.proxy,
    remoteFilter: true,
    remoteSort: true
});