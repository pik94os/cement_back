Ext.define('Cement.store.warehouse.inventory.auxiliary.InventoryKinds', {
    extend: 'Ext.data.Store',
    fields: [
        'id', 'p_name'
    ],
    autoLoad: false
});