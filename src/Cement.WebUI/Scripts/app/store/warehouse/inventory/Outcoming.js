Ext.define('Cement.store.warehouse.inventory.Outcoming', {
    extend: 'Ext.data.Store',
    model: 'Cement.model.warehouse.Inventory',
    autoLoad: false,
    pageSize: Cement.Config.defaultPageSize,
    proxy: Cement.Config.url.warehouse.inventory.outcoming.proxy,
    remoteFilter: true,
    remoteSort: true
});