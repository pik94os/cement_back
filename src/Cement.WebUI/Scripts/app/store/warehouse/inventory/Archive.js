Ext.define('Cement.store.warehouse.inventory.Archive', {
    extend: 'Ext.data.Store',
    model: 'Cement.model.warehouse.Inventory',
    autoLoad: false,
    pageSize: Cement.Config.defaultPageSize,
    proxy: Cement.Config.url.warehouse.inventory.archive.proxy,
    remoteFilter: true,
    remoteSort: true
});