Ext.define('Cement.store.warehouse.inventory.Incoming', {
    extend: 'Ext.data.Store',
    model: 'Cement.model.warehouse.Inventory',
    autoLoad: false,
    pageSize: Cement.Config.defaultPageSize,
    proxy: Cement.Config.url.warehouse.inventory.incoming.proxy,
    remoteFilter: true,
    remoteSort: true
});