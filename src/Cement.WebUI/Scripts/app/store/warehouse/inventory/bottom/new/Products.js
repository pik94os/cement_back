Ext.define('Cement.store.warehouse.inventory.bottom.new.Products', {
    extend: 'Ext.data.Store',
    model: 'Cement.model.products.Product',
    autoLoad: false,
    pageSize: Cement.Config.defaultPageSize,
    proxy: Cement.Config.url.warehouse.inventory.products.proxy,
    remoteFilter: true,
    remoteSort: true
});