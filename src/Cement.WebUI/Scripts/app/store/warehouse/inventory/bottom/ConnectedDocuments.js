Ext.define('Cement.store.warehouse.inventory.bottom.ConnectedDocuments', {
    extend: 'Ext.data.Store',
    model: 'Cement.model.warehouse.ConnectedDocument',
    autoLoad: false,
    pageSize: Cement.Config.defaultPageSize,
    proxy: Cement.Config.url.warehouse.inventory.documents.proxy,
    remoteFilter: true,
    remoteSort: true
});