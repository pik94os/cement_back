Ext.define('Cement.store.warehouse.inventory.bottom.new.Routes', {
    extend: 'Ext.data.Store',
    model: 'Cement.model.products.Route',
    autoLoad: false,
    pageSize: Cement.Config.defaultPageSize,
    proxy: Cement.Config.url.warehouse.inventory.routes.proxy,
    remoteFilter: true,
    remoteSort: true
});