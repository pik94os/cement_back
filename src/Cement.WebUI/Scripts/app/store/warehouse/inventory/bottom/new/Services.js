Ext.define('Cement.store.warehouse.inventory.bottom.new.Services', {
    extend: 'Ext.data.Store',
    model: 'Cement.model.products.Product',
    autoLoad: false,
    pageSize: Cement.Config.defaultPageSize,
    proxy: Cement.Config.url.warehouse.inventory.services.proxy,
    remoteFilter: true,
    remoteSort: true
});