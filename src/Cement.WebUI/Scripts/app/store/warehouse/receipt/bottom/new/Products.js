Ext.define('Cement.store.warehouse.receipt.bottom.new.Products', {
    extend: 'Ext.data.Store',
    model: 'Cement.model.products.Product',
    autoLoad: false,
    pageSize: Cement.Config.defaultPageSize,
    proxy: Cement.Config.url.warehouse.receipt.products.proxy,
    remoteFilter: true,
    remoteSort: true
});