Ext.define('Cement.store.warehouse.receipt.bottom.new.Services', {
    extend: 'Ext.data.Store',
    model: 'Cement.model.products.Product',
    autoLoad: false,
    pageSize: Cement.Config.defaultPageSize,
    proxy: Cement.Config.url.warehouse.receipt.services.proxy,
    remoteFilter: true,
    remoteSort: true
});