Ext.define('Cement.store.warehouse.receipt.bottom.new.Routes', {
    extend: 'Ext.data.Store',
    model: 'Cement.model.products.Route',
    autoLoad: false,
    pageSize: Cement.Config.defaultPageSize,
    proxy: Cement.Config.url.warehouse.receipt.routes.proxy,
    remoteFilter: true,
    remoteSort: true
});