Ext.define('Cement.store.warehouse.receipt.bottom.new.Providers', {
    extend: 'Ext.data.Store',
    model: 'Cement.model.Firm',
    autoLoad: false,
    pageSize: Cement.Config.defaultPageSize,
    proxy: Cement.Config.url.warehouse.receipt.providers.proxy,
    remoteFilter: true,
    remoteSort: true
});