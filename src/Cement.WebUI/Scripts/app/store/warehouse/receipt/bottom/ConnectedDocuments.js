Ext.define('Cement.store.warehouse.receipt.bottom.ConnectedDocuments', {
    extend: 'Ext.data.Store',
    model: 'Cement.model.warehouse.ConnectedDocument',
    autoLoad: false,
    pageSize: Cement.Config.defaultPageSize,
    proxy: Cement.Config.url.warehouse.receipt.documents.proxy,
    remoteFilter: true,
    remoteSort: true
});