Ext.define('Cement.store.warehouse.receipt.Incoming', {
    extend: 'Ext.data.Store',
    model: 'Cement.model.warehouse.Receipt',
    autoLoad: false,
    pageSize: Cement.Config.defaultPageSize,
    proxy: Cement.Config.url.warehouse.receipt.incoming.proxy,
    remoteFilter: true,
    remoteSort: true
});