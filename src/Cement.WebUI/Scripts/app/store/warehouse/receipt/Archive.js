Ext.define('Cement.store.warehouse.receipt.Archive', {
    extend: 'Ext.data.Store',
    model: 'Cement.model.warehouse.Receipt',
    autoLoad: false,
    pageSize: Cement.Config.defaultPageSize,
    proxy: Cement.Config.url.warehouse.receipt.archive.proxy,
    remoteFilter: true,
    remoteSort: true
});