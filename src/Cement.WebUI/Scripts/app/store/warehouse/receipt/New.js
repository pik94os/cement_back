Ext.define('Cement.store.warehouse.receipt.New', {
    extend: 'Ext.data.Store',
    model: 'Cement.model.warehouse.Receipt',
    autoLoad: false,
    pageSize: Cement.Config.defaultPageSize,
    proxy: Cement.Config.url.warehouse.receipt.new_i.proxy,
    remoteFilter: true,
    remoteSort: true
});