Ext.define('Cement.store.warehouse.receipt.Outcoming', {
    extend: 'Ext.data.Store',
    model: 'Cement.model.warehouse.Receipt',
    autoLoad: false,
    pageSize: Cement.Config.defaultPageSize,
    proxy: Cement.Config.url.warehouse.receipt.outcoming.proxy,
    remoteFilter: true,
    remoteSort: true
});