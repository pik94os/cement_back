Ext.define('Cement.store.warehouse.movement.bottom.new.Movements', {
    extend: 'Ext.data.Store',
    model: 'Cement.model.warehouse.Movement',
    autoLoad: false,
    pageSize: Cement.Config.defaultPageSize,
    proxy: Cement.Config.url.warehouse.movement.movements.proxy,
    remoteFilter: true,
    remoteSort: true
});