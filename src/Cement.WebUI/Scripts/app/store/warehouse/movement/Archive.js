Ext.define('Cement.store.warehouse.movement.Archive', {
    extend: 'Ext.data.Store',
    model: 'Cement.model.warehouse.Movement',
    autoLoad: false,
    pageSize: Cement.Config.defaultPageSize,
    proxy: Cement.Config.url.warehouse.movement.archive.proxy,
    remoteFilter: true,
    remoteSort: true
});