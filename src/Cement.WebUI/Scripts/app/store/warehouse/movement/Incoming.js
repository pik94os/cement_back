Ext.define('Cement.store.warehouse.movement.Incoming', {
    extend: 'Ext.data.Store',
    model: 'Cement.model.warehouse.Movement',
    autoLoad: false,
    pageSize: Cement.Config.defaultPageSize,
    proxy: Cement.Config.url.warehouse.movement.incoming.proxy,
    remoteFilter: true,
    remoteSort: true
});