Ext.define('Cement.store.warehouse.movement.Outcoming', {
    extend: 'Ext.data.Store',
    model: 'Cement.model.warehouse.Movement',
    autoLoad: false,
    pageSize: Cement.Config.defaultPageSize,
    proxy: Cement.Config.url.warehouse.movement.outcoming.proxy,
    remoteFilter: true,
    remoteSort: true
});