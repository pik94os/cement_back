Ext.define('Cement.store.warehouse.movement.New', {
    extend: 'Ext.data.Store',
    model: 'Cement.model.warehouse.Movement',
    autoLoad: false,
    pageSize: Cement.Config.defaultPageSize,
    proxy: Cement.Config.url.warehouse.movement.new_i.proxy,
    remoteFilter: true,
    remoteSort: true
});