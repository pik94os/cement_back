Ext.define('Cement.store.goods.auxiliary.Securities', {
    extend: 'Ext.data.Store',
    fields: [
        'id', 'p_name'
    ],
    autoLoad: false
});