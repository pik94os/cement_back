Ext.define('Cement.store.goods.auxiliary.SecuritiesColored', {
    extend: 'Ext.data.Store',
    fields: [
        'id', 'p_name', 'flag'
    ],
    autoLoad: false
});