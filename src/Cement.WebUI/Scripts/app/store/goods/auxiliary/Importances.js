Ext.define('Cement.store.goods.auxiliary.Importances', {
    extend: 'Ext.data.Store',
    fields: [
        'id', 'p_name'
    ],
    autoLoad: false
});