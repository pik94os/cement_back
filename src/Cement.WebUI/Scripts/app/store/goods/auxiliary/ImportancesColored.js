Ext.define('Cement.store.goods.auxiliary.ImportancesColored', {
    extend: 'Ext.data.Store',
    fields: [
        'id', 'p_name', 'flag'
    ],
    autoLoad: false
});