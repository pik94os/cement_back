Ext.define('Cement.store.goods.services.New', {
    extend: 'Ext.data.Store',
    model: 'Cement.model.goods.Service',
    autoLoad: false,
    pageSize: Cement.Config.defaultPageSize,
    proxy: Cement.Config.url.goods.services.new_r.proxy,
    remoteFilter: true,
    remoteSort: true
});