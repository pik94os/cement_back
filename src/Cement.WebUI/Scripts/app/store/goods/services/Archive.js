Ext.define('Cement.store.goods.services.Archive', {
    extend: 'Ext.data.Store',
    model: 'Cement.model.goods.Service',
    autoLoad: false,
    pageSize: Cement.Config.defaultPageSize,
    proxy: Cement.Config.url.goods.services.archive.proxy,
    remoteFilter: true,
    remoteSort: true
});