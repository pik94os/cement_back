Ext.define('Cement.store.goods.services.Outcoming', {
    extend: 'Ext.data.Store',
    model: 'Cement.model.goods.Service',
    autoLoad: false,
    pageSize: Cement.Config.defaultPageSize,
    proxy: Cement.Config.url.goods.services.outcoming.proxy,
    remoteFilter: true,
    remoteSort: true
});