Ext.define('Cement.store.goods.services.Templates', {
    extend: 'Ext.data.Store',
    model: 'Cement.model.goods.Service',
    autoLoad: false,
    pageSize: Cement.Config.defaultPageSize,
    proxy: Cement.Config.url.goods.services.templates.proxy,
    remoteFilter: true,
    remoteSort: true
});