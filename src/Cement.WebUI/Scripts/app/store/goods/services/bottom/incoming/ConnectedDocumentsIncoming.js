Ext.define('Cement.store.goods.services.bottom.incoming.ConnectedDocumentsIncoming', {
    extend: 'Ext.data.Store',
    model: 'Cement.model.goods.ConnectedDocument',
    autoLoad: false,
    pageSize: Cement.Config.defaultPageSize,
    proxy: Cement.Config.url.goods.services.documents.proxy,
    remoteFilter: true,
    remoteSort: true
});