Ext.define('Cement.store.goods.services.bottom.outcoming.Buyers', {
    extend: 'Ext.data.Store',
    model: 'Cement.model.Firm',
    autoLoad: false,
    pageSize: Cement.Config.defaultPageSize,
    proxy: Cement.Config.url.goods.buyers.proxy,
    remoteFilter: true,
    remoteSort: true
});