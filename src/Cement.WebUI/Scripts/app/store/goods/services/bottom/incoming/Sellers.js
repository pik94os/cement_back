Ext.define('Cement.store.goods.services.bottom.incoming.Sellers', {
    extend: 'Ext.data.Store',
    model: 'Cement.model.Firm',
    autoLoad: false,
    pageSize: Cement.Config.defaultPageSize,
    proxy: Cement.Config.url.goods.sellers.proxy,
    remoteFilter: true,
    remoteSort: true
});