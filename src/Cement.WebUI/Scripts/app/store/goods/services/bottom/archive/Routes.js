Ext.define('Cement.store.goods.services.bottom.archive.Routes', {
    extend: 'Ext.data.Store',
    model: 'Cement.model.products.Route',
    autoLoad: false,
    pageSize: Cement.Config.defaultPageSize,
    proxy: Cement.Config.url.goods.routes.proxy,
    remoteFilter: true,
    remoteSort: true
});