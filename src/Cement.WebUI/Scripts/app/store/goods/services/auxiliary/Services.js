Ext.define('Cement.store.goods.services.auxiliary.Services', {
    extend: 'Ext.data.Store',
    model: 'Cement.model.products.Service',
    autoLoad: false,
    proxy: getProxy(Cement.Config.url.goods.services.services_dict, ''),
    pageSize: Cement.Config.defaultPageSize,
    remoteFilter: true
});