Ext.define('Cement.store.goods.services.auxiliary.AllRequests', {
    extend: 'Ext.data.Store',
    model: 'Cement.model.goods.Service',
    proxy: getProxy(Cement.Config.url.goods.services.all_requests_dict, ''),
    remoteFilter: false,
    autoLoad: false
});