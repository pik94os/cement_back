Ext.define('Cement.store.goods.services.Incoming', {
    extend: 'Ext.data.Store',
    model: 'Cement.model.goods.Service',
    autoLoad: false,
    pageSize: Cement.Config.defaultPageSize,
    proxy: Cement.Config.url.goods.services.incoming.proxy,
    remoteFilter: true,
    remoteSort: true
});