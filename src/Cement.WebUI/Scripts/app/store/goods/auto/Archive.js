Ext.define('Cement.store.goods.auto.Archive', {
    extend: 'Ext.data.Store',
    model: 'Cement.model.goods.Auto',
    autoLoad: false,
    pageSize: Cement.Config.defaultPageSize,
    proxy: Cement.Config.url.goods.auto.archive.proxy,
    remoteFilter: true,
    remoteSort: true
});