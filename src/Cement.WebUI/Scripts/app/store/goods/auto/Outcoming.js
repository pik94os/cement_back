Ext.define('Cement.store.goods.auto.Outcoming', {
    extend: 'Ext.data.Store',
    model: 'Cement.model.goods.Auto',
    autoLoad: false,
    pageSize: Cement.Config.defaultPageSize,
    proxy: Cement.Config.url.goods.auto.outcoming.proxy,
    remoteFilter: true,
    remoteSort: true
});