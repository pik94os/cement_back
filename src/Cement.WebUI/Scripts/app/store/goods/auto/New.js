Ext.define('Cement.store.goods.auto.New', {
    extend: 'Ext.data.Store',
    model: 'Cement.model.goods.Auto',
    autoLoad: false,
    pageSize: Cement.Config.defaultPageSize,
    proxy: Cement.Config.url.goods.auto.new_r.proxy,
    remoteFilter: true,
    remoteSort: true
});