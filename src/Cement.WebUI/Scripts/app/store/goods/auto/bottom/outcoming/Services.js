Ext.define('Cement.store.goods.auto.bottom.outcoming.Services', {
    extend: 'Ext.data.Store',
    model: 'Cement.model.products.Service',
    autoLoad: false,
    pageSize: Cement.Config.defaultPageSize,
    proxy: Cement.Config.url.goods.request_services.proxy,
    remoteFilter: true,
    remoteSort: true
});