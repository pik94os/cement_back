Ext.define('Cement.store.goods.auto.bottom.new_r.ConnectedDocumentsIncoming', {
    extend: 'Ext.data.Store',
    model: 'Cement.model.goods.ConnectedDocument',
    autoLoad: false,
    pageSize: Cement.Config.defaultPageSize,
    proxy: Cement.Config.url.goods.auto.documents.proxy,
    remoteFilter: true,
    remoteSort: true
});