Ext.define('Cement.store.goods.auto.bottom.new_r.Products', {
    extend: 'Ext.data.Store',
    model: 'Cement.model.products.Product',
    autoLoad: false,
    pageSize: Cement.Config.defaultPageSize,
    proxy: Cement.Config.url.goods.request_products.proxy,
    remoteFilter: true,
    remoteSort: true
});