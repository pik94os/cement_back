Ext.define('Cement.store.goods.auto.bottom.incoming.Suppliers', {
    extend: 'Ext.data.Store',
    model: 'Cement.model.Firm',
    autoLoad: false,
    pageSize: Cement.Config.defaultPageSize,
    proxy: Cement.Config.url.goods.suppliers.proxy,
    remoteFilter: true,
    remoteSort: true
});