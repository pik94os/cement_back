Ext.define('Cement.store.goods.auto.Incoming', {
    extend: 'Ext.data.Store',
    model: 'Cement.model.goods.Auto',
    autoLoad: false,
    pageSize: Cement.Config.defaultPageSize,
    proxy: Cement.Config.url.goods.auto.incoming.proxy,
    remoteFilter: true,
    remoteSort: true
});