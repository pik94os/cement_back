Ext.define('Cement.store.goods.auto.Templates', {
    extend: 'Ext.data.Store',
    model: 'Cement.model.goods.Auto',
    autoLoad: false,
    pageSize: Cement.Config.defaultPageSize,
    proxy: Cement.Config.url.goods.auto.templates.proxy,
    remoteFilter: true,
    remoteSort: true
});