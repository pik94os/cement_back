Ext.define('Cement.store.goods.auto.auxiliary.Suppliers', {
    extend: 'Ext.data.TreeStore',
    fields: ['id', 'p_name', 'p_address', 'p_work_time', 'p_supplier'],
    autoLoad: false,
    pageSize: Cement.Config.defaultPageSize,
    proxy: getProxy(Cement.Config.url.goods.auto.suppliers_dict, ''),
    remoteFilter: true,
    root: {
    	text: '',
    	id: 0,
    	expanded: false
    }
});