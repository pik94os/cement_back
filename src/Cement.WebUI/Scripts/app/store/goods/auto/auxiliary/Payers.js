Ext.define('Cement.store.goods.auto.auxiliary.Payers', {
    extend: 'Ext.data.TreeStore',
    fields: ['id', 'p_name', 'p_address', 'p_work_time'],
    autoLoad: false,
    pageSize: Cement.Config.defaultPageSize,
    proxy: getProxy(Cement.Config.url.goods.auto.payers_dict, ''),
    remoteFilter: true,
    root: {
    	text: '',
    	id: 0,
    	expanded: false
    }
});