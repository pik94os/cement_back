Ext.define('Cement.store.goods.auto.auxiliary.AllTemplates', {
    extend: 'Ext.data.Store',
    model: 'Cement.model.goods.Auto',
    proxy: getProxy(Cement.Config.url.goods.auto.all_templates_dict, ''),
    remoteFilter: false,
    autoLoad: false
});