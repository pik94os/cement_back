Ext.define('Cement.store.goods.auto.auxiliary.Products', {
    extend: 'Ext.data.Store',
    model: 'Cement.model.products.Product',
    autoLoad: false,
    proxy: getProxy(Cement.Config.url.goods.auto.products_dict, ''),
    pageSize: Cement.Config.defaultPageSize,
    remoteFilter: true
});