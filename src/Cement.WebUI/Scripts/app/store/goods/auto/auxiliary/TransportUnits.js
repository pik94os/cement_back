Ext.define('Cement.store.goods.auto.auxiliary.TransportUnits', {
    extend: 'Ext.data.Store',
    model: 'Cement.model.transport.Unit',
    proxy: getProxy(Cement.Config.url.goods.auto.transport_units_dict, ''),
    remoteFilter: false,
    autoLoad: false
});