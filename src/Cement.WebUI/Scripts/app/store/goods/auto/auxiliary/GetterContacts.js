Ext.define('Cement.store.goods.auto.auxiliary.GetterContacts', {
    extend: 'Ext.data.TreeStore',
    fields: ['id', 'p_name', 'p_position', 'p_department'],
    autoLoad: false,
    pageSize: Cement.Config.defaultPageSize,
    proxy: getProxy(Cement.Config.url.goods.auto.getters_contacts_dict, ''),
    remoteFilter: true,
    root: {
    	text: '',
    	id: 0,
    	expanded: false
    }
});