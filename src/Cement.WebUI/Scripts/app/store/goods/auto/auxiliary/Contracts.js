Ext.define('Cement.store.goods.auto.auxiliary.Contracts', {
    extend: 'Ext.data.TreeStore',
    fields: ['id', 'p_name', 'p_product', 'p_product_name', 'p_product_unit_display', 'p_product_unit', 'p_price', 'p_tax'],
    autoLoad: false,
    pageSize: Cement.Config.defaultPageSize,
    proxy: getProxy(Cement.Config.url.goods.auto.contracts_dict, ''),
    remoteFilter: true,
    root: {
    	text: '',
    	id: 0,
    	expanded: false
    }
});