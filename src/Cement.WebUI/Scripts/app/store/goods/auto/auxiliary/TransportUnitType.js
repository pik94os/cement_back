Ext.define('Cement.store.goods.auto.auxiliary.TransportUnitType', {
    extend: 'Ext.data.Store',
    fields: [
        'id', 'p_name'
    ],
    autoLoad: false
});