Ext.define('Cement.store.goods.auto.auxiliary.AllRequests', {
    extend: 'Ext.data.Store',
    model: 'Cement.model.goods.Auto',
    proxy: getProxy(Cement.Config.url.goods.auto.all_requests_dict, ''),
    remoteFilter: false,
    autoLoad: false
});