Ext.define('Cement.store.goods.auto.auxiliary.LoadKinds', {
    extend: 'Ext.data.Store',
    fields: [
        'id', 'p_name'
    ],
    autoLoad: false
});