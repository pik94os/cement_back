Ext.define('Cement.store.goods.railway.New', {
    extend: 'Ext.data.Store',
    model: 'Cement.model.goods.Railway',
    autoLoad: false,
    pageSize: Cement.Config.defaultPageSize,
    proxy: Cement.Config.url.goods.railway.new_r.proxy,
    remoteFilter: true,
    remoteSort: true
});