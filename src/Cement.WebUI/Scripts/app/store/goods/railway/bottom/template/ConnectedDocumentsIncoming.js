Ext.define('Cement.store.goods.railway.bottom.template.ConnectedDocumentsIncoming', {
    extend: 'Ext.data.Store',
    model: 'Cement.model.goods.ConnectedDocument',
    autoLoad: false,
    pageSize: Cement.Config.defaultPageSize,
    proxy: Cement.Config.url.goods.railway.documents.proxy,
    remoteFilter: true,
    remoteSort: true
});