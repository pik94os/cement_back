Ext.define('Cement.store.goods.railway.bottom.archive.Payers', {
    extend: 'Ext.data.Store',
    model: 'Cement.model.Firm',
    autoLoad: false,
    pageSize: Cement.Config.defaultPageSize,
    proxy: Cement.Config.url.goods.payers.proxy,
    remoteFilter: true,
    remoteSort: true
});