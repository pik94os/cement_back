Ext.define('Cement.store.goods.railway.bottom.incoming.Routes', {
    extend: 'Ext.data.Store',
    model: 'Cement.model.products.Route',
    autoLoad: false,
    pageSize: Cement.Config.defaultPageSize,
    proxy: Cement.Config.url.goods.routes.proxy,
    remoteFilter: true,
    remoteSort: true
});