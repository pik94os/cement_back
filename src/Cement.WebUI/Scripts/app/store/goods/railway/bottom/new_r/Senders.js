Ext.define('Cement.store.goods.railway.bottom.new_r.Senders', {
    extend: 'Ext.data.Store',
    model: 'Cement.model.Firm',
    autoLoad: false,
    pageSize: Cement.Config.defaultPageSize,
    proxy: Cement.Config.url.goods.senders.proxy,
    remoteFilter: true,
    remoteSort: true
});