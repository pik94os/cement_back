Ext.define('Cement.store.goods.railway.bottom.new_r.Products', {
    extend: 'Ext.data.Store',
    model: 'Cement.model.products.Product',
    autoLoad: false,
    pageSize: Cement.Config.defaultPageSize,
    proxy: Cement.Config.url.products.products.proxy,
    remoteFilter: true,
    remoteSort: true
});