Ext.define('Cement.store.goods.railway.Incoming', {
    extend: 'Ext.data.Store',
    model: 'Cement.model.goods.Railway',
    autoLoad: false,
    pageSize: Cement.Config.defaultPageSize,
    proxy: Cement.Config.url.goods.railway.incoming.proxy,
    remoteFilter: true,
    remoteSort: true
});