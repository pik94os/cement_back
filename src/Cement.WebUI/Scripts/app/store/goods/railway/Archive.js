Ext.define('Cement.store.goods.railway.Archive', {
    extend: 'Ext.data.Store',
    model: 'Cement.model.goods.Railway',
    autoLoad: false,
    pageSize: Cement.Config.defaultPageSize,
    proxy: Cement.Config.url.goods.railway.archive.proxy,
    remoteFilter: true,
    remoteSort: true
});