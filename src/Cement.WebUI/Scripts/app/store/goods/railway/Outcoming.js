Ext.define('Cement.store.goods.railway.Outcoming', {
    extend: 'Ext.data.Store',
    model: 'Cement.model.goods.Railway',
    autoLoad: false,
    pageSize: Cement.Config.defaultPageSize,
    proxy: Cement.Config.url.goods.railway.outcoming.proxy,
    remoteFilter: true,
    remoteSort: true
});