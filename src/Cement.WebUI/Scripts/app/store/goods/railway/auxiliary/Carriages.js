Ext.define('Cement.store.goods.railway.auxiliary.Carriages', {
    extend: 'Ext.data.Store',
    fields: ['id', 'p_name', 'p_kind'],
    proxy: getProxy(Cement.Config.url.goods.railway.carriages_dict, ''),
    remoteFilter: false,
    autoLoad: false
});