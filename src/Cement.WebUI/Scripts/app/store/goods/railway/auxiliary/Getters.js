Ext.define('Cement.store.goods.railway.auxiliary.Getters', {
    extend: 'Ext.data.TreeStore',
    fields: ['id', 'p_name', 'p_address', 'p_work_time', 'p_supplier', 'p_warehouse_id', 'p_warehouse_code', 'p_warehouse_station', 'p_warehouse_road'],
    autoLoad: false,
    pageSize: Cement.Config.defaultPageSize,
    proxy: getProxy(Cement.Config.url.goods.railway.getters_dict, ''),
    remoteFilter: true,
    root: {
    	text: '',
    	id: 0,
    	expanded: false
    }
});