Ext.define('Cement.store.goods.catalog.AllPrices', {
    extend: 'Ext.data.Store',
    model: 'Cement.model.products.PriceList',
    proxy: getProxy(Cement.Config.url.goods.catalog.all_prices_dict, ''),
    remoteFilter: false,
    autoLoad: false
});