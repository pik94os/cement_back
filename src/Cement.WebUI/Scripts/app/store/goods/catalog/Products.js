Ext.define('Cement.store.goods.catalog.Products', {
    extend: 'Ext.data.Store',
    model: 'Cement.model.products.Product',
    autoLoad: false,
    pageSize: Cement.Config.defaultPageSize,
    proxy: Cement.Config.url.goods.catalog.products.proxy,
    remoteFilter: true,
    remoteSort: true
});