Ext.define('Cement.store.goods.catalog.Services', {
    extend: 'Ext.data.Store',
    model: 'Cement.model.products.Service',
    autoLoad: false,
    pageSize: Cement.Config.defaultPageSize,
    proxy: Cement.Config.url.goods.catalog.services.proxy,
    remoteFilter: true,
    remoteSort: true
});