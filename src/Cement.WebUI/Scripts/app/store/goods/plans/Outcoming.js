Ext.define('Cement.store.goods.plans.Outcoming', {
    extend: 'Ext.data.Store',
    model: 'Cement.model.goods.Plan',
    autoLoad: false,
    pageSize: Cement.Config.defaultPageSize,
    proxy: Cement.Config.url.goods.plans.outcoming.proxy,
    remoteFilter: true,
    remoteSort: true
});