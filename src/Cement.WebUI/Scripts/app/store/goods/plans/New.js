Ext.define('Cement.store.goods.plans.New', {
    extend: 'Ext.data.Store',
    model: 'Cement.model.goods.Plan',
    autoLoad: false,
    pageSize: Cement.Config.defaultPageSize,
    proxy: Cement.Config.url.goods.plans.new_p.proxy,
    remoteFilter: true,
    remoteSort: true
});