Ext.define('Cement.store.goods.plans.bottom.outcoming.Payers', {
    extend: 'Ext.data.Store',
    model: 'Cement.model.Firm',
    autoLoad: false,
    pageSize: Cement.Config.defaultPageSize,
    proxy: Cement.Config.url.goods.payers.proxy,
    remoteFilter: true,
    remoteSort: true
});