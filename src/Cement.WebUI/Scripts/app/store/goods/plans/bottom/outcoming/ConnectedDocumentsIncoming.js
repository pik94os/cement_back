Ext.define('Cement.store.goods.plans.bottom.outcoming.ConnectedDocumentsIncoming', {
    extend: 'Ext.data.Store',
    model: 'Cement.model.goods.ConnectedDocument',
    autoLoad: false,
    pageSize: Cement.Config.defaultPageSize,
    proxy: Cement.Config.url.goods.plans.documents.proxy,
    remoteFilter: true,
    remoteSort: true
});