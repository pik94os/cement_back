Ext.define('Cement.store.goods.plans.bottom.new_p.ConnectedDocumentsIncoming', {
    extend: 'Ext.data.Store',
    model: 'Cement.model.goods.ConnectedDocument',
    autoLoad: false,
    pageSize: Cement.Config.defaultPageSize,
    proxy: Cement.Config.url.goods.plans.documents.proxy,
    remoteFilter: true,
    remoteSort: true
});