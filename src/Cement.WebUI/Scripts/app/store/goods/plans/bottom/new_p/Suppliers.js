Ext.define('Cement.store.goods.plans.bottom.new_p.Suppliers', {
    extend: 'Ext.data.Store',
    model: 'Cement.model.Firm',
    autoLoad: false,
    pageSize: Cement.Config.defaultPageSize,
    proxy: Cement.Config.url.goods.suppliers.proxy,
    remoteFilter: true,
    remoteSort: true
});