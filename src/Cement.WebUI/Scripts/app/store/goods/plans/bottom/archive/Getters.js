Ext.define('Cement.store.goods.plans.bottom.archive.Getters', {
    extend: 'Ext.data.Store',
    model: 'Cement.model.Firm',
    autoLoad: false,
    pageSize: Cement.Config.defaultPageSize,
    proxy: Cement.Config.url.goods.getters.proxy,
    remoteFilter: true,
    remoteSort: true
});