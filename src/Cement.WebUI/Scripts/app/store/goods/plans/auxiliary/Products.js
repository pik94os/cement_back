Ext.define('Cement.store.goods.plans.auxiliary.Products', {
    extend: 'Ext.data.Store',
    model: 'Cement.model.products.Product',
    autoLoad: false,
    proxy: getProxy(Cement.Config.url.goods.plans.products_dict, ''),
    pageSize: Cement.Config.defaultPageSize,
    remoteFilter: true
});