Ext.define('Cement.store.goods.plans.Archive', {
    extend: 'Ext.data.Store',
    model: 'Cement.model.goods.Plan',
    autoLoad: false,
    pageSize: Cement.Config.defaultPageSize,
    proxy: Cement.Config.url.goods.plans.archive.proxy,
    remoteFilter: true,
    remoteSort: true
});