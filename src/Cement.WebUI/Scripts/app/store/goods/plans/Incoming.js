Ext.define('Cement.store.goods.plans.Incoming', {
    extend: 'Ext.data.Store',
    model: 'Cement.model.goods.Plan',
    autoLoad: false,
    pageSize: Cement.Config.defaultPageSize,
    proxy: Cement.Config.url.goods.plans.incoming.proxy,
    remoteFilter: true,
    remoteSort: true
});