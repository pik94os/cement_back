Ext.define('Cement.store.cargo.transport_exchange.Contract', {
    extend: 'Ext.data.Store',
    model: 'Cement.model.cargo.Exchange',
    autoLoad: false,
    pageSize: Cement.Config.defaultPageSize,
    proxy: Cement.Config.url.cargo.transport_exchange.contract.proxy,
    remoteFilter: true,
    remoteSort: true
});