Ext.define('Cement.store.cargo.transport_exchange.TransportContract', {
    extend: 'Ext.data.Store',
    model: 'Cement.model.cargo.TransportRight',
    autoLoad: false,
    pageSize: Cement.Config.defaultPageSize,
    proxy: Cement.Config.url.cargo.transport_exchange.contract.right_proxy,
    remoteFilter: true,
    remoteSort: true
});