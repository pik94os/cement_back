Ext.define('Cement.store.cargo.participants.Current', {
    extend: 'Ext.data.Store',
    model: 'Cement.model.cargo.Participant',
    autoLoad: false,
    pageSize: Cement.Config.defaultPageSize,
    proxy: Cement.Config.url.cargo.participants.current.proxy,
    remoteFilter: true,
    remoteSort: true
});