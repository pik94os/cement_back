Ext.define('Cement.store.cargo.participants.Archive', {
    extend: 'Ext.data.Store',
    model: 'Cement.model.cargo.Participant',
    autoLoad: false,
    pageSize: Cement.Config.defaultPageSize,
    proxy: Cement.Config.url.cargo.participants.archive.proxy,
    remoteFilter: true,
    remoteSort: true
});