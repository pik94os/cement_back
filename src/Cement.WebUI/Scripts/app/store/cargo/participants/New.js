Ext.define('Cement.store.cargo.participants.New', {
    extend: 'Ext.data.Store',
    model: 'Cement.model.cargo.Participant',
    autoLoad: false,
    pageSize: Cement.Config.defaultPageSize,
    proxy: Cement.Config.url.cargo.participants.new_p.proxy,
    remoteFilter: true,
    remoteSort: true
});