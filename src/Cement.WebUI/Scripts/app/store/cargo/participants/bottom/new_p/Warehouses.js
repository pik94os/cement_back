Ext.define('Cement.store.cargo.participants.bottom.new_p.Warehouses', {
    extend: 'Ext.data.Store',
    model: 'Cement.model.Warehouse',
    autoLoad: false,
    pageSize: Cement.Config.defaultPageSize,
    proxy: Cement.Config.url.corporate.warehouses.proxy,
    remoteFilter: true,
    remoteSort: true
});