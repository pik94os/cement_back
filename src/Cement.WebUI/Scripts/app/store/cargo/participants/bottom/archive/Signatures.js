Ext.define('Cement.store.cargo.participants.bottom.archive.Signatures', {
    extend: 'Ext.data.Store',
    model: 'Cement.model.Signature',
    autoLoad: false,
    pageSize: Cement.Config.defaultPageSize,
    proxy: Cement.Config.url.corporate.signatures.proxy,
    remoteFilter: true,
    remoteSort: true
});