Ext.define('Cement.store.cargo.participants.bottom.current.Employees', {
    extend: 'Ext.data.Store',
    model: 'Cement.model.Employee',
    autoLoad: false,
    pageSize: Cement.Config.defaultPageSize,
    proxy: Cement.Config.url.corporate.employes.proxy,
    remoteFilter: true,
    remoteSort: true
});