Ext.define('Cement.store.cargo.participants.bottom.archive.ConnectedDocumentsIncoming', {
    extend: 'Ext.data.Store',
    model: 'Cement.model.goods.ConnectedDocument',
    autoLoad: false,
    pageSize: Cement.Config.defaultPageSize,
    proxy: Cement.Config.url.cargo.participants.documents.proxy,
    remoteFilter: true,
    remoteSort: true
});