Ext.define('Cement.store.cargo.goods_exchange.Contract', {
    extend: 'Ext.data.Store',
    model: 'Cement.model.cargo.Exchange',
    autoLoad: false,
    pageSize: Cement.Config.defaultPageSize,
    proxy: Cement.Config.url.cargo.goods_exchange.contract.proxy,
    remoteFilter: true,
    remoteSort: true
});