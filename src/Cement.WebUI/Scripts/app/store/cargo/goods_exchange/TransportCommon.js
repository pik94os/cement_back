Ext.define('Cement.store.cargo.goods_exchange.TransportCommon', {
    extend: 'Ext.data.Store',
    model: 'Cement.model.cargo.GoodsRight',
    autoLoad: false,
    pageSize: Cement.Config.defaultPageSize,
    proxy: Cement.Config.url.cargo.goods_exchange.common.right_proxy,
    remoteFilter: true,
    remoteSort: true
});