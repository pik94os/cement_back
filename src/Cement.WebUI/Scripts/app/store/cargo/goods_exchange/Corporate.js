Ext.define('Cement.store.cargo.goods_exchange.Corporate', {
    extend: 'Ext.data.Store',
    model: 'Cement.model.cargo.Exchange',
    autoLoad: false,
    pageSize: Cement.Config.defaultPageSize,
    proxy: Cement.Config.url.cargo.goods_exchange.corporate.proxy,
    remoteFilter: true,
    remoteSort: true
});