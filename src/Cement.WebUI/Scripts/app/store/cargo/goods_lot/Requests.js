Ext.define('Cement.store.cargo.goods_lot.Requests', {
    extend: 'Ext.data.Store',
    model: 'Cement.model.goods.Auto',
    autoLoad: false,
    pageSize: Cement.Config.defaultPageSize,
    proxy: Cement.Config.url.cargo.goods_lot.requests.proxy,
    remoteFilter: true,
    remoteSort: true
});