Ext.define('Cement.store.cargo.goods_lot.bottom.incoming.Sellers', {
    extend: 'Ext.data.Store',
    model: 'Cement.model.Firm',
    autoLoad: false,
    pageSize: Cement.Config.defaultPageSize,
    proxy: Cement.Config.url.cargo.sellers.proxy,
    remoteFilter: true,
    remoteSort: true
});