Ext.define('Cement.store.cargo.goods_lot.bottom.outcoming.ConnectedDocumentsIncoming', {
    extend: 'Ext.data.Store',
    model: 'Cement.model.goods.ConnectedDocument',
    autoLoad: false,
    pageSize: Cement.Config.defaultPageSize,
    proxy: Cement.Config.url.cargo.goods_lot.documents.proxy,
    remoteFilter: true,
    remoteSort: true
});