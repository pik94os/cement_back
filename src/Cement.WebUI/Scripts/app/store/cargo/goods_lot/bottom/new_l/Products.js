Ext.define('Cement.store.cargo.goods_lot.bottom.new_l.Products', {
    extend: 'Ext.data.Store',
    model: 'Cement.model.products.Product',
    autoLoad: false,
    pageSize: Cement.Config.defaultPageSize,
    proxy: Cement.Config.url.products.products.proxy,
    remoteFilter: true,
    remoteSort: true
});