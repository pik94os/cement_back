Ext.define('Cement.store.cargo.goods_lot.bottom.incoming.Buyers', {
    extend: 'Ext.data.Store',
    model: 'Cement.model.Firm',
    autoLoad: false,
    pageSize: Cement.Config.defaultPageSize,
    proxy: Cement.Config.url.cargo.buyers.proxy,
    remoteFilter: true,
    remoteSort: true
});