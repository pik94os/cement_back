Ext.define('Cement.store.cargo.goods_lot.New', {
    extend: 'Ext.data.Store',
    model: 'Cement.model.cargo.GoodsLot',
    autoLoad: false,
    pageSize: Cement.Config.defaultPageSize,
    proxy: Cement.Config.url.cargo.goods_lot.new_l.proxy,
    remoteFilter: true,
    remoteSort: true
});