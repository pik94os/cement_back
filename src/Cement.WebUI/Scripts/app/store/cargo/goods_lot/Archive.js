Ext.define('Cement.store.cargo.goods_lot.Archive', {
    extend: 'Ext.data.Store',
    model: 'Cement.model.cargo.GoodsLot',
    autoLoad: false,
    pageSize: Cement.Config.defaultPageSize,
    proxy: Cement.Config.url.cargo.goods_lot.archive.proxy,
    remoteFilter: true,
    remoteSort: true
});