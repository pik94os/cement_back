Ext.define('Cement.store.cargo.goods_lot.auxiliary.Dispatchers', {
    extend: 'Ext.data.TreeStore',
    fields: ['id', 'p_name', 'p_address'],
    autoLoad: false,
    pageSize: Cement.Config.defaultPageSize,
    proxy: getProxy(Cement.Config.url.cargo.dispatchers_dict, ''),
    remoteFilter: true,
    root: {
    	text: '',
    	id: 0,
    	expanded: false
    }
});