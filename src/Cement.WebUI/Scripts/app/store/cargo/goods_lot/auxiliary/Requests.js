Ext.define('Cement.store.cargo.goods_lot.auxiliary.Requests', {
    extend: 'Ext.data.Store',
    model: 'Cement.model.goods.Auto',
    proxy: getProxy(Cement.Config.url.cargo.requests_dict, ''),
    remoteFilter: false,
    autoLoad: false
});