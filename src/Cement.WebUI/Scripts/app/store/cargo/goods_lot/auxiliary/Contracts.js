Ext.define('Cement.store.cargo.goods_lot.auxiliary.Contracts', {
    extend: 'Ext.data.TreeStore',
    fields: ['id', 'p_name', 'p_group', 'p_subgroup', 'p_trade_mark', 'p_manufacturer', 'p_unit',
		'p_tax', 'p_price', 'p_contract', 'p_contract_name'],
    autoLoad: false,
    pageSize: Cement.Config.defaultPageSize,
    proxy: getProxy(Cement.Config.url.cargo.contracts_dict, ''),
    remoteFilter: true,
    root: {
    	text: '',
    	id: 0,
    	expanded: false
    }
});