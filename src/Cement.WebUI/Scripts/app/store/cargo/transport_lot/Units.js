Ext.define('Cement.store.cargo.transport_lot.Units', {
    extend: 'Ext.data.Store',
    model: 'Cement.model.transport.Unit',
    autoLoad: false,
    pageSize: Cement.Config.defaultPageSize,
    proxy: Cement.Config.url.cargo.transport_lot.units.proxy,
    remoteFilter: true,
    remoteSort: true
});