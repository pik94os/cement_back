Ext.define('Cement.store.cargo.transport_lot.auxiliary.TransportUnits', {
    extend: 'Ext.data.Store',
    model: 'Cement.model.transport.Unit',
    proxy: getProxy(Cement.Config.url.cargo.transport_units_dict, ''),
    remoteFilter: false,
    autoLoad: false
});