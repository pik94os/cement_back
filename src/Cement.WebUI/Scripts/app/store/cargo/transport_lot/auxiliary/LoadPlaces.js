Ext.define('Cement.store.cargo.transport_lot.auxiliary.LoadPlaces', {
    extend: 'Ext.data.TreeStore',
    fields: ['id', 'p_name'],
    autoLoad: false,
    pageSize: Cement.Config.defaultPageSize,
    proxy: getProxy(Cement.Config.url.cargo.cities_dict, ''),
    remoteFilter: true,
    root: {
    	text: '',
    	id: 0,
    	expanded: false
    }
});