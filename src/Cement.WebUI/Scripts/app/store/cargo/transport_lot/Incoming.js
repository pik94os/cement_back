Ext.define('Cement.store.cargo.transport_lot.Incoming', {
    extend: 'Ext.data.Store',
    model: 'Cement.model.cargo.TransportLot',
    autoLoad: false,
    pageSize: Cement.Config.defaultPageSize,
    proxy: Cement.Config.url.cargo.transport_lot.incoming.proxy,
    remoteFilter: true,
    remoteSort: true
});