Ext.define('Cement.store.cargo.transport_lot.Outcoming', {
    extend: 'Ext.data.Store',
    model: 'Cement.model.cargo.TransportLot',
    autoLoad: false,
    pageSize: Cement.Config.defaultPageSize,
    proxy: Cement.Config.url.cargo.transport_lot.outcoming.proxy,
    remoteFilter: true,
    remoteSort: true
});