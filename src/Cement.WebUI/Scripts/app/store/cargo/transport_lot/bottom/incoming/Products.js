Ext.define('Cement.store.cargo.transport_lot.bottom.incoming.Products', {
    extend: 'Ext.data.Store',
    model: 'Cement.model.products.Product',
    autoLoad: false,
    pageSize: Cement.Config.defaultPageSize,
    proxy: Cement.Config.url.products.products.proxy,
    remoteFilter: true,
    remoteSort: true
});