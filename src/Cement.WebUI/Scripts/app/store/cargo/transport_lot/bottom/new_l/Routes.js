Ext.define('Cement.store.cargo.transport_lot.bottom.new_l.Routes', {
    extend: 'Ext.data.Store',
    model: 'Cement.model.products.Route',
    autoLoad: false,
    pageSize: Cement.Config.defaultPageSize,
    proxy: Cement.Config.url.cargo.routes.proxy,
    remoteFilter: true,
    remoteSort: true
});