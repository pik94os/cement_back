Ext.define('Cement.store.cargo.transport_lot.bottom.new_l.Sellers', {
    extend: 'Ext.data.Store',
    model: 'Cement.model.Firm',
    autoLoad: false,
    pageSize: Cement.Config.defaultPageSize,
    proxy: Cement.Config.url.cargo.sellers.proxy,
    remoteFilter: true,
    remoteSort: true
});