Ext.define('Cement.store.cargo.transport_lot.bottom.outcoming.Buyers', {
    extend: 'Ext.data.Store',
    model: 'Cement.model.Firm',
    autoLoad: false,
    pageSize: Cement.Config.defaultPageSize,
    proxy: Cement.Config.url.cargo.buyers.proxy,
    remoteFilter: true,
    remoteSort: true
});