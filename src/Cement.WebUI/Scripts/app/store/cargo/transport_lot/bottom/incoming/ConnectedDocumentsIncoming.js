Ext.define('Cement.store.cargo.transport_lot.bottom.incoming.ConnectedDocumentsIncoming', {
    extend: 'Ext.data.Store',
    model: 'Cement.model.goods.ConnectedDocument',
    autoLoad: false,
    pageSize: Cement.Config.defaultPageSize,
    proxy: Cement.Config.url.cargo.transport_lot.documents.proxy,
    remoteFilter: true,
    remoteSort: true
});