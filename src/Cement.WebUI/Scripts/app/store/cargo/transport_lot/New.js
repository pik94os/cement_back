Ext.define('Cement.store.cargo.transport_lot.New', {
    extend: 'Ext.data.Store',
    model: 'Cement.model.cargo.TransportLot',
    autoLoad: false,
    pageSize: Cement.Config.defaultPageSize,
    proxy: Cement.Config.url.cargo.transport_lot.new_l.proxy,
    remoteFilter: true,
    remoteSort: true
});