Ext.define('Cement.view.goods.plans.form.Form', {
  extend: 'Cement.view.basic.Form',
  alias: 'widget.goods_plans_form_form',
  fieldsDisabled: false,
  url: Cement.Config.url.goods.plans.saveUrl,
  method: Cement.Config.url.goods.plans.saveMethod,
  isFilter: false,
  layout: 'fit',
  allowBlankFields: true,
  showSaveAndSignButton: true,
  saveButtonDisabled: false,

  getItems: function () {
    return [{
                xtype: 'fieldset',
                border: 0, 
                margin: 0,
                defaults: {
                    xtype: 'textfield',
                    anchor: '100%',
                    labelWidth: 200,
                    labelAlign: 'right'
                },
                items: [{
                  fieldLabel: 'Наименование',
                  name: 'p_name'
                }, {
                  xtype: 'radiogroup',
                  columns: 1,
                  labelWidth: 200,
                  labelAlign: 'right',
                  margin: '0 0 10 200',
                  items: [{
                    xtype: 'radio',
                    name: 'p_grp',
                    inputValue: 1,
                    boxLabel: 'Группа'
                  }, {
                    xtype: 'radio',
                    name: 'p_grp',
                    inputValue: 2,
                    boxLabel: 'Подгруппа'
                  }, {
                    xtype: 'radio',
                    name: 'p_grp',
                    inputValue: 3,
                    boxLabel: 'Наименование'
                  }]
                },
                this.getSelectorField('Товар', 'p_product', 'show_product_window'), {
                  name: 'p_january_plan',
                  fieldLabel: 'Январь'
                }, {
                  name: 'p_february_plan',
                  fieldLabel: 'Февраль'
                }, {
                  name: 'p_march_plan',
                  fieldLabel: 'Март'
                }, {
                  name: 'p_april_plan',
                  fieldLabel: 'Апрель'
                }, {
                  name: 'p_may_plan',
                  fieldLabel: 'Май'
                }, {
                  name: 'p_june_plan',
                  fieldLabel: 'Июнь'
                }, {
                  name: 'p_july_plan',
                  fieldLabel: 'Июль'
                }, {
                  name: 'p_august_plan',
                  fieldLabel: 'Август'
                }, {
                  name: 'p_september_plan',
                  fieldLabel: 'Сентябрь'
                }, {
                  name: 'p_october_plan',
                  fieldLabel: 'Октябрь'
                }, {
                  name: 'p_november_plan',
                  fieldLabel: 'Ноябрь'
                }, {
                  name: 'p_december_plan',
                  fieldLabel: 'Декабрь'
                },
                this.getSelectorField('Связанные документы', 'p_documents', 'show_documents_window'),
                {
                  xtype: 'textarea',
                  fieldLabel: 'Комментарий',
                  name: 'p_comment'
                }
              ]
            }
        ];
  },

  initComponent: function () {
    this.callParent(arguments);

    this.down('button[action=show_product_window]').on('click', function () {
      this.createProductsWindow();
      this.productsWindow.show();
    }, this);

    this.down('button[action=show_documents_window]').on('click', function () {
      this.createDocumentsWindow();
      this.documentsWindow.show();
    }, this);
  },

  createDocumentsWindow: function () {
    if (!this.documentsWindow) {
      this.documentsWindow = Ext.create('Cement.view.goods.plans.form.ConnectedDocumentsWindow');
      this.documentsWindow.on('docselected', function (data, ids) {
        this.down('textfield[name=p_documents_display]').setValue(data);
        this.down('hiddenfield[name=p_documents]').setValue(Ext.JSON.encode(ids));
        this.documentsWindow.hide();
      }, this);
    }
  },

  createProductsWindow: function () {
    if (!this.productsWindow) {
      this.productsWindow = Ext.create('Cement.view.goods.plans.form.ProductsWindow');
      this.productsWindow.on('productselected', function (rec) {
        this.down('textfield[name=p_product_display]').setValue(rec.get('p_name'));
        this.down('hiddenfield[name=p_product]').setValue(rec.get('id'));
        this.productsWindow.hide();
      }, this);
    }
  }
});