Ext.define('Cement.view.goods.plans.form.ConnectedDocumentsWindow', {
  extend: 'Cement.view.common.SignWindow',
  xtype: 'goods_plans_connected_documents_window',
  title: 'Выбор связанных документов',
  hideCommentPanel: true,
  leftFrameTitle: 'Документы',
  topGridTitle: 'Внутренние документы',
  bottomGridTitle: 'Выбранные документы',

  bbar: [{
    text: 'Сохранить',
    action: 'confirm'
  }, {
    text: 'Отмена',
    action: 'cancel'
  }],

  storeFields: ['id', 'p_name' ],
  displayField: 'p_name',

  structure_storeId: 'goods.plans.auxiliary.ConnectedDocuments',

  sign: function () {
    var store = this.dstStore,
      data = [],
      ids = [];
    store.each(function (item) {
      data.push(item.get('p_name'));
      ids.push(item.get('id'));
    });
    this.fireEvent('docselected', data.join(', '), ids);
  },

  getBottomColumnsConfig: function () {
    return [{
      xtype: 'rowactions',
      hideable: false,
      resizeable: false,
      width: 18,
      actions: [{ 
        iconCls: 'icon-remove-document', 
        qtip: 'Удалить', 
        callback: this.removeEmployee
      }]
    },
    { text: 'Документ', dataIndex: 'p_name', flex: 1 }];
  },

  getTopColumnsConfig: function () {
    return [
      {
        xtype: 'rowactions',
        hideable: false,
        resizeable: false,
        width: 18,
        keepSelection: true,
        actions: [{ 
          iconCls: 'icon-add-document', 
          qtip: 'Добавить',
          callback: this.addEmployee
        }]
      },
      { text: 'Документ', dataIndex: 'p_name', flex: 1 }
    ];
  },

  closeWindow: function () {
    this.hide();
  },

  loadData: function (data) {
    if (data) {
      var fstore = Ext.getStore(this.structure_storeId);
      fstore.load({
        callback: function () {
          Ext.each(data, function (item) {
            var r = fstore.getNodeById(item);
            if (!this.dstStore) {
              this.setupEvents();
            }
            if (r) {
              this.dstStore.add({
                id: r.get('id'),
                name: r.get('text')
              });
            }
          }, this);
        },
        scope: this
      });
    }
  },

  getTopGridFilters: function () {
    return [{
      text: 'Наименование',
      kind: 'selector',
      field_name: 'p_name',
      checked: true
    }];
  },

  initComponent: function () {
    this.callParent(arguments);
    this.on('beforeclose', function () {
      this.hide();
      return false;
    });
    this.addEvents('docselected');
    this.on('afterrender', function () {
      Ext.getStore(this.structure_storeId).getRootNode().expand();
    }, this);
  }
});