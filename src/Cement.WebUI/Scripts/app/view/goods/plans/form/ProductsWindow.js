Ext.define('Cement.view.goods.plans.form.ProductsWindow', {
  extend: 'Cement.view.goods.auto.form.ProductWindow',
  srcStoreId: 'goods.plans.auxiliary.Products',

  getTopColumnsConfig: function () {
    return [
      {
        xtype: 'rowactions',
        hideable: false,
        resizeable: false,
        width: 18,
        keepSelection: true,
        locked: true,
        actions: [{ 
                  iconCls: 'icon-add-item', 
                  qtip: 'Добавить',
                  callback: this.addEmployee
              }]
      },
      { text: 'Наименование', dataIndex: 'p_name', width: 200, locked: true },
      { text: 'Группа', dataIndex: 'p_group', width: 200 },
      { text: 'Подгруппа', dataIndex: 'p_subgroup', width: 200 },
      { text: 'Торговая марка', dataIndex: 'p_trade_mark', width: 200 },
      { text: 'Производитель', dataIndex: 'p_manufacturer', width: 200 }
    ];
  },

  getBottomColumnsConfig: function () {
    return [
      {
        xtype: 'rowactions',
        hideable: false,
        resizeable: false,
        locked: true,
        width: 18,
        actions: [{ 
                  iconCls: 'icon-delete-item', 
                  qtip: 'Удалить', 
                  callback: this.removeEmployee
              }]
      },
      { text: 'Наименование', dataIndex: 'p_name', width: 200, locked: true },
      { text: 'Группа', dataIndex: 'p_group', width: 200 },
      { text: 'Подгруппа', dataIndex: 'p_subgroup', width: 200 },
      { text: 'Торговая марка', dataIndex: 'p_trade_mark', width: 200 },
      { text: 'Производитель', dataIndex: 'p_manufacturer', width: 200 }
    ];
  },

  getTopGridFilters: function () {
    return [{
            text: 'Наименование',
            kind: 'selector',
            field_name: 'p_name',
            checked: true
        }, {
            text: 'Группа',
            kind: 'selector',
            field_name: 'p_group',
            checked: true
        }, {
            text: 'Подгруппа',
            kind: 'selector',
            field_name: 'p_subgroup',
            checked: true
        }, {
            text: 'Торговая марка',
            kind: 'selector',
            field_name: 'p_trade_mark',
            checked: true
        }, {
            text: 'Производитель',
            kind: 'selector',
            field_name: 'p_manufacturer',
            checked: true
        }];
  }
});