Ext.define('Cement.view.goods.plans.top.Archive', {
	extend: 'Ext.container.Container',
	alias: 'widget.goods_plans_top_archive',
	height: 350,
	
	border: 0,

	title: 'Входящие',
	items: [{
		xtype: 'goods_plans_charts_new',
		height: 250
	}, {
		xtype: 'goods_plans_lists_archive',
		layout: 'fit',
		border: 0,
		flex: 1
	}],

	initComponent: function () {
		this.callParent(arguments);
		Ext.apply(this, {
			layout: {
				type: 'vbox',
				align: 'stretch',
				pack: 'start'
			}
		});
		var store = this.down('goods_plans_lists_archive').down('grid').getStore();
		store.on('load', function () {
			this.down('goods_plans_charts_new').loadData(store.getAt(0));
		}, this);
		this.down('goods_plans_lists_archive').down('grid').on('itemclick', function (grid, item) {
			this.down('goods_plans_charts_new').loadData(item);
		}, this);
	}
});