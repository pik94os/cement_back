Ext.define('Cement.view.goods.plans.View', {
	extend: 'Cement.view.basic.Form',
	alias: 'widget.goods_plans_view',
	fieldsDisabled: true,
	buttons: null,
	border: 0,
	tbar: Cement.Config.modelViewButtons,
	layout: 'autocontainer',
	bbar: null,
    noUserFieldsetsControl: false,
    bodyCls: 'model-form',

	getItems: function () {
		return [{
				xtype: 'panel',
				border: 0,
				layout: 'anchor',
				defaultType: 'textfield',
				anchor: '100%',
				bodyPadding: '10 10 10 5',
				autoScroll: true,
				defaults: {
					anchor: '100%',
					labelWidth: 110
				},
				items: [{
					xtype: 'fieldset',
					layout: 'anchor',
					defaults: {
						xtype: 'textfield',
						anchor: '100%'
					},
					border: 0,
					items: [{
						disabled: true,
						name: 'p_name',
						fieldLabel: 'Наименование'
					}, {
						disabled: true,
						name: 'p_product_display',
						fieldLabel: 'Товар/услуга'
					}, {
						disabled: true,
						name: 'p_unit_display',
						fieldLabel: 'Единица измерения'
					}]
				}, {
					xtype: 'fieldset',
					collapsed: true,
					collapsible: true,
					title: 'Январь',
					defaults: {
						xtype: 'textfield',
						anchor: '100%'
					},
					items: [{
						disabled: true,
						name: 'p_january_plan',
						fieldLabel: 'План'
					}, {
						disabled: true,
						name: 'p_january_fact',
						fieldLabel: 'Факт'
					}, {
						disabled: true,
						name: 'p_january_percents',
						fieldLabel: '+/-, %'
					}]
				}, {
					xtype: 'fieldset',
					collapsed: true,
					collapsible: true,
					title: 'Февраль',
					defaults: {
						xtype: 'textfield',
						anchor: '100%'
					},
					items: [{
						disabled: true,
						name: 'p_february_plan',
						fieldLabel: 'План'
					}, {
						disabled: true,
						name: 'p_february_fact',
						fieldLabel: 'Факт'
					}, {
						disabled: true,
						name: 'p_february_percents',
						fieldLabel: '+/-, %'
					}]
				}, {
					xtype: 'fieldset',
					collapsed: true,
					collapsible: true,
					title: 'Март',
					defaults: {
						disabled: true,
						xtype: 'textfield',
						anchor: '100%'
					},
					items: [{
						name: 'p_march_plan',
						fieldLabel: 'План'
					}, {
						name: 'p_march_fact',
						fieldLabel: 'Факт'
					}, {
						name: 'p_march_percents',
						fieldLabel: '+/-, %'
					}]
				}, {
					xtype: 'fieldset',
					collapsed: true,
					collapsible: true,
					title: 'I квартал',
					defaults: {
						xtype: 'textfield',
						disabled: true,
						anchor: '100%'
					},
					items: [{
						name: 'p_1_quarter_plan',
						fieldLabel: 'План'
					}, {
						name: 'p_1_quarter_fact',
						fieldLabel: 'Факт'
					}, {
						name: 'p_1_quarter_percents',
						fieldLabel: '+/-, %'
					}]
				}, {
					xtype: 'fieldset',
					collapsed: true,
					collapsible: true,
					title: 'Апрель',
					defaults: {
						xtype: 'textfield',
						disabled: true,
						anchor: '100%'
					},
					items: [{
						name: 'p_april_plan',
						fieldLabel: 'План'
					}, {
						name: 'p_april_fact',
						fieldLabel: 'Факт'
					}, {
						name: 'p_april_percents',
						fieldLabel: '+/-, %'
					}]
				}, {
					xtype: 'fieldset',
					collapsed: true,
					collapsible: true,
					title: 'Май',
					defaults: {
						xtype: 'textfield',
						disabled: true,
						anchor: '100%'
					},
					items: [{
						name: 'p_may_plan',
						fieldLabel: 'План'
					}, {
						name: 'p_may_fact',
						fieldLabel: 'Факт'
					}, {
						name: 'p_may_percents',
						fieldLabel: '+/-, %'
					}]
				}, {
					xtype: 'fieldset',
					collapsed: true,
					collapsible: true,
					title: 'Июнь',
					defaults: {
						disabled: true,
						xtype: 'textfield',
						anchor: '100%'
					},
					items: [{
						name: 'p_june_plan',
						fieldLabel: 'План'
					}, {
						name: 'p_june_fact',
						fieldLabel: 'Факт'
					}, {
						name: 'p_june_percents',
						fieldLabel: '+/-, %'
					}]
				}, {
					xtype: 'fieldset',
					collapsed: true,
					collapsible: true,
					title: 'II квартал',
					defaults: {
						disabled: true,
						xtype: 'textfield',
						anchor: '100%'
					},
					items: [{
						name: 'p_2_quarter_plan',
						fieldLabel: 'План'
					}, {
						name: 'p_2_quarter_fact',
						fieldLabel: 'Факт'
					}, {
						name: 'p_2_quarter_percents',
						fieldLabel: '+/-, %'
					}]
				}, {
					xtype: 'fieldset',
					collapsed: true,
					collapsible: true,
					title: 'I полугодие',
					defaults: {
						xtype: 'textfield',
						disabled: true,
						anchor: '100%'
					},
					items: [{
						name: 'p_1_half_plan',
						fieldLabel: 'План'
					}, {
						name: 'p_1_half_fact',
						fieldLabel: 'Факт'
					}, {
						name: 'p_1_half_percents',
						fieldLabel: '+/-, %'
					}]
				}, {
					xtype: 'fieldset',
					collapsed: true,
					collapsible: true,
					title: 'Июль',
					defaults: {
						disabled: true,
						xtype: 'textfield',
						anchor: '100%'
					},
					items: [{
						name: 'p_july_plan',
						fieldLabel: 'План'
					}, {
						name: 'p_july_fact',
						fieldLabel: 'Факт'
					}, {
						name: 'p_july_percents',
						fieldLabel: '+/-, %'
					}]
				}, {
					xtype: 'fieldset',
					collapsed: true,
					collapsible: true,
					title: 'Август',
					defaults: {
						xtype: 'textfield',
						disabled: true,
						anchor: '100%'
					},
					items: [{
						name: 'p_august_plan',
						fieldLabel: 'План'
					}, {
						name: 'p_august_fact',
						fieldLabel: 'Факт'
					}, {
						name: 'p_august_percents',
						fieldLabel: '+/-, %'
					}]
				}, {
					xtype: 'fieldset',
					collapsed: true,
					collapsible: true,
					title: 'Сентябрь',
					defaults: {
						xtype: 'textfield',
						disabled: true,
						anchor: '100%'
					},
					items: [{
						name: 'p_september_plan',
						fieldLabel: 'План'
					}, {
						name: 'p_september_fact',
						fieldLabel: 'Факт'
					}, {
						name: 'p_september_percents',
						fieldLabel: '+/-, %'
					}]
				}, {
					xtype: 'fieldset',
					collapsed: true,
					collapsible: true,
					title: 'III квартал',
					defaults: {
						xtype: 'textfield',
						disabled: true,
						anchor: '100%'
					},
					items: [{
						name: 'p_3_quarter_plan',
						fieldLabel: 'План'
					}, {
						name: 'p_3_quarter_fact',
						fieldLabel: 'Факт'
					}, {
						name: 'p_3_quarter_percents',
						fieldLabel: '+/-, %'
					}]
				}, {
					xtype: 'fieldset',
					collapsed: true,
					collapsible: true,
					title: 'За 3 квартала',
					defaults: {
						xtype: 'textfield',
						disabled: true,
						anchor: '100%'
					},
					items: [{
						name: 'p_for_3_quarters_plan',
						fieldLabel: 'План'
					}, {
						name: 'p_for_3_quarters_fact',
						fieldLabel: 'Факт'
					}, {
						name: 'p_for_3_quarters_percents',
						fieldLabel: '+/-, %'
					}]
				}, {
					xtype: 'fieldset',
					collapsed: true,
					collapsible: true,
					title: 'Октябрь',
					defaults: {
						disabled: true,
						xtype: 'textfield',
						anchor: '100%'
					},
					items: [{
						name: 'p_october_plan',
						fieldLabel: 'План'
					}, {
						name: 'p_october_fact',
						fieldLabel: 'Факт'
					}, {
						name: 'p_october_percents',
						fieldLabel: '+/-, %'
					}]
				}, {
					xtype: 'fieldset',
					collapsed: true,
					collapsible: true,
					title: 'Ноябрь',
					defaults: {
						disabled: true,
						xtype: 'textfield',
						anchor: '100%'
					},
					items: [{
						name: 'p_november_plan',
						fieldLabel: 'План'
					}, {
						name: 'p_november_fact',
						fieldLabel: 'Факт'
					}, {
						name: 'p_november_percents',
						fieldLabel: '+/-, %'
					}]
				}, {
					xtype: 'fieldset',
					collapsed: true,
					collapsible: true,
					title: 'Декабрь',
					defaults: {
						disabled: true,
						xtype: 'textfield',
						anchor: '100%'
					},
					items: [{
						name: 'p_december_plan',
						fieldLabel: 'План'
					}, {
						name: 'p_december_fact',
						fieldLabel: 'Факт'
					}, {
						name: 'p_december_percents',
						fieldLabel: '+/-, %'
					}]
				}, {
					xtype: 'fieldset',
					collapsed: true,
					collapsible: true,
					title: 'IV квартал',
					defaults: {
						disabled: true,
						xtype: 'textfield',
						anchor: '100%'
					},
					items: [{
						name: 'p_4_quarter_plan',
						fieldLabel: 'План'
					}, {
						name: 'p_4_quarter_fact',
						fieldLabel: 'Факт'
					}, {
						name: 'p_4_quarter_percents',
						fieldLabel: '+/-, %'
					}]
				}, {
					xtype: 'fieldset',
					collapsed: true,
					collapsible: true,
					title: 'II полугодие',
					defaults: {
						disabled: true,
						xtype: 'textfield',
						anchor: '100%'
					},
					items: [{
						name: 'p_2_half_plan',
						fieldLabel: 'План'
					}, {
						name: 'p_2_half_fact',
						fieldLabel: 'Факт'
					}, {
						name: 'p_2_half_percents',
						fieldLabel: '+/-, %'
					}]
				}, {
					xtype: 'fieldset',
					collapsed: true,
					collapsible: true,
					title: 'Всего',
					defaults: {
						disabled: true,
						xtype: 'textfield',
						anchor: '100%'
					},
					items: [{
						name: 'p_total_plan',
						fieldLabel: 'План'
					}, {
						name: 'p_total_fact',
						fieldLabel: 'Факт'
					}, {
						name: 'p_total_percents',
						fieldLabel: '+/-, %'
					}]
				}, {
					xtype: 'fieldset',
					collapsed: true,
					collapsible: true,
					title: 'Комментарии',
					defaults: {
						disabled: true,
						xtype: 'textfield',
						anchor: '100%'
					},
					items: [{
						xtype: 'textarea',
						name: 'p_comment',
						labelAlign: 'top',
						fieldLabel: 'Комментарий'
					}]
				}]
		}]
	},

	initComponent: function () {
        Ext.apply(this, {
            items: [
                {
                    xtype: 'panel',
                    border: 0,
                    layout: 'anchor',
                    defaultType: 'textfield',
                    anchor: '100%',
                    bodyPadding: '10 10 10 5',
                    autoScroll: true,
                    defaults: {
                        anchor: '100%',
                        labelWidth: 110
                    },
                    items: this.getItems()
                }
            ]
        });
        this.callParent(arguments);
    }
});
