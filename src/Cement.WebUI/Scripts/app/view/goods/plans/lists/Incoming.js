Ext.define('Cement.view.goods.plans.lists.Incoming', {
	extend: 'Cement.view.goods.plans.lists.New',
	alias: 'widget.goods_plans_lists_incoming',

	gridStore: 'goods.plans.Incoming',
	gridStateId: 'stateGoodsPlansIncoming',

	printUrl: Cement.Config.url.goods.plans.incoming.printGrid,
	helpUrl: Cement.Config.url.goods.plans.incoming.help,
    deleteUrl: Cement.Config.url.goods.plans.incoming.deleteUrl,

    shownTitle: null,

    getActionColumns: function () {
        return {
            xtype: 'rowactions',
            hideable: false,
            resizeable: false,
            locked: true,
            width: 46,
            actions: [
                {
                    iconCls: 'icon-delete-item',
                    qtip: 'Удалить',
                    callback: this.deleteItem
                },
                {
                    iconCls: 'icon-archive-item',
                    qtip: 'В архив',
                    callback: this.archiveItem
                }
            ],
            keepSelection: true
        };
    }
});