Ext.define('Cement.view.goods.plans.lists.Archive', {
    extend: 'Cement.view.basic.Grid',
    alias: 'widget.goods_plans_lists_archive',
    autoLoadStore: true,

    bbarText: 'Показаны планы {0} - {1} из {2}',
    bbarEmptyMsg: 'Нет планов',
    bbarUsersText: 'Планов на странице: ',

    gridStore: 'goods.plans.Archive',
    gridStateId: 'stateGoodsPlansArchive',

    printUrl: Cement.Config.url.goods.plans.archive.printGrid,
    helpUrl: Cement.Config.url.goods.plans.archive.help,
    deleteUrl: Cement.Config.url.goods.plans.archive.deleteUrl,
    unArchiveItemUrl: Cement.Config.url.goods.plans.archive.unArchiveItem,

    shownTitle: null,

    getActionColumns: function () {
        return {
            xtype: 'rowactions',
            hideable: false,
            resizeable: false,
            locked: true,
            width: 46,
            actions: [
                {
                    iconCls: 'icon-unarchive-item',
                    qtip: 'Из архива',
                    callback: this.unArchiveItem
                }
            ],
            keepSelection: true
        };
    },

    getFilterItems: function () {
        return [{
            text: 'Выделить все',
            checked: true,
            checkHandler: this.toggleFilterSelection
        }, '-', {
            text: 'Наименование',
            kind: 'selector',
            field_name: 'p_name',
            checked: true
        }, {
            text: 'Поставщик',
            kind: 'selector',
            field_name: 'p_supplier_name',
            checked: true
        }, {
            text: 'Плательщик наименование',
            kind: 'selector',
            field_name: 'p_payer_name',
            checked: true
        }, {
            text: 'Плательщик адрес',
            kind: 'selector',
            field_name: 'p_payer_address',
            checked: true
        }, {
            text: 'Плательщик рабочее время',
            kind: 'selector',
            field_name: 'p_payer_work_time',
            checked: true
        }, {
            text: 'Грузополучатель наименование',
            kind: 'selector',
            field_name: 'p_getter_name',
            checked: true
        }, {
            text: 'Грузополучатель адрес',
            kind: 'selector',
            field_name: 'p_getter_address',
            checked: true
        }, {
            text: 'Грузополучатель рабочее время',
            kind: 'selector',
            field_name: 'p_getter_work_time',
            checked: true
        }, {
            text: 'Транспорт тип кузова',
            kind: 'selector',
            field_name: 'p_transport_char_type_display',
            checked: true
        }, {
            text: 'Транспорт доп. опции',
            kind: 'selector',
            field_name: 'p_transport_additional_options_display',
            checked: true
        }];
    },

    getGridColumns: function () {
        var result = [
            { text: 'Тип', dataIndex: 'p_kind', width: 60, locked: true },
            { text: 'Наименование', dataIndex: 'p_name', width: 60, locked: true },
            { text: 'Г', dataIndex: 'p_grp', width: 25, locked: true, resizable: false, align: 'center', renderer: this.radioButtonRenderer1, tdCls: 'pseudoinput-column' },
            { text: 'П/Г', dataIndex: 'p_grp', width: 25, locked: true, resizable: false, align: 'center', renderer: this.radioButtonRenderer2, tdCls: 'pseudoinput-column' },
            { text: 'Н', dataIndex: 'p_grp', width: 25, locked: true, resizable: false, align: 'center', renderer: this.radioButtonRenderer3, tdCls: 'pseudoinput-column' },
            { text: 'Товар/услуга', dataIndex: 'p_product_display', width: 60, locked: true },
            { text: 'Ед. изм.', dataIndex: 'p_unit_display', width: 60, locked: true },
            {
                text: 'Январь',
                columns: [
                    { text: 'План', dataIndex: 'p_january_plan', width: 70 },
                    { text: 'Факт', dataIndex: 'p_january_fact', width: 70 },
                    { text: '+/-, %', dataIndex: 'p_january_percents', width: 70, renderer: this.percentRenderer }
                ]
            },
            {
                text: 'Февраль',
                columns: [
                    { text: 'План', dataIndex: 'p_february_plan', width: 70 },
                    { text: 'Факт', dataIndex: 'p_february_fact', width: 70 },
                    { text: '+/-, %', dataIndex: 'p_february_percents', width: 70, renderer: this.percentRenderer }
                ]
            },
            {
                text: 'Март',
                columns: [
                    { text: 'План', dataIndex: 'p_march_plan', width: 70 },
                    { text: 'Факт', dataIndex: 'p_march_fact', width: 70 },
                    { text: '+/-, %', dataIndex: 'p_march_percents', width: 70, renderer: this.percentRenderer }
                ]
            },
            {
                text: 'I квартал',
                columns: [
                    { text: 'План', dataIndex: 'p_1_quarter_plan', width: 70 },
                    { text: 'Факт', dataIndex: 'p_1_quarter_fact', width: 70 },
                    { text: '+/-, %', dataIndex: 'p_1_quarter_percents', width: 70, renderer: this.percentRenderer }
                ]
            },
            {
                text: 'Апрель',
                columns: [
                    { text: 'План', dataIndex: 'p_april_plan', width: 70 },
                    { text: 'Факт', dataIndex: 'p_april_fact', width: 70 },
                    { text: '+/-, %', dataIndex: 'p_april_percents', width: 70, renderer: this.percentRenderer }
                ]
            },
            {
                text: 'Май',
                columns: [
                    { text: 'План', dataIndex: 'p_may_plan', width: 70 },
                    { text: 'Факт', dataIndex: 'p_may_fact', width: 70 },
                    { text: '+/-, %', dataIndex: 'p_may_percents', width: 70, renderer: this.percentRenderer }
                ]
            },
            {
                text: 'Июнь',
                columns: [
                    { text: 'План', dataIndex: 'p_june_plan', width: 70 },
                    { text: 'Факт', dataIndex: 'p_june_fact', width: 70 },
                    { text: '+/-, %', dataIndex: 'p_june_percents', width: 70, renderer: this.percentRenderer }
                ]
            },
            {
                text: 'II квартал',
                columns: [
                    { text: 'План', dataIndex: 'p_2_quarter_plan', width: 70 },
                    { text: 'Факт', dataIndex: 'p_2_quarter_fact', width: 70 },
                    { text: '+/-, %', dataIndex: 'p_2_quarter_percents', width: 70, renderer: this.percentRenderer }
                ]
            },
            {
                text: 'I полугодие',
                columns: [
                    { text: 'План', dataIndex: 'p_1_half_plan', width: 70 },
                    { text: 'Факт', dataIndex: 'p_1_half_fact', width: 70 },
                    { text: '+/-, %', dataIndex: 'p_1_half_percents', width: 70, renderer: this.percentRenderer }
                ]
            },
            {
                text: 'Июль',
                columns: [
                    { text: 'План', dataIndex: 'p_july_plan', width: 70 },
                    { text: 'Факт', dataIndex: 'p_july_fact', width: 70 },
                    { text: '+/-, %', dataIndex: 'p_july_percents', width: 70, renderer: this.percentRenderer }
                ]
            },
            {
                text: 'Август',
                columns: [
                    { text: 'План', dataIndex: 'p_august_plan', width: 70 },
                    { text: 'Факт', dataIndex: 'p_august_fact', width: 70 },
                    { text: '+/-, %', dataIndex: 'p_august_percents', width: 70, renderer: this.percentRenderer }
                ]
            },
            {
                text: 'Сентябрь',
                columns: [
                    { text: 'План', dataIndex: 'p_september_plan', width: 70 },
                    { text: 'Факт', dataIndex: 'p_september_fact', width: 70 },
                    { text: '+/-, %', dataIndex: 'p_september_percents', width: 70, renderer: this.percentRenderer }
                ]
            },
            {
                text: 'III квартал',
                columns: [
                    { text: 'План', dataIndex: 'p_3_quarter_plan', width: 70 },
                    { text: 'Факт', dataIndex: 'p_3_quarter_fact', width: 70 },
                    { text: '+/-, %', dataIndex: 'p_3_quarter_percents', width: 70, renderer: this.percentRenderer }
                ]
            },
            {
                text: 'За три квартала',
                columns: [
                    { text: 'План', dataIndex: 'p_for_3_quarters_plan', width: 70 },
                    { text: 'Факт', dataIndex: 'p_for_3_quarters_fact', width: 70 },
                    { text: '+/-, %', dataIndex: 'p_for_3_quarters_percents', width: 70, renderer: this.percentRenderer }
                ]
            },
            {
                text: 'Октябрь',
                columns: [
                    { text: 'План', dataIndex: 'p_october_plan', width: 70 },
                    { text: 'Факт', dataIndex: 'p_october_fact', width: 70 },
                    { text: '+/-, %', dataIndex: 'p_october_percents', width: 70, renderer: this.percentRenderer }
                ]
            },
            {
                text: 'Ноябрь',
                columns: [
                    { text: 'План', dataIndex: 'p_november_plan', width: 70 },
                    { text: 'Факт', dataIndex: 'p_november_fact', width: 70 },
                    { text: '+/-, %', dataIndex: 'p_november_percents', width: 70, renderer: this.percentRenderer }
                ]
            },
            {
                text: 'Декабрь',
                columns: [
                    { text: 'План', dataIndex: 'p_december_plan', width: 70 },
                    { text: 'Факт', dataIndex: 'p_december_fact', width: 70 },
                    { text: '+/-, %', dataIndex: 'p_december_percents', width: 70, renderer: this.percentRenderer }
                ]
            },
            {
                text: 'IV квартал',
                columns: [
                    { text: 'План', dataIndex: 'p_4_quarter_plan', width: 70 },
                    { text: 'Факт', dataIndex: 'p_4_quarter_fact', width: 70 },
                    { text: '+/-, %', dataIndex: 'p_4_quarter_percents', width: 70, renderer: this.percentRenderer }
                ]
            },
            {
                text: 'II полугодие',
                columns: [
                    { text: 'План', dataIndex: 'p_2_half_plan', width: 70 },
                    { text: 'Факт', dataIndex: 'p_2_half_fact', width: 70 },
                    { text: '+/-, %', dataIndex: 'p_2_half_percents', width: 70, renderer: this.percentRenderer }
                ]
            },
            {
                text: 'Всего',
                columns: [
                    { text: 'План', dataIndex: 'p_total_plan', width: 70 },
                    { text: 'Факт', dataIndex: 'p_total_fact', width: 70 },
                    { text: '+/-, %', dataIndex: 'p_total_percents', width: 70, renderer: this.percentRenderer }
                ]
            }
        ];
        return this.mergeActions(result);
    },

    initComponent: function () {
        Ext.apply(this, {
            title: this.shownTitle
        });
        this.callParent(arguments);
    }
});