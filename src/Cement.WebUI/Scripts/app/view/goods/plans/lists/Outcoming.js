Ext.define('Cement.view.goods.plans.lists.Outcoming', {
	extend: 'Cement.view.goods.plans.lists.New',
	alias: 'widget.goods_plans_lists_outcoming',

	gridStore: 'goods.plans.Outcoming',
	gridStateId: 'stateGoodsPlansOutcoming',

	printUrl: Cement.Config.url.goods.plans.outcoming.printGrid,
	helpUrl: Cement.Config.url.goods.plans.outcoming.help,
    deleteUrl: Cement.Config.url.goods.plans.outcoming.deleteUrl,
    signItemUrl: Cement.Config.url.goods.plans.outcoming.signUrl,

    shownTitle: null,

    getActionColumns: function () {
        return {
            xtype: 'rowactions',
            hideable: false,
            resizeable: false,
            locked: true,
            width: 46,
            actions: [
                {
                    iconCls: 'icon-delete-item',
                    qtip: 'Удалить',
                    callback: this.deleteItem
                },
                {
                    iconCls: 'icon-archive-item',
                    qtip: 'В архив',
                    callback: this.archiveItem
                }
            ],
            keepSelection: true
        };
    }
});