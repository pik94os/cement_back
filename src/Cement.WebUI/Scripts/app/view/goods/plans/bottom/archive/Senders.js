Ext.define('Cement.view.goods.plans.bottom.archive.Senders', {
  extend: 'Cement.view.goods.auto.bottom.new_r.Senders',
  alias: 'widget.goods_plans_bottom_archive_senders',
  gridStore: 'goods.plans.bottom.archive.Senders',
  gridStateId: 'stateGoodsPlansBottomArchiveSenders',
  showFilterButton: false,
  autoLoadStore: false,

  printItemUrl: Cement.Config.url.goods.senders.printItem,
  printUrl: Cement.Config.url.goods.senders.printGrid,
  helpUrl: Cement.Config.url.goods.senders.help
});