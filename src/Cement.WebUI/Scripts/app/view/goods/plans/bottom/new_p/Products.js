Ext.define('Cement.view.goods.plans.bottom.new_p.Products', {
    extend: 'Cement.view.products.price.bottom.NewProducts',
    alias: 'widget.goods_plans_bottom_new_products',
    gridStore: 'goods.plans.bottom.new_p.Products',
    gridStateId: 'stateGoodsPlansBottomNewProducts'
});