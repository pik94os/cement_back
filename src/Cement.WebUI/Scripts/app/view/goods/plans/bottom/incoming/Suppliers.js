Ext.define('Cement.view.goods.plans.bottom.incoming.Suppliers', {
	extend: 'Cement.view.goods.auto.bottom.new_r.Senders',
	alias: 'widget.goods_plans_bottom_incoming_suppliers',
  gridStore: 'goods.plans.bottom.incoming.Suppliers',
  gridStateId: 'stateGoodsPlansBottomIncomingSuppliers',
  showFilterButton: false,

  printItemUrl: Cement.Config.url.goods.suppliers.printItem,
  printUrl: Cement.Config.url.goods.suppliers.printGrid,
  helpUrl: Cement.Config.url.goods.suppliers.help
});