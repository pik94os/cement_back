Ext.define('Cement.view.goods.plans.bottom.new_p.Payers', {
	extend: 'Cement.view.goods.auto.bottom.new_r.Senders',
	alias: 'widget.goods_plans_bottom_new_payers',
  gridStore: 'goods.plans.bottom.new_p.Payers',
  gridStateId: 'stateGoodsPlansBottomNewPayers',
  showFilterButton: false,

  printItemUrl: Cement.Config.url.goods.payers.printItem,
  printUrl: Cement.Config.url.goods.payers.printGrid,
  helpUrl: Cement.Config.url.goods.payers.help
});