Ext.define('Cement.view.goods.plans.bottom.incoming.Routes', {
    extend: 'Cement.view.products.price.bottom.NewRoutes',
    alias: 'widget.goods_plans_bottom_incoming_routes',
    gridStore: 'goods.plans.bottom.incoming.Routes',
    gridStateId: 'stateGoodsPlansBottomIncomingRoutes'
});