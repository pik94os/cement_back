Ext.define('Cement.view.goods.plans.bottom.outcoming.Routes', {
    extend: 'Cement.view.products.price.bottom.NewRoutes',
    alias: 'widget.goods_plans_bottom_outcoming_routes',
    gridStore: 'goods.plans.bottom.outcoming.Routes',
    gridStateId: 'stateGoodsPlansBottomOutcomingRoutes'
});