Ext.define('Cement.view.goods.plans.bottom.archive.Payers', {
	extend: 'Cement.view.goods.auto.bottom.new_r.Senders',
	alias: 'widget.goods_plans_bottom_archive_payers',
  gridStore: 'goods.plans.bottom.archive.Payers',
  gridStateId: 'stateGoodsPlansBottomArchivePayers',
  showFilterButton: false,

  printItemUrl: Cement.Config.url.goods.payers.printItem,
  printUrl: Cement.Config.url.goods.payers.printGrid,
  helpUrl: Cement.Config.url.goods.payers.help
});