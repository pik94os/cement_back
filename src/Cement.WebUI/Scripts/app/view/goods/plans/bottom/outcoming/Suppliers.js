Ext.define('Cement.view.goods.plans.bottom.outcoming.Suppliers', {
	extend: 'Cement.view.goods.auto.bottom.new_r.Senders',
	alias: 'widget.goods_plans_bottom_outcoming_suppliers',
  gridStore: 'goods.plans.bottom.outcoming.Suppliers',
  gridStateId: 'stateGoodsPlansBottomOutcomingSuppliers',
  showFilterButton: false,

  printItemUrl: Cement.Config.url.goods.suppliers.printItem,
  printUrl: Cement.Config.url.goods.suppliers.printGrid,
  helpUrl: Cement.Config.url.goods.suppliers.help
});