Ext.define('Cement.view.goods.plans.bottom.new_p.Routes', {
    extend: 'Cement.view.products.price.bottom.NewRoutes',
    alias: 'widget.goods_plans_bottom_new_routes',
    gridStore: 'goods.plans.bottom.new_p.Routes',
    gridStateId: 'stateGoodsPlansBottomNewRoutes'
});