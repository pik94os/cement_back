Ext.define('Cement.view.goods.plans.bottom.outcoming.Senders', {
  extend: 'Cement.view.goods.auto.bottom.new_r.Senders',
  alias: 'widget.goods_plans_bottom_outcoming_senders',
  gridStore: 'goods.plans.bottom.outcoming.Senders',
  gridStateId: 'stateGoodsPlansBottomOutcomingSenders',
  showFilterButton: false,
  autoLoadStore: false,

  printItemUrl: Cement.Config.url.goods.senders.printItem,
  printUrl: Cement.Config.url.goods.senders.printGrid,
  helpUrl: Cement.Config.url.goods.senders.help
});