Ext.define('Cement.view.goods.plans.bottom.incoming.ConnectedDocumentsIncoming', {
	extend: 'Cement.view.goods.auto.lists.ConnectedDocuments',
	alias: 'widget.goods_plans_bottom_incoming_connected_documents_incoming',

	gridStore: 'goods.plans.bottom.incoming.ConnectedDocumentsIncoming',
	gridStateId: 'stateGoodsPlansBottomIncomingConnectedDocumentsIncoming',

    printUrl: Cement.Config.url.goods.plans.documents.printGrid,
    helpUrl: Cement.Config.url.goods.plans.documents.help
});