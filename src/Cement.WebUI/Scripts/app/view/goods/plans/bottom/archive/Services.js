Ext.define('Cement.view.goods.plans.bottom.archive.Services', {
    extend: 'Cement.view.products.price.bottom.NewServices',
    alias: 'widget.goods_plans_bottom_archive_services',
    gridStore: 'goods.plans.bottom.archive.Services',
    gridStateId: 'stateGoodsPlansBottomArchiveServices'
});