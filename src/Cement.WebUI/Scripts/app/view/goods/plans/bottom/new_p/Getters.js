Ext.define('Cement.view.goods.plans.bottom.new_p.Getters', {
	extend: 'Cement.view.goods.auto.bottom.new_r.Senders',
	alias: 'widget.goods_plans_bottom_new_getters',
  gridStore: 'goods.plans.bottom.new_p.Getters',
  gridStateId: 'stateGoodsPlansBottomNewGetters',
  showFilterButton: false,

  printItemUrl: Cement.Config.url.goods.getters.printItem,
  printUrl: Cement.Config.url.goods.getters.printGrid,
  helpUrl: Cement.Config.url.goods.getters.help
});