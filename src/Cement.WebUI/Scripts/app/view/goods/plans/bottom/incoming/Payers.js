Ext.define('Cement.view.goods.plans.bottom.incoming.Payers', {
	extend: 'Cement.view.goods.auto.bottom.new_r.Senders',
	alias: 'widget.goods_plans_bottom_incoming_payers',
  gridStore: 'goods.plans.bottom.incoming.Payers',
  gridStateId: 'stateGoodsPlansBottomIncomingPayers',
  showFilterButton: false,

  printItemUrl: Cement.Config.url.goods.payers.printItem,
  printUrl: Cement.Config.url.goods.payers.printGrid,
  helpUrl: Cement.Config.url.goods.payers.help
});