Ext.define('Cement.view.goods.plans.bottom.archive.Getters', {
	extend: 'Cement.view.goods.auto.bottom.new_r.Senders',
	alias: 'widget.goods_plans_bottom_archive_getters',
  gridStore: 'goods.plans.bottom.archive.Getters',
  gridStateId: 'stateGoodsPlansBottomArchiveGetters',
  showFilterButton: false,

  printItemUrl: Cement.Config.url.goods.getters.printItem,
  printUrl: Cement.Config.url.goods.getters.printGrid,
  helpUrl: Cement.Config.url.goods.getters.help
});