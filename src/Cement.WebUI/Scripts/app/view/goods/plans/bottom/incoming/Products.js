Ext.define('Cement.view.goods.plans.bottom.incoming.Products', {
    extend: 'Cement.view.products.price.bottom.NewProducts',
    alias: 'widget.goods_plans_bottom_incoming_products',
    gridStore: 'goods.plans.bottom.incoming.Products',
    gridStateId: 'stateGoodsPlansBottomIncomingProducts'
});