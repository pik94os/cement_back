Ext.define('Cement.view.goods.plans.bottom.new_p.Services', {
    extend: 'Cement.view.products.price.bottom.NewServices',
    alias: 'widget.goods_plans_bottom_new_services',
    gridStore: 'goods.plans.bottom.new_p.Services',
    gridStateId: 'stateGoodsPlansBottomNewServices'
});