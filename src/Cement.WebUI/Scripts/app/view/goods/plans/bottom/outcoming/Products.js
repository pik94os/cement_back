Ext.define('Cement.view.goods.plans.bottom.outcoming.Products', {
    extend: 'Cement.view.products.price.bottom.NewProducts',
    alias: 'widget.goods_plans_bottom_outcoming_products',
    gridStore: 'goods.plans.bottom.outcoming.Products',
    gridStateId: 'stateGoodsPlansBottomOutcomingProducts'
});