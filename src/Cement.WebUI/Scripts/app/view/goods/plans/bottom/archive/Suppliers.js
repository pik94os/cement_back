Ext.define('Cement.view.goods.plans.bottom.archive.Suppliers', {
	extend: 'Cement.view.goods.auto.bottom.new_r.Senders',
	alias: 'widget.goods_plans_bottom_archive_suppliers',
  gridStore: 'goods.plans.bottom.archive.Suppliers',
  gridStateId: 'stateGoodsPlansBottomArchiveSuppliers',
  showFilterButton: false,

  printItemUrl: Cement.Config.url.goods.suppliers.printItem,
  printUrl: Cement.Config.url.goods.suppliers.printGrid,
  helpUrl: Cement.Config.url.goods.suppliers.help
});