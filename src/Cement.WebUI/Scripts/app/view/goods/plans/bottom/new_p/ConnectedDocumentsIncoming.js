Ext.define('Cement.view.goods.plans.bottom.new_p.ConnectedDocumentsIncoming', {
	extend: 'Cement.view.goods.auto.lists.ConnectedDocuments',
	alias: 'widget.goods_plans_bottom_new_connected_documents_incoming',

	gridStore: 'goods.plans.bottom.new_p.ConnectedDocumentsIncoming',
	gridStateId: 'stateGoodsPlansBottomNewConnectedDocumentsIncoming',

    printUrl: Cement.Config.url.goods.plans.documents.printGrid,
    helpUrl: Cement.Config.url.goods.plans.documents.help
});