Ext.define('Cement.view.goods.plans.bottom.incoming.Getters', {
	extend: 'Cement.view.goods.auto.bottom.new_r.Senders',
	alias: 'widget.goods_plans_bottom_incoming_getters',
  gridStore: 'goods.plans.bottom.incoming.Getters',
  gridStateId: 'stateGoodsPlansBottomIncomingGetters',
  showFilterButton: false,

  printItemUrl: Cement.Config.url.goods.getters.printItem,
  printUrl: Cement.Config.url.goods.getters.printGrid,
  helpUrl: Cement.Config.url.goods.getters.help
});