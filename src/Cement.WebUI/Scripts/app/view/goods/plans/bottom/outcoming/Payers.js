Ext.define('Cement.view.goods.plans.bottom.outcoming.Payers', {
	extend: 'Cement.view.goods.auto.bottom.new_r.Senders',
	alias: 'widget.goods_plans_bottom_outcoming_payers',
  gridStore: 'goods.plans.bottom.outcoming.Payers',
  gridStateId: 'stateGoodsPlansBottomOutcomingPayers',
  showFilterButton: false,

  printItemUrl: Cement.Config.url.goods.payers.printItem,
  printUrl: Cement.Config.url.goods.payers.printGrid,
  helpUrl: Cement.Config.url.goods.payers.help
});