Ext.define('Cement.view.goods.plans.bottom.archive.Routes', {
    extend: 'Cement.view.products.price.bottom.NewRoutes',
    alias: 'widget.goods_plans_bottom_archive_routes',
    gridStore: 'goods.plans.bottom.archive.Routes',
    gridStateId: 'stateGoodsPlansBottomArchiveRoutes'
});