Ext.define('Cement.view.goods.plans.bottom.archive.ConnectedDocumentsIncoming', {
	extend: 'Cement.view.goods.auto.lists.ConnectedDocuments',
	alias: 'widget.goods_plans_bottom_archive_connected_documents_incoming',

	gridStore: 'goods.plans.bottom.archive.ConnectedDocumentsIncoming',
	gridStateId: 'stateGoodsPlansBottomArchiveConnectedDocumentsIncoming',

    printUrl: Cement.Config.url.goods.plans.documents.printGrid,
    helpUrl: Cement.Config.url.goods.plans.documents.help
});