Ext.define('Cement.view.goods.plans.bottom.outcoming.Getters', {
	extend: 'Cement.view.goods.auto.bottom.new_r.Senders',
	alias: 'widget.goods_plans_bottom_outcoming_getters',
  gridStore: 'goods.plans.bottom.outcoming.Getters',
  gridStateId: 'stateGoodsPlansBottomOutcomingGetters',
  showFilterButton: false,

  printItemUrl: Cement.Config.url.goods.getters.printItem,
  printUrl: Cement.Config.url.goods.getters.printGrid,
  helpUrl: Cement.Config.url.goods.getters.help
});