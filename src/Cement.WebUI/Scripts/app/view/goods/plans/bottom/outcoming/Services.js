Ext.define('Cement.view.goods.plans.bottom.outcoming.Services', {
    extend: 'Cement.view.products.price.bottom.NewServices',
    alias: 'widget.goods_plans_bottom_outcoming_services',
    gridStore: 'goods.plans.bottom.outcoming.Services',
    gridStateId: 'stateGoodsPlansBottomOutcomingServices'
});