Ext.define('Cement.view.goods.plans.bottom.archive.Products', {
    extend: 'Cement.view.products.price.bottom.NewProducts',
    alias: 'widget.goods_plans_bottom_archive_products',
    gridStore: 'goods.plans.bottom.archive.Products',
    gridStateId: 'stateGoodsPlansBottomArchiveProducts'
});