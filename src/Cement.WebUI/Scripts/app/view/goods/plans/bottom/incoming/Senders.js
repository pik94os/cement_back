Ext.define('Cement.view.goods.plans.bottom.incoming.Senders', {
  extend: 'Cement.view.goods.auto.bottom.new_r.Senders',
  alias: 'widget.goods_plans_bottom_incoming_senders',
  gridStore: 'goods.plans.bottom.incoming.Senders',
  gridStateId: 'stateGoodsPlansBottomIncomingSenders',
  showFilterButton: false,
  autoLoadStore: false,

  printItemUrl: Cement.Config.url.goods.senders.printItem,
  printUrl: Cement.Config.url.goods.senders.printGrid,
  helpUrl: Cement.Config.url.goods.senders.help
});