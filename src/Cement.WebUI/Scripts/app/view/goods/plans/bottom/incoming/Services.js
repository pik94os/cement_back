Ext.define('Cement.view.goods.plans.bottom.incoming.Services', {
    extend: 'Cement.view.products.price.bottom.NewServices',
    alias: 'widget.goods_plans_bottom_incoming_services',
    gridStore: 'goods.plans.bottom.incoming.Services',
    gridStateId: 'stateGoodsPlansBottomIncomingServices'
});