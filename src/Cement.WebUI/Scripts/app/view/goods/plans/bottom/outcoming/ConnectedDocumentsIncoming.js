Ext.define('Cement.view.goods.plans.bottom.outcoming.ConnectedDocumentsIncoming', {
	extend: 'Cement.view.goods.auto.lists.ConnectedDocuments',
	alias: 'widget.goods_plans_bottom_outcoming_connected_documents_incoming',

	gridStore: 'goods.plans.bottom.outcoming.ConnectedDocumentsIncoming',
	gridStateId: 'stateGoodsPlansBottomOutcomingConnectedDocumentsIncoming',

    printUrl: Cement.Config.url.goods.plans.documents.printGrid,
    helpUrl: Cement.Config.url.goods.plans.documents.help
});