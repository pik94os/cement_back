Ext.define('Cement.view.goods.plans.charts.New', {
	extend: 'Ext.panel.Panel',
	alias: 'widget.goods_plans_charts_new',
	layout: 'fit',
	border: 0,

	loadData: function (rec) {
		var data = [{
			name: 'Январь',
			data1: rec.get('p_january_plan'),
			data2: rec.get('p_january_fact'),
			data3: 0
		}, {
			name: 'Февраль',
			data1: rec.get('p_february_plan'),
			data2: rec.get('p_february_fact'),
			data3: 0
		}, {
			name: 'Март',
			data1: rec.get('p_march_plan'),
			data2: rec.get('p_march_fact'),
			data3: 0
		}, {
			name: 'Апрель',
			data1: rec.get('p_april_plan'),
			data2: rec.get('p_april_fact'),
			data3: 0
		}, {
			name: 'Май',
			data1: rec.get('p_may_plan'),
			data2: rec.get('p_may_fact'),
			data3: 0
		}, {
			name: 'Июнь',
			data1: rec.get('p_june_plan'),
			data2: rec.get('p_june_fact'),
			data3: 0
		}, {
			name: 'Июль',
			data1: rec.get('p_july_plan'),
			data2: rec.get('p_july_fact'),
			data3: 0
		}, {
			name: 'Август',
			data1: rec.get('p_august_plan'),
			data2: rec.get('p_august_fact'),
			data3: 0
		}, {
			name: 'Сентябрь',
			data1: rec.get('p_september_plan'),
			data2: rec.get('p_september_fact'),
			data3: 0
		}, {
			name: 'Октябрь',
			data1: rec.get('p_october_plan'),
			data2: rec.get('p_october_fact'),
			data3: 0
		}, {
			name: 'Ноябрь',
			data1: rec.get('p_november_plan'),
			data2: rec.get('p_november_fact'),
			data3: 0
		}, {
			name: 'Декабрь',
			data1: rec.get('p_december_plan'),
			data2: rec.get('p_december_fact'),
			data3: 0
		}];
		this.store.loadData(data);
	},

	initComponent: function () {
		window.generateData = function(n, floor){
	        var data = [],
	            p = (Math.random() *  11) + 1,
	            i;
	            
	        floor = (!floor && floor !== 0)? 20 : floor;
	        
	        for (i = 0; i < (n || 12); i++) {
	            data.push({
	                name: Ext.Date.monthNames[i % 12],
	                data1: 0,
	                data2: 0,
	                data3: 0
	            });
	        }
	        return data;
	    };
		this.store = Ext.create('Ext.data.JsonStore', {
	        fields: ['name', 'data1', 'data2', 'data3'],
	        data: generateData()
	    });
		Ext.apply(this, {
			items: {
	            xtype: 'chart',
	            style: 'background:#fff',
	            padding: 15,
	            animate: true,
	            store: this.store,
	            axes: [{
	                type: 'Category',
	                position: 'bottom',
	                fields: ['name'],
	                title: 'Месяц',
	                grid: true
	            }, {
	                type: 'Numeric',
	                position: 'left',
	                fields: ['data1', 'data2'],
	                title: 'Month of the Year',
	                hidden: true
	            }],
	            series: [{
                    type: 'column',
                    axis: 'left',
                    highlight: true,
                    xField: 'name',
                    label: {
                    	field: ['data1', null],
                    	orientation: 'vertical',
                    	display: 'insideStart',
                    	renderer: function () {
                    		return 'План'
                    	}
                    },
                    yField: ['data1', 'qwerqwer']
                },
                {
                    type: 'column',
                    axis: 'right',
                    highlight: true,
                    xField: 'name',
                    label: {
                    	field: [null, 'data2'],
                    	orientation: 'vertical',
                    	display: 'insideStart',
                    	renderer: function () {
                    		return 'Факт'
                    	}
                    },
                    yField: ['qwerqwer', 'data2']
                }]
	        }
		});
		this.callParent(arguments);
	}
});