Ext.define('Cement.view.goods.plans.Total', {
	extend: 'Cement.view.basic.Tabs',
	title: 'Планы',

	items: [{
		xtype: 'goods_plans_complex_new'
	}, {
		xtype: 'goods_plans_complex_incoming'
	}, {
		xtype: 'goods_plans_complex_outcoming'
	}, {
		xtype: 'goods_plans_complex_archive'
	}],

	initComponent: function () {
		Ext.apply(this, {
			title: 'Планы'
		});
		this.callParent(arguments);
	}
});