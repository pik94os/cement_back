Ext.define('Cement.view.goods.auto.complex.Outcoming', {
	extend: 'Cement.view.basic.Complex',
	alias: 'widget.goods_auto_complex_outcoming',
    border: 0,
    enableDetail: false,
    autoLoadStore: false,
    priceId: null,
    bodyPadding: 0,
    padding: 0,
    header: false,
    title: null,
    rowsCount: 3,
    title: 'Исходящие',
    tabTitle: 'Исходящие',
    cls: 'no-side-borders',
    topXType: 'goods_auto_lists_outcoming',
    topDetailType: 'Cement.view.goods.auto.view.Outcoming',
    bottomTitle: 'Связанные документы',
    bottomTabs: [{
        title: 'Связанные документы',
        xtype: 'goods_auto_bottom_outcoming_connected_documents_incoming',
        detailType: 'Cement.view.goods.auto.view.ConnectedDocument',
        constantFilterParam: 'p_base_doc',
        childrenConstantFilterParam: 'p_base_doc',
        border: 0
    }],
    subBottomTitle: 'Справочник',
    subBottomTabs: [
    //{
    //    title: 'Грузоотправитель',
    //    shownTitle: 'Грузоотправитель',
    //    xtype: 'goods_auto_bottom_outcoming_senders',
    //    detailType: 'Cement.view.corporate.firm.View'
    //},
    {
        title: 'Грузополучатель',
        shownTitle: 'Грузополучатель',
        xtype: 'goods_auto_bottom_outcoming_getters',
        detailType: 'Cement.view.corporate.firm.View'
    }, {
        title: 'Склад грузополучателя',
        xtype: 'goods_auto_bottom_warehouses',
        detailType: 'Cement.view.corporate.warehouse.View'
    }, {
        title: 'Плательщик',
        shownTitle: 'Плательщик',
        xtype: 'goods_auto_bottom_outcoming_payers',
        detailType: 'Cement.view.corporate.firm.View'
    }, {
        title: 'Поставщик',
        shownTitle: 'Поставщик',
        xtype: 'goods_auto_bottom_outcoming_suppliers',
        detailType: 'Cement.view.corporate.firm.View'
    }, {
        title: 'Товары',
        shownTitle: 'Товары',
        xtype: 'goods_auto_bottom_outcoming_products',
        detailType: 'Cement.view.products.products.View'
    }, {
        title: 'Услуги',
        shownTitle: 'Услуги',
        xtype: 'goods_auto_bottom_outcoming_services',
        detailType: 'Cement.view.products.services.View'
    },
    {
        title: 'ТЕ',
        xtype: 'goods_auto_bottom_transportunits',
        detailType: 'Cement.view.transport.units.View'
    },
    {
        title: 'Маршрут',
        shownTitle: 'Маршрут',
        xtype: 'goods_auto_bottom_outcoming_routes',
        detailType: 'Cement.view.products.price.RouteView'
    }]
});