Ext.define('Cement.view.goods.auto.complex.New', {
	extend: 'Cement.view.basic.Complex',
	alias: 'widget.goods_auto_complex_new',
    border: 0,
    enableDetail: false,
    autoLoadStore: false,
    priceId: null,
    bodyPadding: 0,
    padding: 0,
    header: false,
    rowsCount: 3,
    title: 'Новые',
    tabTitle: 'Новые',
    cls: 'no-side-borders',
    topXType: 'goods_auto_lists_new',
    topDetailType: 'Cement.view.goods.auto.view.New',
    bottomTitle: 'Связанные документы',
    bottomTabs: [{
        title: 'Связанные документы',
        xtype: 'goods_auto_bottom_new_connected_documents_incoming',
        detailType: 'Cement.view.goods.auto.view.ConnectedDocument',
        constantFilterParam: 'p_base_doc',
        childrenConstantFilterParam: 'p_base_doc',
        border: 0
    }],
    subBottomTitle: 'Справочник',
    subBottomTabs: [
    //{
    //    title: 'Грузоотправитель',
    //    shownTitle: 'Грузоотправитель',
    //    xtype: 'goods_auto_bottom_new_senders',
    //    detailType: 'Cement.view.corporate.firm.View'
    //},
    {
        title: 'Грузополучатель',
        shownTitle: 'Грузополучатель',
        xtype: 'goods_auto_bottom_new_getters',
        detailType: 'Cement.view.corporate.firm.View'
    }, {
        title: 'Склад грузополучателя',
        xtype: 'goods_auto_bottom_warehouses',
        detailType: 'Cement.view.corporate.warehouse.View'
    }, {
        title: 'Плательщик',
        shownTitle: 'Плательщик',
        xtype: 'goods_auto_bottom_new_payers',
        detailType: 'Cement.view.corporate.firm.View'
    }, {
        title: 'Поставщик',
        shownTitle: 'Поставщик',
        xtype: 'goods_auto_bottom_new_suppliers',
        detailType: 'Cement.view.corporate.firm.View'
    }, {
        title: 'Товары',
        shownTitle: 'Товары',
        xtype: 'goods_auto_bottom_new_products',
        detailType: 'Cement.view.products.products.View'
    }, {
        title: 'Услуги',
        shownTitle: 'Услуги',
        xtype: 'goods_auto_bottom_new_services',
        detailType: 'Cement.view.products.services.View'
    }, {
        title: 'Маршрут',
        shownTitle: 'Маршрут',
        xtype: 'goods_auto_bottom_new_routes',
        detailType: 'Cement.view.products.price.RouteView'
    }]
});