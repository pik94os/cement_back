Ext.define('Cement.view.goods.auto.view.ConnectedDocument', {
	extend: 'Cement.view.basic.Form',
	alias: 'widget.goods_auto_view_connected_document',
	fieldsDisabled: true,
	buttons: null,
	border: 0,
	tbar: Cement.Config.modelViewButtons,
	layout: 'autocontainer',
	bbar: null,
	noUserFieldsetsControl: false,
	bodyCls: 'model-form',

	initComponent: function ()  {
		Ext.apply(this, {
			items: [{
				xtype: 'panel',
				border: 0,
				layout: 'anchor',
				defaultType: 'textfield',
				anchor: '100%',
				bodyPadding: '10 10 10 5',
				autoScroll: true,
				defaults: {
					anchor: '100%',
					labelWidth: 110
				},
				items: [{
					xtype: 'textfield',
					name: 'p_number',
					fieldLabel: 'Номер',
					disabled: true
				}, {
					xtype: 'textfield',
					name: 'p_date',
					fieldLabel: 'Дата',
					disabled: true
				}, {
					xtype: 'textfield',
					name: 'p_document_kind_display',
					fieldLabel: 'Вид',
					disabled: true
				}, {
					xtype: 'fieldset',
					title: 'Общие данные',
					defaults: {
						xtype: 'textfield',
						anchor: '100%',
						labelWidth: 110
					},
					layout: 'anchor',
					collapsible: true,
					collapsed: true,
					items: [{
						name: 'p_theme',
						xtype: 'textarea',
						disabled: true,
						fieldLabel: 'Тема документа'
					}]
				}, {
					xtype: 'fieldset',
					title: 'Номенклатура дел',
					defaults: {
						xtype: 'textfield',
						anchor: '100%',
						labelWidth: 110
					},
					layout: 'anchor',
					collapsible: true,
					collapsed: true,
					items: [{
						name: 'p_nom_index',
						disabled: true,
						fieldLabel: 'Индекс дела'
					}, {
						name: 'p_nom_title',
						disabled: true,
						fieldLabel: 'Заголовок дела'
					}, {
						name: 'p_nom_store_count',
						disabled: true,
						fieldLabel: 'Кол-во ед. хр.'
					}, {
						name: 'p_nom_end',
						disabled: true,
						fieldLabel: 'Срок хранения дела и № статей по перечню'
					}, {
						name: 'p_nom_comments',
						disabled: true,
						fieldLabel: 'Примечания'
					}, {
						name: 'p_nom_year',
						disabled: true,
						fieldLabel: 'Год'
					}]
				}, {
					xtype: 'fieldset',
					title: 'Автор',
					defaults: {
						xtype: 'textfield',
						anchor: '100%',
						labelWidth: 110
					},
					layout: 'anchor',
					collapsible: true,
					collapsed: true,
					items: [{
						name: 'p_author_name',
						disabled: true,
						fieldLabel: 'ФИО'
					}, {
						name: 'p_author_department',
						disabled: true,
						fieldLabel: 'Отдел'
					}, {
						name: 'p_author_position',
						disabled: true,
						fieldLabel: 'Должность'
					}]
				}, {
					xtype: 'fieldset',
					title: 'На подпись',
					defaults: {
						xtype: 'textfield',
						anchor: '100%',
						labelWidth: 110
					},
					layout: 'anchor',
					collapsible: true,
					collapsed: true,
					items: [{
						name: 'p_sign_for_name',
						disabled: true,
						fieldLabel: 'ФИО'
					}, {
						name: 'p_sign_for_department',
						disabled: true,
						fieldLabel: 'Отдел'
					}, {
						name: 'p_sign_for_position',
						disabled: true,
						fieldLabel: 'Должность'
					}]
				}]
			}]
		});
		this.callParent(arguments);
	}
});