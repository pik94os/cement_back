Ext.define('Cement.view.goods.auto.view.View', {
	extend: 'Cement.view.goods.auto.view.TemplateView',
	alias: 'widget.goods_auto_view_view',
	fieldsDisabled: true,
	buttons: null,
	border: 0,
	tbar: Cement.Config.modelViewButtons,
	layout: 'autocontainer',
	bbar: null,
	noUserFieldsetsControl: false,
	bodyCls: 'model-form',

	getCommonItems: function () {
	    return [
            {
                fieldLabel: 'Дата выгрузки',
                name: 'p_unload_date',
                disabled: true
            },
            {
                fieldLabel: 'Время выгрузки',
                name: 'p_unload_time',
                disabled: true
            }
            //{
			//    fieldLabel: 'Дата, время загрузки',
			//    name: 'p_load_time',
			//    disabled: true
            //},
            //{
			//    fieldLabel: 'Дата, время выгрузки',
			//    name: 'p_unload_time',
			//    disabled: true
            //},
            //{
			//    fieldLabel: 'Фактическая дата, время загрузки',
			//    disabled: true,
			//    name: 'p_fact_load_time'
            //},
            //{
			//    fieldLabel: 'Фактическое дата, время выгрузки',
			//    disabled: true,
			//    name: 'p_fact_unload_time'
		    //}
	    ];
	},

	getFirstFields: function () {
		return [{
			fieldLabel: 'Номер заявки',
			disabled: true,
			name: 'p_number'
		}, {
			fieldLabel: 'Дата заявки',
			disabled: true,
			name: 'p_date'
		}, {
			fieldLabel: 'Способ отгрузки',
			disabled: true,
			name: 'p_load_kind_display'
		}, {
			fieldLabel: 'Статус',
			disabled: true,
			name: 'p_status_display'
		}];
	},

	getItems: function () {
		return [{
				xtype: 'panel',
				border: 0,
				layout: 'anchor',
				defaultType: 'textfield',
				anchor: '100%',
				bodyPadding: '10 10 10 5',
				autoScroll: true,
				defaults: {
					anchor: '100%',
					labelWidth: 110
				},
				items: [{
					xtype: 'fieldset',
					layout: 'anchor',
					defaults: {
						xtype: 'textfield',
						anchor: '100%'
					},
					border: 0,
					items: this.getFirstFields()
				},
                {
					xtype: 'fieldset',
					collapsed: true,
					collapsible: true,
					title: 'Общие данные',
					defaults: {
						xtype: 'textfield',
						anchor: '100%'
					},
					items: this.getCommonItems()
				},
                {
					xtype: 'fieldset',
					collapsed: true,
					collapsible: true,
					title: 'Поставщик',
					defaults: {
						xtype: 'textfield',
						anchor: '100%'
					},
					items: this.getSupplierFields()
				}, {
					xtype: 'fieldset',
					collapsed: true,
					collapsible: true,
					title: 'Товар',
					defaults: {
						xtype: 'textfield',
						anchor: '100%'
					},
					items: this.getProductFields()
				}, {
					xtype: 'fieldset',
					collapsed: true,
					collapsible: true,
					title: 'Транспорт',
					defaults: {
						xtype: 'textfield',
						anchor: '100%'
					},
					items: this.getTransportFields()
				}, {
					xtype: 'fieldset',
					collapsed: true,
					collapsible: true,
					title: 'Плательщик',
					defaults: {
						xtype: 'textfield',
						anchor: '100%'
					},
					items: this.getPayerFields()
				}, {
					xtype: 'fieldset',
					collapsed: true,
					collapsible: true,
					title: 'Грузополучатель',
					defaults: {
						xtype: 'textfield',
						anchor: '100%'
					},
					items: this.getGetterFields()
				}]
		}]
	}
});