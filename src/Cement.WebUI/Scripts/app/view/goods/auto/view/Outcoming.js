Ext.define('Cement.view.goods.auto.view.Outcoming', {
	extend: 'Cement.view.goods.auto.view.TemplateView',
	alias: 'widget.goods_auto_view_outcoming',
	fieldsDisabled: true,
	buttons: null,
	border: 0,
	tbar: Cement.Config.modelViewButtons,
	layout: 'autocontainer',
	bbar: null,
	noUserFieldsetsControl: false,
	bodyCls: 'model-form',

	getItems: function () {
	    return [
	        {
	            xtype: 'panel',
	            border: 0,
	            layout: 'anchor',
	            defaultType: 'textfield',
	            anchor: '100%',
	            bodyPadding: '10 10 10 5',
	            autoScroll: true,
	            defaults: {
	                anchor: '100%',
	                labelWidth: 110
	            },
	            items: [
	                {
	                    xtype: 'fieldset',
	                    layout: 'anchor',
	                    defaults: {
	                        xtype: 'textfield',
	                        anchor: '100%'
	                    },
	                    border: 0,
	                    items: this.getFirstFields()
	                },
	                {
	                    xtype: 'fieldset',
	                    collapsed: true,
	                    collapsible: true,
	                    title: 'Общие данные',
	                    defaults: {
	                        xtype: 'textfield',
	                        anchor: '100%'
	                    },
	                    items: this.getCommonItems()
	                },
	                {
	                    xtype: 'fieldset',
	                    collapsed: true,
	                    collapsible: true,
	                    title: 'Поставщик',
	                    defaults: {
	                        xtype: 'textfield',
	                        anchor: '100%'
	                    },
	                    items: this.getSupplierFields()
	                },
                    {
                        xtype: 'fieldset',
                        collapsed: true,
                        collapsible: true,
                        title: 'Плательщик',
                        defaults: {
                            xtype: 'textfield',
                            anchor: '100%'
                        },
                        items: this.getPayerFields()
                    },
	                {
	                    xtype: 'fieldset',
	                    collapsed: true,
	                    collapsible: true,
	                    title: 'Грузополучатель',
	                    defaults: {
	                        xtype: 'textfield',
	                        anchor: '100%'
	                    },
	                    items: this.getGetterFields()
	                },
                    {
                        xtype: 'fieldset',
                        collapsed: true,
                        collapsible: true,
                        title: 'Склад грузополучателя',
                        defaults: {
                            xtype: 'textfield',
                            anchor: '100%'
                        },
                        items: this.getWarehouseFields()
                    },
	                {
	                    xtype: 'fieldset',
	                    collapsed: true,
	                    collapsible: true,
	                    title: 'Товар',
	                    defaults: {
	                        xtype: 'textfield',
	                        anchor: '100%'
	                    },
	                    items: this.getProductFields()
	                }, {
	                    xtype: 'fieldset',
	                    collapsed: true,
	                    collapsible: true,
	                    title: 'Транспортная единица',
	                    defaults: {
	                        xtype: 'textfield',
	                        anchor: '100%'
	                    },
	                    items: this.getTransportUnitFields()
	                }, {
	                    xtype: 'fieldset',
	                    collapsed: true,
	                    collapsible: true,
	                    title: 'Транспортные характеристики',
	                    defaults: {
	                        xtype: 'textfield',
	                        anchor: '100%'
	                    },
	                    items: this.getTransportFields()
	                }, {
	                    xtype: 'fieldset',
	                    collapsed: true,
	                    collapsible: true,
	                    title: 'Прочие данные',
	                    defaults: {
	                        xtype: 'textfield',
	                        anchor: '100%'
	                    },
	                    items: this.getAdditionalFields()
	                }
	            ]
	        }
	    ];
	}
});