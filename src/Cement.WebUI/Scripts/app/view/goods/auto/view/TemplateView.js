Ext.define('Cement.view.goods.auto.view.TemplateView', {
	extend: 'Cement.view.basic.Form',
	alias: 'widget.goods_auto_view_template_view',
	fieldsDisabled: true,
	buttons: null,
	border: 0,
	tbar: Cement.Config.modelViewButtons,
	layout: 'autocontainer',
	bbar: null,
    noUserFieldsetsControl: false,
    bodyCls: 'model-form',

    getCommonItems: function () {
        return [
            {
                fieldLabel: 'Дата выгрузки план',
                name: 'p_unload_date',
                disabled: true
            },
            {
                fieldLabel: 'Время выгрузки план',
                name: 'p_unload_time',
                disabled: true
            },
            {
                fieldLabel: 'Дата выгрузки факт',
                name: 'p_unload_date_fact',
                disabled: true
            },
            {
                fieldLabel: 'Время выгрузки факт',
                name: 'p_unload_time_fact',
                disabled: true
            },
            {
                fieldLabel: 'Дата погрузки план',
                name: 'p_shipment_date',
                disabled: true
            },
            {
                fieldLabel: 'Время погрузки план',
                name: 'p_shipment_time',
                disabled: true
            },
            {
                fieldLabel: 'Дата погрузки факт',
                name: 'p_shipment_date_fact',
                disabled: true
            },
            {
                fieldLabel: 'Время погрузки факт',
                name: 'p_shipment_time_fact',
                disabled: true
            }
        ];
    },

    getTransportUnitFields: function () {
        return [
            {
                fieldLabel: 'Наименование ТС',
                name: 'p_unit_name',
                disabled: true
            },
            {
                fieldLabel: 'Гос. номер',
                name: 'p_unit_number',
                disabled: true
            },
            {
                fieldLabel: 'Наименование ПТС',
                name: 'p_pts_name',
                disabled: true
            },
            {
                fieldLabel: 'Гос. номер',
                name: 'p_pts_number',
                disabled: true
            },
            {
                fieldLabel: 'Водитель (Ф.И.О.)',
                name: 'p_driver',
                disabled: true
            }
        ];
    },

    getTransportFields: function () {
		return [{
		    name: 'p_char_type_display',
			fieldLabel: 'Тип кузова',
			disabled: true
		}, {
			xtype: 'checkboxgroup',
	        fieldLabel: 'Способ загрузки',
	        columns: this.getCheckGroupColumnsCount(),
	        items: [{
	        	xtype: 'checkbox',
	        	boxLabel: 'Задний',
	        	inputValue: 1,
	        	disabled: true,
	        	name: 'p_char_load_back'
	        }, {
	        	xtype: 'checkbox',
	        	boxLabel: 'Боковой',
	        	inputValue: 1,
	        	disabled: true,
	        	name: 'p_char_load_left'
	        }, {
	        	xtype: 'checkbox',
	        	boxLabel: 'Верхний',
	        	inputValue: 1,
	        	disabled: true,
	        	name: 'p_char_load_top'
	        }]
		}, {
		    name: 'p_char_additional_display',
			fieldLabel: 'Дополнительные опции',
			disabled: true
		}, {
			fieldLabel: 'Дополнительные характеристики',
			name: 'p_add_additional',
			disabled: true
		}];
	},

	getSupplierFields: function () {
	    return [{
	            fieldLabel: 'Наименование',
	            name: 'p_supplier_display',
	            disabled: true
	        }, {
	            fieldLabel: 'Адрес',
	            name: 'p_supplier_address',
	            disabled: true
	        }, {
	            fieldLabel: 'Контактное лицо',
	            name: 'p_supplier_contact_display',
	            disabled: true
	        }
	    ];
	},

	getProductFields: function () {
	    return [
            {
                name: 'p_contract_display',
                fieldLabel: 'Договор',
                disabled: true
            },
            {
			    name: 'p_product_display',
			    fieldLabel: 'Наименование',
			    disabled: true
		    },
            {
			    name: 'p_product_unit_display',
			    fieldLabel: 'Единица измерения',
			    disabled: true
            },
            {
                name: 'p_product_count',
                fieldLabel: 'Количество план',
                disabled: true
            },
            {
                name: 'p_product_price',
                fieldLabel: 'Цена',
                disabled: true
            },
            {
                name: 'p_product_tax',
                fieldLabel: 'Налог',
                disabled: true
            },
            {
                name: 'p_product_sum',
                fieldLabel: 'Сумма',
                disabled: true
            }
		];
	},

	getPayerFields: function () {
	    return [{
	            name: 'p_payer_display',
	            fieldLabel: 'Наименование',
	            disabled: true
	        }, {
	            fieldLabel: 'Адрес',
	            name: 'p_payer_address',
	            disabled: true
	        }, {
	            name: 'p_payer_contact_display',
	            fieldLabel: 'Контактное лицо',
	            disabled: true
	        }
	    ];
	},

	getGetterFields: function () {
		return [{
				name: 'p_getter_display',
				fieldLabel: 'Наименование',
				disabled: true
			}, {
				fieldLabel: 'Адрес',
				name: 'p_getter_address',
				disabled: true
			}, {
				name: 'p_getter_contact_display',
				fieldLabel: 'Контактное лицо',
				disabled: true
			}
		];
	},

    getWarehouseFields: function() {
        return [{
                name: 'p_warehouse_display',
                fieldLabel: 'Наименование',
                disabled: true
            }, {
                fieldLabel: 'Адрес',
                name: 'p_warehouse_address',
                disabled: true
            }, {
                fieldLabel: 'Время работы',
                name: 'p_warehouse_work_time',
                disabled: true
            }
        ];
    },

	getCheckGroupColumnsCount: function () {
		var result = 3;
		if (this.fieldsDisabled) {
			result = 1;
		}
		return result;
	},

	getFirstFields: function () {
	    return [{
	        fieldLabel: 'Номер заявки',
	        disabled: true,
	        name: 'p_number'
	    }, {
	        fieldLabel: 'Дата заявки',
	        disabled: true,
	        name: 'p_date'
	    }, {
	        fieldLabel: 'Способ отгрузки',
	        disabled: true,
	        name: 'p_load_kind_display'
	    }, {
	        fieldLabel: 'Статус',
	        disabled: true,
	        name: 'p_status_display'
	    }];
	},

	getAdditionalFields: function () {
	    return [
	    {
	        fieldLabel: 'Примечание',
	        xtype: 'textarea',
	        disabled: true,
	        name: 'p_comment'
	    }, {
	        fieldLabel: 'Важность',
	        disabled: true,
	        name: 'p_importance_name'
	    }, {
	        fieldLabel: 'Секретность',
	        disabled: true,
	        name: 'p_security_name'
	    }];
	},

	getItems: function () {
	    return [
	        {
	            xtype: 'panel',
	            border: 0,
	            layout: 'anchor',
	            defaultType: 'textfield',
	            anchor: '100%',
	            bodyPadding: '10 10 10 5',
	            autoScroll: true,
	            defaults: {
	                anchor: '100%',
	                labelWidth: 110
	            },
	            items: [
	                {
	                    xtype: 'fieldset',
	                    layout: 'anchor',
	                    defaults: {
	                        xtype: 'textfield',
	                        anchor: '100%'
	                    },
	                    border: 0,
	                    items: this.getFirstFields()
	                }, {
	                    xtype: 'fieldset',
	                    collapsed: true,
	                    collapsible: true,
	                    title: 'Поставщик',
	                    defaults: {
	                        xtype: 'textfield',
	                        anchor: '100%'
	                    },
	                    items: this.getSupplierFields()
	                }, {
	                    xtype: 'fieldset',
	                    collapsed: true,
	                    collapsible: true,
	                    title: 'Товар',
	                    defaults: {
	                        xtype: 'textfield',
	                        anchor: '100%'
	                    },
	                    items: this.getProductFields()
	                }, {
	                    xtype: 'fieldset',
	                    collapsed: true,
	                    collapsible: true,
	                    title: 'Транспорт',
	                    defaults: {
	                        xtype: 'textfield',
	                        anchor: '100%'
	                    },
	                    items: this.getTransportFields()
	                }, {
	                    xtype: 'fieldset',
	                    collapsed: true,
	                    collapsible: true,
	                    title: 'Плательщик',
	                    defaults: {
	                        xtype: 'textfield',
	                        anchor: '100%'
	                    },
	                    items: this.getPayerFields()
	                }, {
	                    xtype: 'fieldset',
	                    collapsed: true,
	                    collapsible: true,
	                    title: 'Грузополучатель',
	                    defaults: {
	                        xtype: 'textfield',
	                        anchor: '100%'
	                    },
	                    items: this.getGetterFields()
	                }
	            ]
	        }
	    ];
	},

	initComponent: function () {
        Ext.apply(this, {
            items: [
                {
                    xtype: 'panel',
                    border: 0,
                    layout: 'anchor',
                    defaultType: 'textfield',
                    anchor: '100%',
                    bodyPadding: '10 10 10 5',
                    autoScroll: true,
                    defaults: {
                        anchor: '100%',
                        labelWidth: 110
                    },
                    items: this.getItems()
                }
            ]
        });
        this.callParent(arguments);
    }
});
