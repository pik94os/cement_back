Ext.define('Cement.view.goods.auto.lists.Incoming', {
    extend: 'Cement.view.goods.auto.lists.New',
    alias: 'widget.goods_auto_lists_incoming',
    autoLoadStore: true,

    gridStore: 'goods.auto.Incoming',
    gridStateId: 'stateGoodsAutoIncoming',

    printUrl: Cement.Config.url.goods.auto.incoming.printGrid,
    helpUrl: Cement.Config.url.goods.auto.incoming.help,
    deleteUrl: Cement.Config.url.goods.auto.incoming.deleteUrl,
    archiveItemUrl: Cement.Config.url.goods.auto.incoming.archiveItem,

    mixins: [
        'Cement.view.goods.Rerequest'
    ],

    shownTitle: null,

    // creatorTree: Ext.clone(Cement.Creators.contracts),
    // createWindowTitle: 'Создать договор',

    getActionColumns: function () {
        return {
            xtype: 'rowactions',
            hideable: false,
            resizeable: false,
            locked: true,
            width: 46,
            actions: [
                {
                    iconCls: 'icon-archive-item',
                    qtip: 'В архив',
                    callback: this.archiveItem
                }
            ],
            keepSelection: true
        };
    },

    getGridColumns: function () {
        var me = this;
        var result = [
            { text: '№ П/п', dataIndex: 'p_number', width: 60, locked: true },
            { text: 'Дата', dataIndex: 'p_date', width: 60, locked: true },
            {
                text: 'Загрузка план',
                columns: [
                    { text: 'Дата', dataIndex: 'p_shipment_date', width: 50 },
                    { text: 'Время', dataIndex: 'p_shipment_time', width: 50 }
                ]
            },
            {
                text: 'Загрузка факт',
                columns: [
                    { text: 'Дата', dataIndex: 'p_shipment_date_fact', width: 50 },
                    { text: 'Время', dataIndex: 'p_shipment_time_fact', width: 50 }
                ]
            },
            {
                text: 'Выгрузка план',
                columns: [
                    { text: 'Дата', dataIndex: 'p_unload_date', width: 50 },
                    { text: 'Время', dataIndex: 'p_unload_time', width: 50 }
                ]
            },
            {
                text: 'Выгрузка факт',
                columns: [
                    { text: 'Дата', dataIndex: 'p_unload_date_fact', width: 50 },
                    { text: 'Время', dataIndex: 'p_unload_time_fact', width: 50 }
                ]
            },
            { text: 'С', dataIndex: 'p_s', width: 25, resizable: false, align: 'center', renderer: this.radioButtonRenderer1, tdCls: 'pseudoinput-column' },
            { text: 'Ц', dataIndex: 'p_c', width: 25, resizable: false, align: 'center', renderer: this.radioButtonRenderer1, tdCls: 'pseudoinput-column' },
            {
                text: 'Поставщик',
                columns: [
                    { text: 'Наименование', dataIndex: 'p_supplier_display', width: 150 },
                    { text: 'Адрес', dataIndex: 'p_supplier_address', width: 150 },
                    { text: 'Контактное лицо', dataIndex: 'p_supplier_contact_display', width: 150 }
                ]
            },
            {
                text: 'Грузополучатель',
                columns: [
                    { text: 'Наименование', dataIndex: 'p_getter_display', width: 150 },
                    { text: 'Склад', dataIndex: 'p_warehouse_display', width: 150 },
                    { text: 'Адрес', dataIndex: 'p_getter_address', width: 150 },
                    { text: 'Рабочее время', dataIndex: 'p_getter_work_time', width: 150 },
                    { text: 'Контактное лицо', dataIndex: 'p_getter_contact_display', width: 150 }
                ]
            },
            {
                text: 'Товар',
                columns: [
                    { text: 'Договор', dataIndex: 'p_contract_display', width: 150 },
                    { text: 'Наименование', dataIndex: 'p_product_display', width: 150 },
                    { text: 'Ед. изм.', dataIndex: 'p_product_unit_display', width: 50 },
                    { text: 'Кол.план', dataIndex: 'p_product_count', width: 50 },
                    { text: 'Налог', dataIndex: 'p_product_tax', width: 70 },
                    { text: 'Цена', dataIndex: 'p_product_price', width: 70 },
                    { text: 'Сумма', dataIndex: 'p_product_sum', width: 70 }
                ]
            },
            {
                text: 'Транспортная единица',
                columns: [
                    { text: 'Наименование ТС', dataIndex: 'p_unit_name', width: 150 },
                    { text: 'Гос.номер', dataIndex: 'p_unit_number', width: 75 },
                    { text: 'Наименование ПТС', dataIndex: 'p_pts_name', width: 150 },
                    { text: 'Гос.номер', dataIndex: 'p_pts_number', width: 75 },
                    { text: 'Водитель (Ф.И.О.)', dataIndex: 'p_driver', width: 200 }
                ]
            },
            {
                text: 'Транспортные характеристики',
                columns: [
                    { text: 'ДхШхВ', dataIndex: 'p_char_dimensions', width: 100 },
                    { text: 'О', dataIndex: 'p_char_ops', width: 20, resizable: false, align: 'center', renderer: this.radioButtonRenderer1, tdCls: 'pseudoinput-column' },
                    { text: 'П', dataIndex: 'p_char_ops', width: 20, resizable: false, align: 'center', renderer: this.radioButtonRenderer2, tdCls: 'pseudoinput-column' },
                    { text: 'С', dataIndex: 'p_char_ops', width: 20, resizable: false, align: 'center', renderer: this.radioButtonRenderer3, tdCls: 'pseudoinput-column' },
                    { text: 'Тип кузова', dataIndex: 'p_char_type_display', width: 100 },
                    { text: 'З', dataIndex: 'p_char_load_back', width: 20, resizable: false, renderer: this.checkBoxRenderer, align: 'center', tdCls: 'pseudoinput-column' },
                    { text: 'Б', dataIndex: 'p_char_load_left', width: 20, resizable: false, renderer: this.checkBoxRenderer, align: 'center', tdCls: 'pseudoinput-column' },
                    { text: 'В', dataIndex: 'p_char_load_top', width: 20, resizable: false, renderer: this.checkBoxRenderer, align: 'center', tdCls: 'pseudoinput-column' },
                    { text: 'Доп. опции', dataIndex: 'p_char_additional_display', width: 100 },
                    { text: 'Доп. хар-ки', dataIndex: 'p_add_additional', width: 100 }
                ]
            },
            { text: 'Примечание', dataIndex: 'p_comment', width: 150 },
            { text: 'В', dataIndex: 'p_importance_display', width: 27, renderer: me.coloredColumnRenderer },
            { text: 'С', dataIndex: 'p_security_display', width: 27, renderer: me.coloredColumnRenderer },
            { text: 'Статус', dataIndex: 'p_status_display', width: 150 }
        ];
        result = this.mergeActions(result);

        Ext.each(result, function (column) {
            column.height = 2 * me.fixedColumnHeaderHeight;
        });

        return result;
    },

    initComponent: function () {
        Ext.apply(this, {
            title: this.shownTitle
        });
        this.callParent(arguments);
    }
});