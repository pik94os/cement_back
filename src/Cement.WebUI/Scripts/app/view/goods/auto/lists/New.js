Ext.define('Cement.view.goods.auto.lists.New', {
	extend: 'Cement.view.basic.Grid',
	alias: 'widget.goods_auto_lists_new',
    autoLoadStore: true,

    bbarText: 'Показаны заявки {0} - {1} из {2}',
    bbarEmptyMsg: 'Нет заявок',
    bbarUsersText: 'Заявок на странице: ',

	gridStore: 'goods.auto.New',
	gridStateId: 'stateGoodsAutoNew',

	printUrl: Cement.Config.url.goods.auto.new_r.printGrid,
	helpUrl: Cement.Config.url.goods.auto.new_r.help,
	deleteUrl: Cement.Config.url.goods.auto.new_r.deleteUrl,
	setChainEndUrl: Cement.Config.url.goods.auto.new_r.setChainEndUrl,
	overrideColumnHeaderHeight: false,

    shownTitle: null,

    getActionColumns: function () {
        return {
            xtype: 'rowactions',
            hideable: false,
            resizeable: false,
            locked: true,
            width: 46,
            actions: [
                {
                    iconCls: 'icon-edit-item',
                    qtip: 'Редактировать',
                    callback: this.editItem
                },
                {
                    iconCls: 'icon-copy-item',
                    qtip: 'Создать подобную заявку',
                    hideIndex: 'p_is_mediation',
                    callback: this.createLikeThat
                },
                {
                    iconCls: 'icon-delete-item',
                    qtip: 'Удалить',
                    callback: this.deleteItem
                },
                {
                    iconCls: 'icon-page-star',
                    qtip: 'Нет поставщика',
                    hideIndex: 'p_is_initial',
                    callback: this.completeRequest
                }
            ],
            keepSelection: true
        };
    },

    getFilterItems: function () {
        return [{
            text: 'Выделить все',
            checked: true,
            checkHandler: this.toggleFilterSelection
        }, '-', {
            text: 'Наименование',
            kind: 'selector',
            field_name: 'p_name',
            checked: true
        }, {
            text: 'Поставщик',
            kind: 'selector',
            field_name: 'p_supplier_display',
            checked: true
        }, {
            text: 'Плательщик наименование',
            kind: 'selector',
            field_name: 'p_payer_display',
            checked: true
        }, {
            text: 'Плательщик адрес',
            kind: 'selector',
            field_name: 'p_payer_address',
            checked: true
        }, {
            text: 'Плательщик рабочее время',
            kind: 'selector',
            field_name: 'p_payer_work_time',
            checked: true
        }, {
            text: 'Грузополучатель наименование',
            kind: 'selector',
            field_name: 'p_getter_display',
            checked: true
        }, {
            text: 'Грузополучатель адрес',
            kind: 'selector',
            field_name: 'p_getter_address',
            checked: true
        }, {
            text: 'Грузополучатель рабочее время',
            kind: 'selector',
            field_name: 'p_getter_work_time',
            checked: true
        }, {
            text: 'Транспорт тип кузова',
            kind: 'selector',
            field_name: 'p_transport_char_type_display',
            checked: true
        }, {
            text: 'Транспорт доп. опции',
            kind: 'selector',
            field_name: 'p_transport_additional_options_display',
            checked: true
        }];
    },

    coloredColumnRenderer: function (value) {
        return "<div class='colored-icon-combo colored-" + value + "'";
    },

    getGridColumns: function () {
        var me = this;
        var result = [
            { text: '№ П/п', dataIndex: 'p_number', width: 60, locked: true },
            { text: 'Дата', dataIndex: 'p_date', width: 60, locked: true},
            {
                text: 'Загрузка план',
                columns: [
                    { text: 'Дата', dataIndex: 'p_shipment_date', width: 50 },
                    { text: 'Время', dataIndex: 'p_shipment_time', width: 50 }
                ]
            },
            { text: 'С', dataIndex: 'p_s', width: 25, resizable: false, align: 'center', renderer: this.radioButtonRenderer1, tdCls: 'pseudoinput-column' },
            { text: 'Ц', dataIndex: 'p_c', width: 25, resizable: false, align: 'center', renderer: this.radioButtonRenderer1, tdCls: 'pseudoinput-column' },
            {
                text: 'Поставщик',
                columns: [
                    { text: 'Наименование', dataIndex: 'p_supplier_display', width: 150 },
                    { text: 'Адрес', dataIndex: 'p_supplier_address', width: 150 },
                    { text: 'Контактное лицо', dataIndex: 'p_supplier_contact_display', width: 150 }
                ]
            },
            {
                text: 'Плательщик',
                columns: [
                    { text: 'Наименование', dataIndex: 'p_payer_display', width: 150 },
                    { text: 'Адрес', dataIndex: 'p_payer_address', width: 150 },
                    { text: 'Контактное лицо', dataIndex: 'p_payer_contact_display', width: 150 }
                ]
            },
            {
                text: 'Грузополучатель',
                columns: [
                    { text: 'Наименование', dataIndex: 'p_getter_display', width: 150 },
                    { text: 'Склад', dataIndex: 'p_warehouse_display', width: 150 },
                    { text: 'Адрес', dataIndex: 'p_getter_address', width: 150 },
                    { text: 'Рабочее время', dataIndex: 'p_getter_work_time', width: 150 },
                    { text: 'Контактное лицо', dataIndex: 'p_getter_contact_display', width: 150 }
                ]
            },
            {
                text: 'Товар',
                columns: [
                    { text: 'Договор', dataIndex: 'p_contract_display', width: 150 },
                    { text: 'Наименование', dataIndex: 'p_product_display', width: 150 },
                    { text: 'Ед. изм.', dataIndex: 'p_product_unit_display', width: 50 },
                    { text: 'Колюплан', dataIndex: 'p_product_count', width: 50 },
                    { text: 'Налог', dataIndex: 'p_product_tax', width: 70 },
                    { text: 'Цена', dataIndex: 'p_product_price', width: 70 },
                    { text: 'Сумма', dataIndex: 'p_product_sum', width: 70 }
                ]
            },
            {
                text: 'Транспортные характеристики',
                columns: [
                    { text: 'ДхШхВ', dataIndex: 'p_char_dimensions', width: 100 },
                    { text: 'О', dataIndex: 'p_char_ops', width: 20, resizable: false, align: 'center', renderer: this.radioButtonRenderer1, tdCls: 'pseudoinput-column' },
                    { text: 'П', dataIndex: 'p_char_ops', width: 20, resizable: false, align: 'center', renderer: this.radioButtonRenderer2, tdCls: 'pseudoinput-column' },
                    { text: 'С', dataIndex: 'p_char_ops', width: 20, resizable: false, align: 'center', renderer: this.radioButtonRenderer3, tdCls: 'pseudoinput-column' },
                    { text: 'Тип кузова', dataIndex: 'p_char_type_display', width: 100 },
                    { text: 'З', dataIndex: 'p_char_load_back', width: 20, resizable: false, renderer: this.checkBoxRenderer, align: 'center', tdCls: 'pseudoinput-column' },
                    { text: 'Б', dataIndex: 'p_char_load_left', width: 20, resizable: false, renderer: this.checkBoxRenderer, align: 'center', tdCls: 'pseudoinput-column' },
                    { text: 'В', dataIndex: 'p_char_load_top', width: 20, resizable: false, renderer: this.checkBoxRenderer, align: 'center', tdCls: 'pseudoinput-column' },
                    { text: 'Доп. опции', dataIndex: 'p_char_additional_display', width: 100 },
                    { text: 'Доп. хар-ки', dataIndex: 'p_add_additional', width: 100 }
                ]
            },
            { text: 'Примечание', dataIndex: 'p_comment', width: 150 },
            { text: 'В', dataIndex: 'p_importance_display', width: 27, renderer: me.coloredColumnRenderer },
            { text: 'С', dataIndex: 'p_security_display', width: 27, renderer: me.coloredColumnRenderer },
            { text: 'Статус', dataIndex: 'p_status_display', width: 150 }
        ];

        result = this.mergeActions(result);

        Ext.each(result, function (column) {
            column.height = 2 * me.fixedColumnHeaderHeight;
        });

        return result;
    },

    initComponent: function () {
        Ext.apply(this, {
            title: this.shownTitle
        });
        this.callParent(arguments);
    },

    createLikeThat: function (grid, record) {
        var bacisGrid = grid.up('basicgrid');

        var result = Ext.clone(record);
        result.set('id', 0);

        bacisGrid.fireEvent('edititem', result, bacisGrid.$className);
    },

    completeRequest: function (grid, record) {
        var bacisGrid = grid.up('basicgrid');

        if (record.get('p_is_initial') === true) {
            return;
        }

        var win = Ext.create('Ext.window.Window', {
            title: 'Внимание',
            width: 400,
            height: 160,
            layout: 'vbox',
            bodyPadding: 10,
            modal: true,
            closable: false,
            bodyStyle: "background: #f7f7f7",
            items: [
            {
                xtype: 'label',
                text: 'Вы действительно хотите завершить цепочку заявок?',
                height: 25,
                labelAlign: 'top',
                anchor: '100%'
            },
            {
                xtype: 'form',
                layout: 'vbox',
                border: false,
                items: [
                    {
                        xtype: 'datefield',
                        fieldLabel: 'Дата загрузки',
                        name: 'shipment-date',
                        minValue: new Date(),
                        allowBlank: false
                    },
                    {
                        xtype: 'timefield',
                        fieldLabel: 'Время загрузки',
                        name: 'shipment-time',
                        format: 'H:i',
                        allowBlank: false
                    }
                ]
            }
            ],
            dockedItems: [{
                xtype: 'toolbar',
                dock: 'bottom',
                ui: 'footer',
                items: [
                    {
                        xtype: 'tbspacer',
                        flex: 1
                    },
                    {
                        xtype: 'button',
                        text: 'Да',
                        name: 'ok_btn',
                        width: 80
                    },
                    {
                        xtype: 'button',
                        text: 'Нет',
                        name: 'cancel_btn',
                        width: 80
                    },
                    {
                        xtype: 'tbspacer',
                        flex: 1
                    }
                ]
            }]
        });

        win.down('datefield').on('change', function (cmp, val) {
            var now = new Date().setHours(0, 0, 0, 0);
            var timeField = win.down('timefield');
            if ((val - now) == 0) {
                timeField.setMinValue(new Date());
            } else {
                timeField.setMinValue(now);
            }
        }, this);

        win.down('button[name=ok_btn]').on('click', function () {
            if (win.down('form').isValid()) {
                var data = {
                    id: record.get('id'),
                    shipmentDate: win.down('datefield').getValue(),
                    shipmentTime: win.down('timefield').getValue()
                };

                win.setLoading("Сохранение");

                Ext.Ajax.request({
                    method: 'POST',
                    url: bacisGrid.setChainEndUrl,
                    params: data,
                    success: function (response) {
                        win.close();
                        delete win;
                        bacisGrid.getStore().reload();
                    },
                    failure: function () {
                        Cement.Msg.error('Ошибка');
                    }
                });
            }
        });
        win.down('button[name=cancel_btn]').on('click', function () {
            win.close();
            delete win;
        });

        win.show();
    }
});