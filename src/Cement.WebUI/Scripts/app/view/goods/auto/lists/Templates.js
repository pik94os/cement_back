Ext.define('Cement.view.goods.auto.lists.Templates', {
	extend: 'Cement.view.basic.Grid',
	alias: 'widget.goods_auto_lists_templates',
    autoLoadStore: true,

    bbarText: 'Показаны шаблоны {0} - {1} из {2}',
    bbarEmptyMsg: 'Нет шаблонов',
    bbarUsersText: 'Шаблонов на странице: ',

	gridStore: 'goods.auto.Templates',
	gridStateId: 'stateGoodsAutoTemplates',

	printUrl: Cement.Config.url.goods.auto.templates.printGrid,
	helpUrl: Cement.Config.url.goods.auto.templates.help,
    deleteUrl: Cement.Config.url.goods.auto.templates.deleteUrl,

    shownTitle: null,

    getFilterItems: function () {
        return [{
            text: 'Выделить все',
            checked: true,
            checkHandler: this.toggleFilterSelection
        }, '-', {
            text: 'Наименование',
            kind: 'selector',
            field_name: 'p_name',
            checked: true
        }, {
            text: 'Поставщик',
            kind: 'selector',
            field_name: 'p_supplier_name',
            checked: true
        }, {
            text: 'Плательщик наименование',
            kind: 'selector',
            field_name: 'p_payer_name',
            checked: true
        }, {
            text: 'Плательщик адрес',
            kind: 'selector',
            field_name: 'p_payer_address',
            checked: true
        }, {
            text: 'Плательщик рабочее время',
            kind: 'selector',
            field_name: 'p_payer_work_time',
            checked: true
        }, {
            text: 'Грузополучатель наименование',
            kind: 'selector',
            field_name: 'p_getter_name',
            checked: true
        }, {
            text: 'Грузополучатель адрес',
            kind: 'selector',
            field_name: 'p_getter_address',
            checked: true
        }, {
            text: 'Грузополучатель рабочее время',
            kind: 'selector',
            field_name: 'p_getter_work_time',
            checked: true
        }, {
            text: 'Транспорт тип кузова',
            kind: 'selector',
            field_name: 'p_transport_char_type_display',
            checked: true
        }, {
            text: 'Транспорт доп. опции',
            kind: 'selector',
            field_name: 'p_transport_additional_options_display',
            checked: true
        }];
    },

    getGridColumns: function () {
        var result = [
            { text: 'Наименование', dataIndex: 'p_name', width: 90, locked: true },
            { text: 'С', dataIndex: 'p_s', width: 25, locked: true, resizable: false, align: 'center', renderer: this.radioButtonRenderer1, tdCls: 'pseudoinput-column' },
            { text: 'Сд', dataIndex: 'p_sd', width: 25, locked: true, resizable: false, align: 'center', renderer: this.radioButtonRenderer1, tdCls: 'pseudoinput-column' },
            { text: 'Ц', dataIndex: 'p_c', width: 25, locked: true, resizable: false, align: 'center', renderer: this.radioButtonRenderer1, tdCls: 'pseudoinput-column' },
            { text: 'Поставщик', dataIndex: 'p_supplier_name', width: 150, locked: true },
            {
                text: 'Плательщик',
                columns: [
                    { text: 'Наименование', dataIndex: 'p_payer_name', width: 150 },
                    { text: 'Адрес', dataIndex: 'p_payer_address', width: 150 },
                    { text: 'Рабочее время', dataIndex: 'p_payer_work_time', width: 150 }
                ]
            },
            {
                text: 'Грузополучатель',
                columns: [
                    { text: 'Наименование', dataIndex: 'p_getter_name', width: 150 },
                    { text: 'Адрес', dataIndex: 'p_getter_address', width: 150 },
                    { text: 'Рабочее время', dataIndex: 'p_getter_work_time', width: 150 }
                ]
            },
            {
                text: 'Товар',
                columns: [
                    { text: 'Наименование', dataIndex: 'p_product_display', width: 150 },
                    { text: 'Ед. изм.', dataIndex: 'p_product_unit_display', width: 50 },
                    { text: 'Кол-во', dataIndex: 'p_product_quantity', width: 50 },
                    { text: 'Факт кол-во', dataIndex: 'p_product_fact_quantity', width: 50 },
                    { text: 'Цена', dataIndex: 'p_product_price', width: 70 },
                    { text: 'Налог', dataIndex: 'p_product_tax', width: 70 },
                    { text: 'Сумма', dataIndex: 'p_product_sum', width: 70 }
                ]
            },
            {
                text: 'Транспорт',
                columns: [
                    { text: 'О', dataIndex: 'p_transport_char_ops', width: 20, resizable: false, align: 'center', renderer: this.radioButtonRenderer1, tdCls: 'pseudoinput-column' },
                    { text: 'П', dataIndex: 'p_transport_char_ops', width: 20, resizable: false, align: 'center', renderer: this.radioButtonRenderer2, tdCls: 'pseudoinput-column' },
                    { text: 'С', dataIndex: 'p_transport_char_ops', width: 20, resizable: false, align: 'center', renderer: this.radioButtonRenderer3, tdCls: 'pseudoinput-column' },
                    { text: 'Тип кузова', dataIndex: 'p_transport_char_type_display', width: 100 },
                    { text: 'З', dataIndex: 'p_transport_load_back', width: 20, resizable: false, renderer: this.checkBoxRenderer, align: 'center', tdCls: 'pseudoinput-column' },
                    { text: 'Б', dataIndex: 'p_transport_load_left', width: 20, resizable: false, renderer: this.checkBoxRenderer, align: 'center', tdCls: 'pseudoinput-column' },
                    { text: 'В', dataIndex: 'p_transport_load_top', width: 20, resizable: false, renderer: this.checkBoxRenderer, align: 'center', tdCls: 'pseudoinput-column' },
                    { text: 'Доп. опции', dataIndex: 'p_transport_additional_options_display', width: 100 }
                ]
            }
        ];
        return this.mergeActions(result);
    },

    initComponent: function () {
        Ext.apply(this, {
            title: this.shownTitle
        });
        this.callParent(arguments);
    }
});