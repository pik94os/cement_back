Ext.define('Cement.view.goods.auto.lists.ConnectedDocuments', {
	extend: 'Cement.view.basic.Grid',
	alias: 'widget.goods_auto_lists_connected_documents',
    autoLoadStore: false,

	gridStore: 'goods.auto.New',
	gridStateId: 'stateGoodsAutoConnectedDocuments',

	printUrl: Cement.Config.url.goods.auto.new_r.printGrid,
	helpUrl: Cement.Config.url.goods.auto.new_r.help,
    deleteUrl: Cement.Config.url.goods.auto.new_r.deleteUrl,

    showBottomBar: false,
    showCreateButton: false,

    // creatorTree: Ext.clone(Cement.Creators.contracts),
    // createWindowTitle: 'Создать договор',

    getActionColumns: function () {
        return null;
    },

    getToolbarItems: function () {
        return null;
    },

    getGridColumns: function () {
        return [
            { text: '№ П/п', dataIndex: 'p_number', width: 60, locked: true },
            { text: 'Дата', dataIndex: 'p_date', width: 60, locked: true },
            { text: 'Вид', dataIndex: 'p_kind', width: 80, locked: true },
            { text: 'Статус', dataIndex: 'p_status', width: 250 },
            { text: 'Наименование', dataIndex: 'p_name', width: 550 }
        ];
    }
});