Ext.define('Cement.view.goods.auto.form.Form', {
    extend: 'Cement.view.basic.Form',
    alias: 'widget.goods_auto_form_form',
    fieldsDisabled: false,
    url: Cement.Config.url.goods.auto.new_r.saveUrl,
    saveUrl: Cement.Config.url.goods.auto.new_r.saveUrl,
    saveIntermediaryUrl: Cement.Config.url.goods.auto.new_r.saveIntermediaryUrl,
    method: Cement.Config.url.goods.auto.new_r.saveMethod,
    // method: "GET",
    printUrl: Cement.Config.url.goods.auto.new_r.printItem,
    isFilter: false,
    layout: 'fit',
    allowBlankFields: true,
    showSaveAndSignButton: true,
    saveButtonDisabled: false,
    loadItemGetterUrl: Cement.Config.url.goods.auto.new_r.loadItemUrl,

    importancesStoreId: 'goods.auxiliary.Importances',
    importancesColoredStoreId: 'goods.auxiliary.ImportancesColored',
    securitiesStoreId: 'goods.auxiliary.Securities',
    securitiesColoredStoreId: 'goods.auxiliary.SecuritiesColored',
	car_options_store_Id: 'transport.auxiliary.CarOptions',
	car_types_store_Id: 'transport.auxiliary.CharTypes',
    
	getItems: function () {
	    return [
            {
	            xtype: 'hiddenfield',
	            name: 'id'
	        },
            {
                xtype: 'fieldset',
				border: 0,
				margin: 0,
				padding: '0 110',
				defaults: {
					xtype: 'textfield',
					anchor: '100%',
					labelAlign: 'right'
				},
				items: [{
					name: 'p_number',
					fieldLabel: 'Номер'
				}, {
					name: 'p_date',
					fieldLabel: 'Дата',
					xtype: 'datefield',
					value: new Date(),
					minValue: new Date()
				},
				{
				    name: 'p_unload_date',
				    xtype: 'datefield',
				    fieldLabel: 'Дата выгрузки',
				    minValue: new Date()
				},
                {
				    name: 'p_unload_time',
				    xtype: 'timefield',
				    format: 'H:i',
				    fieldLabel: 'Время выгрузки'
                },
				this.getCombo('Способ отгрузки', 'p_load_kind', 'goods.auto.auxiliary.LoadKinds')]
		    },
            {
                xtype: 'fieldset',
                title: 'Поставщик',
                collapsed: false,
                collapsible: true,
                role: 'supplier',
                defaults: {
                    xtype: 'textfield',
                    anchor: '100%',
                    labelWidth: 200,
                    labelAlign: 'right'
                },
                items: [
            	this.getSelectorField('Наименование', 'p_supplier', 'show_supplier_window'), {
            		name: 'p_supplier_address',
            		fieldLabel: 'Адрес',
            		disabled: true
            	},
            	this.getSelectorField('Контактное лицо', 'p_supplier_contact', 'show_supplier_contact_window')
                ]
            }, {
            	xtype: 'fieldset',
            	title: 'Товар',
            	collapsed: false,
            	collapsible: true,
            	role: 'product',
            	defaults: {
                    xtype: 'textfield',
                    anchor: '100%',
                    labelWidth: 200,
                    labelAlign: 'right'
                },
            	items: [
                    //{
                	//    xtype: 'hiddenfield',
                	//    name: 'p_contract'
                    //},
                    //{
                	//    name: 'p_contract_display',
                	//    fieldLabel: 'Договор',
                	//    disabled: true
                    //},
                    this.getSelectorField('Договор', 'p_contract', 'show_contract_window'),
                    //this.getSelectorField('Наименование', 'p_product', 'show_product_window'),
                    {
                        xtype: 'hiddenfield',
                	    name: 'p_product'
                    },
                    {
                        name: 'p_product_display',
                        fieldLabel: 'Наименование',
                        disabled: true
                    },
                    {
                	    name: 'p_product_unit_display',
                	    fieldLabel: 'Единица измерения',
                	    disabled: true
                    },
                    {
                        xtype: 'hiddenfield',
                        name: 'p_product_unit'
                    },
                    {
                        name: 'p_product_price_display',
                        fieldLabel: 'Цена',
                        disabled: true
                    },
                    {
                        xtype: 'hiddenfield',
                        name: 'p_product_price'
                    },
                    {
                        xtype: 'numberfield',
                        name: 'p_product_count',
                        allowDecimals: false,
                        hideTrigger: true,
                	    fieldLabel: 'Количество',
                	    disabled: true
                    },
                    {
                        xtype: 'hiddenfield',
                        name: 'p_product_tax'
                    },
                    {
                        xtype: 'hiddenfield',
                        name: 'p_product_fact_count'
                    }
                ]
            }, {
            	xtype: 'fieldset',
            	title: 'Транспортная единица',
            	collapsed: false,
            	collapsible: true,
            	role: 'transport_unit',
            	defaults: {
                    xtype: 'textfield',
                    anchor: '100%',
                    labelWidth: 200,
                    labelAlign: 'right'
                },
            	items: [
                this.getCombo('Тип транспортной единицы', 'p_char_ops', 'goods.auto.auxiliary.TransportUnitType')
                , this.getCombo('Тип кузова', 'p_char_type', this.car_types_store_Id), {
                	xtype: 'checkboxgroup',
			        fieldLabel: 'Способ выгрузки',
			        columns: 3,
			        items: [{
			        	xtype: 'checkbox',
			        	boxLabel: 'Задний',
			        	inputValue: 1,
			        	name: 'p_char_load_back'
			        }, {
			        	xtype: 'checkbox',
			        	boxLabel: 'Боковой',
			        	inputValue: 1,
			        	name: 'p_char_load_left'
			        }, {
			        	xtype: 'checkbox',
			        	boxLabel: 'Верхний',
			        	inputValue: 1,
			        	name: 'p_char_load_top'
			        }]
                }, this.getCombo('Дополнительные опции', 'p_char_additional', this.car_options_store_Id), {
                	name: 'p_add_additional',
                	fieldLabel: 'Дополнительные характеристики'
                }]
            }, {
                xtype: 'fieldset',
                title: 'Плательщик',
                collapsed: false,
                collapsible: true,
                disabled: true,
                role: 'payer',
                defaults: {
                    xtype: 'textfield',
                    anchor: '100%',
                    labelWidth: 200,
                    labelAlign: 'right'
                },
                items: [
                {
                    xtype: 'hiddenfield',
                    name: 'p_payer'
                },
                {
                    fieldLabel: 'Наименование',
                    name: 'p_payer_display'
                },
                {
                    xtype: 'hiddenfield',
                    name: 'p_payer_contact'
                },
                {
                    fieldLabel: 'Контактное лицо',
                    name: 'p_payer_contact_display'
                },
                {
                    fieldLabel: 'Рабочее время',
                    name: 'p_payer_work_time'
                }]
            }, {
                xtype: 'fieldset',
                title: 'Грузополучатель',
                collapsed: false,
                collapsible: true,
                role: 'getter',
                defaults: {
                    xtype: 'textfield',
                    anchor: '100%',
                    labelWidth: 200,
                    labelAlign: 'right'
                },
                items: [
                {
                    xtype: 'hiddenfield',
                    name: 'p_getter'
                },
                {
                    fieldLabel: 'Наименование',
                    name: 'p_getter_display',
                    disabled: true
                },
                {
                    xtype: 'hiddenfield',
                    name: 'p_getter_address'
                },
            	//this.getSelectorField('Наименование', 'p_getter', 'show_getter_window'),
                //{
                //    xtype: 'hiddenfield',
                //    name: 'p_getter_address',
                //    disabled: true
                //},
                this.getSelectorField('Склад', 'p_warehouse', 'show_warehouse_window'),
            	this.getSelectorField('Контактное лицо', 'p_getter_contact', 'show_getter_contact_window'), {
            		name: 'p_getter_work_time',
            		fieldLabel: 'Рабочее время',
            		disabled: true
                }]
            },
            {
                xtype: 'fieldset',
                title: 'Дополнительные данные',
                collapsed: false,
                collapsible: true,
                defaults: {
                    xtype: 'textfield',
                    anchor: '100%',
                    labelWidth: 200,
                    labelAlign: 'right'
                },
                items: [
                    this.getColoredCombo('Важность', 'p_importance', this.importancesColoredStoreId),
                    this.getColoredCombo('Секретность', 'p_security', this.securitiesColoredStoreId),
                    {
	            	    fieldLabel: 'Примечание',
	            	    name: 'p_comment',
	            	    xtype: 'textarea',
	            	    height: 150
	                }]
            }
        ];
	},

	enableFieldsets: function () {
		this.down('fieldset[role=supplier]').enable();
		this.down('fieldset[role=getter]').enable();
	},

	initComponent: function () {
		this.callParent(arguments);
		this.down('button[action=show_supplier_window]').on('click', function () {
		    this.createSupplierWindow();
		    var requestId = this.down('hiddenfield[name=id]').getValue();
		    this.supplierWindow.setRequestId(requestId);
			this.supplierWindow.show();
		}, this);
		this.down('button[action=show_contract_window]').on('click', function () {
		    this.createContractWindow();
		    this.contractWindow.show();
		}, this);
		this.down('button[action=show_supplier_contact_window]').on('click', function () {
		    this.createSupplierContactWindow();
			this.supplierContactWindow.show();
		}, this);
		//this.down('button[action=show_getter_window]').on('click', function () {
		//	this.createGetterWindow();
		//	this.getterWindow.show();
		//}, this);
		this.down('button[action=show_warehouse_window]').on('click', function () {
		    this.createWarehouseWindow();
		    this.warehouseWindow.show();
		}, this);
		this.down('button[action=show_getter_contact_window]').on('click', function () {
			this.createGetterContactWindow();
			this.getterContactWindow.show();
		}, this);
		this.down('fieldcontainer[role=container_p_supplier_contact]').disable();
		this.down('datefield[name=p_unload_date]').on('change', function (cmp, val) {
		    var now = new Date().setHours(0, 0, 0, 0);
		    var timeField = this.down('timefield[name=p_unload_time]');
		    if ((val - now) == 0) {
		        timeField.setMinValue(new Date());
		    } else {
		        timeField.setMinValue(now);
		    }
		}, this);
	},

	createSupplierContactWindow: function () {
		if (!this.supplierContactWindow) {
			this.supplierContactWindow = Ext.create('Cement.view.goods.auto.form.SupplierContactWindow');
			this.supplierContactWindow.on('selected', function (rec) {
				this.down('textfield[name=p_supplier_contact_display]').setValue(rec.get('p_name'));
				this.down('hiddenfield[name=p_supplier_contact]').setValue(rec.get('id'));
				this.supplierContactWindow.hide();
			}, this);
		}
	},

	createGetterContactWindow: function () {
		if (!this.getterContactWindow) {
			this.getterContactWindow = Ext.create('Cement.view.goods.auto.form.GetterContactWindow');
			this.getterContactWindow.on('selected', function (rec) {
				this.down('textfield[name=p_getter_contact_display]').setValue(rec.get('p_name'));
				this.down('hiddenfield[name=p_getter_contact]').setValue(rec.get('id'));
				this.getterContactWindow.hide();
			}, this);
		}
	},

	createSupplierWindow: function () {
		if (!this.supplierWindow) {
			this.supplierWindow = Ext.create('Cement.view.goods.auto.form.SupplierWindow');
			this.supplierWindow.on('selected', function (rec) {
			    this.down('hiddenfield[name=p_supplier]').setValue(rec.get('id'));
				this.down('textfield[name=p_supplier_display]').setValue(rec.get('p_name'));
				this.down('textfield[name=p_supplier_address]').setValue(rec.get('p_address'));
				this.down('fieldcontainer[role=container_p_supplier_contact]').enable();
				this.down('hiddenfield[name=p_supplier_contact]').setValue('');
				this.down('textfield[name=p_supplier_contact_display]').setValue('');
				this.createSupplierContactWindow();
			    this.createContractWindow();
			    this.supplierContactWindow.setFirm(rec.get('id'));
			    var requestId = this.down('hiddenfield[name=id]').getValue();
			    this.contractWindow.setFirm(rec.get('id'));
			    this.contractWindow.setRequestId(requestId);
				this.supplierWindow.hide();
			}, this);
		}
	},

	createContractWindow: function () {
	    if (!this.createWindow) {
	        this.contractWindow = Ext.create('Cement.view.goods.auto.form.ContractWindow');
	        this.contractWindow.on('productselected', function (product, contract) {
	            var me = this;
	            me.down('hiddenfield[name=p_contract]').setValue(contract.get('id'));
	            me.down('textfield[name=p_contract_display]').setValue(contract.get('p_name'));
	            me.down('hiddenfield[name=p_product]').setValue(product.get('id'));
	            me.down('textfield[name=p_product_display]').setValue(product.get('p_name'));
	            me.down('textfield[name=p_product_unit_display]').setValue(product.get('p_product_unit_display'));
	            me.down('hiddenfield[name=p_product_unit]').setValue(product.get('p_product_unit'));
	            me.down('hiddenfield[name=p_product_price]').setValue(product.get('p_price'));
	            me.down('textfield[name=p_product_price_display]').setValue(product.get('p_price'));
	            me.down('hiddenfield[name=p_product_tax]').setValue(product.get('p_tax'));
	            me.down('textfield[name=p_product_count]').enable();
	            this.contractWindow.hide();
	        }, this);
	    }
	},

	//createPayerWindow: function () {
	//	if (!this.payerWindow) {
	//		this.payerWindow = Ext.create('Cement.view.goods.auto.form.PayerWindow');
	//		this.payerWindow.on('selected', function (rec) {
	//			this.down('textfield[name=p_payer_display]').setValue(rec.get('p_name'));
	//			this.down('textfield[name=p_payer_address]').setValue(rec.get('p_address'));
	//			this.down('textfield[name=p_payer_work_time]').setValue(rec.get('p_work_time'));
	//			this.down('hiddenfield[name=p_payer]').setValue(rec.get('id'));
	//			this.createPayerContactWindow();
	//			this.payerContactWindow.setFirm(rec.get('id'));
	//			this.payerWindow.hide();
	//			this.down('fieldcontainer[role=container_p_payer_contact]').enable();
	//		}, this);
	//	}
	//},

	createGetterWindow: function () {
		if (!this.getterWindow) {
			this.getterWindow = Ext.create('Cement.view.goods.auto.form.GetterWindow');
			this.getterWindow.on('selected', function (rec) {
				this.down('textfield[name=p_getter_display]').setValue(rec.get('p_name'));
				this.down('hiddenfield[name=p_getter_address]').setValue(rec.get('p_address'));
				this.down('hiddenfield[name=p_getter]').setValue(rec.get('id'));
				this.down('textfield[name=p_getter_work_time]').setValue(rec.get('p_work_time'));
				this.createGetterContactWindow();
			    this.createWarehouseWindow();
			    this.getterContactWindow.setFirm(rec.get('id'));
			    this.warehouseWindow.setFirm(rec.get('id'));
				this.getterWindow.hide();
				this.down('fieldcontainer[role=container_p_getter_contact]').enable();
			}, this);
		}
	},

	createWarehouseWindow: function () {
	    if (!this.warehouseWindow) {
	        this.warehouseWindow = Ext.create('Cement.view.goods.auto.form.WarehouseWindow');
	        this.warehouseWindow.on('selected', function (rec) {
	            this.down('hiddenfield[name=p_warehouse]').setValue(rec.get('id'));
	            this.down('textfield[name=p_warehouse_display]').setValue(rec.get('p_name'));
	            this.warehouseWindow.hide();
	        }, this);
	    }
	},

	afterRecordLoad: function (rec) {
	    var me = this;
		this.createSupplierWindow();
		this.createSupplierContactWindow();
		this.createGetterWindow();
		this.createGetterContactWindow();
		this.createContractWindow();
		this.contractWindow.setFirm(rec.get('p_supplier'));
		this.supplierContactWindow.setFirm(rec.get('p_supplier'));
		this.getterContactWindow.setFirm(rec.get('p_getter'));
		if (!rec.get('p_is_initial')) {
		    this.getForm().url = this.saveIntermediaryUrl;
		    this.disableFields(true);
		} else {
		    this.getForm().url = this.saveUrl;
		    this.disableFields(false);
	    }
	},

	onCreate: function () {
	    var me = this;
	    this.disableFields(false);
	    Ext.Ajax.request({
	        url: me.loadItemGetterUrl,
	        method: 'POST',
	        success: function (response, opts) {
	            var getter = Ext.decode(response.responseText);
	            me.down('hiddenfield[name=p_getter]').setValue(getter.id);
	            me.down('textfield[name=p_getter_display]').setValue(getter.p_name);
	            me.down('hiddenfield[name=p_getter_address]').setValue(getter.p_address);
	            me.down('hiddenfield[name=p_getter_contact]').setValue(getter.p_contact);
	            me.down('textfield[name=p_getter_contact_display]').setValue(getter.p_contact_display);
	            me.down('hiddenfield[name=p_payer]').setValue(getter.id);
	            me.down('textfield[name=p_payer_display]').setValue(getter.p_name);
	            me.down('hiddenfield[name=p_payer_contact]').setValue(getter.p_contact);
	            me.down('textfield[name=p_payer_contact_display]').setValue(getter.p_contact_display);
	            me.createGetterContactWindow();
	            me.createWarehouseWindow();
	            me.getterContactWindow.setFirm(getter.id);
	            me.warehouseWindow.setFirm(getter.id);
	        },
	        failure: function (response, opts) {
	            console.log('server-side failure with status code ' + response.status);
	        }
	    });
	},

	reset: function () {
	    this.callParent(arguments);

	    this.getForm().url = this.saveUrl;

		this.down('fieldset[role=supplier]').enable();
		this.down('fieldset[role=getter]').enable();
		this.down('fieldset[role=product]').enable();
	},

	beforeSubmit: function (callback) {
	    var me = this;
        
	    if (!(me.down('[name=p_supplier_contact]').getValue() > 0)) {
	        Cement.Msg.error('Не указано контактное лицо!');
	        return;
	    }
        
	    callback.call();
	},

	disableFields: function(isDisabled) {
	    if (isDisabled) {
	        this.down('fieldset[role=getter]').disable();
	        this.down('button[action=show_getter_contact_window]').hide();
	        this.down('button[action=show_warehouse_window]').hide();
	        this.down('fieldset[role=transport_unit]').disable();
	        this.disableFieldWithTrigger(this.down('datefield[name=p_date]'), isDisabled);
	        this.disableFieldWithTrigger(this.down('datefield[name=p_unload_date]'), isDisabled);
	        this.disableFieldWithTrigger(this.down('timefield[name=p_unload_time]'), isDisabled);
        } else {
	        this.down('fieldset[role=getter]').enable();
	        this.down('button[action=show_getter_contact_window]').show();
	        this.down('button[action=show_warehouse_window]').show();
	        this.down('fieldset[role=transport_unit]').enable();
	        this.disableFieldWithTrigger(this.down('datefield[name=p_date]'), isDisabled);
	        this.disableFieldWithTrigger(this.down('datefield[name=p_unload_date]'), isDisabled);
	        this.disableFieldWithTrigger(this.down('timefield[name=p_unload_time]'), isDisabled);
        }
	},

	disableFieldWithTrigger: function (element, isDisabled) {
	    element.setHideTrigger(isDisabled);
	    element.setDisabled(isDisabled);
    }
});