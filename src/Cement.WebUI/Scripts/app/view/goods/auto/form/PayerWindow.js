Ext.define('Cement.view.goods.auto.form.PayerWindow', {
	extend: 'Cement.view.goods.auto.form.SupplierWindow',
	xtype: 'goods_auto_form_payer_window',
	title: 'Выбор плательщика',
	leftFrameTitle: 'Плательщик',
	topGridTitle: 'Выбрать плательщика',
	bottomGridTitle: 'Выбранный плательщик',
	structure_storeId: 'goods.auto.auxiliary.Payers'
});