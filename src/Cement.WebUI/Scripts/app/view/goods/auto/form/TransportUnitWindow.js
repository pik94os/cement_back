Ext.define('Cement.view.goods.auto.form.TransportUnitWindow', {
	extend: 'Cement.view.goods.auto.form.RequestWindow',
	xtype: 'goods_auto_form_transport_unit_window',
	title: 'Выбор транспортной единицы',
	layout: {
		type: 'vbox',
		align : 'stretch'
	},
	width: 900,
	height: 500,
	bodyPadding: 10,
	bbar: [{
		text: 'Сохранить',
		action: 'confirm'
	}, {
		text: 'Отмена',
		action: 'cancel'
	}],
	modal: true,
	topGridTitle: 'Выбрать транспортную единицу',
	bottomGridTitle: 'Выбранная транспортная единица',
	loadRightFromTree: true,
	storeFields: ['id', 'p_name', 'p_type_display', 'p_auto_mark_model', 'p_auto_number',
		'p_auto_trailer', 'p_auto_trailer_number', 'p_driver_surname', 'p_driver_name',
		'p_driver_lastname', 'p_class_display', 'p_size_display', 'p_go_display', 'p_char_ops',
		'p_char_type_display', 'p_load_left', 'p_load_back', 'p_load_top',
		'p_additional_options_display', 'p_additional_chars_display'],
	srcStoreId: 'goods.auto.auxiliary.TransportUnits',
	noText: 'Поиск на бирже',

	mixins: [
        'Cement.view.basic.GridRenderers'
    ],

	getTopColumnsConfig: function () {
		return [
			{
				xtype: 'rowactions',
				hideable: false,
				resizeable: false,
				width: 18,
				keepSelection: true,
				locked: true,
				actions: [{ 
	                iconCls: 'icon-add-item', 
	                qtip: 'Добавить',
	                callback: this.addEmployee
	            }]
			},
            { text: 'Наименование', dataIndex: 'p_name', locked: true, width: 150 },
            { text: 'Тип', dataIndex: 'p_type_display', locked: true, width: 150 },
            {
                text: 'Транспортное средство',
                columns: [
                    { text: 'Марка, модель', dataIndex: 'p_auto_mark_model', width: 200 },
                    { text: 'Гос. номер', dataIndex: 'p_auto_number', width: 200 }
                ]
            },
            {
                text: 'Прицепное транспортное средство',
                columns: [
                    { text: 'Марка, модель', dataIndex: 'p_auto_trailer', width: 200 },
                    { text: 'Гос. номер', dataIndex: 'p_auto_trailer_number', width: 200 }
                ]
            },
            {
                text: 'Водитель',
                columns: [
                    { text: 'Фамилия', dataIndex: 'p_driver_surname', width: 150 },
                    { text: 'Имя', dataIndex: 'p_driver_name', width: 150 },
                    { text: 'Отчество', dataIndex: 'p_driver_lastname', width: 150 }
                ]
            },
            {
                text: 'Характеристики',
                columns: [
                    { text: 'Класс', dataIndex: 'p_class_display', width: 100 },
                    { text: 'ДхШхВ, м', dataIndex: 'p_size_display', width: 100 },
                    { text: 'Г/О, т/м<sup>3</sup>', dataIndex: 'p_go_display', width: 100 },
                    { text: 'О', dataIndex: 'p_char_ops', width: 20, resizable: false, align: 'center', renderer: this.radioButtonRenderer1, tdCls: 'pseudoinput-column' },
                    { text: 'П', dataIndex: 'p_char_ops', width: 20, resizable: false, align: 'center', renderer: this.radioButtonRenderer2, tdCls: 'pseudoinput-column' },
                    { text: 'С', dataIndex: 'p_char_ops', width: 20, resizable: false, align: 'center', renderer: this.radioButtonRenderer3, tdCls: 'pseudoinput-column' },
                    { text: 'Тип кузова', dataIndex: 'p_char_type_display', width: 100 },
                    { text: 'З', dataIndex: 'p_load_back', width: 20, resizable: false, renderer: this.checkBoxRenderer, align: 'center', tdCls: 'pseudoinput-column' },
                    { text: 'Б', dataIndex: 'p_load_left', width: 20, resizable: false, renderer: this.checkBoxRenderer, align: 'center', tdCls: 'pseudoinput-column' },
                    { text: 'В', dataIndex: 'p_load_top', width: 20, resizable: false, renderer: this.checkBoxRenderer, align: 'center', tdCls: 'pseudoinput-column' },
                    { text: 'Доп. опции', dataIndex: 'p_additional_options_display', width: 100 },
                    { text: 'Доп. характеристики', dataIndex: 'p_additional_chars_display', width: 150 }
                ]
            }
		];
	},

	getBottomColumnsConfig: function () {
		return [
			{
				xtype: 'rowactions',
				hideable: false,
				resizeable: false,
				locked: true,
				width: 18,
				actions: [{ 
	                iconCls: 'icon-delete-item', 
	                qtip: 'Удалить', 
	                callback: this.removeEmployee
	            }]
			},
            { text: 'Наименование', dataIndex: 'p_name', locked: true, width: 150 },
            { text: 'Тип', dataIndex: 'p_type_display', locked: true, width: 150 },
            {
                text: 'Транспортное средство',
                columns: [
                    { text: 'Марка, модель', dataIndex: 'p_auto_mark_model', width: 200 },
                    { text: 'Гос. номер', dataIndex: 'p_auto_number', width: 200 }
                ]
            },
            {
                text: 'Прицепное транспортное средство',
                columns: [
                    { text: 'Марка, модель', dataIndex: 'p_auto_trailer', width: 200 },
                    { text: 'Гос. номер', dataIndex: 'p_auto_trailer_number', width: 200 }
                ]
            },
            {
                text: 'Водитель',
                columns: [
                    { text: 'Фамилия', dataIndex: 'p_driver_surname', width: 150 },
                    { text: 'Имя', dataIndex: 'p_driver_name', width: 150 },
                    { text: 'Отчество', dataIndex: 'p_driver_lastname', width: 150 }
                ]
            },
            {
                text: 'Характеристики',
                columns: [
                    { text: 'Класс', dataIndex: 'p_class_display', width: 100 },
                    { text: 'ДхШхВ, м', dataIndex: 'p_size_display', width: 100 },
                    { text: 'Г/О, т/м<sup>3</sup>', dataIndex: 'p_go_display', width: 100 },
                    { text: 'О', dataIndex: 'p_char_ops', width: 20, resizable: false, align: 'center', renderer: this.radioButtonRenderer1, tdCls: 'pseudoinput-column' },
                    { text: 'П', dataIndex: 'p_char_ops', width: 20, resizable: false, align: 'center', renderer: this.radioButtonRenderer2, tdCls: 'pseudoinput-column' },
                    { text: 'С', dataIndex: 'p_char_ops', width: 20, resizable: false, align: 'center', renderer: this.radioButtonRenderer3, tdCls: 'pseudoinput-column' },
                    { text: 'Тип кузова', dataIndex: 'p_char_type_display', width: 100 },
                    { text: 'З', dataIndex: 'p_load_back', width: 20, resizable: false, renderer: this.checkBoxRenderer, align: 'center', tdCls: 'pseudoinput-column' },
                    { text: 'Б', dataIndex: 'p_load_left', width: 20, resizable: false, renderer: this.checkBoxRenderer, align: 'center', tdCls: 'pseudoinput-column' },
                    { text: 'В', dataIndex: 'p_load_top', width: 20, resizable: false, renderer: this.checkBoxRenderer, align: 'center', tdCls: 'pseudoinput-column' },
                    { text: 'Доп. опции', dataIndex: 'p_additional_options_display', width: 100 },
                    { text: 'Доп. характеристики', dataIndex: 'p_additional_chars_display', width: 150 }
                ]
            }
		];
	},

	getTopGridFilters: function () {
		return [{
            text: 'Выделить все',
            checked: true,
            checkHandler: this.toggleFilterSelection
        }, '-', {
            text: 'Наименование',
            kind: 'selector',
            field_name: 'p_name',
            checked: true
        }, {
            text: 'Тип',
            kind: 'selector',
            field_name: 'p_type_display',
            checked: true
        }, {
            text: 'ТС марка, модель',
            kind: 'selector',
            field_name: 'p_auto_mark_model',
            checked: true
        }, {
            text: 'ТС номер',
            kind: 'selector',
            field_name: 'p_auto_number',
            checked: true
        }, {
            text: 'Прицепное ТС марка, модель',
            kind: 'selector',
            field_name: 'p_auto_trailer',
            checked: true
        }, {
            text: 'Прицепное ТС номер',
            kind: 'selector',
            field_name: 'p_auto_trailer_number',
            checked: true
        }, {
            text: 'Водитель фамилия',
            kind: 'selector',
            field_name: 'p_driver_surname',
            checked: true
        }, {
            text: 'Водитель имя',
            kind: 'selector',
            field_name: 'p_driver_name',
            checked: true
        }, {
            text: 'Водитель отчество',
            kind: 'selector',
            field_name: 'p_driver_lastname',
            checked: true
        }, {
            text: 'Характеристики класс',
            kind: 'selector',
            field_name: 'p_class_display',
            checked: true
        }, {
            text: 'Тип кузова',
            kind: 'selector',
            field_name: 'p_char_type_display',
            checked: true
        }, {
            text: 'Доп. опции',
            kind: 'selector',
            field_name: 'p_additional_options_display',
            checked: true
        }, {
            text: 'Доп. характеристики',
            kind: 'selector',
            field_name: 'p_additional_chars_display',
            checked: true
        }];
	}
});