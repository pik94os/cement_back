Ext.define('Cement.view.goods.auto.form.GetterWindow', {
	extend: 'Cement.view.goods.auto.form.SupplierWindow',
	xtype: 'goods_auto_form_getter_window',
	title: 'Выбор грузополучателя',
	leftFrameTitle: 'Грузополучатель',
	topGridTitle: 'Выбрать грузополучателя',
	bottomGridTitle: 'Выбранный грузополучатель',
	structure_storeId: 'goods.auto.auxiliary.Getters'
});