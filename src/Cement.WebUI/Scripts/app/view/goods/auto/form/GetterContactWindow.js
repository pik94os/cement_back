Ext.define('Cement.view.goods.auto.form.GetterContactWindow', {
	extend: 'Cement.view.goods.auto.form.SupplierContactWindow',
	xtype: 'goods_auto_form_getter_contact_window',
	structure_storeId: 'goods.auto.auxiliary.GetterContacts'
});