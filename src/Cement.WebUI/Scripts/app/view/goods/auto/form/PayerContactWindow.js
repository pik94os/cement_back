Ext.define('Cement.view.goods.auto.form.PayerContactWindow', {
	extend: 'Cement.view.goods.auto.form.SupplierContactWindow',
	xtype: 'goods_auto_form_payer_contact_window',
	structure_storeId: 'goods.auto.auxiliary.PayerContacts'
});