Ext.define('Cement.view.goods.auto.form.SupplierWindow', {
	extend: 'Cement.view.common.SignWindow',
	xtype: 'goods_auto_form_supplier_window',
	title: 'Выбор поставщика',
	leftFrameTitle: 'Поставщик',
	topGridTitle: 'Выбрать поставщика',
	bottomGridTitle: 'Выбранный поставщик',
	structure_storeId: 'goods.auto.auxiliary.Suppliers',
	hideCommentPanel: true,
	singleSelection: true,
	displayField: 'p_name',
	storeFields: ['id', 'p_name', 'p_address', 'p_supplier'],

	bbar: [{
		text: 'Сохранить',
		action: 'confirm'
	}, {
		text: 'Отмена',
		action: 'cancel'
	}],

	setRequestId: function (req_id) {
		Ext.getStore(this.structure_storeId).getProxy().setExtraParam('request_id', req_id);
	},

	sign: function (send) {
		if (this.dstStore.getCount() > 0) {
			this.fireEvent('selected', this.dstStore.getAt(0));
		}
	},

	getTopColumnsConfig: function () {
		return [
			{
				xtype: 'rowactions',
				hideable: false,
				resizeable: false,
				width: 18,
				keepSelection: true,
				actions: [{ 
	                iconCls: 'icon-add-item', 
	                qtip: 'Добавить',
	                callback: this.addEmployee
	            }]
			},
			{ text: 'Наименование', dataIndex: 'p_name', flex: 1 },
			{ text: 'Адрес', dataIndex: 'p_address', flex: 1 }
		];
	},

	getBottomColumnsConfig: function () {
		return [
			{
				xtype: 'rowactions',
				hideable: false,
				resizeable: false,
				width: 18,
				actions: [{ 
	                iconCls: 'icon-delete-item', 
	                qtip: 'Удалить', 
	                callback: this.removeEmployee
	            }]
			},
			{ text: 'Наименование', dataIndex: 'p_name', flex: 1 },
			{ text: 'Адрес', dataIndex: 'p_address', flex: 1 }
		];
	},

		getBottomColumnsConfig: function () {
		return [
			{
				xtype: 'rowactions',
				hideable: false,
				resizeable: false,
				width: 18,
				actions: [{ 
	                iconCls: 'icon-delete-item', 
	                qtip: 'Удалить', 
	                callback: this.removeEmployee
	            }]
			},
			{ text: 'Наименование', dataIndex: 'p_name', flex: 1 }
		];
	},

	getTopGridFilters: function () {
		return [{
            text: 'Выделить все',
            checked: true,
            checkHandler: this.toggleFilterSelection
        }, '-', {
            text: 'ФИО',
            kind: 'selector',
            field_name: 'text',
            checked: true
        }, {
            text: 'Отдел',
            kind: 'selector',
            field_name: 'department',
            checked: false
        }, {
            text: 'Должность',
            kind: 'selector',
            field_name: 'position',
            checked: false
        }];
	},

	initComponent: function () {
		this.callParent(arguments);
		this.on('afterrender', function () {
			Ext.getStore(this.structure_storeId).getRootNode().expand();
		}, this);
	}
});