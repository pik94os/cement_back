Ext.define('Cement.view.goods.auto.form.TemplateWindow', {
	extend: 'Cement.view.goods.auto.form.RequestWindow',
	xtype: 'goods_auto_form_template_window',
	title: 'Выбор шаблона',
	layout: {
		type: 'vbox',
		align : 'stretch'
	},
	width: 900,
	height: 400,
	bodyPadding: 10,
	bbar: [{
		text: 'Сохранить',
		action: 'confirm'
	}, {
		text: 'Отмена',
		action: 'cancel'
	}],
	modal: true,
	topGridTitle: 'Выбрать шаблон',
	bottomGridTitle: 'Выбранный шаблон',
	loadRightFromTree: true,
	storeFields: ['id', 'p_name'],
	srcStoreId: 'goods.auto.auxiliary.AllTemplates',

	getTopColumnsConfig: function () {
		return [
			{
				xtype: 'rowactions',
				hideable: false,
				resizeable: false,
				width: 18,
				keepSelection: true,
				locked: true,
				actions: [{ 
	                iconCls: 'icon-add-people', 
	                qtip: 'Добавить',
	                callback: this.addEmployee
	            }]
			},
			{ text: 'Шаблон', dataIndex: 'p_name', flex: 1 }
		];
	},

	getBottomColumnsConfig: function () {
		return [
			{
				xtype: 'rowactions',
				hideable: false,
				resizeable: false,
				locked: true,
				width: 18,
				actions: [{ 
	                iconCls: 'icon-remove-people', 
	                qtip: 'Удалить', 
	                callback: this.removeEmployee
	            }]
			},
            { text: 'Шаблон', dataIndex: 'p_name', flex: 1 }
		];
	},

	getTopGridFilters: function () {
		return [{
            text: 'Выделить все',
            checked: true,
            checkHandler: this.toggleFilterSelection
        }, '-', {
            text: 'Шаблон',
            kind: 'selector',
            field_name: 'p_name',
            checked: true
        }];
	}
});