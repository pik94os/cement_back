Ext.define('Cement.view.goods.auto.form.ContractWindow', {
    extend: 'Cement.view.common.SignWindow',
    xtype: 'goods_auto_form_contract_window',
    title: 'Выбор товара',
    hideCommentPanel: true,
    leftFrameTitle: 'Договоры',
    topGridTitle: 'Товары',
    bottomGridTitle: 'Выбранный товар',
    singleSelection: true,
    rootVisible: false,
    displayField: 'p_name',
    bbar: [{
        text: 'Сохранить',
        action: 'confirm'
    }, {
        text: 'Отмена',
        action: 'cancel'
    }],

    structure_storeId: 'goods.auto.auxiliary.Contracts',
    loadRightFromTree: false,
    storeFields: ['id', 'p_name', 'p_product_unit', 'p_product_unit_display', 'p_price', 'p_tax'],

    getBottomColumnsConfig: function () {
        return [{
            xtype: 'rowactions',
            hideable: false,
            resizeable: false,
            width: 18,
            actions: [{
                iconCls: 'icon-delete-item',
                qtip: 'Удалить',
                callback: this.removeEmployee
            }]
        },
		{ text: 'Наименование', dataIndex: 'p_name', flex: 1 },
        { text: 'Ед.измерения', dataIndex: 'p_product_unit_display', flex: 1 },
        { text: 'Налог', dataIndex: 'p_tax', flex: 1 },
        { text: 'Цена', dataIndex: 'p_price', flex: 1 }];
    },

    getTopGridFilters: function () {
        return [{
            text: 'Товар',
            kind: 'selector',
            field_name: 'p_name',
            checked: true
        }];
    },

    getTopColumnsConfig: function () {
        return [
			{
			    xtype: 'rowactions',
			    hideable: false,
			    resizeable: false,
			    width: 18,
			    keepSelection: true,
			    actions: [{
			        iconCls: 'icon-add-item',
			        qtip: 'Добавить',
			        callback: this.addEmployee
			    }]
			},
			{ text: 'Наименование', dataIndex: 'p_name', flex: 1 },
            { text: 'Ед.измерения', dataIndex: 'p_product_unit_display', flex: 1 },
            { text: 'Налог', dataIndex: 'p_tax', flex: 1 },
            { text: 'Цена', dataIndex: 'p_price', flex: 1 }
        ];
    },

    sign: function (send) {
        var store = this.down('*[role=sign-dst-grid]').getStore();
        if (store.getCount() > 0) {
            var product = store.getAt(0),
				contract = this.down('treepanel').getSelectionModel().getSelection()[0];
            this.fireEvent('productselected', product, contract);
        }
    },

    closeWindow: function () {
        this.hide();
    },

    loadData: function (data) {
        if (data) {
            var fstore = Ext.getStore(this.structure_storeId);
            fstore.load({
                callback: function () {
                    Ext.each(data, function (item) {
                        var r = fstore.getNodeById(item);
                        if (!this.dstStore) {
                            this.setupEvents();
                        }
                        if (r) {
                            this.dstStore.add({
                                id: r.get('id'),
                                name: r.get('p_name')
                            });
                        }
                    }, this);
                },
                scope: this
            });
        }
    },

    getSrcStore: function () {
        return Ext.create('Ext.data.Store', {
            fields: this.storeFields,
            autoLoad: false,
            remoteFilter: true,
            proxy: getProxy(Cement.Config.url.goods.auto.products_dict, '')
        });
    },

    initComponent: function () {
        this.callParent(arguments);
        this.on('beforeclose', function () {
            this.hide();
            return false;
        });

        var treePanel = this.down('treepanel');

        treePanel.on('selectionchange', function (treepanel, selected) {
            if (selected.length && typeof selected[0].get('id') == "number") {
                var contrId = selected[0].get('id');
                this.srcStore.clearFilter(true);
                
                var requestId = Ext.ComponentQuery.query('.goods_auto_form_form')[0].down('hiddenfield[name=id]').getValue();
                this.srcStore.getProxy().setExtraParam('request_id', requestId);
                this.srcStore.filter([{
                    property: 'p_contract_id',
                    value: contrId
                }]);
            }

        }, this);
        this.addEvents('productselected');
    },

    setFirm: function (firm_id) {
        var store = Ext.getStore(this.structure_storeId);
        store.getProxy().setExtraParam('p_firm_id', firm_id);
    },

    setRequestId: function (req_id) {
        Ext.getStore(this.structure_storeId).getProxy().setExtraParam('request_id', req_id);
    }
});