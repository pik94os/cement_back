Ext.define('Cement.view.goods.auto.form.RequestWindow', {
	extend: 'Ext.window.Window',
	xtype: 'goods_auto_form_request_window',
	title: 'Выбор заявки на товар',
	layout: {
		type: 'vbox',
		align : 'stretch'
	},
	width: 900,
	height: 400,
	bodyPadding: 10,
	bbar: [{
		text: 'Сохранить',
		action: 'confirm'
	}, {
		text: 'Отмена',
		action: 'cancel'
	}],
	modal: true,
	topGridTitle: 'Выбрать заявку на товар',
	bottomGridTitle: 'Выбранная заявка на товар',
	loadRightFromTree: true,
	storeFields: ['id', 'p_date', 'p_payer', 'p_getter_name', 'p_getter_address', 'p_getter_work_time',
		'p_product_display', 'p_product_unit_display', 'p_product_price', 'p_product_count',
		'p_transport_char_ops', 'p_transport_char_type_display', 'p_transport_additional_options_display',
		'p_transport_load_back', 'p_transport_load_left', 'p_transport_load_top',
		'p_status'],
	singleSelection: true,
	cls: 'white-window',
	srcStoreId: 'goods.auto.auxiliary.AllRequests',

	getCenterMargin: function () {
		return '10 0';
	},

	loadData: function (data) {
		if (data) {
			Ext.each(data, function (item) {
				var r = this.srcStore.getById(item);
				if (!this.dstStore) {
					this.setupEvents();
				}
				if (r) {
					this.dstStore.add(r.copy());
				}
			}, this);
		}
	},

	getTopColumnsConfig: function () {
		return [
			{
				xtype: 'rowactions',
				hideable: false,
				resizeable: false,
				width: 18,
				keepSelection: true,
				locked: true,
				actions: [{ 
	                iconCls: 'icon-add-item', 
	                qtip: 'Добавить',
	                callback: this.addEmployee
	            }]
			},
			{ text: '№ П/п', dataIndex: 'p_number', width: 60, locked: true },
            { text: 'Дата', dataIndex: 'p_date', width: 60, locked: true },
            { text: 'Плательщик', dataIndex: 'p_payer_name', width: 150 },
            {
                text: 'Грузополучатель',
                columns: [
                    { text: 'Наименование', dataIndex: 'p_getter_name', width: 150 },
                    { text: 'Адрес', dataIndex: 'p_getter_address', width: 150 },
                    { text: 'Рабочее время', dataIndex: 'p_getter_work_time', width: 150 }
                ]
            },
            {
                text: 'Товар',
                columns: [
                    { text: 'Наименование', dataIndex: 'p_product_display', width: 150 },
                    { text: 'Ед. изм.', dataIndex: 'p_product_unit_display', width: 50 },
                    { text: 'Кол-во', dataIndex: 'p_product_quantity', width: 50 },
                    { text: 'Факт кол-во', dataIndex: 'p_product_fact_quantity', width: 50 },
                    { text: 'Цена', dataIndex: 'p_product_price', width: 70 },
                    { text: 'Налог', dataIndex: 'p_product_tax', width: 70 },
                    { text: 'Сумма', dataIndex: 'p_product_sum', width: 70 }
                ]
            },
            {
                text: 'Транспорт',
                columns: [
                    { text: 'О', dataIndex: 'p_transport_char_ops', width: 20, resizable: false, align: 'center', renderer: this.radioButtonRenderer1, tdCls: 'pseudoinput-column' },
                    { text: 'П', dataIndex: 'p_transport_char_ops', width: 20, resizable: false, align: 'center', renderer: this.radioButtonRenderer2, tdCls: 'pseudoinput-column' },
                    { text: 'С', dataIndex: 'p_transport_char_ops', width: 20, resizable: false, align: 'center', renderer: this.radioButtonRenderer3, tdCls: 'pseudoinput-column' },
                    { text: 'Тип кузова', dataIndex: 'p_transport_char_type_display', width: 100 },
                    { text: 'З', dataIndex: 'p_transport_load_back', width: 20, resizable: false, renderer: this.checkBoxRenderer, align: 'center', tdCls: 'pseudoinput-column' },
                    { text: 'Б', dataIndex: 'p_transport_load_left', width: 20, resizable: false, renderer: this.checkBoxRenderer, align: 'center', tdCls: 'pseudoinput-column' },
                    { text: 'В', dataIndex: 'p_transport_load_top', width: 20, resizable: false, renderer: this.checkBoxRenderer, align: 'center', tdCls: 'pseudoinput-column' },
                    { text: 'Доп. опции', dataIndex: 'p_transport_additional_options_display', width: 100 }
                ]
            },
            { text: 'Статус', dataIndex: 'p_status_display', width: 150 }
		];
	},

	getBottomColumnsConfig: function () {
		return [
			{
				xtype: 'rowactions',
				hideable: false,
				resizeable: false,
				locked: true,
				width: 18,
				actions: [{ 
	                iconCls: 'icon-delete-item', 
	                qtip: 'Удалить', 
	                callback: this.removeEmployee
	            }]
			},
			{ text: '№ П/п', dataIndex: 'p_number', width: 60, locked: true },
            { text: 'Дата', dataIndex: 'p_date', width: 60, locked: true },
            { text: 'Плательщик', dataIndex: 'p_payer_name', width: 150 },
            {
                text: 'Грузополучатель',
                columns: [
                    { text: 'Наименование', dataIndex: 'p_getter_name', width: 150 },
                    { text: 'Адрес', dataIndex: 'p_getter_address', width: 150 },
                    { text: 'Рабочее время', dataIndex: 'p_getter_work_time', width: 150 }
                ]
            },
            {
                text: 'Товар',
                columns: [
                    { text: 'Наименование', dataIndex: 'p_product_display', width: 150 },
                    { text: 'Ед. изм.', dataIndex: 'p_product_unit_display', width: 50 },
                    { text: 'Кол-во', dataIndex: 'p_product_quantity', width: 50 },
                    { text: 'Факт кол-во', dataIndex: 'p_product_fact_quantity', width: 50 },
                    { text: 'Цена', dataIndex: 'p_product_price', width: 70 },
                    { text: 'Налог', dataIndex: 'p_product_tax', width: 70 },
                    { text: 'Сумма', dataIndex: 'p_product_sum', width: 70 }
                ]
            },
            {
                text: 'Транспорт',
                columns: [
                    { text: 'О', dataIndex: 'p_transport_char_ops', width: 20, resizable: false, align: 'center', renderer: this.radioButtonRenderer1, tdCls: 'pseudoinput-column' },
                    { text: 'П', dataIndex: 'p_transport_char_ops', width: 20, resizable: false, align: 'center', renderer: this.radioButtonRenderer2, tdCls: 'pseudoinput-column' },
                    { text: 'С', dataIndex: 'p_transport_char_ops', width: 20, resizable: false, align: 'center', renderer: this.radioButtonRenderer3, tdCls: 'pseudoinput-column' },
                    { text: 'Тип кузова', dataIndex: 'p_transport_char_type_display', width: 100 },
                    { text: 'З', dataIndex: 'p_transport_load_back', width: 20, resizable: false, renderer: this.checkBoxRenderer, align: 'center', tdCls: 'pseudoinput-column' },
                    { text: 'Б', dataIndex: 'p_transport_load_left', width: 20, resizable: false, renderer: this.checkBoxRenderer, align: 'center', tdCls: 'pseudoinput-column' },
                    { text: 'В', dataIndex: 'p_transport_load_top', width: 20, resizable: false, renderer: this.checkBoxRenderer, align: 'center', tdCls: 'pseudoinput-column' },
                    { text: 'Доп. опции', dataIndex: 'p_transport_additional_options_display', width: 100 }
                ]
            },
            { text: 'Статус', dataIndex: 'p_status_display', width: 150 }
		];
	},

	getTopGridFilters: function () {
		return [{
            text: 'Выделить все',
            checked: true,
            checkHandler: this.toggleFilterSelection
        }, '-', {
            text: 'Наименование',
            kind: 'selector',
            field_name: 'p_name',
            checked: true
        }, {
            text: 'Поставщик',
            kind: 'selector',
            field_name: 'p_supplier_name',
            checked: true
        }, {
            text: 'Плательщик наименование',
            kind: 'selector',
            field_name: 'p_payer_name',
            checked: true
        }, {
            text: 'Плательщик адрес',
            kind: 'selector',
            field_name: 'p_payer_address',
            checked: true
        }, {
            text: 'Плательщик рабочее время',
            kind: 'selector',
            field_name: 'p_payer_work_time',
            checked: true
        }, {
            text: 'Грузополучатель наименование',
            kind: 'selector',
            field_name: 'p_getter_name',
            checked: true
        }, {
            text: 'Грузополучатель адрес',
            kind: 'selector',
            field_name: 'p_getter_address',
            checked: true
        }, {
            text: 'Грузополучатель рабочее время',
            kind: 'selector',
            field_name: 'p_getter_work_time',
            checked: true
        }, {
            text: 'Транспорт тип кузова',
            kind: 'selector',
            field_name: 'p_transport_char_type_display',
            checked: true
        }, {
            text: 'Транспорт доп. опции',
            kind: 'selector',
            field_name: 'p_transport_additional_options_display',
            checked: true
        }];
	},

	getSrcStore: function () {
		return Ext.getStore(this.srcStoreId);
	},

	getSrcGrid: function () {
		var m_items = [{
		            xtype: 'cleartrigger',
	                emptyText: 'Поиск',
	                name: 'search_input',
	                onTrigger1Click: this.clearFilter,
	                onTrigger2Click: this.applyFilter
		        }, {
		            text: 'Выделить все',
		            checked: false,
		            checkHandler: this.toggleFilterSelection
		        }, '-'];
		Ext.each(this.getTopGridFilters(), function (itm) {
			m_items.push(itm);
		});
		return {
			xtype: 'panel',
			flex: 1,
			border: 1,
			layout: 'fit',
			role: 'sign-src-panel',
			tbar: [{
				xtype: 'label',
				text: this.topGridTitle,
				padding: '2 0 4 10'
			}, '->', {
                xtype: 'button',
                text: 'Поиск',
                menu: {
                    xtype: 'menu',
                    plain: true,
                    items: m_items
                }
            }],
			items: [{
				xtype: 'grid',
				layout: 'fit',
				role: 'sign-src-grid',
				border: 0,
				store: this.getSrcStore(),
				columns: this.getTopColumnsConfig()
			}]
		};
	},

	getComboRenderer: function (store) {
        return function (val) {
            if (!val) return '';
            return store.getById(val).get('p_name');
        };
    },

	getDstGrid: function () {
		this.statusStore = Ext.getStore('signers_statuses_Store');
		this.editor = Ext.create('Ext.grid.plugin.CellEditing', {
            clicksToEdit: 1,
            listeners: {
                edit: function (editor, e) {
                    e.grid.getSelectionModel().deselectAll(true);
                    e.grid.getSelectionModel().select(e.rowIdx);
                }
            }
        });
		return {
			xtype: 'panel',
			flex: 1,
			border: 1,
			layout: 'fit',
			tbar: [{
				xtype: 'label',
				text: this.bottomGridTitle,
				padding: '2 0 4 10'
			}],
			items: [{
				xtype: 'grid',
				// id: 'sign-dst-grid',
				role: 'sign-dst-grid',
				layout: 'fit',
				border: 0,
				store: Ext.create('Ext.data.Store', {
					fields: this.storeFields
				}),
				columns: this.getBottomColumnsConfig(),
				plugins: [
                    this.editor
                ]
			}]
		};
	},

	initComponent: function () {
		Ext.apply(this, {
			items: [this.getSrcGrid(), {
				xtype: 'panel',
				border: 0,
				margin: '5 0',
				defaults: {
					xtype: 'button'
				},
				role: 'btns-panel',
				items: [{
					text: 'Создать всех',
					action: 'create_all',
					iconCls: 'icon-move-bottom',
					hidden: this.singleSelection,
					margin: '0 5 0 0'
				}, {
					text: 'Создать',
					action: 'create',
					iconCls: 'icon-move-down',
					margin: '0 5 0 0'
				}, {
					text: 'Удалить всех',
					action: 'delete_all',
					hidden: this.singleSelection,
					iconCls: 'icon-move-top',
					margin: '0 5 0 0'
				}, {
					text: 'Удалить',
					action: 'delete',
					iconCls: 'icon-move-up'
				}]
			}, this.getDstGrid()]
		});
		this.callParent(arguments);
		this.setupEvents();
		this.addEvents('requestselected');
		Ext.getStore(this.srcStoreId).load();
		this.on('beforeclose', function () {
			this.hide();
			return false;
		});
	},

	addToSrcStore: function (item) {
		if (!this.dstStore.getById(item.get('id'))) {
			var r = {};
			Ext.each(this.storeFields, function (i) {
				r[i] = item.get(i);
			});
			this.srcStore.add(r);
		}
	},

	setupEvents: function () {
		var commentField = this.down('textarea[name=comment]');
		this.srcStore = this.down('*[role=sign-src-grid]').getStore();
		this.dstStore = this.down('*[role=sign-dst-grid]').getStore();

		this.down('button[action=cancel]').on('click', function () {
			this.closeWindow();
		}, this);

		this.down('button[action=confirm]').on('click', function () {
			this.sign(false);
		}, this);

		if (this.down('button[action=confirm_and_send]')) {
			this.down('button[action=confirm_and_send]').on('click', function () {
				this.sign(true);
			}, this);
		}

		this.down('button[action=delete_all]').on('click', function () {
			this.dstStore.each(function (record) {
				if (record.get('department_id') == this.currentDepartment) {
					this.srcStore.add(record.copy());
				}
			}, this);
			this.dstStore.removeAll();
		}, this);
		this.down('button[action=delete]').on('click', function () {
			var grid = this.down('*[role=sign-dst-grid]'),
				sel = grid.getSelectionModel().getSelection();
			if (sel.length > 0) {
				this.removeE(sel[0]);
			}
		}, this);
		this.down('button[action=create_all]').on('click', function () {
			this.srcStore.each(function (record) {
				this.dstStore.add(record.copy());
			}, this);
			this.srcStore.removeAll();
		}, this);
		this.down('button[action=create]').on('click', function () {
			var grid = this.down('*[role=sign-src-grid]'),
				sel = grid.getSelectionModel().getSelection();
			if (sel.length > 0) {
				this.addE(sel[0]);
			}
		}, this);
	},

	closeWindow: function () {
		this.close();
	},

	sign: function (send) {
		if (this.dstStore.getCount() > 0) {
			this.fireEvent('requestselected', this.dstStore.getAt(0));
		}
	},

	addE: function (record) {
		if (this.singleSelection) {
			if (this.dstStore.getCount() > 0) {
				return;
			}
		}
		this.dstStore.add(record.copy());
		this.srcStore.remove(record);
	},

	removeE: function (record) {
		if (record) {
			if (record.get('department_id') == this.currentDepartment) {
				this.srcStore.add(record.copy());
			}
			this.dstStore.remove(record);
		}
	},

	addEmployee: function (grid, record) {
		grid.up('window').addE(record);
	},

	removeEmployee: function (grid, record) {
		grid.up('window').removeE(record);
	},

	toggleFilterSelection: function (item, checked) {
        var toolbar = item.up('toolbar'),
            items = toolbar.query('menu menucheckitem[kind=selector]');
        Ext.each(items, function (it) {
            it.setChecked(item.checked);
        });
    },

    getFilterFields: function () {
        var items = this.down('toolbar').query('menu menucheckitem[kind=selector]'),
            val = this.down('toolbar cleartrigger[name=search_input]').getValue(),
            result = [];
        Ext.each(items, function (item) {
            if (item.checked) {
                result.push({
                    property: item.field_name,
                    value: new RegExp('.*' + val + '.*')
                });
            }
        });
        return result;
    },

    applyFilter: function () {
        var grid = this.up('*[role=sign-src-panel]').down('*[role=sign-src-grid]'),
            filter = grid.up('window').getFilterFields();
        grid.getStore().clearFilter(true);
        grid.getStore().filter(filter);
    },

    clearFilter: function () {
    	this.up('*[role=sign-src-panel]').down('toolbar cleartrigger[name=search_input]').setValue('');
    	this.up('*[role=sign-src-panel]').down('*[role=sign-src-grid]').getStore().clearFilter(false);
    }
});