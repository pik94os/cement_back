Ext.define('Cement.view.goods.auto.form.WarehouseWindow', {
	extend: 'Cement.view.common.SignWindow',
	xtype: 'goods_auto_form_warehouse_window',
	title: 'Выбор склада',
	leftFrameTitle: 'Склад',
	topGridTitle: 'Выбрать склад',
	bottomGridTitle: 'Выбранный склад',
	structure_storeId: 'goods.auto.auxiliary.Warehouses',
	hideCommentPanel: true,
	singleSelection: true,
	displayField: 'p_name',
	storeFields: ['id', 'p_name', 'p_position', 'p_department'],

	bbar: [{
		text: 'Сохранить',
		action: 'confirm'
	}, {
		text: 'Отмена',
		action: 'cancel'
	}],

	sign: function (send) {
		if (this.dstStore.getCount() > 0) {
			this.fireEvent('selected', this.dstStore.getAt(0));
		}
	},

	getTopColumnsConfig: function () {
		return [
			{
				xtype: 'rowactions',
				hideable: false,
				resizeable: false,
				width: 18,
				keepSelection: true,
				actions: [{ 
	                iconCls: 'icon-add-people', 
	                qtip: 'Добавить',
	                callback: this.addEmployee
	            }]
			},
			{ text: 'Наименование', dataIndex: 'p_name', flex: 1 }
		];
	},

	getBottomColumnsConfig: function () {
		return [
			{
				xtype: 'rowactions',
				hideable: false,
				resizeable: false,
				width: 18,
				actions: [{ 
	                iconCls: 'icon-remove-people', 
	                qtip: 'Удалить', 
	                callback: this.removeEmployee
	            }]
			},
			{ text: 'Наименование', dataIndex: 'p_name', flex: 1 }
		];
	},

	setFirm: function (firm_id) {
		this.firm_id = firm_id;
		var store = Ext.getStore(this.structure_storeId);
		store.getProxy().setExtraParam('p_firm_id', firm_id);
		store.load({
			params: { _id: Math.random()},
			callback: function () {
				store.getRootNode().expand();
			}
		});
	}
});