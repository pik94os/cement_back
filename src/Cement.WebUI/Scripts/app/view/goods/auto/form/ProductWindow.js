Ext.define('Cement.view.goods.auto.form.ProductWindow', {
    extend: 'Ext.window.Window',
    xtype: 'goods_auto_form_product_window',
    title: 'Выбор товара',
    layout: {
        type: 'vbox',
        align: 'stretch'
    },
    width: 900,
    height: 400,
    bodyPadding: 10,
    bbar: [
        {
            text: 'Сохранить',
            action: 'confirm'
        }, {
            text: 'Отмена',
            action: 'cancel'
        }
    ],
    modal: true,
    contractId: null,
	topGridTitle: 'Выбрать товар',
	bottomGridTitle: 'Выбранный товар',
	loadRightFromTree: true,
	storeFields: ['id', 'p_group', 'p_name', 'p_subgroup', 'p_trade_mark', 'p_manufacturer',
		'p_product_unit_display', 'p_tax', 'p_price'],
	singleSelection: true,
	cls: 'white-window',
	srcStoreId: 'goods.auto.auxiliary.Products',

	getCenterMargin: function () {
		return '10 0';
	},

	loadData: function (data) {
		if (data) {
			Ext.each(data, function (item) {
				var r = this.srcStore.getById(item);
				if (!this.dstStore) {
					this.setupEvents();
				}
				if (r) {
					this.dstStore.add(r.copy());
				}
			}, this);
		}
	},

	getTopColumnsConfig: function () {
		return [
			{
				xtype: 'rowactions',
				hideable: false,
				resizeable: false,
				width: 18,
				keepSelection: true,
				locked: true,
				actions: [{ 
	                iconCls: 'icon-add-item', 
	                qtip: 'Добавить',
	                callback: this.addEmployee
	            }]
			},
			{ text: 'Наименование', dataIndex: 'p_name', width: 200, locked: true },
      { text: 'Группа', dataIndex: 'p_group', width: 200 },
      { text: 'Подгруппа', dataIndex: 'p_subgroup', width: 200 },
      { text: 'Торговая марка', dataIndex: 'p_trade_mark', width: 200 },
      { text: 'Производитель', dataIndex: 'p_manufacturer', width: 200 },
      { text: 'Единица измерения', dataIndex: 'p_product_unit_display', width: 100 },
      { text: 'Налог', dataIndex: 'p_tax', width: 100 },
      { text: 'Цена', dataIndex: 'p_price', width: 100 }
		];
	},

	getBottomColumnsConfig: function () {
		return [
			{
				xtype: 'rowactions',
				hideable: false,
				resizeable: false,
				locked: true,
				width: 18,
				actions: [{ 
	                iconCls: 'icon-delete-item', 
	                qtip: 'Удалить', 
	                callback: this.removeEmployee
	            }]
			},
			{ text: 'Наименование', dataIndex: 'p_name', width: 200, locked: true },
      { text: 'Группа', dataIndex: 'p_group', width: 200 },
      { text: 'Подгруппа', dataIndex: 'p_subgroup', width: 200 },
      { text: 'Торговая марка', dataIndex: 'p_trade_mark', width: 200 },
      { text: 'Производитель', dataIndex: 'p_manufacturer', width: 200 },
      { text: 'Единица измерения', dataIndex: 'p_product_unit_display', width: 100 },
      { text: 'Налог', dataIndex: 'p_tax', width: 100 },
      { text: 'Цена', dataIndex: 'p_price', width: 100 }
		];
	},

	getTopGridFilters: function () {
		return [{
            text: 'Наименование',
            kind: 'selector',
            field_name: 'p_name',
            checked: true
        }, {
            text: 'Группа',
            kind: 'selector',
            field_name: 'p_group',
            checked: true
        }, {
            text: 'Подгруппа',
            kind: 'selector',
            field_name: 'p_subgroup',
            checked: true
        }, {
            text: 'Торговая марка',
            kind: 'selector',
            field_name: 'p_trade_mark',
            checked: true
        }, {
            text: 'Производитель',
            kind: 'selector',
            field_name: 'p_manufacturer',
            checked: true
        }];
	},

	getSrcStore: function () {
	    var store = Ext.getStore(this.srcStoreId);
	    store.getProxy().setExtraParam('p_contract_id', this.contractId);
	    return store;
	},

	getSrcGrid: function () {
		var m_items = [{
		            xtype: 'cleartrigger',
	                emptyText: 'Поиск',
	                name: 'search_input',
	                onTrigger1Click: this.clearFilter,
	                onTrigger2Click: this.applyFilter
		        }, {
		            text: 'Выделить все',
		            checked: false,
		            checkHandler: this.toggleFilterSelection
		        }, '-'];
		Ext.each(this.getTopGridFilters(), function (itm) {
			m_items.push(itm);
		});
		return {
			xtype: 'panel',
			flex: 1,
			border: 1,
			layout: 'fit',
			role: 'sign-src-panel',
			tbar: [{
				xtype: 'label',
				text: this.topGridTitle,
				padding: '2 0 4 10'
			}, '->', {
                xtype: 'button',
                text: 'Поиск',
                menu: {
                    xtype: 'menu',
                    plain: true,
                    items: m_items
                }
            }],
			items: [{
				xtype: 'grid',
				layout: 'fit',
				role: 'sign-src-grid',
				border: 0,
				plugins: this.getGridPlugins(),
				store: this.getSrcStore(),
				columns: this.getTopColumnsConfig()
			}]
		};
	},

	getGridPlugins: function () {
        return [{
            ptype: 'rowexpander',
            rowBodyTpl : new Ext.XTemplate(
                '<p>{p_text}</p>'
            )
        }];
    },

	getComboRenderer: function (store) {
        return function (val) {
            if (!val) return '';
            return store.getById(val).get('p_name');
        };
    },

	getDstGrid: function () {
		return {
			xtype: 'panel',
			flex: 1,
			border: 1,
			layout: 'fit',
			tbar: [{
				xtype: 'label',
				text: this.bottomGridTitle,
				padding: '2 0 4 10'
			}],
			items: [{
				xtype: 'grid',
				// id: 'sign-dst-grid',
				role: 'sign-dst-grid',
				layout: 'fit',
				border: 0,
				store: Ext.create('Ext.data.Store', {
					fields: this.storeFields
				}),
				plugins: this.getGridPlugins(),
				columns: this.getBottomColumnsConfig()
			}]
		};
	},

	constructor: function (contractId) {
	    this.contractId = contractId;
	    this.callParent(arguments);
	},

	initComponent: function () {
		Ext.apply(this, {
			items: [this.getSrcGrid(), {
				xtype: 'panel',
				border: 0,
				margin: '5 0',
				defaults: {
					xtype: 'button'
				},
				role: 'btns-panel',
				items: [{
					text: 'Создать всех',
					action: 'create_all',
					iconCls: 'icon-move-bottom',
					hidden: this.singleSelection,
					margin: '0 5 0 0'
				}, {
					text: 'Создать',
					action: 'create',
					iconCls: 'icon-move-down',
					margin: '0 5 0 0'
				}, {
					text: 'Удалить всех',
					action: 'delete_all',
					hidden: this.singleSelection,
					iconCls: 'icon-move-top',
					margin: '0 5 0 0'
				}, {
					text: 'Удалить',
					action: 'delete',
					iconCls: 'icon-move-up'
				}]
			}, this.getDstGrid()]
		});
		this.callParent(arguments);
		this.setupEvents();
		this.addEvents('requestselected');
		Ext.getStore(this.srcStoreId).load();
		this.on('beforeclose', function () {
			this.hide();
			return false;
		});
	},

	addToSrcStore: function (item) {
		if (!this.dstStore.getById(item.get('id'))) {
			var r = {};
			Ext.each(this.storeFields, function (i) {
				r[i] = item.get(i);
			});
			this.srcStore.add(r);
		}
	},

	setupEvents: function () {
		var commentField = this.down('textarea[name=comment]');
		this.srcStore = this.down('*[role=sign-src-grid]').getStore();
		this.dstStore = this.down('*[role=sign-dst-grid]').getStore();

		this.down('button[action=cancel]').on('click', function () {
			this.closeWindow();
		}, this);

		this.down('button[action=confirm]').on('click', function () {
			this.sign(false);
		}, this);

		if (this.down('button[action=confirm_and_send]')) {
			this.down('button[action=confirm_and_send]').on('click', function () {
				this.sign(true);
			}, this);
		}

		this.down('button[action=delete_all]').on('click', function () {
			this.dstStore.each(function (record) {
				if (record.get('department_id') == this.currentDepartment) {
					this.srcStore.add(record.copy());
				}
			}, this);
			this.dstStore.removeAll();
		}, this);
		this.down('button[action=delete]').on('click', function () {
			var grid = this.down('*[role=sign-dst-grid]'),
				sel = grid.getSelectionModel().getSelection();
			if (sel.length > 0) {
				this.removeE(sel[0]);
			}
		}, this);
		this.down('button[action=create_all]').on('click', function () {
			this.srcStore.each(function (record) {
				this.dstStore.add(record.copy());
			}, this);
			this.srcStore.removeAll();
		}, this);
		this.down('button[action=create]').on('click', function () {
			var grid = this.down('*[role=sign-src-grid]'),
				sel = grid.getSelectionModel().getSelection();
			if (sel.length > 0) {
				this.addE(sel[0]);
			}
		}, this);
	},

	closeWindow: function () {
		this.close();
	},

	sign: function (send) {
		if (this.dstStore.getCount() > 0) {
			this.fireEvent('productselected', this.dstStore.getAt(0));
		}
	},

	addE: function (record) {
		if (this.singleSelection) {
			if (this.dstStore.getCount() > 0) {
				return;
			}
		}
		this.dstStore.add(record.copy());
		this.srcStore.remove(record);
	},

	removeE: function (record) {
		if (record) {
			if (record.get('department_id') == this.currentDepartment) {
				this.srcStore.add(record.copy());
			}
			this.dstStore.remove(record);
		}
	},

	addEmployee: function (grid, record) {
		grid.up('window').addE(record);
	},

	removeEmployee: function (grid, record) {
		grid.up('window').removeE(record);
	},

	toggleFilterSelection: function (item, checked) {
        var toolbar = item.up('toolbar'),
            items = toolbar.query('menu menucheckitem[kind=selector]');
        Ext.each(items, function (it) {
            it.setChecked(item.checked);
        });
    },

    getFilterFields: function () {
        var items = this.down('toolbar').query('menu menucheckitem[kind=selector]'),
            val = this.down('toolbar cleartrigger[name=search_input]').getValue(),
            result = [];
        Ext.each(items, function (item) {
            if (item.checked) {
                result.push({
                    property: item.field_name,
                    value: new RegExp('.*' + val + '.*')
                });
            }
        });
        return result;
    },

    applyFilter: function () {
        var grid = this.up('*[role=sign-src-panel]').down('*[role=sign-src-grid]'),
            filter = grid.up('window').getFilterFields();
        grid.getStore().clearFilter(true);
        grid.getStore().filter(filter);
    },

    clearFilter: function () {
    	this.up('*[role=sign-src-panel]').down('toolbar cleartrigger[name=search_input]').setValue('');
    	this.up('*[role=sign-src-panel]').down('*[role=sign-src-grid]').getStore().clearFilter(false);
    },

    setContractId: function (contractId) {
        this.contractId = contractId;
        var store = Ext.getStore(this.srcStoreId);
        store.getProxy().setExtraParam('p_contract_id', contractId);
        store.load();
    }
});