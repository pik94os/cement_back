Ext.define('Cement.view.goods.auto.bottom.template.ConnectedDocumentsIncoming', {
	extend: 'Cement.view.goods.auto.lists.ConnectedDocuments',
	alias: 'widget.goods_auto_bottom_templates_connected_documents_incoming',

	gridStore: 'goods.auto.bottom.template.ConnectedDocumentsIncoming',
	gridStateId: 'stateGoodsAutoBottomTemplateConnectedDocumentsIncoming',

    printUrl: Cement.Config.url.goods.auto.documents.printGrid,
    helpUrl: Cement.Config.url.goods.auto.documents.help
});