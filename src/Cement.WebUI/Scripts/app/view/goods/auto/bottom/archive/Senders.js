Ext.define('Cement.view.goods.auto.bottom.archive.Senders', {
	extend: 'Cement.view.goods.auto.bottom.new_r.Senders',
	alias: 'widget.goods_auto_bottom_archive_senders',
  gridStore: 'goods.auto.bottom.archive.Senders',
  gridStateId: 'stateGoodsAutoBottomArchiveSenders',
  showFilterButton: false,
  autoLoadStore: false,

  printItemUrl: Cement.Config.url.goods.senders.printItem,
  printUrl: Cement.Config.url.goods.senders.printGrid,
  helpUrl: Cement.Config.url.goods.senders.help
});