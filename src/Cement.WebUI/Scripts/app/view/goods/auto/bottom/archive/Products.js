Ext.define('Cement.view.goods.auto.bottom.archive.Products', {
    extend: 'Cement.view.products.price.bottom.NewProducts',
    alias: 'widget.goods_auto_bottom_archive_products',
    gridStore: 'goods.auto.bottom.archive.Products',
    gridStateId: 'stateGoodsAutoBottomArchiveProducts'
});