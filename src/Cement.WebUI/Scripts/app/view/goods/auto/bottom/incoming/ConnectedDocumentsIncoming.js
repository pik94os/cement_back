Ext.define('Cement.view.goods.auto.bottom.incoming.ConnectedDocumentsIncoming', {
	extend: 'Cement.view.goods.auto.lists.ConnectedDocuments',
	alias: 'widget.goods_auto_bottom_incoming_connected_documents_incoming',

	gridStore: 'goods.auto.bottom.incoming.ConnectedDocumentsIncoming',
	gridStateId: 'stateGoodsAutoBottomIncomingConnectedDocumentsIncoming',

    printUrl: Cement.Config.url.goods.auto.documents.printGrid,
    helpUrl: Cement.Config.url.goods.auto.documents.help
});