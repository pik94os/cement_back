Ext.define('Cement.view.goods.auto.bottom.outcoming.Suppliers', {
	extend: 'Cement.view.goods.auto.bottom.template.Senders',
	alias: 'widget.goods_auto_bottom_outcoming_suppliers',
  gridStore: 'goods.auto.bottom.outcoming.Suppliers',
  gridStateId: 'stateGoodsAutoBottomOutcomingSuppliers',
  showFilterButton: false,

  printItemUrl: Cement.Config.url.goods.suppliers.printItem,
  printUrl: Cement.Config.url.goods.suppliers.printGrid,
  helpUrl: Cement.Config.url.goods.suppliers.help
});