Ext.define('Cement.view.goods.auto.bottom.new_r.Services', {
    extend: 'Cement.view.products.price.bottom.NewServices',
    alias: 'widget.goods_auto_bottom_new_services',
    gridStore: 'goods.auto.bottom.new_r.Services',
    gridStateId: 'stateGoodsAutoBottomNewServices'
});