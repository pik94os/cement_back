Ext.define('Cement.view.goods.auto.bottom.template.Getters', {
	extend: 'Cement.view.goods.auto.bottom.new_r.Senders',
	alias: 'widget.goods_auto_bottom_template_getters',
  gridStore: 'goods.auto.bottom.template.Getters',
  gridStateId: 'stateGoodsAutoBottomTemplateGetters',
  showFilterButton: false,

  printItemUrl: Cement.Config.url.goods.getters.printItem,
  printUrl: Cement.Config.url.goods.getters.printGrid,
  helpUrl: Cement.Config.url.goods.getters.help
});