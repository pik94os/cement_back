Ext.define('Cement.view.goods.auto.bottom.archive.ConnectedDocumentsIncoming', {
	extend: 'Cement.view.goods.auto.lists.ConnectedDocuments',
	alias: 'widget.goods_auto_bottom_archive_connected_documents_incoming',

	gridStore: 'goods.auto.bottom.archive.ConnectedDocumentsIncoming',
	gridStateId: 'stateGoodsAutoBottomArchiveConnectedDocumentsIncoming',

    printUrl: Cement.Config.url.goods.auto.documents.printGrid,
    helpUrl: Cement.Config.url.goods.auto.documents.help
});