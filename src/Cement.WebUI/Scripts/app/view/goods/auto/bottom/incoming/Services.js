Ext.define('Cement.view.goods.auto.bottom.incoming.Services', {
    extend: 'Cement.view.products.price.bottom.NewServices',
    alias: 'widget.goods_auto_bottom_incoming_services',
    gridStore: 'goods.auto.bottom.incoming.Services',
    gridStateId: 'stateGoodsAutoBottomIncomingServices'
});