Ext.define('Cement.view.goods.auto.bottom.outcoming.Services', {
    extend: 'Cement.view.products.price.bottom.NewServices',
    alias: 'widget.goods_auto_bottom_outcoming_services',
    gridStore: 'goods.auto.bottom.outcoming.Services',
    gridStateId: 'stateGoodsAutoBottomOutcomingServices'
});