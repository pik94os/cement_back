Ext.define('Cement.view.goods.auto.bottom.new_r.Payers', {
	extend: 'Cement.view.goods.auto.bottom.new_r.Senders',
	alias: 'widget.goods_auto_bottom_new_payers',
  gridStore: 'goods.auto.bottom.new_r.Payers',
  gridStateId: 'stateGoodsAutoBottomNewPayers',
  showFilterButton: false,

  printItemUrl: Cement.Config.url.goods.payers.printItem,
  printUrl: Cement.Config.url.goods.payers.printGrid,
  helpUrl: Cement.Config.url.goods.payers.help
});