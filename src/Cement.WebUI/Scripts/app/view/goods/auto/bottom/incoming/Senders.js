Ext.define('Cement.view.goods.auto.bottom.incoming.Senders', {
	extend: 'Cement.view.goods.auto.bottom.new_r.Senders',
	alias: 'widget.goods_auto_bottom_incoming_senders',
  gridStore: 'goods.auto.bottom.incoming.Senders',
  gridStateId: 'stateGoodsAutoBottomIncomingSenders',
  showFilterButton: false,
  autoLoadStore: false,

  printItemUrl: Cement.Config.url.goods.senders.printItem,
  printUrl: Cement.Config.url.goods.senders.printGrid,
  helpUrl: Cement.Config.url.goods.senders.help
});