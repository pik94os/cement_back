Ext.define('Cement.view.goods.auto.bottom.outcoming.Getters', {
	extend: 'Cement.view.goods.auto.bottom.new_r.Senders',
	alias: 'widget.goods_auto_bottom_outcoming_getters',
  gridStore: 'goods.auto.bottom.outcoming.Getters',
  gridStateId: 'stateGoodsAutoBottomOutcomingGetters',
  showFilterButton: false,

  printItemUrl: Cement.Config.url.goods.getters.printItem,
  printUrl: Cement.Config.url.goods.getters.printGrid,
  helpUrl: Cement.Config.url.goods.getters.help
});