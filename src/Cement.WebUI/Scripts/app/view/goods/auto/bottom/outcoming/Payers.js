Ext.define('Cement.view.goods.auto.bottom.outcoming.Payers', {
	extend: 'Cement.view.goods.auto.bottom.template.Senders',
	alias: 'widget.goods_auto_bottom_outcoming_payers',
  gridStore: 'goods.auto.bottom.outcoming.Payers',
  gridStateId: 'stateGoodsAutoBottomOutcomingPayers',
  showFilterButton: false,

  printItemUrl: Cement.Config.url.goods.payers.printItem,
  printUrl: Cement.Config.url.goods.payers.printGrid,
  helpUrl: Cement.Config.url.goods.payers.help
});