Ext.define('Cement.view.goods.auto.bottom.new_r.Routes', {
    extend: 'Cement.view.office_work.correspondention.bottom.new_c.Routes',
    alias: 'widget.goods_auto_bottom_new_routes',
    gridStore: 'goods.auto.bottom.new_r.Routes',
    gridStateId: 'stateGoodsAutoBottomNewRoutes'
});