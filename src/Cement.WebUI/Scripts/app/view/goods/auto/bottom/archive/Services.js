Ext.define('Cement.view.goods.auto.bottom.archive.Services', {
    extend: 'Cement.view.products.price.bottom.NewServices',
    alias: 'widget.goods_auto_bottom_archive_services',
    gridStore: 'goods.auto.bottom.archive.Services',
    gridStateId: 'stateGoodsAutoBottomArchiveServices'
});