Ext.define('Cement.view.goods.auto.bottom.TransportUnits', {
    extend: 'Cement.view.clients.bottom.TransportUnits',
    alias: 'widget.goods_auto_bottom_transportunits',
    gridStore: 'goods.auto.bottom.TransportUnits',
    showFilterButton: false,

    printItemUrl: Cement.Config.url.goods.transport_units.printItem,
    printUrl: Cement.Config.url.goods.transport_units.printGrid,
    helpUrl: Cement.Config.url.goods.transport_units.help
});