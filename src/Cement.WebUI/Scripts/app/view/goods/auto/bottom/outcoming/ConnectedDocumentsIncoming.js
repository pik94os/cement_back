Ext.define('Cement.view.goods.auto.bottom.outcoming.ConnectedDocumentsIncoming', {
	extend: 'Cement.view.goods.auto.lists.ConnectedDocuments',
	alias: 'widget.goods_auto_bottom_outcoming_connected_documents_incoming',

	gridStore: 'goods.auto.bottom.outcoming.ConnectedDocumentsIncoming',
	gridStateId: 'stateGoodsAutoBottomOutcomingConnectedDocumentsIncoming',

    printUrl: Cement.Config.url.goods.auto.documents.printGrid,
    helpUrl: Cement.Config.url.goods.auto.documents.help
});