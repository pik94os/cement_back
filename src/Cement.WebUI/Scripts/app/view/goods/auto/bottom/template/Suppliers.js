Ext.define('Cement.view.goods.auto.bottom.template.Suppliers', {
	extend: 'Cement.view.goods.auto.bottom.template.Senders',
	alias: 'widget.goods_auto_bottom_template_suppliers',
  gridStore: 'goods.auto.bottom.template.Suppliers',
  gridStateId: 'stateGoodsAutoBottomTemplateSuppliers',
  showFilterButton: false,

  printItemUrl: Cement.Config.url.goods.suppliers.printItem,
  printUrl: Cement.Config.url.goods.suppliers.printGrid,
  helpUrl: Cement.Config.url.goods.suppliers.help
});