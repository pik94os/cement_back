Ext.define('Cement.view.goods.auto.bottom.new_r.Products', {
    extend: 'Cement.view.products.price.bottom.NewProducts',
    alias: 'widget.goods_auto_bottom_new_products',
    gridStore: 'goods.auto.bottom.new_r.Products',
    gridStateId: 'stateGoodsAutoBottomNewProducts'
});