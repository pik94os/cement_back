Ext.define('Cement.view.goods.auto.bottom.outcoming.Products', {
    extend: 'Cement.view.products.price.bottom.NewProducts',
    alias: 'widget.goods_auto_bottom_outcoming_products',
    gridStore: 'goods.auto.bottom.outcoming.Products',
    gridStateId: 'stateGoodsAutoBottomOutcomingProducts'
});