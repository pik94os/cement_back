Ext.define('Cement.view.goods.auto.bottom.new_r.Getters', {
	extend: 'Cement.view.goods.auto.bottom.new_r.Senders',
	alias: 'widget.goods_auto_bottom_new_getters',
  gridStore: 'goods.auto.bottom.new_r.Getters',
  gridStateId: 'stateGoodsAutoBottomNewGetters',
  showFilterButton: false,

  printItemUrl: Cement.Config.url.goods.getters.printItem,
  printUrl: Cement.Config.url.goods.getters.printGrid,
  helpUrl: Cement.Config.url.goods.getters.help
});