Ext.define('Cement.view.goods.auto.bottom.archive.Routes', {
    extend: 'Cement.view.goods.auto.bottom.new_r.Routes',
    alias: 'widget.goods_auto_bottom_archive_routes',
    gridStore: 'goods.auto.bottom.archive.Routes',
    gridStateId: 'stateGoodsAutoBottomArchiveRoutes'
});