Ext.define('Cement.view.goods.auto.bottom.template.Routes', {
    extend: 'Cement.view.products.price.bottom.NewRoutes',
    alias: 'widget.goods_auto_bottom_template_routes',
    gridStore: 'goods.auto.bottom.template.Routes',
    gridStateId: 'stateGoodsAutoBottomTemplateRoutes'
});