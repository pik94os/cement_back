Ext.define('Cement.view.goods.auto.bottom.new_r.Suppliers', {
	extend: 'Cement.view.goods.auto.bottom.new_r.Senders',
	alias: 'widget.goods_auto_bottom_new_suppliers',
  gridStore: 'goods.auto.bottom.new_r.Suppliers',
  gridStateId: 'stateGoodsAutoBottomNewSuppliers',
  showFilterButton: false,

  printItemUrl: Cement.Config.url.goods.suppliers.printItem,
  printUrl: Cement.Config.url.goods.suppliers.printGrid,
  helpUrl: Cement.Config.url.goods.suppliers.help
});