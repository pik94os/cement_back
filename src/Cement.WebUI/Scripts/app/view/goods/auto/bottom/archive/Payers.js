Ext.define('Cement.view.goods.auto.bottom.archive.Payers', {
	extend: 'Cement.view.goods.auto.bottom.template.Senders',
	alias: 'widget.goods_auto_bottom_archive_payers',
  gridStore: 'goods.auto.bottom.archive.Payers',
  gridStateId: 'stateGoodsAutoBottomArchivePayers',
  showFilterButton: false,

  printItemUrl: Cement.Config.url.goods.payers.printItem,
  printUrl: Cement.Config.url.goods.payers.printGrid,
  helpUrl: Cement.Config.url.goods.payers.help
});