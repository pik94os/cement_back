Ext.define('Cement.view.goods.auto.bottom.new_r.ConnectedDocumentsIncoming', {
	extend: 'Cement.view.goods.auto.lists.ConnectedDocuments',
	alias: 'widget.goods_auto_bottom_new_connected_documents_incoming',

	gridStore: 'goods.auto.bottom.new_r.ConnectedDocumentsIncoming',
	gridStateId: 'stateGoodsAutoBottomNewConnectedDocumentsIncoming',

    printUrl: Cement.Config.url.goods.auto.documents.printGrid,
    helpUrl: Cement.Config.url.goods.auto.documents.help
});