Ext.define('Cement.view.goods.auto.bottom.incoming.Suppliers', {
	extend: 'Cement.view.goods.auto.bottom.template.Senders',
	alias: 'widget.goods_auto_bottom_incoming_suppliers',
  gridStore: 'goods.auto.bottom.incoming.Suppliers',
  gridStateId: 'stateGoodsAutoBottomIncomingSuppliers',
  showFilterButton: false,

  printItemUrl: Cement.Config.url.goods.suppliers.printItem,
  printUrl: Cement.Config.url.goods.suppliers.printGrid,
  helpUrl: Cement.Config.url.goods.suppliers.help
});