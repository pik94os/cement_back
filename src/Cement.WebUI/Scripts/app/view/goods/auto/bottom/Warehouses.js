Ext.define('Cement.view.goods.auto.bottom.Warehouses', {
    extend: 'Cement.view.corporate.warehouse.List',
    gridStore: 'goods.auto.bottom.Warehouses',
    //gridStateId: 'stateClientcorporateWarehousesList',
    xtype: 'goods_auto_bottom_warehouses',
    cls: '',
    showBottomBar: false,
    autoLoadStore: false,
    getToolbarItems: function () { return null; },
    getGridFeatures: function () { return null; }
});