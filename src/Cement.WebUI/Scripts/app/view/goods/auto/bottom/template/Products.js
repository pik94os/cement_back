Ext.define('Cement.view.goods.auto.bottom.template.Products', {
    extend: 'Cement.view.products.price.bottom.NewProducts',
    alias: 'widget.goods_auto_bottom_template_products',
    gridStore: 'goods.auto.bottom.template.Products',
    gridStateId: 'stateGoodsAutoBottomTemplateProducts'
});