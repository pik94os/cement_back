Ext.define('Cement.view.goods.auto.bottom.archive.Suppliers', {
	extend: 'Cement.view.goods.auto.bottom.template.Senders',
	alias: 'widget.goods_auto_bottom_archive_suppliers',
  gridStore: 'goods.auto.bottom.archive.Suppliers',
  gridStateId: 'stateGoodsAutoBottomArchiveSuppliers',
  showFilterButton: false,

  printItemUrl: Cement.Config.url.goods.suppliers.printItem,
  printUrl: Cement.Config.url.goods.suppliers.printGrid,
  helpUrl: Cement.Config.url.goods.suppliers.help
});