Ext.define('Cement.view.goods.auto.bottom.incoming.Routes', {
    extend: 'Cement.view.goods.auto.bottom.new_r.Routes',
    alias: 'widget.goods_auto_bottom_incoming_routes',
    gridStore: 'goods.auto.bottom.incoming.Routes',
    gridStateId: 'stateGoodsAutoBottomIncomingRoutes'
});