Ext.define('Cement.view.goods.auto.bottom.outcoming.Routes', {
    extend: 'Cement.view.goods.auto.bottom.new_r.Routes',
    alias: 'widget.goods_auto_bottom_outcoming_routes',
    gridStore: 'goods.auto.bottom.outcoming.Routes',
    gridStateId: 'stateGoodsAutoBottomOutcomingRoutes'
});