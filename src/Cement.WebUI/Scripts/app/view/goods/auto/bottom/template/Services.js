Ext.define('Cement.view.goods.auto.bottom.template.Services', {
    extend: 'Cement.view.products.price.bottom.NewServices',
    alias: 'widget.goods_auto_bottom_template_services',
    gridStore: 'goods.auto.bottom.template.Services',
    gridStateId: 'stateGoodsAutoBottomTemplateServices'
});