Ext.define('Cement.view.goods.auto.bottom.template.Payers', {
	extend: 'Cement.view.goods.auto.bottom.template.Senders',
	alias: 'widget.goods_auto_bottom_template_payers',
  gridStore: 'goods.auto.bottom.template.Payers',
  gridStateId: 'stateGoodsAutoBottomTemplatePayers',
  showFilterButton: false,

  printItemUrl: Cement.Config.url.goods.payers.printItem,
  printUrl: Cement.Config.url.goods.payers.printGrid,
  helpUrl: Cement.Config.url.goods.payers.help
});