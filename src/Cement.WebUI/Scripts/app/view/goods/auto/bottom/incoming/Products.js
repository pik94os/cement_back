Ext.define('Cement.view.goods.auto.bottom.incoming.Products', {
    extend: 'Cement.view.products.price.bottom.NewProducts',
    alias: 'widget.goods_auto_bottom_incoming_products',
    gridStore: 'goods.auto.bottom.incoming.Products',
    gridStateId: 'stateGoodsAutoBottomIncomingProducts'
});