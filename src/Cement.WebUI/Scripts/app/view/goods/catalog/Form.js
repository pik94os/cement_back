Ext.define('Cement.view.goods.catalog.Form', {
	extend: 'Cement.view.basic.Form',
	alias: 'widget.goods_catalog_form',
	fieldsDisabled: false,
	url: Cement.Config.url.goods.catalog.saveUrl,
	method: Cement.Config.url.goods.catalog.saveMethod,
	isFilter: false,
	layout: 'fit',
	allowBlankFields: true,
	showSaveAndSignButton: true,
	saveButtonDisabled: false,

	getItems: function () {
		return [{
                xtype: 'fieldset',
                border: 0, 
                margin: 0,
                defaults: {
                    xtype: 'textfield',
                    anchor: '100%',
                    labelWidth: 200,
                    labelAlign: 'right'
                },
                items: [
        			this.getSelectorField('Прайс-лист', 'p_price', 'show_price_window'), {
        			    xtype: 'textarea',
        				fieldLabel: 'Комментарий',
        				name: 'p_comment',
        				height: 150
        			}
            	]
            }
        ];
	},

	initComponent: function () {
		this.callParent(arguments);

		this.down('button[action=show_price_window]').on('click', function () {
			this.createPriceWindow();
			this.priceWindow.show();
		}, this);
	},

	createPriceWindow: function () {
		if (!this.priceWindow) {
			this.priceWindow = Ext.create('Cement.view.goods.catalog.PricelistsWindow');
			this.priceWindow.on('priceselected', function (rec) {
				this.down('textfield[name=p_price_display]').setValue(rec.get('p_name'));
				this.down('hiddenfield[name=p_price]').setValue(rec.get('id'));
				this.priceWindow.hide();
			}, this);
		}
	}
});