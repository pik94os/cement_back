Ext.define('Cement.view.goods.catalog.Services', {
	extend: 'Cement.view.goods.catalog.Products',
	alias: 'widget.goods_catalog_services',

	gridStore: 'goods.catalog.Services',
    gridStateId: 'stateGoodsCatalogServices',

    printUrl: Cement.Config.url.goods.catalog.services.printGrid,
    helpUrl: Cement.Config.url.goods.catalog.services.help,
    printItemUrl: Cement.Config.url.goods.catalog.services.printItem,
    bbarText: 'Показаны услуги {0} - {1} из {2}',
    bbarEmptyMsg: 'Нет услуг',
    bbarUsersText: 'Услуг на странице: ',

    createRequest: function (grid, record) {
        var rec = Ext.create('Cement.model.goods.Service');
        rec.set('p_service_display', record.get('p_name'));
        rec.set('p_service', record.get('id'));
        grid.up('basicgrid').fireEvent('edititem', rec, rec.$className);
    }
});