Ext.define('Cement.view.goods.catalog.Products', {
	extend: 'Cement.view.basic.Grid',
	alias: 'widget.goods_catalog_products',

	printUrl: Cement.Config.url.products.products.printGrid,
    helpUrl: Cement.Config.url.products.products.help,
    deleteUrl: Cement.Config.url.products.products.deleteUrl,
    printItemUrl: Cement.Config.url.products.products.printItem,
    // cls: 'simple-form',
    bbarText: 'Показаны товары {0} - {1} из {2}',
    bbarEmptyMsg: 'Нет товаров',
    bbarUsersText: 'Товаров на странице: ',

	gridStore: 'goods.catalog.Products',
    gridStateId: 'stateGoodsCatalogProducts',

	gridViewConfig: {
        getRowClass: function(record, index, rowParams, store) {
            return record.get('p_complect').length == 0 ? 'expander-hidden' : '';
        }
    },

    createItem: function () {
        this.fireEvent('createitem', 'Cement.model.goods.Catalog');
    },

    getGridPlugins: function () {
        return [{
            ptype: 'rowexpander',
            rowBodyTpl : new Ext.XTemplate(
                '<p>{p_text}</p>'
            )
        }];
    },

    getFilterItems: function () {
        return [{
            text: 'Наименование',
            kind: 'selector',
            field_name: 'p_name',
            checked: true
        }, {
            text: 'Поставщик',
            kind: 'selector',
            field_name: 'p_supplier',
            checked: true
        }, {
            text: 'Договор',
            kind: 'selector',
            field_name: 'p_contract',
            checked: true
        }, {
            text: 'Группа',
            kind: 'selector',
            field_name: 'p_group',
            checked: true
        }, {
            text: 'Подгруппа',
            kind: 'selector',
            field_name: 'p_subgroup',
            checked: true
        }, {
            text: 'Торговая марка',
            kind: 'selector',
            field_name: 'p_trade_mark',
            checked: true
        }, {
            text: 'Производитель',
            kind: 'selector',
            field_name: 'p_manufacturer',
            checked: true
        }, {
            text: 'Ед. изм.',
            kind: 'selector',
            field_name: 'p_product_unit',
            checked: true
        }, {
            text: 'Цена',
            kind: 'selector',
            field_name: 'p_price',
            checked: true
        }, {
            text: 'Налог',
            kind: 'selector',
            field_name: 'p_tax',
            checked: true
        }];
    },

	getGridColumns: function () {
	    var result = [
            { text: 'Наименование', dataIndex: 'p_name', locked: true, width: 200 },
            { text: 'Поставщик', dataIndex: 'p_supplier_display', width: 150 },
            { text: 'Договор', dataIndex: 'p_contract_display', width: 150 },
            { text: 'Группа', dataIndex: 'p_group_display', width: 150 },
            { text: 'Подгруппа', dataIndex: 'p_subgroup_display', width: 150 },
            { text: 'Торговая марка', dataIndex: 'p_trade_mark_display', width: 150 },
            { text: 'Производитель', dataIndex: 'p_manufacturer_display', width: 150 },
            { text: 'Ед. изм.', dataIndex: 'p_product_unit_display', width: 70 },
            { text: 'Цена', dataIndex: 'p_price', width: 70 },
            { text: 'Налог', dataIndex: 'p_tax', width: 70 }
        ];

        return this.mergeActions(result);
    },

    getActionColumns: function () {
        return {
            xtype: 'rowactions',
            hideable: false,
            locked: true,
            resizeable: false,
            width: 46,
            actions: [{
                iconCls: 'icon-add-item', 
                qtip: 'Создать заявку', 
                callback: this.createRequest
            }],
            keepSelection: true
        };
    },

    createRequest: function (grid, record) {
        var rec = Ext.create('Cement.model.goods.Auto');
        rec.set('p_product_display', record.get('p_name'));
        rec.set('p_product', record.get('id'));
        grid.up('basicgrid').fireEvent('edititem', rec, rec.$className);
    },

    initComponent: function () {
        this.callParent(arguments);

        var columns = this.items.items[0].columns;
        var height = this.fixedColumnHeaderHeight;

        if (this.overrideColumnHeaderHeight) {
            Ext.each(columns, function (item) { item.height = height; });
        }
    }
});