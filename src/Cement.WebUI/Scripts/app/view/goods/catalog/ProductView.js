Ext.define('Cement.view.goods.catalog.ProductView', {
	extend: 'Cement.view.products.products.View',
	alias: 'widget.goods_catalog_product_view',

	getItems: function () {
		this.fieldsDisabled = true;
		return [{
				xtype: 'fieldset',
				border: 0,
				margin: 0,
				// padding: '0 110',
				defaults: {
					xtype: 'textfield',
					anchor: '100%'
				},
				items: this.getBasicFieldSet()
            }, {
                xtype: 'fieldset',
                collapsible: true,
                collapsed: true,
                title: 'Происхождение',
                defaults: {
                    xtype: 'textfield',
                    anchor: '100%',
                    labelWidth: 110
                },
                items: this.getFromFields()
            }, {
                xtype: 'fieldset',
                collapsible: true,
                collapsed: true,
                title: 'Коды',
                defaults: {
                    xtype: 'textfield',
                    anchor: '100%',
                    labelWidth: 110
                },
                items: this.getCodeFieldSet()
            }, {
                xtype: 'fieldset',
                collapsible: true,
                collapsed: true,
                title: 'Технические условия',
                defaults: {
                    xtype: 'textfield',
                    anchor: '100%',
                    labelWidth: 110
                },
                items: this.getTechnicalFieldSet()
            }, {
                xtype: 'fieldset',
                collapsible: true,
                collapsed: true,
                title: 'Сертификаты',
                defaults: {
                    xtype: 'textfield',
                    anchor: '100%',
                    labelWidth: 110
                },
                items: this.getCertificatesFieldSet()
            }, {
                xtype: 'fieldset',
                collapsible: true,
                collapsed: true,
                title: 'Класс опасности (ADR)',
                defaults: {
                    xtype: 'textfield',
                    anchor: '100%',
                    labelWidth: 110
                },
                items: this.getDangerFieldSet()
            }, this.getPacking(), {
                xtype: 'fieldset',
                collapsible: true,
                collapsed: true,
                title: 'Стоимость',
                defaults: {
                    xtype: 'textfield',
                    anchor: '100%',
                    labelWidth: 110
                },
                items: [{
                	name: 'p_tax',
                	fieldLabel: 'Налог',
                	disabled: true
                }, {
                	name: 'p_price',
                	fieldLabel: 'Цена',
                	disabled: true
                }]
            }
        ];
	},

	initComponent: function ()  {
		Ext.apply(this, {
			fieldsDisabled: true,
			items: [{
				xtype: 'panel',
				border: 0,
				layout: 'anchor',
				defaultType: 'textfield',
				anchor: '100%',
				bodyPadding: '10 10 10 5',
				autoScroll: true,
				defaults: {
					anchor: '100%',
					labelWidth: 110
				},
				items: this.getItems()
			}]
		});
		this.callParent(arguments);
	}
});