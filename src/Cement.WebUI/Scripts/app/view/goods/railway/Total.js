Ext.define('Cement.view.goods.railway.Total', {
	extend: 'Cement.view.basic.Tabs',
	title: 'Заявки на товар (ЖД)',

	items: [{
		xtype: 'goods_railway_complex_templates',
		title: 'Шаблоны'
	}, {
		xtype: 'goods_railway_complex_new',
		title: 'Новые'
	}, {
		xtype: 'goods_railway_complex_incoming',
		title: 'Входящие'
	}, {
		xtype: 'goods_railway_complex_outcoming',
		title: 'Исходящие'
	}, {
		xtype: 'goods_railway_complex_archive',
		title: 'Архив'
	}],

	initComponent: function () {
		Ext.apply(this, {
			title: 'Заявки на товар (ЖД)'
		});
		this.callParent(arguments);
	}
});