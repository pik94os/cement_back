Ext.define('Cement.view.goods.railway.form.Form', {
	extend: 'Cement.view.basic.Form',
	alias: 'widget.goods_railway_form_form',
	fieldsDisabled: false,
	url: Cement.Config.url.goods.railway.new_r.saveUrl,
	method: Cement.Config.url.goods.railway.new_r.saveMethod,
	printUrl: Cement.Config.url.goods.railway.new_r.printItem,
	isFilter: false,
	layout: 'fit',
	saveButtonDisabled: false,
	showSaveAndSignButton: true,

	importancesStoreId: 'goods.auxiliary.Importances',
	securitiesStoreId: 'goods.auxiliary.Securities',

	getItems: function () {
		return [{
				xtype: 'fieldset',
				border: 0,
				margin: 0,
				padding: '0 110',
				defaults: {
					xtype: 'textfield',
					anchor: '100%',
					labelAlign: 'right'
				},
				items: [{
					name: 'p_number',
					fieldLabel: 'Номер'
				}, {
					name: 'p_date',
					fieldLabel: 'Дата',
					xtype: 'datefield'
				}]
            }, {
                xtype: 'fieldset',
                title: 'Общие данные',
                collapsed: false,
                collapsible: true,
                defaults: {
                    xtype: 'textfield',
                    anchor: '100%',
                    labelWidth: 200,
                    labelAlign: 'right'
                },
                items: [{
                	xtype: 'checkbox',
                	name: 'p_no_request',
                  inputValue: 'true',
                  uncheckedValue: 'false',
                	fieldLabel: 'Нет заявки'
                },
                	this.getSelectorField('Заявка', 'p_request', 'show_request_window'), {
                	xtype: 'checkbox',
                	name: 'p_no_template',
                  inputValue: 'true',
                  uncheckedValue: 'false',
                	fieldLabel: 'Нет шаблона'
                },
                	this.getSelectorField('Шаблон', 'p_template', 'show_template_window'), {
                		xtype: 'radiogroup',
						fieldLabel: 'Способ отгрузки',
				        combineErrors: true,
				        msgTarget : 'side',
				        role: 'p_load_kind',
				        columns: 1,
				        margin: '0 0 3 0',
				        defaults: {
				            hideLabel: false
				        },
				        items: [{
				          	xtype: 'radio',
				          	name: 'p_load_kind',
				          	role: 'self',
				          	boxLabel: 'Самовывоз',
				          	value: Cement.Config.goods.load_kinds.self,
				          	inputValue: Cement.Config.goods.load_kinds.self,
				          	margin: '0'
				        }, {
				          	xtype: 'radio',
				          	role: 'centr',
				          	name: 'p_load_kind',
				          	boxLabel: 'Централизация',
				          	value: Cement.Config.goods.load_kinds.centr,
				          	inputValue: Cement.Config.goods.load_kinds.centr,
				          	margin: '0'
				        }]
                	}, {
                		fieldLabel: 'Дата, время загрузки',
                		xtype: 'fieldcontainer',
                		layout: 'hbox',
                		role: 'p_load_date',
                		items: [{
                			name: 'p_load_date',
                			xtype: 'datefield',
                			flex: 1,
                			margin: '0 5 0 0'
                		}, {
                			name: 'p_load_time',
                			xtype: 'timefield',
                			flex: 1,
                			margin: '0 0 0 5'
                		}]
                	}, {
                		fieldLabel: 'Дата, время выгрузки',
                		xtype: 'fieldcontainer',
                		layout: 'hbox',
                		role: 'p_unload_date',
                		items: [{
                			name: 'p_unload_date',
                			xtype: 'datefield',
                			flex: 1,
                			margin: '0 5 0 0'
                		}, {
                			name: 'p_unload_time',
                			xtype: 'timefield',
                			flex: 1,
                			margin: '0 0 0 5'
                		}]
                	}
                ]
            }, {
                xtype: 'fieldset',
                title: 'Поставщик',
                collapsed: false,
                collapsible: true,
                role: 'supplier',
                defaults: {
                    xtype: 'textfield',
                    anchor: '100%',
                    labelWidth: 200,
                    labelAlign: 'right'
                },
                items: [{
	            	xtype: 'checkbox',
	            	fieldLabel: 'Нет поставщика',
	            	name: 'p_no_supplier',
                inputValue: 'true',
                uncheckedValue: 'false'
	            },
            	this.getSelectorField('Наименование', 'p_supplier', 'show_supplier_window'), {
            		name: 'p_supplier_address',
            		fieldLabel: 'Адрес',
            		disabled: true
            	},
            	this.getSelectorField('Контактное лицо', 'p_supplier_contact', 'show_supplier_contact_window'), {
            		name: 'p_supplier_work_time',
            		fieldLabel: 'Рабочее время',
            		disabled: true
                }]
            }, {
            	xtype: 'fieldset',
            	title: 'Товар',
            	collapsed: false,
            	collapsible: true,
            	role: 'product',
            	defaults: {
                    xtype: 'textfield',
                    anchor: '100%',
                    labelWidth: 200,
                    labelAlign: 'right'
                },
                items: [{
                	name: 'p_contract',
                	xtype: 'hiddenfield'
                }, {
                	name: 'p_contract_display',
                	fieldLabel: 'Договор',
                	disabled: true
                },
                this.getSelectorField('Наименование', 'p_product', 'show_product_window'), {
                	name: 'p_product_unit',
                	fieldLabel: 'Единица измерения'
                }, {
                	name: 'p_product_count',
                	fieldLabel: 'Количество'
                }, {
                	name: 'p_product_price',
                	fieldLabel: 'Цена',
                	disabled: true
                }]
            }, {
            	xtype: 'fieldset',
            	title: 'Транспорт',
            	collapsed: false,
            	collapsible: true,
            	role: 'transport_unit',
            	defaults: {
                    xtype: 'textfield',
                    anchor: '100%',
                    labelWidth: 200,
                    labelAlign: 'right'
                },
                items: [{
                	name: 'p_carriage_kind',
                	fieldLabel: 'Род вагона'
                }]
            }, {
                xtype: 'fieldset',
                title: 'Грузополучатель',
                collapsed: false,
                collapsible: true,
                role: 'getter',
                defaults: {
                    xtype: 'textfield',
                    anchor: '100%',
                    labelWidth: 200,
                    labelAlign: 'right'
                },
                items: [
            	this.getSelectorField('Наименование', 'p_getter', 'show_getter_window'), {
            		name: 'p_getter_address',
            		fieldLabel: 'Адрес',
            		disabled: true
            	}, {
            		name: 'p_getter_warehouse_code',
            		disabled: true,
            		fieldLabel: 'Код предприятия'
            	}, {
            		name: 'p_getter_warehouse_station',
            		disabled: true,
            		fieldLabel: 'Станция'
            	}, {
            		name: 'p_getter_warehouse_road',
            		disabled: true,
            		fieldLabel: 'Дорога'
            	},
            	this.getSelectorField('Контактное лицо', 'p_getter_contact', 'show_getter_contact_window'), {
            		name: 'p_getter_work_time',
            		fieldLabel: 'Рабочее время',
            		disabled: true
                }]
            }, {
                xtype: 'fieldset',
                title: 'Плательщик',
                collapsed: false,
                collapsible: true,
                role: 'payer',
                defaults: {
                    xtype: 'textfield',
                    anchor: '100%',
                    labelWidth: 200,
                    labelAlign: 'right'
                },
                items: [{
	            	xtype: 'checkbox',
	            	fieldLabel: 'Совпадает с грузополучателем',
	            	name: 'p_payer_as_getter',
                inputValue: 'true',
                uncheckedValue: 'false'
	            },
            	this.getSelectorField('Наименование', 'p_payer', 'show_payer_window'), {
            		name: 'p_payer_address',
            		fieldLabel: 'Адрес',
            		disabled: true
            	},
            	this.getSelectorField('Контактное лицо', 'p_payer_contact', 'show_payer_contact_window'), {
            		name: 'p_payer_work_time',
            		fieldLabel: 'Рабочее время',
            		disabled: true
                }]
            }, {
                xtype: 'fieldset',
                title: 'Дополнительные данные',
                collapsed: false,
                collapsible: true,
                defaults: {
                    xtype: 'textfield',
                    anchor: '100%',
                    labelWidth: 200,
                    labelAlign: 'right'
                },
                items: [
                	this.getCombo('Важность', 'p_importance', this.importancesStoreId),
                	this.getCombo('Секретность', 'p_security', this.securitiesStoreId), {
	            	fieldLabel: 'Примечание',
	            	name: 'p_comment',
	            	xtype: 'textarea',
	            	height: 150
	            }]
            }
        ];
	},

	setTransportUnit: function () {
		if (this.down('checkbox[name=p_no_request]').getValue() &&
			this.down('checkbox[name=p_no_template]').getValue()) {
			this.down('fieldset[role=transport_unit]').enable();
		}
		else {
			this.down('fieldset[role=transport_unit]').disable();
		}
	},

	enableFieldsets: function () {
		this.down('fieldset[role=supplier]').enable();
		this.down('fieldset[role=getter]').enable();
		this.down('fieldset[role=payer]').enable();
	},

	initComponent: function () {
		this.callParent(arguments);
		this.down('checkbox[name=p_no_request]').on('change', function (control, newValue){
			if (newValue) {
				this.down('fieldcontainer[role=container_p_request]').disable();
			}
			else {
				this.down('fieldcontainer[role=container_p_request]').enable();
			}
			this.enableFieldsets();
			this.setTransportUnit();
		}, this);
		this.down('checkbox[name=p_no_template]').on('change', function (control, newValue){
			if (newValue) {
				this.down('fieldcontainer[role=container_p_template]').disable();
			}
			else {
				this.down('fieldcontainer[role=container_p_template]').enable();
			}
			this.enableFieldsets();
			this.setTransportUnit();
		}, this);
		this.down('checkbox[name=p_no_supplier]').on('change', function (control, newValue) {
			if (newValue) {
				this.down('hiddenfield[name=p_contract]').setValue('');
				this.down('textfield[name=p_contract_display]').setValue('');
				this.down('fieldcontainer[role=container_p_supplier]').disable();
				this.down('fieldcontainer[role=container_p_supplier_contact]').disable();
			}
			else {
				this.down('fieldcontainer[role=container_p_supplier]').enable();
				// this.down('fieldcontainer[role=container_p_supplier_contact]').enable();
			}
		}, this);
		this.down('checkbox[name=p_payer_as_getter]').on('change', function (control, newValue) {
			if (newValue) {
				this.down('fieldcontainer[role=container_p_payer]').disable();
				this.down('fieldcontainer[role=container_p_payer_contact]').disable();
				this.down('hiddenfield[name=p_payer]').setValue(this.down('hiddenfield[name=p_getter]').getValue());
				this.down('textfield[name=p_payer_display]').setValue(this.down('textfield[name=p_getter_display]').getValue());
				this.down('textfield[name=p_payer_address]').setValue(this.down('textfield[name=p_getter_address]').getValue());
				this.down('textfield[name=p_payer_contact_display]').setValue(this.down('textfield[name=p_getter_contact_display]').getValue());
				this.down('hiddenfield[name=p_payer_contact]').setValue(this.down('hiddenfield[name=p_getter_contact]').getValue());
				this.down('textfield[name=p_payer_work_time]').setValue(this.down('textfield[name=p_getter_work_time]').getValue());
			}
			else {
				this.down('fieldcontainer[role=container_p_payer]').enable();
				this.down('fieldcontainer[role=container_p_payer_contact]').enable();
			}
		}, this);
		this.down('fieldcontainer[role=container_p_supplier_contact]').disable();
		this.down('fieldcontainer[role=container_p_getter_contact]').disable();
		this.down('fieldcontainer[role=container_p_payer_contact]').disable();
		this.down('button[action=show_request_window]').on('click', function () {
			this.createRequestWindow();
			this.requestWindow.show();
		}, this);
		this.down('button[action=show_template_window]').on('click', function () {
			this.createTemplateWindow();
			this.templateWindow.show();
		}, this);
		this.down('button[action=show_supplier_window]').on('click', function () {
			this.createSupplierWindow();
			this.supplierWindow.show();
		}, this);
		this.down('button[action=show_supplier_contact_window]').on('click', function () {
			this.createSupplierContactWindow();
			this.supplierContactWindow.show();
		}, this);
		this.down('button[action=show_getter_window]').on('click', function () {
			this.createGetterWindow();
			this.getterWindow.show();
		}, this);
		this.down('button[action=show_getter_contact_window]').on('click', function () {
			this.createGetterContactWindow();
			this.getterContactWindow.show();
		}, this);
		this.down('button[action=show_payer_window]').on('click', function () {
			this.createPayerWindow();
			this.payerWindow.show();
		}, this);
		this.down('button[action=show_payer_contact_window]').on('click', function () {
			this.createPayerContactWindow();
			this.payerContactWindow.show();
		}, this);
		// this.down('button[action=show_carriage_window]').on('click', function () {
		// 	this.createTransportWindow();
		// 	this.transportWindow.show();
		// }, this);
		this.down('button[action=show_product_window]').on('click', function () {
			this.createProductWindow();
			this.productWindow.show();
		}, this);
	},

	createProductWindow: function () {
		if (!this.productWindow) {
			this.productWindow = Ext.create('Cement.view.goods.auto.form.ProductWindow');
			this.productWindow.on('productselected', function (rec) {
				this.down('textfield[name=p_product_display]').setValue(rec.get('p_name'));
				this.down('textfield[name=p_product_unit_display]').setValue(rec.get('p_product_unit_display'));
				this.down('hiddenfield[name=p_product]').setValue(rec.get('id'));
				this.productWindow.hide();
			}, this);
		}
	},

	// createTransportWindow: function () {
	// 	if (!this.transportWindow) {
	// 		this.transportWindow = Ext.create('Cement.view.goods.railway.form.CarriageWindow');
	// 		this.transportWindow.on('requestselected', function (rec) {
	// 			this.down('textfield[name=p_carriage_display]').setValue(rec.get('p_name'));
	// 			this.down('textfield[name=p_carriage_kind]').setValue(rec.get('p_kind'));
	// 			this.down('hiddenfield[name=p_carriage]').setValue(rec.get('id'));
	// 			this.transportWindow.hide();
	// 		}, this);
	// 	}
	// },

	createSupplierContactWindow: function () {
		if (!this.supplierContactWindow) {
			this.supplierContactWindow = Ext.create('Cement.view.goods.auto.form.SupplierContactWindow');
			this.supplierContactWindow.on('selected', function (rec) {
				this.down('textfield[name=p_supplier_contact_display]').setValue(rec.get('p_name'));
				this.down('hiddenfield[name=p_supplier_contact]').setValue(rec.get('id'));
				this.supplierContactWindow.hide();
			}, this);
		}
	},

	createGetterContactWindow: function () {
		if (!this.getterContactWindow) {
			this.getterContactWindow = Ext.create('Cement.view.goods.auto.form.GetterContactWindow');
			this.getterContactWindow.on('selected', function (rec) {
				this.down('textfield[name=p_getter_contact_display]').setValue(rec.get('p_name'));
				this.down('hiddenfield[name=p_getter_contact]').setValue(rec.get('id'));
				this.getterContactWindow.hide();
			}, this);
		}
	},

	createPayerContactWindow: function () {
		if (!this.payerContactWindow) {
			this.payerContactWindow = Ext.create('Cement.view.goods.auto.form.PayerContactWindow');
			this.payerContactWindow.on('selected', function (rec) {
				this.down('textfield[name=p_getter_contact_display]').setValue(rec.get('p_name'));
				this.down('hiddenfield[name=p_getter_contact]').setValue(rec.get('id'));
				this.payerContactWindow.hide();
			}, this);
		}
	},

	createSupplierWindow: function () {
		if (!this.supplierWindow) {
			this.supplierWindow = Ext.create('Cement.view.goods.auto.form.SupplierWindow');
			this.supplierWindow.on('selected', function (rec) {
				this.down('checkbox[name=p_no_supplier]').disable();
				this.down('textfield[name=p_supplier_display]').setValue(rec.get('p_supplier'));
				this.down('textfield[name=p_supplier_address]').setValue(rec.get('p_address'));
				this.down('textfield[name=p_supplier_work_time]').setValue(rec.get('p_work_time'));
				this.down('textfield[name=p_contract_display]').setValue(rec.get('p_name'));
				this.down('hiddenfield[name=p_contract]').setValue(rec.get('p_contract_id'));
				this.down('hiddenfield[name=p_supplier]').setValue(rec.get('id'));
				this.down('fieldcontainer[role=container_p_supplier_contact]').enable();
				this.down('hiddenfield[name=p_supplier_contact]').setValue('');
				this.down('textfield[name=p_supplier_contact_display]').setValue('');
				this.createSupplierContactWindow();
				this.supplierContactWindow.setFirm(rec.get('id'));
				this.supplierWindow.hide();
			}, this);
		}
	},

	createPayerWindow: function () {
		if (!this.payerWindow) {
			this.payerWindow = Ext.create('Cement.view.goods.auto.form.PayerWindow');
			this.payerWindow.on('selected', function (rec) {
				this.down('textfield[name=p_payer_display]').setValue(rec.get('p_name'));
				this.down('textfield[name=p_payer_address]').setValue(rec.get('p_address'));
				this.down('textfield[name=p_payer_work_time]').setValue(rec.get('p_work_time'));
				this.down('hiddenfield[name=p_payer]').setValue(rec.get('id'));
				this.createPayerContactWindow();
				this.payerContactWindow.setFirm(rec.get('id'));
				this.payerWindow.hide();
				this.down('fieldcontainer[role=container_p_payer_contact]').enable();
			}, this);
		}
	},

	createGetterWindow: function () {
		if (!this.getterWindow) {
			this.getterWindow = Ext.create('Cement.view.goods.railway.form.GetterWindow');
			this.getterWindow.on('selected', function (rec) {
				this.down('textfield[name=p_getter_display]').setValue(rec.get('p_name'));
				this.down('textfield[name=p_getter_address]').setValue(rec.get('p_address'));
				this.down('hiddenfield[name=p_getter]').setValue(rec.get('id'));
				this.down('textfield[name=p_getter_work_time]').setValue(rec.get('p_work_time'));
				this.down('textfield[name=p_getter_warehouse_code]').setValue(rec.get('p_warehouse_code'));
				this.down('textfield[name=p_getter_warehouse_station]').setValue(rec.get('p_warehouse_station'));
				this.down('textfield[name=p_getter_warehouse_road]').setValue(rec.get('p_warehouse_road'));
				this.createGetterContactWindow();
				this.getterContactWindow.setFirm(rec.get('id'));
				this.getterWindow.hide();
				this.down('fieldcontainer[role=container_p_getter_contact]').enable();
			}, this);
		}
	},

	createRequestWindow: function () {
		if (!this.requestWindow) {
			this.requestWindow= Ext.create('Cement.view.goods.auto.form.RequestWindow');
			this.requestWindow.on('requestselected', function (rec) {
				this.loadRecord(rec);
				this.down('textfield[name=p_request_display]').setValue('№' + rec.get('p_number') + ' от ' + rec.get('p_date'));
				this.down('hiddenfield[name=p_request]').setValue(rec.get('p_id'));
				this.down('radiogroup[role=p_load_kind]').setValue({
					'p_load_kind': rec.get('p_load_kind')
				});
				this.down('radio[role=centr]').enable();
				this.down('checkbox[name=p_no_request]').setValue(false);
				this.down('checkbox[name=p_no_request]').disable();
				this.down('checkbox[name=p_no_template]').setValue(false);
				this.down('checkbox[name=p_no_template]').disable();
				this.down('fieldcontainer[role=container_p_template]').disable();
				this.down('textfield[name=p_template_display]').setValue('');
				if (rec.get('p_load_kind') == Cement.Config.goods.load_kinds.self) {
					this.down('radio[role=centr]').disable();
				}
				this.down('fieldset[role=supplier]').enable();
				this.down('fieldset[role=transport_unit]').disable();
				this.down('fieldset[role=getter]').disable();
				this.down('fieldset[role=payer]').disable();
				this.down('fieldset[role=product]').disable();
				this.down('fieldcontainer[role=p_load_date]').disable();
				this.down('fieldcontainer[role=p_unload_date]').disable();
				this.createSupplierWindow();
				this.supplierWindow.setRequestId(rec.get('id'));
				this.setTransportUnit();
				this.requestWindow.hide();
			}, this);
		}
	},

	createTemplateWindow: function () {
		if (!this.templateWindow) {
			this.templateWindow= Ext.create('Cement.view.goods.auto.form.TemplateWindow');
			this.templateWindow.on('requestselected', function (rec) {
				this.loadRecord(rec);
				this.down('checkbox[name=p_no_template]').setValue(false);
				this.down('checkbox[name=p_no_template]').disable();
				this.down('checkbox[name=p_no_request]').disable();
				this.down('fieldcontainer[role=container_p_request]').disable();
				this.down('textfield[name=p_template_display]').setValue(rec.get('p_name'));
				this.down('hiddenfield[name=p_template]').setValue(rec.get('id'));
				this.setTransportUnit();
				this.down('fieldcontainer[role=container_p_product]').disable();
				this.down('radiogroup[role=p_load_kind]').disable();
				this.down('fieldset[role=supplier]').disable();
				this.down('fieldset[role=getter]').disable();
				this.down('fieldset[role=payer]').disable();
				this.templateWindow.hide();
			}, this);
		}
	},

	afterRecordLoad: function (rec) {
		this.createRequestWindow();
		this.createTemplateWindow();
		this.createSupplierWindow();
		this.createSupplierContactWindow();
		this.createGetterWindow();
		this.createGetterContactWindow();
		this.createPayerWindow();
		this.createPayerContactWindow();
		// this.createTransportWindow();
		this.createProductWindow();
		this.requestWindow.loadData([rec.get('p_request')]);
		this.templateWindow.loadData([rec.get('p_template')]);
		// this.supplierWindow.loadData([rec.get('p_supplier')]);
		this.supplierContactWindow.setFirm(rec.get('p_supplier'));
		// this.supplierContactWindow.loadData([rec.get('p_supplier_contact')]);
		// this.payerWindow.loadData([rec.get('p_payer')]);
		this.payerContactWindow.setFirm(rec.get('p_payer'));
		// this.getterWindow.loadData([rec.get('p_getter')]);
		this.getterContactWindow.setFirm(rec.get('p_getter'));
		// this.transportWindow.loadData([rec.get('p_transport_unit')]);
		// this.productWindow.loadData([rec.get('p_product')]);
	},

	reset: function () {
	    this.callParent(arguments);

		this.down('fieldset[role=supplier]').enable();
		this.down('fieldset[role=getter]').enable();
		this.down('fieldset[role=payer]').enable();
		this.down('fieldset[role=product]').enable();
		this.down('checkbox[name=p_no_request]').setValue(false);
		this.down('checkbox[name=p_no_request]').enable();
		this.down('checkbox[name=p_no_template]').setValue(false);
		this.down('checkbox[name=p_no_template]').enable();
		this.down('radio[role=self]').enable();
		this.down('radio[role=self_contract]').enable();
		this.down('radio[role=centr]').enable();
		this.down('fieldcontainer[role=p_load_date]').enable();
		this.down('fieldcontainer[role=p_unload_date]').enable();
		this.down('radiogroup[role=p_load_kind]').enable();
		this.down('fieldcontainer[role=container_p_product]').enable();
	}
});