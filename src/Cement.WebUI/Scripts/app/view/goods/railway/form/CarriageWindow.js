Ext.define('Cement.view.goods.railway.form.CarriageWindow', {
	extend: 'Cement.view.goods.auto.form.RequestWindow',
	xtype: 'goods_railway_form_carriage_window',
	title: 'Выбор вагона',
	layout: {
		type: 'vbox',
		align : 'stretch'
	},
	width: 900,
	height: 500,
	bodyPadding: 10,
	bbar: [{
		text: 'Сохранить',
		action: 'confirm'
	}, {
		text: 'Отмена',
		action: 'cancel'
	}],
	modal: true,
	topGridTitle: 'Выбрать вагон',
	bottomGridTitle: 'Выбранный вагон',
	loadRightFromTree: true,
	storeFields: ['id', 'p_name', 'p_kind'],
	srcStoreId: 'goods.railway.auxiliary.Carriages',

	getTopColumnsConfig: function () {
		return [
			{
				xtype: 'rowactions',
				hideable: false,
				resizeable: false,
				width: 18,
				keepSelection: true,
				actions: [{ 
	                iconCls: 'icon-add-item', 
	                qtip: 'Добавить',
	                callback: this.addEmployee
	            }]
			},
            { text: 'Вагон', dataIndex: 'p_name', flex: 1 }
		];
	},

	getBottomColumnsConfig: function () {
		return [
			{
				xtype: 'rowactions',
				hideable: false,
				resizeable: false,
				width: 18,
				actions: [{ 
	                iconCls: 'icon-delete-item', 
	                qtip: 'Удалить', 
	                callback: this.removeEmployee
	            }]
			},
            { text: 'Вагон', dataIndex: 'p_name', flex: 1 }
		];
	},

	getTopGridFilters: function () {
		return [{
            text: 'Выделить все',
            checked: true,
            checkHandler: this.toggleFilterSelection
        }, '-', {
            text: 'Вагон',
            kind: 'selector',
            field_name: 'p_name',
            checked: true
        }];
	}
});