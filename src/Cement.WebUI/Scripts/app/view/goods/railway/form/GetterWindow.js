Ext.define('Cement.view.goods.railway.form.GetterWindow', {
	extend: 'Cement.view.common.SignWindow',
	xtype: 'goods_railway_form_getter_window',
	title: 'Выбор грузополучателя и склада',
	leftFrameTitle: 'Склад',
	topGridTitle: 'Выбрать склад',
	bottomGridTitle: 'Выбранный склад',
	structure_storeId: 'goods.railway.auxiliary.Getters',
	hideCommentPanel: true,
	singleSelection: true,
	displayField: 'p_name',
	storeFields: ['id', 'p_name', 'p_address', 'p_work_time', 'p_supplier', 'p_warehouse_id', 'p_warehouse_code', 'p_warehouse_station', 'p_warehouse_road'],

	bbar: [{
		text: 'Сохранить',
		action: 'confirm'
	}, {
		text: 'Отмена',
		action: 'cancel'
	}],

	sign: function (send) {
		if (this.dstStore.getCount() > 0) {
			this.fireEvent('selected', this.dstStore.getAt(0));
		}
	},

	getTopColumnsConfig: function () {
		return [
			{
				xtype: 'rowactions',
				hideable: false,
				resizeable: false,
				width: 18,
				keepSelection: true,
				actions: [{ 
	                iconCls: 'icon-add-item', 
	                qtip: 'Добавить',
	                callback: this.addEmployee
	            }]
			},
			{ text: 'Наименование', dataIndex: 'p_name', flex: 1 },
			{ text: 'Адрес', dataIndex: 'p_address', flex: 1 }
		];
	},

	getBottomColumnsConfig: function () {
		return [
			{
				xtype: 'rowactions',
				hideable: false,
				resizeable: false,
				width: 18,
				actions: [{ 
	                iconCls: 'icon-delete-item', 
	                qtip: 'Удалить', 
	                callback: this.removeEmployee
	            }]
			},
			{ text: 'Наименование', dataIndex: 'p_name', flex: 1 },
			{ text: 'Адрес', dataIndex: 'p_address', flex: 1 }
		];
	},

	initComponent: function () {
		this.callParent(arguments);
		this.on('afterrender', function () {
			Ext.getStore(this.structure_storeId).getRootNode().expand();
		}, this);
	}
});