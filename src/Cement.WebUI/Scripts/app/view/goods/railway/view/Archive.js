Ext.define('Cement.view.goods.railway.view.Archive', {
	extend: 'Cement.view.goods.railway.view.View',
	alias: 'widget.goods_railway_view_archive',

	getFirstFields: function () {
		return [{
			fieldLabel: 'Номер заявки',
			disabled: true,
			name: 'p_number'
		}, {
			fieldLabel: 'Дата заявки',
			disabled: true,
			name: 'p_date'
		}, {
			fieldLabel: 'Способ отгрузки',
			disabled: true,
			name: 'p_load_kind'
		}, {
			fieldLabel: 'Статус',
			disabled: true,
			name: 'p_status_display'
		}, {
			fieldLabel: 'Тип заявки',
			disabled: true,
			name: 'p_kind_display'
		}];
	}
});