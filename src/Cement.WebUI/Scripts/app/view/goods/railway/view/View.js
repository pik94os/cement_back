Ext.define('Cement.view.goods.railway.view.View', {
	extend: 'Cement.view.goods.auto.view.View',
	alias: 'widget.goods_railway_view_view',
	fieldsDisabled: true,
	buttons: null,
	border: 0,
	tbar: Cement.Config.modelViewButtons,
	layout: 'autocontainer',
	bbar: null,
	noUserFieldsetsControl: false,
	bodyCls: 'model-form',

	getTransportFields: function () {
		return [{
			name: 'p_carriage_kind',
			fieldLabel: 'Род вагона',
			disabled: true
		}, {
			name: 'p_carriage_owner',
			fieldLabel: 'Собственник',
			disabled: true
		}, {
			name: 'p_carriage_number',
			fieldLabel: 'Номер вагона',
			disabled: true
		}, {
			name: 'p_carriage_renter',
			fieldLabel: 'Арендатор',
			disabled: true
		}]
	},

	getGetterFields: function () {
		return [{
				xtype: 'container',
				border: 0,
				cls: 'starred',
				html: '<b>Грузополучатель</b>'
			}, {
				name: 'p_getter_name',
				fieldLabel: 'Наименование',
				disabled: true
			}, {
				fieldLabel: 'Адрес',
				name: 'p_getter_address',
				disabled: true
			}, {
				name: 'p_getter_contact',
				fieldLabel: 'Контактное лицо',
				disabled: true
			}, {
				xtype: 'container',
				border: 0,
				cls: 'starred',
				html: '<b>Склад</b>'
			}, {
				name: 'p_getter_warehouse',
				fieldLabel: 'Наименование',
				disabled: true
			}, {
				fieldLabel: 'Адрес',
				name: 'p_getter_warehouse_address',
				disabled: true
			}, {
				fieldLabel: 'Код',
				name: 'p_getter_warehouse_code',
				disabled: true
			}, {
				fieldLabel: 'Код станции',
				name: 'p_getter_warehouse_station_code',
				disabled: true
			}, {
				fieldLabel: 'Станция',
				name: 'p_getter_warehouse_station',
				disabled: true
			}, {
				fieldLabel: 'Дорога',
				name: 'p_getter_warehouse_road',
				disabled: true
			}, {
				fieldLabel: 'Время работы',
				name: 'p_getter_warehouse_work_time',
				disabled: true
			}
		];
	},

	getSenderItems: function () {
		return [{
				xtype: 'container',
				border: 0,
				cls: 'starred',
				html: '<b>Грузоотправитель</b>'
			}, {
				name: 'p_sender_name',
				fieldLabel: 'Наименование',
				disabled: true
			}, {
				fieldLabel: 'Адрес',
				name: 'p_sender_address',
				disabled: true
			}, {
				name: 'p_sender_contact',
				fieldLabel: 'Контактное лицо',
				disabled: true
			}, {
				xtype: 'container',
				border: 0,
				cls: 'starred',
				html: '<b>Склад</b>'
			}, {
				name: 'p_sender_warehouse',
				fieldLabel: 'Наименование',
				disabled: true
			}, {
				fieldLabel: 'Адрес',
				name: 'p_sender_warehouse_address',
				disabled: true
			}, {
				fieldLabel: 'Код',
				name: 'p_sender_warehouse_code',
				disabled: true
			}, {
				fieldLabel: 'Код станции',
				name: 'p_sender_warehouse_station_code',
				disabled: true
			}, {
				fieldLabel: 'Станция',
				name: 'p_sender_warehouse_station',
				disabled: true
			}, {
				fieldLabel: 'Дорога',
				name: 'p_sender_warehouse_road',
				disabled: true
			}, {
				fieldLabel: 'Время работы',
				name: 'p_sender_warehouse_work_time',
				disabled: true
			}
		];
	},

	getItems: function () {
		return [{
				xtype: 'panel',
				border: 0,
				layout: 'anchor',
				defaultType: 'textfield',
				anchor: '100%',
				bodyPadding: '10 10 10 5',
				autoScroll: true,
				defaults: {
					anchor: '100%',
					labelWidth: 110
				},
				items: [{
					xtype: 'fieldset',
					layout: 'anchor',
					defaults: {
						xtype: 'textfield',
						anchor: '100%'
					},
					border: 0,
					items: this.getFirstFields()
				}, {
					xtype: 'fieldset',
					collapsed: true,
					collapsible: true,
					title: 'Общие данные',
					defaults: {
						xtype: 'textfield',
						anchor: '100%'
					},
					items: this.getCommonItems()
				}, {
					xtype: 'fieldset',
					collapsed: true,
					collapsible: true,
					title: 'Грузоотправитель',
					defaults: {
						xtype: 'textfield',
						anchor: '100%'
					},
					items: this.getSenderItems()
				}, {
					xtype: 'fieldset',
					collapsed: true,
					collapsible: true,
					title: 'Поставщик',
					defaults: {
						xtype: 'textfield',
						anchor: '100%'
					},
					items: this.getSupplierFields()
				}, {
					xtype: 'fieldset',
					collapsed: true,
					collapsible: true,
					title: 'Товар',
					defaults: {
						xtype: 'textfield',
						anchor: '100%'
					},
					items: this.getProductFields()
				}, {
					xtype: 'fieldset',
					collapsed: true,
					collapsible: true,
					title: 'Транспорт',
					defaults: {
						xtype: 'textfield',
						anchor: '100%'
					},
					items: this.getTransportFields()
				}, {
					xtype: 'fieldset',
					collapsed: true,
					collapsible: true,
					title: 'Плательщик',
					defaults: {
						xtype: 'textfield',
						anchor: '100%'
					},
					items: this.getPayerFields()
				}, {
					xtype: 'fieldset',
					collapsed: true,
					collapsible: true,
					title: 'Грузополучатель',
					defaults: {
						xtype: 'textfield',
						anchor: '100%'
					},
					items: this.getGetterFields()
				}]
		}]
	}
});