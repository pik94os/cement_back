Ext.define('Cement.view.goods.railway.view.TemplateView', {
	extend: 'Cement.view.goods.auto.view.TemplateView',
	alias: 'widget.goods_railway_view_template_view',
	fieldsDisabled: true,
	buttons: null,
	border: 0,
	tbar: Cement.Config.modelViewButtons,
	layout: 'autocontainer',
	bbar: null,
    noUserFieldsetsControl: false,
    bodyCls: 'model-form',

    getGetterFields: function () {
		return [{
				xtype: 'container',
				border: 0,
				cls: 'starred',
				html: '<b>Грузополучатель</b>'
			}, {
				name: 'p_getter_name',
				fieldLabel: 'Наименование',
				disabled: true
			}, {
				fieldLabel: 'Адрес',
				name: 'p_getter_address',
				disabled: true
			}, {
				name: 'p_getter_contact',
				fieldLabel: 'Контактное лицо',
				disabled: true
			}, {
				xtype: 'container',
				border: 0,
				cls: 'starred',
				html: '<b>Склад</b>'
			}, {
				name: 'p_getter_warehouse',
				fieldLabel: 'Наименование',
				disabled: true
			}, {
				fieldLabel: 'Адрес',
				name: 'p_getter_warehouse_address',
				disabled: true
			}, {
				fieldLabel: 'Код',
				name: 'p_getter_warehouse_code',
				disabled: true
			}, {
				fieldLabel: 'Код станции',
				name: 'p_getter_warehouse_station_code',
				disabled: true
			}, {
				fieldLabel: 'Станция',
				name: 'p_getter_warehouse_station',
				disabled: true
			}, {
				fieldLabel: 'Дорога',
				name: 'p_getter_warehouse_road',
				disabled: true
			}, {
				fieldLabel: 'Время работы',
				name: 'p_getter_warehouse_work_time',
				disabled: true
			}
		];
	},

	getItems: function () {
		return [{
				xtype: 'panel',
				border: 0,
				layout: 'anchor',
				defaultType: 'textfield',
				anchor: '100%',
				bodyPadding: '10 10 10 5',
				autoScroll: true,
				defaults: {
					anchor: '100%',
					labelWidth: 110
				},
				items: [{
					xtype: 'fieldset',
					layout: 'anchor',
					defaults: {
						xtype: 'textfield',
						anchor: '100%'
					},
					border: 0,
					items: this.getFirstFields()
				}, {
					xtype: 'fieldset',
					collapsed: true,
					collapsible: true,
					title: 'Поставщик',
					defaults: {
						xtype: 'textfield',
						anchor: '100%'
					},
					items: this.getSupplierFields()
				}, {
					xtype: 'fieldset',
					collapsed: true,
					collapsible: true,
					title: 'Товар',
					defaults: {
						xtype: 'textfield',
						anchor: '100%'
					},
					items: this.getProductFields()
				}, {
					xtype: 'fieldset',
					collapsed: true,
					collapsible: true,
					title: 'Плательщик',
					defaults: {
						xtype: 'textfield',
						anchor: '100%'
					},
					items: this.getPayerFields()
				}, {
					xtype: 'fieldset',
					collapsed: true,
					collapsible: true,
					title: 'Грузополучатель',
					defaults: {
						xtype: 'textfield',
						anchor: '100%'
					},
					items: this.getGetterFields()
				}]
		}]
	},

	initComponent: function () {
        Ext.apply(this, {
            items: [
                {
                    xtype: 'panel',
                    border: 0,
                    layout: 'anchor',
                    defaultType: 'textfield',
                    anchor: '100%',
                    bodyPadding: '10 10 10 5',
                    autoScroll: true,
                    defaults: {
                        anchor: '100%',
                        labelWidth: 110
                    },
                    items: this.getItems()
                }
            ]
        });
        this.callParent(arguments);
    }
});
