Ext.define('Cement.view.goods.railway.lists.Archive', {
	extend: 'Cement.view.basic.Grid',
	alias: 'widget.goods_railway_lists_archive',
    autoLoadStore: true,

    bbarText: 'Показаны заявки {0} - {1} из {2}',
    bbarEmptyMsg: 'Нет заявок',
    bbarUsersText: 'Заявок на странице: ',

	gridStore: 'goods.railway.Archive',
	gridStateId: 'stateGoodsRailwayArchive',

	printUrl: Cement.Config.url.goods.railway.archive.printGrid,
	helpUrl: Cement.Config.url.goods.railway.archive.help,
    deleteUrl: Cement.Config.url.goods.railway.archive.deleteUrl,
    unArchiveItemUrl: Cement.Config.url.goods.railway.archive.unArchiveItem,

    shownTitle: null,

    getActionColumns: function () {
        return {
            xtype: 'rowactions',
            hideable: false,
            resizeable: false,
            locked: true,
            width: 46,
            actions: [
                {
                    iconCls: 'icon-unarchive-item',
                    qtip: 'Из архива',
                    callback: this.unArchiveItem
                }
            ],
            keepSelection: true
        };
    },

    getFilterItems: function () {
        return [{
            text: 'Выделить все',
            checked: true,
            checkHandler: this.toggleFilterSelection
        }, '-', {
            text: 'Наименование',
            kind: 'selector',
            field_name: 'p_name',
            checked: true
        }, {
            text: 'Грузоотправитель',
            kind: 'selector',
            field_name: 'p_sender_name',
            checked: true
        }, {
            text: 'Грузоотправитель адрес',
            kind: 'selector',
            field_name: 'p_sender_address',
            checked: true
        }, {
            text: 'Грузоотправитель рабочее время',
            kind: 'selector',
            field_name: 'p_sender_work_time',
            checked: true
        }, {
            text: 'Поставщик',
            kind: 'selector',
            field_name: 'p_supplier_name',
            checked: true
        }, {
            text: 'Поставщик адрес',
            kind: 'selector',
            field_name: 'p_supplier_address',
            checked: true
        }, {
            text: 'Поставщик рабочее время',
            kind: 'selector',
            field_name: 'p_supplier_work_time',
            checked: true
        }, {
            text: 'Плательщик наименование',
            kind: 'selector',
            field_name: 'p_payer_name',
            checked: true
        }, {
            text: 'Плательщик адрес',
            kind: 'selector',
            field_name: 'p_payer_address',
            checked: true
        }, {
            text: 'Плательщик рабочее время',
            kind: 'selector',
            field_name: 'p_payer_work_time',
            checked: true
        }, {
            text: 'Грузополучатель наименование',
            kind: 'selector',
            field_name: 'p_getter_name',
            checked: true
        }, {
            text: 'Грузополучатель адрес',
            kind: 'selector',
            field_name: 'p_getter_address',
            checked: true
        }, {
            text: 'Грузополучатель рабочее время',
            kind: 'selector',
            field_name: 'p_getter_work_time',
            checked: true
        }, {
            text: 'Транспорт тип кузова',
            kind: 'selector',
            field_name: 'p_transport_char_type_display',
            checked: true
        }, {
            text: 'Транспорт доп. опции',
            kind: 'selector',
            field_name: 'p_transport_additional_options_display',
            checked: true
        }];
    },

    getGridColumns: function () {
        var result = [
            { text: 'Тип', dataIndex: 'p_kind_display', width: 60, locked: true },
            { text: '№ П/п', dataIndex: 'p_number', width: 60, locked: true },
            { text: 'Дата', dataIndex: 'p_date', width: 60, locked: true },
            { text: 'С', dataIndex: 'p_s', width: 25, locked: true, resizable: false, align: 'center', renderer: this.radioButtonRenderer1, tdCls: 'pseudoinput-column' },
            { text: 'Ц', dataIndex: 'p_c', width: 25, locked: true, resizable: false, align: 'center', renderer: this.radioButtonRenderer1, tdCls: 'pseudoinput-column' },
            {
                text: 'Грузоотправитель',
                columns: [
                    { text: 'Наименование', dataIndex: 'p_sender_name', width: 150 },
                    { text: 'Адрес', dataIndex: 'p_sender_address', width: 150 },
                    { text: 'Рабочее время', dataIndex: 'p_sender_work_time', width: 150 }
                ]
            },
            {
                text: 'Поставщик',
                columns: [
                    { text: 'Наименование', dataIndex: 'p_supplier_name', width: 150 },
                    { text: 'Адрес', dataIndex: 'p_supplier_address', width: 150 },
                    { text: 'Рабочее время', dataIndex: 'p_supplier_work_time', width: 150 }
                ]
            },
            {
                text: 'Плательщик',
                columns: [
                    { text: 'Наименование', dataIndex: 'p_payer_name', width: 150 },
                    { text: 'Адрес', dataIndex: 'p_payer_address', width: 150 },
                    { text: 'Рабочее время', dataIndex: 'p_payer_work_time', width: 150 }
                ]
            },
            {
                text: 'Грузополучатель',
                columns: [
                    { text: 'Наименование', dataIndex: 'p_getter_name', width: 150 },
                    { text: 'Адрес', dataIndex: 'p_getter_address', width: 150 },
                    { text: 'Рабочее время', dataIndex: 'p_getter_work_time', width: 150 }
                ]
            },
            {
                text: 'Товар',
                columns: [
                    { text: 'Наименование', dataIndex: 'p_product_display', width: 150 },
                    { text: 'Ед. изм.', dataIndex: 'p_product_unit_display', width: 50 },
                    { text: 'Кол-во', dataIndex: 'p_product_quantity', width: 50 },
                    { text: 'Факт кол-во', dataIndex: 'p_product_fact_quantity', width: 50 },
                    { text: 'Цена', dataIndex: 'p_product_price', width: 70 },
                    { text: 'Налог', dataIndex: 'p_product_tax', width: 70 },
                    { text: 'Сумма', dataIndex: 'p_product_sum', width: 70 }
                ]
            },
            {
                text: 'Вагон',
                columns: [
                    { text: 'Номер', dataIndex: 'p_carriage_number', width: 100 },
                    { text: 'Род', dataIndex: 'p_carriage_kind', width: 100 }
                ]
            },
            { text: 'Статус', dataIndex: 'p_status_display', width: 150 }
        ];
        return this.mergeActions(result);
    },

    initComponent: function () {
        Ext.apply(this, {
            title: this.shownTitle
        });
        this.callParent(arguments);
    }
});