Ext.define('Cement.view.goods.railway.lists.Outcoming', {
    extend: 'Cement.view.goods.railway.lists.New',
    alias: 'widget.goods_railway_lists_outcoming',
    autoLoadStore: true,

    gridStore: 'goods.railway.Outcoming',
    gridStateId: 'stateGoodsRailwayOutcoming',

    printUrl: Cement.Config.url.goods.railway.outcoming.printGrid,
    helpUrl: Cement.Config.url.goods.railway.outcoming.help,
    deleteUrl: Cement.Config.url.goods.railway.outcoming.deleteUrl,
    archiveItemUrl: Cement.Config.url.goods.railway.outcoming.archiveItem,

    shownTitle: null,

    getActionColumns: function () {
        return {
            xtype: 'rowactions',
            hideable: false,
            resizeable: false,
            locked: true,
            width: 46,
            actions: [
                {
                    iconCls: 'icon-delete-item',
                    qtip: 'Удалить',
                    callback: this.deleteItem
                },
                {
                    iconCls: 'icon-archive-item',
                    qtip: 'В архив',
                    callback: this.archiveItem
                }
            ],
            keepSelection: true
        };
    },

    initComponent: function () {
        Ext.apply(this, {
            title: this.shownTitle
        });
        this.callParent(arguments);
    }
});