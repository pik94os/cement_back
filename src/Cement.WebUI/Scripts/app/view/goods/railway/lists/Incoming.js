Ext.define('Cement.view.goods.railway.lists.Incoming', {
    extend: 'Cement.view.goods.railway.lists.New',
    alias: 'widget.goods_railway_lists_incoming',
    autoLoadStore: true,

    gridStore: 'goods.railway.Incoming',
    gridStateId: 'stateGoodsRailwayIncoming',

    printUrl: Cement.Config.url.goods.railway.incoming.printGrid,
    helpUrl: Cement.Config.url.goods.railway.incoming.help,
    deleteUrl: Cement.Config.url.goods.railway.incoming.deleteUrl,
    archiveItemUrl: Cement.Config.url.goods.railway.incoming.archiveItem,

    mixins: [
        'Cement.view.goods.Rerequest'
    ],

    shownTitle: null,

    // creatorTree: Ext.clone(Cement.Creators.contracts),
    // createWindowTitle: 'Создать договор',

    getActionColumns: function () {
        return {
            xtype: 'rowactions',
            hideable: false,
            resizeable: false,
            locked: true,
            width: 46,
            actions: [
                {
                    iconCls: 'icon-delete-item',
                    qtip: 'Удалить',
                    callback: this.deleteItem
                },
                {
                    iconCls: 'icon-rerequest-item',
                    qtip: 'Перезаявить',
                    callback: this.rerequestItem
                },
                {
                    iconCls: 'icon-archive-item',
                    qtip: 'В архив',
                    callback: this.archiveItem
                }
            ],
            keepSelection: true
        };
    },

    getGridColumns: function () {
        var result = [
            { text: '№ П/п', dataIndex: 'p_number', width: 60, locked: true },
            { text: 'Дата', dataIndex: 'p_date', width: 60, locked: true },
            { text: 'С', dataIndex: 'p_s', width: 25, locked: true, resizable: false, align: 'center', renderer: this.radioButtonRenderer1, tdCls: 'pseudoinput-column' },
            { text: 'Ц', dataIndex: 'p_c', width: 25, locked: true, resizable: false, align: 'center', renderer: this.radioButtonRenderer1, tdCls: 'pseudoinput-column' },
            { text: 'Плательщик', dataIndex: 'p_payer_name', width: 150, locked: true },
            {
                text: 'Грузополучатель',
                columns: [
                    { text: 'Наименование', dataIndex: 'p_getter_name', width: 150 },
                    { text: 'Адрес', dataIndex: 'p_getter_address', width: 150 },
                    { text: 'Рабочее время', dataIndex: 'p_getter_work_time', width: 150 }
                ]
            },
            {
                text: 'Товар',
                columns: [
                    { text: 'Наименование', dataIndex: 'p_product_display', width: 150 },
                    { text: 'Ед. изм.', dataIndex: 'p_product_unit_display', width: 50 },
                    { text: 'Кол-во', dataIndex: 'p_product_quantity', width: 50 },
                    { text: 'Факт кол-во', dataIndex: 'p_product_fact_quantity', width: 50 },
                    { text: 'Цена', dataIndex: 'p_product_price', width: 70 },
                    { text: 'Налог', dataIndex: 'p_product_tax', width: 70 },
                    { text: 'Сумма', dataIndex: 'p_product_sum', width: 70 }
                ]
            },
            {
                text: 'Вагон',
                columns: [
                    { text: 'Номер', dataIndex: 'p_carriage_number', width: 100 },
                    { text: 'Род', dataIndex: 'p_carriage_kind', width: 100 }
                ]
            },
            { text: 'Статус', dataIndex: 'p_status_display', width: 150 }
        ];
        return this.mergeActions(result);
    },

    initComponent: function () {
        Ext.apply(this, {
            title: this.shownTitle
        });
        this.callParent(arguments);
    }
});