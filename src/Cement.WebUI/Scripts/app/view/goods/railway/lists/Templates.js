Ext.define('Cement.view.goods.railway.lists.Templates', {
	extend: 'Cement.view.basic.Grid',
	alias: 'widget.goods_railway_lists_templates',
    autoLoadStore: true,

    bbarText: 'Показаны шаблоны {0} - {1} из {2}',
    bbarEmptyMsg: 'Нет шаблонов',
    bbarUsersText: 'Шаблонов на странице: ',

	gridStore: 'goods.railway.Templates',
	gridStateId: 'stateGoodsRailwayTemplates',

	printUrl: Cement.Config.url.goods.railway.templates.printGrid,
	helpUrl: Cement.Config.url.goods.railway.templates.help,
    deleteUrl: Cement.Config.url.goods.railway.templates.deleteUrl,

    shownTitle: null,

    // creatorTree: Ext.clone(Cement.Creators.contracts),
    // createWindowTitle: 'Создать договор',

    getFilterItems: function () {
        return [{
            text: 'Выделить все',
            checked: true,
            checkHandler: this.toggleFilterSelection
        }, '-', {
            text: 'Наименование',
            kind: 'selector',
            field_name: 'p_name',
            checked: true
        }, {
            text: 'Поставщик',
            kind: 'selector',
            field_name: 'p_supplier_name',
            checked: true
        }, {
            text: 'Плательщик наименование',
            kind: 'selector',
            field_name: 'p_payer_name',
            checked: true
        }, {
            text: 'Плательщик адрес',
            kind: 'selector',
            field_name: 'p_payer_address',
            checked: true
        }, {
            text: 'Плательщик рабочее время',
            kind: 'selector',
            field_name: 'p_payer_work_time',
            checked: true
        }, {
            text: 'Грузополучатель наименование',
            kind: 'selector',
            field_name: 'p_getter_name',
            checked: true
        }, {
            text: 'Грузополучатель адрес',
            kind: 'selector',
            field_name: 'p_getter_address',
            checked: true
        }, {
            text: 'Грузополучатель рабочее время',
            kind: 'selector',
            field_name: 'p_getter_work_time',
            checked: true
        }];
    },

    getGridColumns: function () {
        var result = [
            { text: 'Наименование', dataIndex: 'p_name', width: 90, locked: true },
            { text: 'С', dataIndex: 'p_s', width: 25, locked: true, resizable: false, align: 'center', renderer: this.radioButtonRenderer1, tdCls: 'pseudoinput-column' },
            { text: 'Ц', dataIndex: 'p_c', width: 25, locked: true, resizable: false, align: 'center', renderer: this.radioButtonRenderer1, tdCls: 'pseudoinput-column' },
            { text: 'Поставщик', dataIndex: 'p_supplier_name', width: 150, locked: true },
            {
                text: 'Плательщик',
                columns: [
                    { text: 'Наименование', dataIndex: 'p_payer_name', width: 150 },
                    { text: 'Адрес', dataIndex: 'p_payer_address', width: 150 },
                    { text: 'Рабочее время', dataIndex: 'p_payer_work_time', width: 150 }
                ]
            },
            {
                text: 'Грузополучатель',
                columns: [
                    { text: 'Наименование', dataIndex: 'p_getter_name', width: 150 },
                    { text: 'Адрес', dataIndex: 'p_getter_address', width: 150 },
                    { text: 'Рабочее время', dataIndex: 'p_getter_work_time', width: 150 }
                ]
            },
            {
                text: 'Товар',
                columns: [
                    { text: 'Наименование', dataIndex: 'p_product_display', width: 150 },
                    { text: 'Ед. изм.', dataIndex: 'p_product_unit_display', width: 50 },
                    { text: 'Кол-во', dataIndex: 'p_product_quantity', width: 50 },
                    { text: 'Факт кол-во', dataIndex: 'p_product_fact_quantity', width: 50 },
                    { text: 'Цена', dataIndex: 'p_product_price', width: 70 },
                    { text: 'Налог', dataIndex: 'p_product_tax', width: 70 },
                    { text: 'Сумма', dataIndex: 'p_product_sum', width: 70 }
                ]
            }
        ];
        return this.mergeActions(result);
    },

    initComponent: function () {
        Ext.apply(this, {
            title: this.shownTitle
        });
        this.callParent(arguments);
    }
});