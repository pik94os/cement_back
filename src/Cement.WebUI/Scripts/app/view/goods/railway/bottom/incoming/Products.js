Ext.define('Cement.view.goods.railway.bottom.incoming.Products', {
    extend: 'Cement.view.products.price.bottom.NewProducts',
    alias: 'widget.goods_railway_bottom_incoming_products',
    gridStore: 'goods.railway.bottom.incoming.Products',
    gridStateId: 'stateGoodsRailwayBottomIncomingProducts'
});