Ext.define('Cement.view.goods.railway.bottom.incoming.Routes', {
    extend: 'Cement.view.products.price.bottom.NewRoutes',
    alias: 'widget.goods_railway_bottom_incoming_routes',
    gridStore: 'goods.railway.bottom.incoming.Routes',
    gridStateId: 'stateGoodsRailwayBottomIncomingRoutes'
});