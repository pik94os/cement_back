Ext.define('Cement.view.goods.railway.bottom.outcoming.Payers', {
	extend: 'Cement.view.goods.auto.bottom.template.Senders',
	alias: 'widget.goods_railway_bottom_outcoming_payers',
  gridStore: 'goods.railway.bottom.outcoming.Payers',
  gridStateId: 'stateGoodsRailwayBottomOutcomingPayers',
  showFilterButton: false,

  printItemUrl: Cement.Config.url.goods.payers.printItem,
  printUrl: Cement.Config.url.goods.payers.printGrid,
  helpUrl: Cement.Config.url.goods.payers.help
});