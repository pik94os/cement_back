Ext.define('Cement.view.goods.railway.bottom.new_r.Routes', {
    extend: 'Cement.view.products.price.bottom.NewRoutes',
    alias: 'widget.goods_railway_bottom_new_routes',
    gridStore: 'goods.railway.bottom.new_r.Routes',
    gridStateId: 'stateGoodsRailwayBottomNewRoutes'
});