Ext.define('Cement.view.goods.railway.bottom.template.Suppliers', {
	extend: 'Cement.view.goods.auto.bottom.template.Senders',
	alias: 'widget.goods_railway_bottom_template_suppliers',
  gridStore: 'goods.railway.bottom.template.Suppliers',
  gridStateId: 'stateGoodsRailwayBottomTemplateSuppliers',
  showFilterButton: false,

  printItemUrl: Cement.Config.url.goods.suppliers.printItem,
  printUrl: Cement.Config.url.goods.suppliers.printGrid,
  helpUrl: Cement.Config.url.goods.suppliers.help
});