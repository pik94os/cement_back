Ext.define('Cement.view.goods.railway.bottom.archive.ConnectedDocumentsIncoming', {
	extend: 'Cement.view.goods.auto.lists.ConnectedDocuments',
	alias: 'widget.goods_railway_bottom_archive_connected_documents_incoming',

	gridStore: 'goods.railway.bottom.archive.ConnectedDocumentsIncoming',
	gridStateId: 'stateGoodsRailwayBottomArchiveConnectedDocumentsIncoming',

    printUrl: Cement.Config.url.goods.railway.documents.printGrid,
    helpUrl: Cement.Config.url.goods.railway.documents.help
});