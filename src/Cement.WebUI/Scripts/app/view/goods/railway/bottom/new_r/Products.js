Ext.define('Cement.view.goods.railway.bottom.new_r.Products', {
    extend: 'Cement.view.products.price.bottom.NewProducts',
    alias: 'widget.goods_railway_bottom_new_products',
    gridStore: 'goods.railway.bottom.new_r.Products',
    gridStateId: 'stateGoodsRailwayBottomNewProducts'
});