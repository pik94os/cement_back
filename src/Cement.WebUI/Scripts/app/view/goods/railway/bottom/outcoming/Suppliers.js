Ext.define('Cement.view.goods.railway.bottom.outcoming.Suppliers', {
	extend: 'Cement.view.goods.auto.bottom.template.Senders',
	alias: 'widget.goods_railway_bottom_outcoming_suppliers',
  gridStore: 'goods.railway.bottom.outcoming.Suppliers',
  gridStateId: 'stateGoodsRailwayBottomOutcomingSuppliers',
  showFilterButton: false,

  printItemUrl: Cement.Config.url.goods.suppliers.printItem,
  printUrl: Cement.Config.url.goods.suppliers.printGrid,
  helpUrl: Cement.Config.url.goods.suppliers.help
});