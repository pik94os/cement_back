Ext.define('Cement.view.goods.railway.bottom.outcoming.Products', {
    extend: 'Cement.view.products.price.bottom.NewProducts',
    alias: 'widget.goods_railway_bottom_outcoming_products',
    gridStore: 'goods.railway.bottom.outcoming.Products',
    gridStateId: 'stateGoodsRailwayBottomOutcomingProducts'
});