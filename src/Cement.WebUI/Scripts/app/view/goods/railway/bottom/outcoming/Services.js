Ext.define('Cement.view.goods.railway.bottom.outcoming.Services', {
    extend: 'Cement.view.products.price.bottom.NewServices',
    alias: 'widget.goods_railway_bottom_outcoming_services',
    gridStore: 'goods.railway.bottom.outcoming.Services',
    gridStateId: 'stateGoodsRailwayBottomOutcomingServices'
});