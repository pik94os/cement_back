Ext.define('Cement.view.goods.railway.bottom.archive.Routes', {
    extend: 'Cement.view.products.price.bottom.NewRoutes',
    alias: 'widget.goods_railway_bottom_archive_routes',
    gridStore: 'goods.railway.bottom.archive.Routes',
    gridStateId: 'stateGoodsRailwayBottomArchiveRoutes'
});