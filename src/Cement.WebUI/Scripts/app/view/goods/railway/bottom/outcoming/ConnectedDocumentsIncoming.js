Ext.define('Cement.view.goods.railway.bottom.outcoming.ConnectedDocumentsIncoming', {
	extend: 'Cement.view.goods.auto.lists.ConnectedDocuments',
	alias: 'widget.goods_railway_bottom_outcoming_connected_documents_incoming',

	gridStore: 'goods.railway.bottom.outcoming.ConnectedDocumentsIncoming',
	gridStateId: 'stateGoodsRailwayBottomOutcomingConnectedDocumentsIncoming',

    printUrl: Cement.Config.url.goods.railway.documents.printGrid,
    helpUrl: Cement.Config.url.goods.railway.documents.help
});