Ext.define('Cement.view.goods.railway.bottom.template.Senders', {
	extend: 'Cement.view.goods.auto.bottom.new_r.Senders',
	alias: 'widget.goods_railway_bottom_template_senders',
  gridStore: 'goods.railway.bottom.template.Senders',
  gridStateId: 'stateGoodsRailwayBottomTemplateSenders',
  showFilterButton: false,
  autoLoadStore: false,

  printItemUrl: Cement.Config.url.goods.senders.printItem,
  printUrl: Cement.Config.url.goods.senders.printGrid,
  helpUrl: Cement.Config.url.goods.senders.help
});