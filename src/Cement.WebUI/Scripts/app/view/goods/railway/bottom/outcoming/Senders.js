Ext.define('Cement.view.goods.railway.bottom.outcoming.Senders', {
	extend: 'Cement.view.goods.auto.bottom.new_r.Senders',
	alias: 'widget.goods_railway_bottom_outcoming_senders',
  gridStore: 'goods.railway.bottom.outcoming.Senders',
  gridStateId: 'stateGoodsRailwayBottomOutcomingSenders',
  showFilterButton: false,
  autoLoadStore: false,

  printItemUrl: Cement.Config.url.goods.senders.printItem,
  printUrl: Cement.Config.url.goods.senders.printGrid,
  helpUrl: Cement.Config.url.goods.senders.help
});