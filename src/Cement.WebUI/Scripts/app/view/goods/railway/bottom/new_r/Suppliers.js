Ext.define('Cement.view.goods.railway.bottom.new_r.Suppliers', {
	extend: 'Cement.view.goods.auto.bottom.new_r.Senders',
	alias: 'widget.goods_railway_bottom_new_suppliers',
  gridStore: 'goods.railway.bottom.new_r.Suppliers',
  gridStateId: 'stateGoodsRailwayBottomNewSuppliers',
  showFilterButton: false,

  printItemUrl: Cement.Config.url.goods.suppliers.printItem,
  printUrl: Cement.Config.url.goods.suppliers.printGrid,
  helpUrl: Cement.Config.url.goods.suppliers.help
});