Ext.define('Cement.view.goods.railway.bottom.archive.Senders', {
	extend: 'Cement.view.goods.auto.bottom.new_r.Senders',
	alias: 'widget.goods_railway_bottom_archive_senders',
  gridStore: 'goods.railway.bottom.archive.Senders',
  gridStateId: 'stateGoodsRailwayBottomArchiveSenders',
  showFilterButton: false,
  autoLoadStore: false,

  printItemUrl: Cement.Config.url.goods.senders.printItem,
  printUrl: Cement.Config.url.goods.senders.printGrid,
  helpUrl: Cement.Config.url.goods.senders.help
});