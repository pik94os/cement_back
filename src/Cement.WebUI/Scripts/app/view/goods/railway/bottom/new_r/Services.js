Ext.define('Cement.view.goods.railway.bottom.new_r.Services', {
    extend: 'Cement.view.products.price.bottom.NewServices',
    alias: 'widget.goods_railway_bottom_new_services',
    gridStore: 'goods.railway.bottom.new_r.Services',
    gridStateId: 'stateGoodsRailwayBottomNewServices'
});