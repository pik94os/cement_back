Ext.define('Cement.view.goods.railway.bottom.new_r.Getters', {
	extend: 'Cement.view.goods.auto.bottom.new_r.Senders',
	alias: 'widget.goods_railway_bottom_new_getters',
  gridStore: 'goods.railway.bottom.new_r.Getters',
  gridStateId: 'stateGoodsRailwayBottomNewGetters',
  showFilterButton: false,

  printItemUrl: Cement.Config.url.goods.getters.printItem,
  printUrl: Cement.Config.url.goods.getters.printGrid,
  helpUrl: Cement.Config.url.goods.getters.help
});