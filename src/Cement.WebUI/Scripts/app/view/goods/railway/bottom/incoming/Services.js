Ext.define('Cement.view.goods.railway.bottom.incoming.Services', {
    extend: 'Cement.view.products.price.bottom.NewServices',
    alias: 'widget.goods_railway_bottom_incoming_services',
    gridStore: 'goods.railway.bottom.incoming.Services',
    gridStateId: 'stateGoodsRailwayBottomIncomingServices'
});