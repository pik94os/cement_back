Ext.define('Cement.view.goods.railway.bottom.incoming.Getters', {
	extend: 'Cement.view.goods.auto.bottom.new_r.Senders',
	alias: 'widget.goods_railway_bottom_incoming_getters',
  gridStore: 'goods.railway.bottom.incoming.Getters',
  gridStateId: 'stateGoodsRailwayBottomIncomingGetters',
  showFilterButton: false,

  printItemUrl: Cement.Config.url.goods.getters.printItem,
  printUrl: Cement.Config.url.goods.getters.printGrid,
  helpUrl: Cement.Config.url.goods.getters.help
});