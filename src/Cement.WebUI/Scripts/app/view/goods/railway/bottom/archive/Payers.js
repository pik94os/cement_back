Ext.define('Cement.view.goods.railway.bottom.archive.Payers', {
	extend: 'Cement.view.goods.auto.bottom.template.Senders',
	alias: 'widget.goods_railway_bottom_archive_payers',
  gridStore: 'goods.railway.bottom.archive.Payers',
  gridStateId: 'stateGoodsRailwayBottomArchivePayers',
  showFilterButton: false,

  printItemUrl: Cement.Config.url.goods.payers.printItem,
  printUrl: Cement.Config.url.goods.payers.printGrid,
  helpUrl: Cement.Config.url.goods.payers.help
});