Ext.define('Cement.view.goods.railway.bottom.archive.Products', {
    extend: 'Cement.view.products.price.bottom.NewProducts',
    alias: 'widget.goods_railway_bottom_archive_products',
    gridStore: 'goods.railway.bottom.archive.Products',
    gridStateId: 'stateGoodsRailwayBottomArchiveProducts'
});