Ext.define('Cement.view.goods.railway.bottom.outcoming.Routes', {
    extend: 'Cement.view.products.price.bottom.NewRoutes',
    alias: 'widget.goods_railway_bottom_outcoming_routes',
    gridStore: 'goods.railway.bottom.outcoming.Routes',
    gridStateId: 'stateGoodsRailwayBottomOutcomingRoutes'
});