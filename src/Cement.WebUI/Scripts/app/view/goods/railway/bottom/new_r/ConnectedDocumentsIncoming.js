Ext.define('Cement.view.goods.railway.bottom.new_r.ConnectedDocumentsIncoming', {
	extend: 'Cement.view.goods.auto.lists.ConnectedDocuments',
	alias: 'widget.goods_railway_bottom_new_connected_documents_incoming',

	gridStore: 'goods.railway.bottom.new_r.ConnectedDocumentsIncoming',
	gridStateId: 'stateGoodsRailwayBottomNewConnectedDocumentsIncoming',

    printUrl: Cement.Config.url.goods.railway.documents.printGrid,
    helpUrl: Cement.Config.url.goods.railway.documents.help
});