Ext.define('Cement.view.goods.railway.bottom.archive.Getters', {
	extend: 'Cement.view.goods.auto.bottom.new_r.Senders',
	alias: 'widget.goods_railway_bottom_archive_getters',
  gridStore: 'goods.railway.bottom.archive.Getters',
  gridStateId: 'stateGoodsRailwayBottomArchiveGetters',
  showFilterButton: false,

  printItemUrl: Cement.Config.url.goods.getters.printItem,
  printUrl: Cement.Config.url.goods.getters.printGrid,
  helpUrl: Cement.Config.url.goods.getters.help
});