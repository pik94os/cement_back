Ext.define('Cement.view.goods.railway.bottom.incoming.ConnectedDocumentsIncoming', {
	extend: 'Cement.view.goods.auto.lists.ConnectedDocuments',
	alias: 'widget.goods_railway_bottom_incoming_connected_documents_incoming',

	gridStore: 'goods.railway.bottom.incoming.ConnectedDocumentsIncoming',
	gridStateId: 'stateGoodsRailwayBottomIncomingConnectedDocumentsIncoming',

    printUrl: Cement.Config.url.goods.railway.documents.printGrid,
    helpUrl: Cement.Config.url.goods.railway.documents.help
});