Ext.define('Cement.view.goods.railway.bottom.template.Products', {
    extend: 'Cement.view.products.price.bottom.NewProducts',
    alias: 'widget.goods_railway_bottom_template_products',
    gridStore: 'goods.railway.bottom.template.Products',
    gridStateId: 'stateGoodsRailwayBottomTemplateProducts'
});