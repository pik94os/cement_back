Ext.define('Cement.view.goods.railway.bottom.incoming.Payers', {
	extend: 'Cement.view.goods.auto.bottom.template.Senders',
	alias: 'widget.goods_railway_bottom_incoming_payers',
  gridStore: 'goods.railway.bottom.incoming.Payers',
  gridStateId: 'stateGoodsRailwayBottomIncomingPayers',
  showFilterButton: false,

  printItemUrl: Cement.Config.url.goods.payers.printItem,
  printUrl: Cement.Config.url.goods.payers.printGrid,
  helpUrl: Cement.Config.url.goods.payers.help
});