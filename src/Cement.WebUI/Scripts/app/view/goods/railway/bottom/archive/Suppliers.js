Ext.define('Cement.view.goods.railway.bottom.archive.Suppliers', {
	extend: 'Cement.view.goods.auto.bottom.template.Senders',
	alias: 'widget.goods_railway_bottom_archive_suppliers',
  gridStore: 'goods.railway.bottom.archive.Suppliers',
  gridStateId: 'stateGoodsRailwayBottomArchiveSuppliers',
  showFilterButton: false,

  printItemUrl: Cement.Config.url.goods.suppliers.printItem,
  printUrl: Cement.Config.url.goods.suppliers.printGrid,
  helpUrl: Cement.Config.url.goods.suppliers.help
});