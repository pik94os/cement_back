Ext.define('Cement.view.goods.railway.bottom.template.Payers', {
	extend: 'Cement.view.goods.auto.bottom.template.Senders',
	alias: 'widget.goods_railway_bottom_template_payers',
  gridStore: 'goods.railway.bottom.template.Payers',
  gridStateId: 'stateGoodsRailwayBottomTemplatePayers',
  showFilterButton: false,

  printItemUrl: Cement.Config.url.goods.payers.printItem,
  printUrl: Cement.Config.url.goods.payers.printGrid,
  helpUrl: Cement.Config.url.goods.payers.help
});