Ext.define('Cement.view.goods.railway.bottom.template.Routes', {
    extend: 'Cement.view.products.price.bottom.NewRoutes',
    alias: 'widget.goods_railway_bottom_template_routes',
    gridStore: 'goods.railway.bottom.template.Routes',
    gridStateId: 'stateGoodsRailwayBottomTemplateRoutes'
});