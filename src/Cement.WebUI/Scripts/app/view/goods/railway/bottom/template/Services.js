Ext.define('Cement.view.goods.railway.bottom.template.Services', {
    extend: 'Cement.view.products.price.bottom.NewServices',
    alias: 'widget.goods_railway_bottom_template_services',
    gridStore: 'goods.railway.bottom.template.Services',
    gridStateId: 'stateGoodsRailwayBottomTemplateServices'
});