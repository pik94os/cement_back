Ext.define('Cement.view.goods.railway.bottom.outcoming.Getters', {
	extend: 'Cement.view.goods.auto.bottom.new_r.Senders',
	alias: 'widget.goods_railway_bottom_outcoming_getters',
  gridStore: 'goods.railway.bottom.outcoming.Getters',
  gridStateId: 'stateGoodsRailwayBottomOutcomingGetters',
  showFilterButton: false,

  printItemUrl: Cement.Config.url.goods.getters.printItem,
  printUrl: Cement.Config.url.goods.getters.printGrid,
  helpUrl: Cement.Config.url.goods.getters.help
});