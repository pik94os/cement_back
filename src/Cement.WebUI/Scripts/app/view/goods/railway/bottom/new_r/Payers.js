Ext.define('Cement.view.goods.railway.bottom.new_r.Payers', {
	extend: 'Cement.view.goods.auto.bottom.new_r.Senders',
	alias: 'widget.goods_railway_bottom_new_payers',
  gridStore: 'goods.railway.bottom.new_r.Payers',
  gridStateId: 'stateGoodsRailwayBottomNewPayers',
  showFilterButton: false,

  printItemUrl: Cement.Config.url.goods.payers.printItem,
  printUrl: Cement.Config.url.goods.payers.printGrid,
  helpUrl: Cement.Config.url.goods.payers.help
});