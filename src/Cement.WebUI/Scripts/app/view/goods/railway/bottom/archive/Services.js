Ext.define('Cement.view.goods.railway.bottom.archive.Services', {
    extend: 'Cement.view.products.price.bottom.NewServices',
    alias: 'widget.goods_railway_bottom_archive_services',
    gridStore: 'goods.railway.bottom.archive.Services',
    gridStateId: 'stateGoodsRailwayBottomArchiveServices'
});