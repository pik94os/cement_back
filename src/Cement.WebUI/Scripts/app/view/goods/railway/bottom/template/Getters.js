Ext.define('Cement.view.goods.railway.bottom.template.Getters', {
	extend: 'Cement.view.goods.auto.bottom.new_r.Senders',
	alias: 'widget.goods_railway_bottom_template_getters',
  gridStore: 'goods.railway.bottom.template.Getters',
  gridStateId: 'stateGoodsRailwayBottomTemplateGetters',
  showFilterButton: false,

  printItemUrl: Cement.Config.url.goods.getters.printItem,
  printUrl: Cement.Config.url.goods.getters.printGrid,
  helpUrl: Cement.Config.url.goods.getters.help
});