Ext.define('Cement.view.goods.services.complex.Templates', {
	extend: 'Cement.view.basic.Complex',
	alias: 'widget.goods_services_complex_templates',
    border: 0,
    enableDetail: false,
    autoLoadStore: false,
    priceId: null,
    bodyPadding: 0,
    padding: 0,
    header: false,
    title: null,
    rowsCount: 3,
    title: 'Шаблоны',
    tabTitle: 'Шаблоны',
    cls: 'no-side-borders',
    topXType: 'goods_services_lists_templates',
    topDetailType: 'Cement.view.goods.services.view.TemplateView',
    bottomTitle: 'Связанные документы',
    bottomTabs: [{
        title: 'Связанные документы',
        xtype: 'goods_services_bottom_templates_connected_documents_incoming',
        detailType: 'Cement.view.goods.auto.view.ConnectedDocument',
        constantFilterParam: 'p_base_doc',
        childrenConstantFilterParam: 'p_base_doc',
        border: 0
    }],
    subBottomTitle: 'Справочник',
    subBottomTabs: [{
        title: 'Покупатель',
        shownTitle: 'Покупатель',
        xtype: 'goods_services_bottom_template_buyers',
        detailType: 'Cement.view.corporate.firm.View'
    }, {
        title: 'Продавец',
        shownTitle: 'Продавец',
        xtype: 'goods_services_bottom_template_sellers',
        detailType: 'Cement.view.corporate.firm.View'
    }, {
        title: 'Услуги',
        shownTitle: 'Услуги',
        xtype: 'goods_services_bottom_template_services',
        detailType: 'Cement.view.products.services.View'
    }, {
        title: 'Маршрут',
        shownTitle: 'Маршрут',
        xtype: 'goods_services_bottom_template_routes',
        detailType: 'Cement.view.products.price.RouteView'
    }]
});