Ext.define('Cement.view.goods.services.form.SellerContactWindow', {
	extend: 'Cement.view.goods.auto.form.SupplierContactWindow',
	xtype: 'goods_services_form_seller_contact_window',
	structure_storeId: 'goods.services.auxiliary.SellerContacts'
});