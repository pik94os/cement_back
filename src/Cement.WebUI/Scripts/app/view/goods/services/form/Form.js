Ext.define('Cement.view.goods.services.form.Form', {
	extend: 'Cement.view.basic.Form',
	alias: 'widget.goods_services_form_form',
	fieldsDisabled: false,
	url: Cement.Config.url.goods.services.new_r.saveUrl,
	method: Cement.Config.url.goods.services.new_r.saveMethod,
	printUrl: Cement.Config.url.goods.services.new_r.printItem,
	isFilter: false,
	layout: 'fit',
	showSaveAndSignButton: true,
	saveButtonDisabled: false,

	importancesStoreId: 'goods.auxiliary.Importances',
	securitiesStoreId: 'goods.auxiliary.Securities',

	getItems: function () {
		return [{
				xtype: 'fieldset',
				border: 0,
				margin: 0,
				padding: '0 110',
				defaults: {
					xtype: 'textfield',
					anchor: '100%',
					labelAlign: 'right'
				},
				items: [{
					name: 'p_number',
					fieldLabel: 'Номер'
				}, {
					name: 'p_date',
					fieldLabel: 'Дата',
					xtype: 'datefield'
				}]
            }, {
                xtype: 'fieldset',
                title: 'Общие данные',
                collapsed: false,
                collapsible: true,
                defaults: {
                    xtype: 'textfield',
                    anchor: '100%',
                    labelWidth: 200,
                    labelAlign: 'right'
                },
                items: [{
                	xtype: 'checkbox',
                	name: 'p_no_request',
                  inputValue: 'true',
                  uncheckedValue: 'false',
                	fieldLabel: 'Нет заявки'
                },
                	this.getSelectorField('Заявка', 'p_request', 'show_request_window'), {
                	xtype: 'checkbox',
                	name: 'p_no_template',
                  inputValue: 'true',
                  uncheckedValue: 'false',
                	fieldLabel: 'Нет шаблона'
                },
                	this.getSelectorField('Шаблон', 'p_template', 'show_template_window'),
                	{
                		fieldLabel: 'Дата, время загрузки',
                		xtype: 'fieldcontainer',
                		layout: 'hbox',
                		items: [{
                			name: 'p_load_date',
                			xtype: 'datefield',
                			flex: 1,
                			margin: '0 5 0 0'
                		}, {
                			name: 'p_load_time',
                			xtype: 'timefield',
                			flex: 1,
                			margin: '0 0 0 5'
                		}]
                	}, {
                		fieldLabel: 'Дата, время выгрузки',
                		xtype: 'fieldcontainer',
                		layout: 'hbox',
                		items: [{
                			name: 'p_unload_date',
                			xtype: 'datefield',
                			flex: 1,
                			margin: '0 5 0 0'
                		}, {
                			name: 'p_unload_time',
                			xtype: 'timefield',
                			flex: 1,
                			margin: '0 0 0 5'
                		}]
                	}
                ]
            }, {
                xtype: 'fieldset',
                title: 'Продавец',
                collapsed: false,
                collapsible: true,
                role: 'supplier',
                defaults: {
                    xtype: 'textfield',
                    anchor: '100%',
                    labelWidth: 200,
                    labelAlign: 'right'
                },
                items: [{
                    xtype: 'checkbox',
                    fieldLabel: 'Нет продавца',
                    name: 'p_no_supplier',
                    inputValue: 'true',
                    uncheckedValue: 'false'
                },
            	this.getSelectorField('Наименование', 'p_supplier', 'show_seller_window'), {
            	    name: 'p_supplier_address',
            	    fieldLabel: 'Адрес',
            	    disabled: true
            	},
            	this.getSelectorField('Контактное лицо', 'p_supplier_contact', 'show_seller_contact_window'), {
            	    name: 'p_supplier_work_time',
            	    fieldLabel: 'Рабочее время',
            	    disabled: true
            	}]
            }, {
            	xtype: 'fieldset',
            	title: 'Услуга',
            	role: 'service',
            	collapsed: false,
            	collapsible: true,
            	defaults: {
                    xtype: 'textfield',
                    anchor: '100%',
                    labelWidth: 200,
                    labelAlign: 'right'
                },
                items: [{
                	xtype: 'hiddenfield',
                	name: 'p_contract'
                }, {
                	name: 'p_contract_display',
                	disabled: true,
                	fieldLabel: 'Договор'
                },
                this.getSelectorField('Наименование', 'p_service', 'show_service_window'), {
                	name: 'p_service_unit',
                	disabled: true,
                	fieldLabel: 'Единица измерения'
                }, {
                	name: 'p_service_count',
                	fieldLabel: 'Количество'
                }]
            }, {
                xtype: 'fieldset',
                title: 'Дополнительные данные',
                collapsed: false,
                collapsible: true,
                defaults: {
                    xtype: 'textfield',
                    anchor: '100%',
                    labelWidth: 200,
                    labelAlign: 'right'
                },
                items: [
                	this.getCombo('Важность', 'p_importance', this.importancesStoreId),
                	this.getCombo('Секретность', 'p_security', this.securitiesStoreId), {
	            	fieldLabel: 'Примечание',
	            	name: 'p_comment',
	            	xtype: 'textarea',
	            	height: 150
	            }]
            }
        ];
	},

	initComponent: function () {
		this.callParent(arguments);
		this.down('checkbox[name=p_no_request]').on('change', function (control, newValue){
			if (newValue) {
				this.down('fieldcontainer[role=container_p_request]').disable();
			}
			else {
				this.down('fieldcontainer[role=container_p_request]').enable();
			}
		}, this);
		this.down('checkbox[name=p_no_template]').on('change', function (control, newValue){
			if (newValue) {
				this.down('fieldcontainer[role=container_p_template]').disable();
			}
			else {
				this.down('fieldcontainer[role=container_p_template]').enable();
			}
		}, this);
		this.down('button[action=show_request_window]').on('click', function () {
			this.createRequestWindow();
			this.requestWindow.show();
		}, this);
		this.down('button[action=show_template_window]').on('click', function () {
			this.createTemplateWindow();
			this.templateWindow.show();
		}, this);
		this.down('button[action=show_seller_window]').on('click', function () {
			this.createSellerWindow();
			this.sellerWindow.show();
		}, this);
		this.down('button[action=show_seller_contact_window]').on('click', function () {
			this.createSellerContactWindow();
			this.sellerContactWindow.show();
		}, this);
		this.down('button[action=show_service_window]').on('click', function () {
			this.createServiceWindow();
			this.serviceWindow.show();
		}, this);
		this.down('checkbox[name=p_no_supplier]').on('change', function (control, newValue) {
			if (newValue) {
			    this.down('fieldcontainer[role=container_p_supplier]').disable();
			    this.down('fieldcontainer[role=container_p_supplier_contact]').disable();
			}
			else {
			    this.down('fieldcontainer[role=container_p_supplier]').enable();
				this.down('fieldcontainer[role=container_p_supplier_contact]').enable();
			}
		}, this);
		this.down('fieldcontainer[role=container_p_supplier_contact]').disable();
	},

	createServiceWindow: function (contractId) {
		if (!this.serviceWindow) {
		    this.serviceWindow = Ext.create('Cement.view.goods.services.form.ServiceWindow', contractId);
			this.serviceWindow.on('serviceselected', function (rec) {
				this.down('textfield[name=p_service_display]').setValue(rec.get('p_name'));
				this.down('textfield[name=p_service_unit]').setValue(rec.get('p_unit_display'));
				this.down('hiddenfield[name=p_service]').setValue(rec.get('id'));
				this.serviceWindow.hide();
			}, this);
		} else {
		    if (contractId > 0) {
		        this.serviceWindow.setContractId(contractId);
		    }
		}
	},

	createSellerContactWindow: function () {
		if (!this.sellerContactWindow) {
			this.sellerContactWindow = Ext.create('Cement.view.goods.services.form.SellerContactWindow');
			this.sellerContactWindow.on('selected', function (rec) {
			    this.down('textfield[name=p_supplier_contact_display]').setValue(rec.get('p_name'));
				this.down('hiddenfield[name=p_supplier_contact]').setValue(rec.get('id'));
				this.sellerContactWindow.hide();
			}, this);
		}
	},

	createSellerWindow: function () {
		if (!this.sellerWindow) {
			this.sellerWindow = Ext.create('Cement.view.goods.services.form.SellerWindow');
			this.sellerWindow.on('selected', function (rec) {
			    this.down('checkbox[name=p_no_supplier]').disable();
			    this.down('textfield[name=p_supplier_display]').setValue(rec.get('p_supplier'));
			    this.down('textfield[name=p_supplier_address]').setValue(rec.get('p_address'));
			    this.down('hiddenfield[name=p_supplier]').setValue(rec.get('id'));
			    this.down('textfield[name=p_supplier_work_time]').setValue(rec.get('p_work_time'));
				this.down('hiddenfield[name=p_contract]').setValue(rec.get('p_contract_id'));
				this.down('textfield[name=p_contract_display]').setValue(rec.get('p_name'));
				this.down('hiddenfield[name=p_supplier_contact]').setValue('');
				this.down('textfield[name=p_supplier_contact_display]').setValue('');
				this.createSellerContactWindow();
				this.sellerContactWindow.setFirm(rec.get('id'));
				this.createServiceWindow(rec.get('p_contract_id'));
				this.sellerWindow.hide();
				this.down('fieldcontainer[role=container_p_supplier_contact]').enable();
			}, this);
		}
	},

	createRequestWindow: function () {
		if (!this.requestWindow) {
			this.requestWindow= Ext.create('Cement.view.goods.services.form.RequestWindow');
			this.requestWindow.on('requestselected', function (rec) {
				this.loadRecord(rec);
				this.down('checkbox[name=p_no_template]').disable();
				this.down('fieldcontainer[role=container_p_template]').disable();
				this.down('fieldcontainer[role=container_p_service]').disable();
				this.down('textfield[name=p_request_display]').setValue('№' + rec.get('p_number') + ' от ' + rec.get('p_date'));
				this.down('hiddenfield[name=p_request]').setValue(rec.get('id'));
				this.requestWindow.hide();
			}, this);
		}
	},

	createTemplateWindow: function () {
		if (!this.templateWindow) {
			this.templateWindow= Ext.create('Cement.view.goods.auto.form.TemplateWindow');
			this.templateWindow.on('requestselected', function (rec) {
				this.down('checkbox[name=p_no_template]').disable();
				this.down('checkbox[name=p_no_request]').disable();
				this.down('fieldcontainer[role=container_p_service]').disable();
				this.down('fieldcontainer[role=container_p_request]').disable();
				this.loadRecord(rec);
				this.down('textfield[name=p_template_display]').setValue(rec.get('p_name'));
				this.down('hiddenfield[name=p_template]').setValue(rec.get('id'));
				this.templateWindow.hide();
			}, this);
		}
	},

	afterRecordLoad: function (rec) {
		this.createRequestWindow();
		this.createTemplateWindow();
		this.createSellerWindow();
		this.createSellerContactWindow();
		// this.createProductWindow();
		this.requestWindow.loadData([rec.get('p_request')]);
		this.templateWindow.loadData([rec.get('p_template')]);
		// this.sellerWindow.loadData([rec.get('p_seller')]);
		this.sellerContactWindow.setFirm(rec.get('p_supplier'));
		// this.sellerContactWindow.loadData([rec.get('p_seller_contact')]);
		// this.productWindow.loadData([rec.get('p_product')]);
	},

	reset: function () {
	    this.callParent(arguments);

		this.down('checkbox[name=p_no_template]').enable();
		this.down('checkbox[name=p_no_request]').enable();
		this.down('fieldcontainer[role=container_p_request]').enable();
		this.down('fieldcontainer[role=container_p_template]').enable();
		this.down('fieldcontainer[role=container_p_service]').enable();
	}
});