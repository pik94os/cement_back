Ext.define('Cement.view.goods.services.lists.New', {
	extend: 'Cement.view.basic.Grid',
	alias: 'widget.goods_services_lists_new',
    autoLoadStore: true,

    bbarText: 'Показаны заявки {0} - {1} из {2}',
    bbarEmptyMsg: 'Нет заявок',
    bbarUsersText: 'Заявок на странице: ',

	gridStore: 'goods.services.New',
	gridStateId: 'stateGoodsServicesNew',

	printUrl: Cement.Config.url.goods.services.new_r.printGrid,
	helpUrl: Cement.Config.url.goods.services.new_r.help,
    deleteUrl: Cement.Config.url.goods.services.new_r.deleteUrl,

    shownTitle: null,

    getActionColumns: function () {
        return {
            xtype: 'rowactions',
            hideable: false,
            resizeable: false,
            locked: true,
            width: 46,
            actions: [
                {
                    iconCls: 'icon-edit-item',
                    qtip: 'Редактировать',
                    callback: this.editItem
                },
                {
                    iconCls: 'icon-delete-item',
                    qtip: 'Удалить',
                    callback: this.deleteItem
                }
            ],
            keepSelection: true
        };
    },

    getFilterItems: function () {
        return [{
            text: 'Выделить все',
            checked: true,
            checkHandler: this.toggleFilterSelection
        }, '-', {
            text: 'Продавец',
            kind: 'selector',
            field_name: 'p_seller_name',
            checked: true
        }, {
            text: 'Покупатель наименование',
            kind: 'selector',
            field_name: 'p_buyer_name',
            checked: true
        }, {
            text: 'Покупатель адрес',
            kind: 'selector',
            field_name: 'p_buyer_address',
            checked: true
        }, {
            text: 'Покупатель рабочее время',
            kind: 'selector',
            field_name: 'p_buyer_work_time',
            checked: true
        }, {
            text: 'Услуга наименование',
            kind: 'selector',
            field_name: 'p_service_display',
            checked: true
        }, {
            text: 'Услуга ед. изм.',
            kind: 'selector',
            field_name: 'p_service_unit_display',
            checked: true
        }, {
            text: 'Услуга кол-во',
            kind: 'selector',
            field_name: 'p_service_count',
            checked: true
        }, {
            text: 'Услуга факт кол-во',
            kind: 'selector',
            field_name: 'p_service_fact_count',
            checked: true
        }, {
            text: 'Услуга цена',
            kind: 'selector',
            field_name: 'p_service_price',
            checked: true
        }, {
            text: 'Услуга налог',
            kind: 'selector',
            field_name: 'p_service_tax',
            checked: true
        }, {
            text: 'Услуга сумма',
            kind: 'selector',
            field_name: 'p_service_sum',
            checked: true
        }, {
            text: 'Статус',
            kind: 'selector',
            field_name: 'p_status_display',
            checked: true
        }];
    },

    getGridColumns: function () {
        var result = [
            { text: '№ П/п', dataIndex: 'p_number', width: 60, locked: true },
            { text: 'Дата', dataIndex: 'p_date', width: 60, locked: true },
            { text: 'Продавец', dataIndex: 'p_seller_name', width: 150, locked: true },
            {
                text: 'Покупатель',
                columns: [
                    { text: 'Наименование', dataIndex: 'p_buyer_name', width: 150 },
                    { text: 'Адрес', dataIndex: 'p_buyer_address', width: 150 },
                    { text: 'Рабочее время', dataIndex: 'p_buyer_work_time', width: 150 }
                ]
            },
            {
                text: 'Услуга',
                columns: [
                    { text: 'Наименование', dataIndex: 'p_service_display', width: 150 },
                    { text: 'Ед. изм.', dataIndex: 'p_service_unit_display', width: 50 },
                    { text: 'Кол-во', dataIndex: 'p_service_count', width: 50 },
                    { text: 'Факт. кол-во', dataIndex: 'p_service_fact_count', width: 50 },
                    { text: 'Цена', dataIndex: 'p_service_price', width: 50 },
                    { text: 'Налог', dataIndex: 'p_service_tax', width: 50 },
                    { text: 'Сумма', dataIndex: 'p_service_sum', width: 50 }
                ]
            },
            { text: 'Статус', dataIndex: 'p_status_display', width: 100 }
        ];
        return this.mergeActions(result);
    },

    initComponent: function () {
        Ext.apply(this, {
            title: this.shownTitle
        });
        this.callParent(arguments);
    }
});