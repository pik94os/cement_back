Ext.define('Cement.view.goods.services.lists.Templates', {
	extend: 'Cement.view.basic.Grid',
	alias: 'widget.goods_services_lists_templates',
    autoLoadStore: true,

    bbarText: 'Показаны шаблоны {0} - {1} из {2}',
    bbarEmptyMsg: 'Нет шаблонов',
    bbarUsersText: 'Шаблонов на странице: ',

	gridStore: 'goods.services.Templates',
	gridStateId: 'stateGoodsServicesTemplates',

	printUrl: Cement.Config.url.goods.services.templates.printGrid,
	helpUrl: Cement.Config.url.goods.services.templates.help,
    deleteUrl: Cement.Config.url.goods.services.templates.deleteUrl,

    shownTitle: null,

    getFilterItems: function () {
        return [{
            text: 'Выделить все',
            checked: true,
            checkHandler: this.toggleFilterSelection
        }, '-', {
            text: 'Наименование',
            kind: 'selector',
            field_name: 'p_name',
            checked: true
        }, {
            text: 'Продавец',
            kind: 'selector',
            field_name: 'p_seller_name',
            checked: true
        }, {
            text: 'Покупатель наименование',
            kind: 'selector',
            field_name: 'p_buyer_name',
            checked: true
        }, {
            text: 'Покупатель адрес',
            kind: 'selector',
            field_name: 'p_buyer_address',
            checked: true
        }, {
            text: 'Покупатель рабочее время',
            kind: 'selector',
            field_name: 'p_buyer_work_time',
            checked: true
        }, {
            text: 'Услуга наименование',
            kind: 'selector',
            field_name: 'p_service_display',
            checked: true
        }, {
            text: 'Услуга ед. изм.',
            kind: 'selector',
            field_name: 'p_service_unit_display',
            checked: true
        }];
    },

    getGridColumns: function () {
        var result = [
            { text: 'Наименование', dataIndex: 'p_name', width: 90, locked: true },
            { text: 'Продавец', dataIndex: 'p_seller_name', width: 150, locked: true },
            {
                text: 'Покупатель',
                columns: [
                    { text: 'Наименование', dataIndex: 'p_buyer_name', width: 150 },
                    { text: 'Адрес', dataIndex: 'p_buyer_address', width: 150 },
                    { text: 'Рабочее время', dataIndex: 'p_buyer_work_time', width: 150 }
                ]
            },
            {
                text: 'Услуга',
                columns: [
                    { text: 'Наименование', dataIndex: 'p_service_display', width: 150 },
                    { text: 'Ед. изм.', dataIndex: 'p_service_unit_display', width: 50 }
                ]
            }
        ];
        return this.mergeActions(result);
    },

    initComponent: function () {
        Ext.apply(this, {
            title: this.shownTitle
        });
        this.callParent(arguments);
    }
});