Ext.define('Cement.view.goods.services.bottom.incoming.Buyers', {
	extend: 'Cement.view.goods.auto.bottom.new_r.Senders',
	alias: 'widget.goods_services_bottom_incoming_buyers',
  gridStore: 'goods.services.bottom.incoming.Buyers',
  gridStateId: 'stateGoodsServicesBottomIncomingBuyers',
  showFilterButton: false,

  printItemUrl: Cement.Config.url.goods.buyers.printItem,
  printUrl: Cement.Config.url.goods.buyers.printGrid,
  helpUrl: Cement.Config.url.goods.buyers.help
});