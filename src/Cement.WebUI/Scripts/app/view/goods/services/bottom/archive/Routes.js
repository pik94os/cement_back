Ext.define('Cement.view.goods.services.bottom.archive.Routes', {
    extend: 'Cement.view.products.price.bottom.NewRoutes',
    alias: 'widget.goods_services_bottom_archive_routes',
    gridStore: 'goods.services.bottom.archive.Routes',
    gridStateId: 'stateGoodsServicesBottomArchiveRoutes'
});