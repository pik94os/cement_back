Ext.define('Cement.view.goods.services.bottom.template.ConnectedDocumentsIncoming', {
	extend: 'Cement.view.goods.auto.lists.ConnectedDocuments',
	alias: 'widget.goods_services_bottom_templates_connected_documents_incoming',

	gridStore: 'goods.services.bottom.template.ConnectedDocumentsIncoming',
	gridStateId: 'stateGoodsAutoBottomTemplateConnectedDocumentsIncoming',

    printUrl: Cement.Config.url.goods.services.documents.printGrid,
    helpUrl: Cement.Config.url.goods.services.documents.help
});