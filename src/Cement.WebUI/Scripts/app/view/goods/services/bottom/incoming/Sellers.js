Ext.define('Cement.view.goods.services.bottom.incoming.Sellers', {
	extend: 'Cement.view.goods.auto.bottom.new_r.Senders',
	alias: 'widget.goods_services_bottom_incoming_sellers',
  gridStore: 'goods.services.bottom.incoming.Sellers',
  gridStateId: 'stateGoodsServicesBottomIncomingSellers',
  showFilterButton: false,

  printItemUrl: Cement.Config.url.goods.sellers.printItem,
  printUrl: Cement.Config.url.goods.sellers.printGrid,
  helpUrl: Cement.Config.url.goods.sellers.help
});