Ext.define('Cement.view.goods.services.bottom.incoming.Routes', {
    extend: 'Cement.view.products.price.bottom.NewRoutes',
    alias: 'widget.goods_services_bottom_incoming_routes',
    gridStore: 'goods.services.bottom.incoming.Routes',
    gridStateId: 'stateGoodsServicesBottomIncomingRoutes'
});