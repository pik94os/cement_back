Ext.define('Cement.view.goods.services.bottom.template.Routes', {
    extend: 'Cement.view.products.price.bottom.NewRoutes',
    alias: 'widget.goods_services_bottom_template_routes',
    gridStore: 'goods.services.bottom.template.Routes',
    gridStateId: 'stateGoodsServicesBottomTemplateRoutes'
});