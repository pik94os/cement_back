Ext.define('Cement.view.goods.services.bottom.template.Services', {
    extend: 'Cement.view.products.price.bottom.NewServices',
    alias: 'widget.goods_services_bottom_template_services',
    gridStore: 'goods.services.bottom.template.Services',
    gridStateId: 'stateGoodsServicesBottomTemplateServices'
});