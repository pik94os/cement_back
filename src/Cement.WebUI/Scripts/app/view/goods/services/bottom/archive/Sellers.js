Ext.define('Cement.view.goods.services.bottom.archive.Sellers', {
	extend: 'Cement.view.goods.auto.bottom.new_r.Senders',
	alias: 'widget.goods_services_bottom_archive_sellers',
  gridStore: 'goods.services.bottom.archive.Sellers',
  gridStateId: 'stateGoodsServicesBottomArchiveSellers',
  showFilterButton: false,

  printItemUrl: Cement.Config.url.goods.sellers.printItem,
  printUrl: Cement.Config.url.goods.sellers.printGrid,
  helpUrl: Cement.Config.url.goods.sellers.help
});