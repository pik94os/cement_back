Ext.define('Cement.view.goods.services.bottom.template.Buyers', {
	extend: 'Cement.view.goods.auto.bottom.new_r.Senders',
	alias: 'widget.goods_services_bottom_template_buyers',
  gridStore: 'goods.services.bottom.template.Buyers',
  gridStateId: 'stateGoodsServicesBottomTemplateBuyers',
  showFilterButton: false,

  printItemUrl: Cement.Config.url.goods.buyers.printItem,
  printUrl: Cement.Config.url.goods.buyers.printGrid,
  helpUrl: Cement.Config.url.goods.buyers.help
});