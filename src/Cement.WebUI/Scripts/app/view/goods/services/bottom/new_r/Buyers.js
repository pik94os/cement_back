Ext.define('Cement.view.goods.services.bottom.new_r.Buyers', {
	extend: 'Cement.view.goods.auto.bottom.new_r.Senders',
	alias: 'widget.goods_services_bottom_new_buyers',
  gridStore: 'goods.services.bottom.new_r.Buyers',
  gridStateId: 'stateGoodsServicesBottomNewBuyers',
  showFilterButton: false,

  printItemUrl: Cement.Config.url.goods.buyers.printItem,
  printUrl: Cement.Config.url.goods.buyers.printGrid,
  helpUrl: Cement.Config.url.goods.buyers.help
});