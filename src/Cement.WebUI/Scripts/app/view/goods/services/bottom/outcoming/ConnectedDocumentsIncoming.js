Ext.define('Cement.view.goods.services.bottom.outcoming.ConnectedDocumentsIncoming', {
	extend: 'Cement.view.goods.auto.lists.ConnectedDocuments',
	alias: 'widget.goods_services_bottom_outcoming_connected_documents_incoming',

	gridStore: 'goods.services.bottom.outcoming.ConnectedDocumentsIncoming',
	gridStateId: 'stateGoodsAutoBottomOutcomingConnectedDocumentsIncoming',

    printUrl: Cement.Config.url.goods.services.documents.printGrid,
    helpUrl: Cement.Config.url.goods.services.documents.help
});