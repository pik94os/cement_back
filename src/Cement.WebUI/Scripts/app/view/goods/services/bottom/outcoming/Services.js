Ext.define('Cement.view.goods.services.bottom.outcoming.Services', {
    extend: 'Cement.view.products.price.bottom.NewServices',
    alias: 'widget.goods_services_bottom_outcoming_services',
    gridStore: 'goods.services.bottom.outcoming.Services',
    gridStateId: 'stateGoodsServicesBottomOutcomingServices'
});