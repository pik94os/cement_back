Ext.define('Cement.view.goods.services.bottom.incoming.Services', {
    extend: 'Cement.view.products.price.bottom.NewServices',
    alias: 'widget.goods_services_bottom_incoming_services',
    gridStore: 'goods.services.bottom.incoming.Services',
    gridStateId: 'stateGoodsServicesBottomIncomingServices'
});