Ext.define('Cement.view.goods.services.bottom.archive.Services', {
    extend: 'Cement.view.products.price.bottom.NewServices',
    alias: 'widget.goods_services_bottom_archive_services',
    gridStore: 'goods.services.bottom.archive.Services',
    gridStateId: 'stateGoodsServicesBottomArchiveServices'
});