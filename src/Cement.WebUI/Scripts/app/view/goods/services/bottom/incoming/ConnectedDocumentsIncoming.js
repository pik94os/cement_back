Ext.define('Cement.view.goods.services.bottom.incoming.ConnectedDocumentsIncoming', {
	extend: 'Cement.view.goods.auto.lists.ConnectedDocuments',
	alias: 'widget.goods_services_bottom_incoming_connected_documents_incoming',

	gridStore: 'goods.services.bottom.incoming.ConnectedDocumentsIncoming',
	gridStateId: 'stateGoodsAutoBottomIncomingConnectedDocumentsIncoming',

    printUrl: Cement.Config.url.goods.services.documents.printGrid,
    helpUrl: Cement.Config.url.goods.services.documents.help
});