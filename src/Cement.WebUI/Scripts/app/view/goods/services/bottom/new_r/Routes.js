Ext.define('Cement.view.goods.services.bottom.new_r.Routes', {
    extend: 'Cement.view.products.price.bottom.NewRoutes',
    alias: 'widget.goods_services_bottom_new_routes',
    gridStore: 'goods.services.bottom.new_r.Routes',
    gridStateId: 'stateGoodsServicesBottomNewRoutes'
});