Ext.define('Cement.view.goods.services.bottom.new_r.Services', {
    extend: 'Cement.view.products.price.bottom.NewServices',
    alias: 'widget.goods_services_bottom_new_services',
    gridStore: 'goods.services.bottom.new_r.Services',
    gridStateId: 'stateGoodsServicesBottomNewServices'
});