Ext.define('Cement.view.goods.services.bottom.new_r.ConnectedDocumentsIncoming', {
	extend: 'Cement.view.goods.auto.lists.ConnectedDocuments',
	alias: 'widget.goods_services_bottom_new_connected_documents_incoming',

	gridStore: 'goods.services.bottom.new_r.ConnectedDocumentsIncoming',
	gridStateId: 'stateGoodsAutoBottomNewConnectedDocumentsIncoming',

    printUrl: Cement.Config.url.goods.services.documents.printGrid,
    helpUrl: Cement.Config.url.goods.services.documents.help
});