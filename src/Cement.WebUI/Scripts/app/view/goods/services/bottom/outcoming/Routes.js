Ext.define('Cement.view.goods.services.bottom.outcoming.Routes', {
    extend: 'Cement.view.products.price.bottom.NewRoutes',
    alias: 'widget.goods_services_bottom_outcoming_routes',
    gridStore: 'goods.services.bottom.outcoming.Routes',
    gridStateId: 'stateGoodsServicesBottomOutcomingRoutes'
});