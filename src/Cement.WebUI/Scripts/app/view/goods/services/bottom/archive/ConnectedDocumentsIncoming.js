Ext.define('Cement.view.goods.services.bottom.archive.ConnectedDocumentsIncoming', {
	extend: 'Cement.view.goods.auto.lists.ConnectedDocuments',
	alias: 'widget.goods_services_bottom_archive_connected_documents_incoming',

	gridStore: 'goods.services.bottom.archive.ConnectedDocumentsIncoming',
	gridStateId: 'stateGoodsAutoBottomArchiveConnectedDocumentsIncoming',

    printUrl: Cement.Config.url.goods.services.documents.printGrid,
    helpUrl: Cement.Config.url.goods.services.documents.help
});