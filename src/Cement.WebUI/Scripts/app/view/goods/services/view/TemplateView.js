Ext.define('Cement.view.goods.services.view.TemplateView', {
	extend: 'Cement.view.basic.Form',
	alias: 'widget.goods_services_view_template_view',
	fieldsDisabled: true,
	buttons: null,
	border: 0,
	tbar: Cement.Config.modelViewButtons,
	layout: 'autocontainer',
	bbar: null,
    noUserFieldsetsControl: false,
    bodyCls: 'model-form',

	getSellerFields: function () {
		return [{
			fieldLabel: 'Наименование',
			name: 'p_seller_name',
			disabled: true
		}, {
			fieldLabel: 'Адрес',
			name: 'p_seller_address',
			disabled: true
		}, {
			fieldLabel: 'Контактное лицо',
			name: 'p_seller_contact',
			disabled: true
		}];
	},

	getCommonFields: function () {
		return [{
			name: 'p_start_time',
			fieldLabel: 'Дата, время начала',
			disabled: true
		}, {
			name: 'p_end_time',
			fieldLabel: 'Дата, время окончания',
			disabled: true
		}, {
			name: 'p_fact_start_time',
			fieldLabel: 'Фактическая дата, время начала',
			disabled: true
		}, {
			name: 'p_fact_end_time',
			fieldLabel: 'Фактическая дата, время окончания',
			disabled: true
		}]
	},

	getServiceFields: function () {
		return [{
			fieldLabel: 'Наименование',
			name: 'p_service_name',
			disabled: true
		}, {
			fieldLabel: 'Единица измерения',
			name: 'p_service_unit_display',
			disabled: true
		}];
	},

	getBuyerFields: function () {
		return [{
			fieldLabel: 'Наименование',
			name: 'p_buyer_name',
			disabled: true
		}, {
			fieldLabel: 'Адрес',
			name: 'p_buyer_address',
			disabled: true
		}, {
			fieldLabel: 'Контактное лицо',
			name: 'p_buyer_contact',
			disabled: true
		}];
	},

	getFirstFields: function () {
		return [{
			fieldLabel: 'Наименование',
			disabled: true,
			name: 'p_name'
		}];
	},

	getItems: function () {
		return [{
				xtype: 'panel',
				border: 0,
				layout: 'anchor',
				defaultType: 'textfield',
				anchor: '100%',
				bodyPadding: '10 10 10 5',
				autoScroll: true,
				defaults: {
					anchor: '100%',
					labelWidth: 110
				},
				items: [{
					xtype: 'fieldset',
					layout: 'anchor',
					defaults: {
						xtype: 'textfield',
						anchor: '100%'
					},
					border: 0,
					items: this.getFirstFields()
				}, {
					xtype: 'fieldset',
					collapsed: true,
					collapsible: true,
					title: 'Общие данные',
					defaults: {
						xtype: 'textfield',
						anchor: '100%'
					},
					items: this.getCommonFields()
				}, {
					xtype: 'fieldset',
					collapsed: true,
					collapsible: true,
					title: 'Продавец',
					defaults: {
						xtype: 'textfield',
						anchor: '100%'
					},
					items: this.getSellerFields()
				}, {
					xtype: 'fieldset',
					collapsed: true,
					collapsible: true,
					title: 'Услуга',
					defaults: {
						xtype: 'textfield',
						anchor: '100%'
					},
					items: this.getServiceFields()
				}, {
					xtype: 'fieldset',
					collapsed: true,
					collapsible: true,
					title: 'Покупатель',
					defaults: {
						xtype: 'textfield',
						anchor: '100%'
					},
					items: this.getBuyerFields()
				}]
		}]
	},

	initComponent: function () {
        Ext.apply(this, {
            items: [
                {
                    xtype: 'panel',
                    border: 0,
                    layout: 'anchor',
                    defaultType: 'textfield',
                    anchor: '100%',
                    bodyPadding: '10 10 10 5',
                    autoScroll: true,
                    defaults: {
                        anchor: '100%',
                        labelWidth: 110
                    },
                    items: this.getItems()
                }
            ]
        });
        this.callParent(arguments);
    }
});
