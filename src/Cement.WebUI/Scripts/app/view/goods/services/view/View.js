Ext.define('Cement.view.goods.services.view.View', {
	extend: 'Cement.view.goods.services.view.TemplateView',
	alias: 'widget.goods_services_view_view',

	getServiceFields: function () {
		return [{
			fieldLabel: 'Наименование',
			name: 'p_service_name',
			disabled: true
		}, {
			fieldLabel: 'Единица измерения',
			name: 'p_service_unit_display',
			disabled: true
		}, {
			fieldLabel: 'Количество',
			name: 'p_service_count',
			disabled: true
		}, {
			fieldLabel: 'Фактическое количество',
			name: 'p_service_fact_count',
			disabled: true
		}, {
			fieldLabel: 'Цена',
			name: 'p_service_price',
			disabled: true
		}, {
			fieldLabel: 'Налог',
			name: 'p_service_tax',
			disabled: true
		}, {
			fieldLabel: 'Сумма',
			name: 'p_service_sum',
			disabled: true
		}];
	},

	getFirstFields: function () {
		return [{
			fieldLabel: 'Номер заявки',
			disabled: true,
			name: 'p_number'
		}, {
			fieldLabel: 'Дата заявки',
			disabled: true,
			name: 'p_date'
		}, {
			fieldLabel: 'Статус',
			disabled: true,
			name: 'p_status_display'
		}];
	}
});
