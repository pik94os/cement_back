Ext.define('Cement.view.goods.services.view.Archive', {
	extend: 'Cement.view.goods.services.view.View',
	alias: 'widget.goods_services_view_archive',

	getFirstFields: function () {
		return [{
			fieldLabel: 'Номер заявки',
			disabled: true,
			name: 'p_number'
		}, {
			fieldLabel: 'Дата заявки',
			disabled: true,
			name: 'p_date'
		}, {
			fieldLabel: 'Статус',
			disabled: true,
			name: 'p_status_display'
		}, {
			fieldLabel: 'Тип заявки',
			disabled: true,
			name: 'p_kind'
		}];
	}
});