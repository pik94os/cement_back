Ext.define('Cement.view.goods.Rerequest', {
  rerequestItem: function (grid, record) {
  	var bgrid = grid.up('basicgrid'),
  		newModel = Ext.create(record.$className);
  	newModel.set('p_request', record.get('id'));
  	newModel.set('p_request_display', '№' + record.get('p_number') + ' от ' + record.get('p_date'))
  	bgrid.fireEvent('edititem', newModel, record.$className);
    // bgrid.fireEvent('createitem', Ext.getStore(bgrid.gridStore).model.$className);
  }
});