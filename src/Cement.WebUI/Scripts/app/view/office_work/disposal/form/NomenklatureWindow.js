Ext.define('Cement.view.office_work.documents.form.NomenklatureWindow', {
  extend: 'Ext.window.Window',
  xtype: 'office_work_documents_form_nomenklature_window',
  title: 'Номенклатура дел',
  layout: 'fit',
  width: 400,
  height: 450,
  modal: true,

  bbar: [{
    text: 'Сохранить',
    action: 'confirm'
  }, {
    text: 'Отмена',
    action: 'cancel'
  }],

  items: [{
    xtype: 'treepanel',
    cls: 'tree-no-border-bottom',
    store: 'office_work.auxiliary.Nomenklature',
    border: 0,
    displayField: 'p_name',
    layout: 'fit',
    rootVisible: false
  }],

  sendSelection: function () {
    var sel = this.down('treepanel').getSelectionModel().getSelection();
    if (sel.length > 0) {
      this.fireEvent('itemselected', sel[0]);
    }
  },

  closeWindow: function () {
    this.hide();
  },

  initComponent: function () {
    this.callParent(arguments);
    this.on('beforeclose', function () {
      this.hide();
      return false;
    });
    this.down('button[action=cancel]').on('click', function () {
      this.closeWindow();
    }, this);

    this.down('button[action=confirm]').on('click', function () {
      this.sendSelection();
    }, this);

    this.down('treepanel').on('itemdblclick', function (self, record) {
      this.sendSelection();
    }, this);

    this.addEvents('itemselected');

    var me = this;
    this.on('afterrender', function () {
      me.down('treepanel').getStore().getRootNode().expand();
    }, this);

  }
});