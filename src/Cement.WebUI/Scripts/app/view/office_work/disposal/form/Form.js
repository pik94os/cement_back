Ext.define('Cement.view.office_work.disposal.form.Form', {
    extend: 'Cement.view.office_work.documents.form.Form',
    alias: 'widget.office_work_disposal_form_form',
    url: Cement.Config.url.office_work.disposal.new_d.saveUrl,
    method: Cement.Config.url.office_work.disposal.new_d.saveMethod,
    getItems: function () {
        return [{
            xtype: 'hiddenfield',
            name: 'id'
        }, {
            xtype: 'fieldset',
            border: 0,
            margin: 0,
            defaults: {
                xtype: 'textfield',
                anchor: '100%',
                labelWidth: 200,
                labelAlign: 'right'
            },
            items: [{
                name: 'p_number',
                fieldLabel: 'Номер',
                margin: '5 100'
            }, {
                name: 'p_date',
                fieldLabel: 'Дата',
                xtype: 'datefield',
                format: 'd.m.Y',
                margin: '5 100'
            },
              //this.getCombo('Вид', 'p_kind', this.kindsStoreId),
              this.getSelectorField('Номенклатура дел', 'p_nomenklature', 'show_nomenklature_window'), {
                  name: 'p_theme',
                  fieldLabel: 'Тема документа'
              }, {
                  xtype: 'textarea',
                  fieldLabel: 'Краткое содержание',
                  name: 'p_content',
                  height: 150
              },
              this.getSelectorField('Автор', 'p_author', 'show_author_window'),
              this.getSelectorField('На подпись', 'p_sign_for', 'show_sign_for_window'),
              this.getSelectorField('Сотрудники', 'p_employees', 'show_employees_window'),
              this.getSelectorField('Связанные документы', 'p_documents', 'show_documents_window'),
              //{
              //  xtype: 'datefield',
              //  name: 'p_deadline',
              //  fieldLabel: 'Срок рассмотрения'
              //},
              this.getCombo('Важность', 'p_importance', this.importancesStoreId),
              this.getCombo('Секретность', 'p_security', this.securitiesStoreId),
              this.getFileField('Импорт', 'p_import', 0)
            ]
        }];
    },

    initComponent: function () {
        this.callParent(arguments);

        this.down('button[action=show_employees_window]').on('click', function () {
            this.createEmployeeWindow();
            this.employeeWindow.show();
        }, this);
    },

    createEmployeeWindow: function () {
        if (!this.employeeWindow) {
            this.employeeWindow = Ext.create('Cement.view.personal.messages.form.EmployeeWindow');
            this.employeeWindow.on('selected', function (store) {
                var json = [],
                  str = [];
                store.each(function (rec) {
                    json.push(rec.get('id'));
                    str.push(rec.get('p_name'));
                });
                this.down('textfield[name=p_employees_display]').setValue(str.join(', '));
                this.down('hiddenfield[name=p_employees]').setValue(Ext.JSON.encode(json));
                this.employeeWindow.hide();
            }, this);
        }
    },
});