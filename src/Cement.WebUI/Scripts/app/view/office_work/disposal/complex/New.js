Ext.define('Cement.view.office_work.disposal.complex.New', {
  extend: 'Cement.view.basic.Complex',
  alias: 'widget.office_work_disposal_complex_new',
  border: 0,
  enableDetail: false,
  autoLoadStore: false,
  bodyPadding: 0,
  padding: 0,
  header: false,
  rowsCount: 2,
  title: 'Новые',
  tabTitle: 'Новые',
  cls: 'no-side-borders',
  topXType: 'office_work_disposal_lists_new',
  topDetailType: 'Cement.view.office_work.disposal.View',
  bottomTitle: 'Связанные документы',
  bottomTabs: [{
    title: 'Связанные документы',
    xtype: 'office_work_disposal_bottom_new_connected_documents',
    detailType: 'Cement.view.goods.auto.view.ConnectedDocument',
    constantFilterParam: 'p_base_doc',
    childrenConstantFilterParam: 'p_base_doc',
    border: 0
  }]
});