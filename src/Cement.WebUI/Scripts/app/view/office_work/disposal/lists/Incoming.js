Ext.define('Cement.view.office_work.disposal.lists.Incoming', {
    extend: 'Cement.view.office_work.disposal.lists.New',
    alias: 'widget.office_work_disposal_lists_incoming',

    gridStore: 'office_work.disposal.Incoming',
    gridStateId: 'stateOfficeWorkDisposalIncoming',

    printUrl: Cement.Config.url.office_work.disposal.incoming.printGrid,
    helpUrl: Cement.Config.url.office_work.disposal.incoming.help,
    deleteUrl: Cement.Config.url.office_work.disposal.incoming.deleteUrl,
    archiveItemUrl: Cement.Config.url.office_work.disposal.incoming.archiveItem,

  shownTitle: null,

  getActionColumns: function () {
    return {
        xtype: 'rowactions',
        hideable: false,
        resizeable: false,
        locked: true,
        width: 23,
        actions: [
        //{
        //    iconCls: 'icon-delete-item',
        //    qtip: 'Удалить',
        //    callback: this.deleteItem
        //},
        {
            iconCls: 'icon-archive-item',
            qtip: 'В архив',
            callback: this.archiveItem
        }
        ],
        keepSelection: true
    };
}
});