Ext.define('Cement.view.office_work.disposal.lists.New', {
    extend: 'Cement.view.basic.Grid',
    alias: 'widget.office_work_disposal_lists_new',
    autoLoadStore: false,

    bbarText: 'Показаны документы {0} - {1} из {2}',
    bbarEmptyMsg: 'Нет документов',
    bbarUsersText: 'Документов на странице: ',
  
    gridStore: 'office_work.disposal.New',
    gridStateId: 'stateOfficeWorkDisposalNew',
    
    printUrl: Cement.Config.url.office_work.disposal.new_d.printGrid,
    helpUrl: Cement.Config.url.office_work.disposal.new_d.help,
    deleteUrl: Cement.Config.url.office_work.disposal.new_d.deleteUrl,

    shownTitle: null,

    getActionColumns: function () {
        return {
            xtype: 'rowactions',
            hideable: false,
            resizeable: false,
            locked: true,
            width: 46,
            actions: [
            {
                iconCls: 'icon-edit-item',
                qtip: 'Редактировать',
                callback: this.editItem
            },
            {
                iconCls: 'icon-delete-item',
                qtip: 'Удалить',
                callback: this.deleteItem
            }
            ],
            keepSelection: true
        };
    },

    getFilterItems: function () {
        return [{
            text: 'Номер',
            kind: 'selector',
            field_name: 'p_number',
            checked: true
        }, {
            text: 'Дата',
            kind: 'selector',
            field_name: 'p_date',
            checked: true
        }, {
            text: 'Автор',
            kind: 'selector',
            field_name: 'p_author_display',
            checked: true
        }, {
            text: 'На подпись',
            kind: 'selector',
            field_name: 'p_for_sign_display',
            checked: true
        }, {
            text: 'Тема',
            kind: 'selector',
            field_name: 'p_theme',
            checked: true
        }];
    },

    getGridColumns: function () {
        var result = [
            { text: 'Номер', dataIndex: 'p_number', width: 150 },
            { text: 'Дата', dataIndex: 'p_date', width: 80 },
            { text: 'Автор', dataIndex: 'p_author_display', width: 150 },
            { text: 'На подпись', dataIndex: 'p_for_sign_display', width: 150 },
            { text: 'Тема', dataIndex: 'p_theme', minWidth: 200, flex: 1 },
            { text: '', dataIndex: 'p_security', renderer: this.docStatusColRender, width: 43 }
        ];
        return this.mergeActions(result);
    },

    initComponent: function () {
        Ext.apply(this, {
            title: this.shownTitle
        });
        this.callParent(arguments);
    }
});