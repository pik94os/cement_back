Ext.define('Cement.view.office_work.disposal.lists.Outcoming', {
    extend: 'Cement.view.office_work.disposal.lists.New',
    alias: 'widget.office_work_disposal_lists_outcoming',

    gridStore: 'office_work.disposal.Outcoming',
    gridStateId: 'stateOfficeWorkDisposalOutcoming',

    printUrl: Cement.Config.url.office_work.disposal.outcoming.printGrid,
    helpUrl: Cement.Config.url.office_work.disposal.outcoming.help,
    deleteUrl: Cement.Config.url.office_work.disposal.outcoming.deleteUrl,
    archiveItemUrl: Cement.Config.url.office_work.disposal.outcoming.archiveItem,

  shownTitle: null,

  getActionColumns: function () {
    return {
        xtype: 'rowactions',
        hideable: false,
        resizeable: false,
        locked: true,
        width: 23,
        actions: [
        //{
        //    iconCls: 'icon-delete-item',
        //    qtip: 'Удалить',
        //    callback: this.deleteItem
        //},
        {
            iconCls: 'icon-archive-item',
            qtip: 'В архив',
            callback: this.archiveItem
        }
        ],
        keepSelection: true
    };
}
});