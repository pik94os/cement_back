Ext.define('Cement.view.office_work.disposal.bottom.outcoming.Routes', {
    extend: 'Cement.view.products.price.bottom.NewRoutes',
    alias: 'widget.office_work_disposal_bottom_outcoming_routes',
    gridStore: 'office_work.disposal.bottom.outcoming.Routes',
    gridStateId: 'stateOfficeWorkDisposalBottomOutcomingRoutes'
});