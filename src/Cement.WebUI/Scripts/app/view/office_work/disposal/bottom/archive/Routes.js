Ext.define('Cement.view.office_work.disposal.bottom.archive.Routes', {
    extend: 'Cement.view.products.price.bottom.NewRoutes',
    alias: 'widget.office_work_disposal_bottom_archive_routes',
    gridStore: 'office_work.disposal.bottom.archive.Routes',
    gridStateId: 'stateOfficeWorkDisposalBottomArchiveRoutes'
});