Ext.define('Cement.view.office_work.disposal.bottom.incoming.ConnectedDocuments', {
	extend: 'Cement.view.goods.auto.lists.ConnectedDocuments',
	alias: 'widget.office_work_disposal_bottom_incoming_connected_documents',

	gridStore: 'office_work.disposal.bottom.incoming.ConnectedDocuments',
	gridStateId: 'stateOfficeWorkDisposalIncomingConnectedDocuments',

	printUrl: Cement.Config.url.office_work.disposal.documents.printGrid,
	helpUrl: Cement.Config.url.office_work.disposal.documents.help
});