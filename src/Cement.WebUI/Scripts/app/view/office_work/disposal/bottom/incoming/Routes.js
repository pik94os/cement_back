Ext.define('Cement.view.office_work.disposal.bottom.incoming.Routes', {
    extend: 'Cement.view.products.price.bottom.NewRoutes',
    alias: 'widget.office_work_disposal_bottom_incoming_routes',
    gridStore: 'office_work.disposal.bottom.incoming.Routes',
    gridStateId: 'stateOfficeWorkDisposalBottomIncomingRoutes'
});