Ext.define('Cement.view.office_work.disposal.bottom.new_d.Routes', {
    extend: 'Cement.view.products.price.bottom.NewRoutes',
    alias: 'widget.office_work_disposal_bottom_new_routes',
    gridStore: 'office_work.disposal.bottom.new_d.Routes',
    gridStateId: 'stateOfficeWorkDisposalBottomNewRoutes'
});