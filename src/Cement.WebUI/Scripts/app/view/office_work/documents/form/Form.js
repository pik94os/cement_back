Ext.define('Cement.view.office_work.documents.form.Form', {
  extend: 'Cement.view.basic.Form',
  alias: 'widget.office_work_documents_form_form',
  fieldsDisabled: false,
  url: Cement.Config.url.office_work.documents.new_d.saveUrl,
  method: Cement.Config.url.office_work.documents.new_d.saveMethod,
  isFilter: false,
  layout: 'fit',
  allowBlankFields: true,
  showSaveAndSignButton: true,
  saveButtonDisabled: false,

  kindsStoreId: 'office_work.auxiliary.Kinds',
  importancesStoreId: 'office_work.auxiliary.Importances',
  securitiesStoreId: 'office_work.auxiliary.Securities',

  getItems: function () {
    return [{
      xtype: 'hiddenfield',
      name: 'id'
    }, {
      xtype: 'fieldset',
      border: 0,
      margin: 0,
      defaults: {
        xtype: 'textfield',
        anchor: '100%',
        labelWidth: 200,
        labelAlign: 'right'
      },
      items: [{
        name: 'p_number',
        fieldLabel: 'Номер',
        margin: '5 100'
      }, {
        name: 'p_date',
        fieldLabel: 'Дата',
        xtype: 'datefield',
        format: 'd.m.Y',
        margin: '5 100'
      },
        this.getCombo('Вид', 'p_kind', this.kindsStoreId),
        this.getSelectorField('Номенклатура дел', 'p_nomenklature', 'show_nomenklature_window'), {
          name: 'p_theme',
          fieldLabel: 'Тема документа'
      },{
        xtype: 'textarea',
        fieldLabel: 'Краткое содержание',
        name: 'p_content',
        height: 150
      },
        this.getSelectorField('Автор', 'p_author', 'show_author_window'),
        this.getSelectorField('На подпись', 'p_sign_for', 'show_sign_for_window'),
        this.getSelectorField('Связанные документы', 'p_documents', 'show_documents_window'), {
          xtype: 'datefield',
          name: 'p_deadline',
          fieldLabel: 'Срок рассмотрения'
        },
        this.getCombo('Важность', 'p_importance', this.importancesStoreId),
        this.getCombo('Секретность', 'p_security', this.securitiesStoreId),
        this.getFileField('Импорт', 'p_import', 0)
      ]
    }];
  },

  initComponent: function () {
    this.callParent(arguments);

    this.down('button[action=show_nomenklature_window]').on('click', function () {
      this.createNomWindow();
      this.nomWindow.show();
    }, this);

    this.down('button[action=show_author_window]').on('click', function () {
      this.createAuthorWindow();
      this.authorWindow.show();
    }, this);

    this.down('button[action=show_sign_for_window]').on('click', function () {
      this.createSignForWindow();
      this.signForWindow.show();
    }, this);

    this.down('button[action=show_documents_window]').on('click', function () {
      this.createDocsWindow();
      this.docsWindow.show();
    }, this);
  },

  createDocsWindow: function () {
    if (!this.docsWindow) {
      this.docsWindow = Ext.create('Cement.view.common.ConnectedDocumentsWindow');
      this.docsWindow.on('documentsselected', function (ids, names) {
        this.down('textfield[name=p_documents_display]').setValue(names.join(', '));
        this.down('hiddenfield[name=p_documents]').setValue(Ext.JSON.encode(ids));
        this.docsWindow.hide();
      }, this);
    }
  },

  createSignForWindow: function () {
    if (!this.signForWindow) {
      this.signForWindow = Ext.create('Cement.view.common.EmployeeSelectWindow');
      this.signForWindow.on('signitem', function (id, name) {
        this.down('textfield[name=p_sign_for_display]').setValue(name);
        this.down('hiddenfield[name=p_sign_for]').setValue(id);
        this.signForWindow.hide();
      }, this);
    }
  },

  createNomWindow: function () {
    if (!this.nomWindow) {
      this.nomWindow = Ext.create('Cement.view.office_work.documents.form.NomenklatureWindow');
      this.nomWindow.on('itemselected', function (rec) {
        this.down('textfield[name=p_nomenklature_display]').setValue(rec.get('p_name'));
        this.down('hiddenfield[name=p_nomenklature]').setValue(rec.get('id'));
        this.nomWindow.hide();
      }, this);
    }
  },

  createAuthorWindow: function () {
    if (!this.authorWindow) {
      this.authorWindow = Ext.create('Cement.view.common.EmployeeSelectWindow');
      this.authorWindow.on('signitem', function (id, name) {
        this.down('textfield[name=p_author_display]').setValue(name);
        this.down('hiddenfield[name=p_author]').setValue(id);
        this.authorWindow.hide();
      }, this);
    }
  },

  afterRecordLoad: function (rec) {
    this.createAuthorWindow();
    this.createNomWindow();
    this.createDocsWindow();
    this.createSignForWindow();
  }
});