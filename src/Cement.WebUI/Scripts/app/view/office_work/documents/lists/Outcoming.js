Ext.define('Cement.view.office_work.documents.lists.Outcoming', {
  extend: 'Cement.view.office_work.documents.lists.New',
  alias: 'widget.office_work_documents_lists_outcoming',

  gridStore: 'office_work.documents.Outcoming',
  gridStateId: 'stateOfficeWorkDocumentsOutcoming',

  printUrl: Cement.Config.url.office_work.documents.outcoming.printGrid,
  helpUrl: Cement.Config.url.office_work.documents.outcoming.help,
  deleteUrl: Cement.Config.url.office_work.documents.outcoming.deleteUrl,
  archiveItemUrl: Cement.Config.url.office_work.documents.outcoming.archiveItem,

  shownTitle: null,

  getActionColumns: function () {
    return {
        xtype: 'rowactions',
        hideable: false,
        resizeable: false,
        locked: true,
        width: 23,
        actions: [
        //{
        //    iconCls: 'icon-delete-item',
        //    qtip: 'Удалить',
        //    callback: this.deleteItem
        //},
        {
            iconCls: 'icon-archive-item',
            qtip: 'В архив',
            callback: this.archiveItem
        }
        ],
        keepSelection: true
    };
}
});