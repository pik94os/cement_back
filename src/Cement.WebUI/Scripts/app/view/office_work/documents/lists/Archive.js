Ext.define('Cement.view.office_work.documents.lists.Archive', {
  extend: 'Cement.view.basic.Grid',
  alias: 'widget.office_work_documents_lists_archive',
  autoLoadStore: false,

  bbarText: 'Показаны документы {0} - {1} из {2}',
  bbarEmptyMsg: 'Нет документов',
  bbarUsersText: 'Документов на странице: ',

  gridStore: 'office_work.documents.Archive',
  gridStateId: 'stateOfficeWorkDocumentsArchive',

  printUrl: Cement.Config.url.office_work.documents.archive.printGrid,
  helpUrl: Cement.Config.url.office_work.documents.archive.help,
  deleteUrl: Cement.Config.url.office_work.documents.archive.deleteUrl,
  unArchiveItemUrl: Cement.Config.url.office_work.documents.archive.unArchiveItem,

  shownTitle: null,

  getActionColumns: function () {
    return {
        xtype: 'rowactions',
        hideable: false,
        resizeable: false,
        locked: true,
        width: 46,
        actions: [
        {
            iconCls: 'icon-unarchive-item',
            qtip: 'Из архива',
            callback: this.unArchiveItem
        },
        {
            iconCls: 'icon-delete-item',
            qtip: 'Удалить',
            callback: this.deleteItem
        }
        ],
        keepSelection: true
    };
},

getFilterItems: function () {
    return [{
        text: 'Вид',
        kind: 'selector',
        field_name: 'p_kind_display',
        checked: true
    }, {
        text: 'Тип',
        kind: 'selector',
        field_name: 'p_type_display',
        checked: true
    }, {
        text: 'Номер',
        kind: 'selector',
        field_name: 'p_supplier_name',
        checked: true
    }, {
        text: 'Дата',
        kind: 'selector',
        field_name: 'p_date',
        checked: true
    }, {
        text: 'Автор',
        kind: 'selector',
        field_name: 'p_author_display',
        checked: true
    }, {
        text: 'На подпись',
        kind: 'selector',
        field_name: 'p_for_sign_display',
        checked: true
    }, {
        text: 'Тема',
        kind: 'selector',
        field_name: 'p_theme',
        checked: true
    }, {
        text: 'Задача',
        kind: 'selector',
        field_name: 'p_task_display',
        checked: true
    }];
},

getGridColumns: function () {
    var result = [
        { text: 'Тип', dataIndex: 'p_type_display', width: 60, locked: true },
        { text: 'Вид', dataIndex: 'p_kind_display', width: 80, locked: true },
        { text: 'Номер', dataIndex: 'p_number', width: 150 },
        { text: 'Дата', dataIndex: 'p_date', width: 150 },
        { text: 'Автор', dataIndex: 'p_author_display', width: 150 },
        { text: 'На подпись', dataIndex: 'p_for_sign_display', width: 150 },
        { text: 'Тема', dataIndex: 'p_theme', width: 350 },
        { text: 'Задача', dataIndex: 'p_task_display', width: 150 },
        { text: '', dataIndex: 'p_security', renderer: this.docStatusColRender, width: 43 }
    ];
    return this.mergeActions(result);
},

initComponent: function () {
    Ext.apply(this, {
        title: this.shownTitle
    });
    this.callParent(arguments);
}
});