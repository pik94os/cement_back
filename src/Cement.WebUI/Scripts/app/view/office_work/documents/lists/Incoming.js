Ext.define('Cement.view.office_work.documents.lists.Incoming', {
  extend: 'Cement.view.office_work.documents.lists.New',
  alias: 'widget.office_work_documents_lists_incoming',

  gridStore: 'office_work.documents.Incoming',
  gridStateId: 'stateOfficeWorkDocumentsIncoming',

  printUrl: Cement.Config.url.office_work.documents.incoming.printGrid,
  helpUrl: Cement.Config.url.office_work.documents.incoming.help,
  deleteUrl: Cement.Config.url.office_work.documents.incoming.deleteUrl,
  archiveItemUrl: Cement.Config.url.office_work.documents.incoming.archiveItem,

  shownTitle: null,

  getActionColumns: function () {
    return {
        xtype: 'rowactions',
        hideable: false,
        resizeable: false,
        locked: true,
        width: 23,
        actions: [
        //{
        //    iconCls: 'icon-delete-item',
        //    qtip: 'Удалить',
        //    callback: this.deleteItem
        //},
        {
            iconCls: 'icon-archive-item',
            qtip: 'В архив',
            callback: this.archiveItem
        }
        ],
        keepSelection: true
    };
}
});