Ext.define('Cement.view.office_work.documents.complex.New', {
  extend: 'Cement.view.basic.Complex',
  alias: 'widget.office_work_documents_complex_new',
  border: 0,
  enableDetail: false,
  autoLoadStore: false,
  priceId: null,
  bodyPadding: 0,
  padding: 0,
  header: false,
  title: null,
  rowsCount: 3,
  title: 'Новые',
  tabTitle: 'Новые',
  cls: 'no-side-borders',
  topXType: 'office_work_documents_lists_new',
  topDetailType: 'Cement.view.office_work.documents.View',
  bottomTitle: 'Связанные документы',
  bottomTabs: [{
    title: 'Связанные документы',
    xtype: 'office_work_documents_bottom_new_connected_documents_incoming',
    detailType: 'Cement.view.goods.auto.view.ConnectedDocument',
    constantFilterParam: 'p_base_doc',
    childrenConstantFilterParam: 'p_base_doc',
    border: 0
  }],
  subBottomTitle: 'Справочник',
  subBottomTabs: [{
    title: 'Маршрут',
    shownTitle: 'Маршрут',
    xtype: 'office_work_documents_bottom_new_routes',
    detailType: 'Cement.view.products.price.RouteView'
  }]
});