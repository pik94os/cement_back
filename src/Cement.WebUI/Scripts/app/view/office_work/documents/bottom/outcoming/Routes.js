Ext.define('Cement.view.office_work.documents.bottom.outcoming.Routes', {
    extend: 'Cement.view.products.price.bottom.NewRoutes',
    alias: 'widget.office_work_documents_bottom_outcoming_routes',
    gridStore: 'office_work.documents.bottom.outcoming.Routes',
    gridStateId: 'stateOfficeWorkDocumentsBottomOutcomingRoutes'
});