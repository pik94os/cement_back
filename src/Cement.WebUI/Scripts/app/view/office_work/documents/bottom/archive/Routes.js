Ext.define('Cement.view.office_work.documents.bottom.archive.Routes', {
    extend: 'Cement.view.products.price.bottom.NewRoutes',
    alias: 'widget.office_work_documents_bottom_archive_routes',
    gridStore: 'office_work.documents.bottom.archive.Routes',
    gridStateId: 'stateOfficeWorkDocumentsBottomArchiveRoutes'
});