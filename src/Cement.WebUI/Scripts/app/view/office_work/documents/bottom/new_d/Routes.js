Ext.define('Cement.view.office_work.documents.bottom.new_d.Routes', {
    extend: 'Cement.view.products.price.bottom.NewRoutes',
    alias: 'widget.office_work_documents_bottom_new_routes',
    gridStore: 'office_work.documents.bottom.new_d.Routes',
    gridStateId: 'stateOfficeWorkDocumentsBottomNewRoutes'
});