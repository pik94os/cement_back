Ext.define('Cement.view.office_work.documents.bottom.incoming.ConnectedDocumentsIncoming', {
	extend: 'Cement.view.goods.auto.lists.ConnectedDocuments',
	alias: 'widget.office_work_documents_bottom_incoming_connected_documents_incoming',

	gridStore: 'office_work.documents.bottom.incoming.ConnectedDocumentsIncoming',
	gridStateId: 'stateOfficeWorkDocumentsIncomingConnectedDocumentsIncoming',

    printUrl: Cement.Config.url.office_work.documents.documents.printGrid,
    helpUrl: Cement.Config.url.office_work.documents.documents.help
});