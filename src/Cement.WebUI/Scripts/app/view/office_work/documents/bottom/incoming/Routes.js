Ext.define('Cement.view.office_work.documents.bottom.incoming.Routes', {
    extend: 'Cement.view.products.price.bottom.NewRoutes',
    alias: 'widget.office_work_documents_bottom_incoming_routes',
    gridStore: 'office_work.documents.bottom.incoming.Routes',
    gridStateId: 'stateOfficeWorkDocumentsBottomIncomingRoutes'
});