Ext.define('Cement.view.office_work.correspondention.View', {
  extend: 'Cement.view.basic.Form',
  alias: 'widget.office_work_correspondention_view',
  fieldsDisabled: true,
  buttons: null,
  border: 0,
  tbar: Cement.Config.modelViewButtons,
  layout: 'autocontainer',
  bbar: null,
    noUserFieldsetsControl: false,
    bodyCls: 'model-form',

  getItems: function () {
    return [{
        xtype: 'panel',
        border: 0,
        layout: 'anchor',
        defaultType: 'textfield',
        anchor: '100%',
        bodyPadding: '10 10 10 5',
        autoScroll: true,
        defaults: {
          anchor: '100%',
          labelWidth: 110
        },
        items: [{
          xtype: 'fieldset',
          layout: 'anchor',
          defaults: {
            xtype: 'textfield',
            anchor: '100%'
          },
          border: 0,
          items: [{
            name: 'p_number',
            disabled: true,
            fieldLabel: 'Номер'
          }, {
            name: 'p_date',
            disabled: true,
            fieldLabel: 'Дата'
          }, {
            name: 'p_kind_display',
            disabled: true,
            fieldLabel: 'Вид'
          }]
        }, {
          xtype: 'fieldset',
          collapsed: true,
          collapsible: true,
          title: 'Общие данные',
          defaults: {
            xtype: 'textfield',
            anchor: '100%'
          },
          items: [{
            disabled: true,
            name: 'p_up_date',
            fieldLabel: 'Срок рассмотрения'
          }, {
            disabled: true,
            name: 'p_theme',
            fieldLabel: 'Тема документа',
            xtype: 'textarea',
            labelAlign: 'top',
            height: 100
          }, {
            disabled: true,
            name: 'p_content',
            fieldLabel: 'Краткое содержание',
            xtype: 'textarea',
            labelAlign: 'top',
            height: 100
          }]
        }, {
          xtype: 'fieldset',
          collapsed: true,
          collapsible: true,
          title: 'Номенклатура дел',
          defaults: {
            xtype: 'textfield',
            anchor: '100%'
          },
          items: [{
            disabled: true,
            name: 'p_nom_index',
            fieldLabel: 'Индекс дела'
          }, {
            disabled: true,
            name: 'p_nom_title',
            fieldLabel: 'Заголовок дела'
          }, {
            disabled: true,
            name: 'p_nom_count',
            fieldLabel: 'Кол-во ед. хр'
          }, {
            disabled: true,
            name: 'p_nom_deadline',
            fieldLabel: 'Срок хранения и № статей по перечню'
          }, {
            disabled: true,
            name: 'p_nom_comment',
            fieldLabel: 'Примечания'
          }, {
            disabled: true,
            name: 'p_nom_year',
            fieldLabel: 'Год'
          }]
        }, {
          xtype: 'fieldset',
          collapsed: true,
          collapsible: true,
          title: 'Адресат',
          defaults: {
            xtype: 'textfield',
            anchor: '100%'
          },
          items: [{
            disabled: true,
            name: 'p_adressee_title',
            fieldLabel: 'Наименование'
          }, {
            disabled: true,
            name: 'p_adressee_name',
            fieldLabel: 'ФИО'
          }, {
            disabled: true,
            name: 'p_adressee_department',
            fieldLabel: 'Отдел'
          }, {
            disabled: true,
            name: 'p_adressee_position',
            fieldLabel: 'Должность'
          }]
        }, {
          xtype: 'fieldset',
          collapsed: true,
          collapsible: true,
          title: 'Корреспондент',
          defaults: {
            xtype: 'textfield',
            anchor: '100%'
          },
          items: [{
            disabled: true,
            name: 'p_correspondent_title',
            fieldLabel: 'Наименование'
          }, {
            disabled: true,
            name: 'p_correspondent_name',
            fieldLabel: 'ФИО'
          }, {
            disabled: true,
            name: 'p_correspondent_department',
            fieldLabel: 'Отдел'
          }, {
            disabled: true,
            name: 'p_correspondent_position',
            fieldLabel: 'Должность'
          }]
        }, {
          xtype: 'fieldset',
          collapsed: true,
          collapsible: true,
          title: 'Автор',
          defaults: {
            xtype: 'textfield',
            anchor: '100%'
          },
          items: [{
            disabled: true,
            name: 'p_author_name',
            fieldLabel: 'ФИО'
          }, {
            disabled: true,
            name: 'p_author_department',
            fieldLabel: 'Отдел'
          }, {
            disabled: true,
            name: 'p_author_position',
            fieldLabel: 'Должность'
          }]
        }, {
          xtype: 'fieldset',
          collapsed: true,
          collapsible: true,
          title: 'На подпись',
          defaults: {
            xtype: 'textfield',
            anchor: '100%'
          },
          items: [{
            disabled: true,
            name: 'p_sign_name',
            fieldLabel: 'ФИО'
          }, {
            disabled: true,
            name: 'p_sign_department',
            fieldLabel: 'Отдел'
          }, {
            disabled: true,
            name: 'p_sign_position',
            fieldLabel: 'Должность'
          }]
        }]
    }]
  },

  initComponent: function () {
        Ext.apply(this, {
            items: [
                {
                    xtype: 'panel',
                    border: 0,
                    layout: 'anchor',
                    defaultType: 'textfield',
                    anchor: '100%',
                    bodyPadding: '10 10 10 5',
                    autoScroll: true,
                    defaults: {
                        anchor: '100%',
                        labelWidth: 110
                    },
                    items: this.getItems()
                }
            ]
        });
        this.callParent(arguments);
    }
});
