Ext.define('Cement.view.office_work.correspondention.lists.Incoming', {
  extend: 'Cement.view.basic.Grid',
  alias: 'widget.office_work_correspondention_lists_incoming',
  autoLoadStore: false,

  bbarText: 'Показаны документы {0} - {1} из {2}',
  bbarEmptyMsg: 'Нет документов',
  bbarUsersText: 'Документов на странице: ',

  gridStore: 'office_work.correspondention.Incoming',
  gridStateId: 'stateOfficeWorkCorrespondentionIncoming',

  printUrl: Cement.Config.url.office_work.correspondention.incoming.printGrid,
  helpUrl: Cement.Config.url.office_work.correspondention.incoming.help,
  deleteUrl: Cement.Config.url.office_work.correspondention.incoming.deleteUrl,
  archiveItemUrl: Cement.Config.url.office_work.correspondention.incoming.archiveItem,

  shownTitle: null,

  getActionColumns: function () {
    return {
        xtype: 'rowactions',
        hideable: false,
        resizeable: false,
        locked: true,
        width: 23,
        actions: [
        //{
        //    iconCls: 'icon-delete-item',
        //    qtip: 'Удалить',
        //    callback: this.deleteItem
        //},
        {
            iconCls: 'icon-archive-item',
            qtip: 'В архив',
            callback: this.archiveItem
        }
        ],
        keepSelection: true
    };
  },

getFilterItems: function () {
    return [{
        text: 'Вид',
        kind: 'selector',
        field_name: 'p_kind_display',
        checked: true
    }, {
        text: 'Номер',
        kind: 'selector',
        field_name: 'p_number',
        checked: true
    }, {
        text: 'Дата',
        kind: 'selector',
        field_name: 'p_date',
        checked: true
    }, {
        text: 'Автор',
        kind: 'selector',
        field_name: 'p_author_display',
        checked: true
    }, {
        text: 'Адресат',
        kind: 'selector',
        field_name: 'p_adressee_display',
        checked: true
    }, {
        text: 'На подпись',
        kind: 'selector',
        field_name: 'p_for_sign_display',
        checked: true
    }, {
        text: 'Тема',
        kind: 'selector',
        field_name: 'p_theme',
        checked: true
    }, {
        text: 'Задача',
        kind: 'selector',
        field_name: 'p_task_display',
        checked: true
    }];
},

getGridColumns: function () {
    var result = [
        { text: 'Вид', dataIndex: 'p_kind_display', width: 150, locked: true },
        { text: 'Номер', dataIndex: 'p_number', width: 150 },
        { text: 'Дата', dataIndex: 'p_date', width: 150 },
        { text: 'Корреспондент', dataIndex: 'p_correspondent_display', width: 150 },
        { text: 'Автор', dataIndex: 'p_author_display', width: 150 },
        { text: 'На подпись', dataIndex: 'p_for_sign_display', width: 150 },
        { text: 'Тема', dataIndex: 'p_theme', width: 350 },
        { text: 'Задача', dataIndex: 'p_task_display', width: 150 },
        { text: '', dataIndex: 'p_security', renderer: this.docStatusColRender, width: 43 }
    ];
    return this.mergeActions(result);
},

initComponent: function () {
    Ext.apply(this, {
        title: this.shownTitle
    });
    this.callParent(arguments);
}
});