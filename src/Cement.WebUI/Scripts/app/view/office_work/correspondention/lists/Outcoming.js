Ext.define('Cement.view.office_work.correspondention.lists.Outcoming', {
  extend: 'Cement.view.office_work.correspondention.lists.New',
  alias: 'widget.office_work_correspondention_lists_outcoming',

  gridStore: 'office_work.correspondention.Outcoming',
  gridStateId: 'stateOfficeWorkCorrespondentionOutcoming',

  printUrl: Cement.Config.url.office_work.correspondention.outcoming.printGrid,
  helpUrl: Cement.Config.url.office_work.correspondention.outcoming.help,
  deleteUrl: Cement.Config.url.office_work.correspondention.outcoming.deleteUrl,
  archiveItemUrl: Cement.Config.url.office_work.correspondention.outcoming.archiveItem,

  shownTitle: null,

  getActionColumns: function () {
    return {
        xtype: 'rowactions',
        hideable: false,
        resizeable: false,
        locked: true,
        width: 23,
        actions: [
        //{
        //    iconCls: 'icon-delete-item',
        //    qtip: 'Удалить',
        //    callback: this.deleteItem
        //},
        {
            iconCls: 'icon-archive-item',
            qtip: 'В архив',
            callback: this.archiveItem
        }
        ],
        keepSelection: true
    };
}
});