Ext.define('Cement.view.office_work.correspondention.bottom.outcoming.Routes', {
    extend: 'Cement.view.office_work.correspondention.bottom.new_c.Routes',
    alias: 'widget.office_work_correspondention_bottom_outcoming_routes',
    gridStore: 'office_work.correspondention.bottom.outcoming.Routes',
    gridStateId: 'stateOfficeWorkCorrespondentionBottomOutcomingRoutes'
});