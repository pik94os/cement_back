Ext.define('Cement.view.office_work.correspondention.bottom.outcoming.ConnectedDocumentsIncoming', {
	extend: 'Cement.view.goods.auto.lists.ConnectedDocuments',
	alias: 'widget.office_work_correspondention_bottom_outcoming_connected_documents_incoming',

	gridStore: 'office_work.correspondention.bottom.outcoming.ConnectedDocumentsIncoming',
	gridStateId: 'stateOfficeWorkCorrespondentionOutcomingConnectedDocumentsIncoming',

    printUrl: Cement.Config.url.office_work.correspondention.documents.printGrid,
    helpUrl: Cement.Config.url.office_work.correspondention.documents.help
});