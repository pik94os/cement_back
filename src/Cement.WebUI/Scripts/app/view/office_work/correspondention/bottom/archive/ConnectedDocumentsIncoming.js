Ext.define('Cement.view.office_work.correspondention.bottom.archive.ConnectedDocumentsIncoming', {
	extend: 'Cement.view.goods.auto.lists.ConnectedDocuments',
	alias: 'widget.office_work_correspondention_bottom_archive_connected_documents_incoming',

	gridStore: 'office_work.correspondention.bottom.archive.ConnectedDocumentsIncoming',
	gridStateId: 'stateOfficeWorkCorrespondentionArchiveConnectedDocumentsIncoming',

    printUrl: Cement.Config.url.office_work.correspondention.documents.printGrid,
    helpUrl: Cement.Config.url.office_work.correspondention.documents.help
});