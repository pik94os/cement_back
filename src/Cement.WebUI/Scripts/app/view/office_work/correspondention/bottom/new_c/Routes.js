﻿Ext.define('Cement.view.office_work.correspondention.bottom.new_c.Routes', {
    extend: 'Cement.view.basic.Grid',
    alias: 'widget.office_work_correspondention_bottom_new_routes',
    gridStore: 'office_work.correspondention.bottom.new_c.Routes',
    gridStateId: 'stateOfficeWorkCorrespondentionBottomNewRoutes',

    title: 'Маршруты',
    printUrl: Cement.Config.url.office_work.correspondention.routes.printGrid,
    helpUrl: Cement.Config.url.office_work.correspondention.routes.help,
    printItemUrl: Cement.Config.url.office_work.correspondention.routes.printItem,
    bbarText: 'Показаны маршруты {0} - {1} из {2}',
    bbarEmptyMsg: 'Нет маршрутов',
    bbarUsersText: 'Маршрутов на странице: ',
    autoLoadStore: false,
    tbar: null,

    getToolbarItems: function () { return null; },
    getGridFeatures: function () { return null; },
    showBottomBar: false,

    getGridColumns: function () {
        return [
            { text: 'Наименование', dataIndex: 'p_org_name', width: 150, locked: true, sortable: false },
            { text: 'ФИО', dataIndex: 'p_name', width: 150, locked: true, sortable: false },
            { text: 'Должность', dataIndex: 'p_position', width: 150, locked: true, sortable: false },
            { text: 'Отдел', dataIndex: 'p_department', width: 150, sortable: false },
            { text: 'Статус', dataIndex: 'p_status', width: 150, sortable: false },
            { text: 'Дата поступления', dataIndex: 'p_date_in', width: 150, sortable: false },
            { text: 'Дата отправки', dataIndex: 'p_date_out', width: 150, sortable: false },
            { text: 'Время нахождения', dataIndex: 'p_time_spent', width: 150, sortable: false }
        ];
    }
});