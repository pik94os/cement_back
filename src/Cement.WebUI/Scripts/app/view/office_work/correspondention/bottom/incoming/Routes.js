Ext.define('Cement.view.office_work.correspondention.bottom.incoming.Routes', {
    extend: 'Cement.view.office_work.correspondention.bottom.new_c.Routes',
    alias: 'widget.office_work_correspondention_bottom_incoming_routes',
    gridStore: 'office_work.correspondention.bottom.incoming.Routes',
    gridStateId: 'stateOfficeWorkCorrespondentionBottomIncomingRoutes'
});