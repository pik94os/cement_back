Ext.define('Cement.view.office_work.correspondention.bottom.new_c.ConnectedDocumentsIncoming', {
	extend: 'Cement.view.goods.auto.lists.ConnectedDocuments',
	alias: 'widget.office_work_correspondention_bottom_new_connected_documents_incoming',

	gridStore: 'office_work.correspondention.bottom.new_c.ConnectedDocumentsIncoming',
	gridStateId: 'stateOfficeWorkCorrespondentionNewConnectedDocumentsIncoming',

    printUrl: Cement.Config.url.office_work.correspondention.documents.printGrid,
    helpUrl: Cement.Config.url.office_work.correspondention.documents.help
});