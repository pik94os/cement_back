Ext.define('Cement.view.warehouse.inventory.complex.Outcoming', {
	extend: 'Cement.view.basic.Complex',
	alias: 'widget.warehouse_inventory_complex_outcoming',
    border: 0,
    enableDetail: false,
    autoLoadStore: false,
    priceId: null,
    bodyPadding: 0,
    padding: 0,
    header: false,
    rowsCount: 3,
    title: 'Исходящие',
    tabTitle: 'Исходящие',
    cls: 'no-side-borders',
    topXType: 'warehouse_inventory_lists_outcoming',
    topDetailType: 'Cement.view.warehouse.inventory.view.View',
    bottomTitle: 'Связанные документы',
    bottomTabs: [{
        title: 'Связанные документы',
        xtype: 'warehouse_inventory_bottom_connected_documents',
        detailType: 'Cement.view.warehouse.inventory.view.View',
        constantFilterParam: 'p_base_doc',
        childrenConstantFilterParam: 'p_base_doc',
        border: 0
    }],
    subBottomTitle: 'Справочник',
    subBottomTabs: [
        {
            title: 'Склады',
            shownTitle: 'Склады',
            xtype: 'warehouse_inventory_bottom_new_warehouses',
            detailType: 'Cement.view.corporate.warehouse.View'
        },
        {
            title: 'Товары',
            shownTitle: 'Товары',
            xtype: 'warehouse_inventory_bottom_new_products',
            detailType: 'Cement.view.products.products.View'
        },
        {
            title: 'Услуги',
            shownTitle: 'Услуги',
            xtype: 'warehouse_inventory_bottom_new_services',
            detailType: 'Cement.view.products.services.View'
        },
        {
            title: 'Маршруты',
            shownTitle: 'Маршруты',
            xtype: 'warehouse_inventory_bottom_new_routes',
            detailType: 'Cement.view.products.price.RouteView'
        }
    ]
});