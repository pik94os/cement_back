Ext.define('Cement.view.warehouse.inventory.form.MembersWindow', {
	extend: 'Cement.view.common.SignWindow',
	xtype: 'warehouse_inventory_form_members_window',
	title: 'Выбор членов комиссии',
	hideCommentPanel: true,
	leftFrameTitle: 'Сотрудники',
	topGridTitle: 'Выбрать сотрудника',
	bottomGridTitle: 'Выбранный сотрудник',
    storeFields: ['id', 'p_name'],
    displayField: 'p_name',

    structure_storeId: 'warehouse.inventory.auxiliary.Employee',

    bbar: [{
        text: 'Сохранить',
        action: 'confirm'
    }, {
        text: 'Отмена',
        action: 'cancel'
    }],

	getBottomColumnsConfig: function () {
		return [{
			xtype: 'rowactions',
			hideable: false,
			resizeable: false,
			width: 18,
			actions: [{ 
                iconCls: 'icon-remove-document', 
                qtip: 'Удалить', 
                callback: this.removeEmployee
            }]
		},
            { text: 'Ф.И.О.', dataIndex: 'p_name', flex: 1 }
        ];
	},

	getTopGridFilters: function () {
        return [{
            text: 'Ф.И.О.',
            kind: 'selector',
            field_name: 'p_name',
            checked: false
        }];
	},

	getTopColumnsConfig: function () {
		return [
			{
				xtype: 'rowactions',
				hideable: false,
				resizeable: false,
				width: 18,
				keepSelection: true,
				actions: [{ 
	                iconCls: 'icon-add-document', 
	                qtip: 'Добавить',
	                callback: this.addEmployee
	            }]
			},
			{ text: 'Ф.И.О.', dataIndex: 'p_name', flex: 1 }
		];
	},

    sign: function (send) {
        var store = this.dstStore,
            str = [],
            data = [];
        store.each(function (item) {
            str.push(item.get('p_name'));
            data.push(item.get('id'));
        });
        this.fireEvent('membersselected', data, str);
    },

    initComponent: function () {
        this.callParent(arguments);
        this.on('beforeclose', function () {
            this.hide();
            return false;
        });
        this.down('treepanel').on('itemclick', function (tree, record) {
            var items = [];
            this.srcStore.removeAll();
            if (record.get('leaf')) {
                items.push(record);
            }
            else {
                Ext.each(record.childNodes, function (child) {
                    if (child.get('leaf')) {
                        items.push(child);
                    }
                });
            }
            Ext.each(items, function (item) {
                this.addToSrcStore(item);
            }, this);
        }, this);
        this.addEvents('membersselected');
    }
});