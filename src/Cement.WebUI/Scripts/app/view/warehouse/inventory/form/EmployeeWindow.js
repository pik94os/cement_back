Ext.define('Cement.view.warehouse.inventory.form.EmployeeWindow', {
    extend: 'Cement.view.warehouse.inventory.form.MembersWindow',
	xtype: 'warehouse_inventory_form_employee_window',
	title: 'Выбор сотрудника',
    //structure_storeId: 'goods.auto.auxiliary.SupplierContacts', 
	singleSelection: true,

	sign: function (send) {
		if (this.dstStore.getCount() > 0) {
			this.fireEvent('selected', this.dstStore.getAt(0));
		}
	}
});