Ext.define('Cement.view.warehouse.inventory.form.Inventory', {
    extend: 'Cement.view.basic.DoubleGridForm',
    alias: 'widget.warehouse_inventory_form_inventory',
    fieldsDisabled: false,
    singleSelection: false,
	url: Cement.Config.url.warehouse.inventory.new_i.saveUrl,
	method: Cement.Config.url.warehouse.inventory.new_i.saveMethod,
	printUrl: Cement.Config.url.warehouse.inventory.new_i.printItem,
    
	loadItemUrl: Cement.Config.url.warehouse.inventory.new_i.loadItem,
	loadItemProperty: 'p_base_doc',
	getNewNumberUrl: Cement.Config.url.warehouse.inventory.new_i.newNumberUrl,
    
	storeFields: ['id', 'p_group_display', 'p_subgroup_display', 'p_trade_mark', 'p_manufacturer', 'p_name', 'p_unit', 'p_count', {name:'p_count_fact', defaultValue: 0}],
	kind_store_Id: 'warehouse.inventory.auxiliary.InventoryKinds',

	bottomGridStoreProxy: Cement.Config.url.warehouse.inventory.new_i.stockRoomProductsProxy,

	getTopItems: function () {
	    return [
	        {
	            xtype: 'hiddenfield',
	            name: 'p_products'
	        },
            {
                fieldLabel: 'Номер',
                readOnly: true,
                name: 'p_number'
            },
            {
                xtype: 'datefield',
                fieldLabel: 'Дата',
                name: 'p_date'
            },
	        this.getCombo('Вид', 'p_kind', this.kind_store_Id),
            this.getSelectorField('Склад', 'p_warehouse', 'show_warehouse_window', 100)
	    ];
	},
    
	getBottomGridStore: function () {
	    return Ext.create('Ext.data.Store', {
	        fields: this.storeFields,
	        autoLoad: false,
	        proxy: this.bottomGridStoreProxy
	    });
	},

	getBottomItems: function () {
	    return [
            this.getSelectorField('Председатель комиссии', 'p_chairman', 'show_chairman_window', 150),
            this.getSelectorField('Директор', 'p_director', 'show_director_window', 150),
            {
                xtype: 'fieldcontainer',
                layout: 'hbox',
                items: [{
                    xtype: 'hiddenfield',
                    name: 'p_members'
                },
                {
                    labelWidth: 150,
                    name: 'p_members_display',
                    fieldLabel: 'Члены комиссии',
                    flex: 1,
                    xtype: 'textfield',
                    labelAlign: 'right'
                },
                {
                    xtype: 'button',
                    text: 'Выбрать',
                    margin: '0 0 0 4',
                    action: 'show_members_window',
                    width: 60
                }]
            }
	    ];
	},

	getCommonColumns: function(isSrc) {
        return [
			{ text: 'Группа', dataIndex: 'p_group_display', width: 100 },
            { text: 'Подгруппа', dataIndex: 'p_subgroup_display', width: 100 },
            { text: 'Торговая марка', dataIndex: 'p_trade_mark', width: 100 },
            { text: 'Производитель', dataIndex: 'p_manufacturer', width: 100 },
            { text: 'Наименование', dataIndex: 'p_name', flex: 1 },
            { text: 'Ед.изм.', dataIndex: 'p_unit', width: 80 },
            { text: 'Кол-во план', dataIndex: 'p_count', width: 80 * (isSrc ? 2 : 1) },
            { text: 'Кол-во факт', hidden: isSrc, hideable: !isSrc, dataIndex: 'p_count_fact', width: 80, editor: { xtype: 'numberfield' } }
            //{ text: 'Кол-во план', dataIndex: 'p_count', width: 80 },
            //{ text: 'Кол-во факт', dataIndex: 'p_count_fact', width: 80, editor: { xtype: 'numberfield' } }
        ];
    },

	initComponent: function () {
		this.callParent(arguments);
		var me = this;
		this.down('button[action=show_warehouse_window]').on('click', function () {
		    me.createWarehouseWindow();
		    me.warehouseWindow.show();
		}, this);
		this.down('button[action=show_chairman_window]').on('click', function () {
		    me.createChairmanWindow();
		    me.chairmanWindow.show();
		}, this);
		this.down('button[action=show_director_window]').on('click', function () {
		    me.createDirectorWindow();
		    me.directorWindow.show();
		}, this);
		this.down('button[action=show_members_window]').on('click', function () {
		    me.createMembersWindow();
		    me.membersWindow.show();
		}, this);
	},

    createWarehouseWindow: function() {
        if (!this.warehouseWindow) {
            this.warehouseWindow = Ext.create('Cement.view.warehouse.inventory.form.WarehouseWindow');
            this.warehouseWindow.on('selected', function (rec) {
                this.dstStore.removeAll();
                this.down('hiddenfield[name=p_warehouse]').setValue(rec.get('id'));
                this.down('textfield[name=p_warehouse_display]').setValue(rec.get('p_name'));

                this.srcStore.load({
                    params: {                        
                        p_warehouse: rec.get('id')
                    }
                });

                this.warehouseWindow.hide();
            }, this);
        }
    },

    createChairmanWindow: function () {
        if (!this.chairmanWindow) {
            this.chairmanWindow = Ext.create('Cement.view.warehouse.inventory.form.EmployeeWindow');
            this.chairmanWindow.on('selected', function (rec) {
                this.down('hiddenfield[name=p_chairman]').setValue(rec.get('id'));
                this.down('textfield[name=p_chairman_display]').setValue(rec.get('p_name'));
                this.chairmanWindow.hide();
            }, this);
        }
    },
	
    createDirectorWindow: function () {
        if (!this.directorWindow) {
            this.directorWindow = Ext.create('Cement.view.warehouse.inventory.form.EmployeeWindow');
            this.directorWindow.on('selected', function (rec) {
                this.down('hiddenfield[name=p_director]').setValue(rec.get('id'));
                this.down('textfield[name=p_director_display]').setValue(rec.get('p_name'));
                this.directorWindow.hide();
            }, this);
        }
    },

    createMembersWindow: function() {
        if (!this.membersWindow) {
            this.membersWindow = Ext.create('Cement.view.warehouse.inventory.form.MembersWindow');
            this.membersWindow.on('membersselected', function (json, str) {
                this.down('textfield[name=p_members_display]').setValue(str);
                this.down('hiddenfield[name=p_members]').setValue(Ext.JSON.encode(json));
                this.membersWindow.hide();
            }, this);
        }
    },
    
    beforeSubmit: function (callback) {
        var me = this;

        if (me.dstStore.getCount() == 0) {
            Cement.Msg.warning('Выберите товары!');
            return;
        }

        var products = [];
        me.dstStore.each(function (item) {
            products.push({
                id: item.get('id'),
                count: item.get('p_count_fact')
            });
        });
        
        me.down('hiddenfield[name=p_products]').setValue(Ext.encode(products));

        callback.call();
    },

    afterRecordLoad: function (rec) {
        this.dstStore.removeAll();
        this.srcStore.removeAll();

        var existingProducts = rec.get('p_existing_stockroomproducts');
        var products = rec.get('p_stockroomproducts');
        existingProducts && this.dstStore.add(existingProducts);
        products && this.srcStore.add(products);
    }
});