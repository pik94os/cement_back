Ext.define('Cement.view.warehouse.inventory.bottom.new.Routes', {
    extend: 'Cement.view.office_work.correspondention.bottom.new_c.Routes',
    alias: 'widget.warehouse_inventory_bottom_new_routes',
    gridStore: 'warehouse.inventory.bottom.new.Routes',
    gridStateId: 'stateWarehouseInventoryBottomNewRoutes'
});