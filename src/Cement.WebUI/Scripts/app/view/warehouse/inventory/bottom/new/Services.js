Ext.define('Cement.view.warehouse.inventory.bottom.new.Services', {
    extend: 'Cement.view.products.price.bottom.NewServices',
    alias: 'widget.warehouse_inventory_bottom_new_services',
    gridStore: 'warehouse.inventory.bottom.new.Services',
    gridStateId: 'stateWarehouseInventoryBottomNewServices'
});