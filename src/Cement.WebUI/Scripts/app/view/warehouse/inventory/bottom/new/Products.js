Ext.define('Cement.view.warehouse.inventory.bottom.new.Products', {
    extend: 'Cement.view.products.price.bottom.NewProducts',
    alias: 'widget.warehouse_inventory_bottom_new_products',
    gridStore: 'warehouse.inventory.bottom.new.Products',
    gridStateId: 'stateWarehouseInventoryBottomNewProducts'
});