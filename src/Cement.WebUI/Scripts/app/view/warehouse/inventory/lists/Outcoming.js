Ext.define('Cement.view.warehouse.inventory.lists.Outcoming', {
    extend: 'Cement.view.warehouse.inventory.lists.New',
    alias: 'widget.warehouse_inventory_lists_outcoming',
    autoLoadStore: true,

    gridStore: 'warehouse.inventory.Outcoming',
    gridStateId: 'stateWarehouseInventoryOutcoming',

    printUrl: Cement.Config.url.warehouse.inventory.outcoming.printGrid,
    helpUrl: Cement.Config.url.warehouse.inventory.outcoming.help,
    deleteUrl: Cement.Config.url.warehouse.inventory.outcoming.deleteUrl,
    archiveItemUrl: Cement.Config.url.warehouse.inventory.outcoming.archiveItem,
    showCreateButton: false,
    shownTitle: null,

    getActionColumns: function () {
        return {
            xtype: 'rowactions',
            hideable: false,
            resizeable: false,
            locked: true,
            width: 46,
            actions: [
                {
                    iconCls: 'icon-archive-item',
                    qtip: 'В архив',
                    callback: this.archiveItem
                }
            ],
            keepSelection: true
        };
    }
});