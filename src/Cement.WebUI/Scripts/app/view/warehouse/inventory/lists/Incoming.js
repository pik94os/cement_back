Ext.define('Cement.view.warehouse.inventory.lists.Incoming', {
    extend: 'Cement.view.warehouse.inventory.lists.New',
    alias: 'widget.warehouse_inventory_lists_incoming',
    autoLoadStore: true,

    gridStore: 'warehouse.inventory.Incoming',
    gridStateId: 'stateWarehouseInventoryIncoming',

    printUrl: Cement.Config.url.warehouse.inventory.incoming.printGrid,
    helpUrl: Cement.Config.url.warehouse.inventory.incoming.help,
    deleteUrl: Cement.Config.url.warehouse.inventory.incoming.deleteUrl,
    archiveItemUrl: Cement.Config.url.warehouse.inventory.incoming.archiveItem,
    showCreateButton: false,
    shownTitle: null,

    getActionColumns: function () {
        return {
            xtype: 'rowactions',
            hideable: false,
            resizeable: false,
            locked: true,
            width: 46,
            actions: [
                {
                    iconCls: 'icon-archive-item',
                    qtip: 'В архив',
                    callback: this.archiveItem
                }
            ],
            keepSelection: true
        };
    }
});