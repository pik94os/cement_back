Ext.define('Cement.view.warehouse.inventory.lists.New', {
	extend: 'Cement.view.basic.Grid',
	alias: 'widget.warehouse_inventory_lists_new',
    autoLoadStore: true,

    bbarText: 'Показаны заявки {0} - {1} из {2}',
    bbarEmptyMsg: 'Нет заявок',
    bbarUsersText: 'Заявок на странице: ',

    gridStore: 'warehouse.inventory.New',
    gridStateId: 'stateWarehouseInventoryNew',

    printUrl: Cement.Config.url.warehouse.inventory.new_i.printGrid,
    helpUrl: Cement.Config.url.warehouse.inventory.new_i.help,
    deleteUrl: Cement.Config.url.warehouse.inventory.new_i.deleteUrl,

    shownTitle: null,

    getActionColumns: function () {
        return {
            xtype: 'rowactions',
            hideable: false,
            resizeable: false,
            locked: true,
            width: 46,
            actions: [
                {
                    iconCls: 'icon-edit-item',
                    qtip: 'Редактировать',
                    callback: this.editItem
                },
                {
                    iconCls: 'icon-delete-item',
                    qtip: 'Удалить',
                    callback: this.deleteItem
                }
            ],
            keepSelection: true
        };
    },

    getFilterItems: function () {
        return [{
            text: 'Выделить все',
            checked: true,
            checkHandler: this.toggleFilterSelection
        }, '-', {
            text: 'Номер',
            kind: 'selector',
            field_name: 'p_number',
            checked: true
        }];
    },

    getGridColumns: function () {
        var result = [
            { text: 'Номер', dataIndex: 'p_number', width: 150 },
            { text: 'Дата', dataIndex: 'p_date', width: 150 },
            { text: 'Вид', dataIndex: 'p_kind_display', width: 150 },
            { text: 'Статус', dataIndex: 'p_state', width: 150 },
            { text: 'Наименование', dataIndex: 'p_name', flex: 1 }
        ];

        result = this.mergeActions(result);
        return result;
    },

    initComponent: function () {
        Ext.apply(this, {
            title: this.shownTitle
        });
        this.callParent(arguments);
    }
});