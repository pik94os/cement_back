Ext.define('Cement.view.warehouse.inventory.lists.Archive', {
    extend: 'Cement.view.warehouse.inventory.lists.New',
    alias: 'widget.warehouse_inventory_lists_archive',
    autoLoadStore: true,

    gridStore: 'warehouse.inventory.Archive',
    gridStateId: 'stateWarehouseInventoryArchive',
    gridStateful: false,

    printUrl: Cement.Config.url.warehouse.inventory.archive.printGrid,
    helpUrl: Cement.Config.url.warehouse.inventory.archive.help,
    deleteUrl: Cement.Config.url.warehouse.inventory.archive.deleteUrl,
    unArchiveItemUrl: Cement.Config.url.warehouse.inventory.archive.unArchiveItem,
    showCreateButton: false,
    shownTitle: null,

    getActionColumns: function () {
        return {
            xtype: 'rowactions',
            hideable: false,
            resizeable: false,
            locked: true,
            width: 46,
            actions: [
                {
                    iconCls: 'icon-unarchive-item',
                    qtip: 'Из архива',
                    callback: this.unArchiveItem
                },
                {
                    iconCls: 'icon-delete-item',
                    qtip: 'Удалить',
                    callback: this.deleteItem
                }
            ],
            keepSelection: true
        };
    },

    getGridColumns: function () {
        var result = [
            { text: 'Тип', dataIndex: 'p_type', width: 150 },
            { text: 'Номер', dataIndex: 'p_number', width: 150 },
            { text: 'Дата', dataIndex: 'p_date', width: 150 },
            { text: 'Вид', dataIndex: 'p_kind_display', width: 150 },
            { text: 'Статус', dataIndex: 'p_state', width: 150 },
            { text: 'Наименование', dataIndex: 'p_name', flex: 1 }
        ];

        result = this.mergeActions(result);
        return result;
    }
});