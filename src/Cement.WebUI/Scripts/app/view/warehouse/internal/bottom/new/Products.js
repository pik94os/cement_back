Ext.define('Cement.view.warehouse.internal.bottom.new.Products', {
    extend: 'Cement.view.products.price.bottom.NewProducts',
    alias: 'widget.warehouse_internal_bottom_new_products',
    gridStore: 'warehouse.internal.bottom.new.Products',
    gridStateId: 'stateWarehouseInternalBottomNewProducts'
});