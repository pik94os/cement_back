Ext.define('Cement.view.warehouse.internal.bottom.new.Services', {
    extend: 'Cement.view.products.price.bottom.NewServices',
    alias: 'widget.warehouse_internal_bottom_new_services',
    gridStore: 'warehouse.internal.bottom.new.Services',
    gridStateId: 'stateWarehouseInternalBottomNewServices'
});