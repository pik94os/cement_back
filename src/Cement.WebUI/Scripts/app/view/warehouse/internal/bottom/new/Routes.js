Ext.define('Cement.view.warehouse.internal.bottom.new.Routes', {
    extend: 'Cement.view.office_work.correspondention.bottom.new_c.Routes',
    alias: 'widget.warehouse_internal_bottom_new_routes',
    gridStore: 'warehouse.internal.bottom.new.Routes',
    gridStateId: 'stateWarehouseInternalBottomNewRoutes'
});