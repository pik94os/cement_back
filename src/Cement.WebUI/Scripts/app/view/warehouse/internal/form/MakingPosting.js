Ext.define('Cement.view.warehouse.internal.form.MakingPosting', {
	extend: 'Cement.view.basic.Form',
	alias: 'widget.warehouse_internal_form_making_posting',
	fieldsDisabled: false,
	url: Cement.Config.url.warehouse.internal.new_i.saveUrl,
	method: Cement.Config.url.warehouse.internal.new_i.saveMethod,
	printUrl: Cement.Config.url.warehouse.internal.new_i.printItem,

	loadItemUrl: Cement.Config.url.warehouse.internal.new_i.loadItem,
	loadItemProperty: 'p_base_doc',
	getNewNumberUrl: Cement.Config.url.warehouse.internal.new_i.newNumberUrl,

	notCollapseFieldsetIndex: 0,

	getItems: function () {
	    return [
            {
                xtype: 'hiddenfield',
                name: 'id'
            },
            {
                xtype: 'hiddenfield',
                name: 'p_type',
                value: Cement.Config.warehouse.request_kinds.making_posting
            },
	        {
	            xtype: 'fieldset',
	            border: 0,
	            margin: 0,
	            padding: '0 110',
	            defaults: {
	                xtype: 'textfield',
	                anchor: '100%',
	                labelAlign: 'right'
	            },
	            items: [
	                {
	                    fieldLabel: 'Номер',
	                    readOnly: true,
	                    name: 'p_number'
	                },
                    {
                        xtype: 'datefield',
                        fieldLabel: 'Дата',
                        name: 'p_date'
                    },
                    {
                        xtype: 'selectfield',
                        fieldLabel: 'Склад',
                        name: 'p_warehouse',
                        store: Ext.create('Ext.data.Store', {
                            proxy: getProxy('/api/warehouse/personal'),
                            fields: ['Id', 'Name', 'Address'],
                            autoLoad: false,
                            pageSize: Cement.Config.defaultPageSize,
                            remoteFilter: true,
                            remoteSort: true
                        }),
                        columns: [
                            { text: 'Наименование', dataIndex: 'Name', filter: { xtype: 'textfield' }, flex: 1 },
                            { text: 'Адрес', dataIndex: 'Address', filter: { xtype: 'textfield' }, flex: 1 }
                        ],
                        editable: false,
                        allowBlank: false
                    }
	            ]
	        },
            {
                xtype: 'fieldset',
                collapsible: true,
                collapsed: false,
                title: 'Товар/Услуга',
                defaults: {
                    xtype: 'textfield',
                    anchor: '100%',
                    labelWidth: 200,
                    labelAlign: 'right'
                },
                items: [
                    this.getSelectorField('Прайс-лист', 'p_pricelist', 'show_product_window'),
                    {
                        xtype: 'hiddenfield',
                        name: 'p_pricelist_product'
                    },
                    {
                        xtype: 'hiddenfield',
                        name: 'p_product'
                    },
                    {
                        name: 'p_product_display',
                        fieldLabel: 'Наименование',
                        disabled: true
                    },
                    {
	                    name: 'p_measure_unit_display',
	                    fieldLabel: 'Единица измерения',
	                    disabled: true
                    },
                    {
                        xtype: 'hiddenfield',
                        name: 'p_measure_unit'
                    },
                    {
                        name: 'p_price',
                        fieldLabel: 'Цена',
                        disabled: true
                    },
                    {
                        name: 'p_tax',
                        fieldLabel: 'Налог',
                        disabled: true
                    },
                    {
                        name: 'p_sum',
                        fieldLabel: 'Сумма',
                        disabled: true
                    },
                    {
                        xtype: 'numberfield',
                        name: 'p_count',
                        fieldLabel: 'Количество',
                        hideTrigger: true,
                        allowDecimal: false
                    }
                ]
            },
            {
                xtype: 'fieldset',
                collapsible: true,
                collapsed: false,
                title: 'Дополнительные данные',
                defaults: {
                    xtype: 'textfield',
                    anchor: '100%',
                    labelWidth: 200,
                    labelAlign: 'right'
                },
                items: [
	                {
	                    fieldLabel: 'Примечание',
	                    name: 'p_description',
	                    xtype: 'textarea',
	                    height: 150
	                }
                ]
            }


	    ];
	},

	initComponent: function () {
	    this.callParent(arguments);
	    var me = this;
	    this.down('button[action=show_product_window]').on('click', function () {
	        me.createProductWindow();
	        me.productWindow.show();
	    }, this);
	    //this.down('numberfield[name=p_count]').on('change', function (cmp, newValue) {
	    //    var price = me.down('textfield[name=p_price]').getValue();
	    //    if (price) {
	    //        me.down('textfield[name=p_sum]').setValue(price * newValue);
	    //    }
	    //}, this);
	},

	createProductWindow: function () {
	    if (!this.productWindow) {
	        this.productWindow = Ext.create('Cement.view.warehouse.internal.form.ProductWindow');
	        this.productWindow.on('selected', function (rec, pricelist) {
	            this.down('textfield[name=p_pricelist_display]').setValue(pricelist.get('p_name'));
	            this.down('hiddenfield[name=p_pricelist_product]').setValue(rec.get('p_pricelist_product'));
	            this.down('hiddenfield[name=p_product]').setValue(rec.get('id'));
	            this.down('textfield[name=p_product_display]').setValue(rec.get('p_name'));
	            this.down('textfield[name=p_price]').setValue(rec.get('p_price'));
	            this.down('textfield[name=p_tax]').setValue(rec.get('p_tax'));
	            this.down('textfield[name=p_measure_unit_display]').setValue(rec.get('p_product_unit_display'));
	            this.down('hiddenfield[name=p_measure_unit]').setValue(rec.get('p_product_unit'));
	            this.productWindow.hide();
	        }, this);
	    }
	},


	afterRecordLoad: function (rec) {
		
	}
});