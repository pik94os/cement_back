Ext.define('Cement.view.warehouse.internal.form.Posting', {
	extend: 'Cement.view.basic.Form',
	alias: 'widget.warehouse_internal_form_posting',
	fieldsDisabled: false,
	url: Cement.Config.url.warehouse.internal.new_i.saveUrl,
	method: Cement.Config.url.warehouse.internal.new_i.saveMethod,
	printUrl: Cement.Config.url.warehouse.internal.new_i.printItem,

	loadItemUrl: Cement.Config.url.warehouse.internal.new_i.loadItem,
	loadItemProperty: 'p_base_doc',
	getNewNumberUrl: Cement.Config.url.warehouse.internal.new_i.newNumberUrl,

	notCollapseFieldsetIndex: 0,
	unitsStoreId: 'products.auxiliary.AllUnits',

	getItems: function () {
	    return [
            {
                xtype: 'hiddenfield',
                name: 'id'
            },
            {
                xtype: 'hiddenfield',
                name: 'p_type',
                value: Cement.Config.warehouse.request_kinds.posting
            },
	        {
	            xtype: 'fieldset',
	            border: 0,
	            margin: 0,
	            padding: '0 110',
	            defaults: {
	                xtype: 'textfield',
	                anchor: '100%',
	                labelAlign: 'right'
	            },
	            items: [
	                {
	                    fieldLabel: 'Номер',
	                    readOnly: true,
	                    name: 'p_number'
	                },
                    {
                        xtype: 'datefield',
                        fieldLabel: 'Дата',
                        name: 'p_date'
                    },
                    {
                        xtype: 'selectfield',
                        fieldLabel: 'Склад',
                        name: 'p_warehouse',
                        store: Ext.create('Ext.data.Store', {
                            proxy: getProxy('/api/warehouse/personal'),
                            fields: ['Id', 'Name', 'Address'],
                            autoLoad: false,
                            pageSize: Cement.Config.defaultPageSize,
                            remoteFilter: true,
                            remoteSort: true
                        }),
                        columns: [
                            { text: 'Наименование', dataIndex: 'Name', filter: { xtype: 'textfield' }, flex: 1 },
                            { text: 'Адрес', dataIndex: 'Address', filter: { xtype: 'textfield' }, flex: 1 }
                        ],
                        editable: false,
                        allowBlank: false
                    }
	            ]
	        },
            {
                xtype: 'fieldset',
                collapsible: true,
                collapsed: false,
                title: 'Товар/Услуга',
                defaults: {
                    xtype: 'textfield',
                    anchor: '100%',
                    labelWidth: 200,
                    labelAlign: 'right'
                },
                items: [
                    this.getSelectorField('Наименование', 'p_product', 'show_product_window'),
	                //{
	                //    name: 'p_measure_unit_display',
	                //    fieldLabel: 'Единица измерения',
	                //    disabled: true
	                //},
                    this.getCombo('Единица измерения', 'p_measure_unit', this.unitsStoreId),
                    {
                        xtype: 'numberfield',
                        name: 'p_count',
                        fieldLabel: 'Количество',
                        hideTrigger: true,
                        allowDecimal: false
                    },
                    this.getSelectorField('Основание', 'p_cause', 'show_cause_window')
                ]
            },
            {
                xtype: 'fieldset',
                collapsible: true,
                collapsed: false,
                title: 'Дополнительные данные',
                defaults: {
                    xtype: 'textfield',
                    anchor: '100%',
                    labelWidth: 200,
                    labelAlign: 'right'
                },
                items: [
	                {
	                    fieldLabel: 'Примечание',
	                    name: 'p_description',
	                    xtype: 'textarea',
	                    height: 150
	                }
                ]
            }


	    ];
	},

	initComponent: function () {
	    this.callParent(arguments);
	    var me = this;
	    this.down('button[action=show_cause_window]').on('click', function () {
	        me.createCauseWindow();
	        me.causeWindow.show();
	    }, this);
	    this.down('button[action=show_product_window]').on('click', function () {
	        me.createProductWindow();
	        me.productWindow.show();
	    }, this);
	},

	createCauseWindow: function () {
	    if (!this.causeWindow) {
	        this.causeWindow = Ext.create('Cement.view.warehouse.internal.form.CausesWindow');
	        this.causeWindow.on('causesselected', function (json, str) {
	            this.down('textfield[name=p_cause_display]').setValue(str);
	            this.down('hiddenfield[name=p_cause]').setValue(Ext.JSON.encode(json));
	            this.causeWindow.hide();
	        }, this);
	    }
	},

	createProductWindow: function () {
	    if (!this.productWindow) {
	        this.productWindow = Ext.create('Cement.view.warehouse.internal.form.ProductWindow');
	        this.productWindow.on('selected', function (rec) {
	            this.down('hiddenfield[name=p_product]').setValue(rec.get('id'));
	            this.down('textfield[name=p_product_display]').setValue(rec.get('p_name'));
	            //this.down('textfield[name=p_measure_unit_display]').setValue(rec.get('p_unit'));
	            this.productWindow.hide();
	        }, this);
	    }
	},

	afterRecordLoad: function (rec) {
		
	}
});