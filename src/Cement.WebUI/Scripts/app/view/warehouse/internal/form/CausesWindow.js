Ext.define('Cement.view.warehouse.internal.form.CausesWindow', {
	extend: 'Cement.view.common.SignWindow',
	xtype: 'warehouse_internal_form_causes_window',
	title: 'Выбор основания',
	leftFrameTitle: 'Основание',
	topGridTitle: 'Выбрать основание',
	bottomGridTitle: 'Выбранное основание',
	structure_storeId: 'warehouse.internal.auxiliary.Causes',
	hideCommentPanel: true,
	singleSelection: false,
	displayField: 'p_name',
	storeFields: ['id', 'p_name'],

	bbar: [{
		text: 'Сохранить',
		action: 'confirm'
	}, {
		text: 'Отмена',
		action: 'cancel'
	}],

	sign: function (send) {
	    var store = this.dstStore,
            str = [],
            data = [];
	    store.each(function (item) {
	        str.push(item.get('p_name'));
	        data.push(item.get('id'));
	    });
	    this.fireEvent('causesselected', data, str);
	},

	getTopColumnsConfig: function () {
		return [
			{
				xtype: 'rowactions',
				hideable: false,
				resizeable: false,
				width: 18,
				keepSelection: true,
				actions: [{ 
	                iconCls: 'icon-add-people', 
	                qtip: 'Добавить',
	                callback: this.addEmployee
	            }]
			},
			{ text: 'Наименование', dataIndex: 'p_name', flex: 1 }
		];
	},

	getBottomColumnsConfig: function () {
		return [
			{
				xtype: 'rowactions',
				hideable: false,
				resizeable: false,
				width: 18,
				actions: [{ 
	                iconCls: 'icon-remove-people', 
	                qtip: 'Удалить', 
	                callback: this.removeEmployee
	            }]
			},
			{ text: 'Наименование', dataIndex: 'p_name', flex: 1 }
		];
	},
	
	initComponent: function () {
	    this.callParent(arguments);
	    this.on('beforeclose', function () {
	        this.hide();
	        return false;
	    });
	    this.down('treepanel').on('itemclick', function (tree, record) {
	        var items = [];
	        this.srcStore.removeAll();
	        if (record.get('leaf')) {
	            items.push(record);
	        }
	        else {
	            Ext.each(record.childNodes, function (child) {
	                if (child.get('leaf')) {
	                    items.push(child);
	                }
	            });
	        }
	        Ext.each(items, function (item) {
	            this.addToSrcStore(item);
	        }, this);
	    }, this);
	    this.addEvents('causesselected');
	}
});