Ext.define('Cement.view.warehouse.internal.form.EmployeeWindow', {
    extend: 'Cement.view.common.SignWindow',
    xtype: 'warehouse_internal_form_employee_window',
    title: 'Выбор контактного лица',
    leftFrameTitle: 'Контактное лицо',
    topGridTitle: 'Выбрать контактное лицо',
    bottomGridTitle: 'Выбранное контактное лицо',
    structure_storeId: 'warehouse.internal.auxiliary.WarehouseContacts',
    hideCommentPanel: true,
    singleSelection: true,
    displayField: 'p_name',
    storeFields: ['id', 'p_name', 'p_position', 'p_department'],

    bbar: [{
        text: 'Сохранить',
        action: 'confirm'
    }, {
        text: 'Отмена',
        action: 'cancel'
    }],

    sign: function (send) {
        if (this.dstStore.getCount() > 0) {
            this.fireEvent('selected', this.dstStore.getAt(0));
        }
    },

    getTopColumnsConfig: function () {
        return [
            {
                xtype: 'rowactions',
                hideable: false,
                resizeable: false,
                width: 18,
                keepSelection: true,
                actions: [{ 
                    iconCls: 'icon-add-people', 
                    qtip: 'Добавить',
                    callback: this.addEmployee
                }]
            },
            { text: 'ФИО', dataIndex: 'p_name', flex: 1 },
            { text: 'Должность', dataIndex: 'p_position', flex: 1 },
            { text: 'Отдел', dataIndex: 'p_department', flex: 1 }
        ];
    },

    getBottomColumnsConfig: function () {
        return [
            {
                xtype: 'rowactions',
                hideable: false,
                resizeable: false,
                width: 18,
                actions: [{ 
                    iconCls: 'icon-remove-people', 
                    qtip: 'Удалить', 
                    callback: this.removeEmployee
                }]
            },
            { text: 'ФИО', dataIndex: 'p_name', flex: 1 },
            { text: 'Должность', dataIndex: 'p_position', flex: 1 },
            { text: 'Отдел', dataIndex: 'p_department', flex: 1 }
        ];
    },
    
    setWarehouse: function (warehouse_id) {
        var store = Ext.getStore(this.structure_storeId);
        store.getProxy().setExtraParam('p_warehouse_id', warehouse_id);
    }
});