Ext.define('Cement.view.warehouse.internal.form.Movement', {
	extend: 'Cement.view.basic.Form',
	alias: 'widget.warehouse_internal_form_movement',
	fieldsDisabled: false,
	url: Cement.Config.url.warehouse.internal.new_i.saveUrl,
	method: Cement.Config.url.warehouse.internal.new_i.saveMethod,
	printUrl: Cement.Config.url.warehouse.internal.new_i.printItem,

	loadItemUrl: Cement.Config.url.warehouse.internal.new_i.loadItem,
	loadItemProperty: 'p_base_doc',
	getNewNumberUrl: Cement.Config.url.warehouse.internal.new_i.newNumberUrl,

	notCollapseFieldsetIndex: 0,

	getItems: function () {
	    var me = this;
        
	    return [
            {
                xtype: 'hiddenfield',
                name: 'id'
            },
            {
                xtype: 'hiddenfield',
                name: 'p_type',
                value: Cement.Config.warehouse.request_kinds.movement
            },
	        {
	            xtype: 'fieldset',
	            border: 0,
	            margin: 0,
	            padding: '0 110',
	            defaults: {
	                xtype: 'textfield',
	                anchor: '100%',
	                labelAlign: 'right'
	            },
	            items: [
	                {
	                    fieldLabel: 'Номер',
	                    name: 'p_number',
	                    readOnly: true,
	                    allowBlank: false
	                },
                    {
                        xtype: 'datefield',
                        fieldLabel: 'Дата',
                        name: 'p_date',
                        allowBlank: false
                    },
                    {
                        xtype: 'selectfield',
                        fieldLabel: 'Склад (текущий)',
                        name: 'p_warehouse',
                        store: Ext.create('Ext.data.Store', {
                            proxy: getProxy('/api/warehouse/personal'),
                            fields: ['Id', 'Name', 'Address'],
                            autoLoad: false,
                            pageSize: Cement.Config.defaultPageSize,
                            remoteFilter: true,
                            remoteSort: true
                        }),
                        columns: [
                            { text: 'Наименование', dataIndex: 'Name', filter: { xtype: 'textfield' }, flex: 1 },
                            { text: 'Адрес', dataIndex: 'Address', filter: { xtype: 'textfield' }, flex: 1 }
                        ],
                        editable: false,
                        allowBlank: false,
                        listeners: {
                            change: function () {
                                me.down('selectfield[name=p_warehouse_to]').setValue(null);
                            },
                            scope: me
                        },
                    }
	            ]
	        },
            {
	            xtype: 'fieldset',
	            collapsible: true,
	            collapsed: false,
	            title: 'Склад',
	            defaults: {
	                xtype: 'textfield',
	                anchor: '100%',
	                labelWidth: 200,
	                labelAlign: 'right'
	            },
	            items: [
                    {
                        xtype: 'selectfield',
                        fieldLabel: 'Наименование',
                        name: 'p_warehouse_to',
                        store: Ext.create('Ext.data.Store', {
                            proxy: getProxy('/api/warehouse/organization_owned'),
	                        fields: [ 'Id', 'Name', 'Address'],
	                        autoLoad: false,
	                        pageSize: Cement.Config.defaultPageSize,
	                        remoteFilter: true,
	                        remoteSort: true
                        }),
                        columns: [
                            { text: 'Наименование', dataIndex: 'Name', filter: { xtype: 'textfield' }, flex: 1 },
                            { text: 'Адрес', dataIndex: 'Address', filter: { xtype: 'textfield' }, flex: 1 }
                        ],
                        editable: false,
                        listeners: {
                            change: function(self, newValue) {
                                var tf = me.down('textfield[name=p_warehouse_address]');
                                tf.setValue(newValue && newValue.Address);

                                me.down('selectfield[name=p_stockroom_product]').setValue(null);
                                me.down('hiddenfield[name=p_warehouse_contact]').setValue(null);
                                me.down('textfield[name=p_warehouse_contact_display]').setValue(null);

                                // тут создаем окошко контактного лица и присваиваем ему Id склада
                                if (newValue) {
                                    me.createEmployeeWindow();
                                    me.employeeWindow.setWarehouse(newValue.Id);
                                }
                            },
                            beforeload: function (self, options) {
                                options.params.except = me.down('selectfield[name=p_warehouse]').getValue();
                            },
                            scope: me
                        },
                        allowBlank: false
                    },
	                //this.getSelectorField('Наименование', 'p_warehouse', 'show_warehouse_window'),
                    {
	                    name: 'p_warehouse_address',
	                    fieldLabel: 'Адрес',
	                    disabled: true
                    },
	                this.getSelectorField('Ответственное лицо', 'p_warehouse_contact', 'show_warehouse_contact')
	            ]
            },
            {
                xtype: 'fieldset',
                collapsible: true,
                collapsed: false,
                title: 'Товар/Услуга',
                defaults: {
                    xtype: 'textfield',
                    anchor: '100%',
                    labelWidth: 200,
                    labelAlign: 'right'
                },
                items: [
                    {
                        xtype: 'selectfield',
                        fieldLabel: 'Наименование',
                        name: 'p_stockroom_product',
                        store: 'warehouse.internal.auxiliary.StockroomProducts',
                        columns: [
                            { text: 'Наименование', dataIndex: 'Name', filter: { xtype: 'textfield' }, flex: 1 },
                            { text: 'Единица измерения', dataIndex: 'UnitName', filter: { xtype: 'textfield' }, flex: 1 }
                        ],
                        editable: false,
                        listeners: {
                            change: function (self, newValue) {
                                var measureUnitDisplay = self.up().down('textfield[name=p_measure_unit_display]');
                                measureUnitDisplay.setValue(newValue && newValue.UnitName);
                            },
                            beforeload: function (self, options) {
                                options.params.warehouseId = me.down('selectfield[name=p_warehouse_to]').getValue();
                            }
                        },
                        allowBlank: false
                    },
                    //this.getSelectorField('Наименование', 'p_stockroom_product', 'show_product_window'),
	                {
	                    name: 'p_measure_unit_display',
	                    fieldLabel: 'Единица измерения',
	                    disabled: true
	                },
                    {
                        xtype: 'numberfield',
                        name: 'p_count',
                        fieldLabel: 'Количество',
                        hideTrigger: true,
                        allowDecimal: false
                    }
                ]
            },
            {
                xtype: 'fieldset',
                collapsible: true,
                collapsed: false,
                title: 'Дополнительные данные',
                defaults: {
                    xtype: 'textfield',
                    anchor: '100%',
                    labelWidth: 200,
                    labelAlign: 'right'
                },
                items: [
	                {
                        fieldLabel: 'Примечание',
                        name: 'p_description',
                        xtype: 'textarea',
                        height: 150
	                }
                ]
            }
	            
	        
	    ];
	},

	initComponent: function () {
		this.callParent(arguments);
		var me = this;
		this.down('button[action=show_warehouse_contact]').on('click', function () {
		    me.createEmployeeWindow();
		    me.employeeWindow.show();
		}, this);
		//this.down('button[action=show_warehouse_window]').on('click', function () {
		//    me.createWarehouseWindow();
		//    me.warehouseWindow.show();
		//}, this);
		//this.down('button[action=show_product_window]').on('click', function () {
		//    me.createProductWindow();
		//    me.productWindow.show();
		//}, this);
	},
	
	createEmployeeWindow: function () {
	    if (!this.employeeWindow) {
	        this.employeeWindow = Ext.create('Cement.view.warehouse.internal.form.EmployeeWindow');
	        this.employeeWindow.on('selected', function (rec) {
	            this.down('hiddenfield[name=p_warehouse_contact]').setValue(rec.get('id'));
	            this.down('textfield[name=p_warehouse_contact_display]').setValue(rec.get('p_name'));
	            this.employeeWindow.hide();
	        }, this);
	    }
	},

    createWarehouseWindow: function() {
        if (!this.warehouseWindow) {
            this.warehouseWindow = Ext.create('Cement.view.warehouse.internal.form.WarehouseWindow');
            this.warehouseWindow.on('selected', function (rec) {
                this.down('hiddenfield[name=p_warehouse]').setValue(rec.get('id'));
                this.down('textfield[name=p_warehouse_display]').setValue(rec.get('p_name'));
                this.down('textfield[name=p_warehouse_address]').setValue(rec.get('p_address'));
                this.warehouseWindow.hide();
            }, this);
        }
    },

    createProductWindow: function () {
        if (!this.productWindow) {
            this.productWindow = Ext.create('Cement.view.warehouse.internal.form.ProductWindow');
            this.productWindow.on('selected', function (rec) {
                this.down('hiddenfield[name=p_product]').setValue(rec.get('id'));
                this.down('textfield[name=p_product_display]').setValue(rec.get('p_name'));
                this.down('textfield[name=p_measure_unit_display]').setValue(rec.get('p_unit'));
                this.productWindow.hide();
            }, this);
        }
    },

	afterRecordLoad: function (rec) {
		
	}
});