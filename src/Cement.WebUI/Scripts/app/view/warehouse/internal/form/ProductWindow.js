Ext.define('Cement.view.warehouse.internal.form.ProductWindow', {
    extend: 'Cement.view.common.SignWindow',
    xtype: 'warehouse_internal_form_product_window',
    title: 'Выбор товара',
    leftFrameTitle: 'Прайс-лист',
    topGridTitle: 'Выбрать товар',
    bottomGridTitle: 'Выбранный товар',
    structure_storeId: 'warehouse.internal.auxiliary.Products',
    hideCommentPanel: true,
    singleSelection: true,
    displayField: 'p_name',
    storeFields: ['id', 'p_name', 'p_product_unit', 'p_price', 'p_tax', 'p_pricelist_product', 'p_product_unit_display', 'p_pricelist_product'],
    loadRightFromTree: false,

    bbar: [{
        text: 'Сохранить',
        action: 'confirm'
    }, {
        text: 'Отмена',
        action: 'cancel'
    }],

    getTopColumnsConfig: function () {
        return [
			{
			    xtype: 'rowactions',
			    hideable: false,
			    resizeable: false,
			    width: 18,
			    keepSelection: true,
			    actions: [{
			        iconCls: 'icon-add-people',
			        qtip: 'Добавить',
			        callback: this.addEmployee
			    }]
			},
			{ text: 'Наименование', dataIndex: 'p_name', flex: 1 },
            //{ text: 'Группа', dataIndex: 'p_group'},
            //{ text: 'Подгруппа', dataIndex: 'p_subgroup' },
            //{ text: 'Торговая марка', dataIndex: 'p_trade_mark' },
            //{ text: 'Производитель', dataIndex: 'p_manufacturer' },
            { text: 'Единица измерения', dataIndex: 'p_product_unit_display' },
            { text: 'Налог', dataIndex: 'p_tax' },
            { text: 'Цена', dataIndex: 'p_price' }
        ];
    },

    getBottomColumnsConfig: function () {
        return [
			{
			    xtype: 'rowactions',
			    hideable: false,
			    resizeable: false,
			    width: 18,
			    actions: [{
			        iconCls: 'icon-remove-people',
			        qtip: 'Удалить',
			        callback: this.removeEmployee
			    }]
			},
			{ text: 'Наименование', dataIndex: 'p_name', flex: 1 },
            //{ text: 'Группа', dataIndex: 'p_group' },
            //{ text: 'Подгруппа', dataIndex: 'p_subgroup' },
            //{ text: 'Торговая марка', dataIndex: 'p_trade_mark'},
            //{ text: 'Производитель', dataIndex: 'p_manufacturer' },
            { text: 'Единица измерения', dataIndex: 'p_product_unit_display' },
            { text: 'Налог', dataIndex: 'p_tax' },
            { text: 'Цена', dataIndex: 'p_price'}
        ];
    },
    
    sign: function (send) {
        var store = this.down('*[role=sign-dst-grid]').getStore();
        if (store.getCount() > 0) {
            var product = store.getAt(0),
				pricelist = this.down('treepanel').getSelectionModel().getSelection()[0];
            this.fireEvent('selected', product, pricelist);
        }
    },
    
    getSrcStore: function () {
        return Ext.create('Ext.data.Store', {
            fields: this.storeFields,
            autoLoad: false,
            remoteFilter: true,
            proxy: getProxy(Cement.Config.url.warehouse.internal.products_dict, '')
        });
    },
    
    initComponent: function () {
        this.callParent(arguments);
        this.on('beforeclose', function () {
            this.hide();
            return false;
        });

        var treePanel = this.down('treepanel');

        treePanel.on('selectionchange', function (treepanel, selected) {
            if (selected.length && typeof selected[0].get('id') == "number") {
                var priceListId = selected[0].get('id');
                this.srcStore.clearFilter(true);
                this.srcStore.filter([{
                    property: 'p_pricelist_id',
                    value: priceListId
                }]);
            }

        }, this);
        this.addEvents('selected');
    }
});