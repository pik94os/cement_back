Ext.define('Cement.view.warehouse.internal.view.View', {
    extend: 'Cement.view.basic.Form',
    alias: 'widget.warehouse_internal_view_view',
    fieldsDisabled: true,
    buttons: null,
    border: 0,
    tbar: Cement.Config.modelViewButtons,
    layout: 'autocontainer',
    bbar: null,
    noUserFieldsetsControl: false,
    bodyCls: 'model-form',

    getItems: function () {
        return [
            {
                xtype: 'panel',
                border: 0,
                layout: 'anchor',
                defaultType: 'textfield',
                anchor: '100%',
                bodyPadding: '10 10 10 5',
                autoScroll: true,
                defaults: {
                    anchor: '100%',
                    labelWidth: 110
                },
                items: [
                    {
                        xtype: 'fieldset',
                        layout: 'anchor',
                        defaults: {
                            xtype: 'textfield',
                            anchor: '100%'
                        },
                        border: 0,
                        items: [
                            {
                                fieldLabel: 'Наименование',
                                disabled: true,
                                name: 'p_name'
                            },
                            {
                                fieldLabel: 'Дата',
                                disabled: true,
                                name: 'p_date'
                            },
                            {
                                fieldLabel: 'Вид',
                                disabled: true,
                                name: 'p_kind_display'
                            }
                        ]
                    },
                    {
                        xtype: 'fieldset',
                        collapsed: true,
                        collapsible: true,
                        title: 'Склад',
                        defaults: {
                            xtype: 'textfield',
                            anchor: '100%'
                        },
                        items: [
                            {
                                fieldLabel: 'Наименование',
                                disabled: true,
                                name: 'p_warehouse_display'
                            },
                            {
                                fieldLabel: 'Адрес',
                                disabled: true,
                                name: 'p_warehouse_address'
                            }
                        ]
                    },
                    {
                        xtype: 'fieldset',
                        collapsed: true,
                        collapsible: true,
                        title: 'Товар/Услуга',
                        defaults: {
                            xtype: 'textfield',
                            anchor: '100%'
                        },
                        items: [
                            {
                                fieldLabel: 'Наименование',
                                disabled: true,
                                name: 'p_product_display'
                            },
                            {
                                fieldLabel: 'Единица измерения',
                                disabled: true,
                                name: 'p_measure_unit_display'
                            },
                            {
                                fieldLabel: 'Кол-во план',
                                disabled: true,
                                name: 'p_count'
                            },
                            {
                                fieldLabel: 'Кол-во факт',
                                disabled: true,
                                name: 'p_count_fact'
                            },
                            {
                                fieldLabel: 'Основание',
                                disabled: true,
                                name: 'p_cause_display'
                            }
                        ]
                    },
                    {
                        xtype: 'fieldset',
                        collapsed: true,
                        collapsible: true,
                        title: 'Дополнительные данные',
                        defaults: {
                            xtype: 'textfield',
                            anchor: '100%'
                        },
                        items: [
                            {
                                xtype: 'textarea',
                                fieldLabel: 'Причина',
                                disabled: true,
                                name: 'p_reason'
                            },
                            {
                                xtype: 'textarea',
                                fieldLabel: 'Примечание',
                                disabled: true,
                                name: 'p_description'
                            }
                        ]
                    }
                ]
            }
        ];
    },

    initComponent: function () {
        Ext.apply(this, {
            items: [
                {
                    xtype: 'panel',
                    border: 0,
                    layout: 'anchor',
                    defaultType: 'textfield',
                    anchor: '100%',
                    bodyPadding: '10 10 10 5',
                    autoScroll: true,
                    defaults: {
                        anchor: '100%',
                        labelWidth: 110
                    },
                    items: this.getItems()
                }
            ]
        });
        this.callParent(arguments);
    }
});
