Ext.define('Cement.view.warehouse.internal.lists.Incoming', {
    extend: 'Cement.view.warehouse.internal.lists.New',
    alias: 'widget.warehouse_internal_lists_incoming',
    
    gridStore: 'warehouse.internal.Incoming',
    gridStateId: 'stateWarehouseInternalIncoming',

    printUrl: Cement.Config.url.warehouse.internal.incoming.printGrid,
    helpUrl: Cement.Config.url.warehouse.internal.incoming.help,
    deleteUrl: Cement.Config.url.warehouse.internal.incoming.deleteUrl,
    archiveItemUrl: Cement.Config.url.warehouse.internal.incoming.archiveItem,
    showCreateButton: false,
    shownTitle: null,

    getActionColumns: function () {
        return {
            xtype: 'rowactions',
            hideable: false,
            resizeable: false,
            locked: true,
            width: 46,
            actions: [
                {
                    iconCls: 'icon-archive-item',
                    qtip: 'В архив',
                    callback: this.archiveItem
                }
            ],
            keepSelection: true
        };
    }
});