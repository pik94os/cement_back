Ext.define('Cement.view.warehouse.internal.lists.Outcoming', {
    extend: 'Cement.view.warehouse.internal.lists.New',
    alias: 'widget.warehouse_internal_lists_outcoming',

    gridStore: 'warehouse.internal.Outcoming',
    gridStateId: 'stateWarehouseInternalOutcoming',

    printUrl: Cement.Config.url.warehouse.internal.outcoming.printGrid,
    helpUrl: Cement.Config.url.warehouse.internal.outcoming.help,
    deleteUrl: Cement.Config.url.warehouse.internal.outcoming.deleteUrl,
    archiveItemUrl: Cement.Config.url.warehouse.internal.outcoming.archiveItem,
    showCreateButton: false,
    shownTitle: null,

    getActionColumns: function () {
        return {
            xtype: 'rowactions',
            hideable: false,
            resizeable: false,
            locked: true,
            width: 46,
            actions: [
                {
                    iconCls: 'icon-archive-item',
                    qtip: 'В архив',
                    callback: this.archiveItem
                }
            ],
            keepSelection: true
        };
    }
});