Ext.define('Cement.view.warehouse.internal.lists.Archive', {
    extend: 'Cement.view.warehouse.internal.lists.New',
    alias: 'widget.warehouse_internal_lists_archive',

    gridStore: 'warehouse.internal.Archive',
    gridStateId: 'stateWarehouseInternalArchive',
    gridStateful: false,

    printUrl: Cement.Config.url.warehouse.internal.archive.printGrid,
    helpUrl: Cement.Config.url.warehouse.internal.archive.help,
    deleteUrl: Cement.Config.url.warehouse.internal.archive.deleteUrl,
    unArchiveItemUrl: Cement.Config.url.warehouse.internal.archive.unArchiveItem,
    showCreateButton: false,
    shownTitle: null,

    getActionColumns: function () {
        return {
            xtype: 'rowactions',
            hideable: false,
            resizeable: false,
            locked: true,
            width: 46,
            actions: [
                {
                    iconCls: 'icon-unarchive-item',
                    qtip: 'Из архива',
                    callback: this.unArchiveItem
                },
                {
                    iconCls: 'icon-delete-item',
                    qtip: 'Удалить',
                    callback: this.deleteItem
                }
            ],
            keepSelection: true
        };
    },

    getGridColumns: function () {
        var result = [
            { text: 'Тип', dataIndex: 'p_type', width: 80 },
            { text: 'Вид', dataIndex: 'p_kind_display', width: 80 },
            { text: 'Номер', dataIndex: 'p_number', width: 100 },
            { text: 'Дата', dataIndex: 'p_date', width: 80 },
            { text: 'Склад', dataIndex: 'p_warehouse_display', width: 150 },
            { text: 'Товар/Услуга', dataIndex: 'p_product_display', width: 150 },
            { text: 'Ед.изм.', dataIndex: 'p_measure_unit_display', width: 80 },
            { text: 'Кол-во план', dataIndex: 'p_count', width: 80 },
            { text: 'Кол-во факт', dataIndex: 'p_count_fact', width: 80 },
            { text: 'Основание', dataIndex: 'p_cause_display', width: 80 },
            { text: 'Статус', dataIndex: 'p_state_display', width: 80 },
            { text: 'Примечание', dataIndex: 'p_description', flex: 1 }
        ];

        result = this.mergeActions(result);
        return result;
    }
});