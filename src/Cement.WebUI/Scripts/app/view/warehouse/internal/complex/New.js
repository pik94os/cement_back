Ext.define('Cement.view.warehouse.internal.complex.New', {
	extend: 'Cement.view.basic.Complex',
	alias: 'widget.warehouse_internal_complex_new',
    border: 0,
    enableDetail: false,
    autoLoadStore: false,
    priceId: null,
    bodyPadding: 0,
    padding: 0,
    header: false,
    rowsCount: 3,
    title: 'Новые',
    tabTitle: 'Новые',
    cls: 'no-side-borders',
    topXType: 'warehouse_internal_lists_new',
    topDetailType: 'Cement.view.warehouse.internal.view.View',
    bottomTitle: 'Связанные документы',
    bottomTabs: [{
        title: 'Связанные документы',
        xtype: 'warehouse_internal_bottom_connected_documents',
        detailType: 'Cement.view.warehouse.internal.view.View',
        constantFilterParam: 'p_base_doc',
        childrenConstantFilterParam: 'p_base_doc',
        border: 0
    }],
    subBottomTitle: 'Справочник',
    subBottomTabs: [
        {
            title: 'Склады',
            shownTitle: 'Склады',
            xtype: 'warehouse_internal_bottom_new_warehouses',
            detailType: 'Cement.view.corporate.warehouse.View'
        },
        {
            title: 'Товары',
            shownTitle: 'Товары',
            xtype: 'warehouse_internal_bottom_new_products',
            detailType: 'Cement.view.products.products.View'
        },
        {
            title: 'Услуги',
            shownTitle: 'Услуги',
            xtype: 'warehouse_internal_bottom_new_services',
            detailType: 'Cement.view.products.services.View'
        },
        {
            title: 'Маршруты',
            shownTitle: 'Маршруты',
            xtype: 'warehouse_internal_bottom_new_routes',
            detailType: 'Cement.view.products.price.RouteView'
        }
    ]
});