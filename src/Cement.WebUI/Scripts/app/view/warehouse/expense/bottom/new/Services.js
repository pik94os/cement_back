Ext.define('Cement.view.warehouse.expense.bottom.new.Services', {
    extend: 'Cement.view.products.price.bottom.NewServices',
    alias: 'widget.warehouse_expense_bottom_new_services',
    gridStore: 'warehouse.expense.bottom.new.Services',
    gridStateId: 'stateWarehouseExpenseBottomNewServices'
});