Ext.define('Cement.view.warehouse.expense.bottom.new.Products', {
    extend: 'Cement.view.products.price.bottom.NewProducts',
    alias: 'widget.warehouse_expense_bottom_new_products',
    gridStore: 'warehouse.expense.bottom.new.Products',
    gridStateId: 'stateWarehouseExpenseBottomNewProducts'
});