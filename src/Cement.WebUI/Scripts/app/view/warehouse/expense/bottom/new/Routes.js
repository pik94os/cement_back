Ext.define('Cement.view.warehouse.expense.bottom.new.Routes', {
    extend: 'Cement.view.office_work.correspondention.bottom.new_c.Routes',
    alias: 'widget.warehouse_expense_bottom_new_routes',
    gridStore: 'warehouse.expense.bottom.new.Routes',
    gridStateId: 'stateWarehouseExpenseBottomNewRoutes'
});