Ext.define('Cement.view.warehouse.expense.lists.Archive', {
    extend: 'Cement.view.warehouse.expense.lists.New',
    alias: 'widget.warehouse_expense_lists_archive',

    gridStore: 'warehouse.expense.Archive',
    gridStateId: 'stateWarehouseExpenseArchive',
    gridStateful: false,

    printUrl: Cement.Config.url.warehouse.expense.archive.printGrid,
    helpUrl: Cement.Config.url.warehouse.expense.archive.help,
    deleteUrl: Cement.Config.url.warehouse.expense.archive.deleteUrl,
    unArchiveItemUrl: Cement.Config.url.warehouse.expense.archive.unArchiveItem,
    showCreateButton: false,
    shownTitle: null,

    getActionColumns: function () {
        return {
            xtype: 'rowactions',
            hideable: false,
            resizeable: false,
            locked: true,
            width: 46,
            actions: [
                {
                    iconCls: 'icon-unarchive-item',
                    qtip: 'Из архива',
                    callback: this.unArchiveItem
                },
                {
                    iconCls: 'icon-delete-item',
                    qtip: 'Удалить',
                    callback: this.deleteItem
                }
            ],
            keepSelection: true
        };
    },

    getGridColumns: function () {
        var result = this.callParent(arguments);

        result.unshift({ text: 'Тип', dataIndex: 'p_type', width: 80 });

        return result;
    }
});