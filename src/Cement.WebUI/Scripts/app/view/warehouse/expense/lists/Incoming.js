Ext.define('Cement.view.warehouse.expense.lists.Incoming', {
    extend: 'Cement.view.warehouse.expense.lists.New',
    alias: 'widget.warehouse_expense_lists_incoming',

    gridStore: 'warehouse.expense.Incoming',
    gridStateId: 'stateWarehouseExpenseIncoming',

    printUrl: Cement.Config.url.warehouse.expense.incoming.printGrid,
    helpUrl: Cement.Config.url.warehouse.expense.incoming.help,
    deleteUrl: Cement.Config.url.warehouse.expense.incoming.deleteUrl,
    archiveItemUrl: Cement.Config.url.warehouse.expense.incoming.archiveItem,
    showCreateButton: false,
    shownTitle: null,

    getActionColumns: function () {
        return {
            xtype: 'rowactions',
            hideable: false,
            resizeable: false,
            locked: true,
            width: 46,
            actions: [
                {
                    iconCls: 'icon-archive-item',
                    qtip: 'В архив',
                    callback: this.archiveItem
                }
            ],
            keepSelection: true
        };
    }
});