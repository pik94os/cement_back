Ext.define('Cement.view.warehouse.expense.lists.Outcoming', {
    extend: 'Cement.view.warehouse.expense.lists.New',
    alias: 'widget.warehouse_expense_lists_outcoming',

    gridStore: 'warehouse.expense.Outcoming',
    gridStateId: 'stateWarehouseExpenseOutcoming',

    printUrl: Cement.Config.url.warehouse.expense.outcoming.printGrid,
    helpUrl: Cement.Config.url.warehouse.expense.outcoming.help,
    deleteUrl: Cement.Config.url.warehouse.expense.outcoming.deleteUrl,
    archiveItemUrl: Cement.Config.url.warehouse.expense.outcoming.archiveItem,
    showCreateButton: false,
    shownTitle: null,

    getActionColumns: function () {
        return {
            xtype: 'rowactions',
            hideable: false,
            resizeable: false,
            locked: true,
            width: 46,
            actions: [
                {
                    iconCls: 'icon-archive-item',
                    qtip: 'В архив',
                    callback: this.archiveItem
                }
            ],
            keepSelection: true
        };
    }
});