Ext.define('Cement.view.warehouse.expense.complex.Incoming', {
	extend: 'Cement.view.basic.Complex',
	alias: 'widget.warehouse_expense_complex_incoming',
    border: 0,
    enableDetail: false,
    autoLoadStore: false,
    priceId: null,
    bodyPadding: 0,
    padding: 0,
    header: false,
    rowsCount: 3,
    title: 'Входящие',
    tabTitle: 'Входящие',
    cls: 'no-side-borders',
    topXType: 'warehouse_expense_lists_incoming',
    topDetailType: 'Cement.view.warehouse.expense.view.View',
    bottomTitle: 'Связанные документы',
    bottomTabs: [{
        title: 'Связанные документы',
        xtype: 'warehouse_expense_bottom_connected_documents',
        detailType: 'Cement.view.warehouse.expense.view.View',
        constantFilterParam: 'p_base_doc',
        childrenConstantFilterParam: 'p_base_doc',
        border: 0
    }],
    subBottomTitle: 'Справочник',
    subBottomTabs: [
        {
            title: 'Склады',
            shownTitle: 'Склады',
            xtype: 'warehouse_expense_bottom_new_warehouses',
            detailType: 'Cement.view.corporate.warehouse.View'
        },
        {
            title: 'Поставщик',
            shownTitle: 'Поставщик',
            xtype: 'warehouse_expense_bottom_new_providers',
            detailType: 'Cement.view.corporate.firm.View'
        },
        {
            title: 'Товары',
            shownTitle: 'Товары',
            xtype: 'warehouse_expense_bottom_new_products',
            detailType: 'Cement.view.products.products.View'
        },
        {
            title: 'Услуги',
            shownTitle: 'Услуги',
            xtype: 'warehouse_expense_bottom_new_services',
            detailType: 'Cement.view.products.services.View'
        },
        {
            title: 'Маршруты',
            shownTitle: 'Маршруты',
            xtype: 'warehouse_expense_bottom_new_routes',
            detailType: 'Cement.view.products.price.RouteView'
        }
    ]
});