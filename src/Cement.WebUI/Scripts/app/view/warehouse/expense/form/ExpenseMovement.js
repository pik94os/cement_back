﻿Ext.define('Cement.view.warehouse.expense.form.ExpenseMovement', {
    extend: 'Cement.view.warehouse.expense.form.Expense',
    alias: 'widget.warehouse_expense_form_expense_movement',
    requestField: 'p_stockmovementrequest_id',
    formType: Cement.Config.warehouse.expense_kinds.movement,
    bottomGridStoreProxy: Cement.Config.url.warehouse.expense.new_i.signedMovementRequestProxy,
    
    storeFields: ['id', 'p_number', 'p_date', 'p_kind', 'p_warehouse_display', 'p_warehouse_address', 'p_warehouse_to_display', 'p_warehouse_to_address', 'p_product_display', 'p_measure_unit_display', 'p_count', 'p_product_count_fact', 'p_product_price', 'p_product_tax', 'p_product_sum', 'p_description'],

    getCommonColumns: function (isSrc) {
        return [
            { text: '№ п/п', dataIndex: 'p_number', width: 60 },
            { text: 'Дата', dataIndex: 'p_date', width: 60 },
            { text: 'Вид', dataIndex: 'p_kind', width: 140 },
            {
                text: 'Склад',
                columns: [
                    { text: 'Наименование', dataIndex: 'p_warehouse_display', width: 100 },
                    { text: 'Адрес', dataIndex: 'p_warehouse_address', width: 100 }
                ]
            },
            {
                text: 'Товар',
                columns: [
                    { text: 'Наименование', dataIndex: 'p_product_display', width: 100 },
                    { text: 'Ед.изм.', dataIndex: 'p_measure_unit_display', width: 50 },
                    { text: 'Кол-во план', dataIndex: 'p_count', width: 80 /** (isSrc ? 2 : 1)*/ },
                    //{ text: 'Кол-во факт', hidden: isSrc, hideable: false, dataIndex: 'p_product_count_fact', width: 80, editor: { xtype: 'numberfield' } },
                    { text: 'Цена', dataIndex: 'p_product_price', width: 80 },
                    { text: 'Налог', dataIndex: 'p_product_tax', width: 50 },
                    { text: 'Сумма', dataIndex: 'p_product_sum', width: 80 }
                ]
            },
            { text: 'Примечание', dataIndex: 'p_description', flex: 1 }
        ];
    },

    onDstPopulate: function (record) {
        this.down('textfield[name=p_warehouse_display]').setValue(record.get('p_warehouse_to_display'));
        this.down('textfield[name=p_warehouse_address]').setValue(record.get('p_warehouse_to_address'));
    }
});