Ext.define('Cement.view.warehouse.expense.form.Expense', {
    extend: 'Cement.view.basic.DoubleGridForm',
	alias: 'widget.warehouse_expense_form_expense',
	fieldsDisabled: false,
	url: Cement.Config.url.warehouse.expense.new_i.saveUrl,
	method: Cement.Config.url.warehouse.expense.new_i.saveMethod,
	printUrl: Cement.Config.url.warehouse.expense.new_i.printItem,

	loadItemUrl: Cement.Config.url.warehouse.expense.new_i.loadItem,
	loadItemProperty: 'p_base_doc',
	getNewNumberUrl: Cement.Config.url.warehouse.expense.new_i.newNumberUrl,

    formType: 0, 
	requestField: 'p_request_id',
    bottomGridStoreProxy: null,

    getTopItems: function () {
        return [
            {
                xtype: 'hiddenfield',
                name: this.requestField
            },
            {
                xtype: 'hiddenfield',
                name: 'p_type',
                value: this.formType
            },
            {
                fieldLabel: 'Номер',
                name: 'p_number',
                allowBlank: false,
                readOnly: true
            },
            {
                xtype: 'datefield',
                fieldLabel: 'Дата',
                name: 'p_date',
                allowBlank: false
            },
            {
                disabled: true,
                fieldLabel: 'Склад',
                name: 'p_warehouse_display'
            },
            {
                disabled: true,
                fieldLabel: 'Адрес',
                name: 'p_warehouse_address'
            }
        ];
    },

    getBottomItems: function() {
        return [
            {
                xtype: 'numberfield',
                hideTrigger: true,
                fieldLabel: 'Количество факт',
                name: 'p_count_fact',
                allowBlank: false
            }
        ];
    },

    getBottomGridStore: function () {
        return Ext.create('Ext.data.Store', {
            fields: this.storeFields,
            autoLoad: false,
            proxy: this.bottomGridStoreProxy
        });
    },
    
    beforeSubmit: function (callback) {
        var me = this;

        if (me.dstStore.getCount() == 0) {
            Cement.Msg.warning('Выберите заявку!');
            return;
        }

        me.down(Ext.String.format('hiddenfield[name={0}]', me.requestField)).setValue(me.dstStore.getAt(0).get('id'));

        callback.call();
    },
    
    onDstPopulate: function (record) {
        this.down('textfield[name=p_warehouse_display]').setValue(record.get('p_warehouse_display'));
        this.down('textfield[name=p_warehouse_address]').setValue(record.get('p_warehouse_address'));
    },

    onDstUnPopulate: function (record) {
        this.down('textfield[name=p_warehouse_display]').setValue(null);
        this.down('textfield[name=p_warehouse_address]').setValue(null);
    },
    
    afterRecordLoad: function (rec) {
        this.dstStore.removeAll();
        this.dstStore.add(rec.get('p_request'));
        this.srcStore.load();
    },

    onCreate: function () {
        this.srcStore.load();
    }
});