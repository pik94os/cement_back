﻿Ext.define('Cement.view.warehouse.expense.form.ExpenseWriteoff', {
    extend: 'Cement.view.warehouse.expense.form.Expense',
    alias: 'widget.warehouse_expense_form_expense_writeoff',
    requestField: 'p_stockwriteoffrequest_id',
    formType: Cement.Config.warehouse.expense_kinds.writeoff,
    bottomGridStoreProxy: Cement.Config.url.warehouse.expense.new_i.signedWriteoffRequestProxy,

    storeFields: ['id', 'p_number', 'p_date', 'p_kind', 'p_product_display', 'p_measure_unit_display', 'p_count', 'p_product_count_fact', 'p_product_price', 'p_product_tax', 'p_product_sum', 'p_description', 'p_warehouse_display', 'p_warehouse_address', 'p_warehouse'],
    
    getTopItems: function () {
        var items = this.callParent(arguments);

        items.push(
        {
            disabled: true,
            fieldLabel: 'Основание',
            name: 'p_basement'
        });

        return items;
    },

    getCommonColumns: function (isSrc) {
        return [
            { text: '№ п/п', dataIndex: 'p_number', width: 60 },
            { text: 'Дата', dataIndex: 'p_date', width: 60 },
            { text: 'Вид', dataIndex: 'p_kind', width: 110 },
            { 
                text: 'Товар',
                columns: [
                    { text: 'Наименование', dataIndex: 'p_product_display', width: 100 },
                    { text: 'Ед.изм.', dataIndex: 'p_measure_unit_display', width: 50 },
                    { text: 'Кол-во план', dataIndex: 'p_count', width: 80 /* * (isSrc ? 2 : 1)*/ },
                    //{ text: 'Кол-во факт', hidden: isSrc, hideable: false, dataIndex: 'p_product_count_fact', width: 80, editor: {xtype: 'numberfield'} },
                    { text: 'Цена', dataIndex: 'p_product_price', width: 80 },
                    { text: 'Налог', dataIndex: 'p_product_tax', width: 50 },
                    { text: 'Сумма', dataIndex: 'p_product_sum', width: 80 }
                ]
            },
            { text: 'Примечание', dataIndex: 'p_description', flex: 1 }
        ];
    },
});