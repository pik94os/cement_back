﻿Ext.define('Cement.view.warehouse.expense.form.ExpenseSale', {
    extend: 'Cement.view.warehouse.expense.form.Expense',
    alias: 'widget.warehouse_expense_form_expense_sale',
    requestField: 'p_productrequest_id',
    formType: Cement.Config.warehouse.expense_kinds.sale,
    bottomGridStoreProxy: Cement.Config.url.warehouse.expense.new_i.signedSaleRequestProxy,

    storeFields: ['id', 'p_number', 'p_date', 'p_kind', 'p_consignee_name', 'p_consignee_address', 'p_product_display', 'p_measure_unit_display', 'p_count', 'p_product_count_fact', 'p_product_price', 'p_product_tax', 'p_product_sum', 'p_description'],

    getTopItems: function () {
        return [
            {
                xtype: 'hiddenfield',
                name: this.requestField
            },
            {
                xtype: 'hiddenfield',
                name: 'p_type',
                value: this.formType
            },
            {
                fieldLabel: 'Номер',
                name: 'p_number',
                allowBlank: false,
                readOnly: true
            },
            {
                xtype: 'datefield',
                fieldLabel: 'Дата',
                name: 'p_date',
                allowBlank: false
            },
            {
                xtype: 'selectfield',
                fieldLabel: 'Склад',
                name: 'p_warehouse',
                store: Ext.create('Ext.data.Store', {
                    proxy: getProxy('/api/warehouse/personal'),
                    fields: ['Id', 'Name', 'Address'],
                    autoLoad: false,
                    pageSize: Cement.Config.defaultPageSize,
                    remoteFilter: true,
                    remoteSort: true
                }),
                columns: [
                    { text: 'Наименование', dataIndex: 'Name', filter: { xtype: 'textfield' }, flex: 1 },
                    { text: 'Адрес', dataIndex: 'Address', filter: { xtype: 'textfield' }, flex: 1 }
                ],
                editable: false,
                allowBlank: false,
                listeners: {
                    change: function (self, record) {
                        self.up().down('textfield[name=p_warehouse_address]').setValue(record && record.Address);
                    }
                }
            },
            {
                disabled: true,
                fieldLabel: 'Адрес',
                name: 'p_warehouse_address'
            }
        ];
    },

    getCommonColumns: function (isSrc) {
        return [
            { text: '№ п/п', dataIndex: 'p_number', width: 60 },
            { text: 'Дата', dataIndex: 'p_date', width: 60 },
            { text: 'Вид', dataIndex: 'p_kind', width: 140 },
            {
                text: 'Грузополучатель',
                columns: [
                    { text: 'Наименование', dataIndex: 'p_consignee_name', width: 100 },
                    { text: 'Адрес', dataIndex: 'p_consignee_address', width: 100 }
                ]
            },
            {
                text: 'Товар',
                columns: [
                    { text: 'Наименование', dataIndex: 'p_product_display', width: 100 },
                    { text: 'Ед.изм.', dataIndex: 'p_measure_unit_display', width: 50 },
                    { text: 'Кол-во план', dataIndex: 'p_count', width: 80 /** (isSrc ? 2 : 1)*/ },
                    //{ text: 'Кол-во факт', hidden: isSrc, hideable: false, dataIndex: 'p_product_count_fact', width: 80, editor: { xtype: 'numberfield' } },
                    { text: 'Цена', dataIndex: 'p_product_price', width: 80 },
                    { text: 'Налог', dataIndex: 'p_product_tax', width: 50 },
                    { text: 'Сумма', dataIndex: 'p_product_sum', width: 80 }
                ]
            },
            { text: 'Примечание', dataIndex: 'p_description', flex: 1 }
        ];
    }
});