Ext.define('Cement.view.warehouse.receipt.complex.Archive', {
	extend: 'Cement.view.basic.Complex',
	alias: 'widget.warehouse_receipt_complex_archive',
    border: 0,
    enableDetail: false,
    autoLoadStore: false,
    priceId: null,
    bodyPadding: 0,
    padding: 0,
    header: false,
    rowsCount: 3,
    title: 'Архив',
    tabTitle: 'Архив',
    cls: 'no-side-borders',
    topXType: 'warehouse_receipt_lists_archive',
    topDetailType: 'Cement.view.warehouse.receipt.view.View',
    bottomTitle: 'Связанные документы',
    bottomTabs: [{
        title: 'Связанные документы',
        xtype: 'warehouse_receipt_bottom_connected_documents',
        detailType: 'Cement.view.warehouse.receipt.view.View',
        constantFilterParam: 'p_base_doc',
        childrenConstantFilterParam: 'p_base_doc',
        border: 0
    }],
    subBottomTitle: 'Справочник',
    subBottomTabs: [
        {
            title: 'Склады',
            shownTitle: 'Склады',
            xtype: 'warehouse_receipt_bottom_new_warehouses',
            detailType: 'Cement.view.corporate.warehouse.View'
        },
        {
            title: 'Поставщик',
            shownTitle: 'Поставщик',
            xtype: 'warehouse_receipt_bottom_new_providers',
            detailType: 'Cement.view.corporate.firm.View'
        },
        {
            title: 'Товары',
            shownTitle: 'Товары',
            xtype: 'warehouse_receipt_bottom_new_products',
            detailType: 'Cement.view.products.products.View'
        },
        {
            title: 'Услуги',
            shownTitle: 'Услуги',
            xtype: 'warehouse_receipt_bottom_new_services',
            detailType: 'Cement.view.products.services.View'
        },
        {
            title: 'Маршруты',
            shownTitle: 'Маршруты',
            xtype: 'warehouse_receipt_bottom_new_routes',
            detailType: 'Cement.view.products.price.RouteView'
        }
    ]
});