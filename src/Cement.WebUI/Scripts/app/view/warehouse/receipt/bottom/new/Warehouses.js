﻿Ext.define('Cement.view.warehouse.receipt.bottom.new.Warehouses', {
    extend: 'Cement.view.basic.Grid',
    alias: 'widget.warehouse_receipt_bottom_new_warehouses',
    gridStore: 'warehouse.receipt.bottom.new.Warehouses',
    gridStateId: 'stateWarehouseReceiptBottomNewWarehouses',
    showFilterButton: false,
    autoLoadStore: false,

    //printItemUrl: Cement.Config.url.goods.senders.printItem,
    //printUrl: Cement.Config.url.goods.senders.printGrid,
    //helpUrl: Cement.Config.url.goods.senders.help,

    getActionColumns: function () {
        return null;
    },

    getToolbarItems: function () {
        return null;
    },

    getGridColumns: function () {
        return [
          { text: 'Группа', dataIndex: 'p_group', width: 200, locked: true },
          { text: 'Наименование', dataIndex: 'p_name', width: 200, locked: true },
          { text: 'Адрес', dataIndex: 'p_address', width: 200 },
          { text: 'ИНН', dataIndex: 'p_inn', width: 200 },
          { text: 'КПП', dataIndex: 'p_kpp', width: 200 },
          { text: 'ОГРН', dataIndex: 'p_ogrn', width: 200 },
          { text: 'Вид деятельности', dataIndex: 'p_kind', width: 200 }
        ];
    }
});