Ext.define('Cement.view.warehouse.receipt.bottom.new.Routes', {
    extend: 'Cement.view.office_work.correspondention.bottom.new_c.Routes',
    alias: 'widget.warehouse_receipt_bottom_new_routes',
    gridStore: 'warehouse.receipt.bottom.new.Routes',
    gridStateId: 'stateWarehouseReceiptBottomNewRoutes'
});