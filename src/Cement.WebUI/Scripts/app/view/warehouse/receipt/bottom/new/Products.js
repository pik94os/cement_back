Ext.define('Cement.view.warehouse.receipt.bottom.new.Products', {
    extend: 'Cement.view.products.price.bottom.NewProducts',
    alias: 'widget.warehouse_receipt_bottom_new_products',
    gridStore: 'warehouse.receipt.bottom.new.Products',
    gridStateId: 'stateWarehouseReceiptBottomNewProducts'
});