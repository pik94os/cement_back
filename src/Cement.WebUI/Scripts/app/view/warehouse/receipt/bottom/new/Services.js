Ext.define('Cement.view.warehouse.receipt.bottom.new.Services', {
    extend: 'Cement.view.products.price.bottom.NewServices',
    alias: 'widget.warehouse_receipt_bottom_new_services',
    gridStore: 'warehouse.receipt.bottom.new.Services',
    gridStateId: 'stateWarehouseReceiptBottomNewServices'
});