Ext.define('Cement.view.warehouse.receipt.view.View', {
    extend: 'Cement.view.basic.Form',
    alias: 'widget.warehouse_receipt_view_view',
    fieldsDisabled: true,
    buttons: null,
    border: 0,
    tbar: Cement.Config.modelViewButtons,
    layout: 'autocontainer',
    bbar: null,
    noUserFieldsetsControl: false,
    bodyCls: 'model-form',

    getItems: function () {
        return [
            {
                xtype: 'panel',
                border: 0,
                layout: 'anchor',
                defaultType: 'textfield',
                anchor: '100%',
                bodyPadding: '10 10 10 5',
                autoScroll: true,
                defaults: {
                    anchor: '100%',
                    labelWidth: 110
                },
                items: [
                    {
                        xtype: 'fieldset',
                        layout: 'anchor',
                        defaults: {
                            xtype: 'textfield',
                            anchor: '100%'
                        },
                        border: 0,
                        items: [
                            {
                                fieldLabel: 'Номер',
                                disabled: true,
                                name: 'p_number'
                            },
                            {
                                fieldLabel: 'Дата',
                                disabled: true,
                                name: 'p_date'
                            }
                        ]
                    },
                    {
                        xtype: 'fieldset',
                        collapsed: true,
                        collapsible: true,
                        title: 'Поставщик',
                        defaults: {
                            xtype: 'textfield',
                            anchor: '100%'
                        },
                        items: [
                            {
                                fieldLabel: 'Наименование',
                                disabled: true,
                                name: 'p_provider_display'
                            },
                            {
                                fieldLabel: 'Адрес',
                                disabled: true,
                                name: 'p_provider_address'
                            }
                        ]
                    },
                    {
                        xtype: 'fieldset',
                        collapsed: true,
                        collapsible: true,
                        title: 'Склад',
                        defaults: {
                            xtype: 'textfield',
                            anchor: '100%'
                        },
                        items: [
                            {
                                fieldLabel: 'Наименование',
                                disabled: true,
                                name: 'p_warehouse_display'
                            },
                            {
                                fieldLabel: 'Адрес',
                                disabled: true,
                                name: 'p_warehouse_address'
                            }
                        ]
                    },
                    {
                        xtype: 'fieldset',
                        collapsed: true,
                        collapsible: true,
                        title: 'Товар/Услуга',
                        defaults: {
                            xtype: 'textfield',
                            anchor: '100%'
                        },
                        items: [
                            {
                                fieldLabel: 'Основание',
                                disabled: true,
                                name: 'p_cause_display'
                            },
                            {
                                fieldLabel: 'Наименование',
                                disabled: true,
                                name: 'p_product_display'
                            },
                            {
                                fieldLabel: 'Единица измерения',
                                disabled: true,
                                name: 'p_measure_unit_display'
                            },
                            {
                                fieldLabel: 'Кол-во план',
                                disabled: true,
                                name: 'p_count'
                            },
                            {
                                fieldLabel: 'Кол-во факт',
                                disabled: true,
                                name: 'p_count_fact'
                            }
                        ]
                    },
                    {
                        xtype: 'fieldset',
                        collapsed: true,
                        collapsible: true,
                        title: 'Дополнительные данные',
                        defaults: {
                            xtype: 'textfield',
                            anchor: '100%'
                        },
                        items: [
                            {
                                xtype: 'textarea',
                                fieldLabel: 'Примечание',
                                disabled: true,
                                name: 'p_description'
                            }
                        ]
                    }
                ]
            }
        ];
    },

    initComponent: function () {
        Ext.apply(this, {
            items: [
                {
                    xtype: 'panel',
                    border: 0,
                    layout: 'anchor',
                    defaultType: 'textfield',
                    anchor: '100%',
                    bodyPadding: '10 10 10 5',
                    autoScroll: true,
                    defaults: {
                        anchor: '100%',
                        labelWidth: 110
                    },
                    items: this.getItems()
                }
            ]
        });
        this.callParent(arguments);
    }
});
