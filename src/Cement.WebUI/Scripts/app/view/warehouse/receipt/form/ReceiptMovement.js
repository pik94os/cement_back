﻿Ext.define('Cement.view.warehouse.receipt.form.ReceiptMovement', {
    extend: 'Cement.view.warehouse.receipt.form.Receipt',
    alias: 'widget.warehouse_receipt_form_receipt_movement',

    bottomGridStoreProxy: Cement.Config.url.warehouse.receipt.new_i.signedMovementDataProxy,
    formType: Cement.Config.warehouse.receipt_kinds.movement,
    orderItemField: 'p_order_item_move',

    getCommonColumns: function () {
        return [
            { text: '№ п/п', dataIndex: 'p_number', width: 80 },
            { text: 'Дата', dataIndex: 'p_date', width: 80 },
            { text: 'Вид', dataIndex: 'p_kind', width: 80 },
            {
                text: 'Склад',
                columns: [
                    { text: 'Наименование', dataIndex: 'p_warehouse_name', width: 100 },
                    { text: 'Адрес', dataIndex: 'p_warehouse_address', width: 100 }
                ]
            },
            {
                text: 'Товар',
                columns: [
                    { text: 'Наименование', dataIndex: 'p_product_display', width: 100 },
                    { text: 'Ед.изм.', dataIndex: 'p_measure_unit_display', width: 50 },
                    { text: 'Кол-во', dataIndex: 'p_count', width: 50 },
                    { text: 'Цена', dataIndex: 'p_product_price', width: 150 },
                    { text: 'Налог', dataIndex: 'p_product_tax', width: 50 },
                    { text: 'Сумма', dataIndex: 'p_product_sum', width: 50 }
                ]
            },
           // { text: 'Статус', dataIndex: 'p_state', width: 80 },
            { text: 'Примечание', dataIndex: 'p_description', width: 80 }
        ];
    },
});