Ext.define('Cement.view.warehouse.receipt.form.Receipt', {
    extend: 'Cement.view.basic.DoubleGridForm',
    alias: 'widget.warehouse_receipt_form_receipt',
    fieldsDisabled: false,
    url: Cement.Config.url.warehouse.receipt.new_i.saveUrl,
    method: Cement.Config.url.warehouse.receipt.new_i.saveMethod,
    printUrl: Cement.Config.url.warehouse.receipt.new_i.printItem,

    loadItemUrl: Cement.Config.url.warehouse.expense.new_i.loadItem,
    loadItemProperty: 'p_base_doc',
    getNewNumberUrl: Cement.Config.url.warehouse.receipt.new_i.newNumberUrl,

    hasBottomPanel: false,
    formType: 0,
    orderItemField: 'p_order_item_id',
    bottomGridStoreProxy: null,
    warningOnSave: 'Выберите хотя бы одну запись!',

    storeFields: ['id', 'p_number', 'p_date', 'p_kind', 'p_warehouse_display', 'p_warehouse_address', 'p_product_display', 'p_measure_unit_display', 'p_count', 'p_product_count_fact', 'p_product_price', 'p_product_tax', 'p_product_sum', 'p_state', 'p_description'],
    
    getTopItems: function () {
        return [
            {
                xtype: 'hiddenfield',
                name: this.orderItemField
            },
            {
                xtype: 'hiddenfield',
                name: 'p_type',
                value: this.formType
            },
            {
                fieldLabel: 'Номер',
                name: 'p_number',
                allowBlank: false,
                readOnly: true
            },
            {
                xtype: 'datefield',
                fieldLabel: 'Дата',
                name: 'p_date',
                allowBlank: false
            },
            {
                disabled: true,
                fieldLabel: 'Склад',
                name: 'p_warehouse_display'
            },
            {
                disabled: true,
                fieldLabel: 'Адрес',
                name: 'p_warehouse_address'
            }
        ];
    },

    getBottomGridStore: function () {
        return Ext.create('Ext.data.Store', {
            fields: this.storeFields,
            autoLoad: false,
            proxy: this.bottomGridStoreProxy
        });
    },

    beforeSubmit: function (callback) {
        var me = this;

        if (me.dstStore.getCount() == 0) {
            Cement.Msg.warning(me.warningOnSave);
            return;
        }

        me.down(Ext.String.format('hiddenfield[name={0}]', me.orderItemField)).setValue(me.dstStore.getAt(0).get('id'));

        callback.call();
    },

    onDstPopulate: function (record) {
        this.down('textfield[name=p_warehouse_display]').setValue(record.get('p_warehouse_display'));
        this.down('textfield[name=p_warehouse_address]').setValue(record.get('p_warehouse_address'));
    },

    onDstUnPopulate: function (record) {
        this.down('textfield[name=p_warehouse_display]').setValue(null);
        this.down('textfield[name=p_warehouse_address]').setValue(null);
    },

    afterRecordLoad: function (rec) {
        this.dstStore.removeAll();
        this.dstStore.add(rec.get('p_request'));
        this.srcStore.load();
    },

    onCreate: function () {
        this.srcStore.load();
    }

});