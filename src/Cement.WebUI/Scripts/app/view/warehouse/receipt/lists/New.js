Ext.define('Cement.view.warehouse.receipt.lists.New', {
	extend: 'Cement.view.basic.Grid',
	alias: 'widget.warehouse_receipt_lists_new',
    autoLoadStore: true,

    bbarText: 'Показаны заявки {0} - {1} из {2}',
    bbarEmptyMsg: 'Нет заявок',
    bbarUsersText: 'Заявок на странице: ',

    gridStore: 'warehouse.receipt.New',
    gridStateId: 'stateWarehouseReceiptNew',

    printUrl: Cement.Config.url.warehouse.receipt.new_i.printGrid,
    helpUrl: Cement.Config.url.warehouse.receipt.new_i.help,
    deleteUrl: Cement.Config.url.warehouse.receipt.new_i.deleteUrl,

    shownTitle: null,

    creatorTree: Ext.clone(Cement.Creators.warehouse.children[Cement.Creators.warehouse.receiptIndex]),
    createWindowTitle: 'Создать',

    getActionColumns: function () {
        return {
            xtype: 'rowactions',
            hideable: false,
            resizeable: false,
            locked: true,
            width: 46,
            actions: [
                {
                    iconCls: 'icon-edit-item',
                    qtip: 'Редактировать',
                    callback: this.editItem
                },
                {
                    iconCls: 'icon-delete-item',
                    qtip: 'Удалить',
                    callback: this.deleteItem
                }
            ],
            keepSelection: true
        };
    },

    getFilterItems: function () {
        return [{
            text: 'Выделить все',
            checked: true,
            checkHandler: this.toggleFilterSelection
        }, '-', {
            text: 'Номер',
            kind: 'selector',
            field_name: 'p_number',
            checked: true
        }];
    },

    getGridColumns: function () {
        var result = [
            { text: 'Номер', dataIndex: 'p_number', width: 100 },
            { text: 'Дата', dataIndex: 'p_date', width: 80 },
            { text: 'Поставщик', dataIndex: 'p_provider_display', width: 80 },
            { text: 'Склад', dataIndex: 'p_warehouse_display', width: 150 },
            { text: 'Основание', dataIndex: 'p_cause_display', width: 80 },
            { text: 'Товар/Услуга', dataIndex: 'p_product_display', width: 150 },
            { text: 'Ед.изм.', dataIndex: 'p_measure_unit_display', width: 80 },
            { text: 'Кол-во', dataIndex: 'p_count', width: 80 },
            { text: 'Факт. кол-во', dataIndex: 'p_count_fact', width: 80 },
            { text: 'Статус', dataIndex: 'p_state', width: 80 },
            { text: 'Примечание', dataIndex: 'p_description', flex: 1 }
        ];

        result = this.mergeActions(result);
        return result;
    },

    initComponent: function () {
        Ext.apply(this, {
            title: this.shownTitle
        });
        this.callParent(arguments);
    }
});