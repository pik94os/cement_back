Ext.define('Cement.view.warehouse.receipt.lists.Archive', {
    extend: 'Cement.view.warehouse.receipt.lists.New',
    alias: 'widget.warehouse_receipt_lists_archive',
    autoLoadStore: true,

    gridStore: 'warehouse.receipt.Archive',
    gridStateId: 'stateWarehouseReceiptArchive',
    gridStateful: false,

    printUrl: Cement.Config.url.warehouse.receipt.archive.printGrid,
    helpUrl: Cement.Config.url.warehouse.receipt.archive.help,
    deleteUrl: Cement.Config.url.warehouse.receipt.archive.deleteUrl,
    unArchiveItemUrl: Cement.Config.url.warehouse.receipt.archive.unArchiveItem,
    showCreateButton: false,
    shownTitle: null,

    getActionColumns: function () {
        return {
            xtype: 'rowactions',
            hideable: false,
            resizeable: false,
            locked: true,
            width: 46,
            actions: [
                {
                    iconCls: 'icon-unarchive-item',
                    qtip: 'Из архива',
                    callback: this.unArchiveItem
                },
                {
                    iconCls: 'icon-delete-item',
                    qtip: 'Удалить',
                    callback: this.deleteItem
                }
            ],
            keepSelection: true
        };
    },

    getGridColumns: function () {
        var result = [
            { text: 'Тип', dataIndex: 'p_type', width: 80 },
            { text: 'Номер', dataIndex: 'p_number', width: 100 },
            { text: 'Дата', dataIndex: 'p_date', width: 80 },
            { text: 'Поставщик', dataIndex: 'p_provider_display', width: 80 },
            { text: 'Склад', dataIndex: 'p_warehouse_display', width: 150 },
            { text: 'Основание', dataIndex: 'p_cause_display', width: 80 },
            { text: 'Товар/Услуга', dataIndex: 'p_product_display', width: 150 },
            { text: 'Ед.изм.', dataIndex: 'p_measure_unit_display', width: 80 },
            { text: 'Кол-во', dataIndex: 'p_count', width: 80 },
            { text: 'Факт. кол-во', dataIndex: 'p_count_fact', width: 80 },
            { text: 'Статус', dataIndex: 'p_state', width: 80 },
            { text: 'Примечание', dataIndex: 'p_description', flex: 1 }
        ];

        result = this.mergeActions(result);
        return result;
    }
});