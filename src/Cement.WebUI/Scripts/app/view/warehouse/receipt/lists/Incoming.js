Ext.define('Cement.view.warehouse.receipt.lists.Incoming', {
    extend: 'Cement.view.warehouse.receipt.lists.New',
    alias: 'widget.warehouse_receipt_lists_incoming',
    autoLoadStore: true,

    gridStore: 'warehouse.receipt.Incoming',
    gridStateId: 'stateWarehouseReceiptIncoming',

    printUrl: Cement.Config.url.warehouse.receipt.incoming.printGrid,
    helpUrl: Cement.Config.url.warehouse.receipt.incoming.help,
    deleteUrl: Cement.Config.url.warehouse.receipt.incoming.deleteUrl,
    archiveItemUrl: Cement.Config.url.warehouse.receipt.incoming.archiveItem,
    showCreateButton: false,
    shownTitle: null,

    getActionColumns: function () {
        return {
            xtype: 'rowactions',
            hideable: false,
            resizeable: false,
            locked: true,
            width: 46,
            actions: [
                {
                    iconCls: 'icon-archive-item',
                    qtip: 'В архив',
                    callback: this.archiveItem
                }
            ],
            keepSelection: true
        };
    }
});