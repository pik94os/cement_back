Ext.define('Cement.view.warehouse.receipt.lists.Outcoming', {
    extend: 'Cement.view.warehouse.receipt.lists.New',
    alias: 'widget.warehouse_receipt_lists_outcoming',
    autoLoadStore: true,

    gridStore: 'warehouse.receipt.Outcoming',
    gridStateId: 'stateWarehouseReceiptOutcoming',

    printUrl: Cement.Config.url.warehouse.receipt.outcoming.printGrid,
    helpUrl: Cement.Config.url.warehouse.receipt.outcoming.help,
    deleteUrl: Cement.Config.url.warehouse.receipt.outcoming.deleteUrl,
    archiveItemUrl: Cement.Config.url.warehouse.receipt.outcoming.archiveItem,
    showCreateButton: false,
    shownTitle: null,

    getActionColumns: function () {
        return {
            xtype: 'rowactions',
            hideable: false,
            resizeable: false,
            locked: true,
            width: 46,
            actions: [
                {
                    iconCls: 'icon-archive-item',
                    qtip: 'В архив',
                    callback: this.archiveItem
                }
            ],
            keepSelection: true
        };
    }
});