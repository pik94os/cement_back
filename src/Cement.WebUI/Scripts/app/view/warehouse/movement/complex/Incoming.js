Ext.define('Cement.view.warehouse.movement.complex.Incoming', {
	extend: 'Cement.view.basic.Complex',
	alias: 'widget.warehouse_movement_complex_incoming',
    border: 0,
    enableDetail: false,
    autoLoadStore: false,
    priceId: null,
    bodyPadding: 0,
    padding: 0,
    header: false,
    rowsCount: 3,
    title: 'Входящие',
    tabTitle: 'Входящие',
    cls: 'no-side-borders',
    topXType: 'warehouse_movement_lists_incoming',
    topDetailType: 'Cement.view.warehouse.movement.view.View',
    bottomTitle: 'Связанные документы',
    bottomTabs: [{
        title: 'Связанные документы',
        xtype: 'warehouse_movement_bottom_connected_documents',
        detailType: 'Cement.view.warehouse.movement.view.View',
        constantFilterParam: 'p_base_doc',
        childrenConstantFilterParam: 'p_base_doc',
        border: 0
    }],
    subBottomTitle: 'Справочник',
    subBottomTabs: [
        {
            title: 'Движение',
            shownTitle: 'Движение',
            xtype: 'warehouse_movement_bottom_new_movement',
            detailType: 'Cement.view.products.price.RouteView'
        }
    ]
});