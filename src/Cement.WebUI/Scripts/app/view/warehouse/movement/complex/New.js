Ext.define('Cement.view.warehouse.movement.complex.New', {
	extend: 'Cement.view.basic.Complex',
	alias: 'widget.warehouse_movement_complex_new',
    border: 0,
    enableDetail: false,
    autoLoadStore: false,
    priceId: null,
    bodyPadding: 0,
    padding: 0,
    header: false,
    rowsCount: 3,
    title: 'Новые',
    tabTitle: 'Новые',
    cls: 'simple-form no-side-borders',
    topXType: 'warehouse_movement_lists_new',
    topDetailType: 'Cement.view.warehouse.movement.view.View',
    childFilterColumn: 'stockroomProductHistoryFilterData',
    bottomTitle: 'Связанные документы',
    bottomTabs: [{
        title: 'Связанные документы',
        xtype: 'warehouse_movement_bottom_connected_documents',
        detailType: 'Cement.view.warehouse.movement.view.View',
        constantFilterParam: 'stockroomProductHistoryFilterData',
        border: 0
    }],
    subBottomTitle: 'Справочник',
    subBottomTabs: [
        {
            title: 'Движение',
            shownTitle: 'Движение',
            constantFilterParam: 'stockroomProductHistoryFilterData',
            xtype: 'warehouse_movement_bottom_new_movement',
            detailType: 'Cement.view.products.price.RouteView'
        }
    ]
});