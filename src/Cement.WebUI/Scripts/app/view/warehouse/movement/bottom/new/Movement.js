﻿Ext.define('Cement.view.warehouse.movement.bottom.new.Movement', {
    extend: 'Cement.view.basic.Grid',
    alias: 'widget.warehouse_movement_bottom_new_movement',
    gridStore: 'warehouse.movement.bottom.new.Movements',
    gridStateId: 'stateWarehouseMovementBottomNewMovement',
    showFilterButton: false,
    autoLoadStore: false,

    //printItemUrl: Cement.Config.url.goods.senders.printItem,
    //printUrl: Cement.Config.url.goods.senders.printGrid,
    //helpUrl: Cement.Config.url.goods.senders.help,

    getActionColumns: function () {
        return null;
    },

    getToolbarItems: function () {
        return null;
    },

    getGridColumns: function () {
        return [
            { text: 'Склад', dataIndex: 'p_warehouse_display', width: 100 },
            { text: 'Группа', dataIndex: 'p_group_display', width: 100 },
            { text: 'Подгруппа', dataIndex: 'p_subgroup_display', width: 100 },
            { text: 'Торговая марка', dataIndex: 'p_trade_mark', width: 100 },
            { text: 'Производитель', dataIndex: 'p_manufacturer', width: 100 },
            { text: 'Наименование', dataIndex: 'p_name', flex: 1 },
            { text: 'Ед.изм.', dataIndex: 'p_measure_unit_display', width: 80 },
            { text: 'Начальный остаток', dataIndex: 'p_initial_balance', width: 100 },
            { text: 'Приход', dataIndex: 'p_incoming', width: 80 },
            { text: 'Расход', dataIndex: 'p_rate', width: 80 },
            { text: 'Конечный остаток', dataIndex: 'p_final_balance', width: 100 }
        ];
    }
});