Ext.define('Cement.view.warehouse.movement.lists.Archive', {
    extend: 'Cement.view.warehouse.movement.lists.New',
    alias: 'widget.warehouse_movement_lists_archive',

    gridStore: 'warehouse.movement.Archive',
    gridStateId: 'stateWarehouseMovementArchive',
    gridStateful: false,

    printUrl: Cement.Config.url.warehouse.movement.archive.printGrid,
    helpUrl: Cement.Config.url.warehouse.movement.archive.help,
    deleteUrl: Cement.Config.url.warehouse.movement.archive.deleteUrl,
    unArchiveItemUrl: Cement.Config.url.warehouse.movement.archive.unArchiveItem,
    showCreateButton: false,
    shownTitle: null,

    getActionColumns: function () {
        return {
            xtype: 'rowactions',
            hideable: false,
            resizeable: false,
            locked: true,
            width: 46,
            actions: [
                {
                    iconCls: 'icon-unarchive-item',
                    qtip: 'Из архива',
                    callback: this.unArchiveItem
                },
                {
                    iconCls: 'icon-delete-item',
                    qtip: 'Удалить',
                    callback: this.deleteItem
                }
            ],
            keepSelection: true
        };
    }
});