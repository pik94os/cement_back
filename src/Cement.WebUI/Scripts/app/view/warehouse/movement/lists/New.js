Ext.define('Cement.view.warehouse.movement.lists.New', {
    extend: 'Cement.view.basic.Grid',
    alias: 'widget.warehouse_movement_lists_new',
    autoLoadStore: false,

    bbarText: 'Показаны движения {0} - {1} из {2}',
    bbarEmptyMsg: 'Нет движений',
    bbarUsersText: 'Движений на странице: ',
    showCreateButton: false,
    gridStore: 'warehouse.movement.New',
    gridStateId: 'stateWarehouseMovementNew',

    printUrl: Cement.Config.url.warehouse.movement.new_i.printGrid,
    helpUrl: Cement.Config.url.warehouse.movement.new_i.help,
    deleteUrl: Cement.Config.url.warehouse.movement.new_i.deleteUrl,

    getToolbarItems: function () {
        var me = this;
        var baseItems = this.callParent(arguments);
        var dateFrom = Ext.Date.add(new Date(), Ext.Date.DAY, -7);
        var dateTo = new Date();

        var firstCountainer = baseItems.items.shift();

        baseItems.items.unshift({
            xtype: 'datefield',
            name: 'dateTo',
            emptyText: 'До',
            fieldLabel: 'До',
            labelWidth: 25,
            labelAlign: 'right',
            minValue: dateFrom,
            width: 125,
            value: dateTo,
            listeners: {
                change: me.updateConstantFilter,
                scope: me
            }
        });

        baseItems.items.unshift({
            xtype: 'datefield',
            name: 'dateFrom',
            emptyText: 'От',
            fieldLabel: 'От',
            labelWidth: 25,
            labelAlign: 'right',
            maxValue: dateTo,
            width: 125,
            value: dateFrom,
            listeners: {
                change: me.updateConstantFilter,
                scope: me
            }
        });

        firstCountainer.width = 25;

        baseItems.items.unshift(firstCountainer);

        me.constantFilter = [
            {
                property: 'dateFrom',
                value: dateFrom
            },
            {
                property: 'dateTo',
                value: dateTo
            }
        ];

        return baseItems;
    },

    updateConstantFilter: function () {
        var me = this,
            dateFromField = me.down('datefield[name=dateFrom]'),
            dateToField = me.down('datefield[name=dateTo]'),
            dateFrom = dateFromField.getValue(),
            dateTo = dateToField.getValue();

        dateFromField.setMaxValue(dateTo);
        dateToField.setMinValue(dateFrom);

        me.constantFilter = [
            {
                property: 'dateFrom',
                value: dateFrom
            },
            {
                property: 'dateTo',
                value: dateTo
            }
        ];
    },

    getActionColumns: function () {
        return null;
    },

    getFilterItems: function () {
        return [];
        return [{
            text: 'Выделить все',
            checked: true,
            checkHandler: this.toggleFilterSelection
        }, '-', {
            text: 'Наименование',
            kind: 'selector',
            field_name: 'p_name',
            checked: true
        }];
    },

    getGridColumns: function () {
        var result = [
            { text: 'Склад', dataIndex: 'p_warehouse_display', width: 100 },
            { text: 'Группа', dataIndex: 'p_group_display', width: 100 },
            { text: 'Подгруппа', dataIndex: 'p_subgroup_display', width: 100 },
            { text: 'Торговая марка', dataIndex: 'p_trade_mark', width: 100 },
            { text: 'Производитель', dataIndex: 'p_manufacturer', width: 100 },
            { text: 'Наименование', dataIndex: 'p_name', flex: 1 },
            { text: 'Ед.изм.', dataIndex: 'p_measure_unit_display', width: 80 },
            { text: 'Начальный остаток', dataIndex: 'p_initial_balance', width: 100 },
            { text: 'Приход', dataIndex: 'p_incoming', width: 80 },
            { text: 'Расход', dataIndex: 'p_rate', width: 80 },
            { text: 'Конечный остаток', dataIndex: 'p_final_balance', width: 100 }
        ];

        result = this.mergeActions(result);
        return result;
    },
});