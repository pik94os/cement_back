Ext.define('Cement.view.warehouse.movement.lists.Outcoming', {
    extend: 'Cement.view.warehouse.movement.lists.New',
    alias: 'widget.warehouse_movement_lists_outcoming',

    gridStore: 'warehouse.movement.Outcoming',
    gridStateId: 'stateWarehouseMovementOutcoming',

    printUrl: Cement.Config.url.warehouse.movement.outcoming.printGrid,
    helpUrl: Cement.Config.url.warehouse.movement.outcoming.help,
    deleteUrl: Cement.Config.url.warehouse.movement.outcoming.deleteUrl,
    archiveItemUrl: Cement.Config.url.warehouse.movement.outcoming.archiveItem,
    showCreateButton: false,
    shownTitle: null,

    getActionColumns: function () {
        return {
            xtype: 'rowactions',
            hideable: false,
            resizeable: false,
            locked: true,
            width: 46,
            actions: [
                {
                    iconCls: 'icon-archive-item',
                    qtip: 'В архив',
                    callback: this.archiveItem
                }
            ],
            keepSelection: true
        };
    }
});