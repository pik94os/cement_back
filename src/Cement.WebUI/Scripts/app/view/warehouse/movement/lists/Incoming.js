Ext.define('Cement.view.warehouse.movement.lists.Incoming', {
    extend: 'Cement.view.warehouse.movement.lists.New',
    alias: 'widget.warehouse_movement_lists_incoming',

    gridStore: 'warehouse.movement.Incoming',
    gridStateId: 'stateWarehouseMovementIncoming',

    printUrl: Cement.Config.url.warehouse.movement.incoming.printGrid,
    helpUrl: Cement.Config.url.warehouse.movement.incoming.help,
    deleteUrl: Cement.Config.url.warehouse.movement.incoming.deleteUrl,
    archiveItemUrl: Cement.Config.url.warehouse.movement.incoming.archiveItem,
    showCreateButton: false,
    shownTitle: null,

    getActionColumns: function () {
        return {
            xtype: 'rowactions',
            hideable: false,
            resizeable: false,
            locked: true,
            width: 46,
            actions: [
                {
                    iconCls: 'icon-archive-item',
                    qtip: 'В архив',
                    callback: this.archiveItem
                }
            ],
            keepSelection: true
        };
    }
});