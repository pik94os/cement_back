Ext.define('Cement.view.products.services.View', {
	extend: 'Cement.view.products.services.Form',
	alias: 'widget.products_services_view',
	fieldsDisabled: true,
	buttons: null,
	border: 0,
	tbar: Cement.Config.modelViewButtons,
	layout: 'autocontainer',
	bbar: null,
	noUserFieldsetsControl: false,
	bodyCls: 'model-form',
	certMargins: '0 0 5 0',
	loadItemUrl: Cement.Config.url.products.products.loadItem,

	getBasicFieldSet: function () {
	    var me = this,
            items = me.callParent(arguments);

	    Ext.Array.remove(items, Ext.Array.findBy(items, function(item) { return item.xtype == 'downloadfield'; }));

	    return items;
	},

	getItems: function () {
	    return [
            {
            	xtype: 'cementimage'
            }, 
            {
				xtype: 'fieldset',
				border: 0,
				margin: 0,
				padding: '0',
				defaults: {
					xtype: 'textfield',
					anchor: '100%'
				},
				items: this.getBasicFieldSet()
            }, {
                xtype: 'fieldset',
                collapsible: true,
                collapsed: true,
                title: 'Происхождение',
                defaults: {
                    xtype: 'textfield',
                    anchor: '100%',
                    labelWidth: 110
                },
                items: this.getFromFields()
            }, {
                xtype: 'fieldset',
                collapsible: true,
                collapsed: true,
                title: 'Коды',
                defaults: {
                    xtype: 'textfield',
                    anchor: '100%',
                    labelWidth: 110
                },
                items: this.getCodeFieldSet()
            }, {
                xtype: 'fieldset',
                collapsible: true,
                collapsed: true,
                title: 'Технические условия',
                defaults: {
                    xtype: 'textfield',
                    anchor: '100%',
                    labelWidth: 110
                },
                items: this.getTechnicalFieldSet()
            }, {
                xtype: 'fieldset',
                collapsible: true,
                collapsed: true,
                title: 'Сертификаты',
                defaults: {
                    xtype: 'textfield',
                    anchor: '100%',
                    labelWidth: 110
                },
                items: this.getCertificatesFieldSet()
            }, {
                xtype: 'fieldset',
                collapsible: true,
                collapsed: true,
                title: 'Единица измерения',
                defaults: {
                    xtype: 'textfield',
                    anchor: '100%',
                    labelWidth: 110
                },
                items: [
                	this.getCombo('Единица измерения', 'p_unit', 'products.auxiliary.ServiceUnits')
                ]
            }
        ];
	},

	initComponent: function ()  {
		Ext.apply(this, {
			items: [{
				xtype: 'panel',
				border: 0,
				layout: 'anchor',
				defaultType: 'textfield',
				anchor: '100%',
				bodyPadding: '10 10 10 5',
				autoScroll: true,
				defaults: {
					anchor: '100%',
					labelWidth: 110
				},
				items: this.getItems()
			}]
		});
		this.callParent(arguments);
	}
});