Ext.define('Cement.view.products.services.lists.Archive', {
    extend: 'Cement.view.products.services.lists.Moderation',
    title: 'Архив',
    alias: 'widget.products_services_list_archive',
    gridStore: 'products.services.ServicesArchive',
    gridStateId: 'stateProductsServicesArchiveList',
    unArchiveItemUrl: Cement.Config.url.products.services.unArchiveItem,
    deleteUrl: Cement.Config.url.products.services.deleteItem,
    showCreateButton: false,

    getActionColumns: function () {
        return {
            xtype: 'rowactions',
            hideable: false,
            resizeable: false,
            locked: true,
            width: 46,
            actions: [
                {
                    iconCls: 'icon-unarchive-item',
                    qtip: 'Из архива',
                    callback: this.unArchiveItem
                },
                {
                    iconCls: 'icon-delete-item',
                    qtip: 'Удалить',
                    callback: this.deleteItem
                }
            ],
            keepSelection: true
        };
    },

    getGridColumns: function () {
        var columns = this.callParent(arguments);

        return this.mergeActions(columns);
    }
});