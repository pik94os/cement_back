Ext.define('Cement.view.products.services.lists.Moderation', {
    extend: 'Cement.view.basic.Grid',
    title: 'На модерации',
    alias: 'widget.products_services_list_moderation',
    gridStore: 'products.services.ServicesModeration',
    gridStateId: 'stateProductsServicesModerationList',
    printUrl: Cement.Config.url.products.services.printGrid,
    helpUrl: Cement.Config.url.products.services.help,
    deleteUrl: Cement.Config.url.products.services.deleteUrl,
    printItemUrl: Cement.Config.url.products.services.printItem,
    cls: 'simple-form',
    bbarText: 'Показаны услуги {0} - {1} из {2}',
    bbarEmptyMsg: 'Нет услуг',
    bbarUsersText: 'Услуг на странице: ',
    
    /*gridViewConfig: {
        getRowClass: function(record, index, rowParams, store) {
            return record.get('p_complect').length == 0 ? 'expander-hidden' : '';
        }
    },
    
    getGridPlugins: function () {
        return [{
            ptype: 'rowexpander',
            rowBodyTpl : new Ext.XTemplate(
                '<p>{p_text}</p>'
            )
        }];
    },*/

    getFilterItems: function () {
        return [
        {
            text: 'Наименование',
            kind: 'selector',
            field_name: 'p_name',
            checked: false
        }, {
            text: 'Группа',
            kind: 'selector',
            field_name: 'p_group',
            checked: false
        }, {
            text: 'Подгруппа',
            kind: 'selector',
            field_name: 'p_subgroup',
            checked: false
        }, {
            text: 'Торговая марка',
            kind: 'selector',
            field_name: 'p_trade_mark',
            checked: false
        }, {
            text: 'Производитель',
            kind: 'selector',
            field_name: 'p_manufacturer',
            checked: false
        }];
    },

    getGridColumns: function () {
        var result = [
            //{
            //    text: 'Комплект',
            //    dataIndex: 'p_have_complect',
            //    width: 20,
            //    resizable: false,
            //    renderer: this.checkBoxRenderer,
            //    align: 'center',
            //    tdCls: 'pseudoinput-column'
            //},
            { text: 'Наименование', dataIndex: 'p_name', width: 200, flex: 1 },
            { text: 'Группа', dataIndex: 'p_group_display', width: 200 },
            { text: 'Подгруппа', dataIndex: 'p_subgroup_display', width: 200 },
            { text: 'Торговая марка', dataIndex: 'p_trade_mark', width: 200 },
            { text: 'Производитель', dataIndex: 'p_manufacturer', width: 200 }
        ];
        return result;
    }
});