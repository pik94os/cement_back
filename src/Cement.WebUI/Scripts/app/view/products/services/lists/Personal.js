Ext.define('Cement.view.products.services.lists.Personal', {
    extend: 'Cement.view.products.services.lists.Moderation',
    title: 'Персональный каталог',
    alias: 'widget.products_services_list_personal',
    gridStore: 'products.services.ServicesPersonal',
    gridStateId: 'stateProductsServicesPersonalList',
    archiveItemUrl: Cement.Config.url.products.services.archiveItem,
    showCreateButton: false,

    getActionColumns: function () {
        return {
            xtype: 'rowactions',
            hideable: false,
            resizeable: false,
            locked: true,
            width: 46,
            actions: [
                {
                    iconCls: 'icon-archive-item',
                    qtip: 'В архив',
                    callback: this.archiveItem
                }
            ],
            keepSelection: true
        };
    },

    getGridColumns: function () {
        var columns = this.callParent(arguments);

        return this.mergeActions(columns);
    }
});