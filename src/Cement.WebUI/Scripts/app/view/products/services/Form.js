Ext.define('Cement.view.products.services.Form', {
    extend: 'Cement.view.basic.Form',
    alias: 'widget.products_services_form',
    fieldsDisabled: false,
    url: Cement.Config.url.products.services.saveUrl,
    method: Cement.Config.url.products.services.saveMethod,
    printUrl: Cement.Config.url.products.services.printItem,
    activeItem: 0,
    isFilter: false,
    layout: 'fit',
    certMargins: '0 0 5 202',

    getFromFields: function () {
        return [
			this.getCombo('Страна', 'p_found_country', 'products.auxiliary.Countries'),
			this.getCombo('Производитель', 'p_found_manufacturer', 'products.auxiliary.ServiceManufacturers')
        ];
    },

    categoryChange: function (combo, newValue) {
        if (newValue == '') return;
        var catTree = Ext.getStore('products.auxiliary.ServiceGroups'),
          subCatStore = combo.up('basicform').down('combo[name=p_subgroup]').getStore(),
          selected = catTree.getRootNode().findChild('id', newValue);
        combo.up('basicform').down('combo[name=p_subgroup]').clearValue();
        subCatStore.removeAll();
        if (selected) {
            selected.eachChild(function (child) {
                subCatStore.add({
                    id: child.get('id'),
                    p_name: child.get('text')
                });
            });
        }
    },

    getBasicFieldSet: function () {
        var catTree = Ext.getStore('products.auxiliary.ServiceGroups'),
          catStore = Ext.create('Ext.data.Store', {
              fields: ['id', 'p_name', 'p_show_char']
          }),
          subCatStore = Ext.create('Ext.data.Store', {
              fields: ['id', 'p_name']
          });
        catTree.getRootNode().eachChild(function (child) {
            catStore.add({
                id: child.get('id'),
                p_name: child.get('text'),
                p_show_char: child.get('p_show_char')
            });
        });
        return [
            {
                xtype: 'hiddenfield',
                name: 'id'
            },
            {
                xtype: 'textfield',
                fieldLabel: 'Наименование',
                anchor: '100%',
                allowBlank: this.allowBlankFields,
                disabled: this.fieldsDisabled,
                name: 'p_name'
            },
            this.getCombo('Группа', 'p_group', catStore, this.categoryChange),
            this.getCombo('Подгруппа', 'p_subgroup', subCatStore, null, false, null, true),
            {
                xtype: 'textfield',
                name: 'p_trade_mark',
                disabled: this.fieldsDisabled,
                allowBlank: this.allowBlankFields,
                fieldLabel: 'Торговая марка'
            },
			this.getFileField('Фото', 'p_img')
        ];
    },

    getCodeFieldSet: function () {
        return [{
            name: 'p_code_okun',
            disabled: this.fieldsDisabled,
            allowBlank: this.allowBlankFields,
            fieldLabel: 'ОКУН'
        }, {
            name: 'p_code_partnumber',
            disabled: this.fieldsDisabled,
            allowBlank: this.allowBlankFields,
            fieldLabel: 'Артикул'
        }];
    },

    getTechnicalFieldSet: function () {
        return [{
            name: 'p_tu_gost',
            disabled: this.fieldsDisabled,
            allowBlank: this.allowBlankFields,
            fieldLabel: 'ГОСТ'
        }, {
            name: 'p_tu_gost_r',
            disabled: this.fieldsDisabled,
            allowBlank: this.allowBlankFields,
            fieldLabel: 'ГОСТ Р'
        }, {
            name: 'p_tu_ost',
            disabled: this.fieldsDisabled,
            allowBlank: this.allowBlankFields,
            fieldLabel: 'ОСТ'
        }, {
            name: 'p_tu_stp',
            disabled: this.fieldsDisabled,
            allowBlank: this.allowBlankFields,
            fieldLabel: 'СТП'
        }, {
            name: 'p_tu_tu',
            disabled: this.fieldsDisabled,
            allowBlank: this.allowBlankFields,
            fieldLabel: 'ТУ'
        }];
    },

    getCertificatesFieldSet: function () {
        return [{
                xtype: 'container',
                border: 0,
                cls: 'starred',
                margin: this.certMargins,
                html: '<b>Сертификат соответствия</b>'
            }, {
                name: 'p_sert_soot_number',
                disabled: this.fieldsDisabled,
                allowBlank: this.allowBlankFields,
                fieldLabel: 'Номер'
            }, {
                xtype: 'hidden'
            },
            {
                xtype: 'datefield',
                fieldLabel: 'Дата начала',
                name: 'p_sert_soot_start'
            },
            {
                xtype: 'datefield',
                fieldLabel: 'Дата окончания',
                name: 'p_sert_soot_end'
            },
			this.getFileField('Скан', 'p_sert_soot_img1', '0 66 5 0'),
			this.getFileField('Скан (стр. 2)', 'p_sert_soot_img2', '0 66 5 0'), {
			    xtype: 'container',
			    border: 0,
			    cls: 'starred',
			    margin: this.certMargins,
			    html: '<b>Декларация соответствия</b>'
			}, {
			    name: 'p_sert_deklar_number',
			    disabled: this.fieldsDisabled,
			    allowBlank: this.allowBlankFields,
			    fieldLabel: 'Номер'
			},
            {
                xtype: 'datefield',
                fieldLabel: 'Дата начала',
                name: 'p_sert_deklar_start'
            },
            {
                xtype: 'datefield',
                fieldLabel: 'Дата окончания',
                name: 'p_sert_deklar_end'
            },
			this.getFileField('Скан', 'p_sert_deklar_img', '0 66 5 0'), {
			    xtype: 'container',
			    border: 0,
			    cls: 'starred',
			    margin: this.certMargins,
			    html: '<b>Санитарно-эпидемиологическое заключение</b>'
			}, {
			    name: 'p_sert_san_number',
			    disabled: this.fieldsDisabled,
			    allowBlank: this.allowBlankFields,
			    fieldLabel: 'Номер'
			},
            {
                xtype: 'datefield',
                fieldLabel: 'Дата начала',
                name: 'p_sert_san_start'
            },
            {
                xtype: 'datefield',
                fieldLabel: 'Дата окончания',
                name: 'p_sert_san_end'
            },
			this.getFileField('Скан', 'p_sert_san_img', '0 66 5 0'),
			{
			    xtype: 'container',
			    border: 0,
			    cls: 'starred',
			    margin: this.certMargins,
			    html: '<b>Сертификат пожарной безопасности</b>'
			}, {
			    name: 'p_sert_fire_number',
			    disabled: this.fieldsDisabled,
			    allowBlank: this.allowBlankFields,
			    fieldLabel: 'Номер'
			},
            {
                xtype: 'datefield',
                fieldLabel: 'Дата начала',
                name: 'p_sert_fire_start'
            },
            {
                xtype: 'datefield',
                fieldLabel: 'Дата окончания',
                name: 'p_sert_fire_end'
            },
			this.getFileField('Скан', 'p_sert_fire_img', '0 66 5 0'), {
			    xtype: 'container',
			    border: 0,
			    cls: 'starred',
			    margin: this.certMargins,
			    html: '<b>Декларация о соответствии требованиям пожарной безопасности</b>'
			}, {
			    name: 'p_sert_fire_deklar',
			    disabled: this.fieldsDisabled,
			    allowBlank: this.allowBlankFields,
			    fieldLabel: 'Номер'
			},
            {
                xtype: 'datefield',
                fieldLabel: 'Дата начала',
                name: 'p_sert_fire_deklar_start'
            },
            {
                xtype: 'datefield',
                fieldLabel: 'Дата окончания',
                name: 'p_sert_fire_deklar_end'
            },
			this.getFileField('Скан', 'p_sert_fire_deklar_img', '0 66 5 0')
        ];
    },

    getItems: function () {
        return [{
            xtype: 'fieldset',
            border: 0,
            margin: 0,
            padding: '0 110',
            defaults: {
                xtype: 'textfield',
                anchor: '100%',
                labelAlign: 'right'
            },
            items: this.getBasicFieldSet()
        }, {
            xtype: 'fieldset',
            collapsible: true,
            collapsed: true,
            title: 'Происхождение',
            defaults: {
                xtype: 'textfield',
                anchor: '100%',
                labelWidth: 200,
                labelAlign: 'right'
            },
            items: this.getFromFields()
        }, {
            xtype: 'fieldset',
            collapsible: true,
            collapsed: true,
            title: 'Коды',
            defaults: {
                xtype: 'textfield',
                anchor: '100%',
                labelWidth: 200,
                labelAlign: 'right'
            },
            items: this.getCodeFieldSet()
        }, {
            xtype: 'fieldset',
            collapsible: true,
            collapsed: true,
            title: 'Технические условия',
            defaults: {
                xtype: 'textfield',
                anchor: '100%',
                labelWidth: 200,
                labelAlign: 'right'
            },
            items: this.getTechnicalFieldSet()
        }, {
            xtype: 'fieldset',
            collapsible: true,
            collapsed: true,
            title: 'Сертификаты',
            defaults: {
                xtype: 'textfield',
                anchor: '100%',
                labelWidth: 200,
                labelAlign: 'right'
            },
            items: this.getCertificatesFieldSet()
        }, {
            xtype: 'fieldset',
            collapsible: true,
            collapsed: true,
            title: 'Единица измерения',
            defaults: {
                xtype: 'textfield',
                anchor: '100%',
                labelWidth: 200,
                labelAlign: 'right'
            },
            items: [
                this.getCombo('Единица измерения', 'p_unit', 'products.auxiliary.ServiceUnits')
            ]
        }, {
            xtype: 'fieldset',
            collapsible: true,
            collapsed: true,
            title: 'Комплект',
            defaults: {
                xtype: 'textfield',
                anchor: '100%',
                labelWidth: 200,
                labelAlign: 'right'
            },
            items: [{
                xtype: 'fieldcontainer',
                layout: 'hbox',
                items: [{
                    xtype: 'hiddenfield',
                    name: 'p_complect'
                }, {
                    xtype: 'textfield',
                    labelWidth: 200,
                    labelAlign: 'right',
                    name: 'p_complect_display',
                    fieldLabel: 'Товары, услуги',
                    flex: 1
                }, {
                    xtype: 'button',
                    text: 'Выбрать',
                    margin: '0 0 0 4',
                    action: 'show_complect_window',
                    width: 60
                }]
            }]
        }
        ];
    },

    initComponent: function () {
        this.callParent(arguments);
        if (!this.fieldsDisabled) {
            this.down('button[action=show_complect_window]').on('click', function () {
                this.createComplectWindow();
                this.complectWindow.show();
            }, this);
        }
    },

    createComplectWindow: function () {
        if (!this.complectWindow) {
            this.complectWindow = Ext.create('Cement.view.products.ComplectWindow');
            this.complectWindow.on('complectselected', function (json, str) {
                this.down('textfield[name=p_complect_display]').setValue(str);
                this.down('hiddenfield[name=p_complect]').setValue(Ext.JSON.encode(json));
                this.complectWindow.hide();
            }, this);
        }
    },

    afterRecordLoad: function (rec) {
        if (!this.fieldsDisabled) {
            this.createComplectWindow();
            this.complectWindow.loadData(rec.get('p_complect'));
        }
    }
});