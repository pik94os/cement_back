Ext.define('Cement.view.products.services.Total', {
	extend: 'Cement.view.basic.Tabs',
	title: 'Услуги',

	items:
    [
        {
            xtype: 'products_services_complex_moderation',
		    title: 'На модерацию'
	    },
        {
            xtype: 'products_services_complex_general',
		    title: 'Общий каталог'
	    },
        {
            xtype: 'products_services_complex_personal',
		    title: 'Персональный каталог'
	    },
        {
            xtype: 'products_services_complex_archive',
		    title: 'Архив'
        }
	],

	initComponent: function () {
		Ext.apply(this, {
		    title: 'Услуги'
		});
		this.callParent(arguments);
	}
});