Ext.define('Cement.view.products.services.complex.Personal', {
	extend: 'Cement.view.basic.Complex',
	alias: 'widget.products_services_complex_personal',
    border: 0,
    enableDetail: false,
    autoLoadStore: false,
    priceId: null,
    bodyPadding: 0,
    padding: 0,
    header: false,
    rowsCount: 2,
    title: 'Персональный каталог',
    tabTitle: 'Персональный каталог',
    cls: 'no-side-borders',
    topXType: 'products_services_list_personal',
    topDetailType: 'Cement.view.products.services.View',
    bottomTitle: 'Справочник',
    bottomTabs: [
        {
            title: 'Товары',
            xtype: 'products_products_bottom_products',
            detailType: 'Cement.view.products.products.View',
            constantFilterParam: 'p_product',
            childrenConstantFilterParam: 'p_product',
            border: 0
        },
        {
            title: 'Услуги',
            xtype: 'products_products_bottom_services',
            detailType: 'Cement.view.products.products.View',
            constantFilterParam: 'p_product',
            childrenConstantFilterParam: 'p_product',
            border: 0
        }
    ]
});