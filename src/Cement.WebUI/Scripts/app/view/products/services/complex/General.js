Ext.define('Cement.view.products.services.complex.General', {
	extend: 'Cement.view.basic.Complex',
	alias: 'widget.products_services_complex_general',
    border: 0,
    enableDetail: false,
    autoLoadStore: false,
    priceId: null,
    bodyPadding: 0,
    padding: 0,
    header: false,
    title: null,
    rowsCount: 2,
    tabTitle: 'Общий каталог',
    cls: 'no-side-borders',
    topXType: 'products_services_list_general',
    topDetailType: 'Cement.view.products.services.View',
    bottomTitle: 'Справочник',
    bottomTabs: [
        {
            title: 'Товары',
            xtype: 'products_products_bottom_products',
            detailType: 'Cement.view.products.products.View',
            constantFilterParam: 'p_product',
            childrenConstantFilterParam: 'p_product',
            border: 0
        },
        {
            title: 'Услуги',
            xtype: 'products_products_bottom_services',
            detailType: 'Cement.view.products.products.View',
            constantFilterParam: 'p_product',
            childrenConstantFilterParam: 'p_product',
            border: 0
        }
    ]
});