Ext.define('Cement.view.products.price.CurrentComplex', {
	extend: 'Cement.view.basic.Complex',
	alias: 'widget.products_price_current_complex',
    border: 0,
    enableDetail: false,
    autoLoadStore: false,
    priceId: null,
    bodyPadding: 0,
    padding: 0,
    header: false,
    title: 'Действующие',
    tabTitle: 'Действующие',
    topXType: 'products_price_list_current',
    topDetailType: 'Cement.view.products.price.View',
    rowsCount: 2,
    bottomTitle: 'Справочник',
    bottomTabs: [{
        title: 'Товары',
        xtype: 'products_price_bottom_current_products',
        constantFilterParam: 'p_price_current',
        detailType: 'Cement.view.products.products.View'
    }, {
        title: 'Услуги',
        xtype: 'products_price_bottom_current_services',
        constantFilterParam: 'p_price_current',
        detailType: 'Cement.view.products.services.View'
    }, {
    	title: 'Маршрут',
    	xtype: 'products_price_bottom_current_routes',
        constantFilterParam: 'p_price_current',
    	detailType: 'Cement.view.products.price.RouteView'
    }],

    initComponent: function () {
    	Ext.apply(this, {
    		title: 'Действующие'
    	});
    	this.callParent(arguments);
    }
});