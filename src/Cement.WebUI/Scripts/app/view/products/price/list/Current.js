Ext.define('Cement.view.products.price.list.Current', {
    extend: 'Cement.view.products.price.list.New',
    alias: 'widget.products_price_list_current',
    gridStore: 'products.price.Current',
    gridStateId: 'stateProductsPriceListCurrent',
    printUrl: Cement.Config.url.products.price.current.printGrid,
    helpUrl: Cement.Config.url.products.price.current.help,
    deleteUrl: Cement.Config.url.products.price.current.deleteUrl,
    printItemUrl: Cement.Config.url.products.price.current.printItem,
    archiveItemUrl: Cement.Config.url.products.price.current.archiveItem,

    getActionColumns: function () {
        return {
            xtype: 'rowactions',
            hideable: false,
            resizeable: false,
            locked: true,
            width: 19,
            actions: [
                {
                    iconCls: 'icon-archive-item',
                    qtip: 'Отправить в архив',
                    callback: this.archiveItem
                }
            ],
            keepSelection: true
        };
    },
    getFilterItems: function () {
        return [{
            text: 'Наименование',
            kind: 'selector',
            field_name: 'p_name',
            checked: true
        }, {
            text: 'Действует с',
            kind: 'selector',
            field_name: 'p_date_start',
            checked: true
        }, {
            text: 'Действует по',
            kind: 'selector',
            field_name: 'p_date_end',
            checked: true
        }, {
            text: 'Взамен наименование',
            kind: 'selector',
            field_name: 'p_replace_name',
            checked: true
        }, {
            text: 'Взамен с',
            kind: 'selector',
            field_name: 'p_replace_date_start',
            checked: true
        }, {
            text: 'Взамен по',
            kind: 'selector',
            field_name: 'p_replace_date_end',
            checked: true
        }, {
            text: 'Примечание',
            kind: 'selector',
            field_name: 'p_comment',
            checked: true
        }, {
            text: 'Статус',
            kind: 'selector',
            field_name: 'p_status',
            checked: true
        }];
    },

    getGridColumns: function () {
        var result = [
            { text: 'Наименование', dataIndex: 'p_name', locked: true, width: 350 },
            {
                text: 'Действует',
                columns: [
                    { text: 'с', dataIndex: 'p_date_start', width: 70 },
                    { text: 'по', dataIndex: 'p_date_end', width: 70 }
                ]
            },
            {
                text: 'Взамен',
                columns: [
                    { text: 'Наименование', dataIndex: 'p_replace_name', width: 250 },
                    { text: 'с', dataIndex: 'p_replace_date_start', width: 70 },
                    { text: 'по', dataIndex: 'p_replace_date_end', width: 70 }
                ]
            },
            { text: 'Примечание', dataIndex: 'p_comment', width: 300 },
            { text: 'Статус', dataIndex: 'p_status', width: 300 }
        ];

        return this.mergeActions(result);
    }
});