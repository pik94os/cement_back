Ext.define('Cement.view.products.price.list.New', {
    extend: 'Cement.view.basic.Grid',
    alias: 'widget.products_price_list_new',
    gridStore: 'products.price.New',
    gridStateId: 'stateProductsPriceListNew',
    printUrl: Cement.Config.url.products.price.new_price.printGrid,
    helpUrl: Cement.Config.url.products.price.new_price.help,
    deleteUrl: Cement.Config.url.products.price.new_price.deleteUrl,
    printItemUrl: Cement.Config.url.products.price.new_price.printItem,

    getActionColumns: function () {
        return {
            xtype: 'rowactions',
            hideable: false,
            resizeable: false,
            locked: true,
            width: 46,
            actions: [
                {
                    iconCls: 'icon-edit-item',
                    qtip: 'Редактировать',
                    callback: this.editItem
                },
                {
                    iconCls: 'icon-delete-item',
                    qtip: 'Удалить',
                    callback: this.deleteItem
                }
            ],
            keepSelection: true
        };
    },

    getFilterItems: function () {
        return [{
            text: 'Наименование',
            kind: 'selector',
            field_name: 'p_name',
            checked: true
        }, {
            text: 'Действует с',
            kind: 'selector',
            field_name: 'p_date_start',
            checked: true
        }, {
            text: 'Действует по',
            kind: 'selector',
            field_name: 'p_date_end',
            checked: true
        }, {
            text: 'Взамен наименование',
            kind: 'selector',
            field_name: 'p_replace_name',
            checked: true
        }, {
            text: 'Взамен с',
            kind: 'selector',
            field_name: 'p_replace_date_start',
            checked: true
        }, {
            text: 'Взамен по',
            kind: 'selector',
            field_name: 'p_replace_date_end',
            checked: true
        }, {
            text: 'Примечание',
            kind: 'selector',
            field_name: 'p_comment',
            checked: true
        }];
    },

    getGridColumns: function () {
        var result = [
            { text: 'Наименование', dataIndex: 'p_name', locked: true, width: 350 },
            {
                text: 'Действует',
                columns: [
                    { text: 'с', dataIndex: 'p_date_start', width: 70 },
                    { text: 'по', dataIndex: 'p_date_end', width: 70 }
                ]
            },
            {
                text: 'Взамен',
                columns: [
                    { text: 'Наименование', dataIndex: 'p_replace_name', width: 250 },
                    { text: 'с', dataIndex: 'p_replace_date_start', width: 70 },
                    { text: 'по', dataIndex: 'p_replace_date_end', width: 70 }
                ]
            },
            { text: 'Примечание', dataIndex: 'p_comment', width: 300 }
        ];

        return this.mergeActions(result);
    }
});