Ext.define('Cement.view.products.price.list.Archive', {
    extend: 'Cement.view.products.price.list.New',
    alias: 'widget.products_price_list_archive',
    gridStore: 'products.price.Archive',
    gridStateId: 'stateProductsPriceListArchive',
    printUrl: Cement.Config.url.products.price.archive.printGrid,
    helpUrl: Cement.Config.url.products.price.archive.help,
    deleteUrl: Cement.Config.url.products.price.archive.deleteUrl,
    printItemUrl: Cement.Config.url.products.price.archive.printItem,
    unArchiveItemUrl: Cement.Config.url.products.price.archive.unArchiveItem,

    getActionColumns: function () {
        return {
            xtype: 'rowactions',
            hideable: false,
            resizeable: false,
            locked: true,
            width: 19,
            actions: [
                {
                    iconCls: 'icon-unarchive-item',
                    qtip: 'Удалить из архива',
                    callback: this.unArchiveItem
                },
                {
                    iconCls: 'icon-delete-item',
                    qtip: 'Удалить',
                    callback: this.deleteItem
                }
            ],
            keepSelection: true
        };
    },

    getFilterItems: function () {
        return [{
            text: 'Наименование',
            kind: 'selector',
            field_name: 'p_name',
            checked: true
        }, {
            text: 'Действует с',
            kind: 'selector',
            field_name: 'p_date_start',
            checked: true
        }, {
            text: 'Действует по',
            kind: 'selector',
            field_name: 'p_date_end',
            checked: true
        }, {
            text: 'Примечание',
            kind: 'selector',
            field_name: 'p_comment',
            checked: true
        }];
    },

    getGridColumns: function () {
        var result = [
            { text: 'Наименование', dataIndex: 'p_name', locked: true, width: 740 },
            {
                text: 'Действует',
                columns: [
                    { text: 'с', dataIndex: 'p_date_start', width: 70 },
                    { text: 'по', dataIndex: 'p_date_end', width: 70 }
                ]
            },
            {
                text: 'Примечание',
                dataIndex: 'p_comment',
                minWidth: 300
            }
        ];

        return this.mergeActions(result);
    }
});