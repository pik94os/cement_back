Ext.define('Cement.view.products.price.Form', {
	extend: 'Cement.view.basic.Form',
	alias: 'widget.products_price_form',
	fieldsDisabled: false,
	url: Cement.Config.url.products.price.new_price.saveUrl,
	method: Cement.Config.url.products.price.new_price.saveMethod,
	printUrl: Cement.Config.url.products.price.new_price.printItem,
	isFilter: false,
	layout: 'fit',
	importancesStoreId: 'office_work.auxiliary.Importances',
	securitiesStoreId: 'office_work.auxiliary.Securities',

	getItems: function () {
	    return [
            {
				xtype: 'fieldset',
				border: 0,
				margin: 0,
				defaults:
                {
					xtype: 'textfield',
					anchor: '100%',
					labelWidth: 200,
					labelAlign: 'right'
				},
				items:
                [
                    {
                        name: 'p_name',
                        fieldLabel: 'Наименование'
                    },
                    {
                        name: 'p_date_start',
                        fieldLabel: 'Действует с',
                        xtype: 'datefield'
                    }
                ]
		    },
            {
                xtype: 'fieldset',
                border: 0,
                defaults:
                {
                    xtype: 'textfield',
                    anchor: '100%',
                    labelWidth: 200,
                    labelAlign: 'right'
                },
                items:
                [
                    {
					    xtype: 'fieldcontainer',
					    layout: 'hbox',
					    items:
                        [
                            {
						        xtype: 'hiddenfield',
						        name: 'p_replace'
					        },
                            {
						        xtype: 'textfield',
						        labelWidth: 200,
						        labelAlign: 'right',
						        name: 'p_replace_display',
						        fieldLabel: 'Замена прайс-листа',
						        flex: 1
                            },
                            {
						        xtype: 'button',
						        text: 'Выбрать',
						        margin: '0 0 0 4',
						        action: 'show_replace_window',
						        width: 60
                            }
                        ]
                    },
                    {
					    xtype: 'fieldcontainer',
					    layout: 'hbox',
					    items:[{
						    xtype: 'hiddenfield',
						    name: 'p_products'
					    }, {
						    xtype: 'textfield',
						    labelWidth: 200,
						    labelAlign: 'right',
						    name: 'p_products_display',
						    fieldLabel: 'Товар',
						    flex: 1
					    }, {
						    xtype: 'button',
						    text: 'Выбрать',
						    margin: '0 0 0 4',
						    action: 'show_products_window',
						    width: 60
					    }]
                    },
                    {
					    xtype: 'textarea',
					    fieldLabel: 'Примечание',
					    name: 'p_comment',
					    labelAlign: 'right',
					    height: 150
				    },
                    this.getSelectorField('Автор', 'p_author', 'show_author_window'),
                    this.getSelectorField('На подпись', 'p_sign_for', 'show_sign_for_window'),
                    this.getSelectorField('Связанные документы', 'p_documents', 'show_documents_window'),
                    {
                        xtype: 'datefield',
                        name: 'p_deadline',
                        fieldLabel: 'Срок рассмотрения'
                    },
                    this.getCombo('Важность', 'p_importance', this.importancesStoreId),
                    this.getCombo('Секретность', 'p_security', this.securitiesStoreId),
                    this.getFileField('Импорт', 'p_import', 0)
                ]
            }
        ];
	},

	initComponent: function () {
		this.callParent(arguments);
		if (!this.fieldsDisabled) {
			this.down('button[action=show_replace_window]').on('click', function () {
				this.createReplaceWindow();
				this.replaceWindow.show();
			}, this);
			this.down('button[action=show_products_window]').on('click', function () {
				this.createProductsWindow();
				this.productsWindow.show();
			}, this);
			this.down('button[action=show_author_window]').on('click', function () {
			    this.createAuthorWindow();
			    this.authorWindow.show();
			}, this);
			this.down('button[action=show_sign_for_window]').on('click', function () {
			    this.createSignForWindow();
			    this.signForWindow.show();
			}, this);

			this.down('button[action=show_documents_window]').on('click', function () {
			    this.createDocsWindow();
			    this.docsWindow.show();
			}, this);
		}
	},

	createReplaceWindow: function () {
		if (!this.replaceWindow) {
			this.replaceWindow = Ext.create('Cement.view.products.price.PricelistsWindow');
			this.replaceWindow.on('priceselected', function (rec) {
				this.down('textfield[name=p_replace_display]').setValue(rec.get('p_name'));
				this.down('hiddenfield[name=p_replace]').setValue(rec.get('id'));
				this.replaceWindow.hide();
			}, this);
		}
	},

	createProductsWindow: function () {
		if (!this.productsWindow) {
			this.productsWindow = Ext.create('Cement.view.products.price.ProductsWindow');
			this.productsWindow.on('priceselected', function (store) {
			    var products = [],
			        names = [];
				store.each(function (item) {
				    var newProduct = {
				        Id: item.get('id'),
				        MeasureUnitId: item.get('p_unit'),
				        Price: item.get('p_price'),
				        TaxId: item.get('p_tax')
				    };

				    products.push(newProduct);
				    names.push(item.get('p_name'));
				});

				this.down('textfield[name=p_products_display]').setValue(names.join(', '));
				this.down('hiddenfield[name=p_products]').setValue(Ext.JSON.encode(products));
				this.productsWindow.hide();
			}, this);
		}
	},

	createAuthorWindow: function () {
	    if (!this.authorWindow) {
	        this.authorWindow = Ext.create('Cement.view.common.EmployeeSelectWindow');
	        this.authorWindow.on('signitem', function (id, name) {
	            this.down('textfield[name=p_author_display]').setValue(name);
	            this.down('hiddenfield[name=p_author]').setValue(id);
	            this.authorWindow.hide();
	        }, this);
	    }
	},

	createDocsWindow: function () {
	    if (!this.docsWindow) {
	        this.docsWindow = Ext.create('Cement.view.common.ConnectedDocumentsWindow');
	        this.docsWindow.on('documentsselected', function (ids, names) {
	            this.down('textfield[name=p_documents_display]').setValue(names.join(', '));
	            this.down('hiddenfield[name=p_documents]').setValue(Ext.JSON.encode(ids));
	            this.docsWindow.hide();
	        }, this);
	    }
	},

	createSignForWindow: function () {
	    if (!this.signForWindow) {
	        this.signForWindow = Ext.create('Cement.view.common.EmployeeSelectWindow');
	        this.signForWindow.on('signitem', function (id, name) {
	            this.down('textfield[name=p_sign_for_display]').setValue(name);
	            this.down('hiddenfield[name=p_sign_for]').setValue(id);
	            this.signForWindow.hide();
	        }, this);
	    }
	},

	afterRecordLoad: function (rec) {
	    if (rec.get('id') > 0) {
	        this.down('button[action=show_documents_window]').disable();
	        this.down('button[action=show_sign_for_window]').disable();
	        this.down('button[action=show_author_window]').disable();
	        this.down('filefield').disable();
	        this.down('textfield[name=p_author_display]').disable();
	        this.down('textfield[name=p_sign_for_display]').disable();
	        this.down('textfield[name=p_documents_display]').disable();
	        this.down('datefield[name=p_deadline]').disable();
	        this.down('combo[name=p_importance]').disable();
	        this.down('combo[name=p_security]').disable();
	    }

		if (!this.fieldsDisabled) {
			this.createReplaceWindow();
			this.createProductsWindow();
			this.replaceWindow.loadData([rec.get('p_replace')]);
			this.productsWindow.loadData(Ext.JSON.decode(rec.get('p_products')));
		}
	}
});