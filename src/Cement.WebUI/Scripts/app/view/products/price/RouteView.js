Ext.define('Cement.view.products.price.RouteView', {
	extend: 'Cement.view.basic.Form',
	alias: 'widget.products_price_route_view',
	fieldsDisabled: true,
	buttons: null,
	border: 0,
	tbar: Cement.Config.modelViewButtons,
	printUrl: Cement.Config.url.products.price.routes.printItem,
	layout: 'autocontainer',
	bbar: null,
	noUserFieldsetsControl: false,
	bodyCls: 'model-form',

	initComponent: function ()  {
		Ext.apply(this, {
			items: [{
				xtype: 'panel',
				border: 0,
				layout: 'anchor',
				defaultType: 'textfield',
				anchor: '100%',
				bodyPadding: '10 10 10 5',
				autoScroll: true,
				defaults: {
					anchor: '100%'
				},
				items: [{
					xtype: 'textarea',
					name: 'p_comment',
					disabled: true,
					fieldLabel: 'Комментарии',
					height: 500,
					labelAlign: 'top'
				}]
			}]
		});
		this.callParent(arguments);
	}
});