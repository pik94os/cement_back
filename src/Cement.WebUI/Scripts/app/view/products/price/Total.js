Ext.define('Cement.view.products.price.Total', {
	extend: 'Cement.view.basic.Tabs',
	title: 'Прайс-лист',

	items: [{
		xtype: 'products_price_new_complex',
		title: 'Новые'
	}, {
		xtype: 'products_price_current_complex',
		title: 'Действующие'
	}, {
		xtype: 'products_price_archive_complex',
		title: 'Архив'
	}],

	initComponent: function () {
		Ext.apply(this, {
			title: 'Прайс-лист'
		});
		this.callParent(arguments);
	}
});