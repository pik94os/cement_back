Ext.define('Cement.view.products.price.bottom.CurrentRoutes', {
    extend: 'Cement.view.products.price.bottom.NewRoutes',
    alias: 'widget.products_price_bottom_current_routes',
    gridStore: 'products.price.CurrentRoutes',
    gridStateId: 'stateProductsPriceCurrentRoutes'
});