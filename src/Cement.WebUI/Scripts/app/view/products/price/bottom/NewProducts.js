Ext.define('Cement.view.products.price.bottom.NewProducts', {
    extend: 'Cement.view.basic.Grid',
    title: 'Товары',
    alias: 'widget.products_price_bottom_new_products',
    gridStore: 'products.price.NewProducts',
    gridStateId: 'stateProductsPriceNewProducts',
    printUrl: Cement.Config.url.products.products.printGrid,
    helpUrl: Cement.Config.url.products.products.help,
    deleteUrl: Cement.Config.url.products.products.deleteUrl,
    printItemUrl: Cement.Config.url.products.products.printItem,
    bbarText: 'Показаны товары {0} - {1} из {2}',
    bbarEmptyMsg: 'Нет товаров',
    bbarUsersText: 'Товаров на странице: ',
    autoLoadStore: false,
    tbar: null,
    showBottomBar: false,

    getToolbarItems: function () { return null; },

    getGridFeatures: function () { return null },

    getGridColumns: function () {
        return [
            { text: 'Наименование', dataIndex: 'p_name', width: 150, locked: true },
            { text: 'Группа', dataIndex: 'p_group', width: 150 },
            { text: 'Подгруппа', dataIndex: 'p_subgroup', width: 150 },
            { text: 'Торговая марка', dataIndex: 'p_trade_mark', width: 150 },
            { text: 'Производитель', dataIndex: 'p_manufacturer', width: 150 },
            { text: 'Ед. изм.', dataIndex: 'p_unit_display', width: 70 },
            { text: 'Налог', dataIndex: 'p_tax', width: 70 },
            { text: 'Цена', dataIndex: 'p_price', width: 70 }
        ];
    },

    initComponent: function () {
        Ext.apply(this, {
            title: 'Товары'
        });
        this.callParent(arguments);
    }
});