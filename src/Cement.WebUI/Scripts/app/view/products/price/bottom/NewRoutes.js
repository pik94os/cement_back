Ext.define('Cement.view.products.price.bottom.NewRoutes', {
    extend: 'Cement.view.basic.Grid',
    title: 'Маршруты',
    alias: 'widget.products_price_bottom_new_routes',
    gridStore: 'products.price.NewRoutes',
    gridStateId: 'stateProductsPriceNewRoutes',
    printUrl: Cement.Config.url.products.price.routes.printGrid,
    helpUrl: Cement.Config.url.products.price.routes.help,
    deleteUrl: Cement.Config.url.products.price.routes.deleteUrl,
    printItemUrl: Cement.Config.url.products.price.routes.printItem,
    bbarText: 'Показаны маршруты {0} - {1} из {2}',
    bbarEmptyMsg: 'Нет маршрутов',
    bbarUsersText: 'Маршрутов на странице: ',
    autoLoadStore: false,
    tbar: null,

    getToolbarItems: function () { return null; },
    getGridFeatures: function () { return null; },
    showBottomBar: false,

    getGridColumns: function () {
        return [
            { text: 'ФИО', dataIndex: 'p_name', width: 150, locked: true, sortable: false },
            { text: 'Должность', dataIndex: 'p_position', width: 150, locked: true, sortable: false },
            { text: 'Отдел', dataIndex: 'p_department', width: 150, sortable: false },
            { text: 'Статус', dataIndex: 'p_status', width: 150, sortable: false },
            { text: 'Дата поступления', dataIndex: 'p_date_in', width: 150, sortable: false },
            { text: 'Дата отправки', dataIndex: 'p_date_out', width: 150, sortable: false },
            { text: 'Время нахождения', dataIndex: 'p_time_spent', width: 150, sortable: false }
        ];
    },

    initComponent: function () {
        Ext.apply(this, {
            title: 'Маршруты'
        });
        this.callParent(arguments);
    }
});