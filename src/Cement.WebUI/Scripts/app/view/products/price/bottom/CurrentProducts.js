Ext.define('Cement.view.products.price.bottom.CurrentProducts', {
    extend: 'Cement.view.products.price.bottom.NewProducts',
    alias: 'widget.products_price_bottom_current_products',
    gridStore: 'products.price.CurrentProducts',
    gridStateId: 'stateProductsPriceCurrentProducts'
});