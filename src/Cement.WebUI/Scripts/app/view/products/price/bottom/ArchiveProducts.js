Ext.define('Cement.view.products.price.bottom.ArchiveProducts', {
    extend: 'Cement.view.products.price.bottom.NewProducts',
    alias: 'widget.products_price_bottom_archive_products',
    gridStore: 'products.price.ArchiveProducts',
    gridStateId: 'stateProductsPriceArchiveProducts'
});