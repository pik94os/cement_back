Ext.define('Cement.view.products.price.bottom.NewServices', {
    extend: 'Cement.view.basic.Grid',
    title: 'Услуги',
    alias: 'widget.products_price_bottom_new_services',
    gridStore: 'products.price.NewServices',
    gridStateId: 'stateProductsPriceNewServices',
    printUrl: Cement.Config.url.products.services.printGrid,
    helpUrl: Cement.Config.url.products.services.help,
    deleteUrl: Cement.Config.url.products.services.deleteUrl,
    printItemUrl: Cement.Config.url.products.services.printItem,
    bbarText: 'Показаны услуги {0} - {1} из {2}',
    bbarEmptyMsg: 'Нет услуг',
    bbarUsersText: 'Услуг на странице: ',
    autoLoadStore: false,
    tbar: null,

    getToolbarItems: function () { return null; },
    getGridFeatures: function () { return null },
    showBottomBar: false,

    getGridColumns: function () {
        return [
            { text: 'Наименование', dataIndex: 'p_name', width: 150, locked: true },
            { text: 'Группа', dataIndex: 'p_group', width: 150 },
            { text: 'Подгруппа', dataIndex: 'p_subgroup', width: 150 },
            { text: 'Торговая марка', dataIndex: 'p_trade_mark', width: 150 },
            { text: 'Производитель', dataIndex: 'p_manufacturer', width: 150 },
            { text: 'Ед. изм.', dataIndex: 'p_unit_display', width: 70 },
            { text: 'Налог', dataIndex: 'p_tax', width: 70 },
            { text: 'Цена', dataIndex: 'p_price', width: 70 }
        ];
    },

    initComponent: function () {
        Ext.apply(this, {
            title: 'Услуги'
        });
        this.callParent(arguments);
    }
});