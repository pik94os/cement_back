Ext.define('Cement.view.products.price.bottom.CurrentServices', {
    extend: 'Cement.view.products.price.bottom.NewServices',
    alias: 'widget.products_price_bottom_current_services',
    gridStore: 'products.price.CurrentServices',
    gridStateId: 'stateProductsPriceCurrentServices'
});