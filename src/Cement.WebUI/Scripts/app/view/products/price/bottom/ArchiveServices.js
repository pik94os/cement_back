Ext.define('Cement.view.products.price.bottom.ArchiveServices', {
    extend: 'Cement.view.products.price.bottom.NewServices',
    alias: 'widget.products_price_bottom_archive_services',
    gridStore: 'products.price.ArchiveServices',
    gridStateId: 'stateProductsPriceArchiveServices'
});