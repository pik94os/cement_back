Ext.define('Cement.view.products.price.bottom.ArchiveRoutes', {
    extend: 'Cement.view.products.price.bottom.NewRoutes',
    alias: 'widget.products_price_bottom_archive_routes',
    gridStore: 'products.price.ArchiveRoutes',
    gridStateId: 'stateProductsPriceArchiveRoutes'
});