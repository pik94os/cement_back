Ext.define('Cement.view.products.price.ArchiveComplex', {
	extend: 'Cement.view.basic.Complex',
	alias: 'widget.products_price_archive_complex',
    border: 0,
    enableDetail: false,
    autoLoadStore: false,
    priceId: null,
    bodyPadding: 0,
    padding: 0,
    header: false,
    title: 'Архив',
    tabTitle: 'Архив',
    topXType: 'products_price_list_archive',
    topDetailType: 'Cement.view.products.price.View',
    rowsCount: 2,
    bottomTitle: 'Справочник',
    bottomTabs: [{
        title: 'Товары',
        xtype: 'products_price_bottom_archive_products',
        constantFilterParam: 'p_price_archive',
        detailType: 'Cement.view.products.products.View'
    }, {
        title: 'Услуги',
        xtype: 'products_price_bottom_archive_services',
        constantFilterParam: 'p_price_archive',
        detailType: 'Cement.view.products.services.View'
    }, {
    	title: 'Маршрут',
    	xtype: 'products_price_bottom_archive_routes',
        constantFilterParam: 'p_price_archive',
    	detailType: 'Cement.view.products.price.RouteView'
    }],

    initComponent: function () {
    	Ext.apply(this, {
    		title: 'Архив'
    	});
    	this.callParent(arguments);
    }
});