Ext.define('Cement.view.products.price.View', {
	extend: 'Cement.view.basic.Form',
	alias: 'widget.products_price_view',
	fieldsDisabled: true,
	buttons: null,
	border: 0,
	tbar: Cement.Config.modelViewButtons,
	printUrl: Cement.Config.url.products.price.current.printItem,
	layout: 'autocontainer',
	bbar: null,
	noUserFieldsetsControl: false,
	bodyCls: 'model-form',

	getItems: function () {
	    return [
	        {
	            xtype: 'panel',
	            border: 0,
	            layout: 'anchor',
	            defaultType: 'textfield',
	            anchor: '100%',
	            bodyPadding: '10 10 10 5',
	            autoScroll: true,
	            defaults: {
	                anchor: '100%',
	                labelWidth: 110
	            },
	            items: [
	                {
	                    xtype: 'fieldset',
	                    layout: 'anchor',
	                    defaults: {
	                        xtype: 'textfield',
	                        anchor: '100%'
	                    },
	                    border: 0,
	                    items: [
				            {
				                fieldLabel: 'Наименование',
				                disabled: true,
				                name: 'p_name'
				            }, {
				                fieldLabel: 'Действует с',
                                disabled: true,
				                name: 'p_date_start'
				            }, {
				                fieldLabel: 'Действует по',
				                disabled: true,
				                name: 'p_date_end'
				            }, {
				                fieldLabel: 'Примечание',
				                disabled: true,
				                labelAlign: 'top',
				                name: 'p_comment',
				                height: 300
				            }
	                    ]
	                }
	            ]
	        }
	    ];
	},

	initComponent: function () {
	    Ext.apply(this, {
	        items: [
                {
                    xtype: 'panel',
                    border: 0,
                    layout: 'anchor',
                    defaultType: 'textfield',
                    anchor: '100%',
                    bodyPadding: '10 10 10 5',
                    autoScroll: true,
                    defaults: {
                        anchor: '100%',
                        labelWidth: 110
                    },
                    items: this.getItems()
                }
	        ]
	    });
	    this.callParent(arguments);
	}
});