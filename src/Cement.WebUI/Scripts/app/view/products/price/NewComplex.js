Ext.define('Cement.view.products.price.NewComplex', {
	extend: 'Cement.view.basic.Complex',
	alias: 'widget.products_price_new_complex',
    border: 0,
    enableDetail: false,
    autoLoadStore: false,
    priceId: null,
    bodyPadding: 0,
    padding: 0,
    header: false,
    title: 'Новые',
    tabTitle: 'Новые',
    topXType: 'products_price_list_new',
    topDetailType: 'Cement.view.products.price.View',
    rowsCount: 2,
    bottomTitle: 'Справочник',
    bottomTabs: [{
        title: 'Товары',
        xtype: 'products_price_bottom_new_products',
        constantFilterParam: 'p_price_new',
        detailType: 'Cement.view.products.products.View'
    }, {
        title: 'Услуги',
        xtype: 'products_price_bottom_new_services',
        constantFilterParam: 'p_price_new',
        detailType: 'Cement.view.products.services.View'
    }, {
    	title: 'Маршрут',
    	xtype: 'products_price_bottom_new_routes',
        constantFilterParam: 'p_price_new',
    	detailType: 'Cement.view.products.price.RouteView'
    }],

    initComponent: function () {
    	Ext.apply(this, {
    		title: 'Новые'
    	});
    	this.callParent(arguments);
    }
});