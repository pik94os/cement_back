Ext.define('Cement.view.products.price.ProductsWindow', {
	extend: 'Ext.window.Window',
	xtype: 'products_price_products_window',
	title: 'Выбрать прайс-лист',
	layout: {
		type: 'vbox',
		align : 'stretch'
	},
	width: 900,
	height: 400,
	bodyPadding: 10,
	bbar: [{
		text: 'Сохранить',
		action: 'confirm'
	}, {
		text: 'Отмена',
		action: 'cancel'
	}],
	modal: true,
	title: 'Выбрать товары(услуги)',
	topGridTitle: 'Товары',
	bottomGridTitle: 'Выбранный товар',
	loadRightFromTree: true,
	storeFields: ['id', 'p_name', 'p_date_start', 'p_date_end', 'p_replace_name', 'p_replace_date_start', 'p_replace_date_end', 'p_comment', 'p_tax', 'p_unit', 'p_price'],
	displayField: 'text',
	singleSelection: false,
	cls: 'white-window',
	srcStoreId: 'products.auxiliary.AllProducts',

	unitsStoreId: 'products.auxiliary.AllUnits',
	taxesStoreId: 'products.auxiliary.Taxes',

	mixins: [
		'Cement.view.basic.Combo'
	],

	getCenterMargin: function () {
		return '10 0';
	},

	loadData: function (data) {
		if (data) {
			var fstore = Ext.getStore(this.structure_storeId);
			fstore.load({
				callback: function () {
					Ext.each(data, function (item) {
						var r = fstore.getNodeById(item);
						if (!this.dstStore) {
							this.setupEvents();
						}
						if (r) {
							this.dstStore.add(r.copy());
						}
					}, this);
				},
				scope: this
			});
		}
	},

	getTopColumnsConfig: function () {
		return [
			{
				xtype: 'rowactions',
				hideable: false,
				resizeable: false,
				width: 18,
				keepSelection: true,
				actions: [{ 
	                iconCls: 'icon-add-document', 
	                qtip: 'Добавить',
	                callback: this.addEmployee
	            }]
			},
			{ text: 'Наименование', dataIndex: 'p_name', flex: 1 },
			{ text: 'Группа', dataIndex: 'p_group', flex: 1 },
			{ text: 'Подгруппа', dataIndex: 'p_subgroup', flex: 1 },
			{ text: 'Торговая марка', dataIndex: 'p_trade_mark', flex: 1 },
			{ text: 'Производитель', dataIndex: 'p_manufacturer', flex: 1 }
		];
	},

	getBottomColumnsConfig: function () {
		return [
			{
				xtype: 'rowactions',
				hideable: false,
				resizeable: false,
				width: 18,
				actions: [{ 
	                iconCls: 'icon-remove-document', 
	                qtip: 'Удалить', 
	                callback: this.removeEmployee
	            }]
			},
			{ text: 'Наименование', dataIndex: 'p_name', flex: 1 },
			{ text: 'Группа', dataIndex: 'p_group', flex: 1 },
			{ text: 'Подгруппа', dataIndex: 'p_subgroup', flex: 1 },
			{ text: 'Торговая марка', dataIndex: 'p_trade_mark', flex: 1 },
			{ text: 'Производитель', dataIndex: 'p_manufacturer', flex: 1 },
            { text: 'Единица измерения', dataIndex: 'p_unit_display', flex: 1 },
            { text: 'Цена', dataIndex: 'p_price', flex: 1 },
            { text: 'Налог', dataIndex: 'p_tax_display', flex: 1 }
		];
	},

	getTopGridFilters: function () {
		return [{
            text: 'Наименование',
            kind: 'selector',
            field_name: 'p_name',
            checked: true
        }, {
            text: 'Группа',
            kind: 'selector',
            field_name: 'p_group',
            checked: false
        }, {
            text: 'Подгруппа',
            kind: 'selector',
            field_name: 'p_subgroup',
            checked: false
        }, {
            text: 'Торговая марка',
            kind: 'selector',
            field_name: 'p_trade_mark',
            checked: false
        }, {
            text: 'Производитель',
            kind: 'selector',
            field_name: 'p_manufacturer',
            checked: false
        }];
	},

	getSrcStore: function () {
		return Ext.getStore(this.srcStoreId);
	},

	getSrcGrid: function () {
		var m_items = [{
		            xtype: 'cleartrigger',
	                emptyText: 'Поиск',
	                name: 'search_input',
	                onTrigger1Click: this.clearFilter,
	                onTrigger2Click: this.applyFilter
		        }, {
		            text: 'Выделить все',
		            checked: false,
		            checkHandler: this.toggleFilterSelection
		        }, '-'];
		Ext.each(this.getTopGridFilters(), function (itm) {
			m_items.push(itm);
		});
		return {
			xtype: 'panel',
			flex: 1,
			border: 1,
			layout: 'fit',
			role: 'sign-src-panel',
			tbar: [{
				xtype: 'label',
				text: this.topGridTitle,
				padding: '2 0 4 10'
			}, '->', {
                xtype: 'button',
                text: 'Поиск',
                menu: {
                    xtype: 'menu',
                    plain: true,
                    items: m_items
                }
            }],
			items: [{
				xtype: 'grid',
				layout: 'fit',
				role: 'sign-src-grid',
				border: 0,
				store: this.getSrcStore(),
				columns: this.getTopColumnsConfig()
			}]
		};
	},

	getComboRenderer: function (store) {
        return function (val) {
            if (!val) return '';
            return store.getById(val).get('p_name');
        };
    },

	getDstGrid: function () {
		this.statusStore = Ext.getStore('signers_statuses_Store');
		this.editor = Ext.create('Ext.grid.plugin.CellEditing', {
            clicksToEdit: 1,
            listeners: {
                edit: function (editor, e) {
                    e.grid.getSelectionModel().deselectAll(true);
                    e.grid.getSelectionModel().select(e.rowIdx);
                }
            }
        });
		return {
			xtype: 'panel',
			columnWidth: 0.65,
			border: 1,
			height: 150,
			layout: 'fit',
			tbar: [{
				xtype: 'label',
				text: this.bottomGridTitle,
				padding: '2 0 4 10'
			}],
			items: [{
				xtype: 'grid',
				// id: 'sign-dst-grid',
				role: 'sign-dst-grid',
				layout: 'fit',
				border: 0,
				store: Ext.create('Ext.data.Store', {
					fields: this.storeFields
				}),
				columns: this.getBottomColumnsConfig(),
				plugins: [
                    this.editor
                ]
			}]
		};
	},

	initComponent: function () {
		Ext.apply(this, {
			items: [this.getSrcGrid(), {
				xtype: 'panel',
				border: 0,
				margin: '5 0',
				defaults: {
					xtype: 'button'
				},
				items: [{
					text: 'Создать всех',
					action: 'create_all',
					iconCls: 'icon-move-bottom',
					hidden: this.singleSelection,
					margin: '0 5 0 0'
				}, {
					text: 'Создать',
					action: 'create',
					iconCls: 'icon-move-down',
					margin: '0 5 0 0'
				}, {
					text: 'Удалить всех',
					action: 'delete_all',
					hidden: this.singleSelection,
					iconCls: 'icon-move-top',
					margin: '0 5 0 0'
				}, {
					text: 'Удалить',
					action: 'delete',
					iconCls: 'icon-move-up'
				}]
			}, {
				xtype: 'panel',
				border: 0,
				height: 150,
				layout: 'column',
				items: [
					this.getDstGrid(), {
						margin: '0 0 0 10',
						height: 150,
						xtype: 'form',
						border: 1,
						columnWidth: 0.35,
						bodyPadding: 10,
						role: 'count',
						defaults: {
							anchor: '100%',
							labelWidth: 130,
							labelAlign: 'right'
						},
						disabled: true,
						items: [
							this.getCombo('Единица измерения', 'p_unit', this.unitsStoreId),
							this.getCombo('Налог', 'p_tax', this.taxesStoreId), {
								xtype: 'numberfield',
								name: 'p_price',
								fieldLabel: 'Цена'
							}
						]
					}
				]
			}]
		});
		this.callParent(arguments);
		this.setupEvents();
		this.addEvents('priceselected');
		Ext.getStore(this.srcStoreId).load();
	},

	addToSrcStore: function (item) {
		if (!this.dstStore.getById(item.get('id'))) {
			var r = {};
			Ext.each(this.storeFields, function (i) {
				r[i] = item.get(i);
			});
			this.srcStore.add(r);
		}
	},

	setupEvents: function () {
		var commentField = this.down('textarea[name=comment]');
		this.srcStore = this.down('*[role=sign-src-grid]').getStore();
		this.dstStore = this.down('*[role=sign-dst-grid]').getStore();

		this.down('*[role=sign-dst-grid]').on('selectionchange', function (grid, selected) {
			if (selected.length) {
				this.down('form[role=count]').enable();
			}
			else {
				this.down('form[role=count]').disable();
			}
		}, this);

		this.down('*[role=sign-dst-grid]').on('select', function (grid, record) {
			this.down('form[role=count]').getForm().setValues({
				'p_tax': record.get('p_tax'),
				'p_price': record.get('p_price'),
				'p_unit': record.get('p_unit')
			});
		}, this);

		this.down('*[role=sign-dst-grid]').on('beforedeselect', function (grid, record) {
			if (record) {
				var values = this.down('form[role=count]').getForm().getValues();
				record.set('p_tax', values['p_tax']);
				record.set('p_price', values['p_price']);
				record.set('p_unit', values['p_unit']);

				record.set('p_unit_display',this.down('form[role=count]').getForm().findField("p_unit").getDisplayValue());
				record.set('p_tax_display', this.down('form[role=count]').getForm().findField("p_tax").getDisplayValue());
			}
		}, this);

		this.down('combo[name=p_unit]').on('change', function (combo) {
		    this.setComboDisplayValue(combo.getDisplayValue(), "p_unit_display");
		    this.setComboDisplayValue(combo.getValue(), "p_unit");
		}, this);

		this.down('combo[name=p_tax]').on('change', function (combo) {
		    this.setComboDisplayValue(combo.getDisplayValue(), "p_tax_display");
		    this.setComboDisplayValue(combo.getValue(), "p_tax");
		}, this);

		this.down('numberfield[name=p_price]').on('change', function (combo) {
		    this.setComboDisplayValue(combo.getValue(), "p_price");
		}, this);

		this.down('button[action=cancel]').on('click', function () {
			this.closeWindow();
		}, this);

		this.down('button[action=confirm]').on('click', function () {
			this.sign(false);
		}, this);

		if (this.down('button[action=confirm_and_send]')) {
			this.down('button[action=confirm_and_send]').on('click', function () {
				this.sign(true);
			}, this);
		}

		this.down('button[action=delete_all]').on('click', function () {
			this.dstStore.each(function (record) {
				if (record.get('department_id') == this.currentDepartment) {
					this.srcStore.add(record.copy());
				}
			}, this);
			this.dstStore.removeAll();
		}, this);
		this.down('button[action=delete]').on('click', function () {
			var grid = this.down('*[role=sign-dst-grid]'),
				sel = grid.getSelectionModel().getSelection();
			if (sel.length > 0) {
				this.removeE(sel[0]);
			}
		}, this);
		this.down('button[action=create_all]').on('click', function () {
			this.srcStore.each(function (record) {
				this.dstStore.add(record.copy());
			}, this);
			this.srcStore.removeAll();
		}, this);
		this.down('button[action=create]').on('click', function () {
			var grid = this.down('*[role=sign-src-grid]'),
				sel = grid.getSelectionModel().getSelection();
			if (sel.length > 0) {
				this.addE(sel[0]);
			}
		}, this);
	},

	closeWindow: function () {
		this.close();
	},

	sign: function (send) {
		if (this.dstStore.getCount() > 0) {
			this.fireEvent('priceselected', this.dstStore);
		}
	},

	addE: function (record) {
		if (this.singleSelection) {
			if (this.dstStore.getCount() > 0) {
				return;
			}
		}
		this.dstStore.add(record.copy());
		this.srcStore.remove(record);
	},

	removeE: function (record) {
		if (record) {
			if (record.get('department_id') == this.currentDepartment) {
				this.srcStore.add(record.copy());
			}
			this.dstStore.remove(record);
		}
	},

	addEmployee: function (grid, record) {
		grid.up('window').addE(record);
	},

	removeEmployee: function (grid, record) {
		grid.up('window').removeE(record);
	},

	toggleFilterSelection: function (item, checked) {
        var toolbar = item.up('toolbar'),
            items = toolbar.query('menu menucheckitem[kind=selector]');
        Ext.each(items, function (it) {
            it.setChecked(item.checked);
        });
    },

    getFilterFields: function () {
        var items = this.down('toolbar').query('menu menucheckitem[kind=selector]'),
            val = this.down('toolbar cleartrigger[name=search_input]').getValue(),
            result = [];
        Ext.each(items, function (item) {
            if (item.checked) {
                result.push({
                    property: item.field_name,
                    value: new RegExp('.*' + val + '.*')
                });
            }
        });
        return result;
    },

    setComboDisplayValue: function(value, recordField) {
        var selModel = this.down('*[role=sign-dst-grid]').getSelectionModel();
        if (selModel.selected.items.length == 1) {
            var item = selModel.selected.items[0];
            item.set(recordField, value);
        }
    },

    applyFilter: function () {
        var grid = this.up('*[role=sign-src-panel]').down('*[role=sign-src-grid]'),
            filter = grid.up('window').getFilterFields();
        grid.getStore().clearFilter(true);
        grid.getStore().filter(filter);
    },

    clearFilter: function () {
    	this.up('*[role=sign-src-panel]').down('toolbar cleartrigger[name=search_input]').setValue('');
    	this.up('*[role=sign-src-panel]').down('*[role=sign-src-grid]').getStore().clearFilter(false);
    }
});