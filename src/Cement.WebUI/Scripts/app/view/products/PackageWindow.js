Ext.define('Cement.view.products.PackageWindow', {
	extend: 'Ext.window.Window',
	xtype: 'products_package_window',
	title: 'Тара, упаковка, единица измерения',
	width: 805,
	height: 270,
	layout: 'fit',
	resizable: false,
	modal: true,
	closeAction: 'hide',

	mixins: [
		'Cement.view.basic.Combo'
	],

	getLongField: function (total_label, name1, name2, name3, store, name4, name5) {
		var result = {
			xtype: 'fieldcontainer',
			fieldLabel: total_label,
			combineErrors: true,
			msgTarget : 'side',
			layout: 'hbox',
			defaults: {
				hideLabel: true
			},
			items: [{
				xtype: 'textfield',
				name: name1,
				flex: 1,
				disabled: this.fieldsDisabled
			}]
		},
			combo = {
				xtype: 'combo',
				name: name2,
				editable: false,
				store: Ext.data.StoreManager.lookup(store),
				queryMode: 'local',
				displayField: 'p_name',
				valueField: 'id',
				width: 60,
				margin: '0 0 0 5',
				disabled: this.fieldsDisabled
			};
		if (name4) {
			result.items.push({
				xtype: 'textfield',
				name: name4,
				flex: 1,
				disabled: this.fieldsDisabled,
				margin: '0 0 0 5'
			});
		}
		if (name5) {
			result.items.push({
				xtype: 'textfield',
				name: name5,
				flex: 1,
				disabled: this.fieldsDisabled,
				margin: '0 0 0 5'
			});
		}
		result.items.push(combo);
		if (this.fieldsDisabled) {
			result = {
				xtype: 'textfield',
				fieldLabel: 'Объем',
				name: name3,
				disabled: this.fieldsDisabled
			};
		}
		return result;
	},

	bbar: [{
		text: 'Сохранить',
		action: 'save'
	}, {
		text: 'Отмена',
		action: 'cancel'
	}],

	getStr: function() {
		var values = this.down('form').getForm().getValues(),
			str = '',
			repeat_str = '';
		if (values['p_package_kind']) {
			if (values['p_package_kind'] == Cement.Config.products.package_kinds.SIMPLE) {
				str = 'Единичная упаковка: ';
				str += ' ' + this.getComboSelText('p_package_item_kind');
				str += ', ' + this.getComboSelText('p_package_item_material');
				str += ', ' + this.down('textfield[name=p_package_item_volume]').getValue();
				str += ' ' + this.getComboSelText('p_package_item_volume_unit');
				str += ', ' + this.down('textfield[name=p_package_item_weight]').getValue();
				str += ' ' + this.getComboSelText('p_package_item_weight_unit');
				str += ', ' + this.down('textfield[name=p_package_item_size_w]').getValue() + 'x' +
					this.down('textfield[name=p_package_item_size_h]').getValue() + 'x' +
					this.down('textfield[name=p_package_item_size_d]').getValue();
				str += this.getComboSelText('p_package_item_size_unit');
			}
			if (values['p_package_kind'] == Cement.Config.products.package_kinds.GROUP) {
				str = 'Групповая упаковка: ';
				str += this.down('textfield[name=p_package_group_count]').getValue() + 'шт';
				str += ', ' + this.getComboSelText('p_package_group_kind');
				str += ', ' + this.getComboSelText('p_package_group_material');
				str += ', ' + this.down('textfield[name=p_package_group_volume]').getValue();
				str += ' ' + this.getComboSelText('p_package_group_volume_unit');
				str += ', ' + this.down('textfield[name=p_package_group_weight]').getValue();
				str += ' ' + this.getComboSelText('p_package_group_weight_unit');
				str += ', ' + this.down('textfield[name=p_package_group_size_w]').getValue() + 'x' +
					this.down('textfield[name=p_package_group_size_h]').getValue() + 'x' +
					this.down('textfield[name=p_package_group_size_d]').getValue();
				str += this.getComboSelText('p_package_group_size_unit');
			}
			if (values['p_package_kind'] == Cement.Config.products.package_kinds.TRANSPORT) {
				str = 'Транспортная упаковка: ';
				str += this.down('textfield[name=p_package_vehicle_count]').getValue() + 'шт';
				str += ', ' + this.getComboSelText('p_package_vehicle_kind');
				str += ', ' + this.getComboSelText('p_package_vehicle_material');
				str += ', ' + this.down('textfield[name=p_package_vehicle_volume]').getValue();
				str += ' ' + this.getComboSelText('p_package_vehicle_volume_unit');
				str += ', ' + this.down('textfield[name=p_package_vehicle_weight]').getValue();
				str += ' ' + this.getComboSelText('p_package_vehicle_weight_unit');
				str += ', ' + this.down('textfield[name=p_package_vehicle_size_w]').getValue() + 'x' +
					this.down('textfield[name=p_package_vehicle_size_h]').getValue() + 'x' +
					this.down('textfield[name=p_package_vehicle_size_d]').getValue();
				str += this.getComboSelText('p_package_vehicle_size_unit');
			}
		}
		return str;
	},

	getJSON: function() {
		return Ext.JSON.encode(this.down('form').getForm().getValues());
	},

	initComponent: function () {
		Ext.apply(this, {
			items: [{
				xtype: 'form',
				border: 0,
				layout: {
					type: 'absolute'
				},
				items: [{
					xtype: 'panel',
					x: 10,
					y: 10,
					width: 221,
					height: 190,
					bodyPadding: 10,
					items: [{
						xtype: 'fieldcontainer',
						defaultType: 'radiofield',
						layout: 'vbox',
						items: [{
							boxLabel: 'Единичная упаковка',
							inputValue: Cement.Config.products.package_kinds.SIMPLE,
							name: 'p_package_kind'
						}, {
							boxLabel: 'Групповая упаковка',
							inputValue: Cement.Config.products.package_kinds.GROUP,
							name: 'p_package_kind'
						}, {
							boxLabel: 'Транспортная упаковка',
							inputValue: Cement.Config.products.package_kinds.TRANSPORT,
							name: 'p_package_kind'
						}]
					}]
				}, {
					xtype: 'panel',
					width: 540,
					height: 190,
					x: 241,
					y: 10,
					bodyPadding: '10',
					items: [{
						xtype: 'panel',
						border: 0,
						layout: 'anchor',
						hidden: true,
						role: 'repeat_details',
						itemId: 'repeat_details_' + Cement.Config.products.package_kinds.SIMPLE,
						defaults: {
							anchor: '100%',
							labelWidth: 120
						},
						items: [this.getCombo('Вид упаковки', 'p_package_item_kind', 'products.auxiliary.PackageKinds'),
							this.getCombo('Материал', 'p_package_item_material', 'products.auxiliary.PackageMaterials'),
							this.getLongField(
								'Объем:',
								'p_package_item_volume',
								'p_package_item_volume_unit',
								'p_package_item_volume_display',
								'products.auxiliary.VolumeUnits'
							),
							this.getLongField(
								'Вес:',
								'p_package_item_weight',
								'p_package_item_weight_unit',
								'p_package_item_weight_display',
								'products.auxiliary.WeightUnits'
							),
							this.getLongField(
								'Дл. x Шир. x Выс.',
								'p_package_item_size_w',
								'p_package_item_size_unit',
								'p_package_item_size_display',
								'products.auxiliary.SizeUnits',
								'p_package_item_size_h',
								'p_package_item_size_d'
							)
						]
					}, {
						xtype: 'panel',
						hidden: true,
						layout: 'anchor',
						role: 'repeat_details',
						itemId: 'repeat_details_' + Cement.Config.products.package_kinds.GROUP,
						border: 0,
						defaults: {
							anchor: '100%',
							xtype: 'textfield',
							labelWidth: 170
						},
						items: [{
							fieldLabel: 'Кол-во един. упаковки, шт.',
							disabled: this.fieldsDisabled,
							allowBlank: this.allowBlankFields,
							name: 'p_package_group_count'
						},
						this.getCombo('Вид упаковки', 'p_package_group_kind', 'products.auxiliary.PackageKinds'),
						this.getCombo('Материал', 'p_package_group_material', 'products.auxiliary.PackageMaterials'),
						this.getLongField(
							'Объем',
							'p_package_group_volume',
							'p_package_group_volume_unit',
							'p_package_group_volume_display',
							'products.auxiliary.VolumeUnits'
						),
						this.getLongField(
							'Вес',
							'p_package_group_weight',
							'p_package_group_weight_unit',
							'p_package_group_weight_display',
							'products.auxiliary.WeightUnits'
						),
						this.getLongField(
							'Дл. x Шир. x Выс.',
							'p_package_group_size_w',
							'p_package_group_size_unit',
							'p_package_group_size_display',
							'products.auxiliary.SizeUnits',
							'p_package_group_size_h',
							'p_package_group_size_d'
						)]
					}, {
						xtype: 'panel',
						layout: 'anchor',
						hidden: true,
						role: 'repeat_details',
						itemId: 'repeat_details_' + Cement.Config.products.package_kinds.TRANSPORT,
						border: 0,
						defaults: {
							anchor: '100%',
							labelWidth: 170,
							xtype: 'textfield'
						},
						items: [{
							fieldLabel: 'Кол-во груп. упаковки, шт.',
							disabled: this.fieldsDisabled,
							allowBlank: this.allowBlankFields,
							name: 'p_package_vehicle_count'
						},
						this.getCombo('Вид упаковки', 'p_package_vehicle_kind', 'products.auxiliary.PackageKinds'),
						this.getCombo('Материал', 'p_package_vehicle_material', 'products.auxiliary.PackageMaterials'),
						this.getLongField(
							'Объем',
							'p_package_vehicle_volume',
							'p_package_vehicle_volume_unit',
							'p_package_vehicle_volume_display',
							'products.auxiliary.VolumeUnits'
						),
						this.getLongField(
							'Вес',
							'p_package_vehicle_weight',
							'p_package_vehicle_weight_unit',
							'p_package_vehicle_weight_display',
							'products.auxiliary.WeightUnits'
						),
						this.getLongField(
							'Дл. x Шир. x Выс.',
							'p_package_vehicle_size_w',
							'p_package_vehicle_size_unit',
							'p_package_vehicle_size_display',
							'products.auxiliary.SizeUnits',
							'p_package_vehicle_size_h',
							'p_package_vehicle_size_d'
						)]
					}]
				}]
			}]
		});
		this.callParent(arguments);
		this.addEvents('packagecreated');
		Ext.each(this.query('radiofield[name=p_package_kind]'), function (item) {
			item.on('change', function () {
				var values = this.down('form').getForm().getValues();
				if (values['p_package_kind']) {
					if (!values['p_package_kind'].length) {
						Ext.each(this.query('*[role=repeat_details]'), function (container) {
							container.hide();
						});
						this.down('panel[itemId=repeat_details_' + values['p_package_kind']+']').show();
					}
				}
			}, this);
		}, this);
		this.down('button[action=save]').on('click', function () {
			this.fireEvent('packagecreated', this.getJSON(), this.getStr());
		}, this);
		this.down('button[action=cancel]').on('click', function () {
			this.hide();
		}, this);
		this.on('beforeclose', function () {
			this.hide();
			return false;
		});
	},

	closeWindow: function () {
		this.hide();
	},

	loadData: function (data) {
		if (data) {
			this.down('form').getForm().setValues(data);
		}
	}
});