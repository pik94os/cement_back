Ext.define('Cement.view.products.ComplectWindow', {
	extend: 'Cement.view.common.SignWindow',
	xtype: 'products_complect_window',
	title: 'Выбор товара, услуги в комплект',
	hideCommentPanel: true,
	leftFrameTitle: 'Товары, услуги',
	topGridTitle: 'Выбрать товар, услугу',
	bottomGridTitle: 'Выбранный товар, услуга',
    storeFields: ['id', 'p_name', 'p_trade_mark', 'p_manufacturer', 'p_unit', 'p_complect'],
    displayField: 'p_name',

	structure_storeId: 'products.auxiliary.Complect',

    bbar: [{
        text: 'Сохранить',
        action: 'confirm'
    }, {
        text: 'Отмена',
        action: 'cancel'
    }],

    complectColRenderer: function (value, obj, record) {
        var result = "<div class='doc-status-icon";
        if (record.get('p_complect')) {
            result += ' has-documents';
        }
        result += "'></div>";
        return result;
    },

	getBottomColumnsConfig: function () {
		return [{
			xtype: 'rowactions',
			hideable: false,
			resizeable: false,
			width: 18,
			actions: [{ 
                iconCls: 'icon-remove-document', 
                qtip: 'Удалить', 
                callback: this.removeEmployee
            }]
		},
            { text: '', dataIndex: 'p_complect', renderer: this.complectColRenderer, width: 20, tdCls: 'dark' },
            { text: 'Наименование', dataIndex: 'p_name', flex: 1 },
            { text: 'Торговая марка', dataIndex: 'p_trade_mark', flex: 1 },
            { text: 'Производитель', dataIndex: 'p_manufacturer', flex: 1 },
            { text: 'Ед. изм.', dataIndex: 'p_unit', flex: 1 }
        ];
	},

	getTopGridFilters: function () {
        return [{
            text: 'Наименование',
            kind: 'selector',
            field_name: 'p_name',
            checked: false
        }, {
            text: 'Торговая марка',
            kind: 'selector',
            field_name: 'p_trade_mark',
            checked: false
        }, {
            text: 'Производитель',
            kind: 'selector',
            field_name: 'p_manufacturer',
            checked: false
        }, {
            text: 'Ед. изм.',
            kind: 'selector',
            field_name: 'p_unit',
            checked: false
        }];
	},

	getTopColumnsConfig: function () {
		return [
			{
				xtype: 'rowactions',
				hideable: false,
				resizeable: false,
				width: 18,
				keepSelection: true,
				actions: [{ 
	                iconCls: 'icon-add-document', 
	                qtip: 'Добавить',
	                callback: this.addEmployee
	            }]
			},
            { text: '', dataIndex: 'p_complect', renderer: this.complectColRenderer, width: 20, tdCls: 'dark' },
			{ text: 'Наименование', dataIndex: 'p_name', flex: 1 },
            { text: 'Торговая марка', dataIndex: 'p_trade_mark', flex: 1 },
            { text: 'Производитель', dataIndex: 'p_manufacturer', flex: 1 },
            { text: 'Ед. изм.', dataIndex: 'p_unit', flex: 1 }
		];
	},

    sign: function (send) {
        var store = this.dstStore,
            str = [],
            data = [];
        store.each(function (item) {
            str.push(item.get('p_name'));
            data.push(item.get('id'));
        });
        this.fireEvent('complectselected', data, str);
    },

    initComponent: function () {
        this.callParent(arguments);
        this.on('beforeclose', function () {
            this.hide();
            return false;
        });
        this.down('treepanel').on('itemclick', function (tree, record) {
            var items = [];
            this.srcStore.removeAll();
            if (record.get('leaf')) {
                items.push(record);
            }
            else {
                Ext.each(record.childNodes, function (child) {
                    if (child.get('leaf')) {
                        items.push(child);
                    }
                });
            }
            Ext.each(items, function (item) {
                this.addToSrcStore(item);
            }, this);
        }, this);
        this.addEvents('complectselected');
        //var tree = this.down('treepanel');
        //tree.collapseAll();
        //tree.expandNode(tree.getRootNode()); //Store loading
    }
});