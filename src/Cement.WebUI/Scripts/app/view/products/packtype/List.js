Ext.define('Cement.view.products.packtype.List', {
    extend: 'Cement.view.basic.Grid',
    title: 'Тип упаковки',
    alias: 'widget.products_packtype_list',
    gridStore: 'products.PackType',
    gridStateId: 'stateProductsList',
    printUrl: Cement.Config.url.products.packtype.printGrid,
    helpUrl: Cement.Config.url.products.packtype.help,
    printItemUrl: Cement.Config.url.products.packtype.printItem,
    cls: 'simple-form',
    bbarText: 'Показаны типы упаковки {0} - {1} из {2}',
    bbarEmptyMsg: 'Нет типов упаковки',
    bbarUsersText: 'Типов упаковки на странице: ',
    showCreateButton: false,

    getFilterItems: function () {
        return [{
            text: 'Наименование',
            kind: 'selector',
            field_name: 'p_name',
            checked: false
        }, {
            text: 'Описание',
            kind: 'selector',
            field_name: 'p_text',
            checked: false
        }];
    },

    getGridColumns: function () {
        var result = [
            { text: 'Наименование', dataIndex: 'p_name', width: 200},
            { text: 'Описание', dataIndex: 'p_text', flex: 1 }
        ];
        return result;
    },

    initComponent: function () {
        Ext.apply(this, {
            title: 'Тип упаковки'
        });
        this.callParent(arguments);
    }
});