Ext.define('Cement.view.products.packtype.View', {
    extend: 'Cement.view.basic.Form',
    alias: 'widget.products_packtype_view',
	fieldsDisabled: true,
	buttons: null,
	border: 0,
	tbar: Cement.Config.modelViewButtons,
	printUrl: Cement.Config.url.products.packtype.printItem,
	layout: 'autocontainer',
	bbar: null,
	noUserFieldsetsControl: false,
	bodyCls: 'model-form',
	certMargins: '0 0 5 0',

	getItems: function () {
	    return [
	        {
	            xtype: 'panel',
	            border: 0,
	            layout: 'anchor',
	            defaultType: 'textfield',
	            anchor: '100%',
	            bodyPadding: '10 10 10 5',
	            autoScroll: true,
	            defaults: {
	                anchor: '100%',
	                labelWidth: 110
	            },
	            items: [
	                {
	                    xtype: 'fieldset',
	                    layout: 'anchor',
	                    defaults: {
	                        xtype: 'textfield',
	                        anchor: '100%',
	                        labelAlign: 'top',
	                        labelClsExtra: 'top-label-left'
	                    },
	                    border: 0,
	                    items: [
                            {
                                xtype: 'cementimage'
                            },
				            {
				                fieldLabel: 'Наименование',
				                disabled: true,
				                name: 'p_name'
				            }, {
				                fieldLabel: 'Описание',
				                xtype: 'textarea',
				                disabled: true,
				                name: 'p_text',
				                height: 300
				            }
	                    ]
	                }
	            ]
	        }
	    ];
	},

	initComponent: function () {
	    Ext.apply(this, {
	        items: [
                {
                    xtype: 'panel',
                    border: 0,
                    layout: 'anchor',
                    defaultType: 'textfield',
                    anchor: '100%',
                    bodyPadding: '10 10 10 5',
                    autoScroll: true,
                    defaults: {
                        anchor: '100%',
                        labelWidth: 110
                    },
                    items: this.getItems()
                }
	        ]
	    });
	    this.callParent(arguments);
	}
});