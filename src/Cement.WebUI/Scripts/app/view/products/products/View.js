Ext.define('Cement.view.products.products.View', {
	extend: 'Cement.view.products.products.Form',
	alias: 'widget.products_products_view',
	fieldsDisabled: true,
	buttons: null,
	border: 0,
	tbar: Cement.Config.modelViewButtons,
	layout: 'autocontainer',
	bbar: null,
	noUserFieldsetsControl: false,
	bodyCls: 'model-form',
	certMargins: '0 0 5 0',
	loadItemUrl: Cement.Config.url.products.products.loadItem,

	getBasicFieldSet: function () {
	    var me = this,
            items = me.callParent(arguments);

	    Ext.Array.remove(items, Ext.Array.findBy(items, function (item) { return item.xtype == 'downloadfield'; }));

	    return items;
	},

	getPackingSecondFields: function () {
		return [{
			xtype: 'radio',
			name: 'p_package_type',
			value: Cement.Config.products.package_kinds.GROUP,
			inputValue: Cement.Config.products.package_kinds.GROUP,
			style: 'font-weight: bold; font-size: 11px;',
			disabled: this.fieldsDisabled,
			boxLabel: 'Групповая упаковка'
		}, {
			fieldLabel: 'Кол-во един. упаковки, шт.',
			disabled: this.fieldsDisabled,
			allowBlank: this.allowBlankFields,
			name: 'p_package_group_count'
		}, {
			xtype: 'textfield',
			disabled: true,
			name: 'p_package_group_kind_display',
			fieldLabel: 'Вид упаковки'
		}, {
			xtype: 'textfield',
			disabled: true,
			name: 'p_package_group_material_display',
			fieldLabel: 'Материал'
		}, {
			xtype: 'textfield',
			disabled: true,
			name: 'p_package_group_volume_display',
			fieldLabel: 'Объем'
		}, {
			xtype: 'textfield',
			disabled: true,
			name: 'p_package_group_weight_display',
			fieldLabel: 'Вес'
		}, {
			xtype: 'textfield',
			disabled: true,
			name: 'p_package_group_size_display',
			fieldLabel: 'Дл. x Шир. x Выс.'
		}];
	},

	getPackingThirdFields: function () {
		return [{
				xtype: 'radio',
				name: 'p_package_type',
				value: Cement.Config.products.package_kinds.TRANSPORT,
				disabled: this.fieldsDisabled,
				inputValue: Cement.Config.products.package_kinds.TRANSPORT,
				style: 'font-weight: bold; font-size: 11px;',
				boxLabel: 'Транспортная упаковка'
			}, {
				fieldLabel: 'Кол-во груп. упаковки, шт.',
				disabled: this.fieldsDisabled,
				allowBlank: this.allowBlankFields,
				name: 'p_package_vehicle_count'
			}, {
				xtype: 'textfield',
				disabled: true,
				fieldLabel: 'Вид упаковки',
				name: 'p_package_vehicle_kind_display'
			}, {
				xtype: 'textfield',
				disabled: true,
				fieldLabel: 'Материал',
				name: 'p_package_vehicle_material'
			}, {
				xtype: 'textfield',
				disabled: true,
				fieldLabel: 'Объем',
				name: 'p_package_vehicle_volume_display'
			}, {
				xtype: 'textfield',
				disabled: true,
				fieldLabel: 'Вес',
				name: 'p_package_vehicle_weight_display'
			}, {
				xtype: 'textfield',
				disabled: true,
				fieldLabel: 'Дл. x Шир. x Выс.',
				name: 'p_package_vehicle_size_display'
			}];
	},

	getPackingFirstFields: function () {
		var padding = 0;
		if (!this.fieldsDisabled) {
			padding = '0 0 36 0';
		}
		return [{
			xtype: 'radio',
			disabled: this.fieldsDisabled,
			name: 'p_package_type',
			value: Cement.Config.products.package_kinds.SIMPLE,
			inputValue: Cement.Config.products.package_kinds.SIMPLE,
			style: 'font-weight: bold; font-size: 11px;',
			boxLabel: 'Единичная упаковка',
			padding: padding
		}, {
			xtype: 'textfield',
			disabled: true,
			name: 'p_package_item_kind_display',
			fieldLabel: 'Вид упаковки'
		}, {
			xtype: 'textfield',
			disabled: true,
			name: 'p_package_item_material_display',
			fieldLabel: 'Материал'
		}, {
			xtype: 'textfield',
			disabled: true,
			name: 'p_package_item_volume_display',
			fieldLabel: 'Объем'
		}, {
			xtype: 'textfield',
			disabled: true,
			name: 'p_package_item_weight_display',
			fieldLabel: 'Вес'
		}, {
			xtype: 'textfield',
			disabled: true,
			name: 'p_package_item_size_display',
			fieldLabel: 'Дл. x Шир. x Выс.'
		}];
	},



	getPacking: function () {
		var result = {
			layout: 'anchor',
			xtype: 'fieldset',
			collapsed: true,
			collapsible: true,
			title: 'Тара и упаковка',
			cls: 'package',
			defaults: {
				xtype: 'textfield',
				anchor: '100%',
				labelWidth: 160
			},
			anchor: '100%',
			items: this.getPackingFirstFields()
		};
		items2 = this.getPackingSecondFields(),
		items3 = this.getPackingThirdFields();
		for (var i = 0; i < items2.length; i++) {
			result.items.push(items2[i]);
		}
		for (var i = 0; i < items3.length; i++) {
			result.items.push(items3[i]);
		}
		return result;
	},

	getItems: function () {
		this.fieldsDisabled = true;
	    return [
	        {
	            xtype: 'cementimage'
	        },
            {
				xtype: 'fieldset',
				border: 0,
				margin: 0,
				// padding: '0 110',
				defaults: {
					xtype: 'textfield',
					anchor: '100%'
				},
				items: this.getBasicFieldSet()
            }, {
                xtype: 'fieldset',
                collapsible: true,
                collapsed: true,
                title: 'Происхождение',
                defaults: {
                    xtype: 'textfield',
                    anchor: '100%',
                    labelWidth: 110
                },
                items: this.getFromFields()
            }, {
                xtype: 'fieldset',
                collapsible: true,
                collapsed: true,
                title: 'Коды',
                defaults: {
                    xtype: 'textfield',
                    anchor: '100%',
                    labelWidth: 110
                },
                items: this.getCodeFieldSet()
            }, {
                xtype: 'fieldset',
                collapsible: true,
                collapsed: true,
                title: 'Технические условия',
                defaults: {
                    xtype: 'textfield',
                    anchor: '100%',
                    labelWidth: 110
                },
                items: this.getTechnicalFieldSet()
            }, {
                xtype: 'fieldset',
                collapsible: true,
                collapsed: true,
                title: 'Сертификаты',
                defaults: {
                    xtype: 'textfield',
                    anchor: '100%',
                    labelWidth: 110
                },
                items: this.getCertificatesFieldSet()
            }, {
                xtype: 'fieldset',
                collapsible: true,
                collapsed: true,
                title: 'Класс опасности (ADR)',
                defaults: {
                    xtype: 'textfield',
                    anchor: '100%',
                    labelWidth: 110
                },
                items: this.getDangerFieldSet()
            }, this.getPacking()
        ];
	},

	initComponent: function ()  {
		Ext.apply(this, {
			fieldsDisabled: true,
			items: [{
				xtype: 'panel',
				border: 0,
				layout: 'anchor',
				defaultType: 'textfield',
				anchor: '100%',
				bodyPadding: '10 10 10 5',
				autoScroll: true,
				defaults: {
					anchor: '100%',
					labelWidth: 110
				},
				items: this.getItems()
			}]
		});
		this.callParent(arguments);
	}
});