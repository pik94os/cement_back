Ext.define('Cement.view.products.products.Total', {
	extend: 'Cement.view.basic.Tabs',
	title: 'Товары',

	items:
    [
        {
            xtype: 'products_products_complex_moderation',
		    title: 'На модерацию'
	    },
        {
            xtype: 'products_products_complex_general',
		    title: 'Общий каталог'
	    },
        {
            xtype: 'products_products_complex_personal',
		    title: 'Персональный каталог'
	    },
        {
            xtype: 'products_products_complex_archive',
		    title: 'Архив'
        }
	],

	initComponent: function () {
		Ext.apply(this, {
			title: 'Товары'
		});
		this.callParent(arguments);
	}
});