Ext.define('Cement.view.products.products.List', {
    extend: 'Cement.view.basic.Grid',
    title: 'Товары',
    alias: 'widget.products_products_list',
    gridStore: 'products.Products',
    gridStateId: 'stateProductsProductsList',
    printUrl: Cement.Config.url.products.products.printGrid,
    helpUrl: Cement.Config.url.products.products.help,
    deleteUrl: Cement.Config.url.products.products.deleteUrl,
    printItemUrl: Cement.Config.url.products.products.printItem,
    cls: 'simple-form',
    bbarText: 'Показаны товары {0} - {1} из {2}',
    bbarEmptyMsg: 'Нет товаров',
    bbarUsersText: 'Товаров на странице: ',
    
    gridViewConfig: {
        getRowClass: function(record, index, rowParams, store) {
            return record.get('p_complect').length == 0 ? 'expander-hidden' : '';
        }
    },
    
    getGridPlugins: function () {
        return [{
            ptype: 'rowexpander',
            rowBodyTpl : new Ext.XTemplate(
                '<p>{p_text}</p>'
            )
        }];
    },

    getFilterItems: function () {
        return [{
            text: 'Наименование',
            kind: 'selector',
            field_name: 'p_name',
            checked: false
        }, {
            text: 'Группа',
            kind: 'selector',
            field_name: 'p_group',
            checked: false
        }, {
            text: 'Подгруппа',
            kind: 'selector',
            field_name: 'p_subgroup',
            checked: false
        }, {
            text: 'Торговая марка',
            kind: 'selector',
            field_name: 'p_trade_mark',
            checked: false
        }, {
            text: 'Производитель',
            kind: 'selector',
            field_name: 'p_manufacturer',
            checked: false
        }];
    },

    getGridColumns: function () {
        var result = [
            { text: 'Наименование', dataIndex: 'p_name', width: 200, locked: true },
            { text: 'Группа', dataIndex: 'p_group_display', width: 200 },
            { text: 'Подгруппа', dataIndex: 'p_subgroup_display', width: 200 },
            { text: 'Торговая марка', dataIndex: 'p_trade_mark', width: 200 },
            { text: 'Производитель', dataIndex: 'p_manufacturer', width: 200 }
        ];
        return this.mergeActions(result);
    },

    initComponent: function () {
        Ext.apply(this, {
            title: 'Товары'
        });
        this.callParent(arguments);

        var columns = this.items.items[0].columns;
        var height = this.fixedColumnHeaderHeight;

        if (this.overrideColumnHeaderHeight) {
            Ext.each(columns, function (item) { item.height = height; });
        }
    }
});