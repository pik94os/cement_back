Ext.define('Cement.view.products.products.bottom.Products', {
    extend: 'Cement.view.products.price.bottom.NewProducts',
    alias: 'widget.products_products_bottom_products',
    gridStore: 'products.products.bottom.Products',
    gridStateId: 'stateProductsProductsBottomProducts'
});