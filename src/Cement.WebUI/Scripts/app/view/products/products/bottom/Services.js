Ext.define('Cement.view.products.products.bottom.Services', {
    extend: 'Cement.view.products.price.bottom.NewServices',
    alias: 'widget.products_products_bottom_services',
    gridStore: 'products.products.bottom.Services',
    gridStateId: 'stateProductsProductsBottomServices'
});