Ext.define('Cement.view.products.products.lists.Personal', {
    extend: 'Cement.view.products.products.lists.Moderation',
    title: 'Персональный каталог',
    alias: 'widget.products_products_list_personal',
    gridStore: 'products.products.ProductsPersonal',
    gridStateId: 'stateProductsProductsPersonalList',
    archiveItemUrl: Cement.Config.url.products.products.archiveItem,
    showCreateButton: false,

    getActionColumns: function () {
        return {
            xtype: 'rowactions',
            hideable: false,
            resizeable: false,
            locked: true,
            width: 46,
            actions: [
                {
                    iconCls: 'icon-archive-item',
                    qtip: 'В архив',
                    callback: this.archiveItem
                }
            ],
            keepSelection: true
        };
    },

    getGridColumns: function () {
        var columns = this.callParent(arguments);

        return this.mergeActions(columns);
    }
});