Ext.define('Cement.view.products.products.lists.General', {
    extend: 'Cement.view.products.products.lists.Moderation',
    title: 'Общий каталог',
    alias: 'widget.products_products_list_general',
    gridStore: 'products.products.ProductsGeneral',
    gridStateId: 'stateProductsProductsGeneralList',
    toPersonalCatalogUrl: Cement.Config.url.products.products.toPersonal,
    showCreateButton: false,

    getActionColumns: function () {
        return {
            xtype: 'rowactions',
            hideable: false,
            resizeable: false,
            locked: true,
            width: 46,
            actions: [
                {
                    iconCls: 'icon-add-personal-catalog-item',
                    qtip: 'Добавить в персональный каталог',
                    callback: this.toPersonalCatalog
                }
            ],
            keepSelection: true
        };
    },

    toPersonalCatalog: function (grid, record) {
        if (record) {
            Ext.Msg.confirm('Внимание', 'Вы действительно хотите добавить элемент в персональный каталог?', function (btn) {
                if (btn == 'yes') {
                    Ext.Ajax.request({
                        url: Ext.String.format(grid.up('basicgrid').toPersonalCatalogUrl, record.get('id')),
                        method: 'POST',
                        success: function (response) {
                            var json = Ext.JSON.decode(response.responseText);
                            if (json.success) {
                                Cement.Msg.info(json.msg);
                            }
                            else {
                                Cement.Msg.error(json.msg);
                            }
                        },
                        failure: function () {
                            Cement.Msg.error('Ошибка при отправке в персональный каталог');
                        }
                    });
                }
            });
        }
    },

    getGridColumns: function () {
        var columns = this.callParent(arguments);

        return this.mergeActions(columns);
    }
});