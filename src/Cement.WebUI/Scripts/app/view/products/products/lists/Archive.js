Ext.define('Cement.view.products.products.lists.Archive', {
    extend: 'Cement.view.products.products.lists.Moderation',
    title: 'Архив',
    alias: 'widget.products_products_list_archive',
    gridStore: 'products.products.ProductsArchive',
    gridStateId: 'stateProductsProductsArchiveList',
    unArchiveItemUrl: Cement.Config.url.products.products.unArchiveItem,
    deleteUrl: Cement.Config.url.products.products.deleteItem,
    showCreateButton: false,

    getActionColumns: function () {
        return {
            xtype: 'rowactions',
            hideable: false,
            resizeable: false,
            locked: true,
            width: 46,
            actions: [
                {
                    iconCls: 'icon-unarchive-item',
                    qtip: 'Из архива',
                    callback: this.unArchiveItem
                },
                {
                    iconCls: 'icon-delete-item',
                    qtip: 'Удалить',
                    callback: this.deleteItem
                }
            ],
            keepSelection: true
        };
    },

    getGridColumns: function () {
        var columns = this.callParent(arguments);

        return this.mergeActions(columns);
    }
});