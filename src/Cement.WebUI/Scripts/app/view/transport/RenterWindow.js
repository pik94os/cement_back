Ext.define('Cement.view.transport.RenterWindow', {
	extend: 'Cement.view.common.SignWindow',
	xtype: 'transport_renter_window',
	leftFrameTitle: 'Арендатор',
	topGridTitle: 'Выбрать арендатора',
	bottomGridTitle: 'Выбранный арендатор',
	title: 'Выбор арендатора',
	structure_storeId: 'transport.auxiliary.Renters',
	hideCommentPanel: true,
	singleSelection: true,
	storeFields: ['id', 'name', 'address'],

	bbar: [{
		text: 'Сохранить',
		action: 'confirm'
	}, {
		text: 'Отмена',
		action: 'cancel'
	}],

	sign: function (send) {
		var store = this.down('*[role=sign-dst-grid]').getStore();
		if (store.getCount() > 0) {
			this.fireEvent('signitem', store.getAt(0));
		}
	},

	getTopGridFilters: function () {
		return [{
            text: 'Наименование',
            kind: 'selector',
            field_name: 'name',
            checked: true
        }, {
            text: 'Адрес',
            kind: 'selector',
            field_name: 'address',
            checked: false
        }];
	},

	addToSrcStore: function (item) {
		if (!this.dstStore.getById(item.get('id'))) {
			this.srcStore.add({
				id: item.get('id'),
				name: item.get('text'),
				address: item.get('address')
			});
		}
	},

	getTopColumnsConfig: function () {
		return [
			{
				xtype: 'rowactions',
				hideable: false,
				resizeable: false,
				width: 18,
				keepSelection: true,
				actions: [{ 
	                iconCls: 'icon-add-item', 
	                qtip: 'Добавить',
	                callback: this.addEmployee
	            }]
			},
			{ text: 'Наименование', dataIndex: 'name', flex: 1 },
			{ text: 'Адрес', dataIndex: 'address', flex: 1 }
		];
	},

	getBottomColumnsConfig: function () {
		return [
			{
				xtype: 'rowactions',
				hideable: false,
				resizeable: false,
				width: 18,
				actions: [{ 
	                iconCls: 'icon-delete-item', 
	                qtip: 'Удалить', 
	                callback: this.removeEmployee
	            }]
			},
			{ text: 'Наименование', dataIndex: 'name', flex: 1 },
			{ text: 'Адрес', dataIndex: 'address', flex: 1 }
		];
	},

	loadData: function (data) {
		if (data) {
			var fstore = Ext.getStore(this.structure_storeId);
			fstore.load({
				scope: this,
				callback: function () {
					Ext.each(data, function (item) {
						var r = fstore.getById(item);
						if (!this.dstStore) {
							this.setupEvents();
						}
						if (r) {
							this.dstStore.add({
								id: r.get('id'),
								address: r.get('address'),
								name: r.get('text')
							});
						}
					}, this);
				}
			});
		}
	},

	closeWindow: function () {
		this.hide();
	},

	initComponent: function () {
		this.callParent(arguments);
		var tree = this.down('treepanel');
		tree.collapseAll();
		tree.expandNode(tree.getRootNode()); //Store loading
		this.on('beforeclose', function () {
			this.hide();
			return false;
		});
	}
});