Ext.define('Cement.view.transport.auto.Form', {
	extend: 'Cement.view.basic.Form',
	alias: 'widget.transport_auto_form',
	fieldsDisabled: false,
	url: Cement.Config.url.transport.auto.owned.saveUrl,
	method: Cement.Config.url.transport.auto.owned.saveMethod,
	printUrl: Cement.Config.url.transport.auto.owned.printItem,

	notCollapseFieldsetIndex: 0,

	drivingCategoriesStoreId: 'transport.auxiliary.TransportDrivingCategories',
	car_options_store_Id: 'transport.auxiliary.CarOptions',
	car_types_store_Id: 'transport.auxiliary.CharTypes',
	char_classes_store_Id: 'transport.auxiliary.CharClasses',
	wheelFormulasStoreId: 'transport.auxiliary.WheelFormulas',
	columnsStoreId: 'transport.auxiliary.Columns',
	subdivisionsStoreId: 'transport.auxiliary.Subdivisions',

	categoryChange: function (combo, newValue) {
	    if (newValue == '') return;

	    var basicForm = combo.up('basicform');
		var	catTree = Ext.getStore('transport.auxiliary.TransportCategories'),
			subCatStore = basicForm.down('combo[name=p_sub_category]').getStore(),
			selected = catTree.getRootNode().findChild('id', newValue);

		basicForm.down('combo[name=p_sub_category]').clearValue();
		basicForm.down('fieldcontainer[role=markmodelspec]').enable();
		
		if (!combo.getStore().getById(newValue)) {
			if (combo.getStore().getById(newValue).get('p_show_char')) {
			    basicForm.down('fieldset[role=p_char]').show();
			}
			else {
			    basicForm.down('fieldset[role=p_char]').hide();
			}
		}
		subCatStore.removeAll();
		if (selected) {
			selected.eachChild(function (child) {
				subCatStore.add({
					id: child.get('id'),
					p_name: child.get('text')
				});
			});
		}
	},

	subCategoryChange: function (combo, newValue, oldValue) {
	    if (oldValue == null) {
	        return;
	    }

	    var basicForm = combo.up('basicform');
	    basicForm.down('textfield[name=p_mark_model_spec_display]').setValue(null);
	    basicForm.down('hidden[name=p_model]').setValue(null);
	    basicForm.down('hidden[name=p_mark_model_spec]').setValue(null);
	},

	getBasicFields: function () {
		var	catTree = Ext.getStore('transport.auxiliary.TransportCategories'),
			catStore = Ext.create('Ext.data.Store', {
				fields: ['id', 'p_name', 'p_show_char']
			}),
			subCatStore = Ext.create('Ext.data.Store', {
				fields: ['id', 'p_name']
			});
		catTree.getRootNode().eachChild(function (child) {
			catStore.add({
				id: child.get('id'),
				p_name: child.get('text'),
				p_show_char: child.get('p_show_char')
			});
		});
		return [{
				xtype: 'hiddenfield',
				name: 'id'
			},
			this.getCombo('Категория ТС', 'p_category', catStore, this.categoryChange),
			this.getCombo('Подкатегория ТС', 'p_sub_category', subCatStore, this.subCategoryChange, false, null, true),
			{
				xtype: 'fieldcontainer',
				layout: 'hbox',
				disabled: true,
				role: 'markmodelspec',
				items:[{
					xtype: 'hiddenfield',
					name: 'p_model'
				}, {
					xtype: 'hiddenfield',
					name: 'p_mark_model_spec'
				}, {
					xtype: 'textfield',
					labelWidth: 200,
					labelAlign: 'right',
					name: 'p_mark_model_spec_display',
					fieldLabel: 'Наименование',
					flex: 1,
                    readOnly: true
                }, {
                	xtype: 'button',
                	text: 'Выбрать',
                	margin: '0 0 0 4',
                	action: 'show_marks_window',
                	width: 60
                }]
			},
			{
				fieldLabel: 'Год выпуска',
				name: 'p_year'
			},
			this.getNumberField(),
			this.getFileField('Фото', 'p_img')
		];
	},

	getNumberField: function () {
		var result = {
			xtype: 'fieldcontainer',
	        fieldLabel: 'Гос. номер',
	        combineErrors: true,
	        msgTarget : 'side',
	        layout: 'hbox',
	        defaults: {
	          hideLabel: true
	        },
	        items: [{
	        	xtype: 'textfield',
	        	name: 'p_number',
	        	flex: 1,
						disabled: this.fieldsDisabled,
						allowBlank: this.allowBlankFields
	        }, {
	        	xtype: 'combo',
	        	name: 'p_number_region_code',
	        	editable: false,
	        	store: Ext.getStore('transport.auxiliary.RegionCodes'),
	        	queryMode: 'local',
	        	displayField: 'p_name',
	        	valueField: 'id',
	        	width: 60,
	        	margin: '0 0 0 5',
	        	disabled: this.fieldsDisabled,
	        	allowBlank: this.allowBlankFields
	        }, {
	        	xtype: 'combo',
	        	store: Ext.getStore('transport.auxiliary.CountryCodes'),
	        	editable: false,
	        	queryMode: 'local',
	        	displayField: 'p_name',
	        	valueField: 'id',
	        	width: 60,
	        	margin: '0 0 0 5',
	        	name: 'p_number_country_code',
	        	disabled: this.fieldsDisabled,
	        	allowBlank: this.allowBlankFields
	        }]
		};
		if (this.fieldsDisabled) {
			result = {
				fieldLabel: 'Гос. номер',
				name: 'p_number_display'
			};
		}
		return result;
	},

	/*
	 *
	 *  Регистрационные данные
	 *
	 */

	getRegFields: function () {
		return [{
			name: 'p_reg_vin',
			fieldLabel: 'VIN',
			allowBlank: this.allowBlankFields,
			disabled: this.fieldsDisabled
		}, {
			name: 'p_reg_engine',
			fieldLabel: 'Двигатель №',
			allowBlank: this.allowBlankFields,
			disabled: this.fieldsDisabled
		}, {
			name: 'p_reg_chasis',
			fieldLabel: 'Шасси №',
			allowBlank: this.allowBlankFields,
			disabled: this.fieldsDisabled
		}, {
			name: 'p_reg_body',
			fieldLabel: 'Кузов',
			allowBlank: this.allowBlankFields,
			disabled: this.fieldsDisabled
		},
			this.getCombo('Колесная формула', 'p_reg_wheel_formula', this.wheelFormulasStoreId),
			this.getCombo('Водительская категория', 'p_reg_category', this.drivingCategoriesStoreId),
		{
			name: 'p_reg_eco_class',
			fieldLabel: 'Эко класс',
			xtype: 'numberfield',
			allowBlank: this.allowBlankFields,
			hideTrigger: this.fieldsDisabled,
			disabled: this.fieldsDisabled
		},
		{
			name: 'p_reg_color',
			fieldLabel: 'Цвет',
			allowBlank: this.allowBlankFields,
			disabled: this.fieldsDisabled
		}, {
			name: 'p_reg_power',
			fieldLabel: 'Мощность',
			allowBlank: this.allowBlankFields,
			xtype: 'numberfield',
			hideTrigger: this.fieldsDisabled,
			disabled: this.fieldsDisabled
		}, {
			name: 'p_reg_engine_volume',
			fieldLabel: 'Объем двигателя',
			allowBlank: this.allowBlankFields,
			xtype: 'numberfield',
			hideTrigger: this.fieldsDisabled,
			disabled: this.fieldsDisabled
		}, {
			name: 'p_reg_max_mass',
			fieldLabel: 'Максимальная масса',
			allowBlank: this.allowBlankFields,
			xtype: 'numberfield',
			hideTrigger: this.fieldsDisabled,
			disabled: this.fieldsDisabled
		}, {
			name: 'p_reg_truck_mass',
			fieldLabel: 'Масса без нагрузки',
			allowBlank: this.allowBlankFields,
			xtype: 'numberfield',
			hideTrigger: this.fieldsDisabled,
			disabled: this.fieldsDisabled
		}];
	},

	/*
	 *
	 *   ПТС.ПСМ
	 *
	 */
	getPtsFields: function () {
		return [{
			name: 'p_pts_serie',
			fieldLabel: 'Серия',
			disabled: this.fieldsDisabled
		}, {
			name: 'p_pts_number',
			fieldLabel: 'Номер',
			disabled: this.fieldsDisabled
		}, {
			name: 'p_pts_manufacturer',
			fieldLabel: 'Производитель',
			disabled: this.fieldsDisabled
		}, {
			name: 'p_pts_date',
			fieldLabel: 'Дата',
			xtype: this.getDateFieldXType(),
			disabled: this.fieldsDisabled
		},
			this.getFileField('Скан стр. 1', 'p_pts_page1'),
			this.getFileField('Скан стр. 2', 'p_pts_page2')
		];
	},

	/*
	 *
	 *   Свидетельство
	 *
	 */
	getDocumentFields: function () {
		return [{
			name: 'p_document_serie',
			fieldLabel: 'Серия',
			disabled: this.fieldsDisabled
		}, {
			name: 'p_document_number',
			fieldLabel: 'Номер',
			disabled: this.fieldsDisabled
		}, {
			name: 'p_document_owner',
			fieldLabel: 'Собственник',
			disabled: this.fieldsDisabled
		}, {
			name: 'p_document_date',
			fieldLabel: 'Дата',
			xtype: this.getDateFieldXType(),
			disabled: this.fieldsDisabled
		},
			this.getFileField('Скан стр. 1', 'p_document_page1'),
			this.getFileField('Скан стр. 2', 'p_document_page2')
		];
	},

	/*
	 *
	 *    Характеристика
	 *
	 */
	getCharFields: function () {
		return [
			this.getCombo('Класс', 'p_char_class', this.char_classes_store_Id),
			{
				xtype: 'radiogroup',
				fieldLabel: 'Тип ТС',
		        combineErrors: true,
		        msgTarget : 'side',
		        columns: this.getCheckGroupColumnsCount(),
		        margin: '0 0 3 0',
		        defaults: {
		            hideLabel: false,
		            flex: 1
		        },
		        items: [{
		          	xtype: 'radio',
		          	name: 'p_char_ops',
		          	boxLabel: 'Одиночка',
		          	disabled: this.fieldsDisabled,
		          	value: '1',
		          	inputValue: 1,
		          	margin: '0'
		        }, {
		          	xtype: 'radio',
		          	name: 'p_char_ops',
		          	boxLabel: 'П/Прицеп',
		          	disabled: this.fieldsDisabled,
		          	value: '2',
		          	inputValue: 2,
		          	margin: '0'
		        }, {
		          	xtype: 'radio',
		          	name: 'p_char_ops',
		          	boxLabel: 'Сцепка',
		          	disabled: this.fieldsDisabled,
		          	value: '3',
		          	inputValue: 3,
		          	margin: '0'
		        }]
		    },
		    	this.getCombo('Тип кузова', 'p_char_type', this.car_types_store_Id),
		    	this.getSizeFields(),
		    {
				xtype: 'checkboxgroup',
		        fieldLabel: 'Способ загрузки',
		        columns: this.getCheckGroupColumnsCount(),
		        items: [{
		        	xtype: 'checkbox',
		        	boxLabel: 'Задний',
		        	inputValue: 1,
		        	disabled: this.fieldsDisabled,
		        	name: 'p_char_load_back'
		        }, {
		        	xtype: 'checkbox',
		        	boxLabel: 'Боковой',
		        	inputValue: 1,
		        	disabled: this.fieldsDisabled,
		        	name: 'p_char_load_left'
		        }, {
		        	xtype: 'checkbox',
		        	boxLabel: 'Верхний',
		        	inputValue: 1,
		        	disabled: this.fieldsDisabled,
		        	name: 'p_char_load_top'
		        }]
			}, {
				name: 'p_char_capacity',
				fieldLabel: 'Грузоподъем., т.',
				xtype: 'numberfield',
				hideTrigger: this.fieldsDisabled,
				disabled: this.fieldsDisabled
			}, {
				name: 'p_char_volume',
				fieldLabel: 'Объем, куб. м.',
				xtype: 'numberfield',
				hideTrigger: this.fieldsDisabled,
				disabled: this.fieldsDisabled
			},
				this.getCombo('Дополнительные опции', 'p_char_additional', this.car_options_store_Id),
			{
				name: 'p_add_additional',
				fieldLabel: 'Дополнительные характеристики',
				disabled: this.fieldsDisabled
			}]
	},

	getSizeFields: function () {
		var result = {
			xtype: 'fieldcontainer',
			fieldLabel: 'Д х Ш х В, м',
			combineErrors: true,
			msgTarget : 'side',
			layout: 'hbox',
			defaults: {
				hideLabel: false,
				xtype: 'numberfield',
				hideTrigger: this.fieldsDisabled,
				flex: 1,
				labelWidth: 12,
				labelSeparator: ''
			},
			items: [{
				name: 'p_char_length',
				disabled: this.fieldsDisabled
			}, {
				name: 'p_char_width',
				margin: '0 0 0 8',
				fieldLabel: 'x ',
				disabled: this.fieldsDisabled
			}, {
				name: 'p_char_height',
				margin: '0 0 0 8',
				fieldLabel: 'x ',
				disabled: this.fieldsDisabled
			}]
	    };
	    if (this.fieldsDisabled) {
	    	result = {
	    		fieldLabel: 'ДхШхВ, м',
	    		name: 'p_char_size'
	    	};
	    }
	    return result;
	},

	getCheckGroupColumnsCount: function () {
		var result = 3;
		if (this.fieldsDisabled) {
			result = 1;
		}
		return result;
	},

	getColumnFields: function () {
    var employeeDepartmentCombo = this.getCombo('Колонна', 'p_add_column', this.columnsStoreId, null, false, null, true, 'remote');
    employeeDepartmentCombo.labelAlign = 'right';
    if (!this.fieldsDisabled) {
      employeeDepartmentCombo.flex = 1;
      employeeDepartmentCombo.labelWidth = 100;
      return {
          xtype: 'fieldcontainer',
          layout: 'hbox',
          padding: '0 0 0 90',
          items: [employeeDepartmentCombo, {
              xtype: 'button',
              text: 'Создать',
              margin: '0 0 0 4',
              action: 'show_create_column_window',
              width: 60
          }]
      };
    }
    else {
      return employeeDepartmentCombo;
    }
  },

  getSubdivisionFields: function () {
    var employeeDepartmentCombo = this.getCombo('Подразделение', 'p_add_subdivision', this.subdivisionsStoreId);
    employeeDepartmentCombo.labelAlign = 'right';
    if (!this.fieldsDisabled) {
      employeeDepartmentCombo.flex = 1;
      employeeDepartmentCombo.labelWidth = 100;
      return {
          xtype: 'fieldcontainer',
          layout: 'hbox',
          padding: '0 0 0 90',
          items: [employeeDepartmentCombo, {
              xtype: 'button',
              text: 'Создать',
              margin: '0 0 0 4',
              action: 'show_create_subdivision_window',
              width: 60
          }]
      };
    }
    else {
      return employeeDepartmentCombo;
    }
  },

	/*
	 *
	 *   Дополнительно
	 *
	 */
	getAdditionalFields: function () {
		return [this.getSubdivisionFields(), this.getColumnFields(), {
			name: 'p_add_garage_number',
			fieldLabel: 'Гаражный номер',
			disabled: this.fieldsDisabled
		}];
	},

	getItems: function () {
		return [{
			xtype: 'container',
			padding: 0,
			border: 0,
			layout: 'hbox',
			items: [{
				xtype: 'container',
				flex: 1,
				border: 0,
				layout: 'anchor',
				defaults: {
					xtype: 'textfield',
					anchor: '100%',
					labelWidth: 200,
					labelAlign: 'right'
				},
				items: this.getBasicFields()
			}, {
				xtype: 'container',
				border: 0,
				width: 120,
				items: [{
					xtype: 'image',
					src: Cement.Config.default_photo,
					maxWidth: 83,
					maxHeight: 100,
					x: 23,
					y: 20,
					cls: 'pos-relative'
				}]
			}]
		}, {
			xtype: 'fieldset',
			collapsible: true,
			collapsed: false,
			title: 'Регистрационные данные',
			defaults: {
				xtype: 'textfield',
				anchor: '100%',
				labelWidth: 190,
				labelAlign: 'right'
			},
			items: this.getRegFields()
		}, {
			xtype: 'fieldset',
			collapsible: true,
			collapsed: false,
			title: 'ПТС.ПСМ',
			defaults: {
				xtype: 'textfield',
				anchor: '100%',
				labelWidth: 190,
				labelAlign: 'right'
			},
			items: this.getPtsFields()
		}, {
			xtype: 'fieldset',
			collapsible: true,
			collapsed: false,
			title: 'Свидетельство',
			defaults: {
				xtype: 'textfield',
				anchor: '100%',
				labelWidth: 190,
				labelAlign: 'right'
			},
			items: this.getDocumentFields()
		}, {
			xtype: 'fieldset',
			collapsible: true,
			collapsed: false,
			title: 'Характеристика',
			role: 'p_char',
			defaults: {
				xtype: 'textfield',
				anchor: '100%',
				labelWidth: 190,
				labelAlign: 'right'
			},
			items: this.getCharFields()
		}, {
			xtype: 'fieldset',
			collapsible: true,
			collapsed: false,
			title: 'Дополнительно',
			defaults: {
				xtype: 'textfield',
				anchor: '100%',
				labelWidth: 190,
				labelAlign: 'right'
			},
			items: this.getAdditionalFields()
		}]
	},

	initComponent: function () {
		// Ext.apply(this, {
		// 	bodyPadding: '20px 80px',
		// });
		Ext.getStore(this.columnsStoreId).load();
		Ext.getStore(this.subdivisionsStoreId).load();
		window.formXX = this;
		this.callParent(arguments);
		var me = this;
		if (!this.fieldsDisabled) {
			this.down('button[action=show_marks_window]').on('click', function () {
				me.createModificationWindow();
				Cement.util.Animation.createWindowAnimation(this.modificationWindow);
				me.modificationWindow.show();
			}, this);

			if (this.down('button[action=show_create_column_window]')) {
				this.down('button[action=show_create_column_window]').on('click', function () {
					me.createColumnWindow();
					me.columnWindow.show();
				});
			}

			if (this.down('button[action=show_create_subdivision_window]')) {
				this.down('button[action=show_create_subdivision_window]').on('click', function () {
					me.createSubdivisionWindow();
					me.subdivisionWindow.show();
				});
			}
		}
	},

	createSubdivisionWindow: function () {
		this.subdivisionWindow = Ext.create('Cement.view.basic.SimpleCreatorWindow', {
			createUrl: Cement.Config.url.transport.auto.create_subdivision,
		  title: 'Создать подразделение',
		  comboName: 'p_add_subdivision',
		  comboLabel: 'Подразделение',
		  storeId: this.subdivisionsStoreId,
		  parent: this
		});
	},

	createColumnWindow: function () {
		this.columnWindow = Ext.create('Cement.view.basic.SimpleCreatorWindow', {
			createUrl: Cement.Config.url.transport.auto.create_column,
		  title: 'Создать колонну',
		  comboName: 'p_add_column',
		  comboLabel: 'Колонна',
		  storeId: this.columnsStoreId,
		  parent: this
		});
	},

	createModificationWindow: function () {
	    var group = this.down('combo[name=p_category]').getValue(),
            sub_group = this.down('combo[name=p_sub_category]').getValue();

	    if (!this.modificationWindow) {
	        this.modificationWindow = Ext.create('Cement.view.transport.auto.ModificationWindow', {
	            group: group,
	            sub_group: sub_group
	        });
	        this.modificationWindow.on('modificationselected', function(modification_id, modification_name, model_id) {
	            this.down('hidden[name=p_model]').setValue(model_id);
	            this.down('hidden[name=p_mark_model_spec]').setValue(modification_id);
	            this.down('textfield[name=p_mark_model_spec_display]').setValue(modification_name);
	            this.modificationWindow.hide();
	        }, this);
	    } else {
	        this.modificationWindow.group = group;
	        this.modificationWindow.sub_group = sub_group;
	    }
	},

	afterRecordLoad: function (rec) {
		if (!this.fieldsDisabled) {
			this.createModificationWindow();
			this.modificationWindow.loadData(rec.get('p_mark_model_spec'));
			this.down('hidden[name=p_mark_model_spec]').setValue(Ext.JSON.encode(rec.get('p_mark_model_spec')));
		}
	}
});