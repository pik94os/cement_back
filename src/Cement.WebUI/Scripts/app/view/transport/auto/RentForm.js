Ext.define('Cement.view.transport.auto.RentForm', {
	extend: 'Cement.view.basic.Form',
	alias: 'widget.transport_auto_rent_form',
	fieldsDisabled: false,
	url: Cement.Config.url.transport.auto.sent_to_rent.saveUrl,
	method: Cement.Config.url.transport.auto.sent_to_rent.saveMethod,
	printUrl: Cement.Config.url.transport.auto.sent_to_rent.printItem,

	notCollapseFieldsetIndex: 0,

	getItems: function () {
		return [{
			xtype: 'container',
			padding: 0,
			border: 0,
			layout: 'hbox',
			items: [{
				xtype: 'container',
				flex: 1,
				border: 0,
				layout: 'anchor',
				defaults: {
					xtype: 'textfield',
					anchor: '100%',
					labelWidth: 200,
					labelAlign: 'right'
				},
				items: [{
					xtype: 'fieldcontainer',
					layout: 'hbox',
					items:[{
						xtype: 'hiddenfield',
						name: 'p_transport'
					}, {
						xtype: 'textfield',
						labelWidth: 200,
						labelAlign: 'right',
						name: 'p_transport_display',
						fieldLabel: 'Транспортное средство',
						flex: 1
	                }, {
	                	xtype: 'button',
	                	text: 'Выбрать',
	                	margin: '0 0 0 4',
	                	action: 'show_transport_window',
	                	width: 60
	                }]
				}, {
					xtype: 'fieldcontainer',
					layout: 'hbox',
					items:[{
						xtype: 'hiddenfield',
						name: 'p_renter'
					}, {
						xtype: 'textfield',
						labelWidth: 200,
						labelAlign: 'right',
						name: 'p_renter_display',
						fieldLabel: 'Арендатор',
						flex: 1
	                }, {
	                	xtype: 'button',
	                	text: 'Выбрать',
	                	margin: '0 0 0 4',
	                	action: 'show_renters_window',
	                	width: 60
	                }]
				}, {
					xtype: 'fieldcontainer',
					layout: 'hbox',
					items:[{
						xtype: 'hiddenfield',
						name: 'p_contract'
					}, {
						xtype: 'hiddenfield',
						name: 'p_document'
					}, {
						xtype: 'textfield',
						labelWidth: 200,
						labelAlign: 'right',
						name: 'p_document_display',
						fieldLabel: 'Основание',
						flex: 1
	                }, {
	                	xtype: 'button',
	                	text: 'Выбрать',
	                	margin: '0 0 0 4',
	                	action: 'show_documents_window',
	                	width: 60
	                }]
				}, {
					xtype: 'textfield',
					name: 'p_service_display',
					fieldLabel: 'Услуга',
					disabled: true
				}, {
					fieldLabel: 'Действует с',
					name: 'p_date_start',
					xtype: 'datefield'
				}, {
					fieldLabel: 'Действует по',
					name: 'p_date_end',
					xtype: 'datefield'
				}]
			}]
		}]
	},

	initComponent: function () {
		Ext.apply(this, {
			bodyPadding: '20px 80px'
		});
		this.callParent(arguments);
		this.down('button[action=show_renters_window]').on('click', function () {
			this.createRentersWindow();
			this.rentersWindow.show();
		}, this);
		this.down('button[action=show_documents_window]').on('click', function () {
			this.createDocumentWindow();
			this.documentsWindow.show();
		}, this);
		this.down('button[action=show_transport_window]').on('click', function () {
			this.createTransportWindow();
			this.transportWindow.show();
		}, this);
	},

	createDocumentWindow: function () {
		if (!this.documentsWindow) {
			this.documentsWindow = Ext.create('Cement.view.transport.RentDocumentWindow');
			this.documentsWindow.on('signitem', function (data, contract_id) {
				this.down('textfield[name=p_document_display]').setValue(data.get('name'));
				this.down('textfield[name=p_service_display]').setValue(data.get('service_name'));
				this.down('hidden[name=p_contract]').setValue(contract_id);
				this.down('hidden[name=p_document]').setValue(data.get('id'));
				this.documentsWindow.hide();
			}, this);
		}
	},

	createTransportWindow: function () {
		if (!this.transportWindow) {
			this.transportWindow = Ext.create('Cement.view.transport.auto.AllTransportWindow');
			this.transportWindow.on('signitem', function (data) {
				this.down('hiddenfield[name=p_transport]').setValue(data.get('id'));
				this.down('textfield[name=p_transport_display]').setValue(data.get('p_name'));
				this.transportWindow.hide();
			}, this);
		}
	},

	createRentersWindow: function () {
		if (!this.rentersWindow) {
			this.rentersWindow = Ext.create('Cement.view.transport.RenterWindow');
			this.rentersWindow.on('signitem', function (data) {
				this.down('textfield[name=p_renter_display]').setValue(data.get('name'));
				this.down('hidden[name=p_renter]').setValue(data.get('id'));
				this.rentersWindow.hide();
			}, this);
		}
	},

	afterRecordLoad: function (rec) {
		if (!this.fieldsDisabled) {
			this.createRentersWindow();
			this.createDocumentWindow();
			this.createTransportWindow();
			this.rentersWindow.loadData([rec.get('p_renter')]);
			this.documentsWindow.loadData([rec.get('p_document')]);
			this.transportWindow.loadData([rec.get('p_transport')]);
		}
	}
});