Ext.define('Cement.view.transport.auto.Owned', {
    extend: 'Cement.view.basic.Grid',
    alias: 'widget.transport_auto_owned',
    gridStore: 'transport.auto.Owned',
    gridStateId: 'stateTransportAutoOwned',

    printUrl: Cement.Config.url.transport.auto.owned.printGrid,
    helpUrl: Cement.Config.url.transport.auto.owned.help,
    deleteUrl: Cement.Config.url.transport.auto.owned.deleteUrl,
    printItemUrl: Cement.Config.url.transport.auto.owned.printItem,
    loadItemUrl: Cement.Config.url.transport.auto.owned.detailUrl,
    loadItemOnEdit: true,
    overrideColumnHeaderHeight: false,

    mixins: [
    	'Cement.view.transport.auto.ToolbarMixin'
    ],

    creatorTree: Ext.clone(Cement.Creators.transport.children[0]),
    createWindowTitle: 'Создать',

    getAdditionalButtons: function () {
        return [{
            xtype: 'button',
            text: 'Выбор ТС',
            menu: {
                xtype: 'menu',
                plain: true,
                items: this.buildAdditionalButtons()
            }
        }];
    },

    getFilterItems: function () {
        return this.buildFilterItems();
    },

    getGridColumns: function () {
        var me = this;
        var result = [
            { text: 'Категория', dataIndex: 'p_category_display', locked: true },
            { text: 'Подкатегория', dataIndex: 'p_sub_category_display', locked: true },
            {
                text: 'Транспортное средство',
                columns: [
                    { text: 'Марка, модель, модификация', dataIndex: 'p_mark_model_spec_display', width: 200 },
                    { text: 'Гос. номер', dataIndex: 'p_number_display', width: 100 }
                ]
            },
            {
                text: 'Характеристики',
                columns: [
                    { text: 'Класс', dataIndex: 'p_class_display', width: 100 },
                    { text: 'ДхШхВ, м', dataIndex: 'p_size_display', width: 100 },
                    { text: 'Г/О, т/м<sup>3</sup>', dataIndex: 'p_go_display', width: 100 },
                    { text: 'О', dataIndex: 'p_char_ops', width: 20, resizable: false, align: 'center', renderer: this.radioButtonRenderer1, tdCls: 'pseudoinput-column' },
                    { text: 'П', dataIndex: 'p_char_ops', width: 20, resizable: false, align: 'center', renderer: this.radioButtonRenderer2, tdCls: 'pseudoinput-column' },
                    { text: 'С', dataIndex: 'p_char_ops', width: 20, resizable: false, align: 'center', renderer: this.radioButtonRenderer3, tdCls: 'pseudoinput-column' },
                    { text: 'Тип кузова', dataIndex: 'p_char_type_display', width: 100 },
                    { text: 'З', dataIndex: 'p_load_back', width: 20, resizable: false, renderer: this.checkBoxRenderer, align: 'center', tdCls: 'pseudoinput-column' },
                    { text: 'Б', dataIndex: 'p_load_left', width: 20, resizable: false, renderer: this.checkBoxRenderer, align: 'center', tdCls: 'pseudoinput-column' },
                    { text: 'В', dataIndex: 'p_load_top', width: 20, resizable: false, renderer: this.checkBoxRenderer, align: 'center', tdCls: 'pseudoinput-column' },
                    { text: 'Доп. опции', dataIndex: 'p_additional_options_display', width: 100 },
                    { text: 'Доп. характеристики', dataIndex: 'p_additional_chars_display', width: 150 }
                ]
            },
            { text: 'Подразделение', dataIndex: 'p_department_display', width: 150 },
            { text: 'Колонна', dataIndex: 'p_column_display', width: 150 },
            { text: 'Гаражный номер', dataIndex: 'p_garage_number', width: 150 }
        ];

        result = this.mergeActions(result);

        Ext.each(result, function (column) {
            column.height = 2 * me.fixedColumnHeaderHeight;
        });

        return result;
    }
});