Ext.define('Cement.view.transport.auto.ViewGotInRent', {
	extend: 'Cement.view.transport.auto.ViewSentToRent',
	alias: 'widget.transport_auto_view_got_in_rent',
	firmTabName: 'Арендодатель',
	detailUrl: Cement.Config.url.transport.auto.got_in_rent.detailUrl
});