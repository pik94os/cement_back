Ext.define('Cement.view.transport.auto.ModificationWindow', {
	extend: 'Cement.view.common.SignWindow',
	xtype: 'transport_auto_modification_window',
	title: 'Выбор модификации',
	hideCommentPanel: true,
	leftFrameTitle: 'Марки и модели',
	topGridTitle: 'Модификации',
	bottomGridTitle: 'Выбранная модификация',
	singleSelection: true,
    rootVisible: true,

	bbar: [{
		text: 'Сохранить',
		action: 'confirm'
	}, {
		text: 'Отмена',
		action: 'cancel'
	}],

	group: null,
	sub_group: null,

	structure_storeId: 'transport.auxiliary.Models',
	loadRightFromTree: false,
	storeFields: ['id', 'text' ],

	getBottomColumnsConfig: function () {
		return [{
			xtype: 'rowactions',
			hideable: false,
			resizeable: false,
			width: 18,
			actions: [{
                iconCls: 'icon-delete-item',
                qtip: 'Удалить',
                callback: this.removeEmployee
            }]
		},
		{ text: 'Модификация', dataIndex: 'text', flex: 1 }];
	},

	getTopGridFilters: function () {
		return [{
            text: 'Модификация',
            kind: 'selector',
            field_name: 'text',
            checked: true
        }];
	},

	getTopColumnsConfig: function () {
		return [
			{
				xtype: 'rowactions',
				hideable: false,
				resizeable: false,
				width: 18,
				keepSelection: true,
				actions: [{
	                iconCls: 'icon-add-item',
	                qtip: 'Добавить',
	                callback: this.addEmployee
	            }]
			},
			{ text: 'Модификация', dataIndex: 'text', flex: 1 }
		];
	},

	sign: function (send) {
		var store = this.down('*[role=sign-dst-grid]').getStore();
		if (store.getCount() > 0) {
			var model = store.getAt(0),
				modification = this.down('treepanel').getSelectionModel().getSelection()[0].get('id');
			this.fireEvent('modificationselected', model.get('id'), model.get('text'), modification);
		}
	},

	closeWindow: function () {
		this.hide();
	},

	loadData: function (data) {
		if (data) {
			var fstore = Ext.getStore(this.structure_storeId);
			fstore.load({
				callback: function () {
					Ext.each(data, function (item) {
						var r = fstore.getNodeById(item);
						if (!this.dstStore) {
							this.setupEvents();
						}
						if (r) {
							this.dstStore.add({
								id: r.get('id'),
								name: r.get('text')
							});
						}
					}, this);
				},
				scope: this
			});
		}
	},

	getSrcStore: function () {
		return Ext.create('Ext.data.Store', {
			fields: this.storeFields,
			autoLoad: false,
			remoteFilter: true,
			proxy: getProxy(Cement.Config.url.transport.auto.modifications_dict, '')
		});
	},

	initComponent: function () {
		this.callParent(arguments);
		this.on('beforeclose', function () {
			this.hide();
			return false;
		});

	    var treePanel = this.down('treepanel');

	    treePanel.getStore().on('beforeload', function (store, operation) {
	        operation = operation || {};
	        operation.params = operation.params || {};

	        operation.params.p_category = this.group;
	        operation.params.p_sub_category = this.sub_group;
	    }, this);

		treePanel.on('selectionchange', function (treepanel, selected) {
			if (selected.length) {
				this.srcStore.clearFilter(true);
				this.srcStore.filter([{
					property: 'p_parent',
					value: selected[0].get('id')
				}]);
			}
		}, this);
		this.addEvents('modificationselected');
		//var tree = this.down('treepanel');
		//tree.collapseAll();
		//tree.expandNode(tree.getRootNode()); //Store loading
	}
});