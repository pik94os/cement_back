Ext.define('Cement.view.transport.auto.ToolbarMixin', {
    buildFilterItems: function () {
        return [{
            text: 'Категория',
            kind: 'selector',
            field_name: 'p_category_display',
            checked: false
        }, {
            text: 'Подкатегория',
            kind: 'selector',
            field_name: 'p_sub_category',
            checked: false
        }, {
            text: 'Марка, модель, модификация',
            kind: 'selector',
            field_name: 'p_mark_model_spec',
            checked: false
        }, {
            text: 'Гос. номер',
            kind: 'selector',
            field_name: 'p_number',
            checked: false
        }, {
            text: 'Класс',
            kind: 'selector',
            field_name: 'p_class',
            checked: false
        }, {
            text: 'ДхШхВ',
            kind: 'selector',
            field_name: 'p_size',
            checked: false
        }, {
            text: 'Г/О',
            kind: 'selector',
            field_name: 'p_go',
            checked: false
        }, {
            text: 'Тип кузова',
            kind: 'selector',
            field_name: 'p_char_type',
            checked: false
        }, {
            text: 'Доп. опции',
            kind: 'selector',
            field_name: 'p_additional_options',
            checked: false
        }, {
            text: 'Доп. характеристики',
            kind: 'selector',
            field_name: 'p_additional_chars',
            checked: false
        }, {
            text: 'Подразделение',
            kind: 'selector',
            field_name: 'p_department',
            checked: false
        }, {
            text: 'Колонна',
            kind: 'selector',
            field_name: 'p_column',
            checked: false
        }, {
            text: 'Гаражный номер',
            kind: 'selector',
            field_name: 'p_garage_number',
            checked: false
        }];
    },

    filterTransportType: function (item, checked) {
        var toolbar = item.up('toolbar'),
            grid = toolbar.up('basicgrid'),
            items = toolbar.query('menu menucheckitem[kind=transport_selector]'),
            params = [];
        if (grid.disableStoreReload) {
            return;
        }
        if (item.parentMenu.parentItem !== undefined) {
            var checked = true;
            item.parentMenu.items.each(function (si) {
                checked = checked && si.checked;
            });
            item.parentMenu.parentItem.setChecked(checked, true);
        }
        else {
            if (item.menu) {
                if (item.menu.items) {
                    item.menu.items.each(function (si) {
                        si.setChecked(item.checked, true);
                    });
                }
            }
        }
        Ext.each(items, function (it) {
            if (it.checked === true) {
                if (it.catId) {
                    if (params.indexOf(it.catId) < 0) {
                        params.push(it.catId);
                    }
                }
            }
        });
        grid.constantFilter = [{
            property: 'p_categories',
            value: params
        }];
        grid.reloadGrid();
    },

    filterTransportTypeToggle: function (item, checked) {
        var toolbar = item.up('toolbar'),
            grid = toolbar.up('basicgrid'),
            items = toolbar.query('menu menucheckitem[kind=transport_selector]');
        Ext.each(items, function (it) {
            it.setChecked(item.checked, true);
        });
        grid.filterTransportType(item);
    },

    buildAdditionalButtons: function () {
        var store = Ext.getStore('transport.auxiliary.TransportCategories'),
            items = [{
                text: 'Выделить все',
                checked: true,
                checkHandler: this.filterTransportTypeToggle
            }, '-'],
            me = this;
        store.getRootNode().eachChild(function (child) {
            item = {
                checked: true,
                checkHandler: me.filterTransportType,
                catId: child.get('id'),
                text: child.get('text'),
                kind: 'transport_selector'
            };
            if (child.hasChildNodes()) {
                item.menu = {
                    items: []
                };
                child.eachChild(function (sub_child) {
                    item.menu.items.push({
                        checked: true,
                        checkHandler: me.filterTransportType,
                        catId: sub_child.get('id'),
                        kind: 'transport_selector',
                        text: sub_child.get('text')
                    });
                });
            }
            items.push(item);
        });
        return items;
    }
});