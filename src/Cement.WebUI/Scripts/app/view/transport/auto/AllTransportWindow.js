Ext.define('Cement.view.transport.auto.AllTransportWindow', {
	extend: 'Cement.view.transport.units.CarsWindow',
	structure_storeId: 'transport.auxiliary.AllTransport',
	leftFrameTitle: 'Транспортное средство',
	topGridTitle: 'Транспортное средство',
	bottomGridTitle: 'Выбранное транспортное средство'
});