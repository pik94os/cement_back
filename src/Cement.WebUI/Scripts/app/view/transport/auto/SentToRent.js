Ext.define('Cement.view.transport.auto.SentToRent', {
    extend: 'Cement.view.basic.Grid',
    alias: 'widget.transport_auto_sent_to_rent',
    gridStore: 'transport.auto.SentToRent',
    gridStateId: 'stateTransportAutoSentToRent',

    printUrl: Cement.Config.url.transport.auto.sent_to_rent.printGrid,
    helpUrl: Cement.Config.url.transport.auto.sent_to_rent.help,
    deleteUrl: Cement.Config.url.transport.auto.sent_to_rent.deleteUrl,
    printItemUrl: Cement.Config.url.transport.auto.sent_to_rent.printItem,

    mixins: [
        'Cement.view.transport.auto.ToolbarMixin'
    ],

    getAdditionalButtons: function () {
        return [{
            xtype: 'button',
            text: 'Выбор ТС',
            menu: {
                xtype: 'menu',
                plain: true,
                items: this.buildAdditionalButtons()
            }
        }];
    },

    getFilterItems: function () {
        return this.buildFilterItems();
    },

    getActionColumns: function () {
        return {
            xtype: 'rowactions',
            hideable: false,
            resizeable: false,
            locked: true,
            width: 23,
            actions: [
                {
                    iconCls: 'icon-delete-item',
                    qtip: 'Удалить',
                    callback: this.deleteItem
                }
            ],
            keepSelection: true
        };
    },

    getGridColumns: function () {
        var result = [
            { text: 'Категория', dataIndex: 'p_category_display', locked: true },
            { text: 'Подкатегория', dataIndex: 'p_sub_category_display', locked: true },
            {
                text: 'Транспортное средство',
                columns: [
                    { text: 'Марка, модель, модификация', dataIndex: 'p_mark_model_spec_display', width: 200 },
                    { text: 'Гос. номер', dataIndex: 'p_number_display', width: 100 }
                ]
            },
            {
                text: 'Характеристики',
                columns: [
                    { text: 'Класс', dataIndex: 'p_class_display', width: 100 },
                    { text: 'ДхШхВ, м', dataIndex: 'p_size_display', width: 100 },
                    { text: 'Г/О, т/м<sup>3</sup>', dataIndex: 'p_go_display', width: 100 },
                    { text: 'О', dataIndex: 'p_char_ops', width: 20, resizable: false, align: 'center', renderer: this.radioButtonRenderer1, tdCls: 'pseudoinput-column' },
                    { text: 'П', dataIndex: 'p_char_ops', width: 20, resizable: false, align: 'center', renderer: this.radioButtonRenderer2, tdCls: 'pseudoinput-column' },
                    { text: 'С', dataIndex: 'p_char_ops', width: 20, resizable: false, align: 'center', renderer: this.radioButtonRenderer3, tdCls: 'pseudoinput-column' },
                    { text: 'Тип кузова', dataIndex: 'p_char_type_display', width: 100 },
                    { text: 'З', dataIndex: 'p_load_back', width: 20, resizable: false, renderer: this.checkBoxRenderer, align: 'center', tdCls: 'pseudoinput-column' },
                    { text: 'Б', dataIndex: 'p_load_left', width: 20, resizable: false, renderer: this.checkBoxRenderer, align: 'center', tdCls: 'pseudoinput-column' },
                    { text: 'В', dataIndex: 'p_load_top', width: 20, resizable: false, renderer: this.checkBoxRenderer, align: 'center', tdCls: 'pseudoinput-column' },
                    { text: 'Доп. опции', dataIndex: 'p_additional_options_display', width: 100 },
                    { text: 'Доп. характеристики', dataIndex: 'p_additional_chars_display', width: 150 }
                ]
            },
            { text: 'Подразделение', dataIndex: 'p_department_display', width: 150 },
            { text: 'Колонна', dataIndex: 'p_column_display', width: 150 },
            { text: 'Гаражный номер', dataIndex: 'p_garage_number', width: 150 },
            {
                text: 'Арендатор',
                columns: [
                    { text: 'Наименование', dataIndex: 'p_rent_name', width: 120 },
                    { text: 'Адрес', dataIndex: 'p_rent_address', width: 120 },
                    { text: 'Действует с', dataIndex: 'p_rent_from', width: 120 },
                    { text: 'Действует по', dataIndex: 'p_rent_to', width: 120 }
                ]
            },
            {
                text: 'Услуга',
                columns: [
                    { text: 'Наименование', dataIndex: 'p_rent_service_name', width: 120 },
                    { text: 'Ед. изм', dataIndex: 'p_rent_service_unit_display', width: 50 },
                    { text: 'Кол-во', dataIndex: 'p_rent_service_count', width: 50 },
                    { text: 'Факт. к.', dataIndex: 'p_rent_service_fact_count', width: 50 },
                    { text: 'Цена', dataIndex: 'p_rent_service_price', width: 50 },
                    { text: 'Налог', dataIndex: 'p_rent_service_tax', width: 50 },
                    { text: 'Сумма', dataIndex: 'p_rent_service_sum', width: 50 }
                ]
            }
        ];

        return this.mergeActions(result);
    }
    
});