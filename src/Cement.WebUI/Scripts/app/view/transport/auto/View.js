Ext.define('Cement.view.transport.auto.View', {
	extend: 'Cement.view.transport.auto.Form',
	alias: 'widget.transport_auto_view',
	fieldsDisabled: true,
	buttons: null,
	border: 0,
	tbar: Cement.Config.modelViewButtons,
	layout: 'autocontainer',
	bbar: null,
	noUserFieldsetsControl: false,
	bodyCls: 'model-form',
	detailUrl: Cement.Config.url.transport.auto.owned.detailUrl,

	getBasicFields: function () {
		var result = [{
				xtype: 'hiddenfield',
				name: 'id'
			},
			this.getCombo('Марка', 'p_mark', this.car_marks_store_Id, this.markChanged),
			this.getCombo('Модель', 'p_model', this.car_models_store_Id, this.modelChanged),
			this.getCombo('Модификация', 'p_modification', this.car_versions_store_Id),
			{
				xtype: this.getDateFieldXType(),
				fieldLabel: 'Год выпуска',
				name: 'p_year'
			},
			this.getNumberField(),
			this.getFileField('Фото', 'p_img')
		];
		return result;
	},

	getItems: function () {
		return [{
			xtype: 'cementimage'
		}, {
			xtype: 'panel',
			padding: 0,
			border: 0,
			layout: 'hbox',
			items: [{
				xtype: 'panel',
				flex: 1,
				border: 0,
				layout: 'anchor',
				defaults: {
					xtype: 'textfield',
					anchor: '100%'
				},
				items: this.getBasicFields()
			}]
		}, {
			xtype: 'fieldset',
			title: 'Регистрационные данные',
			layout: 'anchor',
			defaults: {
				xtype: 'textfield',
				anchor: '100%'
			},
			collapsible: true,
			collapsed: true,
			items: this.getRegFields()
		}, {
			xtype: 'fieldset',
			title: 'ПТС.ПСМ',
			layout: 'anchor',
			defaults: {
				xtype: 'textfield',
				anchor: '100%'
			},
			collapsible: true,
			collapsed: true,
			items: this.getPtsFields()
		}, {
			xtype: 'fieldset',
			title: 'Свидетельство',
			layout: 'anchor',
			defaults: {
				xtype: 'textfield',
				anchor: '100%'
			},
			collapsible: true,
			collapsed: true,
			items: this.getDocumentFields()
		},
			this.getViewCharFieldSet(), {
			xtype: 'fieldset',
			title: 'Дополнительно',
			layout: 'anchor',
			defaults: {
				xtype: 'textfield',
				anchor: '100%'
			},
			collapsible: true,
			collapsed: true,
			items: this.getAdditionalFields()
		}];
	},

	getViewCharFieldSet: function () {
		return {
			xtype: 'fieldset',
			title: 'Характеристика',
			layout: 'anchor',
			role: 'char_display',
			defaults: {
				xtype: 'textfield',
				anchor: '100%'
			},
			collapsible: true,
			collapsed: true,
			items: this.getCharFields()
		};
	},

	initComponent: function ()  {
		Ext.apply(this, {
			items: [{
				xtype: 'panel',
				border: 0,
				layout: 'anchor',
				defaultType: 'textfield',
				anchor: '100%',
				bodyPadding: '10 10 10 5',
				autoScroll: true,
				defaults: {
					anchor: '100%',
					labelWidth: 110
				},
				items: this.getItems()
			}]
		});

		this.callParent(arguments);
		this.setupImgCentering();
	},

	loadRecord: function (rec) {
		if (!rec.get) return;
		var me = this;
		me.setLoading(true);
		Ext.Ajax.request({
			url: Ext.String.format(me.detailUrl, rec.get('id')),
			success: function (response) {
				var r = Ext.create('Cement.model.transport.FullInfo', Ext.JSON.decode(response.responseText));
				me.getForm().loadRecord(r);
				if (r.get('show_char')) {
					me.down('fieldset[role=char_display]').show();
				}
				else {
					me.down('fieldset[role=char_display]').hide();
				}
				me.setLoading(false);
      }
    });
	}
});