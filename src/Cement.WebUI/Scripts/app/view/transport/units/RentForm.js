Ext.define('Cement.view.transport.units.RentForm', {
	extend: 'Cement.view.basic.Form',
	alias: 'widget.transport_units_rent_form',
	fieldsDisabled: false,
	url: Cement.Config.url.transport.units.sent_to_rent.saveUrl,
	method: Cement.Config.url.transport.units.sent_to_rent.saveMethod,
	printUrl: Cement.Config.url.transport.units.sent_to_rent.printItem,

	getItems: function (argument) {
		return [{
			xtype: 'container',
			layout: 'anchor',
			defaults: {
				xtype: 'textfield',
				anchor: '100%',
				labelWidth: 190,
				labelAlign: 'right'
			},
			items: [{
				xtype: 'fieldcontainer',
				layout: 'hbox',
				items:[{
					xtype: 'hiddenfield',
					name: 'p_transport_unit'
				}, {
					xtype: 'textfield',
					labelWidth: 190,
					labelAlign: 'right',
					name: 'p_transport_unit_display',
					fieldLabel: 'Транспортная единица',
					flex: 1
                }, {
                	xtype: 'button',
                	text: 'Выбрать',
                	margin: '0 0 0 4',
                	action: 'show_transport_window',
                	width: 60
                }]
			}, {
				xtype: 'fieldcontainer',
				layout: 'hbox',
				items:[{
					xtype: 'hiddenfield',
					name: 'p_renter'
				}, {
					xtype: 'textfield',
					labelWidth: 190,
					labelAlign: 'right',
					name: 'p_renter_display',
					fieldLabel: 'Организация',
					flex: 1
                }, {
                	xtype: 'button',
                	text: 'Выбрать',
                	margin: '0 0 0 4',
                	action: 'show_renters_window',
                	width: 60
                }]
			}, {
				xtype: 'fieldcontainer',
				layout: 'hbox',
				items:[{
					xtype: 'hiddenfield',
					name: 'p_contract'
				}, {
					xtype: 'hiddenfield',
					name: 'p_document'
				}, {
					xtype: 'textfield',
					labelWidth: 190,
					labelAlign: 'right',
					name: 'p_document_display',
					fieldLabel: 'Основание',
					flex: 1
                }, {
                	xtype: 'button',
                	text: 'Выбрать',
                	margin: '0 0 0 4',
                	action: 'show_documents_window',
                	width: 60
                }]
			}, {
				fieldLabel: 'Действует с',
				name: 'p_date_start',
				allowBlank: false,
				xtype: 'datefield'
			}, {
				fieldLabel: 'Действует по',
				name: 'p_date_end',
				allowBlank: false,
				xtype: 'datefield'
			}]
		}];
	},

	initComponent: function () {
		this.callParent(arguments);
		this.down('button[action=show_renters_window]').on('click', function () {
			this.createRentersWindow();
			this.rentersWindow.show();
		}, this);
		this.down('button[action=show_documents_window]').on('click', function () {
			this.createDocumentWindow();
			this.documentsWindow.show();
		}, this);
		this.down('button[action=show_transport_window]').on('click', function () {
			this.createTransportWindow();
			this.transportWindow.show();
		}, this);
	},

	createDocumentWindow: function () {
		if (!this.documentsWindow) {
			this.documentsWindow = Ext.create('Cement.view.transport.RentDocumentWindow');
			this.documentsWindow.on('signitem', function (data, contract_id) {
				this.down('textfield[name=p_document_display]').setValue(data.get('name'));
				this.down('hidden[name=p_contract]').setValue(contract_id);
				this.down('hidden[name=p_document]').setValue(data.get('id'));
				this.documentsWindow.hide();
			}, this);
		}
	},

	createTransportWindow: function () {
		if (!this.transportWindow) {
			this.transportWindow = Ext.create('Cement.view.transport.units.AllUnitsWindow');
			this.transportWindow.on('signitem', function (data) {
				this.down('hiddenfield[name=p_transport_unit]').setValue(data.get('id'));
				this.down('textfield[name=p_transport_unit_display]').setValue(data.get('p_name'));
				this.transportWindow.hide();
			}, this);
		}
	},

	createRentersWindow: function () {
		if (!this.rentersWindow) {
			this.rentersWindow = Ext.create('Cement.view.transport.RenterWindow');
			this.rentersWindow.on('signitem', function (data) {
				this.down('textfield[name=p_renter_display]').setValue(data.get('name'));
				this.down('hidden[name=p_renter]').setValue(data.get('id'));
				this.rentersWindow.hide();
			}, this);
		}
	},

	afterRecordLoad: function (rec) {
		if (!this.fieldsDisabled) {
			this.createRentersWindow();
			this.createDocumentWindow();
			this.rentersWindow.loadData([rec.get('p_renter')]);
			this.documentsWindow.loadData([rec.get('p_document')]);
		}
	}
});