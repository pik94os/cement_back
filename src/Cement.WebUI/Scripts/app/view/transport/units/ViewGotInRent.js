Ext.define('Cement.view.transport.units.ViewGotInRent', {
	extend: 'Cement.view.transport.units.ViewSentToRent',
	alias: 'widget.transport_unit_view_got_in_rent',
	firmTitle: 'Получено в управление',
	detailUrl: Cement.Config.url.transport.units.got_in_rent.detailUrl
});