Ext.define('Cement.view.transport.units.AllUnitsWindow', {
	extend: 'Cement.view.transport.units.CarsWindow',
	xtype: 'transport_units_all_units_window',
	title: 'Выбор транспортной единицы',
	hideCommentPanel: true,
	leftFrameTitle: 'Транспортная единица',
	topGridTitle: 'Выбрать транспортное средство',
	bottomGridTitle: 'Выбранная транспортная единица',
	mixins: [
		'Cement.view.basic.GridRenderers'
	],

	structure_storeId: 'transport.auxiliary.AllUnits',

	getBottomColumnsConfig: function () {
		return [{
			xtype: 'rowactions',
			hideable: false,
			resizeable: false,
			locked: true,
			width: 18,
			actions: [{ 
                iconCls: 'icon-delete-item', 
                qtip: 'Удалить', 
                callback: this.removeEmployee
            }]
		}, { text: 'Наименование', dataIndex: 'p_name', locked: true },
            {
                text: 'Транспортное средство',
                columns: [
                    { text: 'Марка, модель', dataIndex: 'p_auto_mark_model', width: 200 },
                    { text: 'Гос. номер', dataIndex: 'p_auto_number', width: 200 }
                ]
            },
            {
                text: 'Прицепное транспортное средство',
                columns: [
                    { text: 'Марка, модель', dataIndex: 'p_auto_trailer', width: 200 },
                    { text: 'Гос. номер', dataIndex: 'p_auto_trailer_number', width: 200 }
                ]
            },
            {
                text: 'Водитель',
                columns: [
                    { text: 'Фамилия', dataIndex: 'p_driver_surname', width: 150 },
                    { text: 'Имя', dataIndex: 'p_driver_name', width: 150 },
                    { text: 'Отчество', dataIndex: 'p_driver_lastname', width: 150 }
                ]
            },
            {
                text: 'Характеристики',
                columns: [
                    { text: 'Класс', dataIndex: 'p_class_display', width: 100 },
                    { text: 'ДхШхВ, м', dataIndex: 'p_size_display', width: 100 },
                    { text: 'Г/О, т/м<sup>3</sup>', dataIndex: 'p_go_display', width: 100 },
                    { text: 'О', dataIndex: 'p_char_ops', width: 20, resizable: false, align: 'center', renderer: this.radioButtonRenderer1, tdCls: 'pseudoinput-column' },
                    { text: 'П', dataIndex: 'p_char_ops', width: 20, resizable: false, align: 'center', renderer: this.radioButtonRenderer2, tdCls: 'pseudoinput-column' },
                    { text: 'С', dataIndex: 'p_char_ops', width: 20, resizable: false, align: 'center', renderer: this.radioButtonRenderer3, tdCls: 'pseudoinput-column' },
                    { text: 'Тип кузова', dataIndex: 'p_char_type_display', width: 100 },
                    { text: 'З', dataIndex: 'p_load_back', width: 20, resizable: false, renderer: this.checkBoxRenderer, align: 'center', tdCls: 'pseudoinput-column' },
                    { text: 'Б', dataIndex: 'p_load_left', width: 20, resizable: false, renderer: this.checkBoxRenderer, align: 'center', tdCls: 'pseudoinput-column' },
                    { text: 'В', dataIndex: 'p_load_top', width: 20, resizable: false, renderer: this.checkBoxRenderer, align: 'center', tdCls: 'pseudoinput-column' },
                    { text: 'Доп. опции', dataIndex: 'p_additional_options_display', width: 100 },
                    { text: 'Доп. характеристики', dataIndex: 'p_additional_chars_display', width: 150 }
                ]
            },
            {
                text: 'Переданные в управление',
                columns: [
                    { text: 'Наименование', dataIndex: 'p_control_sent_name', width: 150 },
                    { text: 'Адрес', dataIndex: 'p_control_sent_address', width: 200 },
                    { text: 'Действует с', dataIndex: 'p_control_date_start', width: 90 },
                    { text: 'Действует по', dataIndex: 'p_control_date_end', width: 90 }
                ]
            }];
	},

	getTopGridFilters: function () {
        return Ext.create('Cement.view.transport.auto.ToolbarMixin').buildFilterItems();
	},

	getTopColumnsConfig: function () {
		return [
			{
				xtype: 'rowactions',
				hideable: false,
				resizeable: false,
				width: 18,
				keepSelection: true,
				locked: true,
				actions: [{ 
	                iconCls: 'icon-add-item', 
	                qtip: 'Добавить',
	                callback: this.addEmployee
	            }]
			},
			{ text: 'Наименование', dataIndex: 'p_name', locked: true },
            {
                text: 'Транспортное средство',
                columns: [
                    { text: 'Марка, модель', dataIndex: 'p_auto_mark_model', width: 200 },
                    { text: 'Гос. номер', dataIndex: 'p_auto_number', width: 200 }
                ]
            },
            {
                text: 'Прицепное транспортное средство',
                columns: [
                    { text: 'Марка, модель', dataIndex: 'p_auto_trailer', width: 200 },
                    { text: 'Гос. номер', dataIndex: 'p_auto_trailer_number', width: 200 }
                ]
            },
            {
                text: 'Водитель',
                columns: [
                    { text: 'Фамилия', dataIndex: 'p_driver_surname', width: 150 },
                    { text: 'Имя', dataIndex: 'p_driver_name', width: 150 },
                    { text: 'Отчество', dataIndex: 'p_driver_lastname', width: 150 }
                ]
            },
            {
                text: 'Характеристики',
                columns: [
                    { text: 'Класс', dataIndex: 'p_class_display', width: 100 },
                    { text: 'ДхШхВ, м', dataIndex: 'p_size_display', width: 100 },
                    { text: 'Г/О, т/м<sup>3</sup>', dataIndex: 'p_go_display', width: 100 },
                    { text: 'О', dataIndex: 'p_char_ops', width: 20, resizable: false, align: 'center', renderer: this.radioButtonRenderer1, tdCls: 'pseudoinput-column' },
                    { text: 'П', dataIndex: 'p_char_ops', width: 20, resizable: false, align: 'center', renderer: this.radioButtonRenderer2, tdCls: 'pseudoinput-column' },
                    { text: 'С', dataIndex: 'p_char_ops', width: 20, resizable: false, align: 'center', renderer: this.radioButtonRenderer3, tdCls: 'pseudoinput-column' },
                    { text: 'Тип кузова', dataIndex: 'p_char_type_display', width: 100 },
                    { text: 'З', dataIndex: 'p_load_back', width: 20, resizable: false, renderer: this.checkBoxRenderer, align: 'center', tdCls: 'pseudoinput-column' },
                    { text: 'Б', dataIndex: 'p_load_left', width: 20, resizable: false, renderer: this.checkBoxRenderer, align: 'center', tdCls: 'pseudoinput-column' },
                    { text: 'В', dataIndex: 'p_load_top', width: 20, resizable: false, renderer: this.checkBoxRenderer, align: 'center', tdCls: 'pseudoinput-column' },
                    { text: 'Доп. опции', dataIndex: 'p_additional_options_display', width: 100 },
                    { text: 'Доп. характеристики', dataIndex: 'p_additional_chars_display', width: 150 }
                ]
            },
            {
                text: 'Переданные в управление',
                columns: [
                    { text: 'Наименование', dataIndex: 'p_control_sent_name', width: 150 },
                    { text: 'Адрес', dataIndex: 'p_control_sent_address', width: 200 },
                    { text: 'Действует с', dataIndex: 'p_control_date_start', width: 90 },
                    { text: 'Действует по', dataIndex: 'p_control_date_end', width: 90 }
                ]
            }
		];
	}
});