Ext.define('Cement.view.transport.units.DriversWindow', {
	extend: 'Cement.view.common.SignWindow',
	xtype: 'transport_units_drivers_window',
	hideCommentPanel: true,
	leftFrameTitle: 'Водители',
	topGridTitle: 'Выбрать водителя',
	bottomGridTitle: 'Выбранный водитель',
	storeFields: ['id', 'p_name', 'p_position', 'p_department' ],
	structure_storeId: 'transport.auxiliary.AllDrivers',
	displayField: 'p_name',
	bbar: [{
		text: 'Сохранить',
		action: 'confirm'
	}, {
		text: 'Отмена',
		action: 'cancel'
	}],
	singleSelection: true,

	sign: function (send) {
		var store = this.down('*[role=sign-dst-grid]').getStore();
		if (store.getCount() > 0) {
			this.fireEvent('signitem', store.getAt(0));
		}
	},

	getBottomColumnsConfig: function () {
		return [
			{
				xtype: 'rowactions',
				hideable: false,
				resizeable: false,
				width: 18,
				actions: [{ 
	                iconCls: 'icon-remove-people', 
	                qtip: 'Удалить', 
	                callback: this.removeEmployee
	            }]
			},
			{ text: 'ФИО', dataIndex: 'p_name', flex: 1 },
			{ text: 'Должность', dataIndex: 'p_position', flex: 1 },
			{ text: 'Отдел', dataIndex: 'p_department', flex: 1 }
		];
	},

	getTopColumnsConfig: function () {
		return [
			{
				xtype: 'rowactions',
				hideable: false,
				resizeable: false,
				width: 18,
				keepSelection: true,
				actions: [{ 
	                iconCls: 'icon-add-people', 
	                qtip: 'Добавить',
	                callback: this.addEmployee
	            }]
			},
			{ text: 'ФИО', dataIndex: 'p_name', flex: 1 },
			{ text: 'Должность', dataIndex: 'p_position', flex: 1 },
			{ text: 'Отдел', dataIndex: 'p_department', flex: 1 }
		];
	},

	addToSrcStore: function (item) {
		if (!this.dstStore.getById(item.get('id'))) {
			this.srcStore.add(item.copy());
		}
	},

	initComponent: function () {
		this.callParent(arguments);
		this.on('beforeclose', function () {
			this.hide();
			return false;
		});
		this.down('treepanel').on('itemclick', function (tree, record) {
			var items = [];
			this.srcStore.removeAll();
			if (record.get('leaf')) {
				items.push(record);
			}
			else {
				Ext.each(record.childNodes, function (child) {
					if (child.get('leaf')) {
						items.push(child);
					}
				});
			}
			Ext.each(items, function (item) {
				this.addToSrcStore(item);
			}, this);
		}, this);
		this.addEvents('modificationselected');
		//var tree = this.down('treepanel');
		//tree.collapseAll();
		//tree.expandNode(tree.getRootNode()); //Store loading
	}
});