Ext.define('Cement.view.transport.units.TrailersWindow', {
	extend: 'Cement.view.transport.units.CarsWindow',
	structure_storeId: 'transport.auxiliary.AllTrailers',
	leftFrameTitle: 'Прицепное транспортное средство',
	topGridTitle: 'Выбрать прицепное средство',
	bottomGridTitle: 'Выбранное прицепное транспортное средство'
});