Ext.define('Cement.view.transport.units.Form', {
	extend: 'Cement.view.basic.Form',
	alias: 'widget.transport_units_form',
	fieldsDisabled: false,
	url: Cement.Config.url.transport.units.new_units.saveUrl,
	method: Cement.Config.url.transport.units.new_units.saveMethod,
	printUrl: Cement.Config.url.transport.units.new_units.printItem,

	getItems: function (argument) {
		return [{
			xtype: 'container',
			layout: 'anchor',
			defaults: {
				xtype: 'textfield',
				anchor: '100%',
				labelWidth: 190,
				labelAlign: 'right'
			},
			items: [{
				fieldLabel: 'Наименование',
				name: 'p_name',
				allowBlank: false
			}, {
				fieldLabel: 'Дата начала',
				name: 'p_date_start',
				allowBlank: false,
				xtype: 'datefield'
			}, {
				xtype: 'fieldcontainer',
				layout: 'hbox',
				items:[{
					xtype: 'hiddenfield',
					name: 'p_transport'
				}, {
					xtype: 'textfield',
					labelWidth: 190,
					labelAlign: 'right',
					name: 'p_transport_display',
					fieldLabel: 'Транспортное средство',
					flex: 1
                }, {
                	xtype: 'button',
                	text: 'Выбрать',
                	margin: '0 0 0 4',
                	action: 'show_transport_window',
                	width: 60
                }]
			}, {
				xtype: 'fieldcontainer',
				layout: 'hbox',
				items:[{
					xtype: 'hiddenfield',
					name: 'p_trailer'
				}, {
					xtype: 'textfield',
					labelWidth: 190,
					labelAlign: 'right',
					name: 'p_trailer_display',
					fieldLabel: 'Прицепное транспортное средство',
					flex: 1
                }, {
                	xtype: 'button',
                	text: 'Выбрать',
                	margin: '0 0 0 4',
                	action: 'show_trailer_window',
                	width: 60
                }]
			}, {
				xtype: 'fieldcontainer',
				layout: 'hbox',
				items:[{
					xtype: 'hiddenfield',
					name: 'p_driver'
				}, {
					xtype: 'textfield',
					labelWidth: 190,
					labelAlign: 'right',
					name: 'p_driver_display',
					fieldLabel: 'Водитель',
					flex: 1
                }, {
                	xtype: 'button',
                	text: 'Выбрать',
                	margin: '0 0 0 4',
                	action: 'show_driver_window',
                	width: 60
                }]
			}]
		}];
	},

	initComponent: function () {
		this.callParent(arguments);
		this.down('button[action=show_transport_window]').on('click', function () {
			this.createTransportWindow();
			this.transportWindow.show();
		}, this);
		this.down('button[action=show_trailer_window]').on('click', function () {
			this.createTrailerWindow();
			this.trailerWindow.show();
		}, this);
		this.down('button[action=show_driver_window]').on('click', function () {
			this.createDriverWindow();
			this.driverWindow.show();
		}, this);
	},

	createTransportWindow: function () {
		if (!this.transportWindow) {
			this.transportWindow = Ext.create('Cement.view.transport.units.CarsWindow');
			this.transportWindow.on('signitem', function (model) {
				this.down('hiddenfield[name=p_transport]').setValue(model.get('id'));
				this.down('textfield[name=p_transport_display]').setValue(model.get('p_name'));
				this.transportWindow.hide();
			}, this);
			this.transportWindow.show();
		}
	},

	createTrailerWindow: function () {
		if (!this.trailerWindow) {
			this.trailerWindow = Ext.create('Cement.view.transport.units.TrailersWindow');
			this.trailerWindow.on('signitem', function (model) {
				this.down('hiddenfield[name=p_trailer]').setValue(model.get('id'));
				this.down('textfield[name=p_trailer_display]').setValue(model.get('p_name'));
				this.trailerWindow.hide();
			}, this);
			this.trailerWindow.show();
		}
	},

	createDriverWindow: function () {
		if (!this.driverWindow) {
			this.driverWindow = Ext.create('Cement.view.transport.units.DriversWindow');
			this.driverWindow.on('signitem', function (model) {
				this.down('hiddenfield[name=p_driver]').setValue(model.get('id'));
				this.down('textfield[name=p_driver_display]').setValue(model.get('p_name'));
				this.driverWindow.hide();
			}, this);
			this.driverWindow.show();
		}
	},

	afterRecordLoad: function (rec) {
		this.createTransportWindow();
		this.createTrailerWindow();
		this.createDriverWindow();
		this.transportWindow.loadData([rec.get('id')]);
		this.trailerWindow.loadData([rec.get('id')]);
		this.driverWindow.loadData([rec.get('id')]);
	}
});