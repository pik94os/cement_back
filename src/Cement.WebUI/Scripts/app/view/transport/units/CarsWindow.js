Ext.define('Cement.view.transport.units.CarsWindow', {
	extend: 'Cement.view.common.SignWindow',
	xtype: 'transport_units_cars_window',
	title: 'Выбор транспортного средства',
	hideCommentPanel: true,
	leftFrameTitle: 'Транспортное средство',
	topGridTitle: 'Выбрать транспортное средство',
	bottomGridTitle: 'Выбранное транспортное средство',

	bbar: [{
		text: 'Сохранить',
		action: 'confirm'
	}, {
		text: 'Отмена',
		action: 'cancel'
	}],
	width: 900,
	height: 550,

	mixins: [
		'Cement.view.basic.GridRenderers'
	],

	structure_storeId: 'transport.auxiliary.AllCars',
	loadRightFromTree: false,
	singleSelection: true,
	displayField: 'p_name',

	getBottomColumnsConfig: function () {
		return [{
			xtype: 'rowactions',
			hideable: false,
			resizeable: false,
			locked: true,
			width: 18,
			actions: [{ 
                iconCls: 'icon-delete-item', 
                qtip: 'Удалить', 
                callback: this.removeEmployee
            }]
		},
		{ text: 'Категория', dataIndex: 'p_category_display', locked: true },
        { text: 'Подкатегория', dataIndex: 'p_sub_category_display', locked: true },
        {
            text: 'Транспортное средство',
            columns: [
                { text: 'Марка, модель, модификация', dataIndex: 'p_mark_model_spec_display', width: 200 },
                { text: 'Гос. номер', dataIndex: 'p_number_display', width: 100 }
            ]
        },
        {
            text: 'Характеристики',
            columns: [
                { text: 'Класс', dataIndex: 'p_class_display', width: 100 },
                { text: 'ДхШхВ, м', dataIndex: 'p_size_display', width: 100 },
                { text: 'Г/О, т/м<sup>3</sup>', dataIndex: 'p_go_display', width: 100 },
                { text: 'О', dataIndex: 'p_char_ops', width: 20, resizable: false, align: 'center', renderer: this.radioButtonRenderer1, tdCls: 'pseudoinput-column' },
                { text: 'П', dataIndex: 'p_char_ops', width: 20, resizable: false, align: 'center', renderer: this.radioButtonRenderer2, tdCls: 'pseudoinput-column' },
                { text: 'С', dataIndex: 'p_char_ops', width: 20, resizable: false, align: 'center', renderer: this.radioButtonRenderer3, tdCls: 'pseudoinput-column' },
                { text: 'Тип кузова', dataIndex: 'p_char_type_display', width: 100 },
                { text: 'З', dataIndex: 'p_load_back', width: 20, resizable: false, renderer: this.checkBoxRenderer, align: 'center', tdCls: 'pseudoinput-column' },
                { text: 'Б', dataIndex: 'p_load_left', width: 20, resizable: false, renderer: this.checkBoxRenderer, align: 'center', tdCls: 'pseudoinput-column' },
                { text: 'В', dataIndex: 'p_load_top', width: 20, resizable: false, renderer: this.checkBoxRenderer, align: 'center', tdCls: 'pseudoinput-column' },
                { text: 'Доп. опции', dataIndex: 'p_additional_options_display', width: 100 },
                { text: 'Доп. характеристики', dataIndex: 'p_additional_chars_display', width: 150 }
            ]
        },
        { text: 'Подразделение', dataIndex: 'p_department_display', width: 150 },
        { text: 'Колонна', dataIndex: 'p_column_display', width: 150 },
        { text: 'Гаражный номер', dataIndex: 'p_garage_number', width: 150 },
        {
            text: 'Арендодатель',
            columns: [
                { text: 'Наименование', dataIndex: 'p_rent_name', width: 120 },
                { text: 'Адрес', dataIndex: 'p_rent_address', width: 120 },
                { text: 'Действует с', dataIndex: 'p_rent_from', width: 120 },
                { text: 'Действует по', dataIndex: 'p_rent_to', width: 120 }
            ]
        },
        {
            text: 'Услуга',
            columns: [
                { text: 'Наименование', dataIndex: 'p_rent_service_name', width: 120 },
                { text: 'Ед. изм', dataIndex: 'p_rent_service_unit_display', width: 50 },
                { text: 'Кол-во', dataIndex: 'p_rent_service_count', width: 50 },
                { text: 'Факт. к.', dataIndex: 'p_rent_service_fact_count', width: 50 },
                { text: 'Цена', dataIndex: 'p_rent_service_price', width: 50 },
                { text: 'Налог', dataIndex: 'p_rent_service_tax', width: 50 },
                { text: 'Сумма', dataIndex: 'p_rent_service_sum', width: 50 }
            ]
        }];
	},

	getTopGridFilters: function () {
		return Ext.create('Cement.view.transport.auto.ToolbarMixin').buildFilterItems();
	},

	getTopColumnsConfig: function () {
		return [
			{
				xtype: 'rowactions',
				hideable: false,
				resizeable: false,
				width: 18,
				keepSelection: true,
				locked: true,
				actions: [{ 
	                iconCls: 'icon-add-item', 
	                qtip: 'Добавить',
	                callback: this.addEmployee
	            }]
			},
			{ text: 'Категория', dataIndex: 'p_category_display', locked: true },
	        { text: 'Подкатегория', dataIndex: 'p_sub_category_display', locked: true },
	        {
	            text: 'Транспортное средство',
	            columns: [
	                { text: 'Марка, модель, модификация', dataIndex: 'p_mark_model_spec_display', width: 200 },
	                { text: 'Гос. номер', dataIndex: 'p_number_display', width: 100 }
	            ]
	        },
	        {
	            text: 'Характеристики',
	            columns: [
	                { text: 'Класс', dataIndex: 'p_class_display', width: 100 },
	                { text: 'ДхШхВ, м', dataIndex: 'p_size_display', width: 100 },
	                { text: 'Г/О, т/м<sup>3</sup>', dataIndex: 'p_go_display', width: 100 },
	                { text: 'О', dataIndex: 'p_char_ops', width: 20, resizable: false, align: 'center', renderer: this.radioButtonRenderer1, tdCls: 'pseudoinput-column' },
	                { text: 'П', dataIndex: 'p_char_ops', width: 20, resizable: false, align: 'center', renderer: this.radioButtonRenderer2, tdCls: 'pseudoinput-column' },
	                { text: 'С', dataIndex: 'p_char_ops', width: 20, resizable: false, align: 'center', renderer: this.radioButtonRenderer3, tdCls: 'pseudoinput-column' },
	                { text: 'Тип кузова', dataIndex: 'p_char_type_display', width: 100 },
	                { text: 'З', dataIndex: 'p_load_back', width: 20, resizable: false, renderer: this.checkBoxRenderer, align: 'center', tdCls: 'pseudoinput-column' },
	                { text: 'Б', dataIndex: 'p_load_left', width: 20, resizable: false, renderer: this.checkBoxRenderer, align: 'center', tdCls: 'pseudoinput-column' },
	                { text: 'В', dataIndex: 'p_load_top', width: 20, resizable: false, renderer: this.checkBoxRenderer, align: 'center', tdCls: 'pseudoinput-column' },
	                { text: 'Доп. опции', dataIndex: 'p_additional_options_display', width: 100 },
	                { text: 'Доп. характеристики', dataIndex: 'p_additional_chars_display', width: 150 }
	            ]
	        },
	        { text: 'Подразделение', dataIndex: 'p_department_display', width: 150 },
	        { text: 'Колонна', dataIndex: 'p_column_display', width: 150 },
	        { text: 'Гаражный номер', dataIndex: 'p_garage_number', width: 150 },
	        {
	            text: 'Арендодатель',
	            columns: [
	                { text: 'Наименование', dataIndex: 'p_rent_name', width: 120 },
	                { text: 'Адрес', dataIndex: 'p_rent_address', width: 120 },
	                { text: 'Действует с', dataIndex: 'p_rent_from', width: 120 },
	                { text: 'Действует по', dataIndex: 'p_rent_to', width: 120 }
	            ]
	        },
	        {
	            text: 'Услуга',
	            columns: [
	                { text: 'Наименование', dataIndex: 'p_rent_service_name', width: 120 },
	                { text: 'Ед. изм', dataIndex: 'p_rent_service_unit_display', width: 50 },
	                { text: 'Кол-во', dataIndex: 'p_rent_service_count', width: 50 },
	                { text: 'Факт. к.', dataIndex: 'p_rent_service_fact_count', width: 50 },
	                { text: 'Цена', dataIndex: 'p_rent_service_price', width: 50 },
	                { text: 'Налог', dataIndex: 'p_rent_service_tax', width: 50 },
	                { text: 'Сумма', dataIndex: 'p_rent_service_sum', width: 50 }
	            ]
	        }
		];
	},

	sign: function (send) {
		var store = this.down('*[role=sign-dst-grid]').getStore();
		if (store.getCount() > 0) {
			this.fireEvent('signitem', store.getAt(0));
		}
	},

	closeWindow: function () {
		this.hide();
	},

	getSrcStore: function () {
		return Ext.create('Ext.data.Store', {
			model: 'Cement.model.transport.Auto',
			autoLoad: false
		});
	},

	addToSrcStore: function (item) {
		if (!this.dstStore.getById(item.get('id'))) {
			this.srcStore.add(item.copy());
		}
	},

	initComponent: function () {
		this.callParent(arguments);
		this.on('beforeclose', function () {
			this.hide();
			return false;
		});
		this.down('treepanel').on('itemclick', function (tree, record) {
			var items = [];
			this.srcStore.removeAll();
			if (record.get('leaf')) {
				items.push(record);
			}
			else {
				Ext.each(record.childNodes, function (child) {
					if (child.get('leaf')) {
						items.push(child);
					}
				});
			}
			Ext.each(items, function (item) {
				this.addToSrcStore(item);
			}, this);
		}, this);
		this.addEvents('modificationselected');
		//var tree = this.down('treepanel');
		//tree.collapseAll();
		//tree.expandNode(tree.getRootNode()); //Store loading
	}
});