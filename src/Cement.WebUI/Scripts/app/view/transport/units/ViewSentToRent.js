Ext.define('Cement.view.transport.units.ViewSentToRent', {
	extend: 'Ext.panel.Panel',
	alias: 'widget.transport_unit_view_sent_to_rent',
	fieldsDisabled: true,
	buttons: null,
	border: 0,
	tbar: Cement.Config.modelViewButtons,
	layout: 'autocontainer',
	bbar: null,
	noUserFieldsetsControl: false,
	bodyCls: 'model-form',
	detailUrl: Cement.Config.url.transport.units.sent_to_rent.detailUrl,
	firmTitle: 'Передано в управление',

	mixins: [
		'Cement.view.basic.HasImg'
	],

	loadRecord: function (rec) {
		if (!rec.get) return;
		var me = this;
		me.setLoading(true);
		Ext.Ajax.request({
            url: Ext.String.format(me.detailUrl, rec.get('id')),
            success: function (response) {
            	var json = Ext.JSON.decode(response.responseText),
            		r = Ext.create('Cement.model.transport.FullInfo', json.auto),
            		r2 = Ext.create('Cement.model.transport.FullInfo', json.trailer),
            		d = Ext.create('Cement.model.Employee', json.driver),
            		f = Ext.create('Cement.model.Firm', json.firm);
            	me.down('transport_auto_view[role=truck]').loadRecord(r);
            	me.down('transport_auto_view[role=trailer]').loadRecord(r2);
            	me.down('employeeview').loadRecord(d);
            	me.down('firmview').loadRecord(f);
				if (r.get('show_char')) {
					me.down('transport_auto_view[role=truck] fieldset[role=char_display]').show();
				}
				else {
					me.down('transport_auto_view[role=truck] fieldset[role=char_display]').hide();
				}
				if (r2.get('show_char')) {
					me.down('transport_auto_view[role=trailer] fieldset[role=char_display]').show();
				}
				else {
					me.down('transport_auto_view[role=trailer] fieldset[role=char_display]').hide();
				}
				me.setLoading(false);
            }
        });
	},

	initComponent: function ()  {
		var items = [{
			xtype: 'transport_auto_view',
			title: 'Транспортное средство',
			layout: 'fit',
			role: 'truck',
			tbar: null
		}, {
			xtype: 'transport_auto_view',
			title: 'Прицепное ТС',
			layout: 'fit',
			role: 'trailer',
			tbar: null
		},{
			xtype: 'employeeview',
			title: 'Водитель',
			layout: 'fit',
			tbar: null
		}, {
			xtype: 'firmview',
			title: this.firmTitle,
			layout: 'fit',
			tbar: null
		}];
		Ext.apply(this, {
			items: [{
				xtype: 'panel',
				border: 0,
				layout: 'accordion',
				defaultType: 'textfield',
				anchor: '100%',
				bodyPadding: '0',
				autoScroll: true,
				oveflowX: 'hidden',
				defaults: {
					anchor: '100%',
					labelWidth: 110
				},
				items: items
			}]
		});
		this.callParent(arguments);
		this.setupImgCentering();
	}
});