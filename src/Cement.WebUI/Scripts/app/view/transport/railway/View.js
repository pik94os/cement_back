Ext.define('Cement.view.transport.railway.View', {
	extend: 'Cement.view.basic.Form',
	alias: 'widget.transport_railway_view',
	fieldsDisabled: true,
	buttons: null,
	border: 0,
	tbar: Cement.Config.modelViewButtons,
	layout: 'autocontainer',
	bbar: null,
	noUserFieldsetsControl: false,
	bodyCls: 'model-form',
	detailUrl: Cement.Config.url.transport.railway.owned.detailUrl,

	afterRecordLoad: function (rec) {
		this.down('container[role=name]').update(rec.get('p_name'));
		var img = this.down('image'),
			src = rec.get('p_photo');
		if (img) {
			if (src === undefined || src == '') {
				src = Cement.Config.default_photo;
			}
			img.setSrc(src);
			img.doComponentLayout();
		}
	},

	initComponent: function () {
		Ext.apply(this, {
			items: [{
				xtype: 'panel',
				border: 0,
				layout: 'anchor',
				defaultType: 'textfield',
				anchor: '100%',
				bodyPadding: '10 10 10 5',
				autoScroll: true,
				defaults: {
					anchor: '100%',
					xtype: 'textfield',
					labelWidth: 110
				},
				items: [{
					xtype: 'image',
					src: Cement.Config.default_photo,
					maxWidth: 83,
					maxHeight: 100,
					x: 23,
					y: 0,
					cls: 'pos-relative'
				}, {
					xtype: 'container',
					role: 'name',
					align: 'center',
					padding: '3 0 10 0',
					html: '123',
					cls: 'carriage-name'
				}, {
					fieldLabel: 'Модель',
					name: 'p_model_display',
					disabled: true
				}, {
					fieldLabel: 'Год выпуска',
					name: 'p_made_year',
					disabled: true
				}, {
					fieldLabel: 'Номер',
					name: 'p_number_display',
					disabled: true
				}, {
					fieldLabel: 'Род',
					name: 'p_kind',
					disabled: true
				}, {
					fieldLabel: 'Особенность модели',
					name: 'p_feature_display',
					disabled: true
				}, {
					fieldLabel: 'Грузоподъемность, т.',
					name: 'p_cargo_max',
					disabled: true
				}, {
					fieldLabel: 'Масса тары min, т.',
					name: 'p_package_mass_min',
					disabled: true
				}, {
					fieldLabel: 'Масса тары max, т.',
					name: 'p_package_mass_max',
					disabled: true
				}, {
					fieldLabel: 'Длина по осям автосцепки, мм.',
					name: 'p_axis_length',
					disabled: true
				}, {
					fieldLabel: 'Количество осей, шт',
					name: 'p_axis_count',
					disabled: true
				}, {
					fieldLabel: 'Осевая нагрузка, т.',
					name: 'p_axis_load',
					disabled: true
				}, {
					fieldLabel: 'Переходная площадка',
					name: 'p_gangway',
					disabled: true
				}, {
					fieldLabel: 'Объем кузова, куб. м',
					name: 'p_volume',
					disabled: true
				}, {
					fieldLabel: 'Модель тележки',
					name: 'p_trolley_model',
					disabled: true
				}, {
					fieldLabel: 'Габарит',
					name: 'p_size',
					disabled: true
				}, {
					fieldLabel: 'Изготовитель',
					name: 'p_manufacturer',
					disabled: true
				}]
			}]
		});

		this.callParent(arguments);
		this.setupImgCentering();
	}
});