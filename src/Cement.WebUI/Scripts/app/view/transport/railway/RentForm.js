Ext.define('Cement.view.transport.railway.RentForm', {
	extend: 'Cement.view.basic.Form',
	alias: 'widget.transport_railway_rent_form',
	fieldsDisabled: false,
	url: Cement.Config.url.transport.railway.sent_to_rent.saveUrl,
	method: Cement.Config.url.transport.railway.sent_to_rent.saveMethod,
	printUrl: Cement.Config.url.transport.railway.sent_to_rent.printItem,

	notCollapseFieldsetIndex: 0,

	carriagesStoreId: 'transport.auxiliary.OwnedCarriages',

	getItems: function (argument) {
		return [{
			xtype: 'container',
			padding: 0,
			border: 0,
			layout: 'hbox',
			items: [{
				xtype: 'container',
				flex: 1,
				border: 0,
				layout: 'anchor',
				defaults: {
					xtype: 'textfield',
					anchor: '100%',
					labelWidth: 200,
					labelAlign: 'right'
				},
				items: [
					this.getCombo('Вагон номер', 'p_carriage_number', this.carriagesStoreId), {
					xtype: 'fieldcontainer',
					layout: 'hbox',
					items:[{
						xtype: 'hiddenfield',
						name: 'p_renter'
					}, {
						xtype: 'textfield',
						labelWidth: 200,
						labelAlign: 'right',
						name: 'p_renter_display',
						fieldLabel: 'Арендатор',
						flex: 1
	                }, {
	                	xtype: 'button',
	                	text: 'Выбрать',
	                	margin: '0 0 0 4',
	                	action: 'show_renters_window',
	                	width: 60
	                }]
				}, {
					xtype: 'fieldcontainer',
					layout: 'hbox',
					items:[{
						xtype: 'hiddenfield',
						name: 'p_document'
					}, {
						xtype: 'textfield',
						labelWidth: 200,
						labelAlign: 'right',
						name: 'p_document_display',
						fieldLabel: 'Основание',
						flex: 1
	                }, {
	                	xtype: 'button',
	                	text: 'Выбрать',
	                	margin: '0 0 0 4',
	                	action: 'show_documents_window',
	                	width: 60
	                }]
				}, {
					xtype: 'textfield',
					name: 'p_service_display',
					fieldLabel: 'Услуга',
					disabled: true
				}, {
					fieldLabel: 'Действует с',
					name: 'p_date_start',
					xtype: 'datefield'
				}, {
					fieldLabel: 'Действует по',
					name: 'p_date_end',
					xtype: 'datefield'
				}]
			}]
		}]
	},

	initComponent: function () {
		Ext.apply(this, {
			bodyPadding: '20px 80px'
		});
		this.callParent(arguments);
		Ext.getStore(this.carriagesStoreId).load();
		this.down('button[action=show_renters_window]').on('click', function () {
			this.createRentersWindow();
			this.rentersWindow.show();
		}, this);
		this.down('button[action=show_documents_window]').on('click', function () {
			this.createDocumentWindow();
			this.documentsWindow.show();
		}, this);
	},

	createDocumentWindow: function () {
		if (!this.documentsWindow) {
			this.documentsWindow = Ext.create('Cement.view.transport.RentDocumentWindow');
			this.documentsWindow.on('signitem', function (data) {
				this.down('textfield[name=p_document_display]').setValue(data.get('name'));
				this.down('textfield[name=p_service_display]').setValue(data.get('service_name'));
				this.down('hidden[name=p_document]').setValue(data.get('id'));
				this.documentsWindow.hide();
			}, this);
		}
	},

	createRentersWindow: function () {
		if (!this.rentersWindow) {
			this.rentersWindow = Ext.create('Cement.view.transport.RenterWindow');
			this.rentersWindow.on('signitem', function (data) {
				this.down('textfield[name=p_renter_display]').setValue(data.get('name'));
				this.down('hidden[name=p_renter]').setValue(data.get('id'));
				this.rentersWindow.hide();
			}, this);
		}
	},

	afterRecordLoad: function (rec) {
		if (!this.fieldsDisabled) {
			this.createRentersWindow();
			this.createDocumentWindow();
			this.rentersWindow.loadData([rec.get('p_renter')]);
			this.documentsWindow.loadData([rec.get('p_document')]);
		}
	}
});