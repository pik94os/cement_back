Ext.define('Cement.view.transport.railway.SentToRent', {
    extend: 'Cement.view.basic.Grid',
    alias: 'widget.transport_railway_send_to_rent',
    gridStore: 'transport.railway.SentToRent',
    gridStateId: 'stateTransportRailwaySendToRent',
    printUrl: Cement.Config.url.transport.railway.sent_to_rent.printGrid,
    helpUrl: Cement.Config.url.transport.railway.sent_to_rent.help,
    deleteUrl: Cement.Config.url.transport.railway.sent_to_rent.deleteUrl,
    printItemUrl: Cement.Config.url.transport.railway.sent_to_rent.printItem,

    getFilterItems: function () {
        return [{
            text: 'Модель',
            kind: 'selector',
            field_name: 'p_model',
            checked: false
        }, {
            text: 'Номер',
            kind: 'selector',
            field_name: 'p_number',
            checked: false
        }, {
            text: 'Марка, модель, модификация',
            kind: 'selector',
            field_name: 'p_feature',
            checked: false
        }]
    },

    getActionColumns: function () {
        return {
            xtype: 'rowactions',
            hideable: false,
            resizeable: false,
            locked: true,
            width: 46,
            actions: [{
                iconCls: 'icon-delete-item',
                qtip: 'Удалить',
                callback: this.deleteItem
            }],
            keepSelection: true
        };
    },

    getGridColumns: function () {
        return [
            this.getActionColumns(),
            { text: 'Модель', dataIndex: 'p_model_display', width: 200 },
            { text: 'Номер', dataIndex: 'p_number_display', width: 200 },
            { text: 'Особенность модели', dataIndex: 'p_feature_display', width: 200 },
            {
                text: 'Арендатор',
                columns: [
                    { text: 'Наименование', dataIndex: 'p_rent_name', width: 120 },
                    { text: 'Адрес', dataIndex: 'p_rent_address', width: 120 },
                    { text: 'Действует с', dataIndex: 'p_rent_from', width: 120 },
                    { text: 'Действует по', dataIndex: 'p_rent_to', width: 120 }
                ]
            },
            {
                text: 'Услуга',
                columns: [
                    { text: 'Наименование', dataIndex: 'p_rent_service_name', width: 120 },
                    { text: 'Ед. изм', dataIndex: 'p_rent_service_unit_display', width: 50 },
                    { text: 'Кол-во', dataIndex: 'p_rent_service_count', width: 50 },
                    { text: 'Факт. к.', dataIndex: 'p_rent_service_fact_count', width: 50 },
                    { text: 'Цена', dataIndex: 'p_rent_service_price', width: 50 },
                    { text: 'Налог', dataIndex: 'p_rent_service_tax', width: 50 },
                    { text: 'Сумма', dataIndex: 'p_rent_service_sum', width: 50 }
                ]
            }
        ];
    }
});