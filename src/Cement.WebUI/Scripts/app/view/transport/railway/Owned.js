Ext.define('Cement.view.transport.railway.Owned', {
    extend: 'Cement.view.basic.Grid',
    alias: 'widget.transport_railway_owned',
    gridStore: 'transport.railway.Owned',
    gridStateId: 'stateTransportRailwayOwned',
    printUrl: Cement.Config.url.transport.railway.owned.printGrid,
    helpUrl: Cement.Config.url.transport.railway.owned.help,
    deleteUrl: Cement.Config.url.transport.railway.owned.deleteUrl,
    printItemUrl: Cement.Config.url.transport.railway.owned.printItem,

    creatorTree: Ext.clone(Cement.Creators.transport.children[3]),
    createWindowTitle: 'Создать',

    getFilterItems: function () {
        return [{
            text: 'Модель',
            kind: 'selector',
            field_name: 'p_model',
            checked: false
        }, {
            text: 'Номер',
            kind: 'selector',
            field_name: 'p_number',
            checked: false
        }, {
            text: 'Марка, модель, модификация',
            kind: 'selector',
            field_name: 'p_feature',
            checked: false
        }]
    },

    getGridColumns: function () {
        return [
            this.getActionColumns(),
            { text: 'Модель', dataIndex: 'p_model_display', flex: 1 },
            { text: 'Номер', dataIndex: 'p_number_display', flex: 1 },
            { text: 'Особенность модели', dataIndex: 'p_feature_display', flex: 1 }
        ];
    }
});