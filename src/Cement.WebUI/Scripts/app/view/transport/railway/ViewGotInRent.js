Ext.define('Cement.view.transport.railway.ViewGotInRent', {
	extend: 'Cement.view.transport.railway.ViewSentToRent',
	alias: 'widget.transport_railway_view_got_in_rent',
	firmTabName: 'Арендодатель',
	detailUrl: Cement.Config.url.transport.railway.got_in_rent.detailUrl
});