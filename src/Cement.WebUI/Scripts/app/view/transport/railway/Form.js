Ext.define('Cement.view.transport.railway.Form', {
	extend: 'Cement.view.basic.Form',
	alias: 'widget.transport_railway_form',
	fieldsDisabled: false,
	url: Cement.Config.url.transport.railway.owned.saveUrl,
	method: Cement.Config.url.transport.railway.owned.saveMethod,
	printUrl: Cement.Config.url.transport.railway.owned.printItem,

	notCollapseFieldsetIndex: 0,

	modelsStoreId: 'transport.auxiliary.RailwayModels',
	manufacturersStoreId: 'transport.auxiliary.RailwayManufacturers',

	getItems: function (argument) {
		return [{
			xtype: 'container',
			layout: 'anchor',
			defaults: {
				xtype: 'textfield',
				anchor: '100%',
				labelWidth: 190,
				labelAlign: 'right'
			},
			items: [
				this.getCombo('Модель', 'p_model', this.modelsStoreId),
				{
					xtype: 'numberfield',
					fieldLabel: 'Год выпуска',
					name: 'p_made_year',
					allowBlank: false
				}, {
					fieldLabel: 'Номер',
					name: 'p_number',
					allowBlank: false
				},
				this.getCombo('Изготовитель', 'p_manufacturer', this.manufacturersStoreId)
			]
		}];
	},

	initComponent: function () {
		this.callParent(arguments);
		Ext.getStore(this.modelsStoreId).load();
		Ext.getStore(this.manufacturersStoreId).load();
	}
});