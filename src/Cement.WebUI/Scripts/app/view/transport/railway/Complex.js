Ext.define('Cement.view.transport.railway.Complex', {
    extend: 'Cement.view.basic.Tabs',

    initComponent: function () {
        Ext.apply(this, {
            title: 'Железнодорожный транспорт',
            items: [{
                title: 'Собственные',
                layout: 'fit',
                xtype: 'transport_railway_owned',
                detailType: 'Cement.view.transport.railway.View'
            }, {
                title: 'Переданные в аренду',
                layout: 'fit',
                xtype: 'transport_railway_send_to_rent',
                detailType: 'Cement.view.transport.railway.ViewSentToRent'
            }, {
                title: 'Полученные в аренду',
                layout: 'fit',
                xtype: 'transport_railway_got_in_rent',
                detailType: 'Cement.view.transport.railway.ViewGotInRent'
            }]
        });
        this.callParent(arguments);
    }
});