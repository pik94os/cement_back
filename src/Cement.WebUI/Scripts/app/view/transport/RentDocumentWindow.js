Ext.define('Cement.view.transport.RentDocumentWindow', {
	extend: 'Cement.view.common.SignWindow',
	xtype: 'transport_rent_document_window',
	leftFrameTitle: 'Договора и услуги',
	topGridTitle: 'Выбрать услугу',
	bottomGridTitle: 'Выбранная услуга',
	title: 'Выбор договора и услуги',
	structure_storeId: 'transport.auxiliary.RentDocuments',
	hideCommentPanel: true,
	singleSelection: true,
	storeFields: ['id', 'name', 'service_name', 'p_group', 'p_subgroup', 'p_trademark', 'p_manufacturer', 'p_unit', 'p_tax', 'p_price'],
	width: 950,
	height: 600,

	bbar: [{
		text: 'Сохранить',
		action: 'confirm'
	}, {
		text: 'Отмена',
		action: 'cancel'
	}],

	sign: function (send) {
		var store = this.down('*[role=sign-dst-grid]').getStore();
		if (store.getCount() > 0) {
			this.fireEvent('signitem', store.getAt(0), this.down('treepanel').getSelectionModel().getSelection()[0].get('id'));
		}
	},

	addToSrcStore: function (item) {
		if (!this.dstStore.getById(item.get('id'))) {
			this.srcStore.add({
				id: item.get('id'),
				service_name: item.get('text'),
				name: item.parentNode.get('text'),
				p_group: item.get('p_group'),
				p_subgroup: item.get('p_subgroup'),
				p_trademark: item.get('p_trademark'),
				p_manufacturer: item.get('p_manufacturer'),
				p_unit: item.get('p_unit'),
				p_tax: item.get('p_tax'),
				p_price: item.get('p_price')
			});
		}
	},

	getTopGridFilters: function () {
		return [{
            text: 'Наименование',
            kind: 'selector',
            field_name: 'name',
            checked: true
        }, {
            text: 'Группа',
            kind: 'selector',
            field_name: 'p_group',
            checked: false
        }, {
            text: 'Группа',
            kind: 'selector',
            field_name: 'p_group',
            checked: false
        }, {
            text: 'Группа',
            kind: 'selector',
            field_name: 'p_group',
            checked: false
        }, {
            text: 'Подгруппа',
            kind: 'selector',
            field_name: 'p_subgroup',
            checked: false
        }, {
            text: 'Торговая марка',
            kind: 'selector',
            field_name: 'p_trademark',
            checked: false
        }, {
            text: 'Ед. изм.',
            kind: 'selector',
            field_name: 'p_unit',
            checked: false
        }, {
            text: 'Налог',
            kind: 'selector',
            field_name: 'p_tax',
            checked: false
        }, {
            text: 'Цена',
            kind: 'selector',
            field_name: 'p_price',
            checked: false
        }];
	},

	getTopColumnsConfig: function () {
		return [
			{
				xtype: 'rowactions',
				hideable: false,
				resizeable: false,
				width: 18,
				keepSelection: true,
				actions: [{
	                iconCls: 'icon-add-document',
	                qtip: 'Добавить',
	                callback: this.addEmployee
	            }]
			},
			{ text: 'Наименование', dataIndex: 'name', flex: 1 },
			{ text: 'Группа', dataIndex: 'p_group', flex: 1 },
			{ text: 'Подгруппа', dataIndex: 'p_subgroup', flex: 1 },
			{ text: 'Торговая марка', dataIndex: 'p_trademark', flex: 1 },
			{ text: 'Ед. изм.', dataIndex: 'p_unit', flex: 0.3 },
			{ text: 'Налог', dataIndex: 'p_tax', flex: 0.3 },
			{ text: 'Цена', dataIndex: 'p_price', flex: 0.3 }
		];
	},

	getBottomColumnsConfig: function () {
		return [
			{
				xtype: 'rowactions',
				hideable: false,
				resizeable: false,
				width: 18,
				actions: [{
	                iconCls: 'icon-remove-document',
	                qtip: 'Удалить',
	                callback: this.removeEmployee
	            }]
			},
			{ text: 'Наименование', dataIndex: 'name', flex: 1 },
			{ text: 'Группа', dataIndex: 'p_group', flex: 1 },
			{ text: 'Подгруппа', dataIndex: 'p_subgroup', flex: 1 },
			{ text: 'Торговая марка', dataIndex: 'p_trademark', flex: 1 },
			{ text: 'Ед. изм.', dataIndex: 'p_unit', flex: 0.3 },
			{ text: 'Налог', dataIndex: 'p_tax', flex: 0.3 },
			{ text: 'Цена', dataIndex: 'p_price', flex: 0.3 }
		];
	},

	loadData: function (data) {
		if (data) {
			var fstore = Ext.getStore(this.structure_storeId);
			fstore.load({
				scope: this,
				callback: function () {
					Ext.each(data, function (item) {
						var r = fstore.getById(item);
						if (!this.dstStore) {
							this.setupEvents();
						}
						if (r) {
							this.dstStore.add({
								id: r.get('id'),
								service_name: r.get('service_name'),
								name: r.get('text')
							});
						}
					}, this);
				}
			});
		}
	},

	closeWindow: function () {
		this.hide();
	},

	initComponent: function () {
		this.callParent(arguments);
		var tree = this.down('treepanel');
		tree.collapseAll();
		tree.expandNode(tree.getRootNode()); //Store loading
		this.on('beforeclose', function () {
			this.hide();
			return false;
		});
	}
});