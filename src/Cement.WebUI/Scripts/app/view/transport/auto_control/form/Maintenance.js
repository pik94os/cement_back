Ext.define('Cement.view.transport.auto_control.form.Maintenance', {
	extend: 'Cement.view.basic.Form',
	alias: 'widget.transport_auto_control_form_maintenance',
	fieldsDisabled: false,
	url: Cement.Config.url.transport.auto_control.maintenance.saveUrl,
	method: Cement.Config.url.transport.auto_control.maintenance.saveMethod,
	printUrl: Cement.Config.url.transport.auto_control.maintenance.printItem,
	activeItem: 0,
	isFilter: false,
	notCollapseFieldsetIndex: 0,
	width: 500,
	allowBlankFields: false,

	cars_store_Id: 'transport.auto_control.auxiliary.Cars',
	car_drivers_store_Id: 'transport.auto_control.auxiliary.Drivers',

	getItems: function () {
		return [
            {
			xtype: 'panel',
			layout: 'anchor',
			border: 0,
			defaults:
            {
				anchor: '100%',
				xtype: 'textfield',
				labelAlign: 'right',
				labelWidth: 130
			},
			items:
            [
                {
				    xtype: 'hiddenfield',
				    name: 'id'
			    },
                {
                    xtype: 'fieldcontainer',
                    layout: 'hbox',
                    items:
                    [
                        {
                            xtype: 'hiddenfield',
                            name: 'p_transport'
                        },
                        {
                            xtype: 'textfield',
                            labelWidth: 130,
                            labelAlign: 'right',
                            name: 'p_transport_display',
                            fieldLabel: 'Транспортное средство',
                            flex: 1
                        },
                        {
                            xtype: 'button',
                            text: 'Выбрать',
                            margin: '0 0 0 4',
                            action: 'show_transport_window',
                            width: 60
                        }
                    ]
                },
                {
				    xtype: this.getDateFieldXType(),
				    fieldLabel: 'Дата',
				    name: 'p_date',
				    allowBlank: this.allowBlankFields,
				    disabled: this.fieldsDisabled
			    }, {
			        fieldLabel: 'Пробег',
			        xtype: 'numberfield',
			        allowBlank: this.allowBlankFields,
			        hideTrigger: this.fieldsDisabled,
				    disabled: this.fieldsDisabled,
				    name: 'p_odometer'
			    }, {
				    fieldLabel: 'Наименование',
				    allowBlank: this.allowBlankFields,
				    hideTrigger: this.fieldsDisabled,
				    disabled: this.fieldsDisabled,
				    name: 'p_name'
			    },
                this.getSelectorField('Автосервис', 'p_autoservice', 'show_autoservice_window', 130), {
                    name: 'p_address',
                    fieldLabel: 'Адрес',
                    disabled: true
                },
                {
				    fieldLabel: 'Мастер',
				    allowBlank: this.allowBlankFields,
				    disabled: this.fieldsDisabled,
				    name: 'p_master'
			    }, {
				    fieldLabel: 'Дата выполнения',
				    allowBlank: this.allowBlankFields,
				    disabled: this.fieldsDisabled,
				    xtype: this.getDateFieldXType(),
				    name: 'p_made_date'
			    }, {
			        fieldLabel: 'Стоимость',
			        xtype: 'numberfield',
			        allowBlank: this.allowBlankFields,
			        hideTrigger: this.fieldsDisabled,
				    disabled: this.fieldsDisabled,
				    name: 'p_cost'
			    }, {
				    fieldLabel: 'Заметки',
				    allowBlank: this.allowBlankFields,
				    disabled: this.fieldsDisabled,
				    name: 'p_comment'
			    },
				{
				    xtype: 'fieldcontainer',
				    layout: 'hbox',
				    items:[{
				        xtype: 'hiddenfield',
				        name: 'p_driver'
				    }, {
				        xtype: 'textfield',
				        labelWidth: 130,
				        labelAlign: 'right',
				        name: 'p_driver_display',
				        fieldLabel: 'Водитель',
				        flex: 1
				    }, {
				        xtype: 'button',
				        text: 'Выбрать',
				        margin: '0 0 0 4',
				        action: 'show_driver_window',
				        width: 60
				    }]
				},
			    {
				    fieldLabel: 'Документ',
				    allowBlank: this.allowBlankFields,
				    disabled: this.fieldsDisabled,
				    name: 'p_document'
			    },
				this.getFileField('Документ', 'p_document_file')
			]
		}];
	},

	initComponent: function () {
	    this.callParent(arguments);
	    this.down('button[action=show_transport_window]').on('click', function () {
	        this.createTransportWindow();
	        this.transportWindow.show();
	    }, this);
	    this.down('button[action=show_driver_window]').on('click', function () {
	        this.createDriverWindow();
	        this.driverWindow.show();
	    }, this);
	    this.down('button[action=show_autoservice_window]').on('click', function () {
	        this.createAutoserviceWindow();
	        this.autoserviceWindow.show();
	    }, this);
		if (!this.fieldsDisabled) {
			Ext.getStore(this.cars_store_Id).load();
			Ext.getStore(this.car_drivers_store_Id).load();
		}
	},

	createTransportWindow: function () {
	    if (!this.transportWindow) {
	        this.transportWindow = Ext.create('Cement.view.transport.units.CarsWindow');
	        this.transportWindow.on('signitem', function (model) {
	            this.down('hiddenfield[name=p_transport]').setValue(model.get('id'));
	            this.down('textfield[name=p_transport_display]').setValue(model.get('p_name'));
	            this.transportWindow.hide();
	        }, this);
	    }
	},

	createDriverWindow: function () {
	    if (!this.driverWindow) {
	        this.driverWindow = Ext.create('Cement.view.transport.units.DriversWindow');
	        this.driverWindow.on('signitem', function (model) {
	            this.down('hiddenfield[name=p_driver]').setValue(model.get('id'));
	            this.down('textfield[name=p_driver_display]').setValue(model.get('p_name'));
	            this.driverWindow.hide();
	        }, this);
	    }
	},

	createAutoserviceWindow: function () {
	    if (!this.autoserviceWindow) {
	        this.autoserviceWindow = Ext.create('Cement.view.transport.auto_control.form.AutoserviceWindow');
	        this.autoserviceWindow.on('selected', function (rec) {
	            this.down('textfield[name=p_autoservice_display]').setValue(rec.get('p_name'));
	            this.down('textfield[name=p_address]').setValue(rec.get('p_address'));
	            this.down('hiddenfield[name=p_autoservice]').setValue(rec.get('id'));
	            this.autoserviceWindow.hide();
	        }, this);
	    }
	},

	afterRecordLoad: function (rec) {
	    this.createTransportWindow();
	    this.createDriverWindow();
	    this.createAutoserviceWindow();
	    this.transportWindow.loadData([rec.get('id')]);
	    this.driverWindow.loadData([rec.get('id')]);
	}
});