Ext.define('Cement.view.transport.auto_control.form.Regulation', {
	extend: 'Cement.view.basic.Form',
	alias: 'widget.transport_auto_control_form_regulation',
	fieldsDisabled: false,
	url: Cement.Config.url.transport.auto_control.regulation.saveUrl,
	method: Cement.Config.url.transport.auto_control.regulation.saveMethod,
	printUrl: Cement.Config.url.transport.auto_control.regulation.printItem,
	activeItem: 0,
	isFilter: false,
	notCollapseFieldsetIndex: 0,
	width: 500,
	allowBlankFields: false,

	cars_store_Id: 'transport.auto_control.auxiliary.Cars',
	work_names_store_Id: 'transport.auto_control.auxiliary.WorkNames',
	work_units_store_Id: 'transport.auto_control.auxiliary.WorkUnits',
	new_regulation_items_storeId: 'transport.auto_control.auxiliary.NewRegulationItems',
	regulation_items_storeId: 'transport.auto_control.auxiliary.RegulationItems',

	getCarField: function () {
		if (!this.fieldsDisabled) {
			return {
				xtype: 'panel',
				border: 0,
				padding: 0,
				layout: 'column',
				items: [{
					xtype: 'panel',
					border: 0,
					columnWidth: 0.25,
					html: '&nbsp;'
				}, {
					xtype: 'panel',
					border: 0,
					columnWidth: 0.5,
					layout: 'anchor',
					defaults: {
						anchor: '100%',
						labelWidth: 150
					},
					items: [
						this.getCombo('Транспортное средство', 'p_transport_unit', this.cars_store_Id)
					]
				}, {
					xtype: 'panel',
					border: 0,
					columnWidth: 0.25
				}]
			};
		}
		else {
			return {
				xtype: 'panel',
				border: 0,
				padding: 0,
				layout: 'anchor',
				defaults: {
					anchor: '100%'
				},
				items: [{
					xtype: 'panel',
					border: 0,
					padding: '0 0 10 0',
					cls: 'panel-center-aligned',
					html: 'Транспортное средство:'
				}, {
					xtype: 'textfield',
					name: 'p_transport_unit_display',
					disabled: true
				}]
			}
		}
	},

	getPanels: function () {
		var result = {
			xtype: 'panel',
			border: 0,
			layout: {
					type: 'column',
					align: 'stretch',
					padding: 0
				},
					margin: '10 0 0 0',
					defaults: {
						flex: 1
					},
					items: [{
						xtype: 'hiddenfield',
						name: 'p_complect'
					}, {
						xtype: 'grid',
						height: 400,
						columnWidth: 0.5,
						role: 'dest-grid',
						cls: 'bordered-grid',
						viewConfig: {
							emptyText: '<div class="price-empty-grid">Перетащите сюда товары и(или) услуги для добавления в прайс-лист</div>',							
				            plugins: {
				                ptype: 'gridviewdragdrop',
				                dragGroup: 'firstGridDDGroup',
				                dropGroup: 'secondGridDDGroup'
				            },
			            	listeners: {
			                	drop: function(node, data, dropRec, dropPosition) {
			                    	var dropOn = dropRec ? ' ' + dropPosition + ' ' + dropRec.get('name') : ' on empty view';
			                    	// Ext.example.msg("Drag from right to left", 'Dropped ' + data.records[0].get('name') + dropOn);
			                	}
			            	}
			        	},
			        store: this.new_regulation_items_storeId,
			        columns: [
				        {text: "Запчасть/услуга", flex: 1, sortable: true, dataIndex: 'p_name'}
					],
			        stripeRows: true,
			        margins: '0 2 0 0'
					}, {
						xtype: 'panel',
						width: 32,
						bodyPadding: '5',
						border: 0,
						layout: 'vbox',
						align: 'center',
						defaults: {
							xtype: 'button'
						},
						items: [{
							margin: '114 0 4 0',
							iconCls: 'icon-move-top',
							action: 'make_first'
						}, {
							margin: '0 0 4 0',
							iconCls: 'icon-move-up',
							action: 'move_up'
						},  {
							margin: '0 0 4 0',
							iconCls: 'icon-move-right',
							action: 'move_right'
						}, {
							margin: '0 0 4 0',
							iconCls: 'icon-move-left',
							action: 'move_left'
						}, {
							margin: '0 0 4 0',
							iconCls: 'icon-move-down',
							action: 'move_down'
						}, {
							iconCls: 'icon-move-bottom',
							action: 'make_last'
						}]
					}, {
						xtype: 'grid',
						height: 400,
						columnWidth: 0.5,
						cls: 'bordered-grid',
						role: 'source-grid',
						viewConfig: {
		            plugins: {
		                ptype: 'gridviewdragdrop',
		                dragGroup: 'secondGridDDGroup',
		                dropGroup: 'firstGridDDGroup'
		            },
		            listeners: {
		                drop: function(node, data, dropRec, dropPosition) {
		                    var dropOn = dropRec ? ' ' + dropPosition + ' ' + dropRec.get('name') : ' on empty view';
		                }
		            }
		        },
		        store: this.regulation_items_storeId,
		        columns: [
			        { text: "Запчасть/услуга", flex: 1, sortable: true, dataIndex: 'p_name'}
				    ],
		        stripeRows: true,
		        margins: '0 0 0 3'
			}]
		};
		if (this.fieldsDisabled) {
			result = {
				xtype: 'textfield',
				fieldLabel: 'Запчасть/услуга',
				name: 'p_items_display',
				disabled: true
			};
		}
		return result;
	},

	getItems: function () {
		return [
			this.getCarField(), {
			xtype: 'panel',
			layout: 'anchor',
			border: 0,
			defaults: {
				anchor: '100%',
				xtype: 'textfield'
			},
			items: [{
				xtype: 'hiddenfield',
				name: 'id'
			}, {
				fieldLabel: 'Наименование',
				allowBlank: this.allowBlankFields,
				disabled: this.fieldsDisabled,
				name: 'p_name'
			}, {
				fieldLabel: 'Период',
				allowBlank: this.allowBlankFields,
				disabled: this.fieldsDisabled,
				name: 'p_period'
			}, {
				fieldLabel: 'Пробег',
				allowBlank: this.allowBlankFields,
				disabled: this.fieldsDisabled,
				name: 'p_odometer'
			},

				this.getPanels(),

				this.getCombo('Наименование работы', 'p_work_name', this.work_names_store_Id),
				this.getCombo('Ед. изм.', 'p_work_unit', this.work_units_store_Id),
			{
				fieldLabel: 'Количество',
				allowBlank: this.allowBlankFields,
				disabled: this.fieldsDisabled,
				name: 'p_count'
			}, {
				fieldLabel: 'Дополнительно',
				allowBlank: this.allowBlankFields,
				disabled: this.fieldsDisabled,
				name: 'p_comment'
			}, {
				xtype: 'hiddenfield',
				name: 'p_items'
			}]
		}];
	},

	initComponent: function () {
		this.callParent(arguments);
		if (!this.fieldsDisabled) {
			Ext.getStore(this.cars_store_Id).load();
			Ext.getStore(this.work_names_store_Id).load();
			Ext.getStore(this.work_units_store_Id).load();
			Ext.getStore(this.regulation_items_storeId).load();
		}
	},

	afterRecordLoad: function (record) {
		if (!this.fieldsDisabled) {
			var me = this;
			Ext.getStore(me.regulation_items_storeId).load({
				callback: function () {
					var complect = record.get('p_items'),
						destStore = me.down('grid[role=dest-grid]').getStore(),
						srcStore = me.down('grid[role=source-grid]').getStore();
					Ext.each(complect, function (c) {
						var rec = srcStore.getById("" + c);
						if (rec) {
							var newModel = rec.copy();
							destStore.add(newModel);
							srcStore.remove(rec);
						}
					}, me);
				}
			});
		}
	},

	beforeSubmit: function (callback) {
		var complect = [];
		this.down('grid[role=dest-grid]').getStore().each(function (rec) {
			complect.push(rec.get('id'));
		});
		this.down('hiddenfield[name=p_items]').setValue(
			Ext.JSON.encode(complect)
		);
		callback.call();
	}
});