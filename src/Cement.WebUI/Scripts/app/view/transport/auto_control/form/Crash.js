Ext.define('Cement.view.transport.auto_control.form.Crash', {
	extend: 'Cement.view.basic.Form',
	alias: 'widget.transport_auto_control_form_crash',
	fieldsDisabled: false,
	url: Cement.Config.url.transport.auto_control.crash.saveUrl,
	method: Cement.Config.url.transport.auto_control.crash.saveMethod,
	printUrl: Cement.Config.url.transport.auto_control.crash.printItem,
	activeItem: 0,
	isFilter: false,
	notCollapseFieldsetIndex: 0,
	width: 500,
	allowBlankFields: false,

	cars_store_Id: 'transport.auto_control.auxiliary.Cars',
	car_drivers_store_Id: 'transport.auto_control.auxiliary.Drivers',

	getCarField: function () {
		if (!this.fieldsDisabled) {
			return {
				xtype: 'panel',
				border: 0,
				padding: 0,
				layout: 'column',
				items: [{
					xtype: 'panel',
					border: 0,
					columnWidth: 0.25,
					html: '&nbsp;'
				}, {
					xtype: 'panel',
					border: 0,
					columnWidth: 0.5,
					layout: 'anchor',
					defaults: {
						anchor: '100%',
						labelWidth: 150
					},
					items: [
						this.getCombo('Транспортное средство', 'p_transport_unit', this.cars_store_Id)
					]
				}, {
					xtype: 'panel',
					border: 0,
					columnWidth: 0.25
				}]
			};
		}
		else {
			return {
				xtype: 'panel',
				border: 0,
				padding: 0,
				layout: 'anchor',
				defaults: {
					anchor: '100%'
				},
				items: [{
					xtype: 'panel',
					border: 0,
					padding: '0 0 10 0',
					cls: 'panel-center-aligned',
					html: 'Транспортное средство:'
				}, {
					xtype: 'textfield',
					name: 'p_transport_unit_display',
					disabled: true
				}]
			}
		}
	},

	getItems: function () {
		return [
			this.getCarField(), {
			xtype: 'panel',
			layout: 'anchor',
			border: 0,
			defaults: {
				anchor: '100%',
				xtype: 'textfield'
			},
			items: [{
				xtype: 'hiddenfield',
				name: 'id'
			}, {
				xtype: this.getDateFieldXType(),
				fieldLabel: 'Дата',
				name: 'p_date',
				allowBlank: this.allowBlankFields,
				disabled: this.fieldsDisabled
			}, {
				fieldLabel: 'Кол-во ТС',
				name: 'p_count',
				allowBlank: this.allowBlankFields,
				disabled: this.fieldsDisabled
			}, {
				fieldLabel: 'Ущерб',
				allowBlank: this.allowBlankFields,
				disabled: this.fieldsDisabled,
				name: 'p_damage'
			}, {
				fieldLabel: 'Описание',
				allowBlank: this.allowBlankFields,
				disabled: this.fieldsDisabled,
				name: 'p_comment'
			}, {
				fieldLabel: 'Сумма',
				allowBlank: this.allowBlankFields,
				disabled: this.fieldsDisabled,
				name: 'p_sum'
			},
				this.getCombo('Водитель', 'p_driver', this.car_drivers_store_Id)
			]
		}];
	},

	initComponent: function () {
		this.callParent(arguments);
		if (!this.fieldsDisabled) {
			Ext.getStore(this.cars_store_Id).load();
			Ext.getStore(this.car_drivers_store_Id).load();
		}
	}
});