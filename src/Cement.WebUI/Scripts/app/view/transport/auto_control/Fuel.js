Ext.define('Cement.view.transport.auto_control.Fuel', {
	extend: 'Cement.view.basic.Grid',
	alias: 'widget.transport_auto_control_fuel',

	gridStore: 'transport.auto_control.Fuel',
	gridStateId: 'stateTransportAutoControlFuel',

    bbarText: 'Показано топливо {0} - {1} из {2}',
    bbarEmptyMsg: 'Нет топлива',
    bbarUsersText: 'Топлива на странице: ',

	printUrl: Cement.Config.url.transport.auto_control.fuel.printGrid,
	helpUrl: Cement.Config.url.transport.auto_control.fuel.help,
    printItemUrl: Cement.Config.url.transport.auto_control.fuel.printItem,
    deleteUrl: Cement.Config.url.transport.auto_control.fuel.deleteUrl,

    getFilterItems: function () {
        return [{
            text: 'Дата',
            kind: 'selector',
            field_name: 'p_date',
            checked: true
        }, {
            text: 'Наименование',
            kind: 'selector',
            field_name: 'p_name',
            checked: true
        }, {
            text: 'Ед. изм.',
            kind: 'selector',
            field_name: 'p_unit',
            checked: true
        }, {
            text: 'Кол-во',
            kind: 'selector',
            field_name: 'p_count',
            checked: true
        }, {
            text: 'АЗС',
            kind: 'selector',
            field_name: 'p_station',
            checked: true
        }, {
            text: 'Адрес',
            kind: 'selector',
            field_name: 'p_address',
            checked: true
        }, {
            text: 'Стоимость',
            kind: 'selector',
            field_name: 'p_cost',
            checked: true
        }, {
            text: 'Документ',
            kind: 'selector',
            field_name: 'p_document',
            checked: true
        }, {
            text: 'Водитель',
            kind: 'selector',
            field_name: 'p_driver_display',
            checked: true
        }];
    },

    getGridColumns: function () {
        var result = [
            { text: 'Транспортное средство', dataIndex: 'p_transport_unit_display', width: 250, locked: true },
            { text: 'Дата', dataIndex: 'p_date', width: 150 },
            { text: 'Наименование', dataIndex: 'p_name', width: 150 },
            { text: 'Период', dataIndex: 'p_period', width: 150 },
            { text: 'Ед. изм.', dataIndex: 'p_unit', width: 150 },
            { text: 'Кол-во', dataIndex: 'p_count', width: 150 },
            { text: 'АЗС', dataIndex: 'p_station', width: 150 },
            { text: 'Адрес', dataIndex: 'p_address', width: 150 },
            { text: 'Стоимость', dataIndex: 'p_cost', width: 150 },
            { text: 'Документ', dataIndex: 'p_document', width: 150 },
            { text: 'Водитель', dataIndex: 'p_driver_display', width: 150 }
        ];
        return this.mergeActions(result);
    },

    initComponent: function () {
        Ext.apply(this, {
            title: 'Топливо'
        });
        this.callParent(arguments);
    }
});