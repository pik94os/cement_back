Ext.define('Cement.view.transport.auto_control.Part', {
	extend: 'Cement.view.basic.Grid',
	alias: 'widget.transport_auto_control_part',

	gridStore: 'transport.auto_control.Part',
	gridStateId: 'stateTransportAutoControlTrucks',

    bbarText: 'Показаны запчасти {0} - {1} из {2}',
    bbarEmptyMsg: 'Нет запчастей',
    bbarUsersText: 'Запчастей на странице: ',

	printUrl: Cement.Config.url.transport.auto_control.parts.printGrid,
	helpUrl: Cement.Config.url.transport.auto_control.parts.help,
    printItemUrl: Cement.Config.url.transport.auto_control.parts.printItem,
    deleteUrl: Cement.Config.url.transport.auto_control.parts.deleteUrl,

    getFilterItems: function () {
        return [{
            text: 'Дата',
            kind: 'selector',
            field_name: 'p_date',
            checked: true
        }, {
            text: 'Наименование',
            kind: 'selector',
            field_name: 'p_name',
            checked: true
        }, {
            text: 'Ед. изм.',
            kind: 'selector',
            field_name: 'p_unit',
            checked: true
        }, {
            text: 'Кол-во',
            kind: 'selector',
            field_name: 'p_count',
            checked: true
        }, {
            text: 'Пробег',
            kind: 'selector',
            field_name: 'p_odometer',
            checked: true
        }, {
            text: 'Автосервис',
            kind: 'selector',
            field_name: 'p_autoservice',
            checked: true
        }, {
            text: 'Адрес',
            kind: 'selector',
            field_name: 'p_address',
            checked: true
        }, {
            text: 'Мастер',
            kind: 'selector',
            field_name: 'p_master',
            checked: true
        }, {
            text: 'Дата начала',
            kind: 'selector',
            field_name: 'p_date_start',
            checked: true
        }, {
            text: 'Дата окончания',
            kind: 'selector',
            field_name: 'p_date_end',
            checked: true
        }, {
            text: 'Стоимость',
            kind: 'selector',
            field_name: 'p_cost',
            checked: true
        }, {
            text: 'Документ',
            kind: 'selector',
            field_name: 'p_document',
            checked: true
        }, {
            text: 'Водитель',
            kind: 'selector',
            field_name: 'p_driver_display',
            checked: true
        }];
    },

    getGridColumns: function () {
        var result = [
            { text: 'Транспортное средство', dataIndex: 'p_transport_unit_display', width: 250, locked: true },
            { text: 'Наименование', dataIndex: 'p_name', width: 150 },
            { text: 'Период', dataIndex: 'p_period', width: 150 },
            { text: 'Ед. изм.', dataIndex: 'p_unit', width: 150 },
            { text: 'Кол-во', dataIndex: 'p_count', width: 150 },
            { text: 'Пробег', dataIndex: 'p_odometer', width: 150 },
            { text: 'Автосервис', dataIndex: 'p_autoservice', width: 150 },
            { text: 'Адрес', dataIndex: 'p_address', width: 150 },
            { text: 'Мастер', dataIndex: 'p_master', width: 150 },
            { text: 'Дата начала', dataIndex: 'p_date_start', width: 150 },
            { text: 'Дата окончания', dataIndex: 'p_date_end', width: 150 },
            { text: 'Стоимость', dataIndex: 'p_cost', width: 150 },
            { text: 'Документ', dataIndex: 'p_document', width: 150 },
            { text: 'Водитель', dataIndex: 'p_driver_display', width: 150 }
        ];
        return this.mergeActions(result);
    },

    initComponent: function () {
        Ext.apply(this, {
            title: 'Запчасти/услуги'
        });
        this.callParent(arguments);
    }
});