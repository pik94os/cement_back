Ext.define('Cement.view.transport.auto_control.Odometer', {
	extend: 'Cement.view.basic.Grid',
	alias: 'widget.transport_auto_control_odometer',

	gridStore: 'transport.auto_control.Odometer',
	gridStateId: 'stateTransportAutoControlOdometer',

    bbarText: 'Показан пробег {0} - {1} из {2}',
    bbarEmptyMsg: 'Нет пробега',
    bbarUsersText: 'Пробега на странице: ',

	printUrl: Cement.Config.url.transport.auto_control.odometer.printGrid,
	helpUrl: Cement.Config.url.transport.auto_control.odometer.help,
    printItemUrl: Cement.Config.url.transport.auto_control.odometer.printItem,
    deleteUrl: Cement.Config.url.transport.auto_control.odometer.deleteUrl,

    getFilterItems: function () {
        return [{
            text: 'Дата фиксации',
            kind: 'selector',
            field_name: 'p_date',
            checked: true
        }, {
            text: 'Пробег',
            kind: 'selector',
            field_name: 'p_value',
            checked: true
        }];
    },

    getGridColumns: function () {
        var result = [
            { text: 'Транспортное средство', dataIndex: 'p_transport_unit_display', width: 250, locked: true },
            { text: 'Дата фиксации', dataIndex: 'p_date', width: 150 },
            { text: 'Пробег', dataIndex: 'p_value', width: 150 }
        ];
        return this.mergeActions(result);
    },

    initComponent: function () {
        Ext.apply(this, {
            title: 'Пробег'
        });
        this.callParent(arguments);
    }
});