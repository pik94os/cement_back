Ext.define('Cement.view.transport.auto_control.Crash', {
	extend: 'Cement.view.basic.Grid',
	alias: 'widget.transport_auto_control_crash',

	gridStore: 'transport.auto_control.Crash',
	gridStateId: 'stateTransportAutoControlCrash',

    bbarText: 'Показаны ДТП {0} - {1} из {2}',
    bbarEmptyMsg: 'Нет ДТП',
    bbarUsersText: 'ДТП на странице: ',

	printUrl: Cement.Config.url.transport.auto_control.crash.printGrid,
	helpUrl: Cement.Config.url.transport.auto_control.crash.help,
    printItemUrl: Cement.Config.url.transport.auto_control.crash.printItem,
    deleteUrl: Cement.Config.url.transport.auto_control.crash.deleteUrl,

    getFilterItems: function () {
        return [{
            text: 'Кол-во ТС',
            kind: 'selector',
            field_name: 'p_count',
            checked: true
        }, {
            text: 'Ущерб',
            kind: 'selector',
            field_name: 'p_damage',
            checked: true
        }, {
            text: 'Описание',
            kind: 'selector',
            field_name: 'p_comment',
            checked: true
        }, {
            text: 'Сумма',
            kind: 'selector',
            field_name: 'p_sum',
            checked: true
        }, {
            text: 'Водитель',
            kind: 'selector',
            field_name: 'p_driver_display',
            checked: true
        }];
    },

    getGridColumns: function () {
        var result = [
            { text: 'Транспортное средство', dataIndex: 'p_transport_unit_display', width: 250, locked: true },
            { text: 'Дата', dataIndex: 'p_date', width: 150 },
            { text: 'Кол-во ТС', dataIndex: 'p_count', width: 150 },
            { text: 'Ущерб', dataIndex: 'p_damage', width: 150 },
            { text: 'Описание', dataIndex: 'p_comment', width: 250 },
            { text: 'Сумма', dataIndex: 'p_sum', width: 150 },
            { text: 'Водитель', dataIndex: 'p_driver_display', width: 150 }
        ];
        return this.mergeActions(result);
    },

    initComponent: function () {
        Ext.apply(this, {
            title: 'ДТП'
        });
        this.callParent(arguments);
    }
});