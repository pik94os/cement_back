Ext.define('Cement.view.transport.auto_control.Insurance', {
	extend: 'Cement.view.basic.Grid',
	alias: 'widget.transport_auto_control_insurance',

	gridStore: 'transport.auto_control.Insurance',
	gridStateId: 'stateTransportAutoControlInsurance',

    bbarText: 'Показаны страховки {0} - {1} из {2}',
    bbarEmptyMsg: 'Нет страховок',
    bbarUsersText: 'Страховок на странице: ',

	printUrl: Cement.Config.url.transport.auto_control.insurance.printGrid,
	helpUrl: Cement.Config.url.transport.auto_control.insurance.help,
    printItemUrl: Cement.Config.url.transport.auto_control.insurance.printItem,
    deleteUrl: Cement.Config.url.transport.auto_control.insurance.deleteUrl,

    getFilterItems: function () {
        return [{
            text: 'Наименование',
            kind: 'selector',
            field_name: 'p_name',
            checked: true
        }, {
            text: 'Страховая компания',
            kind: 'selector',
            field_name: 'p_company',
            checked: true
        }, {
            text: 'Адрес',
            kind: 'selector',
            field_name: 'p_address',
            checked: true
        }, {
            text: 'Контактное лицо',
            kind: 'selector',
            field_name: 'p_contact',
            checked: true
        }, {
            text: 'Телефон',
            kind: 'selector',
            field_name: 'p_phone',
            checked: true
        }, {
            text: 'Номер страховки',
            kind: 'selector',
            field_name: 'p_number',
            checked: true
        }, {
            text: 'Дата начала',
            kind: 'selector',
            field_name: 'p_date_start',
            checked: true
        }, {
            text: 'Дата окончания',
            kind: 'selector',
            field_name: 'p_date_end',
            checked: true
        }, {
            text: 'Сумма страховки',
            kind: 'selector',
            field_name: 'p_sum',
            checked: true
        }];
    },

    getGridColumns: function () {
        var result = [
            { text: 'Транспортное средство', dataIndex: 'p_transport_unit_display', width: 250, locked: true },
            { text: 'Наименование', dataIndex: 'p_name', width: 150 },
            { text: 'Страховая компания', dataIndex: 'p_company', width: 150 },
            { text: 'Адрес', dataIndex: 'p_address', width: 150 },
            { text: 'Контактное лицо', dataIndex: 'p_contact', width: 150 },
            { text: 'Телефон', dataIndex: 'p_phone', width: 150 },
            { text: 'Номер страховки', dataIndex: 'p_number', width: 150 },
            { text: 'Дата начала', dataIndex: 'p_date_start', width: 150 },
            { text: 'Дата окончания', dataIndex: 'p_date_end', width: 150 },
            { text: 'Сумма страховки', dataIndex: 'p_sum', width: 150 }
        ];
        return this.mergeActions(result);
    },

    initComponent: function () {
        Ext.apply(this, {
            title: 'Страховки'
        });
        this.callParent(arguments);
    }
});