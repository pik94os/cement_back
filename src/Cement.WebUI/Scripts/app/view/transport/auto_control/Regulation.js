Ext.define('Cement.view.transport.auto_control.Regulation', {
	extend: 'Cement.view.basic.Grid',
	alias: 'widget.transport_auto_control_regulation',

	gridStore: 'transport.auto_control.Regulation',
	gridStateId: 'stateTransportAutoControlTrucks',

    bbarText: 'Показаны регламенты {0} - {1} из {2}',
    bbarEmptyMsg: 'Нет регламентов',
    bbarUsersText: 'Регламентов на странице: ',

	printUrl: Cement.Config.url.transport.auto_control.regulation.printGrid,
	helpUrl: Cement.Config.url.transport.auto_control.regulation.help,
    printItemUrl: Cement.Config.url.transport.auto_control.regulation.printItem,
    deleteUrl: Cement.Config.url.transport.auto_control.regulation.deleteUrl,

    getFilterItems: function () {
        return [{
            text: 'Дата',
            kind: 'selector',
            field_name: 'p_date',
            checked: true
        }, {
            text: 'Наименование',
            kind: 'selector',
            field_name: 'p_name',
            checked: true
        }, {
            text: 'Период',
            kind: 'selector',
            field_name: 'p_period',
            checked: true
        }, {
            text: 'Пробег',
            kind: 'selector',
            field_name: 'p_odometer',
            checked: true
        }];
    },

    getGridColumns: function () {
        var result = [
            { text: 'Транспортное средство', dataIndex: 'p_transport_unit_display', width: 250, locked: true },
            { text: 'Наименование', dataIndex: 'p_name', width: 150 },
            { text: 'Период', dataIndex: 'p_period', width: 150 },
            { text: 'Пробег', dataIndex: 'p_odometer', width: 150 }
        ];
        return this.mergeActions(result);
    },

    initComponent: function () {
        Ext.apply(this, {
            title: 'Регламент'
        });
        this.callParent(arguments);
    }
});