Ext.define('Cement.view.transport.auto_control.Maintenance', {
	extend: 'Cement.view.basic.Grid',
	alias: 'widget.transport_auto_control_maintenance',

	gridStore: 'transport.auto_control.Maintenance',
	gridStateId: 'stateTransportAutoControlTrucks',

    bbarText: 'Показано техобслуживание {0} - {1} из {2}',
    bbarEmptyMsg: 'Нет техобслуживания',
    bbarUsersText: 'Техобслуживаний на странице: ',

	printUrl: Cement.Config.url.transport.auto_control.maintenance.printGrid,
	helpUrl: Cement.Config.url.transport.auto_control.maintenance.help,
    printItemUrl: Cement.Config.url.transport.auto_control.maintenance.printItem,
    deleteUrl: Cement.Config.url.transport.auto_control.maintenance.deleteUrl,

    getFilterItems: function () {
        return [{
            text: 'Дата',
            kind: 'selector',
            field_name: 'p_date',
            checked: true
        }, {
            text: 'Наименование',
            kind: 'selector',
            field_name: 'p_transport_unit',
            checked: true
        }, {
            text: 'Пробег',
            kind: 'selector',
            field_name: 'p_odometer',
            checked: true
        }, {
            text: 'Автосервис',
            kind: 'selector',
            field_name: 'p_autoservice',
            checked: true
        }, {
            text: 'Адрес',
            kind: 'selector',
            field_name: 'p_address',
            checked: true
        }, {
            text: 'Мастер',
            kind: 'selector',
            field_name: 'p_master',
            checked: true
        }, {
            text: 'Дата выполнения',
            kind: 'selector',
            field_name: 'p_made_date',
            checked: true
        }, {
            text: 'Стоимость',
            kind: 'selector',
            field_name: 'p_cost',
            checked: true
        }, {
            text: 'Заметки',
            kind: 'selector',
            field_name: 'p_comment',
            checked: true
        }, {
            text: 'Документ',
            kind: 'selector',
            field_name: 'p_document',
            checked: true
        }, {
            text: 'Водитель',
            kind: 'selector',
            field_name: 'p_driver',
            checked: true
        }];
    },

    getGridColumns: function () {
        var result = [
            { text: 'Транспортное средство', dataIndex: 'p_transport_unit_display', width: 250, locked: true },
            { text: 'Дата', dataIndex: 'p_date', width: 150 },
            { text: 'Наименование', dataIndex: 'p_transport_unit_display', width: 150 },
            { text: 'Пробег', dataIndex: 'p_odometer', width: 150 },
            { text: 'Автосервис', dataIndex: 'p_sub_category', width: 150 },
            { text: 'Адрес', dataIndex: 'p_address', width: 150 },
            { text: 'Мастер', dataIndex: 'p_master', width: 150 },
            { text: 'Дата выполнения', dataIndex: 'p_made_date', width: 150 },
            { text: 'Стоимость', dataIndex: 'p_cost', width: 150 },
            { text: 'Заметки', dataIndex: 'p_comment', width: 150 },
            { text: 'Документ', dataIndex: 'p_document', width: 150 },
            { text: 'Водитель', dataIndex: 'p_driver_display', width: 150 }
        ];
        return this.mergeActions(result);
    },

    initComponent: function () {
        Ext.apply(this, {
            title: 'Техобслуживание'
        });
        this.callParent(arguments);
    }
});