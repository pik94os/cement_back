Ext.define('Cement.view.transport.timesheet.Total', {
    extend: 'Cement.view.basic.Grid',
    alias: 'widget.transport_timesheet_total',
    bbarText: 'Показаны записи {0} - {1} из {2}',
    bbarEmptyMsg: 'Нет записей',
    bbarUsersText: 'Записей на странице: ',
    gridStore: 'transport.Timesheet',
    printUrl: Cement.Config.url.transport.timesheet.printGrid,
    helpUrl: Cement.Config.url.transport.timesheet.help,
    deleteUrl: Cement.Config.url.transport.timesheet.deleteUrl,
    printItemUrl: Cement.Config.url.transport.timesheet.printItem,
    cls: 'simple-form',

    getFilterItems: function () {
        return [];
    },

    createBbar: function (combo) {
        return Ext.create('Ext.PagingToolbar', {
            store: this.gridStore,
            displayInfo: true,
            displayMsg: this.bbarText,
            emptyMsg: this.bbarEmptyMsg,
            beforePageText: "Страница",
            items: ['-',
                this.bbarUsersText,
                combo,
                { xtype: 'tbseparator' },
                {
                    xtype: 'container',
                    height: 16,
                    padding: "1 2 0 2",
                    html: "<div class='tb-icon timesheetkind-available'></div> Свободен"
                },
                {
                    xtype: 'container',
                    height: 16,
                    padding: "1 2 0 2",
                    html: "<div class='tb-icon timesheetkind-buzy'></div> Задействован"
                },
                {
                    xtype: 'container',
                    height: 16,
                    padding: "1 2 0 2",
                    html: "<div class='tb-icon timesheetkind-in_repair'></div> Ремонт"
                },
                {
                    xtype: 'container',
                    height: 16,
                    padding: "1 2 0 2",
                    html: "<div class='tb-icon timesheetkind-missing'></div> Отсутствует"
                }
            ]
        });
    },

    createMainComponent: function (bbar, lbar) {
        return {
            layout: 'fit',
            border: 0,
            bbar: bbar,
            lbar: lbar,
            items: [
                {
                    store: Ext.getStore(this.gridStore),
                    xtype: 'transport_timesheet_panel',
                    border: 0,
                    style: 'border-top: solid 1px #bcbcbc',
                    layout: 'fit'
                }
            ]
        };
    },
});