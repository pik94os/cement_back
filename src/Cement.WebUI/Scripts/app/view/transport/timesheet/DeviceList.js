﻿Ext.define('Cement.view.transport.timesheet.DeviceList', {
    extend: 'Ext.view.View',
    alias: 'widget.transport_timesheet_device_list',
    itemSelector: 'div.transport-unit',
    emptyText: 'Нет записей',
    loadMask: false,
    
    processItemEvent: function (record, item, index, e) {
        if (e.type == "mousedown" && e.button == 0) {
            if (this._selectedNode) {
                Ext.fly(this._selectedNode).removeCls(this.selectedItemCls);
            }
            this._deselectedNode = this._selectedNode;
            this._selectedNode = item;
            Ext.fly(this._selectedNode).addCls(this.selectedItemCls);
            this.fireEvent('select', record, item, index, e);
        }

        //if (Ext.fly(item).hasCls('x-item-child')) {
        //}
    },

    initComponent: function () {
        var me = this,
          listTpl = new Ext.XTemplate(
          '<tpl for=".">',
            '<div class="transport-unit" style="height: {[ this.getHeight(values) ]};">',
              '{p_name}<br/>{p_number}',
            '</div>',
          '</tpl>',
          {
              getHeight: function (val) {
                  return '34px; min-height: 34px;font-family: tahoma, arial, verdana, sans-serif;font-size: 11px;font-style: normal;font-variant: normal;font-weight: normal;text-overflow: ellipsis;white-space: nowrap;overflow-x: hidden;overflow-y: hidden;line-height: 13px; padding: 3px 6px 4px 6px; border-top: solid 1px #ddd';
                  var result = 0;
                  Ext.each(val.p_indicators, function (item) {
                      if (me.parentView.isIndicatorVisible && me.parentView.isIndicatorVisible(item.p_code)) {
                          if (item.kind == 'markers') {
                              result += 32;
                          }
                          else {
                              result += 7;
                          }
                      }
                  });
                  if (result > 0) {
                      result += 1;
                      if (result < 36) {
                          result = 36;
                      }
                      result += 'px';
                  }
                  else {
                      result = 'auto';
                  }
                  if (result < 20) {

                  }
                  return result;
              }
          }
        );
        Ext.apply(this, {
            tpl: listTpl
        });
        this.callParent(arguments);
    }
});