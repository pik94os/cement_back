function getScrollbarWidth() {
    var outer = document.createElement("div");
    outer.style.visibility = "hidden";
    outer.style.width = "100px";
    outer.style.msOverflowStyle = "scrollbar"; // needed for WinJS apps

    document.body.appendChild(outer);

    var widthNoScroll = outer.offsetWidth;
    // force scrollbars
    outer.style.overflow = "scroll";

    // add innerdiv
    var inner = document.createElement("div");
    inner.style.width = "100%";
    outer.appendChild(inner);

    var widthWithScroll = inner.offsetWidth;

    // remove divs
    outer.parentNode.removeChild(outer);

    return widthNoScroll - widthWithScroll;
}

Ext.define('Cement.view.transport.timesheet.Panel', {
    extend: 'Ext.panel.Panel',
    alias: 'widget.transport_timesheet_panel',
    xtype: 'transport_timesheet_panel',
    //title: 'Индикаторы',
    rangeKind: 'week',
    bodyCls: 'indicators-total-control',
    //cls: 'no-side-borders simple-form',

    initComponent: function () {
        var me = this;
        
        var tbar = {
            items: [
                '->', {
                    handler: this.onPrevClick,
                    scope: this,
                    iconCls: 'x-tbar-page-prev'
                }, {
                    text: 'Час',
                    handler: this.onHourClick,
                    scope: this,
                    toggleGroup: 'tb-views'
                }, {
                    text: 'День',
                    handler: this.onDayClick,
                    scope: this,
                    toggleGroup: 'tb-views'
                }, {
                    text: 'Неделя',
                    pressed: true,
                    handler: this.onWeekClick,
                    scope: this,
                    toggleGroup: 'tb-views'
                }, {
                    text: 'Месяц',
                    handler: this.onMonthClick,
                    scope: this,
                    toggleGroup: 'tb-views'
                }, {
                    handler: this.onNextClick,
                    scope: this,
                    iconCls: 'x-tbar-page-next'
                }, '->'
            ]
        };

        Ext.apply(this, {
            layout: {
                type: 'vbox',
                align: 'stretch',
                pack: 'start'
            },
            tbar: tbar,
            items: [
            {
                border: 0,
                height: 40,
                layout: {
                    type: 'hbox',
                    pack: 'start',
                    align: 'stretch'
                },
                items: [
                    {
                        xtype: 'panel',
                        style: 'background-color: #fff',
                        border: 0,
                        width: 200,
                        minWidth: 100
                    },
                    {
                        xtype: 'splitter',
                        style: 'background-color: #dcdcda;',
                        width: 1,
                        listeners: {
                            move: function (self, x) {
                                if (x == 200) {
                                    return;
                                }

                                var devList = Ext.ComponentQuery.query('transport_timesheet_device_list');

                                if (devList && devList.length) {
                                    devList[0].setWidth(x);
                                }
                            }
                        }
                    },
                    {
                        xtype: 'transport_timesheet_timeline',
                        store: me.store,
                        parentXtype: me.xtype,
                        flex: 1
                    }
                ]
            },
            {
                xtype: 'panel',
                style: 'background-color: #ebebeb; border-top: solid 1px #dcdcda',
                border: 0,
                height: 6
            },
            {
                xtype: 'panel',
                //autoScroll: true,
                overflowY: 'overlay',
                border: 0,
                flex: 1,
                layout: 'anchor',
                anchor: '-10, 0',
                items: [{
                    layout: {
                        type: 'hbox',
                        align: 'stretch',
                        pack: 'start'
                    },
                    border: 0,
                    items: [{
                        xtype: 'transport_timesheet_device_list',
                        store: me.store,
                        parentXtype: me.xtype,
                        width: 200,
                        listeners: {
                            select: me.selectUnit
                        }
                    },
                    {
                        style: 'background-color: #dcdcda;',
                        border: 0,
                        width: 1
                    },
                    {
                        xtype: 'transport_timesheet_bar_panel',
                        store: me.store,
                        parentXtype: me.xtype,
                        flex: 1
                    }]
                }]
            }]
        });

        this.startDate = new Date();
        this.startDate.setHours(0);
        this.startDate.setMinutes(0);
        this.startDate.setSeconds(0);
        this.setFactStartEnd();
        this.callParent(arguments);
    },

    selectUnit: function(record, item, index, e) {
        Cement.app.getController('Navigation').showComplexDetails('Cement.view.transport.units.View', record);
    },

    reload: function () {
        this.store.clearFilter(true);
        this.store.filter([
          { property: 'p_indicators', value: Ext.JSON.encode(this.visibleIndicators) },
          { property: 'p_date_start', value: Ext.Date.format(this.startDate, 'd.m.Y H') },
          { property: 'p_kind', value: this.rangeKind }
        ]);
    },

    getEndDate: function () {
        if (this.rangeKind == 'day') {
            return Ext.Date.add(this.startDate, Ext.Date.DAY, 1);
        }
        if (this.rangeKind == 'week') {
            return Ext.Date.add(this.startDate, Ext.Date.DAY, 7);
        }
        return Ext.Date.add(this.startDate, Ext.Date.MONTH, 1);
    },

    getLengthInterval: function () {
        return (this.endDateFact - this.startDateFact);
        // return Ext.Date.between(this.getEndDate(), this.startDate);
    },

    setFactStartEnd: function () {
        if (this.rangeKind == 'hour') {
            this.startDateFact = Ext.Date.subtract(this.startDate, Ext.Date.MINUTE, 15);
            this.endDateFact = Ext.Date.add(this.startDateFact, Ext.Date.MINUTE, 90);
        }
        else {
            this.startDate.setHours(0);
        }
        if (this.rangeKind == 'day') {
            this.startDateFact = Ext.Date.subtract(this.startDate, Ext.Date.HOUR, 6);
            this.endDateFact = Ext.Date.add(this.startDateFact, Ext.Date.HOUR, 36);
        }
        if (this.rangeKind == 'week') {
            var first = this.startDate.getDate() - this.startDate.getDay() + 1,
              first_dt = Ext.Date.add(this.startDate, Ext.Date.DAY, -this.startDate.getDay() + 1);
            this.startDateFact = Ext.Date.subtract(first_dt, Ext.Date.HOUR, 24);
            this.endDateFact = Ext.Date.add(this.startDateFact, Ext.Date.DAY, 9);
        }
        if (this.rangeKind == 'month') {
            this.startDateFact = Ext.Date.subtract(Ext.Date.getFirstDateOfMonth(this.startDate), Ext.Date.DAY, 4);
            this.endDateFact = Ext.Date.add(Ext.Date.add(this.startDateFact, Ext.Date.MONTH, 1), Ext.Date.DAY, 5);
        }

        this.startDateFact.setMilliseconds(0);
    },

    onPrevClick: function () {
        if (this.rangeKind == 'hour') {
            this.startDate = Ext.Date.subtract(this.startDate, Ext.Date.HOUR, 1);
        }
        if (this.rangeKind == 'day') {
            this.startDate = Ext.Date.subtract(this.startDate, Ext.Date.DAY, 1);
        }
        if (this.rangeKind == 'week') {
            this.startDate = Ext.Date.subtract(this.startDate, Ext.Date.DAY, 7);
        }
        if (this.rangeKind == 'month') {
            this.startDate = Ext.Date.subtract(this.startDate, Ext.Date.MONTH, 1);
        }
        this.setFactStartEnd();
        this.reload();
    },

    onNextClick: function () {
        if (this.rangeKind == 'hour') {
            this.startDate = Ext.Date.add(this.startDate, Ext.Date.HOUR, 1);
        }
        if (this.rangeKind == 'day') {
            this.startDate = Ext.Date.add(this.startDate, Ext.Date.DAY, 1);
        }
        if (this.rangeKind == 'week') {
            this.startDate = Ext.Date.add(this.startDate, Ext.Date.DAY, 7);
        }
        if (this.rangeKind == 'month') {
            this.startDate = Ext.Date.getFirstDateOfMonth(Ext.Date.add(this.startDate, Ext.Date.MONTH, 1));
        }
        this.setFactStartEnd();
        this.reload();
    },

    onHourClick: function () {
        this.rangeKind = 'hour';
        this.setFactStartEnd();
        this.reload();
    },

    onDayClick: function () {
        this.rangeKind = 'day';
        this.setFactStartEnd();
        this.reload();
    },

    onWeekClick: function () {
        this.rangeKind = 'week';
        this.setFactStartEnd();
        this.reload();
    },

    onMonthClick: function () {
        this.rangeKind = 'month';
        this.setFactStartEnd();
        this.reload();
    },

    setStartDate: function (dt) {
        this.startDate = dt;
        this.reload();
    }
});