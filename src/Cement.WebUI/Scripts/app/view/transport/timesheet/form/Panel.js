Ext.define('Cement.view.transport.timesheet.form.Panel', {
    extend: 'Cement.view.basic.Form',
    alias: 'widget.transport_timesheet_form',
    fieldsDisabled: false,
    url: Cement.Config.url.transport.timesheet.saveUrl,
    method: Cement.Config.url.transport.timesheet.saveMethod,
    printUrl: Cement.Config.url.transport.timesheet.printItem,
    layout: 'fit',

    api: {
        submit: function (form, onCompleteCallback, options) {
            var timesheetForm = options.form.owner,
                grid = timesheetForm.down('transport_timesheet_form_grid'),
                datefield = timesheetForm.down('datefield'),
                store = grid.getStore();

            store.sync({
                success: function () {
                    onCompleteCallback(true);
                    timesheetForm.reloadStore(store, datefield.getValue());
                },
                failure: function () {
                    onCompleteCallback(false);
                }
            });
        }
    },
    
    beforeSubmit: function (callback) {
        var me = this,
            grid = me.down('transport_timesheet_form_grid'),
            store = grid.getStore();

        Ext.each(store.getNewRecords(), function (rec) {
            rec.commit();
        });

        if (store.getModifiedRecords().length > 0) {
            callback.call();
        }
    },

    getItems: function () {
        return [
        {
            padding: 5,
            layout: {
                type: 'vbox',
                align: 'stretch'
            },
            xtype: 'container',
            border: 0,
            items: [
            {
                xtype: 'panel',
                bodyCls: 'custom-background',
                items: [
                    {
                        //padding: '5 5 0 5',
                        margin: '2 0 2 0',
                        fieldLabel: 'Дата',
                        name: 'p_date',
                        allowBlank: false,
                        xtype: 'datefield',
                        labelWidth: 150,
                        labelAlign: 'right',
                        editable: false,
                        listeners: {
                            change: this.onDateChange
                        }
                    }
                ]
            },
                    {
                        margin: '5 0 0 0',
                        xtype: 'transport_timesheet_form_grid',
                        layout: 'fit',
                        style: 'border: solid 1px #bcbcbc',
                        flex: '1'
                    }
            ]
        }];
    },

    getNavPanel: function () {
        return null;
    },

    onCreate: function () {
        var me = this,
            datefield = me.down('datefield');

        datefield.setMinValue(new Date());
        datefield.setValue(new Date());
    },

    reset: function () {
        // debugger;
    },

    reloadStore: function (store, date) {
        store.load({
            scope: this,
            params: {
                'p_filter': Ext.JSON.encode([{ property: 'p_date', value: Ext.Date.format(date, 'd.m.Y') }])
            },
            callback: function (records) {

                Ext.each(records, function (rec) {
                    if (!rec.get('p_accounting')) {
                        rec.set('p_accounting', Cement.Config.transport.timesheet_kinds.available);
                    }
                });
            }
        });
    },

    onDateChange: function (self, newValue) {
        var form = self.up('transport_timesheet_form'),
            store = form.down('transport_timesheet_form_grid').getStore();

        form.reloadStore(store, newValue);

        //var store = self.up().up().down('transport_timesheet_form_grid').getStore();

        //store.load({
        //    scope: this,
        //    params: {
        //        'p_filter' : Ext.JSON.encode([{ property: 'p_date', value: Ext.Date.format(newValue, 'd.m.Y') }])
        //    },
        //    callback: function (records) {

        //        Ext.each(records, function(rec) {
        //            if (!rec.get('p_accounting')) {
        //                rec.set('p_accounting', Cement.Config.transport.timesheet_kinds.available);
        //            }
        //        });
        //    }
        //});
    }
});