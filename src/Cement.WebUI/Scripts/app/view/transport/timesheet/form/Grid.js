Ext.define('Cement.view.transport.timesheet.form.Grid', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.transport_timesheet_form_grid',
    storeId: 'transport.timesheet.Units',

    mixins: [
        'Cement.view.basic.GridRenderers'
    ],

    plugins: [
        Ext.create('Ext.grid.plugin.CellEditing', {
             clicksToEdit: 1
        })
    ],

    columnRenderer: function (value) {
        var result = "<div class='permission-icon";

        switch (value) {
            case Cement.Config.transport.timesheet_kinds.available:
                result += " timesheetkind-available'></div> " + 'Свободен';
                break;

            case Cement.Config.transport.timesheet_kinds.buzy:
                result += " timesheetkind-buzy'></div> " + 'Задействован';
                break;

            case Cement.Config.transport.timesheet_kinds.in_repair:
                result += " timesheetkind-in_repair'></div> " + 'Ремонт';
                break;

            default:
                result += " timesheetkind-missing'></div> " + 'Отсутствует';
        }

        return result;
    },

    initComponent: function () {
        var me = this;

        me.columns = [
            {
                text: 'Транспортное средство',
                locked: true,
                columns: [
                    { text: 'Марка, модель', dataIndex: 'p_auto_mark_model', width: 150 },
                    { text: 'Гос. номер', dataIndex: 'p_auto_number', width: 80 }
                ]
            },
            {
                text: 'Прицепное трансп.средство',
                columns: [
                    { text: 'Марка, модель', dataIndex: 'p_auto_trailer', width: 150 },
                    { text: 'Гос. номер', dataIndex: 'p_auto_trailer_number', width: 80 }
                ]
            },
            { text: 'Водитель', dataIndex: 'p_driver_name', width: 150 },
            {
                text: 'Характеристики',
                columns: [
                    { text: 'Класс', dataIndex: 'p_class_display', flex: 1 },
                    { text: 'ДхШхВ, м', dataIndex: 'p_size_display', flex: 1 },
                    { text: 'Г/О, т/м<sup>3</sup>', dataIndex: 'p_go_display', flex: 1 },
                    { text: 'О', dataIndex: 'p_char_ops', width: 20, resizable: false, align: 'center', renderer: this.radioButtonRenderer1, tdCls: 'pseudoinput-column' },
                    { text: 'П', dataIndex: 'p_char_ops', width: 20, resizable: false, align: 'center', renderer: this.radioButtonRenderer2, tdCls: 'pseudoinput-column' },
                    { text: 'С', dataIndex: 'p_char_ops', width: 20, resizable: false, align: 'center', renderer: this.radioButtonRenderer3, tdCls: 'pseudoinput-column' },
                    { text: 'Тип кузова', dataIndex: 'p_char_type_display', flex: 1 },
                    { text: 'З', dataIndex: 'p_load_back', width: 20, resizable: false, renderer: this.checkBoxRenderer, align: 'center', tdCls: 'pseudoinput-column' },
                    { text: 'Б', dataIndex: 'p_load_left', width: 20, resizable: false, renderer: this.checkBoxRenderer, align: 'center', tdCls: 'pseudoinput-column' },
                    { text: 'В', dataIndex: 'p_load_top', width: 20, resizable: false, renderer: this.checkBoxRenderer, align: 'center', tdCls: 'pseudoinput-column' },
                    { text: 'Доп. опции', dataIndex: 'p_additional_options_display', flex: 1 },
                    { text: 'Доп. характеристики', dataIndex: 'p_additional_chars_display', flex: 1 }
                ]
            },
            {
                text: 'Учет',
                dataIndex: 'p_accounting',
                flex: 0.5,
                minWidth: 110,
                renderer: me.columnRenderer,
                editor: {
                    xtype: 'combobox',
                    anchor: '100%',
                    listConfig: {
                        getInnerTpl: function (displayField) {
                            return '<div class="permission-icon-combo timesheetkind-{flag}"></div> {' + displayField + '}';
                        }
                    },
                    triggerAction: 'all',
                    editable: false,
                    valueField: 'id',
                    displayField: 'text',
                    store: new Ext.data.SimpleStore({
                        fields: ['id', 'text', 'flag'],
                        data: [
                            [Cement.Config.transport.timesheet_kinds.available, 'Свободен', 'available'],
                            [Cement.Config.transport.timesheet_kinds.buzy, 'Задействован', 'buzy'],
                            [Cement.Config.transport.timesheet_kinds.in_repair, 'Ремонт', 'in_repair'],
                            [Cement.Config.transport.timesheet_kinds.missing, 'Отсутствует', 'missing']
                        ]
                    })
                }
            }
        ];

        Ext.each(me.columns, function(column) {
            column.height = 40;
        });
        
        me.store = Ext.getStore(me.storeId);
        me.callParent(arguments);
    },
});