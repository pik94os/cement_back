Ext.define('Cement.view.transport.timesheet.BarPanel', {
    extend: 'Ext.view.View',
    alias: 'widget.transport_timesheet_bar_panel',
    cls: 'navigation-indicators-grid',
    itemSelector: '.x-item',
    //overItemCls: 'x-item-over',
    //emptyText: 'Нет данных',
    //multiSelect: true,

    processItemEvent: function (record, item, index, e) {
        //if (Ext.fly(item).hasCls('x-item-child')) {
        //    if (e.type == "mousedown" && e.button == 0) {
        //        var indId = item.getAttribute('data-indicator-id'),
        //          valId = item.getAttribute('data-value-id');
        //        if (this._selectedNode) {
        //            Ext.fly(this._selectedNode).removeCls(this.selectedItemCls);
        //        }
        //        this._deselectedNode = this._selectedNode;
        //        this._selectedNode = item;
        //        Ext.fly(this._selectedNode).addCls(this.selectedItemCls);
        //        this.fireEvent('indicatorSelect', record, indId, valId);
        //    }
        //}
    },

    updateIndexes: function (startIndex, endIndex) {
        //var ns = this.all.elements,
        //    records = this.store.getRange(),
        //    i, j;
        //startIndex = startIndex || 0;
        //endIndex = endIndex || ((endIndex === 0) ? 0 : (ns.length - 1));
        //for (i = startIndex, j = startIndex - 1; i <= endIndex; i++) {
        //    if (!Ext.fly(ns[i]).is('.x-item-child')) {
        //        j++;
        //    }

        //    ns[i].viewIndex = i;
            
        //    ns[i].viewRecordId = records[j].internalId;
        //    if (!ns[i].boundView) {
        //        ns[i].boundView = this.id;
        //    }
        //}
    },

    setTableSize: function (sz) {
        this.tableSize = sz;
        this.refresh();
    },

    initComponent: function () {
        var
          me = this,
          imageTpl = new Ext.XTemplate(
            '<div>',
                '<table class="stripes" cellspacing="0" cellpadding="0" width="{[ this.getTableSize() ]}px;">',
                    '<tpl for=".">',
                        '<tr>',
                            '{[ this.renderRow(values) ]}',
                        '</tr>',
                    '</tpl>',
                    '<tr>',
                      '{[ this.renderTopTable() ]}',
                    '</tr>',
                '</table>',
              //'<tpl for=".">',
              //  //'<div class="indicator-value x-item indicator-{#}" style="height: {[ this.getHeight(values) ]}px; min-height: 34px;">',
              //   '<div class="indicator-value x-item" style="height: {[ this.getHeight(values) ]}px; min-height: 34px;">',
              //      '<tpl for="p_indicators">',
              //              '<div class="x-item x-item-child value" data-qtip="{p_tooltip}" data-value-id="{id}" style="{[ this.getStyleStr(values) ]}; height: 34px; background: {p_color};opacity: 0.3"></div>', 
              //          '</tpl>',
              //      '</div>',
              //'</tpl>',
            '</div>',
            {
                getRowCls: function (val) {
                    var result = 'row-stripes';
                    if (val.kind == 'markers') {
                        result = 'row-markers';
                    }
                    return result;
                },

                getPosition: function (val) {
                    var d = Ext.Date.parse(val.p_date_start, "d.m.Y H:i"),
                      len = me.parentView.getLengthInterval(),
                      delta = d - me.parentView.startDateFact;
                    return (delta / len * 100) + '%';
                },

                getStyleStr: function (val) {
                    var d = Ext.Date.parse(val.p_date_start, "d.m.Y H:i"),
                      d2 = Ext.Date.parse(val.p_date_end, "d.m.Y H:i"),
                      len = me.parentView.getLengthInterval(),
                      delta = d - me.parentView.startDateFact,
                      delta2 = d2 - d,
                      width = (delta2 / len * 100) + '%',
                      from = (delta / len * 100) + '%';
                    //debugger;
                    return 'left: ' + from + '; width: ' + width;//((me.parentView.getWidth() - 10) * delta2 / len) + 'px';
                },

                getHeight: function (val) {
                    return 34;
                    var result = 0;
                    Ext.each(val, function (item) {
                        if (me.parentView.isIndicatorVisible && me.parentView.isIndicatorVisible(item.p_code)) {
                            if (item.kind == 'markers') {
                                result += 16;
                            }
                            else {
                                result += 7;
                            }
                        }
                    });
                    if (result > 0) {
                        result += 1;
                        if (result < 47) {
                            result = 47;
                        }
                        // result += 'px';
                    }
                    else {
                        result = 'auto'
                    }
                    return result;
                },

                getTableSize: function () {
                    return me.tableSize;
                },
                
                getClsForDate: function (values, date) {
                    var res = Ext.Array.findBy(values, function (item) {
                        var d = Ext.Date.parse(item.p_date_start, "d.m.Y H:i"),
                            d2 = Ext.Date.parse(item.p_date_end, "d.m.Y H:i");

                        return d <= date && date <= d2;
                    });

                    if (res) {
                        switch (res.p_accounting) {
                            case 10:
                                return ' available';
                            case 20:
                                return ' buzy';
                            case 30:
                                return ' in_repair';
                            //case 40:
                            //    return ' missing';
                        }
                    }

                    return '';
                },

                renderTopTable: function () {
                    var result = '', startDt, cls, addTxt, i;

                    if (me.parentView.rangeKind == 'week') {
                        startDt = Ext.Date.add(me.parentView.startDate, Ext.Date.DAY, -me.parentView.startDate.getDay());
                        for (i = 0; i < 9; i++) {
                            cls = '';
                            if (i == 0 || i == 8) {
                                cls = 'indicator-filled';
                            }
                            var dt = Ext.Date.format(Ext.Date.add(startDt, Ext.Date.DAY, i), 'D d');
                            if (i == 0) {
                                dt = Ext.Date.format(Ext.Date.add(startDt, Ext.Date.DAY, i), 'M, d Y');
                            }
                            //result += '<th width="11.11111111111%" colspan="4" class="' + cls + '"><span class="head-text">' + dt + '</span></th>';
                            result += '<th width="11.11111111111%" colspan="4" class="' + cls + '"><span class="head-text"></span></th>';
                        }
                    } else if (me.parentView.rangeKind == 'month') {
                        startDt = Ext.Date.getFirstDateOfMonth(me.parentView.startDate);
                        var lastDt = Ext.Date.getLastDateOfMonth(me.parentView.startDate);
                        lastDt.setHours(23);
                        var days = Math.ceil((lastDt - startDt) / (60 * 60 * 24 * 1000)),
                            totaldays = days + 8,
                          txt1 = Ext.Date.format(me.parentView.startDateFact, 'M, Y'),
                          txt2 = Ext.Date.format(me.parentView.startDate, 'M, Y'),
                          txt3 = Ext.Date.format(me.parentView.endDateFact, 'M');
                        //result += '<th colspan="4" width="' + (4 / totaldays * 100) + '%"><span class="head-text">' + txt1 + '</span></th>';
                        //result += '<th colspan="' + days + '" width="' + (days / totaldays * 100) + '%" class="head-al-right"><span class="head-text">' + txt2 + '</span></th>';
                        //result += '<th colspan="4" width="' + (4 / totaldays * 100) + '%"><span class="head-text">' + txt3 + '</span></th>';

                        result += '<th colspan="4" width="' + (4 / totaldays * 100) + '%"><span class="head-text"></span></th>';
                        result += '<th colspan="' + days + '" width="' + (days / totaldays * 100) + '%" class="head-al-right"><span class="head-text"></span></th>';
                        result += '<th colspan="4" width="' + (4 / totaldays * 100) + '%"><span class="head-text"></span></th>';
                    } else if (me.parentView.rangeKind == 'day') {
                        cls = "indicator-filled";
                        addTxt = '';
                        var times = ['18:00', '00:00', '06:00', '12:00', '18:00', '00:00'];

                        for (i = 0; i < 6; i++) {
                            addTxt = '';
                            if (i == 0 || i == 5) {
                                cls = 'indicator-filled';
                            }
                            else {
                                cls = '';
                            }
                            if (i == 0) {
                                addTxt = Ext.Date.format(Ext.Date.add(me.parentView.startDate, Ext.Date.HOUR, -6), ' D, M d, Y');
                            }
                            if (i == 1) {
                                addTxt = Ext.Date.format(me.parentView.startDate, ' D, M d');
                            }
                            if (i == 5) {
                                addTxt = Ext.Date.format(Ext.Date.add(me.parentView.startDate, Ext.Date.HOUR, 30), ' D, M d');
                            }
                            //result += '<th width="16.66%" colspan="6" class="' + cls + '"><span class="head-text">' + times[i] + addTxt + '</span></th>';
                            result += '<th width="16.66%" colspan="6" class="' + cls + '"><span class="head-text"></span></th>';
                            // result += '<th>' + Ext.Date.format(Ext.Date.add(me.parentView.startDate, Ext.Date.DAY, 5 * i), 'd.m.Y') + '</th>';
                        }
                    } else if (me.parentView.rangeKind == 'hour') {
                        cls = "indicator-filled";
                        addTxt = '';

                        for (i = 0; i < 6; i++) {
                            addTxt = '';
                            if (i == 0 || i == 5) {
                                cls = 'indicator-filled';
                            }
                            else {
                                cls = '';
                            }
                            if (i == 0) {
                                addTxt = Ext.Date.format(Ext.Date.add(me.parentView.startDate, Ext.Date.MINUTE, -15), 'H.i D,M d, Y');
                            }
                            if (i == 1 || i == 5) {
                                addTxt = Ext.Date.format(Ext.Date.add(me.parentView.startDate, Ext.Date.MINUTE, 0), 'H.i D,M d');
                            }
                            if (i > 1 && i < 5) {
                                addTxt = Ext.Date.format(Ext.Date.add(me.parentView.startDate, Ext.Date.MINUTE, (i - 1) * 15), 'H.i');
                            }
                            //result += '<th width="16.66%" colspan="3" class="' + cls + '"><span class="head-text">' + addTxt + '</span></th>';
                            result += '<th width="16.66%" colspan="3" class="' + cls + '"><span class="head-text"></span></th>';
                        }
                    }
                    return result;
                },

                renderRow: function (values) {
                    var result = '',
                        cls,
                        now,
                        i,
                        height = 34,
                        start = me.parentView.startDateFact;
                        //height = values.length > 0 ? this.getHeight(values[0].p_indicators) * values.length : 0;

                    if (me.parentView.rangeKind == 'week') {
                        var firstDt = Ext.Date.add(me.parentView.startDate, Ext.Date.DAY, -me.parentView.startDate.getDay() + 1);
                        for (i = 0; i < 36; i++) {
                            cls = '';

                            if (i < 4 || i > 31) {
                                cls = 'indicator-filled';
                            }

                            cls += this.getClsForDate(values.p_indicators, Ext.Date.add(start, Ext.Date.HOUR, i * 6 + 3));
                            
                            if ((i + 1) % 4 == 0) {
                                cls += ' solid';
                            }
                            result += '<td style="height:' + height + 'px;" class="' + cls + '"></td>';
                        }
                    } else if (me.parentView.rangeKind == 'month') {
                        var startDt = Ext.Date.getFirstDateOfMonth(me.parentView.startDate),
                            lastDt = Ext.Date.getLastDateOfMonth(me.parentView.startDate);
                        lastDt.setHours(23);
                        var days = Math.ceil((lastDt - startDt) / (60 * 60 * 24 * 1000));
                        for (i = 0; i < days + 8; i++) {
                            cls = '';
                            if (i < 4 || i > days + 3) {
                                cls = 'indicator-filled';
                            }

                            cls += this.getClsForDate(values.p_indicators, Ext.Date.add(start, Ext.Date.HOUR, i * 24 + 12));

                            if (i == 3 || i == 3 + days) {
                                cls += ' solid';
                            }
                            result += '<td style="height:' + height + 'px;" class="' + cls + '"></td>';
                        }
                    } else if (me.parentView.rangeKind == 'day') {
                        for (i = 0; i < 36; i++) {
                            cls = '';
                            if (i < 6 || i > 29) {
                                cls = "indicator-filled";
                            }

                            cls += this.getClsForDate(values.p_indicators, Ext.Date.add(start, Ext.Date.HOUR, i + 0.5));

                            if ((i + 1) % 6 == 0) {
                                cls += ' solid';
                            }
                            result += '<td style="height:' + height + 'px;" class="' + cls + '"></td>';
                        }
                    } else if (me.parentView.rangeKind == 'hour') {
                        for (i = 0; i < 18; i++) {
                            cls = '';
                            if (i < 3 || i > 14) {
                                cls = "indicator-filled";
                            }

                            cls += this.getClsForDate(values.p_indicators, Ext.Date.add(start, Ext.Date.MINUTE, i * 5 + 2));

                            if ((i + 1) % 3 == 0) {
                                cls += ' solid';
                            }
                            result += '<td style="height:' + height + 'px;" class="' + cls + '"></td>';
                        }
                    }
                    return result;
                }
            }
        );
        Ext.apply(this, {
            tpl: imageTpl
        });
        this.callParent(arguments);
        this.on('afterrender', function () {
            this.parentView = this.up(this.parentXtype);
        }, this);
    }
});