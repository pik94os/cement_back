Ext.define('Cement.view.transport.timesheet.Timeline', {
    extend: 'Ext.view.View',
    alias: 'widget.transport_timesheet_timeline',
    cls: 'navigation-indicators-grid navigation-indicators-timeline',
    itemSelector: 'div.indicator-value',
    emptyText: '',
    loadMask: false,
    
    setTableSize: function (sz) {
        this.tableSize = sz;
        this.refresh();
    },

    initComponent: function () {
        var me = this,
          imageTpl = new Ext.XTemplate(
            '<div style="height: 40px; position: relative;">',
              '<table class="stripes" cellspacing="0" cellpadding="0" width="{[ this.getTableSize() ]}px;">',
                '<tr class="head">',
                  '{[ this.renderTopTable() ]}',
                '</tr>',
                '<tr>',
                  '{[ this.renderTable(values) ]}',
                '</tr>',
              '</table>',
            '</div>',
            '<tpl for=".">',
            '</tpl>',
            {
                getTableSize: function () {
                    return me.tableSize;
                },

                getHeight: function (val) {
                    return 34;
                    var result = 0;
                    Ext.each(val, function (item) {
                        if (me.parentView.isIndicatorVisible(item.p_code)) {
                            if (item.kind == 'markers') {
                                result += 16;
                            }
                            else {
                                result += 7;
                            }
                        }
                    });
                    if (result > 0) {
                        result += 1;
                        if (result < 47) {
                            result = 47;
                        }
                        // result += 'px';
                    }
                    else {
                        result = 'auto';
                    }
                    return result;
                },

                renderTopTable: function () {
                    var result = '', startDt, cls, addTxt, i;
                    
                    if (me.parentView.rangeKind == 'week') {
                        startDt = Ext.Date.add(me.parentView.startDate, Ext.Date.DAY, -me.parentView.startDate.getDay());
                        for (i = 0; i < 9; i++) {
                            cls = '';
                            if (i == 0 || i == 8) {
                                cls = 'indicator-filled';
                            }
                            var dt = Ext.Date.format(Ext.Date.add(startDt, Ext.Date.DAY, i), 'D d');
                            if (i == 0) {
                                dt = Ext.Date.format(Ext.Date.add(startDt, Ext.Date.DAY, i), 'M, d Y');
                            }
                            result += '<th width="11.11111111111%" colspan="4" class="' + cls + '"><span class="head-text">' + dt + '</span></th>';
                        }
                    } else if (me.parentView.rangeKind == 'month') {
                        startDt = Ext.Date.getFirstDateOfMonth(me.parentView.startDate);
                        var lastDt = Ext.Date.getLastDateOfMonth(me.parentView.startDate);
                        lastDt.setHours(23);
                        var days = Math.ceil((lastDt - startDt) / (60 * 60 * 24 * 1000)),
                            totaldays = days + 8,
                          txt1 = Ext.Date.format(me.parentView.startDateFact, 'M, Y'),
                          txt2 = Ext.Date.format(me.parentView.startDate, 'M, Y'),
                          txt3 = Ext.Date.format(me.parentView.endDateFact, 'M');
                        result += '<th colspan="4" width="' + (4 / totaldays * 100) + '%"><span class="head-text">' + txt1 + '</span></th>';
                        result += '<th colspan="' + days + '" width="' + (days / totaldays * 100) + '%" class="head-al-right"><span class="head-text">' + txt2 + '</span></th>';
                        result += '<th colspan="4" width="' + (4 / totaldays * 100) + '%"><span class="head-text">' + txt3 + '</span></th>';
                    } else if (me.parentView.rangeKind == 'day') {
                        cls = "indicator-filled";
                        addTxt = '';
                        var times = ['18:00', '00:00', '06:00', '12:00', '18:00', '00:00'];
                          
                        for (i = 0; i < 6; i++) {
                            addTxt = '';
                            if (i == 0 || i == 5) {
                                cls = 'indicator-filled';
                            }
                            else {
                                cls = '';
                            }
                            if (i == 0) {
                                addTxt = Ext.Date.format(Ext.Date.add(me.parentView.startDate, Ext.Date.HOUR, -6), ' D, M d, Y');
                            }
                            if (i == 1) {
                                addTxt = Ext.Date.format(me.parentView.startDate, ' D, M d');
                            }
                            if (i == 5) {
                                addTxt = Ext.Date.format(Ext.Date.add(me.parentView.startDate, Ext.Date.HOUR, 30), ' D, M d');
                            }
                            result += '<th width="16.66%" colspan="6" class="' + cls + '"><span class="head-text">' + times[i] + addTxt + '</span></th>';
                            // result += '<th>' + Ext.Date.format(Ext.Date.add(me.parentView.startDate, Ext.Date.DAY, 5 * i), 'd.m.Y') + '</th>';
                        }
                    } else if (me.parentView.rangeKind == 'hour') {
                        cls = "indicator-filled";
                        addTxt = '';

                        for (i = 0; i < 6; i++) {
                            addTxt = '';
                            if (i == 0 || i == 5) {
                                cls = 'indicator-filled';
                            }
                            else {
                                cls = '';
                            }

                            if (i == 0) {
                                addTxt = Ext.Date.format(Ext.Date.add(me.parentView.startDate, Ext.Date.MINUTE, -15), 'H.i D,M d, Y');
                            } else if (i == 1) {
                                addTxt = Ext.Date.format(Ext.Date.add(me.parentView.startDate, Ext.Date.MINUTE, 0), 'H.i D,M d');
                            } else if (i == 5) {
                                addTxt = Ext.Date.format(Ext.Date.add(me.parentView.startDate, Ext.Date.MINUTE, 60), 'H.i D,M d');
                            } else {
                                addTxt = Ext.Date.format(Ext.Date.add(me.parentView.startDate, Ext.Date.MINUTE, (i - 1) * 15), 'H.i');
                            }
                            result += '<th width="16.66%" colspan="3" class="' + cls + '"><span class="head-text">' + addTxt + '</span></th>';
                        }
                    }
                    return result;
                },

                renderTable: function (values) {
                    var result = '',
                        cls, now, i,
                        height = values.length > 0 ? this.getHeight(values[0].p_indicators) * values.length : 0;
                    
                    if (me.parentView.rangeKind == 'week') {
                        var firstDt = Ext.Date.add(me.parentView.startDate, Ext.Date.DAY, -me.parentView.startDate.getDay() + 1),
                        cur = new Date();
                        for (i = 0; i < 36; i++) {
                            cls = '';
                            var st = Ext.Date.add(firstDt, Ext.Date.HOUR, (i - 4) * 6),
                                end = Ext.Date.add(firstDt, Ext.Date.HOUR, (i - 3) * 6);
                            if (i < 4 || i > 31) {
                                cls = 'indicator-filled';
                            }
                            if ((i + 1) % 4 == 0) {
                                cls += ' solid';
                            }
                            if (cur >= st && cur <= end) {
                                //cls += ' now';
                            }
                            result += '<td style="height:' + height + 'px;" class="' + cls + '"></td>';
                        }
                    } else if (me.parentView.rangeKind == 'month') {
                        var startDt = Ext.Date.getFirstDateOfMonth(me.parentView.startDate),
                            lastDt = Ext.Date.getLastDateOfMonth(me.parentView.startDate);
                        now = Ext.Date.format(new Date(), 'd.m.Y');
                        lastDt.setHours(23);
                        var days = Math.ceil((lastDt - startDt) / (60 * 60 * 24 * 1000));
                        for (i = 0; i < days + 8; i++) {
                            cls = '';
                            if (i < 4 || i > days + 3) {
                                cls = 'indicator-filled';
                            }
                            if (i == 3 || i == 3 + days) {
                                cls += ' solid';
                            }
                            if (now == Ext.Date.format(Ext.Date.add(startDt, Ext.Date.DAY, (i - 4)), 'd.m.Y')) {
                                //cls += ' now';
                            }
                            result += '<td style="height:' + height + 'px;" class="' + cls + '"></td>';
                        }
                    } else if (me.parentView.rangeKind == 'day') {
                        cls = "indicator-day-filled";
                        now = Ext.Date.format(new Date(), 'd.m.Y.H');

                        for (i = 0; i < 36; i++) {
                            if (i < 6 || i > 29) {
                                cls = "indicator-filled";
                            }
                            else {
                                cls = "";
                            }
                            if (now == Ext.Date.format(Ext.Date.add(me.parentView.startDate, Ext.Date.HOUR, i - 6), 'd.m.Y.H')) {
                                //cls = 'now';
                            }
                            if ((i + 1) % 6 == 0) {
                                cls += ' solid';
                            }
                            result += '<td style="height:' + height + 'px;" class="' + cls + '"></td>';
                        }
                    } else if (me.parentView.rangeKind == 'hour') {
                        cls = "indicator-day-filled";
                        now = new Date();

                        for (i = 0; i < 18; i++) {
                            var start = Ext.Date.add(me.parentView.startDate, Ext.Date.MINUTE, (i - 3) * 5),
                              en = Ext.Date.add(me.parentView.startDate, Ext.Date.MINUTE, (i - 2) * 5);
                            if (i < 3 || i > 14) {
                                cls = "indicator-filled";
                            }
                            else {
                                cls = "";
                            }
                            if (now >= start && now <= en) {
                                //cls = 'now';
                            }
                            if ((i + 1) % 3 == 0) {
                                cls += ' solid';
                            }
                            result += '<td style="height:' + height + 'px;" class="' + cls + '"></td>';
                        }
                    }
                    return result;
                }
            }
        );
        Ext.apply(this, {
            tpl: imageTpl
        });
        this.callParent(arguments);
        this.on('afterrender', function () {
            this.parentView = this.up(this.parentXtype);
        }, this);
    }
});