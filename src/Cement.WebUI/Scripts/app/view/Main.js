Ext.define('Cement.view.Main', {
    extend: 'Ext.container.Container',
    requires: [
        'Ext.tab.Panel',
        'Ext.layout.container.Border',
        'Cement.view.corporate.startpage.Dashboard'
    ],

    xtype: 'app-main',

    layout: {
        type: 'border'
    },

    items: [
        {
            region: 'west',
            xtype: 'navigation',
            stateful: true,
            stateId: 'stateNavigation',
            title: 'КАТАЛОГ',
            titleAlign: 'center'
        },
        {
            region: 'center',
            xtype: 'tabpanel',
            id: 'master_region',
            cls: 'main-tabpanel',
            items: [{
                xtype: 'corporate_startpage_dashboard',
                closable: false
            }]
        },
        {
            region: 'north',
            xtype: 'container',
            id: 'heading',
            //html: '<div class="logo"><img src="' + Cement.Config.url.logo + '"/></div>',
            cls: 'app-header',
            height: 80,
            layout: {
                type: 'hbox',
                align: 'stretch'
            },
            items: [
                {
                    xtype: 'container',
                    html: '<div class="logo"><img src="' + Cement.Config.url.logo + '"/></div>',
                    flex: 1
                }, {
                    border: false,
                    padding: 5,
                    items: [{
                        xtype: 'button',
                        text: 'Выход',
                        padding: 5,
                        icon: '/content/img/door_in.png',
                        listeners: {
                            click: function() {
                                window.location.assign("/account/logout");
                            }
                        }

                    }]
                }
            ]
        },
        {
            region: 'east',
            xtype: 'panel',
            title: 'ДАННЫЕ',
            titleAlign: 'center',
            collapsible: true,
            collapsed: false,
            id: 'detail_region',
            width: 300,
            minWidth: 250,
            maxWidth: 700,
            split: true,
            border: 0,
            layout: 'fit'
        }
    ],

    initComponent: function() {
        this.callParent(arguments);
        var masterRegion = this.down('tabpanel[id=master_region]');
        var detailRegion = this.down('panel[id=detail_region]');
        var navigation = this.down('navigation');

        var masterRegionDoLayout = function() {
            masterRegion.getActiveTab().doLayout();
        };

        detailRegion.on('collapse', masterRegionDoLayout);
        detailRegion.on('resize', masterRegionDoLayout);
        detailRegion.on('expand', masterRegionDoLayout);

        navigation.on('collapse', masterRegionDoLayout);
        navigation.on('resize', masterRegionDoLayout);
        navigation.on('expand', masterRegionDoLayout);
    }
});