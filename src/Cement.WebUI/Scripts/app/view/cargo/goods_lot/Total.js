Ext.define('Cement.view.cargo.goods_lot.Total', {
	extend: 'Cement.view.basic.Tabs',
	title: 'Лот на груз',

	items: [{
		xtype: 'cargo_goods_lot_lists_requests',
		layout: 'fit',
		title: 'Заявки на товар'
	}, {
		xtype: 'cargo_goods_lot_complex_new',
		title: 'Новые'
	}, {
		xtype: 'cargo_goods_lot_complex_incoming',
		title: 'Входящие'
	}, {
		xtype: 'cargo_goods_lot_complex_outcoming',
		title: 'Исходящие'
	}, {
		xtype: 'cargo_goods_lot_complex_archive',
		title: 'Архив'
	}],

	initComponent: function () {
		Ext.apply(this, {
			title: 'Лот на груз'
		});
		this.callParent(arguments);
	}
});