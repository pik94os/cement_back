Ext.define('Cement.view.cargo.goods_lot.complex.Archive', {
	extend: 'Cement.view.basic.Complex',
	alias: 'widget.cargo_goods_lot_complex_archive',
    border: 0,
    enableDetail: false,
    autoLoadStore: false,
    priceId: null,
    bodyPadding: 0,
    padding: 0,
    header: false,
    title: null,
    rowsCount: 3,
    title: 'Архив',
    tabTitle: 'Архив',
    cls: 'no-side-borders',
    topXType: 'cargo_goods_lot_lists_archive',
    topDetailType: 'Cement.view.cargo.goods_lot.view.View',
    bottomTitle: 'Связанные документы',
    bottomTabs: [{
        title: 'Связанные документы',
        xtype: 'cargo_goods_lot_bottom_archive_connected_documents_incoming',
        detailType: 'Cement.view.goods.auto.view.ConnectedDocument',
        constantFilterParam: 'p_base_doc',
        childrenConstantFilterParam: 'p_base_doc',
        border: 0
    }],
    subBottomTitle: 'Справочник',
    subBottomTabs: [{
        title: 'Покупатель',
        shownTitle: 'Покупатель',
        xtype: 'cargo_goods_lot_bottom_archive_buyers',
        detailType: 'Cement.view.corporate.firm.View'
    }, {
        title: 'Продавец',
        shownTitle: 'Продавец',
        xtype: 'cargo_goods_lot_bottom_archive_sellers',
        detailType: 'Cement.view.corporate.firm.View'
    }, {
        title: 'Услуги',
        shownTitle: 'Услуги',
        xtype: 'cargo_goods_lot_bottom_archive_services',
        detailType: 'Cement.view.products.services.View'
    }, {
        title: 'Товары',
        shownTitle: 'Товары',
        xtype: 'cargo_goods_lot_bottom_archive_products',
        detailType: 'Cement.view.products.products.View'
    }, {
        title: 'Маршрут',
        shownTitle: 'Маршрут',
        xtype: 'cargo_goods_lot_bottom_archive_routes',
        detailType: 'Cement.view.products.price.RouteView'
    }]
});