Ext.define('Cement.view.cargo.goods_lot.view.View', {
	extend: 'Ext.panel.Panel',
	alias: 'widget.cargo_goods_lot_view_view',
	fieldsDisabled: true,
	buttons: null,
	border: 0,
	tbar: Cement.Config.modelViewButtons,
	layout: 'autocontainer',
	bbar: null,
	noUserFieldsetsControl: false,
	bodyCls: 'model-form',
	firmTabName: 'Арендатор',
	detailUrl: Cement.Config.url.cargo.goods_lot.detailUrl,

	mixins: [
		'Cement.view.basic.HasImg'
	],

	loadRecord: function (rec) {
		if (!rec.get) return;
		var me = this;
		me.setLoading(true);
		Ext.Ajax.request({
            url: Ext.String.format(me.detailUrl, rec.get('id')),
            success: function (response) {
            	var json = Ext.JSON.decode(response.responseText),
            		r = Ext.create('Cement.model.goods.Auto', json.request),
            		f = Ext.create('Cement.model.transport.Unit', json.unit),
            		s = Ext.create('Cement.model.products.Service', json.service);
            	me.down('goods_auto_view_view').loadRecord(r);
            	me.down('transport_unit_view').loadRecordObj(json.unit);
            	me.down('products_services_view').loadRecord(s);
				if (r.get('show_char')) {
					me.down('transport_auto_view fieldset[role=char_display]').show();
				}
				else {
					me.down('transport_auto_view fieldset[role=char_display]').hide();
				}
				me.setLoading(false);
            }
        });
	},

	initComponent: function ()  {
		var items = [{
			xtype: 'goods_auto_view_view',
			title: 'Заявка',
			layout: 'fit',
			tbar: null
		}, {
			xtype: 'products_services_view',
			title: 'Услуга',
			layout: 'fit',
			tbar: null
		}, {
			xtype: 'transport_unit_view',
			title: 'Транспортная единица',
			layout: 'fit',
			tbar: null
		}];
		Ext.apply(this, {
			items: [{
				xtype: 'panel',
				border: 0,
				layout: 'accordion',
				defaultType: 'textfield',
				anchor: '100%',
				bodyPadding: '0',
				autoScroll: true,
				oveflowX: 'hidden',
				defaults: {
					anchor: '100%',
					labelWidth: 110
				},
				items: items
			}]
		});
		this.callParent(arguments);
		this.setupImgCentering();
	}
});