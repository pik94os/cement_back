Ext.define('Cement.view.cargo.goods_lot.bottom.outcoming.Services', {
    extend: 'Cement.view.products.price.bottom.NewServices',
    alias: 'widget.cargo_goods_lot_bottom_outcoming_services',
    gridStore: 'cargo.goods_lot.bottom.outcoming.Services',
    gridStateId: 'stateCargoGoodsLotBottomOutcomingServices'
});