Ext.define('Cement.view.cargo.goods_lot.bottom.new_l.Routes', {
    extend: 'Cement.view.products.price.bottom.NewRoutes',
    alias: 'widget.cargo_goods_lot_bottom_new_routes',
    gridStore: 'cargo.goods_lot.bottom.new_l.Routes',
    gridStateId: 'stateCargoGoodsLotBottomNewRoutes'
});