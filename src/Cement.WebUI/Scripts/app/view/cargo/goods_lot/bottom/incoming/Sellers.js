Ext.define('Cement.view.cargo.goods_lot.bottom.incoming.Sellers', {
	extend: 'Cement.view.goods.auto.bottom.new_r.Senders',
	alias: 'widget.cargo_goods_lot_bottom_incoming_sellers',
	gridStore: 'cargo.goods_lot.bottom.incoming.Sellers',
	gridStateId: 'stateCargoGoodsLotBottomIncomingSellers',
	showFilterButton: false,

	printItemUrl: Cement.Config.url.cargo.sellers.printItem,
	printUrl: Cement.Config.url.cargo.sellers.printGrid,
	helpUrl: Cement.Config.url.cargo.sellers.help
});