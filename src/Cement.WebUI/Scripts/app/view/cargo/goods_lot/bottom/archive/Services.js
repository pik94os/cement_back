Ext.define('Cement.view.cargo.goods_lot.bottom.archive.Services', {
    extend: 'Cement.view.products.price.bottom.NewServices',
    alias: 'widget.cargo_goods_lot_bottom_archive_services',
    gridStore: 'cargo.goods_lot.bottom.archive.Services',
    gridStateId: 'stateCargoGoodsLotBottomArchiveServices'
});