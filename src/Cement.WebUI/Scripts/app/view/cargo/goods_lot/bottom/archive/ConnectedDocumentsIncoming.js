Ext.define('Cement.view.cargo.goods_lot.bottom.archive.ConnectedDocumentsIncoming', {
	extend: 'Cement.view.goods.auto.lists.ConnectedDocuments',
	alias: 'widget.cargo_goods_lot_bottom_archive_connected_documents_incoming',

	gridStore: 'cargo.goods_lot.bottom.archive.ConnectedDocumentsIncoming',
	gridStateId: 'stateCargoGoodsLotBottomArchiveConnectedDocumentsIncoming',

    printUrl: Cement.Config.url.cargo.goods_lot.documents.printGrid,
    helpUrl: Cement.Config.url.cargo.goods_lot.documents.help
});