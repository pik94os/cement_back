Ext.define('Cement.view.cargo.goods_lot.bottom.archive.Products', {
    extend: 'Cement.view.products.price.bottom.NewProducts',
    alias: 'widget.cargo_goods_lot_bottom_archive_products',
    gridStore: 'cargo.goods_lot.bottom.archive.Products',
    gridStateId: 'stateCargoGoodsLotBottomArchiveProducts'
});