Ext.define('Cement.view.cargo.goods_lot.bottom.outcoming.Routes', {
    extend: 'Cement.view.products.price.bottom.NewRoutes',
    alias: 'widget.cargo_goods_lot_bottom_outcoming_routes',
    gridStore: 'cargo.goods_lot.bottom.outcoming.Routes',
    gridStateId: 'stateCargoGoodsLotBottomOutcomingRoutes'
});