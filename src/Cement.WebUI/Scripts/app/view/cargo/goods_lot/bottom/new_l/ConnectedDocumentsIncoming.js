Ext.define('Cement.view.cargo.goods_lot.bottom.new_l.ConnectedDocumentsIncoming', {
	extend: 'Cement.view.goods.auto.lists.ConnectedDocuments',
	alias: 'widget.cargo_goods_lot_bottom_new_connected_documents_incoming',

	gridStore: 'cargo.goods_lot.bottom.new_l.ConnectedDocumentsIncoming',
	gridStateId: 'stateCargoGoodsLotBottomNewConnectedDocumentsIncoming',

    printUrl: Cement.Config.url.cargo.goods_lot.documents.printGrid,
    helpUrl: Cement.Config.url.cargo.goods_lot.documents.help
});