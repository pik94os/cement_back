Ext.define('Cement.view.cargo.goods_lot.bottom.new_l.Sellers', {
	extend: 'Cement.view.goods.auto.bottom.new_r.Senders',
	alias: 'widget.cargo_goods_lot_bottom_new_sellers',
	gridStore: 'cargo.goods_lot.bottom.new_l.Sellers',
	gridStateId: 'stateCargoGoodsLotBottomIncomingSellers',
	showFilterButton: false,

	printItemUrl: Cement.Config.url.cargo.sellers.printItem,
	printUrl: Cement.Config.url.cargo.sellers.printGrid,
	helpUrl: Cement.Config.url.cargo.sellers.help
});