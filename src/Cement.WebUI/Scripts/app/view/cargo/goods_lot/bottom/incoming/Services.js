Ext.define('Cement.view.cargo.goods_lot.bottom.incoming.Services', {
    extend: 'Cement.view.products.price.bottom.NewServices',
    alias: 'widget.cargo_goods_lot_bottom_incoming_services',
    gridStore: 'cargo.goods_lot.bottom.incoming.Services',
    gridStateId: 'stateCargoGoodsLotBottomIncomingServices'
});