Ext.define('Cement.view.cargo.goods_lot.bottom.new_l.Services', {
    extend: 'Cement.view.products.price.bottom.NewServices',
    alias: 'widget.cargo_goods_lot_bottom_new_services',
    gridStore: 'cargo.goods_lot.bottom.new_l.Services',
    gridStateId: 'stateCargoGoodsLotBottomNewServices'
});