Ext.define('Cement.view.cargo.goods_lot.bottom.incoming.Routes', {
    extend: 'Cement.view.products.price.bottom.NewRoutes',
    alias: 'widget.cargo_goods_lot_bottom_incoming_routes',
    gridStore: 'cargo.goods_lot.bottom.incoming.Routes',
    gridStateId: 'stateCargoGoodsLotBottomIncomingRoutes'
});