Ext.define('Cement.view.cargo.goods_lot.bottom.outcoming.Buyers', {
	extend: 'Cement.view.goods.auto.bottom.new_r.Senders',
	alias: 'widget.cargo_goods_lot_bottom_outcoming_buyers',
	gridStore: 'cargo.goods_lot.bottom.outcoming.Buyers',
	gridStateId: 'stateCargoGoodsLotBottomOutcomingBuyers',
	showFilterButton: false,

	printItemUrl: Cement.Config.url.cargo.buyers.printItem,
	printUrl: Cement.Config.url.cargo.buyers.printGrid,
	helpUrl: Cement.Config.url.cargo.buyers.help
});