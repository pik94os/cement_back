Ext.define('Cement.view.cargo.goods_lot.bottom.outcoming.Products', {
    extend: 'Cement.view.products.price.bottom.NewProducts',
    alias: 'widget.cargo_goods_lot_bottom_outcoming_products',
    gridStore: 'cargo.goods_lot.bottom.outcoming.Products',
    gridStateId: 'stateCargoGoodsLotBottomOutcomingProducts'
});