Ext.define('Cement.view.cargo.goods_lot.bottom.new_l.Products', {
    extend: 'Cement.view.products.price.bottom.NewProducts',
    alias: 'widget.cargo_goods_lot_bottom_new_products',
    gridStore: 'cargo.goods_lot.bottom.new_l.Products',
    gridStateId: 'stateCargoGoodsLotBottomNewProducts'
});