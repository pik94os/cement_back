Ext.define('Cement.view.cargo.goods_lot.bottom.outcoming.Sellers', {
	extend: 'Cement.view.goods.auto.bottom.new_r.Senders',
	alias: 'widget.cargo_goods_lot_bottom_outcoming_sellers',
	gridStore: 'cargo.goods_lot.bottom.outcoming.Sellers',
	gridStateId: 'stateCargoGoodsLotBottomOutcomingSellers',
	showFilterButton: false,

	printItemUrl: Cement.Config.url.cargo.sellers.printItem,
	printUrl: Cement.Config.url.cargo.sellers.printGrid,
	helpUrl: Cement.Config.url.cargo.sellers.help
});