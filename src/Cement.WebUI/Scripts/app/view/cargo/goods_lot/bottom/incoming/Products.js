Ext.define('Cement.view.cargo.goods_lot.bottom.incoming.Products', {
    extend: 'Cement.view.products.price.bottom.NewProducts',
    alias: 'widget.cargo_goods_lot_bottom_incoming_products',
    gridStore: 'cargo.goods_lot.bottom.incoming.Products',
    gridStateId: 'stateCargoGoodsLotBottomIncomingProducts'
});