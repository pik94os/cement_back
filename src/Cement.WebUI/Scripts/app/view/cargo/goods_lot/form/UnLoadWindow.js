Ext.define('Cement.view.cargo.goods_lot.form.UnLoadWindow', {
	extend: 'Cement.view.cargo.goods_lot.form.LoadWindow',
	xtype: 'cargo_goods_lot_form_unload_window',
	title: 'Выбор населенного пункта выгрузки',
	structure_storeId: 'cargo.goods_lot.auxiliary.UnLoadPlaces'
});