Ext.define('Cement.view.cargo.goods_lot.form.RequestWindow', {
	extend: 'Cement.view.goods.auto.form.RequestWindow',
	xtype: 'cargo_goods_lot_form_request_window',
	srcStoreId: 'cargo.goods_lot.auxiliary.Requests'
});