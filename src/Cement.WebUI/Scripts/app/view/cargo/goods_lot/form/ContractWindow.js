Ext.define('Cement.view.cargo.goods_lot.form.ContractWindow', {
	extend: 'Cement.view.common.SignWindow',
	title: 'Выбор договора и услуги',
	layout: {
		type: 'border'
	},
	width: 900,
	height: 400,
	hideCommentPanel: true,
	bbar: [{
		text: 'Сохранить',
		action: 'confirm'
	}, {
		text: 'Отмена',
		action: 'cancel'
	}],
	modal: true,
	leftFrameTitle: 'Договора и услуги',
	topGridTitle: 'Выбрать услугу',
	bottomGridTitle: 'Выбранная услуга',
	structure_storeId: 'cargo.goods_lot.auxiliary.Contracts',
	loadRightFromTree: true,
	storeFields: ['id', 'p_name', 'p_group', 'p_subgroup', 'p_trade_mark', 'p_manufacturer', 'p_unit',
		'p_tax', 'p_price', 'p_contract', 'p_contract_name'],
	displayField: 'p_name',
	singleSelection: true,

	sign: function (send) {
		var store = this.dstStore;
		if (store.getCount() > 0) {
			this.fireEvent('selected', store.getAt(0));
		}
	},

	getTopColumnsConfig: function () {
		return [
			{
				xtype: 'rowactions',
				hideable: false,
				resizeable: false,
				width: 18,
				locked: true,
				keepSelection: true,
				actions: [{ 
	                iconCls: 'icon-add-item', 
	                qtip: 'Добавить',
	                callback: this.addEmployee
	            }]
			},
			{ text: 'Наименование', dataIndex: 'p_name', locked: true, width: 200 },
			{ text: 'Группа', dataIndex: 'p_group', width: 150 },
			{ text: 'Подгруппа', dataIndex: 'p_subgroup', width: 150 },
			{ text: 'Торговая марка', dataIndex: 'p_trade_mark', width: 150 },
			{ text: 'Производитель', dataIndex: 'p_manufacturer', width: 150 },
			{ text: 'Ед. изм.', dataIndex: 'p_unit', width: 60 },
			{ text: 'Налог', dataIndex: 'p_tax', width: 60 },
			{ text: 'Цена', dataIndex: 'p_price', width: 60 }
		];
	},

	getBottomColumnsConfig: function () {
		return [
			{
				xtype: 'rowactions',
				hideable: false,
				resizeable: false,
				locked: true,
				width: 18,
				actions: [{ 
	                iconCls: 'icon-delete-item', 
	                qtip: 'Удалить', 
	                callback: this.removeEmployee
	            }]
			},
			{ text: 'Наименование', dataIndex: 'p_name', locked: true, width: 200 },
			{ text: 'Группа', dataIndex: 'p_group', width: 150 },
			{ text: 'Подгруппа', dataIndex: 'p_subgroup', width: 150 },
			{ text: 'Торговая марка', dataIndex: 'p_trade_mark', width: 150 },
			{ text: 'Производитель', dataIndex: 'p_manufacturer', width: 150 },
			{ text: 'Ед. изм.', dataIndex: 'p_unit', width: 60 },
			{ text: 'Налог', dataIndex: 'p_tax', width: 60 },
			{ text: 'Цена', dataIndex: 'p_price', width: 60 }
		];
	},

	getTopGridFilters: function () {
		return [{
            text: 'Выделить все',
            checked: true,
            checkHandler: this.toggleFilterSelection
        }, '-', {
            text: 'Наименование',
            kind: 'selector',
            field_name: 'p_name',
            checked: true
        }, {
            text: 'Группа',
            kind: 'selector',
            field_name: 'p_group',
            checked: false
        }, {
            text: 'Подгруппа',
            kind: 'selector',
            field_name: 'p_subgroup',
            checked: false
        }, {
            text: 'Торговая марка',
            kind: 'selector',
            field_name: 'p_trade_mark',
            checked: false
        }, {
            text: 'Производитель',
            kind: 'selector',
            field_name: 'p_manufacturer',
            checked: false
        }, {
            text: 'Ед. изм',
            kind: 'selector',
            field_name: 'p_unit',
            checked: false
        }, {
            text: 'Налог',
            kind: 'selector',
            field_name: 'p_tax',
            checked: false
        }, {
            text: 'Цена',
            kind: 'selector',
            field_name: 'p_price',
            checked: false
        }];
	},

	initComponent: function () {
		this.callParent(arguments);
		this.on('beforeclose', function () {
			this.hide();
			return false;
		});
		this.on('afterrender', function () {
			Ext.getStore(this.structure_storeId).getRootNode().expand();
		}, this);
	}
});