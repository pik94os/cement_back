Ext.define('Cement.view.cargo.goods_lot.form.DispatcherWindow', {
	extend: 'Cement.view.common.SignWindow',
	xtype: 'cargo_goods_lot_form_dispatcher_window',
	title: 'Выбор диспетчера',
	hideCommentPanel: true,

	bbar: [{
		text: 'Сохранить',
		action: 'confirm'
	}, {
		text: 'Отмена',
		action: 'cancel'
	}],
	leftFrameTitle: 'Диспетчер',
	topGridTitle: 'Выбрать диспетчера',
	bottomGridTitle: 'Выбранный диспетчер',
	structure_storeId: 'cargo.goods_lot.auxiliary.Dispatchers',
	loadRightFromTree: true,
	storeFields: ['id', 'p_name', 'p_address'],
	displayField: 'p_name',
	singleSelection: true,

	sign: function (send) {
		var store = this.down('*[role=sign-dst-grid]').getStore();
		if (store.getCount() > 0) {
			this.fireEvent('selected', store.getAt(0));
		}
	},

	closeWindow: function () {
		this.hide();
	},

	getTopColumnsConfig: function () {
		return [
			{
				xtype: 'rowactions',
				hideable: false,
				resizeable: false,
				width: 18,
				keepSelection: true,
				actions: [{ 
	                iconCls: 'icon-add-item', 
	                qtip: 'Добавить',
	                callback: this.addEmployee
	            }]
			},
			{ text: 'Наименование', dataIndex: 'p_name', flex: 1 },
			{ text: 'Адрес', dataIndex: 'p_address', flex: 1 }
		];
	},

	getBottomColumnsConfig: function () {
		return [
			{
				xtype: 'rowactions',
				hideable: false,
				resizeable: false,
				width: 18,
				actions: [{ 
	                iconCls: 'icon-delete-item', 
	                qtip: 'Удалить', 
	                callback: this.removeEmployee
	            }]
			},
			{ text: 'Наименование', dataIndex: 'p_name', flex: 1 },
			{ text: 'Адрес', dataIndex: 'p_address', flex: 1 }
		];
	},

	getTopGridFilters: function () {
		return [{
            text: 'Выделить все',
            checked: true,
            checkHandler: this.toggleFilterSelection
        }, '-', {
            text: 'Наименование',
            kind: 'selector',
            field_name: 'p_name',
            checked: true
        }, {
            text: 'Адрес',
            kind: 'selector',
            field_name: 'p_address',
            checked: true
        }];
	},

	initComponent: function () {
		this.callParent(arguments);
		this.on('beforeclose', function () {
			this.hide();
			return false;
		});
		this.on('afterrender', function () {
			Ext.getStore(this.structure_storeId).getRootNode().expand();
		}, this);
	}
});