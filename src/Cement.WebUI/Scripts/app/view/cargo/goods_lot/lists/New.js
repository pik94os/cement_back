Ext.define('Cement.view.cargo.goods_lot.lists.New', {
	extend: 'Cement.view.basic.Grid',
	alias: 'widget.cargo_goods_lot_lists_new',
    autoLoadStore: true,

    bbarText: 'Показаны лоты {0} - {1} из {2}',
    bbarEmptyMsg: 'Нет лотов',
    bbarUsersText: 'Лотов на странице: ',

	gridStore: 'cargo.goods_lot.New',
	gridStateId: 'stateCargoGoodsLotNew',

	printUrl: Cement.Config.url.cargo.goods_lot.new_l.printGrid,
	helpUrl: Cement.Config.url.cargo.goods_lot.new_l.help,
    deleteUrl: Cement.Config.url.cargo.goods_lot.new_l.deleteUrl,
    bidUrl: Cement.Config.url.cargo.goods_lot.new_l.bidUrl,

    shownTitle: null,

    getActionColumns: function () {
        return {
            xtype: 'rowactions',
            hideable: false,
            resizeable: false,
            locked: true,
            width: 46,
            actions: [
                {
                    iconCls: 'icon-edit-item',
                    qtip: 'Редактировать',
                    callback: this.editItem
                },
                {
                    iconCls: 'icon-delete-item',
                    qtip: 'Удалить',
                    callback: this.deleteItem
                },
                {
                    iconCls: 'icon-bid-item',
                    qtip: 'Заявить',
                    callback: this.bidItem
                }
            ],
            keepSelection: true
        };
    },

    bidItem: function () {
        var me = this;
        Ext.Msg.confirm('Внимание', 'Вы уверены?', function (btn) {
            if (btn == 'yes') {
                Ext.Ajax.request({
                    url: Ext.String.format(grid.up('basicgrid').bidUrl, record.get('id')),
                    success: function (response) {
                        var obj = Ext.decode(response.responseText);
                        if (obj.success) {
                            if (obj.msg) {
                                Cement.Msg.info(obj.msg);
                            }
                            else {
                                Cement.Msg.info('Элемент отправлен');
                            }
                            grid.getStore().load();
                        }
                        else {
                            Cement.Msg.error(obj.msg);
                        }
                    },
                    failure: function () {
                        Cement.Msg.error('Ошибка');
                    }
                });
            }
        });
    },

    getFilterItems: function () {
        return [{
            text: '№ П/п',
            kind: 'selector',
            field_name: 'p_number',
            checked: true
        }, {
            text: 'Дата',
            kind: 'selector',
            field_name: 'p_date',
            checked: true
        }, {
            text: 'Покупатель',
            kind: 'selector',
            field_name: 'p_buyer_name',
            checked: true
        }, {
            text: 'Из код',
            kind: 'selector',
            field_name: 'p_from_code',
            checked: true
        }, {
            text: 'Из регион',
            kind: 'selector',
            field_name: 'p_from_region',
            checked: true
        }, {
            text: 'Из населенный пункт',
            kind: 'selector',
            field_name: 'p_from_city',
            checked: true
        }, {
            text: 'Из КМ',
            kind: 'selector',
            field_name: 'p_from_km',
            checked: true
        }, {
            text: 'В код',
            kind: 'selector',
            field_name: 'p_to_code',
            checked: true
        }, {
            text: 'В регион',
            kind: 'selector',
            field_name: 'p_to_region',
            checked: true
        }, {
            text: 'В населенный пункт',
            kind: 'selector',
            field_name: 'p_to_city',
            checked: true
        }, {
            text: 'В КМ',
            kind: 'selector',
            field_name: 'p_to_km',
            checked: true
        }, {
            text: 'Транспортная единица',
            kind: 'selector',
            field_name: 'p_transport_name',
            checked: true
        }, {
            text: 'Транспорт ДхШхВ',
            kind: 'selector',
            field_name: 'p_transport_size_display',
            checked: true
        }, {
            text: 'Транспорт тип кузова',
            kind: 'selector',
            field_name: 'p_transport_char_type_display',
            checked: true
        }, {
            text: 'Транспорт доп опции',
            kind: 'selector',
            field_name: 'p_transport_additional_options_display',
            checked: true
        }, {
            text: 'Транспорт доп характеристики',
            kind: 'selector',
            field_name: 'p_transport_additional_chars_display',
            checked: true
        }, {
            text: 'Услуга ед. изм.',
            kind: 'selector',
            field_name: 'p_service_unit_display',
            checked: true
        }, {
            text: 'Услуга кол-во',
            kind: 'selector',
            field_name: 'p_service_count',
            checked: true
        }, {
            text: 'Услуга факт кол-во',
            kind: 'selector',
            field_name: 'p_service_fact_count',
            checked: true
        }, {
            text: 'Услуга цена',
            kind: 'selector',
            field_name: 'p_service_price',
            checked: true
        }, {
            text: 'Услуга налог',
            kind: 'selector',
            field_name: 'p_service_tax',
            checked: true
        }, {
            text: 'Услуга сумма',
            kind: 'selector',
            field_name: 'p_service_sum',
            checked: true
        }, {
            text: 'Условия оплаты',
            kind: 'selector',
            field_name: 'p_payment_rules_display',
            checked: true
        }, {
            text: 'Примечание',
            kind: 'selector',
            field_name: 'p_comment',
            checked: true
        }, {
            text: 'Статус',
            kind: 'selector',
            field_name: 'p_status_display',
            checked: true
        }];
    },

    getGridColumns: function () {
        var result = [
            { text: '№ П/п', dataIndex: 'p_number', width: 60, locked: true },
            { text: 'Дата', dataIndex: 'p_date', width: 60, locked: true },
            { text: 'Покупатель', dataIndex: 'p_buyer_name', width: 150, locked: true },
            {
                text: 'Из',
                columns: [
                    { text: 'Код', dataIndex: 'p_from_code', width: 40 },
                    { text: 'Регион', dataIndex: 'p_from_region', width: 150 },
                    { text: 'Населенный пункт', dataIndex: 'p_from_city', width: 150 },
                    { text: 'КМ', dataIndex: 'p_from_km', width: 30 }
                ]
            },
            {
                text: 'В',
                columns: [
                    { text: 'Код', dataIndex: 'p_to_code', width: 40 },
                    { text: 'Регион', dataIndex: 'p_to_region', width: 150 },
                    { text: 'Населенный пункт', dataIndex: 'p_to_city', width: 150 },
                    { text: 'КМ', dataIndex: 'p_to_km', width: 30 }
                ]
            },
            {
                text: 'Товар',
                columns: [
                    { text: 'Наименование', dataIndex: 'p_product_display', width: 150 },
                    { text: 'Сумма', dataIndex: 'p_product_package', width: 70 }
                ]
            },
            {
                text: 'Транспорт',
                columns: [
                    { text: 'Транспортная единица', dataIndex: 'p_transport_name', width: 100 },
                    { text: 'ДхШхВ', dataIndex: 'p_transport_size_display', width: 100 },
                    { text: 'О', dataIndex: 'p_transport_char_ops', width: 20, resizable: false, align: 'center', renderer: this.radioButtonRenderer1, tdCls: 'pseudoinput-column' },
                    { text: 'П', dataIndex: 'p_transport_char_ops', width: 20, resizable: false, align: 'center', renderer: this.radioButtonRenderer2, tdCls: 'pseudoinput-column' },
                    { text: 'С', dataIndex: 'p_transport_char_ops', width: 20, resizable: false, align: 'center', renderer: this.radioButtonRenderer3, tdCls: 'pseudoinput-column' },
                    { text: 'Тип кузова', dataIndex: 'p_transport_char_type_display', width: 100 },
                    { text: 'З', dataIndex: 'p_transport_load_back', width: 20, resizable: false, renderer: this.checkBoxRenderer, align: 'center', tdCls: 'pseudoinput-column' },
                    { text: 'Б', dataIndex: 'p_transport_load_left', width: 20, resizable: false, renderer: this.checkBoxRenderer, align: 'center', tdCls: 'pseudoinput-column' },
                    { text: 'В', dataIndex: 'p_transport_load_top', width: 20, resizable: false, renderer: this.checkBoxRenderer, align: 'center', tdCls: 'pseudoinput-column' },
                    { text: 'Доп. опции', dataIndex: 'p_transport_additional_options_display', width: 100 },
                    { text: 'Доп. характеристики', dataIndex: 'p_transport_additional_chars_display', width: 100 }
                ]
            },
            {
                text: 'Услуга',
                columns: [
                    { text: 'Ед. изм.', dataIndex: 'p_service_unit_display', width: 50 },
                    { text: 'Кол-во', dataIndex: 'p_service_count', width: 50 },
                    { text: 'Факт. кол-во', dataIndex: 'p_service_fact_count', width: 50 },
                    { text: 'Цена', dataIndex: 'p_service_price', width: 50 },
                    { text: 'Налог', dataIndex: 'p_service_tax', width: 50 },
                    { text: 'Сумма', dataIndex: 'p_service_sum', width: 50 }
                ]
            },
            { text: 'Условия оплаты', dataIndex: 'p_payment_rules_display', width: 150 },
            { text: 'Примечание', dataIndex: 'p_comment', width: 150 },
            { text: 'Статус', dataIndex: 'p_status_display', width: 150 }
        ];
        return this.mergeActions(result);
    },

    initComponent: function () {
        Ext.apply(this, {
            title: this.shownTitle
        });
        this.callParent(arguments);
    }
});