Ext.define('Cement.view.cargo.goods_lot.lists.Requests', {
	extend: 'Cement.view.goods.auto.lists.New',
	alias: 'widget.cargo_goods_lot_lists_requests',

	gridStore: 'cargo.goods_lot.Requests',
	gridStateId: 'stateCargoGoodsLotRequests',

    showCreateButton: false,

    printUrl: Cement.Config.url.cargo.goods_lot.requests.printGrid,
    helpUrl: Cement.Config.url.cargo.goods_lot.requests.help,
    deleteUrl: Cement.Config.url.cargo.goods_lot.requests.deleteUrl,
    signItemUrl: Cement.Config.url.cargo.goods_lot.requests.signUrl,

    shownTitle: 'Заявки',

    getActionColumns: function () {
        // return {
        //     xtype: 'checkcolumn',
        //     resizeable: false,
        //     locked: true,
        //     width: 30,
        //     dataIndex: 'p_checked',
        //     text: '',
        //     listeners: {
        //         checkchange: this.checkChanged
        //     }
        // };
        return {
            xtype: 'rowactions',
            hideable: false,
            locked: true,
            resizeable: false,
            width: 46,
            actions: [{
                iconCls: 'icon-add-item', 
                qtip: 'Создать лот', 
                callback: this.createLot
            }],
            keepSelection: true
        };
    },

    createLot: function (grid, record) {
        var rec = Ext.create('Cement.model.cargo.GoodsLot');
        rec.set('p_from_display', record.get('p_from_display'));
        rec.set('p_from', record.get('p_from'));
        rec.set('p_to_display', record.get('p_to_display'));
        rec.set('p_to', record.get('p_to'));
        grid.up('basicgrid').fireEvent('edititem', rec, rec.$className);
    },

    checkChanged: function (col, rowIndex, checked) {
        var grid = col.up('grid'),
            store = grid.getStore(),
            model = store.getAt(rowIndex),
            bgrid = col.up('basicgrid'),
            ids = [];
        if (!bgrid.disableCheck) {
            if (checked) {
                bgrid.checkedItems.add(model);
            }
            else {
                bgrid.checkedItems.remove(model);
            }
            bgrid.checkedItems.each(function (item) {
                ids.push(item.get('id'));
            });
            bgrid.disableCheck = true;
            store.clearFilter(true);
            store.filter('p_ids', ids);
        }
    },

    // createItem: function () {
    //     var record = Ext.create('Cement.model.cargo.GoodsLot');
    //     record.set('p_from_display', 'ad');
    //     record.set('p_from', 'ad');
    //     this.fireEvent('edititem', record, Ext.getStore(this.gridStore).model.$className);
    // },

    initComponent: function () {
        this.callParent(arguments);
        // this.disableCheck = false;
        // this.checkedItems = Ext.create('Ext.util.MixedCollection');
        // var store = Ext.getStore(this.gridStore);
        // store.on('load', function () {
        //     this.checkedItems.each(function (item) {
        //         store.getById(item.get('id')).set('p_checked', true);
        //     });
        //     this.disableCheck = false;
        // }, this);
    }
});