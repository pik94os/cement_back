Ext.define('Cement.view.cargo.participants.lists.Current', {
	extend: 'Cement.view.basic.Grid',
	alias: 'widget.cargo_participants_lists_current',
    autoLoadStore: true,

    bbarText: 'Показаны участники {0} - {1} из {2}',
    bbarEmptyMsg: 'Нет участников',
    bbarUsersText: 'Участников на странице: ',

	gridStore: 'cargo.participants.Current',
	gridStateId: 'stateCargoParticipantsCurrent',

	printUrl: Cement.Config.url.cargo.participants.current.printGrid,
	helpUrl: Cement.Config.url.cargo.participants.current.help,
    deleteUrl: Cement.Config.url.cargo.participants.current.deleteUrl,
    archiveItemUrl: Cement.Config.url.cargo.participants.current.archiveUrl,

    shownTitle: null,

    getActionColumns: function () {
        return {
            xtype: 'rowactions',
            hideable: false,
            resizeable: false,
            locked: true,
            width: 46,
            actions: [
                {
                    iconCls: 'icon-archive-item',
                    qtip: 'В архив',
                    callback: this.archiveItem
                }
            ],
            keepSelection: true
        };
    },

    getFilterItems: function () {
        return [{
            text: 'Тип сессии',
            kind: 'selector',
            field_name: 'p_session_display',
            checked: true
        }, {
            text: 'Группа',
            kind: 'selector',
            field_name: 'p_group_display',
            checked: true
        }, {
            text: 'Наименование',
            kind: 'selector',
            field_name: 'p_name',
            checked: true
        }, {
            text: 'Адрес',
            kind: 'selector',
            field_name: 'p_address',
            checked: true
        }, {
            text: 'КПП',
            kind: 'selector',
            field_name: 'p_kpp',
            checked: true
        }, {
            text: 'ИНН',
            kind: 'selector',
            field_name: 'p_inn',
            checked: true
        }, {
            text: 'ОГРН',
            kind: 'selector',
            field_name: 'p_ogrn',
            checked: true
        }, {
            text: 'Дата начала',
            kind: 'selector',
            field_name: 'p_date_start',
            checked: true
        }, {
            text: 'Дата окончания',
            kind: 'selector',
            field_name: 'p_date_end',
            checked: true
        }];
    },

    getGridColumns: function () {
        var result = [
            { text: 'Тип сессии', dataIndex: 'p_session_display', width: 90, locked: true },
            { text: 'Группа', dataIndex: 'p_group_display', width: 90, locked: true },
            { text: 'Наименование', dataIndex: 'p_name', width: 120, locked: true },
            { text: 'Адрес', dataIndex: 'p_address', width: 150 },
            { text: 'КПП', dataIndex: 'p_kpp', width: 150 },
            { text: 'ИНН', dataIndex: 'p_inn', width: 150 },
            { text: 'ОГРН', dataIndex: 'p_ogrn', width: 150 },
            { text: 'Дата начала', dataIndex: 'p_date_start', width: 150 },
            { text: 'Дата окончания', dataIndex: 'p_date_end', width: 150 }
        ];
        return this.mergeActions(result);
    },

    initComponent: function () {
        Ext.apply(this, {
            title: this.shownTitle
        });
        this.callParent(arguments);
    }
});