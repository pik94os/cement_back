Ext.define('Cement.view.cargo.participants.bottom.archive.Employees', {
	extend: 'Cement.view.corporate.employee.List',
	gridStore: 'cargo.participants.bottom.archive.Employees',
	gridStateId: 'stateCargoParticipantsBottomArchiveEmployeesList',
	xtype: 'cargo_participants_bottom_archive_employees',
	tbar: null,
	showBottomBar: false,
	autoLoadStore: false,
	cls: 'nested-form',
	getToolbarItems: function () { return null; },
	getGridFeatures: function () { return null },
	getActionColumns: function () { return null; }
});