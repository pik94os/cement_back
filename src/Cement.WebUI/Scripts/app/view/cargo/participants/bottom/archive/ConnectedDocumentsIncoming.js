Ext.define('Cement.view.cargo.participants.bottom.archive.ConnectedDocumentsIncoming', {
	extend: 'Cement.view.goods.auto.lists.ConnectedDocuments',
	alias: 'widget.cargo_participants_bottom_archive_connected_documents_incoming',

	gridStore: 'cargo.participants.bottom.archive.ConnectedDocumentsIncoming',
	gridStateId: 'stateCargoParticipantsBottomArchiveConnectedDocumentsIncoming',

    printUrl: Cement.Config.url.cargo.participants.documents.printGrid,
    helpUrl: Cement.Config.url.cargo.participants.documents.help
});