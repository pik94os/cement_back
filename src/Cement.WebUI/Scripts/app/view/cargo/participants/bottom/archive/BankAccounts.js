Ext.define('Cement.view.cargo.participants.bottom.archive.BankAccounts', {
	extend: 'Cement.view.corporate.bank_account.List',
	gridStore: 'cargo.participants.bottom.archive.BankAccounts',
	gridStateId: 'stateCargoParticipantsBottomArchiveBankAccountsList',
	xtype: 'cargo_participants_bottom_archive_bank_accounts',
	tbar: null,
	showBottomBar: false,
	cls: 'nested-form',
	autoLoadStore: false,
	getToolbarItems: function () { return null; },
	getGridFeatures: function () { return null },
	getActionColumns: function () { return null; }
});