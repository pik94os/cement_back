Ext.define('Cement.view.cargo.participants.bottom.new_p.Warehouses', {
	extend: 'Cement.view.corporate.warehouse.List',
	gridStore: 'cargo.participants.bottom.new_p.Warehouses',
	gridStateId: 'stateCargoParticipantsBottomNewWarehousesList',
	xtype: 'cargo_participants_bottom_new_warehouses',
	tbar: null,
    showBottomBar: false,
    cls: 'nested-form',
	autoLoadStore: false,
	getToolbarItems: function () { return null; },
    getGridFeatures: function () { return null },
	getGridColumns: function () {
        return [
            { text: 'Наименование',  dataIndex: 'p_name', flex: 2 },
            { text: 'Адрес', dataIndex: 'p_address', flex: 1 },
            { text: 'Код предприятия', dataIndex: 'p_firm_code', flex: 1 },
            { text: 'Станция', dataIndex: 'p_station_display', flex: 1 },
            { text: 'Код станции', dataIndex: 'p_station_code', flex: 1 },
            { text: 'Дорога', dataIndex: 'p_road', flex: 1 }
        ];
    }
});