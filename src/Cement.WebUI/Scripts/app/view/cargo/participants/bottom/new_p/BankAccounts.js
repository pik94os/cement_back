Ext.define('Cement.view.cargo.participants.bottom.new_p.BankAccounts', {
	extend: 'Cement.view.corporate.bank_account.List',
	gridStore: 'cargo.participants.bottom.new_p.BankAccounts',
	gridStateId: 'stateCargoParticipantsBottomNewBankAccountsList',
	xtype: 'cargo_participants_bottom_new_bank_accounts',
	tbar: null,
	showBottomBar: false,
	cls: 'nested-form',
	autoLoadStore: false,
	getToolbarItems: function () { return null; },
	getGridFeatures: function () { return null },
	getActionColumns: function () { return null; }
});