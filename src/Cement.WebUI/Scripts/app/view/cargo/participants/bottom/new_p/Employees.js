Ext.define('Cement.view.cargo.participants.bottom.new_p.Employees', {
	extend: 'Cement.view.corporate.employee.List',
	gridStore: 'cargo.participants.bottom.new_p.Employees',
	gridStateId: 'stateCargoParticipantsBottomNewEmployeesList',
	xtype: 'cargo_participants_bottom_new_employees',
	tbar: null,
	showBottomBar: false,
	autoLoadStore: false,
	cls: 'nested-form',
	getToolbarItems: function () { return null; },
	getGridFeatures: function () { return null },
	getActionColumns: function () { return null; }
});