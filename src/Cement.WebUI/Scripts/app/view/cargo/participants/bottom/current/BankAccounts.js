Ext.define('Cement.view.cargo.participants.bottom.current.BankAccounts', {
	extend: 'Cement.view.corporate.bank_account.List',
	gridStore: 'cargo.participants.bottom.current.BankAccounts',
	gridStateId: 'stateCargoParticipantsBottomCurrentBankAccountsList',
	xtype: 'cargo_participants_bottom_current_bank_accounts',
	tbar: null,
	showBottomBar: false,
	cls: 'nested-form',
	autoLoadStore: false,
	getToolbarItems: function () { return null; },
	getGridFeatures: function () { return null },
	getActionColumns: function () { return null; }
});