Ext.define('Cement.view.cargo.participants.bottom.new_p.Signatures', {
	extend: 'Cement.view.corporate.signature.List',
	gridStore: 'cargo.participants.bottom.new_p.Signatures',
	gridStateId: 'stateCargoParticipantsNewSignaturesList',
	xtype: 'cargo_participants_bottom_new_signatures',
	tbar: null,
	showBottomBar: false,
	autoLoadStore: false,
	cls: 'nested-form',
	getToolbarItems: function () { return null; },
	getGridFeatures: function () { return null },
	getActionColumns: function () { return null; }
});