Ext.define('Cement.view.cargo.participants.bottom.current.ConnectedDocumentsIncoming', {
	extend: 'Cement.view.goods.auto.lists.ConnectedDocuments',
	alias: 'widget.cargo_participants_bottom_current_connected_documents_incoming',

	gridStore: 'cargo.participants.bottom.current.ConnectedDocumentsIncoming',
	gridStateId: 'stateCargoParticipantsBottomCurrentConnectedDocumentsIncoming',

    printUrl: Cement.Config.url.cargo.participants.documents.printGrid,
    helpUrl: Cement.Config.url.cargo.participants.documents.help
});