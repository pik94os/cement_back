Ext.define('Cement.view.cargo.participants.bottom.current.Employees', {
	extend: 'Cement.view.corporate.employee.List',
	gridStore: 'cargo.participants.bottom.current.Employees',
	gridStateId: 'stateCargoParticipantsBottomCurrentEmployeesList',
	xtype: 'cargo_participants_bottom_current_employees',
	tbar: null,
	showBottomBar: false,
	autoLoadStore: false,
	cls: 'nested-form',
	getToolbarItems: function () { return null; },
	getGridFeatures: function () { return null },
	getActionColumns: function () { return null; }
});