Ext.define('Cement.view.cargo.participants.bottom.archive.Signatures', {
	extend: 'Cement.view.corporate.signature.List',
	gridStore: 'cargo.participants.bottom.archive.Signatures',
	gridStateId: 'stateCargoParticipantsArchiveSignaturesList',
	xtype: 'cargo_participants_bottom_archive_signatures',
	tbar: null,
	showBottomBar: false,
	autoLoadStore: false,
	cls: 'nested-form',
	getToolbarItems: function () { return null; },
	getGridFeatures: function () { return null },
	getActionColumns: function () { return null; }
});