Ext.define('Cement.view.cargo.participants.bottom.current.Signatures', {
	extend: 'Cement.view.corporate.signature.List',
	gridStore: 'cargo.participants.bottom.current.Signatures',
	gridStateId: 'stateCargoParticipantsCurrentSignaturesList',
	xtype: 'cargo_participants_bottom_current_signatures',
	tbar: null,
	showBottomBar: false,
	autoLoadStore: false,
	cls: 'nested-form',
	getToolbarItems: function () { return null; },
	getGridFeatures: function () { return null },
	getActionColumns: function () { return null; }
});