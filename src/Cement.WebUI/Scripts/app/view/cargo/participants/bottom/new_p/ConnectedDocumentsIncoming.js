Ext.define('Cement.view.cargo.participants.bottom.new_p.ConnectedDocumentsIncoming', {
	extend: 'Cement.view.goods.auto.lists.ConnectedDocuments',
	alias: 'widget.cargo_participants_bottom_new_connected_documents_incoming',

	gridStore: 'cargo.participants.bottom.new_p.ConnectedDocumentsIncoming',
	gridStateId: 'stateCargoParticipantsBottomNewConnectedDocumentsIncoming',

    printUrl: Cement.Config.url.cargo.participants.documents.printGrid,
    helpUrl: Cement.Config.url.cargo.participants.documents.help
});