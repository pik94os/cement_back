Ext.define('Cement.view.cargo.participants.Total', {
	extend: 'Cement.view.basic.Tabs',
	title: 'Участники',

	items: [{
		xtype: 'cargo_participants_complex_new',
		title: 'Новые',
		shownTitle: 'Новые',
		detailType: ''
	}, {
		xtype: 'cargo_participants_complex_current',
		title: 'Действующие',
		shownTitle: 'Действующие',
		detailType: ''
	}, {
		xtype: 'cargo_participants_complex_archive',
		title: 'Архив',
		shownTitle: 'Архив',
		detailType: ''
	}],

	initComponent: function () {
		Ext.apply(this, {
			title: 'Участники'
		});
		this.callParent(arguments);
	}
});