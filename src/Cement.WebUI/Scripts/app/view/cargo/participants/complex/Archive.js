Ext.define('Cement.view.cargo.participants.complex.Archive', {
	extend: 'Cement.view.basic.Complex',
	alias: 'widget.cargo_participants_complex_archive',
    border: 0,
    enableDetail: false,
    autoLoadStore: false,
    priceId: null,
    bodyPadding: 0,
    padding: 0,
    header: false,
    title: null,
    rowsCount: 3,
    title: 'Архив',
    tabTitle: 'Архив',
    cls: 'no-side-borders',
    topXType: 'cargo_participants_lists_archive',
    topDetailType: '',
    bottomTitle: 'Связанные документы',
    bottomTabs: [{
        title: 'Связанные документы',
        xtype: 'cargo_participants_bottom_archive_connected_documents_incoming',
        detailType: 'Cement.view.goods.auto.view.ConnectedDocument',
        constantFilterParam: 'p_base_doc',
        childrenConstantFilterParam: 'p_base_doc',
        border: 0
    }],
    subBottomTitle: 'Справочник',
    subBottomTabs: [{
        title: 'Реквизиты счетов',
        xtype: 'cargo_participants_bottom_archive_bank_accounts',
        detailType: 'Cement.view.corporate.bank_account.View'
    }, {
        title: 'Склады',
        xtype: 'cargo_participants_bottom_archive_warehouses',
        detailType: 'Cement.view.corporate.warehouse.View'
    }, {
        title: 'Сотрудники',
        xtype: 'cargo_participants_bottom_archive_employees',
        detailType: 'Cement.view.corporate.employee.View'
    }, {
        title: 'Подписи',
        xtype: 'cargo_participants_bottom_archive_signatures',
        detailType: 'Cement.view.corporate.signature.View'
    }]
});