Ext.define('Cement.view.cargo.participants.complex.New', {
	extend: 'Cement.view.basic.Complex',
	alias: 'widget.cargo_participants_complex_new',
    border: 0,
    enableDetail: false,
    autoLoadStore: false,
    priceId: null,
    bodyPadding: 0,
    padding: 0,
    header: false,
    title: null,
    rowsCount: 3,
    title: 'Новые',
    tabTitle: 'Новые',
    cls: 'no-side-borders',
    topXType: 'cargo_participants_lists_new',
    topDetailType: '',
    bottomTitle: 'Связанные документы',
    bottomTabs: [{
        title: 'Связанные документы',
        xtype: 'cargo_participants_bottom_new_connected_documents_incoming',
        detailType: 'Cement.view.goods.auto.view.ConnectedDocument',
        constantFilterParam: 'p_base_doc',
        childrenConstantFilterParam: 'p_base_doc',
        border: 0
    }],
    subBottomTitle: 'Справочник',
    subBottomTabs: [{
        title: 'Реквизиты счетов',
        xtype: 'cargo_participants_bottom_new_bank_accounts',
        detailType: 'Cement.view.corporate.bank_account.View'
    }, {
        title: 'Склады',
        xtype: 'cargo_participants_bottom_new_warehouses',
        detailType: 'Cement.view.corporate.warehouse.View'
    }, {
        title: 'Сотрудники',
        xtype: 'cargo_participants_bottom_new_employees',
        detailType: 'Cement.view.corporate.employee.View'
    }, {
        title: 'Подписи',
        xtype: 'cargo_participants_bottom_new_signatures',
        detailType: 'Cement.view.corporate.signature.View'
    }]
});