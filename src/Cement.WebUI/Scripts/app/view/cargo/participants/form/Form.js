Ext.define('Cement.view.cargo.participants.form.Form', {
	extend: 'Cement.view.basic.Form',
	alias: 'widget.cargo_participants_form_form',
	fieldsDisabled: false,
	url: Cement.Config.url.cargo.participants.new_p.saveUrl,
	method: Cement.Config.url.cargo.participants.new_p.saveMethod,
	printUrl: Cement.Config.url.cargo.participants.new_p.printItem,
	isFilter: false,
	layout: 'fit',
	allowBlankFields: true,
	showSaveAndSignButton: true,
	saveButtonDisabled: false,

	getItems: function () {
		return [{
				xtype: 'fieldset',
				border: 0,
				margin: 0,
				defaults: {
					xtype: 'textfield',
					anchor: '100%',
                    labelWidth: 200,
					labelAlign: 'right'
				},
				items: [{
					xtype: 'radiogroup',
					fieldLabel: 'Сессия',
					columns: 1,
					items: [{
						name: 'p_session',
						inputValue: Cement.Config.cargo.sessions.corporate,
						boxLabel: 'Корпоративная'
					}, {
						name: 'p_session',
						inputValue: Cement.Config.cargo.sessions.contract,
						boxLabel: 'Контрактная'
					}, {
						name: 'p_session',
						inputValue: Cement.Config.cargo.sessions.common,
						boxLabel: 'Общая'
					}]
				}, this.getSelectorField('Наименование', 'p_client', 'show_client_window'), {
					name: 'p_contract_display',
					fieldLabel: 'Договор',
					disabled: true
				}, {
					name: 'p_contract',
					xtype: 'hiddenfield'
				}, {
					name: 'p_date_start',
					fieldLabel: 'Дата начала',
					xtype: 'datefield'
				}, {
					name: 'p_date_end',
					fieldLabel: 'Дата окончания',
					xtype: 'datefield'
				}]
            }
        ];
	},

	initComponent: function () {
		this.callParent(arguments);

		this.down('button[action=show_client_window]').on('click', function () {
			this.createClientWindow();
			this.clientWindow.show();
		}, this);
	},

	createClientWindow: function () {
		if (!this.clientWindow) {
			this.clientWindow = Ext.create('Cement.view.cargo.participants.form.ClientWindow');
			this.clientWindow.on('signitem', function (rec) {
				this.down('textfield[name=p_client_display]').setValue(rec.get('p_name'));
				this.down('hiddenfield[name=p_client]').setValue(rec.get('id'));
				this.clientWindow.hide();
			}, this);
		}
	},

	afterRecordLoad: function (rec) {
		this.createClientWindow();
	}
});