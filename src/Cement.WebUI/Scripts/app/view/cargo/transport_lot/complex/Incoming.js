Ext.define('Cement.view.cargo.transport_lot.complex.Incoming', {
	extend: 'Cement.view.basic.Complex',
	alias: 'widget.cargo_transport_lot_complex_incoming',
    border: 0,
    enableDetail: false,
    autoLoadStore: false,
    priceId: null,
    bodyPadding: 0,
    padding: 0,
    header: false,
    title: null,
    rowsCount: 3,
    title: 'Входящие',
    tabTitle: 'Входящие',
    cls: 'no-side-borders',
    topXType: 'cargo_transport_lot_lists_incoming',
    topDetailType: 'Cement.view.cargo.goods_lot.view.View',
    bottomTitle: 'Связанные документы',
    bottomTabs: [{
        title: 'Связанные документы',
        xtype: 'cargo_transport_lot_bottom_incoming_connected_documents_incoming',
        detailType: 'Cement.view.goods.auto.view.ConnectedDocument',
        constantFilterParam: 'p_base_doc',
        childrenConstantFilterParam: 'p_base_doc',
        border: 0
    }],
    subBottomTitle: 'Справочник',
    subBottomTabs: [{
        title: 'Покупатель',
        shownTitle: 'Покупатель',
        xtype: 'cargo_transport_lot_bottom_incoming_buyers',
        detailType: 'Cement.view.corporate.firm.View'
    }, {
        title: 'Продавец',
        shownTitle: 'Продавец',
        xtype: 'cargo_transport_lot_bottom_incoming_sellers',
        detailType: 'Cement.view.corporate.firm.View'
    }, {
        title: 'Услуги',
        shownTitle: 'Услуги',
        xtype: 'cargo_transport_lot_bottom_incoming_services',
        detailType: 'Cement.view.products.services.View'
    }, {
        title: 'Товары',
        shownTitle: 'Товары',
        xtype: 'cargo_transport_lot_bottom_incoming_products',
        detailType: 'Cement.view.products.products.View'
    }, {
        title: 'Маршрут',
        shownTitle: 'Маршрут',
        xtype: 'cargo_transport_lot_bottom_incoming_routes',
        detailType: 'Cement.view.products.price.RouteView'
    }]
});