Ext.define('Cement.view.cargo.transport_lot.bottom.archive.Products', {
    extend: 'Cement.view.products.price.bottom.NewProducts',
    alias: 'widget.cargo_transport_lot_bottom_archive_products',
    gridStore: 'cargo.transport_lot.bottom.archive.Products',
    gridStateId: 'stateCargoTransportLotBottomArchiveProducts'
});