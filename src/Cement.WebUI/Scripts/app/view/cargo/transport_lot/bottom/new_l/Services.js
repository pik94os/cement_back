Ext.define('Cement.view.cargo.transport_lot.bottom.new_l.Services', {
    extend: 'Cement.view.products.price.bottom.NewServices',
    alias: 'widget.cargo_transport_lot_bottom_new_services',
    gridStore: 'cargo.transport_lot.bottom.new_l.Services',
    gridStateId: 'stateCargoTransportLotBottomNewServices'
});