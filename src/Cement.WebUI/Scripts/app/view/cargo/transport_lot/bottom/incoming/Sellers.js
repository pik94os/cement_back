Ext.define('Cement.view.cargo.transport_lot.bottom.incoming.Sellers', {
	extend: 'Cement.view.goods.auto.bottom.new_r.Senders',
	alias: 'widget.cargo_transport_lot_bottom_incoming_sellers',
	gridStore: 'cargo.transport_lot.bottom.incoming.Sellers',
	gridStateId: 'stateCargoTransportLotBottomIncomingSellers',
	showFilterButton: false,

	printItemUrl: Cement.Config.url.cargo.sellers.printItem,
	printUrl: Cement.Config.url.cargo.sellers.printGrid,
	helpUrl: Cement.Config.url.cargo.sellers.help
});