Ext.define('Cement.view.cargo.transport_lot.bottom.incoming.Products', {
    extend: 'Cement.view.products.price.bottom.NewProducts',
    alias: 'widget.cargo_transport_lot_bottom_incoming_products',
    gridStore: 'cargo.transport_lot.bottom.incoming.Products',
    gridStateId: 'stateCargoTransportLotBottomIncomingProducts'
});