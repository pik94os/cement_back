Ext.define('Cement.view.cargo.transport_lot.bottom.archive.Services', {
    extend: 'Cement.view.products.price.bottom.NewServices',
    alias: 'widget.cargo_transport_lot_bottom_archive_services',
    gridStore: 'cargo.transport_lot.bottom.archive.Services',
    gridStateId: 'stateCargoTransportLotBottomArchiveServices'
});