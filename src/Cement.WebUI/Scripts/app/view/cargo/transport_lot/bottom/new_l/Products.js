Ext.define('Cement.view.cargo.transport_lot.bottom.new_l.Products', {
    extend: 'Cement.view.products.price.bottom.NewProducts',
    alias: 'widget.cargo_transport_lot_bottom_new_products',
    gridStore: 'cargo.transport_lot.bottom.new_l.Products',
    gridStateId: 'stateCargoTransportLotBottomNewProducts'
});