Ext.define('Cement.view.cargo.transport_lot.bottom.outcoming.Routes', {
    extend: 'Cement.view.products.price.bottom.NewRoutes',
    alias: 'widget.cargo_transport_lot_bottom_outcoming_routes',
    gridStore: 'cargo.transport_lot.bottom.outcoming.Routes',
    gridStateId: 'stateCargoTransportLotBottomOutcomingRoutes'
});