Ext.define('Cement.view.cargo.transport_lot.bottom.outcoming.Services', {
    extend: 'Cement.view.products.price.bottom.NewServices',
    alias: 'widget.cargo_transport_lot_bottom_outcoming_services',
    gridStore: 'cargo.transport_lot.bottom.outcoming.Services',
    gridStateId: 'stateCargoTransportLotBottomOutcomingServices'
});