Ext.define('Cement.view.cargo.transport_lot.bottom.incoming.Services', {
    extend: 'Cement.view.products.price.bottom.NewServices',
    alias: 'widget.cargo_transport_lot_bottom_incoming_services',
    gridStore: 'cargo.transport_lot.bottom.incoming.Services',
    gridStateId: 'stateCargoTransportLotBottomIncomingServices'
});