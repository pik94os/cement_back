Ext.define('Cement.view.cargo.transport_lot.bottom.incoming.Routes', {
    extend: 'Cement.view.products.price.bottom.NewRoutes',
    alias: 'widget.cargo_transport_lot_bottom_incoming_routes',
    gridStore: 'cargo.transport_lot.bottom.incoming.Routes',
    gridStateId: 'stateCargoTransportLotBottomIncomingRoutes'
});