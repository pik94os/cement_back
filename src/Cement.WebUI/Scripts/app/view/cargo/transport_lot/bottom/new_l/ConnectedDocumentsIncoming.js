Ext.define('Cement.view.cargo.transport_lot.bottom.new_l.ConnectedDocumentsIncoming', {
	extend: 'Cement.view.goods.auto.lists.ConnectedDocuments',
	alias: 'widget.cargo_transport_lot_bottom_new_connected_documents_incoming',

	gridStore: 'cargo.transport_lot.bottom.new_l.ConnectedDocumentsIncoming',
	gridStateId: 'stateCargoTransportLotBottomNewConnectedDocumentsIncoming',

    printUrl: Cement.Config.url.cargo.transport_lot.documents.printGrid,
    helpUrl: Cement.Config.url.cargo.transport_lot.documents.help
});