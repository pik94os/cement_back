Ext.define('Cement.view.cargo.transport_lot.bottom.outcoming.Products', {
    extend: 'Cement.view.products.price.bottom.NewProducts',
    alias: 'widget.cargo_transport_lot_bottom_outcoming_products',
    gridStore: 'cargo.transport_lot.bottom.outcoming.Products',
    gridStateId: 'stateCargoTransportLotBottomOutcomingProducts'
});