Ext.define('Cement.view.cargo.transport_lot.bottom.archive.Routes', {
    extend: 'Cement.view.products.price.bottom.NewRoutes',
    alias: 'widget.cargo_transport_lot_bottom_archive_routes',
    gridStore: 'cargo.transport_lot.bottom.archive.Routes',
    gridStateId: 'stateCargoTransportLotBottomArchiveRoutes'
});