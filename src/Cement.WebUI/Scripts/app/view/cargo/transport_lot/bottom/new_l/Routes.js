Ext.define('Cement.view.cargo.transport_lot.bottom.new_l.Routes', {
    extend: 'Cement.view.products.price.bottom.NewRoutes',
    alias: 'widget.cargo_transport_lot_bottom_new_routes',
    gridStore: 'cargo.transport_lot.bottom.new_l.Routes',
    gridStateId: 'stateCargoTransportLotBottomNewRoutes'
});