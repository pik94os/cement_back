Ext.define('Cement.view.cargo.transport_lot.bottom.outcoming.Sellers', {
	extend: 'Cement.view.goods.auto.bottom.new_r.Senders',
	alias: 'widget.cargo_transport_lot_bottom_outcoming_sellers',
	gridStore: 'cargo.transport_lot.bottom.outcoming.Sellers',
	gridStateId: 'stateCargoTransportLotBottomOutcomingSellers',
	showFilterButton: false,

	printItemUrl: Cement.Config.url.cargo.sellers.printItem,
	printUrl: Cement.Config.url.cargo.sellers.printGrid,
	helpUrl: Cement.Config.url.cargo.sellers.help
});