Ext.define('Cement.view.cargo.transport_lot.bottom.archive.Buyers', {
	extend: 'Cement.view.goods.auto.bottom.new_r.Senders',
	alias: 'widget.cargo_transport_lot_bottom_archive_buyers',
	gridStore: 'cargo.transport_lot.bottom.archive.Buyers',
	gridStateId: 'stateCargoTransportLotBottomArchiveBuyers',
	showFilterButton: false,

	printItemUrl: Cement.Config.url.cargo.buyers.printItem,
	printUrl: Cement.Config.url.cargo.buyers.printGrid,
	helpUrl: Cement.Config.url.cargo.buyers.help
});