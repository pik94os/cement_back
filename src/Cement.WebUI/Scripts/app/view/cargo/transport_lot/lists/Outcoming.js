Ext.define('Cement.view.cargo.transport_lot.lists.Outcoming', {
	extend: 'Cement.view.cargo.transport_lot.lists.New',
	alias: 'widget.cargo_transport_lot_lists_outcoming',

	gridStore: 'cargo.transport_lot.Outcoming',
	gridStateId: 'stateCargoTransportLotOutcoming',

	printUrl: Cement.Config.url.cargo.transport_lot.outcoming.printGrid,
	helpUrl: Cement.Config.url.cargo.transport_lot.outcoming.help,
    deleteUrl: Cement.Config.url.cargo.transport_lot.outcoming.deleteUrl,
    signItemUrl: Cement.Config.url.cargo.transport_lot.outcoming.signUrl,
    archiveItemUrl: Cement.Config.url.cargo.transport_lot.outcoming.archiveItem,

    getActionColumns: function () {
        return {
            xtype: 'rowactions',
            hideable: false,
            resizeable: false,
            locked: true,
            width: 46,
            actions: [
                {
                    iconCls: 'icon-delete-item',
                    qtip: 'Удалить',
                    callback: this.deleteItem
                },
                {
                    iconCls: 'icon-archive-item',
                    qtip: 'В архив',
                    callback: this.archiveItem
                }
            ],
            keepSelection: true
        };
    }
});