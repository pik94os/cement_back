Ext.define('Cement.view.cargo.transport_lot.lists.Units', {
    extend: 'Cement.view.transport.units.lists.New',
    alias: 'widget.cargo_transport_lists_units',
    gridStore: 'cargo.transport_lot.Units',
    gridStateId: 'stateCargoTransportListsUnits',

    showCreateButton: false,

    printUrl: Cement.Config.url.cargo.transport_lot.units.printGrid,
    helpUrl: Cement.Config.url.cargo.transport_lot.units.help,
    printItemUrl: Cement.Config.url.cargo.transport_lot.units.printItem,

    getActionColumns: function () {
        return {
            xtype: 'rowactions',
            hideable: false,
            locked: true,
            resizeable: false,
            width: 46,
            actions: [{
                iconCls: 'icon-add-item', 
                qtip: 'Создать лот', 
                callback: this.createLot
            }],
            keepSelection: true
        };
    },

    createLot: function (grid, record) {
        var rec = Ext.create('Cement.model.cargo.TransportLot');
        rec.set('p_from_display', record.get('p_from_display'));
        rec.set('p_from', record.get('p_from'));
        rec.set('p_to_display', record.get('p_to_display'));
        rec.set('p_to', record.get('p_to'));
        grid.up('basicgrid').fireEvent('edititem', rec, rec.$className);
    }
});