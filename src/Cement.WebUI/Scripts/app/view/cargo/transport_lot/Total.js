Ext.define('Cement.view.cargo.transport_lot.Total', {
	extend: 'Cement.view.basic.Tabs',
	title: 'Лот на транспорт',

	items: [{
		xtype: 'cargo_transport_lists_units',
		layout: 'fit',
		title: 'Транспортные единицы'
	}, {
		xtype: 'cargo_transport_lot_complex_new',
		title: 'Новые'
	}, {
		xtype: 'cargo_transport_lot_complex_incoming',
		title: 'Входящие'
	}, {
		xtype: 'cargo_transport_lot_complex_outcoming',
		title: 'Исходящие'
	}, {
		xtype: 'cargo_transport_lot_complex_archive',
		title: 'Архив'
	}],

	initComponent: function () {
		Ext.apply(this, {
			title: 'Лот на транспорт'
		});
		this.callParent(arguments);
	}
});