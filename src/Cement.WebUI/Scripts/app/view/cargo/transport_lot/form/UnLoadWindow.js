Ext.define('Cement.view.cargo.transport_lot.form.UnLoadWindow', {
	extend: 'Cement.view.cargo.goods_lot.form.LoadWindow',
	xtype: 'cargo_transport_lot_form_unload_window',
	title: 'Выбор населенного пункта выгрузки',
	structure_storeId: 'cargo.transport_lot.auxiliary.UnLoadPlaces'
});