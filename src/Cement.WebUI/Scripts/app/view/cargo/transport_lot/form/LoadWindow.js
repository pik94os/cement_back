Ext.define('Cement.view.cargo.transport_lot.form.LoadWindow', {
	extend: 'Cement.view.cargo.goods_lot.form.LoadWindow',
	xtype: 'cargo_transport_lot_form_load_window',
	structure_storeId: 'cargo.transport_lot.auxiliary.LoadPlaces'
});