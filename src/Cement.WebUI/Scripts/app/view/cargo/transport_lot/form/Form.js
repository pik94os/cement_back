Ext.define('Cement.view.cargo.transport_lot.form.Form', {
	extend: 'Cement.view.basic.Form',
	alias: 'widget.cargo_transport_lot_form_form',
	fieldsDisabled: false,
	url: Cement.Config.url.goods.auto.new_r.saveUrl,
	method: Cement.Config.url.goods.auto.new_r.saveMethod,
	printUrl: Cement.Config.url.goods.auto.new_r.printItem,
	isFilter: false,
	layout: 'fit',
	allowBlankFields: true,
	showSaveAndSignButton: true,
	saveButtonDisabled: false,

	getItems: function () {
		return [{
				xtype: 'fieldset',
				border: 0,
				margin: 0,
				padding: '0 110',
				defaults: {
					xtype: 'textfield',
					anchor: '100%',
					labelAlign: 'right'
				},
				items: [{
					name: 'p_number',
					fieldLabel: 'Номер'
				}, {
					name: 'p_date',
					fieldLabel: 'Дата',
					xtype: 'datefield'
				}]
            }, {
                xtype: 'fieldset',
                border: 0, 
                margin: 0,
                defaults: {
                    xtype: 'textfield',
                    anchor: '100%',
                    labelWidth: 200,
                    labelAlign: 'right'
                },
                items: [{
            		fieldLabel: 'Дата, время загрузки',
            		xtype: 'fieldcontainer',
            		layout: 'hbox',
            		role: 'p_load_date',
            		items: [{
            			name: 'p_load_date',
            			xtype: 'datefield',
            			flex: 1,
            			margin: '0 5 0 0'
            		}, {
            			name: 'p_load_time',
            			xtype: 'timefield',
            			flex: 1,
            			margin: '0 0 0 5'
            		}]
            	}, {
            		fieldLabel: 'Дата, время выгрузки',
            		xtype: 'fieldcontainer',
            		layout: 'hbox',
            		role: 'p_unload_date',
            		items: [{
            			name: 'p_unload_date',
            			xtype: 'datefield',
            			flex: 1,
            			margin: '0 5 0 0'
            		}, {
            			name: 'p_unload_time',
            			xtype: 'timefield',
            			flex: 1,
            			margin: '0 0 0 5'
            		}]
            	},
            		this.getSelectorField('Из', 'p_from', 'show_from_window'),
            		this.getSelectorField('В', 'p_to', 'show_to_window'), {
        			xtype: 'checkbox',
        			fieldLabel: 'Кругорейс',
        			name: 'p_cirle'
        		}, {
        			xtype: 'checkbox',
        			fieldLabel: 'Есть диспетчер',
        			name: 'p_has_dispatcher'
        		},
        			this.getSelectorField('Диспетчер', 'p_dispatcher', 'show_dispatcher_window'),
        			this.getSelectorField('Транспортная единица', 'p_transport_unit', 'show_transport_unit_window'),
        			this.getSelectorField('Договор', 'p_contract', 'show_contract_window'),
        			this.getSelectorField('Прайс-лист', 'p_price', 'show_price_window'), {
        				xtype: 'textfield',
        				fieldLabel: 'Услуга',
        				name: 'p_service_display',
        				disabled: true
        			},
        			this.getSelectorField('Условия оплаты', 'p_payment_rules', 'show_payment_rules_window'), {
        				xtype: 'textarea',
        				fieldLabel: 'Примечание',
        				name: 'p_comment',
        				height: 150
        			}
            	]
            }
        ];
	},

	initComponent: function () {
		this.callParent(arguments);
		this.reset();

		this.down('checkbox[name=p_has_dispatcher]').on('change', function (control, newValue) {
			if (newValue) {
				this.down('fieldcontainer[role=container_p_dispatcher]').enable();
				this.down('fieldcontainer[role=container_p_contract]').enable();
				this.down('fieldcontainer[role=container_p_price]').disable();
			}
			else {
				this.down('fieldcontainer[role=container_p_dispatcher]').disable();
				this.down('fieldcontainer[role=container_p_contract]').disable();
				this.down('fieldcontainer[role=container_p_price]').enable();
			}
		}, this);

		this.down('button[action=show_from_window]').on('click', function () {
			this.createLoadWindow();
			this.loadWindow.show();
		}, this);

		this.down('button[action=show_to_window]').on('click', function () {
			this.createUnLoadWindow();
			this.unLoadWindow.show();
		}, this);

		this.down('button[action=show_dispatcher_window]').on('click', function () {
			this.createDispatcherWindow();
			this.dispatcherWindow.show();
		}, this);

		this.down('button[action=show_transport_unit_window]').on('click', function () {
			this.createTransportWindow();
			this.transportWindow.show();
		}, this);

		this.down('button[action=show_payment_rules_window]').on('click', function () {
			this.createPaymentRulesWindow();
			this.paymentWindow.show();
		}, this);

		this.down('button[action=show_contract_window]').on('click', function () {
			this.createContractWindow();
			this.contractWindow.show();
		}, this);

		this.down('button[action=show_price_window]').on('click', function () {
			this.createPriceWindow();
			this.priceWindow.show();
		}, this);
	},

	createPriceWindow: function () {
		if (!this.priceWindow) {
			this.priceWindow = Ext.create('Cement.view.cargo.goods_lot.form.PricelistWindow');
			this.priceWindow.on('selected', function (rec) {
				this.down('textfield[name=p_price_display]').setValue(rec.get('p_pricelist_name'));
				this.down('hiddenfield[name=p_price]').setValue(rec.get('p_pricelist'));
				this.down('textfield[name=p_service_display]').setValue(rec.get('p_name'));
				this.down('hiddenfield[name=p_service]').setValue(rec.get('id'));
				this.priceWindow.hide();
			}, this);
		}
	},

	createContractWindow: function () {
		if (!this.contractWindow) {
			this.contractWindow = Ext.create('Cement.view.cargo.goods_lot.form.ContractWindow');
			this.contractWindow.on('selected', function (rec) {
				this.down('textfield[name=p_contract_display]').setValue(rec.get('p_contract_name'));
				this.down('hiddenfield[name=p_contract]').setValue(rec.get('p_contract'));
				this.down('textfield[name=p_service_display]').setValue(rec.get('p_name'));
				this.down('hiddenfield[name=p_service]').setValue(rec.get('id'));
				this.contractWindow.hide();
			}, this);
		}
	},

	createDispatcherWindow: function () {
		if (!this.dispatcherWindow) {
			this.dispatcherWindow = Ext.create('Cement.view.cargo.goods_lot.form.DispatcherWindow');
			this.dispatcherWindow.on('selected', function (rec) {
				this.down('textfield[name=p_dispatcher_display]').setValue(rec.get('p_name'));
				this.down('hiddenfield[name=p_dispatcher]').setValue(rec.get('id'));
				this.dispatcherWindow.hide();
				this.down('fieldcontainer[role=p_load_date]').disable();
				this.down('fieldcontainer[role=p_unload_date]').disable();
				this.down('fieldcontainer[role=container_p_price]').disable();
				this.down('fieldcontainer[role=container_p_service]').disable();
			}, this);
		}
	},

	createPaymentRulesWindow: function () {
		if (!this.paymentWindow) {
			this.paymentWindow = Ext.create('Cement.view.contracts.contracts.form.PaymentRulesWindow');
			this.paymentWindow.on('paymentrulescreated', function (json, str) {
				this.down('textfield[name=p_payment_rules_display]').setValue(str);
				this.down('hiddenfield[name=p_payment_rules]').setValue(json);
				this.paymentWindow.hide();
			}, this);
		}
	},

	createTransportWindow: function () {
		if (!this.transportWindow) {
			this.transportWindow = Ext.create('Cement.view.cargo.transport_lot.form.TransportUnitWindow');
			this.transportWindow.on('requestselected', function (rec) {
				this.down('textfield[name=p_transport_unit_display]').setValue(rec.get('p_name'));
				this.down('hiddenfield[name=p_transport_unit]').setValue(rec.get('id'));
				this.transportWindow.hide();
			}, this);
		}
	},

	createLoadWindow: function () {
		if (!this.loadWindow) {
			this.loadWindow = Ext.create('Cement.view.cargo.transport_lot.form.LoadWindow');
			this.loadWindow.on('selected', function (rec) {
				this.down('textfield[name=p_from_display]').setValue(rec.get('p_name'));
				this.down('hiddenfield[name=p_from]').setValue(rec.get('id'));
				this.loadWindow.hide();
			}, this);
		}
	},

	createUnLoadWindow: function () {
		if (!this.unLoadWindow) {
			this.unLoadWindow = Ext.create('Cement.view.cargo.transport_lot.form.UnLoadWindow');
			this.unLoadWindow.on('selected', function (rec) {
				this.down('textfield[name=p_to_display]').setValue(rec.get('p_name'));
				this.down('hiddenfield[name=p_to]').setValue(rec.get('id'));
				this.unLoadWindow.hide();
			}, this);
		}
	},

	afterRecordLoad: function (rec) {
		this.createLoadWindow();
		this.createUnLoadWindow();
		this.createTransportWindow();
		this.createDispatcherWindow();
		this.createPriceWindow();
		this.createContractWindow();
		this.createPaymentRulesWindow();
	},

	reset: function () {
	    this.callParent(arguments);

		this.down('fieldcontainer[role=p_load_date]').disable();
		this.down('fieldcontainer[role=p_unload_date]').disable();
		this.down('fieldcontainer[role=container_p_price]').enable();
		this.down('fieldcontainer[role=container_p_contract]').disable();
		this.down('hidden[name=p_payment_rules]').setValue('');
		this.down('textfield[name=p_payment_rules_display]').setValue('');
	}
});