Ext.define('Cement.view.cargo.transport_lot.form.TransportUnitWindow', {
	extend: 'Cement.view.goods.auto.form.TransportUnitWindow',
	xtype: 'cargo_transport_lot_form_transport_unit_window',
	srcStoreId: 'cargo.transport_lot.auxiliary.TransportUnits'
});