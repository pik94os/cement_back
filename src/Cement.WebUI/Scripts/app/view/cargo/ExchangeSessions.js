Ext.define('Cement.view.cargo.ExchangeSessions', {
	moveToContract: function (grid, record) {
        Ext.Msg.confirm('Внимание', 'Вы действительно хотите переместить элемент на контрактную сессию?', function (btn) {
            if (btn == 'yes') {
                Ext.Ajax.request({
                    url: Ext.String.format(grid.up('basicgrid').moveToContractUrl, record.get('id')),
                    success: function (response) {
                        var json = Ext.JSON.decode(response.responseText);
                        if (json.success) {
                            grid.getStore().load();
                            Cement.Msg.info(json.msg);
                        }
                        else {
                            Cement.Msg.error(json.msg);
                        }
                    },
                    failure: function () {
                        Cement.Msg.error('Ошибка при перемещении');
                    }
                });
            }
        });
    },

    moveToCommon: function (grid, record) {
        Ext.Msg.confirm('Внимание', 'Вы действительно хотите переместить элемент на общую сессию?', function (btn) {
            if (btn == 'yes') {
                Ext.Ajax.request({
                    url: Ext.String.format(grid.up('basicgrid').moveToCommonUrl, record.get('id')),
                    success: function (response) {
                        var json = Ext.JSON.decode(response.responseText);
                        if (json.success) {
                            grid.getStore().load();
                            Cement.Msg.info(json.msg);
                        }
                        else {
                            Cement.Msg.error(json.msg);
                        }
                    },
                    failure: function () {
                        Cement.Msg.error('Ошибка при перемещении');
                    }
                });
            }
        });
    },

    moveToCorporate: function (grid, record) {
        Ext.Msg.confirm('Внимание', 'Вы действительно хотите переместить элемент на корпоративную сессию?', function (btn) {
            if (btn == 'yes') {
                Ext.Ajax.request({
                    url: Ext.String.format(grid.up('basicgrid').moveToCorporateUrl, record.get('id')),
                    success: function (response) {
                        var json = Ext.JSON.decode(response.responseText);
                        if (json.success) {
                            grid.getStore().load();
                            Cement.Msg.info(json.msg);
                        }
                        else {
                            Cement.Msg.error(json.msg);
                        }
                    },
                    failure: function () {
                        Cement.Msg.error('Ошибка при перемещении');
                    }
                });
            }
        });
    }
});