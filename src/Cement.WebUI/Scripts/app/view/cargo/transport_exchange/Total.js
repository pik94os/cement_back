Ext.define('Cement.view.cargo.transport_exchange.Total', {
	extend: 'Cement.view.basic.Tabs',
	title: 'Биржа транспорта',

	items: [{
		xtype: 'cargo_transport_exchange_complex_corporate',
		title: 'Корпоративная',
		shownTitle: 'Корпоративная',
		detailType: ''
	}, {
		xtype: 'cargo_transport_exchange_complex_contract',
		title: 'Контрактная',
		shownTitle: 'Контрактная',
		detailType: ''
	}, {
		xtype: 'cargo_transport_exchange_complex_common',
		title: 'Общая',
		shownTitle: 'Общая',
		detailType: ''
	}],

	initComponent: function () {
		Ext.apply(this, {
			title: 'Биржа транспорта'
		});
		this.callParent(arguments);
	}
});