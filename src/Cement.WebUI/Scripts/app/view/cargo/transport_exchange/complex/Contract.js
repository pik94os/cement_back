Ext.define('Cement.view.cargo.transport_exchange.complex.Contract', {
	extend: 'Cement.view.basic.TwoSide',
	alias: 'widget.cargo_transport_exchange_complex_contract',
    linkUrl: Cement.Config.url.cargo.transport_exchange.corporate.linkUrl,

	leftControl: {
        xtype: 'cargo_transport_exchange_lists_contract',
        layout: 'fit'
    },
    rightControl: {
        xtype: 'cargo_transport_exchange_right_contract',
        layout: 'fit'
    }
});