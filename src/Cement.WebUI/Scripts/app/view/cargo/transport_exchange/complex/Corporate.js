Ext.define('Cement.view.cargo.transport_exchange.complex.Corporate', {
	extend: 'Cement.view.basic.TwoSide',
	alias: 'widget.cargo_transport_exchange_complex_corporate',
    linkUrl: Cement.Config.url.cargo.transport_exchange.corporate.linkUrl,

	leftControl: {
        xtype: 'cargo_transport_exchange_lists_corporate',
        layout: 'fit'
    },
    rightControl: {
        xtype: 'cargo_transport_exchange_right_corporate',
        layout: 'fit'
    }
});