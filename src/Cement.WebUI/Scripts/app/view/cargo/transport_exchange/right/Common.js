Ext.define('Cement.view.cargo.transport_exchange.right.Common', {
    extend: 'Cement.view.cargo.transport_exchange.right.Corporate',
    alias: 'widget.cargo_transport_exchange_right_common',
    autoLoadStore: true,

    gridStore: 'cargo.transport_exchange.TransportCommon',
    gridStateId: 'stateTransportExchangeTransportCommon'
});