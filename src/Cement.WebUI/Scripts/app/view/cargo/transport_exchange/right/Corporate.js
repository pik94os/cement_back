Ext.define('Cement.view.cargo.transport_exchange.right.Corporate', {
    extend: 'Cement.view.basic.Grid',
    alias: 'widget.cargo_transport_exchange_right_corporate',
    autoLoadStore: true,

    bbarText: 'Показаны заявки {0} - {1} из {2}',
    bbarEmptyMsg: 'Нет заявок',
    bbarUsersText: 'Заявок на странице: ',

    gridStore: 'cargo.transport_exchange.TransportCorporate',
    gridStateId: 'stateTransportExchangeTransportCorporate',

    disabled: true,

    title: 'Лот на груз',
    shownTitle: 'Лот на груз',
    collapsible: true,
    collapsed: false,
    // showLeftBar: false,
    showCreateButton: false,
    // showSimpleButtonsOnTop: true,

    mixins: [
        'Cement.view.cargo.Exchange'
    ],

    getActionColumns: function () {
        return {
            xtype: 'rowactions',
            hideable: false,
            resizeable: false,
            locked: true,
            width: 46,
            actions: [
                {
                    iconCls: 'icon-link',
                    qtip: 'Связать',
                    callback: this.linkItem
                }
            ],
            keepSelection: true
        };
    },

    getFilterItems: function () {
        return [{
            text: '№ П/п',
            kind: 'selector',
            field_name: 'p_number',
            checked: true
        }, {
            text: 'Товар группа',
            kind: 'selector',
            field_name: 'p_product_group',
            checked: true
        }, {
            text: 'Товар подгруппа',
            kind: 'selector',
            field_name: 'p_product_subgroup',
            checked: true
        }, {
            text: 'Товар наименование',
            kind: 'selector',
            field_name: 'p_product_display',
            checked: true
        }, {
            text: 'Товар упаковка',
            kind: 'selector',
            field_name: 'p_product_package',
            checked: true
        }, {
            text: 'Товар ед. изм.',
            kind: 'selector',
            field_name: 'p_product_unit_display',
            checked: true
        }, {
            text: 'Товар кол-во',
            kind: 'selector',
            field_name: 'p_product_quantity',
            checked: true
        }, {
            text: 'Транспорт тип кузова',
            kind: 'selector',
            field_name: 'p_transport_char_type_display',
            checked: true
        }, {
            text: 'Транспорт доп. опции',
            kind: 'selector',
            field_name: 'p_transport_additional_options_display',
            checked: true
        }];
    },

    getGridColumns: function () {
        var result = [
            { text: '№ П/п', dataIndex: 'p_number', width: 60, locked: true },
            {
                text: 'Товар',
                columns: [
                    { text: 'Группа', dataIndex: 'p_product_group', width: 150 },
                    { text: 'Подгруппа', dataIndex: 'p_product_subgroup', width: 150 },
                    { text: 'Наименование', dataIndex: 'p_product_display', width: 150 },
                    { text: 'Упаковка', dataIndex: 'p_product_package', width: 150 },
                    { text: 'Ед. изм.', dataIndex: 'p_product_unit_display', width: 50 },
                    { text: 'Кол-во', dataIndex: 'p_product_quantity', width: 50 }
                ]
            },
            {
                text: 'Транспорт',
                columns: [
                    { text: 'О', dataIndex: 'p_transport_char_ops', width: 20, resizable: false, align: 'center', renderer: this.radioButtonRenderer1, tdCls: 'pseudoinput-column' },
                    { text: 'П', dataIndex: 'p_transport_char_ops', width: 20, resizable: false, align: 'center', renderer: this.radioButtonRenderer2, tdCls: 'pseudoinput-column' },
                    { text: 'С', dataIndex: 'p_transport_char_ops', width: 20, resizable: false, align: 'center', renderer: this.radioButtonRenderer3, tdCls: 'pseudoinput-column' },
                    { text: 'Тип кузова', dataIndex: 'p_transport_char_type_display', width: 100 },
                    { text: 'З', dataIndex: 'p_transport_load_back', width: 20, resizable: false, renderer: this.checkBoxRenderer, align: 'center', tdCls: 'pseudoinput-column' },
                    { text: 'Б', dataIndex: 'p_transport_load_left', width: 20, resizable: false, renderer: this.checkBoxRenderer, align: 'center', tdCls: 'pseudoinput-column' },
                    { text: 'В', dataIndex: 'p_transport_load_top', width: 20, resizable: false, renderer: this.checkBoxRenderer, align: 'center', tdCls: 'pseudoinput-column' },
                    { text: 'Доп. опции', dataIndex: 'p_transport_additional_options_display', width: 100 }
                ]
            }
        ];
        return this.mergeActions(result);
    },

    initComponent: function () {
        Ext.apply(this, {
            title: this.shownTitle
        });
        this.callParent(arguments);
        this.addEvents('linkitem');
    }
});