Ext.define('Cement.view.cargo.transport_exchange.right.Contract', {
    extend: 'Cement.view.cargo.transport_exchange.right.Corporate',
    alias: 'widget.cargo_transport_exchange_right_contract',
    autoLoadStore: true,

    gridStore: 'cargo.transport_exchange.TransportContract',
    gridStateId: 'stateTransportExchangeTransportContract'
});