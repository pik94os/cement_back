Ext.define('Cement.view.cargo.transport_exchange.lists.Corporate', {
	extend: 'Cement.view.cargo.transport_exchange.lists.Contract',
	alias: 'widget.cargo_transport_exchange_lists_corporate',
    autoLoadStore: true,

	gridStore: 'cargo.transport_exchange.Corporate',
	gridStateId: 'stateCargoTransportExchangeCorporate',

	printUrl: Cement.Config.url.cargo.transport_exchange.common.printGrid,
	helpUrl: Cement.Config.url.cargo.transport_exchange.common.help,
    deleteUrl: Cement.Config.url.cargo.transport_exchange.common.deleteUrl,
    signItemUrl: Cement.Config.url.cargo.transport_exchange.common.signUrl,
    archiveItemUrl: Cement.Config.url.cargo.transport_exchange.common.archiveItem,
    moveToContractUrl: Cement.Config.url.cargo.transport_exchange.corporate.moveToContract,

    shownTitle: null,

    mixins: [
        'Cement.view.cargo.ExchangeSessions'
    ],

    getActionColumns: function () {
        return {
            xtype: 'rowactions',
            hideable: false,
            resizeable: false,
            locked: true,
            width: 46,
            actions: [
                {
                    iconCls: 'icon-down-item',
                    qtip: 'Переместить на контрактную',
                    callback: this.moveToContract
                }
            ],
            keepSelection: true
        };
    }
});