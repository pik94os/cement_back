Ext.define('Cement.view.cargo.transport_exchange.lists.Common', {
	extend: 'Cement.view.cargo.transport_exchange.lists.Contract',
	alias: 'widget.cargo_transport_exchange_lists_common',
    autoLoadStore: true,

	gridStore: 'cargo.transport_exchange.Common',
	gridStateId: 'stateCargoTransportExchangeCommon',

	printUrl: Cement.Config.url.cargo.transport_exchange.common.printGrid,
	helpUrl: Cement.Config.url.cargo.transport_exchange.common.help,
    moveToContractUrl: Cement.Config.url.cargo.transport_exchange.common.moveToContract,

    shownTitle: null,

    mixins: [
        'Cement.view.cargo.ExchangeSessions'
    ],

    getActionColumns: function () {
        return {
            xtype: 'rowactions',
            hideable: false,
            resizeable: false,
            locked: true,
            width: 46,
            actions: [
                {
                    iconCls: 'icon-up-item',
                    qtip: 'Переместить на контрактную',
                    callback: this.moveToContract
                }
            ],
            keepSelection: true
        };
    }
});