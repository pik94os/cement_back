Ext.define('Cement.view.cargo.goods_exchange.lists.Common', {
	extend: 'Cement.view.cargo.goods_exchange.lists.Contract',
	alias: 'widget.cargo_goods_exchange_lists_common',
    autoLoadStore: true,

	gridStore: 'cargo.goods_exchange.Common',
	gridStateId: 'stateCargoGoodsExchangeCommon',

	printUrl: Cement.Config.url.cargo.goods_exchange.common.printGrid,
	helpUrl: Cement.Config.url.cargo.goods_exchange.common.help,
    moveToContractUrl: Cement.Config.url.cargo.goods_exchange.common.moveToContract,

    shownTitle: null,

    mixins: [
        'Cement.view.cargo.ExchangeSessions'
    ],

    getActionColumns: function () {
        return {
            xtype: 'rowactions',
            hideable: false,
            resizeable: false,
            locked: true,
            width: 46,
            actions: [
                {
                    iconCls: 'icon-up-item',
                    qtip: 'Переместить на контрактную',
                    callback: this.moveToContract
                }
            ],
            keepSelection: true
        };
    }
});