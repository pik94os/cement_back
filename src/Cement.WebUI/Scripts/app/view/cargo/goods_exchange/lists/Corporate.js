Ext.define('Cement.view.cargo.goods_exchange.lists.Corporate', {
	extend: 'Cement.view.cargo.goods_exchange.lists.Contract',
	alias: 'widget.cargo_goods_exchange_lists_corporate',
    autoLoadStore: true,

	gridStore: 'cargo.goods_exchange.Corporate',
	gridStateId: 'stateCargoGoodsExchangeCorporate',

	printUrl: Cement.Config.url.cargo.goods_exchange.corporate.printGrid,
	helpUrl: Cement.Config.url.cargo.goods_exchange.corporate.help,
    moveToContractUrl: Cement.Config.url.cargo.goods_exchange.corporate.moveToContract,

    shownTitle: null,

    mixins: [
        'Cement.view.cargo.ExchangeSessions'
    ],

    getActionColumns: function () {
        return {
            xtype: 'rowactions',
            hideable: false,
            resizeable: false,
            locked: true,
            width: 46,
            actions: [
                {
                    iconCls: 'icon-down-item',
                    qtip: 'Переместить на контрактную',
                    callback: this.moveToContract
                }
            ],
            keepSelection: true
        };
    }
});