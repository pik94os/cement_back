Ext.define('Cement.view.cargo.goods_exchange.Total', {
	extend: 'Cement.view.basic.Tabs',
	title: 'Биржа грузов',

	items: [{
		xtype: 'cargo_goods_exchange_complex_corporate',
		title: 'Корпоративная',
		shownTitle: 'Корпоративная',
		detailType: ''
	}, {
		xtype: 'cargo_goods_exchange_complex_contract',
		title: 'Контрактная',
		shownTitle: 'Контрактная',
		detailType: ''
	}, {
		xtype: 'cargo_goods_exchange_complex_common',
		title: 'Общая',
		shownTitle: 'Общая',
		detailType: ''
	}],

	initComponent: function () {
		Ext.apply(this, {
			title: 'Биржа грузов'
		});
		this.callParent(arguments);
	}
});