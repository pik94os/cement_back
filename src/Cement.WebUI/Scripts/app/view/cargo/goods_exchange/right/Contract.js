Ext.define('Cement.view.cargo.goods_exchange.right.Contract', {
    extend: 'Cement.view.cargo.goods_exchange.right.Corporate',
    alias: 'widget.cargo_goods_exchange_right_contract',
    autoLoadStore: true,

    gridStore: 'cargo.goods_exchange.TransportContract',
    gridStateId: 'stateGoodsExchangeTransportContract'
});