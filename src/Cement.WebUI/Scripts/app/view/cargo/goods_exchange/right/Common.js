Ext.define('Cement.view.cargo.goods_exchange.right.Common', {
    extend: 'Cement.view.cargo.goods_exchange.right.Corporate',
    alias: 'widget.cargo_goods_exchange_right_common',
    autoLoadStore: true,

    gridStore: 'cargo.goods_exchange.TransportCommon',
    gridStateId: 'stateGoodsExchangeTransportCommon'
});