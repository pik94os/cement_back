Ext.define('Cement.view.cargo.goods_exchange.right.Corporate', {
    extend: 'Cement.view.basic.Grid',
    alias: 'widget.cargo_goods_exchange_right_corporate',
    autoLoadStore: true,

    bbarText: 'Показаны ТЕ {0} - {1} из {2}',
    bbarEmptyMsg: 'Нет ТЕ',
    bbarUsersText: 'ТЕ на странице: ',

    gridStore: 'cargo.goods_exchange.TransportCorporate',
    gridStateId: 'stateGoodsExchangeTransportCorporate',

    disabled: true,

    title: 'Лот на транспорт',
    shownTitle: 'Лот на транспорт',
    collapsible: true,
    collapsed: false,
    // showLeftBar: true,
    showCreateButton: false,
    // showSimpleButtonsOnTop: true,

    mixins: [
        'Cement.view.cargo.Exchange'
    ],

    getActionColumns: function () {
        return {
            xtype: 'rowactions',
            hideable: false,
            resizeable: false,
            locked: true,
            width: 46,
            actions: [
                {
                    iconCls: 'icon-link',
                    qtip: 'Связать',
                    callback: this.linkItem
                }
            ],
            keepSelection: true
        };
    },

    getFilterItems: function () {
        return [{
            text: 'Наименование',
            kind: 'selector',
            field_name: 'p_name',
            checked: true
        }, {
            text: 'Тип',
            kind: 'selector',
            field_name: 'p_kind',
            checked: true
        }, {
            text: 'ТС марка, модель',
            kind: 'selector',
            field_name: 'p_auto_mark_model',
            checked: true
        }, {
            text: 'ТС гос номер',
            kind: 'selector',
            field_name: 'p_auto_number',
            checked: true
        }, {
            text: 'Прицепное ТС марка, модель',
            kind: 'selector',
            field_name: 'p_auto_trailer',
            checked: true
        }, {
            text: 'Прицепное ТС гос номер',
            kind: 'selector',
            field_name: 'p_auto_trailer_number',
            checked: true
        }, {
            text: 'Водитель фамилия',
            kind: 'selector',
            field_name: 'p_driver_surname',
            checked: true
        }, {
            text: 'Водитель имя',
            kind: 'selector',
            field_name: 'p_driver_name',
            checked: true
        }, {
            text: 'Водитель отчество',
            kind: 'selector',
            field_name: 'p_driver_lastname',
            checked: true
        }, {
            text: 'Транспорт тип кузова',
            kind: 'selector',
            field_name: 'p_transport_char_type_display',
            checked: true
        }, {
            text: 'Доп. опции',
            kind: 'selector',
            field_name: 'p_additional_options_display',
            checked: true
        }, {
            text: 'Доп. характеристики',
            kind: 'selector',
            field_name: 'p_additional_chars_display',
            checked: true
        }];
    },

    getGridColumns: function () {
        var result = [
            { text: 'Наименование', dataIndex: 'p_name', width: 140, locked: true },
            { text: 'Тип', dataIndex: 'p_kind', width: 60, locked: true },
            {
                text: 'Транспортное средство',
                columns: [
                    { text: 'Марка, модель', dataIndex: 'p_auto_mark_model', width: 200 },
                    { text: 'Гос. номер', dataIndex: 'p_auto_number', width: 200 }
                ]
            },
            {
                text: 'Прицепное транспортное средство',
                columns: [
                    { text: 'Марка, модель', dataIndex: 'p_auto_trailer', width: 200 },
                    { text: 'Гос. номер', dataIndex: 'p_auto_trailer_number', width: 200 }
                ]
            },
            {
                text: 'Водитель',
                columns: [
                    { text: 'Фамилия', dataIndex: 'p_driver_surname', width: 150 },
                    { text: 'Имя', dataIndex: 'p_driver_name', width: 150 },
                    { text: 'Отчество', dataIndex: 'p_driver_lastname', width: 150 }
                ]
            },
            {
                text: 'Характеристики',
                columns: [
                    { text: 'ДхШхВ, м', dataIndex: 'p_transport_size_display', width: 100 },
                    { text: 'Г/О, т/м<sup>3</sup>', dataIndex: 'p_transport_go_display', width: 100 },
                    { text: 'О', dataIndex: 'p_transport_char_ops', width: 20, resizable: false, align: 'center', renderer: this.radioButtonRenderer1, tdCls: 'pseudoinput-column' },
                    { text: 'П', dataIndex: 'p_transport_char_ops', width: 20, resizable: false, align: 'center', renderer: this.radioButtonRenderer2, tdCls: 'pseudoinput-column' },
                    { text: 'С', dataIndex: 'p_transport_char_ops', width: 20, resizable: false, align: 'center', renderer: this.radioButtonRenderer3, tdCls: 'pseudoinput-column' },
                    { text: 'Тип кузова', dataIndex: 'p_transport_char_type_display', width: 100 },
                    { text: 'З', dataIndex: 'p_transport_load_back', width: 20, resizable: false, renderer: this.checkBoxRenderer, align: 'center', tdCls: 'pseudoinput-column' },
                    { text: 'Б', dataIndex: 'p_transport_load_left', width: 20, resizable: false, renderer: this.checkBoxRenderer, align: 'center', tdCls: 'pseudoinput-column' },
                    { text: 'В', dataIndex: 'p_transport_load_top', width: 20, resizable: false, renderer: this.checkBoxRenderer, align: 'center', tdCls: 'pseudoinput-column' },
                    { text: 'Доп. опции', dataIndex: 'p_transport_additional_options_display', width: 100 },
                    { text: 'Доп. характеристики', dataIndex: 'p_transport_additional_chars_display', width: 150 }
                ]
            }
        ];
        return this.mergeActions(result);
    },

    initComponent: function () {
        Ext.apply(this, {
            title: this.shownTitle
        });
        this.callParent(arguments);
        this.addEvents('linkitem');
    }
});