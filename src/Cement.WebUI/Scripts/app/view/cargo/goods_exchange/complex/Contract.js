Ext.define('Cement.view.cargo.goods_exchange.complex.Contract', {
	extend: 'Cement.view.basic.TwoSide',
	alias: 'widget.cargo_goods_exchange_complex_contract',
    linkUrl: Cement.Config.url.cargo.goods_exchange.contract.linkUrl,

	leftControl: {
        xtype: 'cargo_goods_exchange_lists_contract',
        layout: 'fit'
    },
    rightControl: {
        xtype: 'cargo_goods_exchange_right_contract',
        layout: 'fit'
    }
});