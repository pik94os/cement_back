Ext.define('Cement.view.cargo.goods_exchange.complex.Corporate', {
	extend: 'Cement.view.basic.TwoSide',
	alias: 'widget.cargo_goods_exchange_complex_corporate',
    linkUrl: Cement.Config.url.cargo.goods_exchange.corporate.linkUrl,

	leftControl: {
        xtype: 'cargo_goods_exchange_lists_corporate',
        layout: 'fit'
    },
    rightControl: {
        xtype: 'cargo_goods_exchange_right_corporate',
        layout: 'fit'
    }
});