Ext.define('Cement.view.cargo.goods_exchange.complex.Common', {
	extend: 'Cement.view.basic.TwoSide',
	alias: 'widget.cargo_goods_exchange_complex_common',
    linkUrl: Cement.Config.url.cargo.goods_exchange.common.linkUrl,

	leftControl: {
        xtype: 'cargo_goods_exchange_lists_common',
        layout: 'fit'
    },
    rightControl: {
        xtype: 'cargo_goods_exchange_right_corporate',
        layout: 'fit'
    }
});