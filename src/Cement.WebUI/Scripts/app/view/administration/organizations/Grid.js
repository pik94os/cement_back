Ext.define('Cement.view.administration.organizations.Grid', {
    extend: 'Cement.view.basic.Grid',
    cls: 'simple-form',
    bbarText: 'Показаны организации {0} - {1} из {2}',
    bbarEmptyMsg: 'Нет организаций',
    bbarUsersText: 'Организаций на странице: ',
    gridStore: 'administration.organizations.Organizations',
    gridStateful: false,
    alias: 'widget.admin_org_grid',
    title: 'Организации',
    loadItemUrl: Cement.Config.url.administration.organizations.loadItem,

    getGridColumns: function () {
        var me = this;
        var result = [
            {
                text: 'Наименование',
                dataIndex: 'p_name',
                flex: 1
            },
            {
                text: 'ИНН',
                dataIndex: 'p_inn',
                width: 200
            }
        ];
        
        return this.mergeActions(result);
    },

    getActionColumns: function () {
        return {
            xtype: 'rowactions',
            hideable: false,
            resizeable: false,
            locked: true,
            width: 46,
            actions: [
                {
                    iconCls: 'icon-edit-item',
                    qtip: 'Редактировать',
                    callback: this.editItem
                }
            ],
            keepSelection: true
        };
    },

    editItem: function (grid, record) {
        var bacisGrid = grid.up('basicgrid');
        bacisGrid.createEditWindow();
        bacisGrid.editWindow.createNewItem(false);
        bacisGrid.editWindow.show();
        
        Ext.Ajax.request({
            url: Ext.String.format(bacisGrid.loadItemUrl, record.get('id')),
            method: 'POST',
            success: function (response) {
                var rec = Ext.JSON.decode(response.responseText);
                var form = bacisGrid.editWindow.down('form').getForm();
                form.setValues(rec);
            },
            failure: function () {
                Cement.Msg.error('Ошибка при загрузке элемента');
            }
        });

    },

    createItem: function() {
        this.createEditWindow();
        this.editWindow.createNewItem(true);
        this.editWindow.show();
    },

    createEditWindow: function () {
        var me = this;
        if (!this.editWindow) {
            this.editWindow = Ext.create('Cement.view.administration.organizations.EditWindow');
        }
        this.editWindow.on('close', function (cmp, rec) {
            me.down('grid').getStore().reload();
        }, this);
        this.editWindow.down('form').getForm().reset(true);

    }
});