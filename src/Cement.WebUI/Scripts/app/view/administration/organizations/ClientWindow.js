Ext.define('Cement.view.administration.organizations.ClientWindow', {
	extend: 'Ext.window.Window',
	xtype: 'org_client_window',
	width: 800,
	height: 400,
	hideCommentPanel: false,
	modal: true,
	title: 'Выберите клиента',
	layout: 'fit',
	closable: true,
	bodyStyle: "background: #f7f7f7",
	closeAction: 'hide',

	initComponent: function () {
	    var me = this;
	    this.callParent(arguments);

	    this.on('beforeshow', function() {
	        me.down('grid').getStore().reload();
	    });
	},

	items: [
    {
        xtype: 'grid',
        border: false,
        store: 'administration.organizations.Clients',
        columns: [
            {
                text: 'Наименование',
                dataIndex: 'p_name',
                flex: 1,
                items:[{
                    xtype: 'searchtrigger',
                    autoSearch: false
                }]},
            {
                text: 'ИНН',
                dataIndex: 'p_inn',
                width: 150,
                items: [{
                    xtype: 'searchtrigger',
                    autoSearch: false
                }]
            }
        ],
        dockedItems: [{
            xtype: 'pagingtoolbar',
            store: 'administration.organizations.Clients',
            dock: 'bottom',
            border: false,
            displayInfo: true
        }]
    }]
});