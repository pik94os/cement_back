Ext.define('Cement.view.administration.organizations.EditWindow', {
	extend: 'Ext.window.Window',
	xtype: 'org_edit_window',
	width: 500,
	height: 220,
	hideCommentPanel: false,
	modal: true,
	title: 'Организация',
	layout: 'fit',
	bodyPadding: 10,
	closable: true,
	bodyStyle: "background: #f7f7f7",
	resizable: false,
	closeAction: 'hide',
	saveItemUrl: Cement.Config.url.administration.organizations.saveItem,

    initComponent: function () {
        var me = this;
	    this.callParent(arguments);

	    this.down('button[name=save_btn]').on('click', function () {
	        var window = this.up('window');
	        var form = window.down('form').getForm();
	        window.setLoading("Сохранение");
	        if (form.isValid()) {
	            form.submit({
	                url: window.saveItemUrl,
	                success: function (frm, action) {
	                    window.setLoading(false);
	                    window.close();
	                },
	                failure: function (frm, action) {
	                    window.setLoading(false);
	                    Ext.MessageBox.alert('Ошибка', action.result.message);
	                }
	            });
	        }
	    });

	    this.down('button[name=select_client_btn]').on('click', function () {
	        var window = this.up('window');
	        window.createClientWindow();
	        window.clientWindow.show();
	    });

	    this.down('button[name=generate_pass_btn]').on('click', function () {
	        var randomPassword = me.generateNewPassword(8);
	        me.down('textfield[name=p_pass]').setValue(randomPassword);
	    });

	    this.down('checkbox[name=p_change_password]').on('change', function (cmp, newValue) {
	        var passTextfield = me.down('textfield[name=p_pass]');
	        passTextfield.setDisabled(!newValue);
	        passTextfield.allowBlank = !newValue;
	        passTextfield.clearInvalid();
	        me.down('button[name=generate_pass_btn]').setDisabled(!newValue);
	    });

	    this.down('form').on('validitychange', function (f, valid) {
	        var saveButton = me.down('button[name=save_btn]');
	        valid ? saveButton.enable() : saveButton.disable();
	    }, this);
	},

	items: [
    {
        xtype: 'form',
        width: 550,
        layout: 'vbox',
        border: false,
        defaults: {
            labelWidth: 150,
            labelAlign: 'right'
        },
        items: [
            {
                xtype: 'panel',
                margin: '5 0 5 0',
                border: false,
                layout: 'hbox',
                defaults: {
                    labelWidth: 150,
                    labelAlign: 'right'
                },
                items: [
                    {
                        xtype: 'hiddenfield',
                        name: 'id'
                    },
                    {
                        xtype: 'hiddenfield',
                        name: 'p_client'
                    },
                    {
                        xtype: 'textfield',
                        fieldLabel: 'Клиент',
                        width: 362,
                        name: 'p_client_display',
                        allowBlank: false,
                        readOnly: true
                    },
                    {
                        xtype: 'button',
                        text: 'Выбрать',
                        width: 90,
                        name: 'select_client_btn',
                        margin: '0 0 0 3'
                    }
                ]
            },
            {
                name: 'p_logo',
                fieldLabel: 'Логотип',
                xtype: 'filefield',
                width: 455,
                buttonConfig: {
                  width: 90
                },
                buttonText: 'Загрузить'
            },
            {
                xtype: 'textfield',
                vtype: 'email',
                fieldLabel: 'Email администратора',
                width: 455,
                name: 'p_email',
                allowBlank: false
            },
            {
                xtype: 'checkbox',
                fieldLabel: 'Сменить пароль',
                name: 'p_change_password'
            },
            {
                xtype: 'panel',
                border: false,
                layout: 'hbox',
                itemId: 'passwordPanel',
                defaults: {
                    labelWidth: 150,
                    labelAlign: 'right'
                },
                items:[
                    {
                        xtype: 'textfield',
                        fieldLabel: 'Пароль администратора',
                        readOnly: true,
                        name: 'p_pass',
                        width: 362,
                        allowBlank: true,
                        disabled: true
                    },
                    {
                        xtype: 'button',
                        text: 'Сгенерировать',
                        width: 90,
                        name: 'generate_pass_btn',
                        margin: '0 0 0 3',
                        disabled: true
                    }
                ]
            }
        ]
    }],
	dockedItems: [{
	    xtype: 'toolbar',
	    dock: 'bottom',
	    ui: 'footer',
	    items: [
            {
                xtype: 'tbspacer',
                flex: 1
            },
            {
                xtype: 'button',
                text: 'Сохранить',
                iconCls: 'icon-save-item',
                name: 'save_btn'
            },
            {
                xtype: 'tbspacer',
                flex: 1
            }
	    ]
	}],

	createClientWindow: function () {
	    if (!this.clientWindow) {
	        this.clientWindow = Ext.create('Cement.view.administration.organizations.ClientWindow');
	        this.clientWindow.down('grid').on('itemdblclick', function (cmp, rec) {
	            this.down('hiddenfield[name=p_client]').setValue(rec.get('id'));
	            this.down('textfield[name=p_client_display]').setValue(rec.get('p_name'));
	            this.clientWindow.hide();
	        }, this);
	    }
	},

    createNewItem: function(isNew) {
        this.down('checkbox[name=p_change_password]').setDisabled(isNew);
        var passTextField = this.down('textfield[name=p_pass]');
        passTextField.setDisabled(!isNew);
        passTextField.allowBlank = !isNew;
        passTextField.clearInvalid();
        this.down('button[name=generate_pass_btn]').setDisabled(!isNew);
    },

	generateNewPassword: function (length) {
        var charset = "abcdefghijklnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789",
        retVal = "";
        for (var i = 0, n = charset.length; i < length; ++i) {
            retVal += charset.charAt(Math.floor(Math.random() * n));
        }
        return retVal;
    }
});