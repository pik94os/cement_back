Ext.define('Cement.view.administration.superusers.List', {
    extend: 'Cement.view.basic.Grid',
    alias: 'widget.administration_superusers_lists',
    autoLoadStore: true,
    cls: 'simple-form',

    bbarText: 'Показаны пользователи {0} - {1} из {2}',
    bbarEmptyMsg: 'Нет пользователей',
    bbarUsersText: 'Пользователей на странице: ',

    gridStore: 'administration.Superusers',
    gridStateful: false,

    deleteUrl: Cement.Config.url.administration.superusers.deleteUrl,
    manageStateUrl: Cement.Config.url.administration.superusers.manageStateUrl,

    getActionColumns: function () {
        return {
            xtype: 'rowactions',
            hideable: false,
            resizeable: false,
            locked: true,
            width: 46,
            actions: [
                {
                    iconCls: 'icon-delete-item',
                    qtip: 'Удалить',
                    callback: this.deleteItem,
                    hideIndex: 'p_is_deletable'
                },
                {
                    iconCls: 'icon-lock',
                    qtip: 'Заблокировать',
                    hideIndex: 'p_is_blockable',
                    callback: this.manageUserStateFactory(true, 'Вы действительно хотите заблокировать пользователя?')
                },
                {
                    iconCls: 'icon-unlock',
                    qtip: 'Разблокировать',
                    hideIndex: 'p_is_unblockable',
                    callback: this.manageUserStateFactory(false, 'Вы действительно хотите разблокировать пользователя?')
                }
            ],
            keepSelection: true
        };
    },

    getFilterItems: function() {
        return [];
    },

    getGridColumns: function() {
        var result = [
            { text: 'Логин', dataIndex: 'p_login', flex: 1 },
            { text: 'Статус', dataIndex: 'p_state', flex: 1 }
        ];

        return this.mergeActions(result);
    },

    createItem: function () {
        var me = this;

        var win = Ext.create('Ext.window.Window', {
            cls: [Ext.baseCSSPrefix + 'message-box', Ext.baseCSSPrefix + 'hide-offsets'],
            layout: {
                type: 'vbox',
                align: 'stretch'
            },
            closable: false,
            width: 250,
            height: 113,
            resizable: false,
            draggable: false,
            modal: true,
            items: [new Ext.form.Panel({
                layout: {
                    type: 'vbox',
                    align: 'stretch'
                },
                border: false,
                padding: 10,
                bodyStyle: "background: #E8E8E8",
                items: [
                    {
                        xtype: 'label',
                        text: 'Логин (email)',
                        padding: 5
                    },
                    {
                        xtype: 'textfield',
                        vtype: 'email',
                        hideLabel: true,
                        maxLength: 200,
                        padding: 5,
                        enforceMaxLength: true
                    }
                ]
            })],
            dockedItems: [new Ext.toolbar.Toolbar({
                ui: 'footer',
                dock: 'bottom',
                layout: {
                    type: 'hbox',
                    pack: 'center'
                },
                items: [
                    {
                        xtype: 'button',
                        text: 'ОК',
                        minWidth: 75,
                        disabled: true,
                        role: 'doit'
                    }, {
                        xtype: 'button',
                        text: 'Отмена',
                        minWidth: 75,
                        role: 'close'
                    }
                ]
            })]
        });

        var saveButton = win.down('button[role=doit]');

        saveButton.on('click', function () {
            var textField = this.up('window').down('textfield');

            if (!textField.isValid()) {
                return;
            }
            
            win.setLoading("Сохранение");

            Ext.Ajax.request({
                method: Cement.Config.url.administration.superusers.saveMethod,
                url: Cement.Config.url.administration.superusers.saveUrl,
                params: {
                    login: textField.getValue()
                },
                callback: function() {
                    win.setLoading(false);
                },
                success: function () {
                    me.reloadGrid();
                    win.close();
                    delete win;
                },
                failure: function (response) {
                    Cement.Msg.error(response, 'Ошибка');
                }
            });
        });
        win.down('button[role=close]').on('click', function () {
            win.close();
            delete win;
        });
        
        win.down('form').on('validitychange', function (f, valid) {
            valid ? saveButton.enable() : saveButton.disable();
        }, this);

        win.show();
    },

    printItem: function (self, record) {
        
    },

    manageUserStateFactory: function (lock, text) {
        var me = this;
        return function (grid, record) {
            Ext.Msg.confirm('Внимание', text, function (btn) {
                if (btn == 'yes') {

                    me.setLoading("Обработка...");

                    var url = Ext.String.format(me.manageStateUrl, lock, record.get('id'));

                    Ext.Ajax.request({
                        url: url,
                        method: 'POST',
                        callback: function () {
                            me.setLoading(false);
                        },
                        success: function () {
                            me.reloadGrid();
                        },
                        failure: function () {
                            Cement.Msg.error(response, 'Ошибка');
                        }
                    });
                }
            });
        };
    }
});