Ext.define('Cement.view.administration.dictionaries.EditWindow', {
    extend: 'Ext.window.Window',
    xtype: 'administration_dictionaries_editwindow',
    width: 500,
    height: 220,
    hideCommentPanel: false,
    modal: true,
    title: '',
    layout: 'fit',
    bodyPadding: 10,
    closable: false,
    bodyStyle: "background: #f7f7f7",
    resizable: false,
    closeAction: 'hide',
    saveUrl: Cement.Config.url.administration.dictionaries.saveUrl,

    initComponent: function () {
        var me = this;
        me.items = me.getWinItems();
        this.callParent(arguments);

        this.down('button[name=save_btn]').on('click', function () {
            var window = me;
            var form = window.down('form').getForm();
            if (form.isValid()) {
                form.submit({
                    waitMsg: "Сохранение...",
                    url: me.saveUrl,
                    success: function () {
                        window.close();
                        window.gridStore.reload();
                    },
                    failure: function (self, result) {
                        var message = null;

                        try {
                            message = Ext.decode(result.response.responseText).Message;
                        } catch (e) {
                        }

                        Cement.Msg.error(message || 'Ошибка при сохранении информации');
                    }
                });
            }
        });

        this.down('button[name=cancel]').on('click', function () {
            me.close();
        });

        this.down('form').on('validitychange', function (f, valid) {
            var saveButton = me.down('button[name=save_btn]');
            valid ? saveButton.enable() : saveButton.disable();
        }, this);
    },

    getWinItems: function () {

        var nameRenderer = function (value) {
            return (value && value.Name) || '';
        };

        var measUnitStore = Ext.getStore('products.auxiliary.MeasureUnits');

        var res = [
            {
                xtype: 'form',
                width: 550,
                layout:
                {
                    type: 'vbox',
                    align: 'stretch'
                },
                border: false,
                defaults: {
                    //labelWidth: 150,
                    labelAlign: 'right',
                    height: 22,
                    xtype: 'textfield',
                    allowBlankCfg: false
                },
                items: [
                    {
                        xtype: 'hiddenfield',
                        name: 'id'
                    },
                    {
                        xtype: 'hiddenfield',
                        name: 'entityType'
                    },
                    {
                        name: 'PhotoLink',
                        bindedName: 'Photo',
                        height: 120,
                        xtype: 'cementimage',
                        margin: '0 0 5 0',
                        allowBlankCfg: true,
                        imgStyle: 'max-height: 120px; max-width: 500px;',
                    },
                    {
                        xtype: 'selectfield',
                        fieldLabel: 'Класс опасности',
                        name: 'Class',
                        idProperty: 'id',
                        entityType: 'DangerClass',
                        editable: false
                    },
                    {
                        xtype: 'selectfield',
                        fieldLabel: 'Марка',
                        name: 'Brand',
                        idProperty: 'id',
                        entityType: 'VehicleBrand',
                        editable: false
                    },
                    {
                        xtype: 'selectfield',
                        fieldLabel: 'Категория',
                        name: 'Category',
                        idProperty: 'id',
                        entityType: 'VehicleCategory',
                        editable: false,
                        listeners: {
                            change: this.categoryChange,
                            scope: this
                        }

                    },
                    {
                        xtype: 'selectfield',
                        fieldLabel: 'Подкатегория',
                        name: 'SubCategory',
                        idProperty: 'id',
                        entityType: 'VehicleSubCategory',
                        editable: false,
                        allowBlankCfg: true
                    },
                    {
                        xtype: 'selectfield',
                        fieldLabel: 'Модель',
                        name: 'Model',
                        idProperty: 'id',
                        entityType: 'VehicleModel',
                        editable: false,
                        columns: [
                            { text: 'Марка', dataIndex: 'Brand', renderer: nameRenderer, filter: { xtype: 'textfield' }, flex: 1 },
                            { text: 'Категория', dataIndex: 'Category', renderer: nameRenderer, filter: { xtype: 'textfield' }, flex: 1 },
                            { text: 'Подкатегория', dataIndex: 'SubCategory', renderer: nameRenderer, filter: { xtype: 'textfield' }, flex: 1 },
                            { text: 'Наименование', dataIndex: 'Name', filter: { xtype: 'textfield' }, flex: 1 }
                        ]
                    },
                    {
                        xtype: 'selectfield',
                        fieldLabel: 'Группа',
                        name: 'ProductGroup',
                        idProperty: 'id',
                        entityType: 'ProductGroup',
                        columns: [
                            { text: 'Наименование', dataIndex: 'Name', filter: { xtype: 'textfield' }, flex: 3 },
                            {
                                text: 'Тип',
                                flex: 1,
                                dataIndex: 'ProductType',
                                renderer: function (val) {
                                    switch (val) {
                                        case 10: return 'Товар';
                                        case 20: return 'Услуга';
                                        default: return '';
                                    }
                                },
                                filter: {
                                    xtype: 'combo',
                                    operand: CondExpr.operands.eq,
                                    queryMode: 'local',
                                    editable: false,
                                    displayField: 'name',
                                    //forceSelection: true,
                                    valueField: 'id',
                                    store: 
                                    Ext.create('Ext.data.Store', {
                                        fields: ['id', 'name'],
                                        data : [
                                            {"id": 10, "name":"Товар"},
                                            {"id": 20, "name":"Услуга"}
                                        ]
                                    })
                                }
                            }
                        ],
                        editable: false
                    },
                    {
                        fieldLabel: 'Группа',
                        name: 'MeasureUnitGroup',
                        xtype: 'combo',
                        queryMode: 'local',
                        editable: false,
                        displayField: 'name',
                        valueField: 'id',
                        store: measUnitStore
                    },
                    {
                        fieldLabel: 'Код',
                        name: 'Code'
                    },
                    {
                        fieldLabel: 'Наименование',
                        name: 'Name'
                    },
                    {
                        fieldLabel: 'Краткое наим-е',
                        name: 'ShortName'
                    },
                    {
                        xtype: 'textarea',
                        fieldLabel: 'Описание',
                        name: 'Description',
                        height: 150,
                        maxLength: 2000,
                        enforceMaxLength: true,
                        allowBlankCfg: true
                    },
                    {
                        fieldLabel: 'Полное наим-е',
                        name: 'FullName'
                    },
                    {
                        fieldLabel: 'БИК',
                        name: 'Bik',
                        allowBlankCfg: true
                    },
                    {
                        fieldLabel: 'Кор.счет',
                        name: 'Ks',
                        allowBlankCfg: true
                    },
                    {
                        fieldLabel: 'Тип',
                        name: 'ProductType',
                        xtype: 'combo',
                        queryMode: 'local',
                        editable: false,
                        displayField: 'name',
                        valueField: 'id',
                        store: Ext.create('Ext.data.Store', {
                            fields: ['id', 'name'],
                            data : [
                                {"id": 10, "name":"Товар"},
                                {"id": 20, "name":"Услуга"}
                            ]
                        })
                    },
                    {
                        xtype: 'numberfield',
                        fieldLabel: 'Значение',
                        name: 'Rate'
                    },
                    {
                        fieldLabel: 'Номер',
                        name: 'Num'
                    },
                    {
                        fieldLabel: 'Регион',
                        name: 'RegionName'
                    },
                    {
                        fieldLabel: 'Наименов-е ЖД',
                        name: 'RailwayName'
                    },
                    {
                        fieldLabel: 'Крат. наим-е ЖД',
                        name: 'RailwayShortName'
                    },
                    {
                        fieldLabel: 'Нацональная',
                        name: 'RailwayShortName'
                    },
                    {
                        xtype: 'checkbox',
                        inputValue: true,
                        fieldLabel: 'Национальная',
                        name: 'IsNational'
                    },
                    {
                        xtype: 'checkbox',
                        inputValue: true,
                        fieldLabel: 'Не вкл. в ЕСКК',
                        name: 'IsNotInEskk'
                    },
                    {
                        name: 'Photo',
                        fieldLabel: 'Фото',
                        xtype: 'filefield',
                        buttonText: 'Загрузить',
                        allowBlankCfg: true,
                        listeners: {
                            change: function () { this.down('cementimage').setValue(null); },
                            scope: this
                        }
                    }
                ]
            }
        ];

        return res;
    },
    dockedItems: [{
        xtype: 'toolbar',
        dock: 'bottom',
        ui: 'footer',
        items: [
            {
                xtype: 'tbspacer',
                flex: 1
            },
            {
                xtype: 'button',
                text: 'Сохранить',
                name: 'save_btn',
                width: 90
            },
            {
                xtype: 'button',
                text: 'Отмена',
                name: 'cancel',
                width: 90
            },
            {
                xtype: 'tbspacer',
                flex: 1
            }
        ]
    }],

    categoryChange: function (self, value) {
        var sf = this.down('selectfield[name=SubCategory]');

        sf.store.filters.removeAll();
        sf.store.filters.add(new Ext.util.Filter({
            property: 'entityType',
            value: sf.entityType
        }));
        sf.store.filters.add(new Ext.util.Filter({
            property: 'Category',
            value: value ? value.id : null
        }));

        sf.setValue(null);
    },
    
    manageFields: function (fields) {
        var heigth = 0;

        Ext.each(this.down('form').items.items, function (field) {
            if (field.xtype !== 'hiddenfield') {
                if (fields.indexOf(field.name) >= 0 || fields.indexOf(field.bindedName) >= 0) {
                    field.show();
                    field.allowBlank = field.allowBlankCfg;

                    heigth += field.height + 5;
                } else {
                    field.hide();
                    field.allowBlank = true;
                }
            }
        });

        this.setHeight(heigth + 76);
    }
});