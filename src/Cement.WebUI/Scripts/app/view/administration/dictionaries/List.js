Ext.define('Cement.view.administration.dictionaries.List', {
    extend: 'Cement.view.basic.Grid',
    alias: 'widget.administration_dictionaries_lists',
    autoLoadStore: false,
    cls: 'simple-form',

    overrideColumnHeaderHeight: false,

    bbarText: 'Показаны записи {0} - {1} из {2}',
    bbarEmptyMsg: 'Нет записей',
    bbarUsersText: 'Записей на странице: ',

    gridStore: 'administration.Dictionaries',
    gridStateful: false,

    showFilterButton: false,
    showSettings: false,

    entityType: null,
    entityProperties: null,

    deleteUrlTemplate: Cement.Config.url.administration.dictionaries.deleteUrl,

    entityListUrl: Cement.Config.url.administration.dictionaries.entityListUrl,
    
    getFilterItems: function() {
        return [];
    },

    getGridColumns: function () {
        var nameRenderer = function(value) {
            return (value && value.Name) || '';
        };
        
        var boolrenderer = function (val) {
            switch (val) {
                case true: return 'Да';
                case false: return 'Нет';
                default: return '';
            }
        };

        var productTypeRenderer = function (val) {
            switch (val) {
                case 10: return 'Товар';
                case 20: return 'Услуга';
                default: return '';
            }
        };

        var measUnitStore = Ext.getStore('products.auxiliary.MeasureUnits');
        var measUnitStore2 = Ext.create('Cement.store.products.auxiliary.MeasureUnits');
        measUnitStore2.insert(0, { id: null, name: '-' });

        var measureUnitGroupFilter = {
            xtype: 'combo',
            operand: CondExpr.operands.eq,
            queryMode: 'local',
            editable: false,
            displayField: 'name',
            valueField: 'id',
            store: measUnitStore2
        };

        var boolFilterFactory = function() {
            return {
                xtype: 'combo',
                operand: CondExpr.operands.eq,
                queryMode: 'local',
                editable: false,
                displayField: 'name',
                valueField: 'id',
                store: Ext.create('Ext.data.Store', {
                    fields: ['id', 'name'],
                    data: [
                        { "id": null, "name": "-" },
                        { "id": true, "name": "Да" },
                        { "id": false, "name": "Нет" }
                    ]
                })
            };
        };

        var productTypeFilterFactory= function() {
            return {
                xtype: 'combo',
                operand: CondExpr.operands.eq,
                queryMode: 'local',
                editable: false,
                displayField: 'name',
                valueField: 'id',
                store:
                    Ext.create('Ext.data.Store', {
                        fields: ['id', 'name'],
                        data: [
                            { "id": null, "name": "-" },
                            { "id": 10, "name": "Товар" },
                            { "id": 20, "name": "Услуга" }
                        ]
                    })
            };
        };

        var measureUnitRenderer = function (val) {
            var res = measUnitStore.getById(val);
            return (res && res.get('name')) || '';
        };

        var result = [
            { text: 'Марка', dataIndex: 'Brand', renderer: nameRenderer },
            { text: 'Марка', dataIndex: 'ModelBrand', mapping: 'Model.Brand.Name' },
            { text: 'Категория', dataIndex: 'Category', renderer: nameRenderer },
            { text: 'Категория', dataIndex: 'ModelCategory', mapping: 'Model.Category.Name' },
            { text: 'Подкатегория', dataIndex: 'SubCategory', renderer: nameRenderer },
            { text: 'Подкатегория', dataIndex: 'ModelSubCategory', mapping: 'Model.SubCategory.Name' },
            { text: 'Модель', dataIndex: 'Model', renderer: nameRenderer },
            { text: 'Класс опасности', dataIndex: 'Class', renderer: nameRenderer },
            { text: 'Тип', dataIndex: 'ProductGroupType', renderer: productTypeRenderer, filter: productTypeFilterFactory(), flex: 0.5, mapping: 'ProductGroup.ProductType' },
            { text: 'Группа', dataIndex: 'ProductGroup', renderer: nameRenderer },
            { text: 'Группа', dataIndex: 'MeasureUnitGroup', renderer: measureUnitRenderer, filter: measureUnitGroupFilter },
            { text: 'Код', dataIndex: 'Code' },
            { text: 'Наименование', dataIndex: 'Name', hidden: false },
            { text: 'Краткое наименование', dataIndex: 'ShortName' },
            { text: 'Номер', dataIndex: 'Num' },
            { text: 'Регион', dataIndex: 'RegionName' },
            { text: 'Наименование ЖД', dataIndex: 'RailwayName' },
            { text: 'Краткое наименование ЖД', dataIndex: 'RailwayShortName' },
            { text: 'Фактическое значение', dataIndex: 'Rate', filter: { xtype: 'numberfield', operand: CondExpr.operands.eq} },
            { text: 'Отображаемое значение', dataIndex: 'StringValue', sortable: false, filter: null },
            { text: 'Описание', dataIndex: 'Description', flex: 3 },
            { text: 'Нацональная', dataIndex: 'IsNational', renderer: boolrenderer, filter: boolFilterFactory() },
            { text: 'Не включена в ЕСКК', dataIndex: 'IsNotInEskk', renderer: boolrenderer, filter: boolFilterFactory() },
            { text: 'Полное наименование', dataIndex: 'FullName' },
            { text: 'БИК', dataIndex: 'Bik' },
            { text: 'Кор.счет', dataIndex: 'Ks' },
            { text: 'Тип', dataIndex: 'ProductType', flex: 0.5, renderer: productTypeRenderer, filter: productTypeFilterFactory() },
        ];

        Ext.each(result, function(column) {
            if (column.filter === undefined) {
                column.filter = { xtype: 'textfield' };
            }

            if (column.hidden === undefined) {
                column.hidden = true;
            }

            if (column.flex === undefined) {
                column.flex = 1;
            }

            if (column.mapping) {
                column.filterName = column.mapping;
                column.getSortParam = function() { return column.mapping; };
            }

            column.locable = false;
            column.menuDisabled = true;
        });

        return this.mergeActions(result);
    },

    getAdditionalButtons: function () {
        return [
        {
            xtype: 'combobox',
            name: 'type',
            queryMode: 'local',
            store: Ext.create('Ext.data.Store', {
                fields: ['entity', 'name', 'properties'],
                proxy: {
                    type: 'ajax',
                    url: this.entityListUrl,
                    reader: {
                        type: 'json',
                        root: 'data'
                    },
                    actionMethods: { read: 'POST' },
                },
                autoLoad: true
            }),
            fieldLabel: 'Тип',
            labelAlign: 'right',
            labelWidth: 50,
            width: 350,
            triggerAction: 'all',
            editable: false,
            valueField: 'entity',
            displayField: 'name',
            listConfig: {
                maxHeight: 600,
            },
            listeners: {
                change: this.entityTypeSelect,
                scope: this
            }
        }];
    },

    getActionColumns: function () {
        var me = this;

        return {
            xtype: 'rowactions',
            hideable: false,
            resizeable: false,
            locked: true,
            width: 46,
            actions: [
                {
                    iconCls: 'icon-edit-item',
                    qtip: 'Редактировать',
                    callback: this.editItem,
                    scope: me
                },
                {
                    iconCls: 'icon-delete-item',
                    qtip: 'Удалить',
                    callback: this.deleteItemOverride,
                    scope: me
                }
            ],
            keepSelection: true
        };
    },

    createItem: function () {
        this.showEditWindow(null);
    },

    editItem: function (grid, record) {
        grid.up('basicgrid').showEditWindow(record.data);
    },

    printItem: function (self, record) {
        
    },

    entityTypeSelect : function(self, newValue, oldValue, eOpts) {
        var grid = this.down('grid');

        var properties = self.valueModels[0].get('properties');

        Ext.each(grid.columns, function(column) {
            if (column.xtype !== 'rowactions') {
                if (properties.indexOf(column.dataIndex) >= 0) {
                    column.show();
                } else {
                    column.hide();
                }
            }
        });
        
        this.constantFilter = [{
            property: 'entityType',
            value: newValue
        }];

        this.entityType = newValue;
        this.entityProperties = properties;

        this.reloadGrid();
    },

    showEditWindow: function (record) {
        if (!this.entityType) {
            Cement.Msg.info('Выберите тип!');
            return;
        }

        var me = this;
        if (!this.editWindow) {
            this.editWindow = Ext.create('Cement.view.administration.dictionaries.EditWindow');
            this.editWindow.gridStore = me.down('grid').getStore();
        }

        this.editWindow.manageFields(this.entityProperties);

        var form = this.editWindow.down('form').getForm();
        form.reset(true);
        
        if (record) {
            form.setValues(record);
        }

        this.editWindow.down('hiddenfield[name=entityType]').setValue(this.entityType);
        this.editWindow.show();
    },

    deleteItemOverride: function (grid, record) {
        var basicGrid = grid.up('basicgrid');
        basicGrid.deleteUrl = Ext.String.format(basicGrid.deleteUrlTemplate, basicGrid.entityType, '{0}'),
        basicGrid.deleteItem(grid, record);
    },

    initComponent: function () {
        var store = Ext.getStore(this.gridStore);
        
        store.clearFilter(true);
        store.removeAll();

        this.constantFilter = null;
        this.callParent(arguments);
    }
});