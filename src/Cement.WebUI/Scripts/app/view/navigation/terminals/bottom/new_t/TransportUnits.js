Ext.define('Cement.view.navigation.terminals.bottom.new_t.TransportUnits', {
  extend: 'Cement.view.transport.units.lists.Archive',
  alias: 'widget.navigation_terminals_bottom_new_transport_units',

  gridStore: 'navigation.terminals.bottom.new_t.TransportUnits',
  gridStateId: 'stateNavigationTerminalsBottomNewTransportUnits'
});