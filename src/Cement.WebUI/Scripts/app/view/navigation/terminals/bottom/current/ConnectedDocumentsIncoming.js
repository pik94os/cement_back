Ext.define('Cement.view.navigation.terminals.bottom.current.ConnectedDocumentsIncoming', {
	extend: 'Cement.view.goods.auto.lists.ConnectedDocuments',
	alias: 'widget.navigation_terminals_bottom_current_connected_documents_incoming',

	gridStore: 'navigation.terminals.bottom.current.ConnectedDocumentsIncoming',
	gridStateId: 'stateNavigationTerminalsBottomCurrentConnectedDocumentsIncoming',

    printUrl: Cement.Config.url.navigation.terminals.documents.printGrid,
    helpUrl: Cement.Config.url.navigation.terminals.documents.help
});