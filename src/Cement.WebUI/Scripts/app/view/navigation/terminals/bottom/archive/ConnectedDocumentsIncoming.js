Ext.define('Cement.view.navigation.terminals.bottom.archive.ConnectedDocumentsIncoming', {
	extend: 'Cement.view.goods.auto.lists.ConnectedDocuments',
	alias: 'widget.navigation_terminals_bottom_archive_connected_documents_incoming',

	gridStore: 'navigation.terminals.bottom.archive.ConnectedDocumentsIncoming',
	gridStateId: 'stateNavigationTerminalsBottomArchiveConnectedDocumentsIncoming',

    printUrl: Cement.Config.url.navigation.terminals.documents.printGrid,
    helpUrl: Cement.Config.url.navigation.terminals.documents.help
});