Ext.define('Cement.view.navigation.terminals.bottom.archive.TransportUnits', {
  extend: 'Cement.view.transport.units.lists.Archive',
  alias: 'widget.navigation_terminals_bottom_archive_transport_units',

  gridStore: 'navigation.terminals.bottom.archive.TransportUnits',
  gridStateId: 'stateNavigationTerminalsBottomArchiveTransportUnits'
});