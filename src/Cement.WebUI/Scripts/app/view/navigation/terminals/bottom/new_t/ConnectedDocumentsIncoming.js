Ext.define('Cement.view.navigation.terminals.bottom.new_t.ConnectedDocumentsIncoming', {
	extend: 'Cement.view.goods.auto.lists.ConnectedDocuments',
	alias: 'widget.navigation_terminals_bottom_new_connected_documents_incoming',

	gridStore: 'navigation.terminals.bottom.new_t.ConnectedDocumentsIncoming',
	gridStateId: 'stateNavigationTerminalsBottomNewConnectedDocumentsIncoming',

    printUrl: Cement.Config.url.navigation.terminals.documents.printGrid,
    helpUrl: Cement.Config.url.navigation.terminals.documents.help
});