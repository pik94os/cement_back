Ext.define('Cement.view.navigation.terminals.bottom.current.TransportUnits', {
  extend: 'Cement.view.transport.units.lists.Archive',
  alias: 'widget.navigation_terminals_bottom_current_transport_units',

  gridStore: 'navigation.terminals.bottom.current.TransportUnits',
  gridStateId: 'stateNavigationTerminalsBottomCurrentTransportUnits'
});