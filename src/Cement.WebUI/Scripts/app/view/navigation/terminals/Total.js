Ext.define('Cement.view.navigation.terminals.Total', {
	extend: 'Cement.view.basic.Tabs',
	title: 'Терминалы',

	items: [{
		xtype: 'navigation_terminals_complex_new',
		title: 'Новые'
	}, {
		xtype: 'navigation_terminals_complex_current',
		title: 'Действующие'
	}, {
		xtype: 'navigation_terminals_complex_archive',
		title: 'Архив'
	}],

	initComponent: function () {
		Ext.apply(this, {
			title: 'Терминалы'
		});
		this.callParent(arguments);
	}
});