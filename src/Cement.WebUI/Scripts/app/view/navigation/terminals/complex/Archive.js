Ext.define('Cement.view.navigation.terminals.complex.Archive', {
	extend: 'Cement.view.basic.Complex',
	alias: 'widget.navigation_terminals_complex_archive',
    border: 0,
    enableDetail: false,
    autoLoadStore: false,
    priceId: null,
    bodyPadding: 0,
    padding: 0,
    header: false,
    title: null,
    rowsCount: 3,
    title: 'Архив',
    tabTitle: 'Архив',
    cls: 'no-side-borders',
    topXType: 'navigation_terminals_lists_archive',
    topDetailType: 'Cement.view.navigation.terminals.view.View',
    bottomTitle: 'Связанные документы',
    bottomTabs: [{
        title: 'Связанные документы',
        xtype: 'navigation_terminals_bottom_archive_connected_documents_incoming',
        detailType: 'Cement.view.goods.auto.view.ConnectedDocument',
        constantFilterParam: 'p_base_doc',
        childrenConstantFilterParam: 'p_base_doc',
        border: 0
    }],
    subBottomTitle: 'Справочник',
    subBottomTabs: [{
        title: 'Транспортная единица',
        shownTitle: 'Транспортная единица',
        xtype: 'navigation_terminals_bottom_archive_transport_units',
        detailType: 'Cement.view.transport.units.ViewArchive'
    }]
});