Ext.define('Cement.view.navigation.terminals.complex.New', {
	extend: 'Cement.view.basic.Complex',
	alias: 'widget.navigation_terminals_complex_new',
    border: 0,
    enableDetail: false,
    autoLoadStore: false,
    priceId: null,
    bodyPadding: 0,
    padding: 0,
    header: false,
    title: null,
    rowsCount: 3,
    title: 'Новые',
    tabTitle: 'Новые',
    cls: 'no-side-borders',
    topXType: 'navigation_terminals_lists_new',
    topDetailType: 'Cement.view.navigation.terminals.view.View',
    bottomTitle: 'Связанные документы',
    bottomTabs: [{
        title: 'Связанные документы',
        xtype: 'navigation_terminals_bottom_new_connected_documents_incoming',
        detailType: 'Cement.view.goods.auto.view.ConnectedDocument',
        constantFilterParam: 'p_base_doc',
        childrenConstantFilterParam: 'p_base_doc',
        border: 0
    }],
    subBottomTitle: 'Справочник',
    subBottomTabs: [{
        title: 'Транспортная единица',
        shownTitle: 'Транспортная единица',
        xtype: 'navigation_terminals_bottom_new_transport_units',
        detailType: 'Cement.view.transport.units.ViewArchive'
    }]
});