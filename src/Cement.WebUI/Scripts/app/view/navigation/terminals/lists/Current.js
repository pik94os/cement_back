Ext.define('Cement.view.navigation.terminals.lists.Current', {
	extend: 'Cement.view.navigation.terminals.lists.New',
	alias: 'widget.navigation_terminals_lists_current',

	gridStore: 'navigation.terminals.Current',
	gridStateId: 'stateNavigationTerminalsCurrent',

	printUrl: Cement.Config.url.navigation.terminals.current.printGrid,
	helpUrl: Cement.Config.url.navigation.terminals.current.help,
    
    getActionColumns: function (argument) {
        return null;
    }
});