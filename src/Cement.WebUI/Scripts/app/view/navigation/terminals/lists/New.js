Ext.define('Cement.view.navigation.terminals.lists.New', {
	extend: 'Cement.view.basic.Grid',
	alias: 'widget.navigation_terminals_lists_new',
    autoLoadStore: true,

    bbarText: 'Показаны терминалы {0} - {1} из {2}',
    bbarEmptyMsg: 'Нет терминалов',
    bbarUsersText: 'Терминалов на странице: ',

	gridStore: 'navigation.terminals.New',
	gridStateId: 'stateNavigationTerminalsNew',

	printUrl: Cement.Config.url.navigation.terminals.new_t.printGrid,
	helpUrl: Cement.Config.url.navigation.terminals.new_t.help,
    deleteUrl: Cement.Config.url.navigation.terminals.new_t.deleteUrl,

    shownTitle: null,

    getFilterItems: function () {
        return [{
            text: 'Наименование',
            kind: 'selector',
            field_name: 'p_name',
            checked: true
        }, {
            text: 'Метка',
            kind: 'selector',
            field_name: 'p_label',
            checked: true
        }, {
            text: 'ТС марка, модель',
            kind: 'selector',
            field_name: 'p_transport_model_display',
            checked: true
        }, {
            text: 'ТС гос. номер',
            kind: 'selector',
            field_name: 'p_transport_number_display',
            checked: true
        }, {
            text: 'Нав. данные IMEI',
            kind: 'selector',
            field_name: 'p_nav_imei',
            checked: true
        }, {
            text: 'Нав. данные связь',
            kind: 'selector',
            field_name: 'p_nav_connect',
            checked: true
        }, {
            text: 'Нав. данные сигнал',
            kind: 'selector',
            field_name: 'p_nav_signal',
            checked: true
        }, {
            text: 'Нав. данные зажигание',
            kind: 'selector',
            field_name: 'p_nav_ignition',
            checked: true
        }, {
            text: 'Нав. данные состояние',
            kind: 'selector',
            field_name: 'p_nav_state',
            checked: true
        }, {
            text: 'Нав. данные пробег спутник',
            kind: 'selector',
            field_name: 'p_nav_odometer_sat',
            checked: true
        }, {
            text: 'Нав. данные пробег спид',
            kind: 'selector',
            field_name: 'p_nav_odometer_speed',
            checked: true
        }, {
            text: 'Нав. данные тахометр',
            kind: 'selector',
            field_name: 'p_nav_tahometer',
            checked: true
        }, {
            text: 'Нав. данные скорость',
            kind: 'selector',
            field_name: 'p_nav_speed',
            checked: true
        }, {
            text: 'Нав. данные высота',
            kind: 'selector',
            field_name: 'p_nav_altitude',
            checked: true
        }, {
            text: 'Нав. данные вход 1',
            kind: 'selector',
            field_name: 'p_nav_input_1',
            checked: true
        }, {
            text: 'Нав. данные вход 2',
            kind: 'selector',
            field_name: 'p_nav_input_2',
            checked: true
        }, {
            text: 'Нав. данные вход 3',
            kind: 'selector',
            field_name: 'p_nav_input_3',
            checked: true
        }, {
            text: 'Нав. данные вход 4',
            kind: 'selector',
            field_name: 'p_nav_input_4',
            checked: true
        }, {
            text: 'Нав. данные АЦП 1',
            kind: 'selector',
            field_name: 'p_nav_adc_1',
            checked: true
        }, {
            text: 'Нав. данные АЦП 2',
            kind: 'selector',
            field_name: 'p_nav_adc_2',
            checked: true
        }, {
            text: 'Нав. данные бак выровненный',
            kind: 'selector',
            field_name: 'p_nav_tank_aligned',
            checked: true
        }, {
            text: 'Нав. данные бак исходный',
            kind: 'selector',
            field_name: 'p_nav_tank_initial',
            checked: true
        }, {
            text: 'Нав. данные расход',
            kind: 'selector',
            field_name: 'p_nav_consumption',
            checked: true
        }, {
            text: 'Нав. данные цвет',
            kind: 'selector',
            field_name: 'p_nav_color',
            checked: true
        }, {
            text: 'Дата начала',
            kind: 'selector',
            field_name: 'p_date_start',
            checked: true
        }, {
            text: 'Дата окончания',
            kind: 'selector',
            field_name: 'p_date_end',
            checked: true
        }];
    },

    getGridColumns: function () {
        var result = [
            { text: 'Наименование', dataIndex: 'p_name', width: 150, locked: true },
            { text: 'Метка', dataIndex: 'p_label', width: 90, locked: true },
            { text: 'Транспортное средство', locked: true,
                columns: [
                    { text: 'Марка, модель', dataIndex: 'p_transport_model_display', width: 140 },
                    { text: 'Гос. номер', dataIndex: 'p_transport_number_display', width: 80 }
                ]
            },
            { text: 'Навигационные данные',
                columns: [
                    { text: 'IMEI', dataIndex: 'p_nav_imei', width: 90 },
                    { text: 'Связь', dataIndex: 'p_nav_connect', width: 90 },
                    { text: 'Сигнал', dataIndex: 'p_nav_signal', width: 90 },
                    { text: 'Зажигание', dataIndex: 'p_nav_ignition', width: 90 },
                    { text: 'Состояние', dataIndex: 'p_nav_state', width: 90 },
                    { text: 'Пробег спутник', dataIndex: 'p_nav_odometer_sat', width: 90 },
                    { text: 'Пробег спид.', dataIndex: 'p_nav_odometer_speed', width: 90 },
                    { text: 'Тахометр', dataIndex: 'p_nav_tahometer', width: 90 },
                    { text: 'Скорость', dataIndex: 'p_nav_speed', width: 90 },
                    { text: 'Высота', dataIndex: 'p_nav_altitude', width: 90 },
                    { text: 'Вход 1', dataIndex: 'p_nav_input_1', width: 90 },
                    { text: 'Вход 2', dataIndex: 'p_nav_input_2', width: 90 },
                    { text: 'Вход 3', dataIndex: 'p_nav_input_3', width: 90 },
                    { text: 'Вход 4', dataIndex: 'p_nav_input_4', width: 90 },
                    { text: 'АЦП 1', dataIndex: 'p_nav_adc_1', width: 90 },
                    { text: 'АЦП 2', dataIndex: 'p_nav_adc_2', width: 90 },
                    { text: 'Бак выровненный', dataIndex: 'p_nav_tank_aligned', width: 90 },
                    { text: 'Бак исходный', dataIndex: 'p_nav_tank_initial', width: 90 },
                    { text: 'Расход', dataIndex: 'p_nav_consumption', width: 90 },
                    { text: 'Цвет', dataIndex: 'p_nav_color', width: 90 }
                ]
            },
            { text: 'Дата начала', dataIndex: 'p_date_start', width: 90 },
            { text: 'Дата окончания', dataIndex: 'p_date_end', width: 90 }
        ];
        return this.mergeActions(result);
    },

    initComponent: function () {
        Ext.apply(this, {
            title: this.shownTitle
        });
        this.callParent(arguments);
    }
});