Ext.define('Cement.view.navigation.terminals.lists.Archive', {
	extend: 'Cement.view.navigation.terminals.lists.New',
	alias: 'widget.navigation_terminals_lists_archive',

	gridStore: 'navigation.terminals.Archive',
	gridStateId: 'stateNavigationTerminalsArchive',

	printUrl: Cement.Config.url.navigation.terminals.archive.printGrid,
	helpUrl: Cement.Config.url.navigation.terminals.archive.help,
    
    getActionColumns: function (argument) {
        return null;
    }
});