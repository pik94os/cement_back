Ext.define('Cement.view.navigation.terminals.form.Form', {
	extend: 'Cement.view.basic.Form',
	alias: 'widget.navigation_terminals_form_form',
	fieldsDisabled: false,
	url: Cement.Config.url.navigation.terminals.new_t.saveUrl,
	method: Cement.Config.url.navigation.terminals.new_t.saveMethod,
	layout: 'fit',
	allowBlankFields: true,
	saveButtonDisabled: false,

	serviceSuppliersStoreId: 'navigation.terminals.auxiliary.ServiceSuppliers',
  colorsStoreId: 'navigation.terminals.auxiliary.Colors',
  labelsStoreId: 'navigation.terminals.auxiliary.Labels',

	getItems: function () {
    var listConfig = {
        getInnerTpl: function(displayField) {
          var tpl = '<div class="color-dd-item">'+
                    '<div style="background: {p_color_code};" class="color-dd-item-bar"></div>'+
                    '<div class="color-dd-item-text">{p_name}</div></div>';
          return tpl;
        }
    },
      listConfigLabels = {
        getInnerTpl: function(displayField) {
          var tpl = '<div class="color-dd-item">'+
                      '<img class="color-dd-item-bar" src={p_label_src}/>'+
                      '<div class="color-dd-item-text">{p_name}</div></div>';
          return tpl;
        }
    };
		return [{
			xtype: 'fieldset',
			border: 0,
			margin: 0,
			padding: '0 40',
      collapsed: false,
      collapsible: false,
      defaults: {
        xtype: 'textfield',
        anchor: '100%',
        labelWidth: 150,
        labelAlign: 'right'
      },
      items: [{
      	name: 'p_name',
      	fieldLabel: 'Наименование'
      }, {
        xtype: 'datefield',
        name: 'p_date_start',
        fieldLabel: 'Дата'
      }, 
      this.getCombo('Поставщик услуг', 'p_service_supplier', this.serviceSuppliersStoreId, this.serviceSupplierChanged), {
      	name: 'p_ip',
      	fieldLabel: 'IP адрес',
        disabled: true
      }, {
        name: 'p_port',
        fieldLabel: 'Порт',
        disabled: true
      }, {
        name: 'p_nav_imei',
        fieldLabel: 'IMEI'
      },
    	this.getSelectorField('Транспортное средство', 'p_transport_unit', 'show_tu_window', 150), 
      this.getCombo('Цвет трека', 'p_nav_color', this.colorsStoreId, null, false, listConfig),
      this.getCombo('Метка терминала', 'p_label', this.labelsStoreId, null, false, listConfigLabels),
      {
        xtype: 'fieldcontainer',
        fieldLabel: 'Показатели 1',
        defaultType: 'checkboxfield',
        layout: 'column',
        defaults: {
          columnWidth: 0.25
        },
        items: [{
          boxLabel: 'Индикатор 1',
          name: 'p_param_1_ind_1',
          value: 1
        }, {
          boxLabel: 'Индикатор 2',
          name: 'p_param_1_ind_2',
          value: 2
        }, {
          boxLabel: 'Индикатор 3',
          name: 'p_param_1_ind_3',
          value: 3
        }, {
          boxLabel: 'Индикатор 4',
          name: 'p_param_1_ind_4',
          value: 4
        }, {
          boxLabel: 'Индикатор 5',
          name: 'p_param_1_ind_5',
          value: 5
        }, {
          boxLabel: 'Индикатор 6',
          name: 'p_param_1_ind_6',
          value: 6
        }, {
          boxLabel: 'Индикатор 7',
          name: 'p_param_1_ind_7',
          value: 7
        }, {
          boxLabel: 'Индикатор 8',
          name: 'p_param_1_ind_8',
          value: 8
        }, {
          boxLabel: 'Индикатор 9',
          name: 'p_param_1_ind_9',
          value: 9
        }, {
          boxLabel: 'Индикатор 10',
          name: 'p_param_1_ind_10',
          value: 10
        }, {
          boxLabel: 'Индикатор 11',
          name: 'p_param_1_ind_11',
          value: 11
        }, {
          boxLabel: 'Индикатор 12',
          name: 'p_param_1_ind_12',
          value: 12
        }]
      }, {
        xtype: 'fieldcontainer',
        fieldLabel: 'Показатели 2',
        defaultType: 'checkboxfield',
        layout: 'column',
        defaults: {
          columnWidth: 0.25
        },
        items: [{
          boxLabel: 'Индикатор 1',
          name: 'p_param_2_ind_1',
          value: 1
        }, {
          boxLabel: 'Индикатор 2',
          name: 'p_param_2_ind_2',
          value: 2
        }, {
          boxLabel: 'Индикатор 3',
          name: 'p_param_2_ind_3',
          value: 3
        }, {
          boxLabel: 'Индикатор 4',
          name: 'p_param_2_ind_4',
          value: 4
        }, {
          boxLabel: 'Индикатор 5',
          name: 'p_param_2_ind_5',
          value: 5
        }, {
          boxLabel: 'Индикатор 6',
          name: 'p_param_2_ind_6',
          value: 6
        }, {
          boxLabel: 'Индикатор 7',
          name: 'p_param_2_ind_7',
          value: 7
        }, {
          boxLabel: 'Индикатор 8',
          name: 'p_param_2_ind_8',
          value: 8
        }, {
          boxLabel: 'Индикатор 9',
          name: 'p_param_2_ind_9',
          value: 9
        }, {
          boxLabel: 'Индикатор 10',
          name: 'p_param_2_ind_10',
          value: 10
        }, {
          boxLabel: 'Индикатор 11',
          name: 'p_param_2_ind_11',
          value: 11
        }, {
          boxLabel: 'Индикатор 12',
          name: 'p_param_2_ind_12',
          value: 12
        }]
      }, {
        xtype: 'fieldcontainer',
        fieldLabel: 'Показатели 3',
        defaultType: 'checkboxfield',
        layout: 'column',
        defaults: {
          columnWidth: 0.25
        },
        items: [{
          boxLabel: 'Индикатор 1',
          name: 'p_param_3_ind_1',
          value: 1
        }, {
          boxLabel: 'Индикатор 2',
          name: 'p_param_3_ind_2',
          value: 2
        }, {
          boxLabel: 'Индикатор 3',
          name: 'p_param_3_ind_3',
          value: 3
        }, {
          boxLabel: 'Индикатор 4',
          name: 'p_param_3_ind_4',
          value: 4
        }, {
          boxLabel: 'Индикатор 5',
          name: 'p_param_3_ind_5',
          value: 5
        }, {
          boxLabel: 'Индикатор 6',
          name: 'p_param_3_ind_6',
          value: 6
        }, {
          boxLabel: 'Индикатор 7',
          name: 'p_param_3_ind_7',
          value: 7
        }, {
          boxLabel: 'Индикатор 8',
          name: 'p_param_3_ind_8',
          value: 8
        }, {
          boxLabel: 'Индикатор 9',
          name: 'p_param_3_ind_9',
          value: 9
        }, {
          boxLabel: 'Индикатор 10',
          name: 'p_param_3_ind_10',
          value: 10
        }, {
          boxLabel: 'Индикатор 11',
          name: 'p_param_3_ind_11',
          value: 11
        }, {
          boxLabel: 'Индикатор 12',
          name: 'p_param_3_ind_12',
          value: 12
        }]
      }]
    }];
	},

	initComponent: function () {
		this.callParent(arguments);

    this.down('button[action=show_tu_window]').on('click', function () {
      this.createTUWindow();
      this.tuWindow.show();
    }, this);

    Ext.getStore(this.serviceSuppliersStoreId).load();
	},

  serviceSupplierChanged: function (combo, newValue) {
    var form = combo.up('basicform'),
      val = combo.getStore().getById(newValue);
    form.down('textfield[name=p_ip]').setValue(val.get('p_ip'));
    form.down('textfield[name=p_port]').setValue(val.get('p_port'));
  },

  createTUWindow: function () {
    if (!this.tuWindow) {
      this.tuWindow = Ext.create('Cement.view.navigation.terminals.form.TransportUnitWindow');
      this.tuWindow.on('requestselected', function (rec) {
        this.down('textfield[name=p_transport_unit_display]').setValue(rec.get('p_name'));
        this.down('hiddenfield[name=p_transport_unit]').setValue(rec.get('id'));
        this.tuWindow.hide();
      }, this);
    }
  },

	afterRecordLoad: function (rec) {
		this.createTUWindow();
	}
});