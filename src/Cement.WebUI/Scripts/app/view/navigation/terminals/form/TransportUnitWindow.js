Ext.define('Cement.view.navigation.terminals.form.TransportUnitWindow', {
  extend: 'Cement.view.goods.auto.form.TransportUnitWindow',
  xtype: 'navigation_terminals_form_transport_unit_window',
  srcStoreId: 'navigation.terminals.auxiliary.TransportUnits'
});