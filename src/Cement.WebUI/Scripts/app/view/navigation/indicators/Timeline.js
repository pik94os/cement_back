Ext.define('Cement.view.navigation.indicators.Timeline', {
  extend: 'Ext.view.View',
  alias: 'widget.navigation_indicators_timeline',
  cls: 'navigation-indicators-grid navigation-indicators-timeline',
  itemSelector: 'div.indicator-value',
  emptyText: '',

  setTableSize: function (sz) {
    this.tableSize = sz;
    this.refresh();
  },
  
  initComponent: function () {
    var 
      me = this,
      imageTpl = new Ext.XTemplate(
        '<div style="height: 45px; position: relative;">',
          '<table class="stripes" cellspacing="0" cellpadding="0" width="{[ this.getTableSize() ]}px;">',
            '<tr class="head">',
              '{[ this.renderTopTable() ]}',
            '</tr>',
            '<tr>',
              '{[ this.renderTable(values) ]}',
            '</tr>',
          '</table>',
        '</div>',
        '<tpl for=".">',
        '</tpl>',
        {
          getTableSize: function () {
            return me.tableSize;
          },

          getHeight: function (val) {
            result = 0;
            Ext.each(val, function (item) {
              if (me.parentView.isIndicatorVisible(item.p_code)) {
                if (item.kind == 'markers') {
                  result += 16;
                }
                else {
                  result += 7;
                }
              }
            });
            if (result > 0) {
              result += 1;
              if (result < 47) {
                result = 47;
              }
              // result += 'px';
            }
            else {
              result = 'auto'
            }
            return result;
          },

          renderTopTable: function () {
            var result = '',
              today = Ext.Date.format(new Date(), 'd.m.Y'),
              fmtDigit = function (dig) {
                if (dig < 10) {
                  dig = '0' + dig;
                }
                return dig;
              }
            if (me.parentView.rangeKind == 'week') {
              var start_dt = Ext.Date.add(me.parentView.startDate, Ext.Date.DAY, - me.parentView.startDate.getDay());
              for (i = 0; i < 9; i++) {
                var cls = '';
                if (i == 0 || i == 8) {
                  cls = 'indicator-filled';
                }
                var dt = Ext.Date.format(Ext.Date.add(start_dt, Ext.Date.DAY, i), 'D d');
                if (i == 0) {
                  dt = Ext.Date.format(Ext.Date.add(start_dt, Ext.Date.DAY, i), 'M, d Y');
                }
                result += '<th width="11.111%" colspan="4" class="' + cls + '"><span class="head-text">' + dt + '</span></th>';
              }
            }
            if (me.parentView.rangeKind == 'month') {
              var start_dt = Ext.Date.getFirstDateOfMonth(me.parentView.startDate),
                last_dt = Ext.Date.getLastDateOfMonth(me.parentView.startDate);
              last_dt.setHours(23);
              var days = Math.ceil((last_dt - start_dt) / (60 * 60 * 24 * 1000)),
                txt1 = Ext.Date.format(me.parentView.startDateFact, 'M, Y'),
                txt2 = Ext.Date.format(me.parentView.startDate, 'M, Y'),
                txt3 = Ext.Date.format(me.parentView.endDateFact, 'M');
              result += '<th colspan="4" width="10.256%"><span class="head-text">' + txt1 + '</span></th>';
              result += '<th colspan="' + days + '" width="' + (days / 39 * 100) + '%" class="head-al-right"><span class="head-text">' + txt2 + '</span></th>';
              result += '<th colspan="' + (35 - days) + '" width="' + ((35 - days) / 39 * 100) + '%"><span class="head-text">' + txt3 + '</span></th>';
            }
            if (me.parentView.rangeKind == 'day') {
              var cls = "indicator-filled",
                times = [ '18:00', '00:00', '06:00', '12:00', '18:00', '00:00' ],
                addTxt = '';
              for (i = 0; i < 6; i++) {
                addTxt = '';
                if (i == 0 || i == 5) {
                  cls = 'indicator-filled';
                }
                else {
                  cls = '';
                }
                if (i == 0) {
                  addTxt = Ext.Date.format(Ext.Date.add(me.parentView.startDate, Ext.Date.HOUR, -6), ' D, M d, Y');
                }
                if (i == 1) {
                  addTxt = Ext.Date.format(me.parentView.startDate, ' D, M d');
                }
                if (i == 5) {
                  addTxt = Ext.Date.format(Ext.Date.add(me.parentView.startDate, Ext.Date.HOUR, 30), ' D, M d');
                }
                result += '<th width="16.66%" colspan="6" class="' + cls + '"><span class="head-text">' + times[i] + addTxt + '</span></th>';
                // result += '<th>' + Ext.Date.format(Ext.Date.add(me.parentView.startDate, Ext.Date.DAY, 5 * i), 'd.m.Y') + '</th>';
              }
            }
            if (me.parentView.rangeKind == 'hour') {
              var cls = "indicator-filled",
                addTxt = '';
              for (i = 0; i < 6; i++) {
                addTxt = '';
                if (i == 0 || i == 5) {
                  cls = 'indicator-filled';
                }
                else {
                  cls = '';
                }
                if (i == 0) {
                  addTxt = Ext.Date.format(Ext.Date.add(me.parentView.startDate, Ext.Date.MINUTE, -15), 'H.i D,M d, Y');
                }
                if (i == 1 || i == 5) {
                  addTxt = Ext.Date.format(Ext.Date.add(me.parentView.startDate, Ext.Date.MINUTE, 0), 'H.i D,M d');
                }
                if (i > 1 && i < 5) {
                  addTxt = Ext.Date.format(Ext.Date.add(me.parentView.startDate, Ext.Date.MINUTE, (i - 1) * 15), 'H.i');
                }
                result += '<th width="16.66%" colspan="3" class="' + cls + '"><span class="head-text">' + addTxt + '</span></th>';
              }
            }
            return result;
          },

          renderTable: function (values) {
            var result = '',
              height = this.getHeight(values[0].p_indicators) * values.length;
            if (me.parentView.rangeKind == 'week') {
              var first = me.parentView.startDate.getDate() - me.parentView.startDate.getDay() + 1;
                first_dt = Ext.Date.add(me.parentView.startDate, Ext.Date.DAY, - me.parentView.startDate.getDay() + 1),
                cur = new Date();
              for (i = 0; i < 36; i++) {
                var cls = '',
                  st = Ext.Date.add(first_dt, Ext.Date.HOUR, (i - 4) * 6),
                  en = Ext.Date.add(first_dt, Ext.Date.HOUR, (i - 3) * 6);
                if (i < 4 || i > 31) {
                  cls = 'indicator-filled';
                }
                if ((i + 1) % 4 == 0) {
                  cls += ' solid';
                }
                if (cur >= st && cur <= en) {
                  cls += ' now';
                }
                result += '<td style="height:' + height + 'px;" class="' + cls + '"></td>';
              }
            }
            if (me.parentView.rangeKind == 'month') {
              var start_dt = Ext.Date.getFirstDateOfMonth(me.parentView.startDate),
                last_dt = Ext.Date.getLastDateOfMonth(me.parentView.startDate),
                now = Ext.Date.format(new Date(), 'd.m.Y');
              last_dt.setHours(23);
              var days = Math.ceil((last_dt - start_dt) / (60 * 60 * 24 * 1000));
              for (i = 0; i < days + 8; i++) {
                var cls = '';
                if (i < 4 || i > days + 3) {
                  cls = 'indicator-filled';
                }
                if (i == 3 || i == 3 + days) {
                  cls += ' solid';
                }
                if (now == Ext.Date.format(Ext.Date.add(start_dt, Ext.Date.DAY, (i - 4)), 'd.m.Y')) {
                  cls += ' now';
                }
                result += '<td style="height:' + height + 'px;" class="' + cls + '"></td>';
              }
            }
            if (me.parentView.rangeKind == 'day') {
              var cls = "indicator-day-filled",
                now = Ext.Date.format(new Date(), 'd.m.Y.H');
              for (i = 0; i < 36; i++) {
                if (i < 6 || i > 29) {
                  cls = "indicator-filled";
                }
                else {
                  cls = "";
                }
                if (now == Ext.Date.format(Ext.Date.add(me.parentView.startDate, Ext.Date.HOUR, i-6), 'd.m.Y.H')) {
                  cls = 'now';
                }
                if ((i + 1) % 6 == 0) {
                  cls += ' solid';
                }
                result += '<td style="height:' + height + 'px;" class="' + cls + '"></td>';
              }
            }
            if (me.parentView.rangeKind == 'hour') {
              var cls = "indicator-day-filled",
                now = new Date();
              for (i = 0; i < 18; i++) {
                var st = Ext.Date.add(me.parentView.startDate, Ext.Date.MINUTE, (i - 3) * 5),
                  en = Ext.Date.add(me.parentView.startDate, Ext.Date.MINUTE, (i - 2) * 5);
                if (i < 3 || i > 14) {
                  cls = "indicator-filled";
                }
                else {
                  cls = "";
                }
                if (now >= st && now <= en) {
                  cls = 'now';
                }
                if ((i + 1) % 3 == 0) {
                  cls += ' solid';
                }
                result += '<td style="height:' + height + 'px;" class="' + cls + '"></td>';
              }
            }
            return result;
          }
        }
    );
    Ext.apply(this, {
      tpl: imageTpl
    });
    this.callParent(arguments);
    this.on('afterrender', function () {
      this.parentView = this.up('navigation_indicators_panel');
    }, this);
  }
});