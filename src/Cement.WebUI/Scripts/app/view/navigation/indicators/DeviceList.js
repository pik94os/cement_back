Ext.define('Cement.view.navigation.indicators.DeviceList', {
  extend: 'Ext.view.View',
  alias: 'widget.navigation_indicators_device_list',
  // store: Ext.getStore('navigation.Indicators'),
  itemSelector: 'div.thumb-wrap',
  emptyText: 'No images available',
  width: 1900,

  initComponent: function () {
    var me = this,
      listTpl = new Ext.XTemplate(
      '<tpl for=".">',
        '<div class="indicator-device-name" style="height: {[ this.getHeight(values) ]};">',
          '<span class="text-value">{p_name}<br/>{p_number}</span>',
        '</div>',
      '</tpl>',
      {
        getHeight: function (val) {
          result = 0;
          Ext.each(val.p_indicators, function (item) {
            if (me.parentView.isIndicatorVisible(item.p_code)) {
              if (item.kind == 'markers') {
                result += 32;
              }
              else {
                result += 7;
              }
            }
          });
          if (result > 0) {
            result += 1;
            if (result < 36) {
              result = 36;
            }
            result += 'px';
          }
          else {
            result = 'auto;'
          }
          if (result < 20) {

          }
          return result;
        }
      }
    );
    Ext.apply(this, {
      tpl: listTpl
    });
    this.callParent(arguments);
    this.on('afterrender', function () {
      this.parentView = this.up('navigation_indicators_panel');
    }, this);
  }
});