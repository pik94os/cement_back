Ext.define('Cement.view.navigation.indicators.Total', {
    extend: 'Cement.view.basic.Complex',
    alias: 'widget.navigation_indicators_total',
    border: 0,
    enableDetail: false,
    autoLoadStore: false,
    priceId: null,
    bodyPadding: 0,
    padding: 0,
    header: false,
    title: null,
    rowsCount: 3,
    title: 'Индикаторы',
    tabTitle: 'Индикаторы',
    cls: 'no-side-borders simple-form',
    topXType: 'navigation_indicators_panel',
    topDetailType: 'Cement.view.navigation.indicators.View',
    bottomTitle: 'Связанные документы',
    bottomTabs: [{
        title: 'Связанные документы',
        xtype: 'accounting_documents_bottom_incoming_connected_documents_incoming',
        detailType: 'Cement.view.goods.auto.view.ConnectedDocument',
        constantFilterParam: 'p_base_doc',
        childrenConstantFilterParam: 'p_base_doc',
        border: 0
    }],
    subBottomTitle: 'Справочник',
    subBottomTabs: [{
        title: 'Маршрут',
        shownTitle: 'Маршрут',
        xtype: 'accounting_documents_bottom_incoming_routes',
        detailType: 'Cement.view.products.price.RouteView',
        constantFilterParam: 'p_navigation_map_total'
    }],

    initComponent: function () {
        this.callParent(arguments);
        this.down('navigation_indicators_panel').on('showDetails', this.showDetails, this);
    },

    showDetails: function (c, id) {
        Ext.each(this.bottomTabs, function (controlObj) {
            var store = Ext.getStore(this.down(controlObj.xtype).gridStore);
            store.clearFilter(true);
            store.filter([
                { property: controlObj.constantFilterParam, value: id }
            ]);
        }, this);
        Ext.each(this.subBottomTabs, function (controlObj) {
            var store = Ext.getStore(this.down(controlObj.xtype).gridStore);
            store.clearFilter(true);
            store.filter([
                { property: controlObj.constantFilterParam, value: id }
            ]);
        }, this);
    }
});