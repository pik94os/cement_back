function getScrollbarWidth() {
    var outer = document.createElement("div");
    outer.style.visibility = "hidden";
    outer.style.width = "100px";
    outer.style.msOverflowStyle = "scrollbar"; // needed for WinJS apps

    document.body.appendChild(outer);

    var widthNoScroll = outer.offsetWidth;
    // force scrollbars
    outer.style.overflow = "scroll";

    // add innerdiv
    var inner = document.createElement("div");
    inner.style.width = "100%";
    outer.appendChild(inner);        

    var widthWithScroll = inner.offsetWidth;

    // remove divs
    outer.parentNode.removeChild(outer);

    return widthNoScroll - widthWithScroll;
}

Ext.define('Cement.view.navigation.indicators.Panel', {
  extend: 'Ext.panel.Panel',
  alias: 'widget.navigation_indicators_panel',
  title: 'Индикаторы',
  rangeKind: 'week',
  bodyCls: 'indicators-total-control',

  initComponent: function () {
    this.store = Ext.getStore('navigation.Indicators');
    var indicatorsStore = Ext.getStore('navigation.indicators.Kinds'),
      indicatorsMenu = [],
      me = this;
    indicatorsStore.each(function (item) {
      var menuObj = {
        text: item.get('p_name'),
        handler: me.indFilter,
        action: 'indicator_' + item.get('id'),
        checked: true
      };
      if (item.get('p_has_detailed_version')) {
        menuObj.menu = [{
          group: 'indicator_group_' + item.get('id'),
          checked: true,
          role: 'detailed',
          handler: me.indFilter,
          text: 'С детализацией'
        }, {
          group: 'indicator_group_' + item.get('id'),
          checked: false,
          handler: me.indFilter,
          text: 'Без детализации'
        }];
      }
      indicatorsMenu.push(menuObj);
    });

    var panel = {
      layout: {
        type: 'hbox',
        pack: 'start'
      },
      border: 0,
      items: [{
        xtype: 'navigation_indicators_device_list',
        store: this.store,
        width: 200
      }, {
        xtype: 'navigation_indicators_bar_panel',
        store: this.store,
        flex: 1
      }]
    };
    Ext.apply(this, {
      layout: {
        type: 'vbox',
        align : 'stretch',
        pack  : 'start'
      },
      tbar: {
        items: [{
            xtype: 'button',
            role: 'filter',
            iconCls: 'icon-page-star',
            text: 'Выбор индикатора',
            menu: {
              role: 'indicator-select',
              items: indicatorsMenu
            }
        }, {
          xtype: 'button',
          iconCls: 'icon-page-text',
          text: 'Условные обозначения',
          action: 'show_legend',
          handler: this.showLegend
        }, '->', {
            handler: this.onPrevClick,
            scope: this,
            iconCls: 'x-tbar-page-prev'
        }, {
          text: 'Час',
          handler: this.onHourClick,
          scope: this,
          toggleGroup: 'tb-views'
        }, {
          text: 'День',
          handler: this.onDayClick,
          scope: this,
          toggleGroup: 'tb-views'
        }, {
          text: 'Неделя',
          pressed: true,
          handler: this.onWeekClick,
          scope: this,
          toggleGroup: 'tb-views'
        }, {
          text: 'Месяц',
          handler: this.onMonthClick,
          scope: this,
          toggleGroup: 'tb-views'
        }, {
            handler: this.onNextClick,
            scope: this,
            iconCls: 'x-tbar-page-next'
        }, '->']
      },
      items: [
      {
        xtype: "navigation_indicators_timeline",
        height: 45,
        store: this.store,
        margin: '0 ' + getScrollbarWidth() + ' 0 200'
      }, {
        xtype: 'panel',
        autoScroll: true,
        overflowY: 'auto',
        border: 0,
        flex: 1,
        layout: 'anchor',
        anchor: '-10, 0',
        items: [ panel ]
      }]
    });
    this.startDate = new Date();
    this.startDate.setHours(0);
    this.startDate.setMinutes(0);
    this.startDate.setSeconds(0);
    this.setFactStartEnd();
    this.callParent(arguments);
    this.on('afterrender', function () {
      this.reload();
    }, this);
    this.down('navigation_indicators_bar_panel').on('indicatorSelect', function (record, indId, valId) {
      if (valId) {
        this.fireEvent('showDetails', 'Cement.view.navigation.indicators.View', valId);
      }
    }, this);
  },

  indFilter: function (btn) {
    var menu = btn.up('menu[role=indicator-select]'),
      visibleIndicators = [],
      panel = menu.up('navigation_indicators_panel');
    menu.items.each(function (item) {
      if (item.checked) {
        if (item.menu) {
          var detailItem = item.menu.down('menuitem[role=detailed]');
          if (detailItem.checked) {
            visibleIndicators.push(item.action + '_detail');
          }
          else {
            visibleIndicators.push(item.action + '_simple');
          }
        }
        else {
          visibleIndicators.push(item.action);
        }
      }
    });
    panel.visibleIndicators = visibleIndicators;
    panel.reload();
  },

  isIndicatorVisible: function (code) {
    if (!this.visibleIndicators) {
      return true;
    }
    var result = false;
    Ext.each(this.visibleIndicators, function (ind) {
      if (ind) {
        if (ind.indexOf(code) > -1) {
          result = true;
        }
      }
    });
    return result;
    // return (this.visibleIndicators.indexOf(code) >= 0);
  },

  reload: function () {
    this.store.clearFilter(true);
    this.store.filter([
      { property: 'p_indicators', value: Ext.JSON.encode(this.visibleIndicators) },
      { property: 'p_date_start', value: Ext.Date.format(this.startDate, 'd.m.Y') },
      { property: 'p_kind', value: this.rangeKind }
    ]);
  },

  getEndDate: function () {
    if (this.rangeKind == 'day') {
      return Ext.Date.add(this.startDate, Ext.Date.DAY, 1);
    }
    if (this.rangeKind == 'week') {
      return Ext.Date.add(this.startDate, Ext.Date.DAY, 7);
    }
    return Ext.Date.add(this.startDate, Ext.Date.MONTH, 1);
  },

  getLengthInterval: function () {
    return (this.endDateFact - this.startDateFact);
    // return Ext.Date.between(this.getEndDate(), this.startDate);
  },

  setFactStartEnd: function () {
    if (this.rangeKind == 'hour') {
      this.startDateFact = Ext.Date.subtract(this.startDate, Ext.Date.MINUTE, 15);
      this.endDateFact = Ext.Date.add(this.startDateFact, Ext.Date.MINUTE, 75);
    }
    else {
      this.startDate.setHours(0);
    }
    if (this.rangeKind == 'day') {
      this.startDateFact = Ext.Date.subtract(this.startDate, Ext.Date.HOUR, 6);
      this.endDateFact = Ext.Date.add(this.startDateFact, Ext.Date.HOUR, 36);
    }
    if (this.rangeKind == 'week') {
      var first = this.startDate.getDate() - this.startDate.getDay() + 1,
        first_dt = Ext.Date.add(this.startDate, Ext.Date.DAY, - this.startDate.getDay() + 1);
      this.startDateFact = Ext.Date.subtract(first_dt, Ext.Date.HOUR, 24);
      this.endDateFact = Ext.Date.add(this.startDateFact, Ext.Date.DAY, 9);
    }
    if (this.rangeKind == 'month') {
      this.startDateFact = Ext.Date.subtract(Ext.Date.getFirstDateOfMonth(this.startDate), Ext.Date.DAY, 4);
      this.endDateFact = Ext.Date.add(this.startDateFact, Ext.Date.DAY, 38);
    }
  },

  onPrevClick: function () {
    if (this.rangeKind == 'hour') {
      this.startDate = Ext.Date.subtract(this.startDate, Ext.Date.HOUR, 1);
    }
    if (this.rangeKind == 'day') {
      this.startDate = Ext.Date.subtract(this.startDate, Ext.Date.DAY, 1);
    }
    if (this.rangeKind == 'week') {
      this.startDate = Ext.Date.subtract(this.startDate, Ext.Date.DAY, 7);
    }
    if (this.rangeKind == 'month') {
      this.startDate = Ext.Date.subtract(this.startDate, Ext.Date.MONTH, 1);
    }
    this.setFactStartEnd();
    this.reload();
  },

  onNextClick: function () {
    if (this.rangeKind == 'hour') {
      this.startDate = Ext.Date.add(this.startDate, Ext.Date.HOUR, 1);
    }
    if (this.rangeKind == 'day') {
      this.startDate = Ext.Date.add(this.startDate, Ext.Date.DAY, 1);
    }
    if (this.rangeKind == 'week') {
      this.startDate = Ext.Date.add(this.startDate, Ext.Date.DAY, 7);
    }
    if (this.rangeKind == 'month') {
      this.startDate = Ext.Date.getFirstDateOfMonth(Ext.Date.add(this.startDate, Ext.Date.MONTH, 1));
    }
    this.setFactStartEnd();
    this.reload();
  },

  onHourClick: function () {
    this.rangeKind = 'hour';
    this.setFactStartEnd();
    this.reload();
  },

  onDayClick: function () {
    this.rangeKind = 'day';
    this.setFactStartEnd();
    this.reload();
  },

  onWeekClick: function () {
    this.rangeKind = 'week';
    this.setFactStartEnd();
    this.reload();
  },

  onMonthClick: function () {
    this.rangeKind = 'month';
    this.setFactStartEnd();
    this.reload();
  },

  setStartDate: function (dt) {
    this.startDate = dt;
    this.reload();
  },

  showLegend: function () {
    Ext.create('Cement.view.navigation.indicators.LegendWindow').show();
  }
});