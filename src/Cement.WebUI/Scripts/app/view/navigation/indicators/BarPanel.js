Ext.define('Cement.view.navigation.indicators.BarPanel', {
  extend: 'Ext.view.View',
  alias: 'widget.navigation_indicators_bar_panel',
  cls: 'navigation-indicators-grid',
  itemSelector: '.x-item',
  overItemCls: 'x-item-over',
  emptyText: 'Нет данных',
  multiSelect: true,

  processItemEvent: function(record, item, index, e) {
    if (Ext.fly(item).hasCls('x-item-child')) {
      if (e.type == "mousedown" && e.button == 0) {
        var indId = item.getAttribute('data-indicator-id'),
          valId = item.getAttribute('data-value-id');
        if (this._selectedNode) {
          Ext.fly(this._selectedNode).removeCls(this.selectedItemCls);
        }
        this._deselectedNode = this._selectedNode;
        this._selectedNode = item;
        Ext.fly(this._selectedNode).addCls(this.selectedItemCls);
        this.fireEvent('indicatorSelect', record, indId, valId);
      }
    }
  },

  updateIndexes : function(startIndex, endIndex) {
    var ns = this.all.elements,
        records = this.store.getRange(),
        i, j;
    startIndex = startIndex || 0;
    endIndex = endIndex || ((endIndex === 0) ? 0 : (ns.length - 1));
    for(i = startIndex, j = startIndex - 1; i <= endIndex; i++){
        if (!Ext.fly(ns[i]).is('.x-item-child')) {
            j++;
        }

        ns[i].viewIndex = i;

        ns[i].viewRecordId = records[j].internalId;
        if (!ns[i].boundView) {
            ns[i].boundView = this.id;
        }
    }
  },

  initComponent: function () {
    var 
      me = this,
      imageTpl = new Ext.XTemplate(
        '<div>',
          '<tpl for=".">',
            '<div class="indicator-value x-item indicator-{#}" style="height: {[ this.getHeight(values) ]}px;">',
              '<div class="indicator-value-wrapper">',
                '<tpl for="p_indicators">',
                  '<div class="row {[ this.getRowCls(values) ]}">',
                    '<tpl for="p_values">',
                      '<tpl if="parent.kind == \'stripes\'">',
                        '<div class="x-item x-item-child value" data-qtip="{p_tooltip}" data-indicator-id="{parent.id}" data-value-id="{id}" style="{[ this.getStyleStr(values) ]}; background: {p_color};"></div>',
                      '</tpl>',
                      '<tpl if="parent.kind == \'markers\'">',
                        '<div class="x-item x-item-child value {p_color}" data-qtip="{p_tooltip}" data-indicator-id="{parent.id}" data-value-id="{id}" style="left: {[ this.getPosition(values) ]};"></div>',
                      '</tpl>',
                    '</tpl>',
                  '</div>',
                '</tpl>',
              '</div>',
            '</div>',
          '</tpl>',
        '</div>',
        {
          getRowCls: function (val) {
            var result = 'row-stripes';
            if (val.kind == 'markers') {
              result = 'row-markers';
            }
            return result;
          },

          getPosition: function (val) {
            var d = Ext.Date.parse(val.p_date_start, "d.m.Y H:i"),
              len = me.parentView.getLengthInterval(),
              delta = d - me.parentView.startDateFact;
            return (delta / len * 100) + '%';
          },

          getStyleStr: function (val) {
            var d = Ext.Date.parse(val.p_date_start, "d.m.Y H:i"),
              d2 = Ext.Date.parse(val.p_date_end, "d.m.Y H:i"),
              len = me.parentView.getLengthInterval(),
              delta = d - me.parentView.startDateFact,
              delta2 = d2 - d,
              width = (delta2 / len * 100) + '%',
              from = (delta / len * 100) + '%';
            return 'left: ' + from + '; width: ' + width;
          },

          getHeight: function (val) {
            result = 0;
            Ext.each(val, function (item) {
              if (me.parentView.isIndicatorVisible(item.p_code)) {
                if (item.kind == 'markers') {
                  result += 16;
                }
                else {
                  result += 7;
                }
              }
            });
            if (result > 0) {
              result += 1;
              if (result < 47) {
                result = 47;
              }
              // result += 'px';
            }
            else {
              result = 'auto'
            }
            return result;
          }
        }
    );
    Ext.apply(this, {
      tpl: imageTpl
    });
    this.callParent(arguments);
    this.on('afterrender', function () {
      this.parentView = this.up('navigation_indicators_panel');
    }, this);
  }
});