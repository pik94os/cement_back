Ext.define('Cement.view.navigation.indicators.LegendWindow', {
  extend: 'Ext.window.Window',
  width: 480,
  height: 350,
  layout: 'fit',
  resizable: false,

  bbar: [{
    iconCls: 'icon-cancel',
    action: 'close',
    text: 'Закрыть'
  }],

  items: [
    {
      xtype: 'panel',
      bodyPadding: 15,
      border: 0,
      html:
        '<div class="indicator-legend-column">' +
          '<b class="legend-head">Индикатор движения:</b>' +
          '<div class="legend-row">' +
            '<span class="color-label color-1"></span><span class="text">0</span>' +
          '</div>' +
          '<div class="legend-row">' +
            '<span class="color-label color-2"></span><span class="text">>20 км/ч</span>' +
          '</div>' +
          '<div class="legend-row">' +
            '<span class="color-label color-3"></span><span class="text">>40 км/ч</span>' +
          '</div>' + 
          '<div class="legend-row">' +
            '<span class="color-label color-4"></span><span class="text">>60 км/ч</span>' +
          '</div>' +
          '<div class="legend-row">' +
            '<span class="color-label color-5"></span><span class="text">>80 км/ч</span>' +
          '</div>' +
          '<div class="legend-row">' +
            '<span class="color-label color-6"></span><span class="text">>100 км/ч</span>' +
          '</div>' +
          '<div class="legend-row">' +
            '<span class="color-label color-7"></span><span class="text">>120 км/ч</span>' +
          '</div>' +
          '<div class="legend-row">' +
            '<span class="color-label color-8"></span><span class="text">>140 км/ч</span>' +
          '</div>' +
          '<div class="legend-row">' +
            '<span class="color-label color-9"></span><span class="text">>160 км/ч</span>' +
          '</div>' +
          '<div class="legend-row">' +
            '<span class="color-label color-10"></span><span class="text">>180 км/ч</span>' +
          '</div>' + 
        '</div>' +
        '<div class="indicator-legend-column">' +
          '<b class="legend-head">Индикатор работы:</b>' +
          '<div class="legend-row">' +
            '<span class="color-label color-11"></span><span class="text">пробег с заявкой</span>' +
          '</div>' + 
          '<div class="legend-row">' +
            '<span class="color-label color-12"></span><span class="text">простой</span>' +
          '</div>' + 
          '<div class="legend-row">' +
            '<span class="color-label color-13"></span><span class="text">ремонт</span>' +
          '</div>' +
          '<b class="legend-head">Индикатор событий:</b>' +
          '<div class="legend-row">' +
            '<span class="color-label color-14"></span><span class="text">обычный уровень</span>' +
          '</div>' + 
          '<div class="legend-row">' +
            '<span class="color-label color-15"></span><span class="text">средний уровень</span>' +
          '</div>' + 
          '<div class="legend-row">' +
            '<span class="color-label color-16"></span><span class="text">высокий уровень</span>' +
          '</div>' +
        '</div>'
    }
  ],

  initComponent: function () {
    this.callParent(arguments);
    this.down('button[action=close]').on('click', function () {
      this.close();
    }, this);
  }
});