Ext.define('Cement.view.navigation.map.View', {
  extend: 'Cement.view.basic.Form',
  alias: 'widget.navigation_map_view',
  fieldsDisabled: true,
  buttons: null,
  border: 0,
  tbar: Cement.Config.modelViewButtons,
  layout: 'autocontainer',
  bbar: null,
  noUserFieldsetsControl: false,
  bodyCls: 'model-form',

  initComponent: function ()  {
    Ext.apply(this, {
      items: [{
        xtype: 'panel',
        border: 0,
        layout: 'anchor',
        defaultType: 'textfield',
        anchor: '100%',
        bodyPadding: '10 10 10 5',
        autoScroll: true,
        defaults: {
          anchor: '100%',
          labelWidth: 110
        },
        items: [{
          xtype: 'textfield',
          name: 'p_name',
          fieldLabel: 'Наименование',
          disabled: true
        }, {
          xtype: 'textfield',
          name: 'p_date',
          fieldLabel: 'Дата',
          disabled: true
        }, {
          xtype: 'textarea',
          name: 'p_theme',
          fieldLabel: 'Тема',
          disabled: true,
          height: 150
        }, {
          xtype: 'textarea',
          name: 'p_content',
          fieldLabel: 'Содержание',
          disabled: true,
          height: 300
        }]
      }]
    });
    this.callParent(arguments);
  }
});