Ext.define('Cement.view.navigation.map.MapPanel', {
    extend: 'Ext.panel.Panel',
    alias: 'widget.navigation_map_map_panel',

    tbar: [
        '->',
        {
            xtype: 'button',
            action: 'reload',
            iconCls: 'icon-reload',
            text: 'Обновить'
        }
    ],

    initComponent: function () {
        this.callParent(arguments);
        this.on('afterlayout', this.initMap, this);
        this.down('button[action=reload]').on('click', this.loadStore, this);
    },

    initMap: function () {
        if (this.mapInit) return;
        this.mapInit = true;
        var me = this,
            map = new OpenLayers.Map({
            div: this.body.dom.id,
            projection: new OpenLayers.Projection("EPSG:900913")
        }),
        layer = new OpenLayers.Layer.OSM( "Simple OSM Map"),
        layerGoogle = new OpenLayers.Layer.Google(
            "Google Streets", { numZoomLevels: 20 }
            ),
        layerGoogle2 = new OpenLayers.Layer.Google(
            "Google Hybrid", {type: google.maps.MapTypeId.HYBRID, numZoomLevels: 20 }
            ),
        switcher = new OpenLayers.Control.LayerSwitcher();
        map.addLayers([layer, layerGoogle, layerGoogle2]);
        map.setCenter(
            new OpenLayers.LonLat(45.184792, 54.180925).transform(
                new OpenLayers.Projection("EPSG:4326"),
                map.getProjectionObject()
                ), 12
            );
        map.addControl(switcher);
        map.extControl = this;
        this.map = map;

        map.events.register('moveend', map, this.updateStore);
        map.events.register('zoomend', map, this.updateStore);

        this.store = Ext.getStore('navigation.MapObjects');
        this.store.on('load', this.setMarkers, this);
        this.loadStore();

        setInterval(function () {
            me.loadStore();
        }, 10000);
    },

    updateStore: function (ev) {
        ev.object.extControl.loadStore();
    },

    loadStore: function () {
        var bounds = this.map.getExtent(),
            topLeft = this.getPoint(bounds.top, bounds.left, true),
            bottomRight = this.getPoint(bounds.bottom, bounds.right, true);
        this.store.clearFilter(true);
        this.store.filter([
            { property: 'p_left', value: topLeft.lon },
            { property: 'p_bottom', value: bottomRight.lat },
            { property: 'p_right', value: bottomRight.lon },
            { property: 'p_top', value: topLeft.lat },
            { property: 'p_zoom', value: this.map.getZoom() }
        ]);
    },

    setMarkers: function () {
        if (this.popup) {
            this.map.removePopup(this.popup);
            // this.popup.destroy();
            this.popup = null;
        }
        this.clearMap();
        var me = this,
        map = this.map,
        markers = new OpenLayers.Layer.Markers( "Markers" ),
        vectorLayer = new OpenLayers.Layer.Vector("Simple Geometry", {
            styleMap: new OpenLayers.StyleMap({'default':{
                strokeColor: "#00FF00",
                strokeOpacity: 1,
                strokeWidth: 3,
                fillColor: "#FF5500",
                fillOpacity: 0.5,
                pointRadius: 6,
                pointerEvents: "visiblePainted",
                label : "${text}",
                
                fontColor: "${favColor}",
                fontSize: "12px",
                fontFamily: "Courier New, monospace",
                fontWeight: "bold",
                labelAlign: "${align}",
                labelXOffset: "${xOffset}",
                labelYOffset: "${yOffset}",
                labelOutlineColor: "white",
                labelOutlineWidth: 3
            }})
        });
        map.addLayer(vectorLayer);
        map.addLayer(markers);

        this.mapLayers = [markers, vectorLayer];
        this.store.each(function (item) {
            var icon, offset, size;
            if (item.get('p_is_cluster')) {
                size = new OpenLayers.Size(21,25);
                offset = new OpenLayers.Pixel(-(size.w / 2), -size.h);
                icon = new OpenLayers.Icon('http://www.openlayers.org/dev/img/marker.png', size, offset);
            }
            else {
                if (item.get('p_is_truck')) {
                    size = new OpenLayers.Size(32, 20);
                    offset = new OpenLayers.Pixel(-(size.w/2), -size.h);
                    icon = new OpenLayers.Icon(Cement.Config.url.navigation.map.car_marker, size, offset);
                }
                else {
                    size = new OpenLayers.Size(31, 32);
                    offset = new OpenLayers.Pixel(-(size.w/2), -size.h);
                    icon = new OpenLayers.Icon(Cement.Config.url.navigation.map.house_marker, size, offset);
                }
            }
            var point = me.getPoint(item.get('p_lat'), item.get('p_lng')),
                marker = new OpenLayers.Marker(point, icon);
            marker.model = item;
            marker.ctl = me;
            markers.addMarker(marker);
            if (item.get('p_is_cluster')) {
                var 
                    pointV = new OpenLayers.Geometry.Point(point.lon, point.lat),
                    pointFeature = new OpenLayers.Feature.Vector(pointV);
                pointFeature.attributes = {
                    text: item.get('p_cluster_count'),
                    favColor: 'red',
                    align: "cm"
                };
                vectorLayer.addFeatures([pointFeature]);
                marker.events.register("click", marker, me.clusterClick);
            }
            else {
                marker.events.register("click", marker, me.markerClick);
                marker.events.register("mouseover", marker, function (e) {
                        var popup = new OpenLayers.Popup("Chicken",
                            new OpenLayers.LonLat(this.lonlat.lon, this.lonlat.lat),
                            new OpenLayers.Size(10, 10),
                            this.model.get('p_name')
                        );
                        popup.autoSize = true;
                        this.popup = popup;
                        me.popup = popup;
                        map.addPopup(popup);
                        OpenLayers.Event.stop(e);
                    }
                );
                marker.events.register('mouseout', marker,
                    function(e) {
                        map.removePopup(this.popup);
                        this.popup.destroy();
                        this.popup = null;
                        OpenLayers.Event.stop(e);
                    }
                );
            }
        });
    },

    clearMap: function () {
        var me = this;
        if (this.mapLayers) {
            Ext.each(this.mapLayers, function (layer) {
                me.map.removeLayer(layer);
            });
        }
    },

    clusterClick: function (ev) {
        ev.object.map.setCenter(ev.object.lonlat, ev.object.map.getZoom() + 2);
    },

    markerClick: function (ev) {
        ev.object.ctl.fireEvent('showDetails', 'Cement.view.navigation.map.View', ev.object.model);
    },

    getPoint: function (lat, lng, reverse) {
        var obj = this.map.getProjectionObject(),
        epsg = new OpenLayers.Projection("EPSG:4326"),
        x = epsg,
        y = obj;
        if (reverse) {
            x = obj;
            y = epsg;
        }
        return new OpenLayers.LonLat(lng, lat).transform(x, y);
    }
});