Ext.define('Cement.view.navigation.map.Total', {
    extend: 'Cement.view.basic.Complex',
    alias: 'widget.navigation_map_total',
    border: 0,
    enableDetail: false,
    autoLoadStore: false,
    priceId: null,
    bodyPadding: 0,
    padding: 0,
    header: false,
    title: null,
    rowsCount: 3,
    title: 'Карта',
    tabTitle: 'Карта',
    cls: 'no-side-borders simple-form',
    topXType: 'navigation_map_map_panel',
    topDetailType: 'Cement.view.navigation.map.View',
    bottomTitle: 'Связанные документы',
    bottomTabs: [{
        title: 'Связанные документы',
        xtype: 'accounting_documents_bottom_incoming_connected_documents_incoming',
        detailType: 'Cement.view.goods.auto.view.ConnectedDocument',
        constantFilterParam: 'p_base_doc',
        childrenConstantFilterParam: 'p_base_doc',
        border: 0
    }],
    subBottomTitle: 'Справочник',
    subBottomTabs: [{
        title: 'Маршрут',
        shownTitle: 'Маршрут',
        constantFilterParam: 'p_base_doc',
        xtype: 'accounting_documents_bottom_incoming_routes',
        detailType: 'Cement.view.products.price.RouteView'
    }],

    initComponent: function () {
        this.callParent(arguments);
        this.down('navigation_map_map_panel').on('showDetails', this.showDetails, this);
    },

    showDetails: function (c, rec) {
        Ext.each(this.bottomTabs, function (controlObj) {
            var store = Ext.getStore(this.down(controlObj.xtype).gridStore);
            store.filter([
                { property: controlObj.constantFilterParam, value: rec.get('p_base_doc') || rec.get('id') }
            ]);
        }, this);
        Ext.each(this.subBottomTabs, function (controlObj) {
            var store = Ext.getStore(this.down(controlObj.xtype).gridStore);
            store.clearFilter(true);
            store.filter([
                { property: controlObj.constantFilterParam, value: rec.get('p_base_doc') || rec.get('id') }
            ]);
        }, this);
    }
});