Ext.define('Cement.view.navigation.geozones.form.Form', {
  extend: 'Cement.view.basic.Form',
  alias: 'widget.navigation_geozones_form_form',
  fieldsDisabled: false,
  url: Cement.Config.url.office_work.documents.new_d.saveUrl,
  method: Cement.Config.url.office_work.documents.new_d.saveMethod,
  isFilter: false,
  layout: 'fit',
  allowBlankFields: true,
  showSaveAndSignButton: true,
  saveButtonDisabled: false,

  getItems: function () {
    return [{
      xtype: 'fieldset',
      border: 0,
      margin: 0,
      padding: '0 40',
      collapsed: false,
      collapsible: false,
      defaults: {
        xtype: 'textfield',
        anchor: '100%',
        labelWidth: 150,
        labelAlign: 'right'
      },
      items: [{
        name: 'p_name',
        fieldLabel: 'Наименование'
      }, {
        xtype: 'hidden',
        name: 'p_geometry'
      }]
    }, {
      xtype: 'fieldset',
      border: 0,
      margin: 0,
      collapsible: false,
      collapsed: false,
      items: [{
        xtype: 'panel',
        height: 400,
        role: 'map_editor'
      }]
    }];
  },

  initComponent: function () {
    this.callParent(arguments);
    this.on('afterlayout', this.initMap, this);
  },

  updateHiddensState: function () {
    this.down('hidden[name=p_geometry]').setValue(Ext.JSON.encode(this.getGeometryObject()));
  },

  initMap: function () {
    var panel_id = this.down('panel[role=map_editor]').body.dom.id;
    document.getElementById(panel_id).innerHTML = '';
    this.curPrimitive = '';
    var me = this,
        map = new OpenLayers.Map({
        div: panel_id,
        projection: new OpenLayers.Projection("EPSG:900913")
    }),
    layer = new OpenLayers.Layer.OSM( "Simple OSM Map"),
    layerGoogle = new OpenLayers.Layer.Google(
        "Google Streets", { numZoomLevels: 20 }
        ),
    layerGoogle2 = new OpenLayers.Layer.Google(
        "Google Hybrid", {type: google.maps.MapTypeId.HYBRID, numZoomLevels: 20 }
        ),
    switcher = new OpenLayers.Control.LayerSwitcher();
    this.map = map;
    map.addLayers([layer, layerGoogle, layerGoogle2]);
    map.setCenter(
        new OpenLayers.LonLat(45.184792, 54.180925).transform(
            new OpenLayers.Projection("EPSG:4326"),
            map.getProjectionObject()
            ), 12
        );
    map.addControl(switcher);
    map.extControl = this;
    this.map = map;

    var key = null;
    var vLayer = new OpenLayers.Layer.Vector("Vector Layer");
    this.vLayer = vLayer;
    var drawControls = {
      polygon: {
        draw: new OpenLayers.Control.DrawFeature(vLayer,
          OpenLayers.Handler.Polygon, {
            displayClass: 'drawPolygon'
          }),
        modify: new OpenLayers.Control.ModifyFeature(vLayer)
      },
      box: {
        draw: new OpenLayers.Control.DrawFeature(vLayer,
          OpenLayers.Handler.RegularPolygon, {
            handlerOptions: {
              sides: 4,
              irregular: true
            },
            displayClass: 'drawBox'
          }
          ),
        modify: new OpenLayers.Control.ModifyFeature(vLayer, {
          mode: OpenLayers.Control.ModifyFeature.RESIZE
        })
      },
      circle: {
        draw: new OpenLayers.Control.DrawFeature(vLayer,
          OpenLayers.Handler.RegularPolygon, {
            handlerOptions: {
              sides: 40
            },
            displayClass: 'drawCircle'
          }),
        modify: new OpenLayers.Control.ModifyFeature(vLayer, {
          mode: OpenLayers.Control.ModifyFeature.RESIZE
        })
      }
    };
    this.drawControls = drawControls;
    var navControl = new OpenLayers.Control.Navigation({
      displayClass: 'resetControl'
    });
    navControl.events.on({
      'activate': function () {
        vLayer.removeAllFeatures();
      }
    });
    me.controlsPanel = new OpenLayers.Control.Panel({
      defaultControl: navControl
    });
    me.controlsPanel.addControls([navControl]);
    me.map.addLayers([vLayer]);

    var dragControl = new OpenLayers.Control.DragFeature(vLayer);
    dragControl.onDrag = function () {
      drawControls[currentControl].modify.deactivate();
    };
    dragControl.onComplete = function () {
      dragControl.deactivate();
      drawControls[currentControl].modify.activate();
      drawControls[currentControl].modify.selectFeature(vLayer.features[0]);
      dragControl.activate();
      me.updateHiddensState();
    };

    me.map.addControl(dragControl);
    // me.map.events.on({
    //   'click': function () {
    //     var key = null;
    //     dragControl.deactivate();
    //     for (key in drawControls) {
    //       if (drawControls.hasOwnProperty(key)) {
    //         drawControls[key].draw.deactivate();
    //         drawControls[key].modify.deactivate();
    //       }
    //     }
    //   }
    // });
    var currentControl = 'polygon';
    this.currentControl = currentControl;
    var featureAddedHandler = function () {
      drawControls[currentControl].draw.deactivate();
      drawControls[currentControl].modify.activate();
      drawControls[currentControl].modify.selectFeature(vLayer.features[0]);
      dragControl.activate();
      me.updateHiddensState();
    };
    var activateControlHandler = function (ev) {
      var key = null;
      currentControl = ev.object.obj;
      me.currentControl = currentControl;
      dragControl.deactivate();
      for (key in drawControls) {
        if (drawControls.hasOwnProperty(key)) {
          if (key !== currentControl) {
            if (drawControls.hasOwnProperty(key)) {
              drawControls[key].draw.deactivate();
              drawControls[key].modify.deactivate();
            }
          }
        }
      }
      vLayer.removeAllFeatures();
    };
    for (key in drawControls) {
      if (drawControls.hasOwnProperty(key)) {
        me.map.addControl(drawControls[key].draw);
        me.map.addControl(drawControls[key].modify);
        drawControls[key].draw.obj = key;
        drawControls[key].draw.events.register('featureadded', vLayer, featureAddedHandler);
        drawControls[key].draw.events.on({
          'activate': activateControlHandler
        });
        drawControls[key].modify.onModification = Ext.bind(me.updateHiddensState, me);
        me.controlsPanel.addControls([
          drawControls[key].draw
        ]);
      }
    }
    me.map.addControl(me.controlsPanel);
    if (this.loadedModel) {
      this.afterRecordLoad(this.loadedModel);
      this.loadedModel = null;
    }
  },

  getPoint: function (lat, lng, reverse) {
    var obj = this.map.getProjectionObject(),
      epsg = new OpenLayers.Projection("EPSG:4326");
    return new OpenLayers.LonLat(lng, lat).transform(
      reverse ? obj : epsg,
      reverse ? epsg : obj
    );
  },

  getGeometryObject: function () {
    if (this.vLayer.features.length == 0) {
      return null;
    }
    var curPrimitive = this.currentControl,
      geometry = this.vLayer.features[0].geometry,
      result = null,
      me = this;
    if (curPrimitive === 'box') {
      var topLeft = me.getPoint(geometry.bounds.top, geometry.bounds.left, true),
        bottomRight = me.getPoint(geometry.bounds.bottom, geometry.bounds.right, true);
      result = {
        p_shape: 'box',
        p_top_left: {
          p_lat: topLeft.lat,
          p_lng: topLeft.lon
        },
        p_bottom_right: {
          p_lat: bottomRight.lat,
          p_lng: bottomRight.lon
        }
      };
    }
    if (curPrimitive === 'polygon') {
      result = {
        p_shape: 'polygon',
        p_coords: []
      };
      var len = geometry.components[0].components.length - 1;
      for (i = 0; i < len; i += 1) {
        var item = me.getPoint(
          geometry.components[0].components[i].y,
          geometry.components[0].components[i].x,
          true
        );
        result.p_coords.push({ p_lat: item.lat, p_lng: item.lon });
      }
    }
    if (curPrimitive === 'circle') {
      var c = me.getPoint(
        geometry.bounds.centerLonLat.lat,
        geometry.bounds.centerLonLat.lon,
        true
      ), 
        g = new OpenLayers.Geometry.Point(
          geometry.bounds.centerLonLat.lon,
          geometry.bounds.centerLonLat.lat
        );
      result = {
        p_shape: 'circle',
        p_center: {
          p_lat: c.lat,
          p_lng: c.lon
        },
        p_radius: g.distanceTo(geometry.components[0].components[0])
      };
    }
    return result;
  },

  afterRecordLoad: function (rec) {
    if (!this.vLayer) {
      this.loadedModel = rec;
      return;
    }
    this.vLayer.removeAllFeatures();
    var geometry = rec.get('p_geometry'),
      feature = null,
      featureStyle = {
        'strokeWidth': 2,
        'strokeColor': '#ee9900',
        'fillColor': '#ee9900',
        'fillOpacity': 0.2
      };
    if (geometry.p_shape == 'box') {
      var point1 = this.getPoint(geometry.p_top_left.p_lat, geometry.p_top_left.p_lng),
        point2 = this.getPoint(geometry.p_bottom_right.p_lat, geometry.p_bottom_right.p_lng),
        poly = new OpenLayers.Bounds(point1.lon, point1.lat, point2.lon, point2.lat).toGeometry(),
      feature = new OpenLayers.Feature.Vector(poly, null, featureStyle);
      this.currentControl = 'box';
    }
    if (geometry.p_shape == 'polygon') {
      var points = [];
      Ext.each(geometry.p_coords, function (coord) {
        var point = this.getPoint(coord.p_lat, coord.p_lng);
        points.push(new OpenLayers.Geometry.Point(point.lon, point.lat));
      }, this);
      var ring = new OpenLayers.Geometry.LinearRing(points),
        polygon = new OpenLayers.Geometry.Polygon([ring]);
      feature = new OpenLayers.Feature.Vector(polygon, null, featureStyle);
      this.currentControl = 'polygon';
    }
    if (geometry.p_shape == 'circle') {
      var point = this.getPoint(geometry.p_center.p_lat, geometry.p_center.p_lng),
        gpoint  = new OpenLayers.Geometry.Point(point.lon, point.lat),
        aCircle = OpenLayers.Geometry.Polygon.createRegularPolygon(gpoint, geometry.p_radius, 40, 0 );
      feature = new OpenLayers.Feature.Vector(aCircle, null, featureStyle);
      this.currentControl = 'circle';
    }
    if (feature) {
      this.vLayer.addFeatures([feature]);
      this.drawControls[this.currentControl].modify.activate();
      this.drawControls[this.currentControl].modify.selectFeature(vLayer.features[0]);
      this.updateHiddensState();
    }
  }
});