Ext.define('Cement.view.navigation.geozones.complex.Current', {
  extend: 'Cement.view.basic.Complex',
  alias: 'widget.navigation_geozones_complex_current',
  border: 0,
  enableDetail: false,
  autoLoadStore: false,
  priceId: null,
  bodyPadding: 0,
  padding: 0,
  header: false,
  title: null,
  rowsCount: 3,
  title: 'Действующие',
  tabTitle: 'Действующие',
  cls: 'no-side-borders',
  topXType: 'navigation_geozones_lists_current',
  topDetailType: 'Cement.view.navigation.geozones.View',
  bottomTitle: 'Связанные документы',
  bottomTabs: [{
    title: 'Связанные документы',
    xtype: 'navigation_geozones_bottom_current_connected_documents_incoming',
    detailType: 'Cement.view.goods.auto.view.ConnectedDocument',
    constantFilterParam: 'p_base_doc',
    childrenConstantFilterParam: 'p_base_doc',
    border: 0
  }],
  subBottomTitle: 'Справочник',
  subBottomTabs: [{
    title: 'Маршрут',
    shownTitle: 'Маршрут',
    xtype: 'navigation_geozones_bottom_current_routes',
    detailType: 'Cement.view.products.price.RouteView'
  }]
});