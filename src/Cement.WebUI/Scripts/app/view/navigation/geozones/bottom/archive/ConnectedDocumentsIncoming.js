Ext.define('Cement.view.navigation.geozones.bottom.archive.ConnectedDocumentsIncoming', {
	extend: 'Cement.view.goods.auto.lists.ConnectedDocuments',
	alias: 'widget.navigation_geozones_bottom_archive_connected_documents_incoming',

	gridStore: 'navigation.geozones.bottom.archive.ConnectedDocumentsIncoming',
	gridStateId: 'stateNavigationGeozonesArchiveConnectedDocumentsIncoming',

    printUrl: Cement.Config.url.navigation.geozones.documents.printGrid,
    helpUrl: Cement.Config.url.navigation.geozones.documents.help
});