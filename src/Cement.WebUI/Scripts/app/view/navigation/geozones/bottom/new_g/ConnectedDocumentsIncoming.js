Ext.define('Cement.view.navigation.geozones.bottom.new_g.ConnectedDocumentsIncoming', {
	extend: 'Cement.view.goods.auto.lists.ConnectedDocuments',
	alias: 'widget.navigation_geozones_bottom_new_connected_documents_incoming',

	gridStore: 'navigation.geozones.bottom.new_g.ConnectedDocumentsIncoming',
	gridStateId: 'stateNavigationGeozonesNewConnectedDocumentsIncoming',

    printUrl: Cement.Config.url.navigation.geozones.documents.printGrid,
    helpUrl: Cement.Config.url.navigation.geozones.documents.help
});