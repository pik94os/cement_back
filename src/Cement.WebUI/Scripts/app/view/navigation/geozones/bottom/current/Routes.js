Ext.define('Cement.view.navigation.geozones.bottom.current.Routes', {
    extend: 'Cement.view.products.price.bottom.NewRoutes',
    alias: 'widget.navigation_geozones_bottom_current_routes',
    gridStore: 'navigation.geozones.bottom.current.Routes',
    gridStateId: 'stateNavigationGeozonesBottomCurrentRoutes'
});