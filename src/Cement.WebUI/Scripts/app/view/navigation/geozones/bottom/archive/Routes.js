Ext.define('Cement.view.navigation.geozones.bottom.archive.Routes', {
    extend: 'Cement.view.products.price.bottom.NewRoutes',
    alias: 'widget.navigation_geozones_bottom_archive_routes',
    gridStore: 'navigation.geozones.bottom.archive.Routes',
    gridStateId: 'stateNavigationGeozonesBottomArchiveRoutes'
});