Ext.define('Cement.view.navigation.geozones.bottom.new_g.Routes', {
    extend: 'Cement.view.products.price.bottom.NewRoutes',
    alias: 'widget.navigation_geozones_bottom_new_routes',
    gridStore: 'navigation.geozones.bottom.new_g.Routes',
    gridStateId: 'stateNavigationGeozonesBottomNewRoutes'
});