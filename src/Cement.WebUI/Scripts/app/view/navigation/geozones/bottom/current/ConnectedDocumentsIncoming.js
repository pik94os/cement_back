Ext.define('Cement.view.navigation.geozones.bottom.current.ConnectedDocumentsIncoming', {
	extend: 'Cement.view.goods.auto.lists.ConnectedDocuments',
	alias: 'widget.navigation_geozones_bottom_current_connected_documents_incoming',

	gridStore: 'navigation.geozones.bottom.current.ConnectedDocumentsIncoming',
	gridStateId: 'stateNavigationGeozonesCurrentConnectedDocumentsIncoming',

    printUrl: Cement.Config.url.navigation.geozones.documents.printGrid,
    helpUrl: Cement.Config.url.navigation.geozones.documents.help
});