Ext.define('Cement.view.navigation.geozones.Total', {
  extend: 'Cement.view.basic.Tabs',
  title: 'Геозоны',

  items: [{
    xtype: 'navigation_geozones_complex_new',
    title: 'Новые'
  }, {
    xtype: 'navigation_geozones_complex_current',
    title: 'Действующие'
  }, {
    xtype: 'navigation_geozones_complex_archive',
    title: 'Архив'
  }],

  initComponent: function () {
    Ext.apply(this, {
      title: 'Геозоны'
    });
    this.callParent(arguments);
  }
});