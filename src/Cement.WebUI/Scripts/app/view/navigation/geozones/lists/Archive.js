Ext.define('Cement.view.navigation.geozones.lists.Archive', {
  extend: 'Cement.view.navigation.geozones.lists.New',
  alias: 'widget.navigation_geozones_lists_archive',

  gridStore: 'navigation.geozones.Archive',
  gridStateId: 'stateNavigationGeozonesArchive',

  printUrl: Cement.Config.url.navigation.geozones.archive.printGrid,
  helpUrl: Cement.Config.url.navigation.geozones.archive.help,
  deleteUrl: Cement.Config.url.navigation.geozones.archive.deleteUrl,
  unArchiveItemUrl: Cement.Config.url.navigation.geozones.archive.unArchiveItem,

  shownTitle: null,

  getActionColumns: function () {
    return {
        xtype: 'rowactions',
        hideable: false,
        resizeable: false,
        locked: true,
        width: 46,
        actions: [
        {
            iconCls: 'icon-unarchive-item',
            qtip: 'Из архива',
            callback: this.unArchiveItem
        }
        ],
        keepSelection: true
    };
}
});