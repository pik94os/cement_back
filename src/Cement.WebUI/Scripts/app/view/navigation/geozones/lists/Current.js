Ext.define('Cement.view.navigation.geozones.lists.Current', {
  extend: 'Cement.view.navigation.geozones.lists.New',
  alias: 'widget.navigation_geozones_lists_current',

  gridStore: 'navigation.geozones.Current',
  gridStateId: 'stateNavigationGeozonesCurrent',

  printUrl: Cement.Config.url.navigation.geozones.current.printGrid,
  helpUrl: Cement.Config.url.navigation.geozones.current.help,
  deleteUrl: Cement.Config.url.navigation.geozones.current.deleteUrl,
  archiveItemUrl: Cement.Config.url.navigation.geozones.current.archiveItem,

  shownTitle: null,

  getActionColumns: function () {
    return {
        xtype: 'rowactions',
        hideable: false,
        resizeable: false,
        locked: true,
        width: 46,
        actions: [
        {
            iconCls: 'icon-delete-item',
            qtip: 'Удалить',
            callback: this.deleteItem
        },
        {
            iconCls: 'icon-archive-item',
            qtip: 'В архив',
            callback: this.archiveItem
        }
        ],
        keepSelection: true
    };
}
});