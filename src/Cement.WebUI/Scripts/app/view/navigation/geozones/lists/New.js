Ext.define('Cement.view.navigation.geozones.lists.New', {
  extend: 'Cement.view.basic.Grid',
  alias: 'widget.navigation_geozones_lists_new',
  autoLoadStore: true,

  bbarText: 'Показаны геозоны {0} - {1} из {2}',
  bbarEmptyMsg: 'Нет геозон',
  bbarUsersText: 'Геозон на странице: ',

  gridStore: 'navigation.geozones.New',
  gridStateId: 'stateNavigationGeozonesNew',

  printUrl: Cement.Config.url.navigation.geozones.new_g.printGrid,
  helpUrl: Cement.Config.url.navigation.geozones.new_g.help,
  deleteUrl: Cement.Config.url.navigation.geozones.new_g.deleteUrl,

  shownTitle: null,

  getActionColumns: function () {
    return {
        xtype: 'rowactions',
        hideable: false,
        resizeable: false,
        locked: true,
        width: 46,
        actions: [
        {
            iconCls: 'icon-edit-item',
            qtip: 'Редактировать',
            callback: this.editItem
        },
        {
            iconCls: 'icon-delete-item',
            qtip: 'Удалить',
            callback: this.deleteItem
        }
        ],
        keepSelection: true
    };
},

getFilterItems: function () {
    return [{
        text: 'Вид',
        kind: 'selector',
        field_name: 'p_kind_display',
        checked: true
    }, {
        text: 'Наименование',
        kind: 'selector',
        field_name: 'p_name',
        checked: true
    }];
},

getGridColumns: function () {
    var result = [
        { text: 'Вид', dataIndex: 'p_kind_display', width: 150 },
        { text: 'Наименование', dataIndex: 'p_name', width: 150, flex: 1 }
    ];
    return this.mergeActions(result);
},

initComponent: function () {
    Ext.apply(this, {
        title: this.shownTitle
    });
    this.callParent(arguments);
}
});