Ext.define('Cement.view.common.EmployeeSelectWindow', {
	extend: 'Cement.view.common.EmployeeWindow',
	xtype: 'widget.common_employee_select_window',
	singleSelection: true,

	sign: function (send) {
		var store = this.down('*[role=sign-dst-grid]').getStore();
		if (store.getCount() > 0) {
			var rec = store.getAt(0);
			this.fireEvent('signitem', rec.get('id'), rec.get('text'), rec.get('position'));
		}
	},

	getBottomColumnsConfig: function () {
		return [
			{
				xtype: 'rowactions',
				hideable: false,
				resizeable: false,
				width: 18,
				actions: [{ 
	                iconCls: 'icon-remove-people', 
	                qtip: 'Удалить', 
	                callback: this.removeEmployee
	            }]
			},
			{ text: 'ФИО', dataIndex: 'text', flex: 1 },
			{ text: 'Должность', dataIndex: 'position', flex: 1 },
			{ text: 'Отдел', dataIndex: 'department', flex: 1 }
		];
	},

	loadData: function (data) {
		if (data) {
			var fstore = Ext.getStore(this.structure_storeId);
			Ext.each(data, function (item) {
				var r = fstore.getById(item);
				if (!this.dstStore) {
					this.setupEvents();
				}
				if (r) {
					// if (!this.dstStore.getById(r.get('id'))) {
						this.dstStore.add({
							id: r.get('id'),
							name: r.get('text'),
							position: r.get('position'),
							department: r.parentNode.get('text'),
							department_id: r.parentNode.get('id'),
							status: item.p_status
						});
					// }
				}
			}, this);
		}
	}

});