Ext.define('Cement.view.common.BasicCreator', {
	extend: 'Ext.panel.Panel',
	alias: 'widget.basiccreator',
	isCreator: true,
	collapseSidebar: true,
	tabTitle: 'Перс. данные',
	cls: 'side-borders',
	layout: 'fit',
	treeSelector: true,

	contentForms: [{
		text: 'Реквизиты счета',
		xtype: 'bankaccountform',
		master: 'Cement.view.bank_account.List'
	}, {
		text: 'Склад',
		xtype: 'new_warehouseform',
		master: 'Cement.view.warehouse.List'
	}, {
		text: 'Сотрудник',
		xtype: 'new_employeeform',
		master: 'Cement.view.employee.List'
	}, {
		text: 'Подпись',
		xtype: 'new_signatureform',
		master: 'Cement.view.signature.List'
	}],
	flatContentForms: [],

	//Change active form depending on comboBox selection
	changeForm: function (combo, newValue) {
		var panel = combo.up('basiccreator'),
			dirRoot = null;
		if (panel) {
			panel.items.each(function (item) {
				item.hide();
			});
			combo.store.getRootNode().removeAll();
			Ext.each(this.flatContentForms, function (cf) {
				if (cf.xtype == newValue) {
					Ext.each(this.contentForms, function (ccf) {
						if (ccf.treeId == cf.topLevel) {
							dirRoot = ccf;
						}
					});
				}
			}, this);
			if (dirRoot) {
				var newRoot = [];
				this.appendItem(newRoot, dirRoot)
				combo.store.getRootNode().appendChild(newRoot);
			}

			Ext.each(this.flatContentForms, function (item) {
				if (item.xtype == newValue) {
					if (item.sidebarCollapsed) {
						this.fireEvent('collapseSidebar');
					}
					else {
						this.fireEvent('expandSidebar');
					}
				}
			}, this);
			combo.setValue(newValue);
			panel.activePanel = panel.down('[kind=' + newValue + ']');
			if (!panel.activePanel) {
				var new_panel = Ext.createByAlias('widget.panel', {
					layout: 'fit',
					hidden: false,
					border: 0,
					kind: newValue,
					items: [{
						xtype: newValue,
						border: 0,
						layout: 'anchor',
						anchor: '100%',
						autoScroll: true,
						prevNextHidden: true,
						bodyPadding: 10
					}]
				});
				panel.items.add(new_panel);
				panel.activePanel = new_panel;
				panel.doLayout();
				panel.setupSaveHandler(new_panel);
			}
			if (panel.activePanel) {
				panel.activePanel.show();
				if (panel.activePanel.onAfterShow) {
					panel.activePanel.onAfterShow();
				}
			}
		}
	},

	//Load Record to actual form
	loadForEdit: function (record, cls) {
		var me = this;
		Ext.each(this.flatContentForms, function (itm) {
			if (itm.master == cls) {
				if (this.down('combobox')) {
					this.down('combobox').setValue(itm.xtype);
				}
				else {
					this.down('treepicker').setValue(itm.xtype);
				}
				if (me.activePanel) {
					me.activePanel.down('basicform').loadRecord(record);
				}
			}
		}, this);
	},

	appendItem: function (root, item) {
		if (!item.leaf) {
			var sroot = {
				text: item.text,
				expanded: item.expanded === undefined ? item.expanded : true,
				tid: item.treeId,
				leaf: false,
				children: []
			}
			Ext.each(item.children, function (it) {
				if (!it.leaf) {
					this.appendItem(sroot, it);
				}
				else {
					sroot.children.push({
						id: it.xtype,
						text: it.text,
						master: it.master,
						leaf: true
					});
				}
			}, this);
			if (root.children) {
				root.children.push(sroot);
			}
			else {
				root.push(sroot);
			}
		}
		else {
			if (root.children) {
				root.children.push({
					id: item.xtype,
					text: item.text,
					master: item.master,
					leaf: true
				});
			}
			else {
				root.push({
					id: item.xtype,
					text: item.text,
					master: item.master,
					leaf: true
				});
			}
		}
	},

	processItem: function (root, item) {
		if (!item.leaf) {
			var sroot = {
				text: item.text,
				expanded: true,
				tid: item.treeId,
				leaf: false,
				children: []
			}
			Ext.each(item.children, function (it) {
				if (!it.leaf) {
					this.processItem(sroot, it);
				}
				else {
					sroot.children.push({
						id: it.xtype,
						text: it.text,
						master: it.master,
						leaf: true
					});
					this.flatContentForms.push(it);
				}
			}, this);
			root.children.push(sroot);
		}
		else {
			root.children.push({
				id: item.xtype,
				text: item.text,
				master: item.master,
				leaf: true
			});
			this.flatContentForms.push(item);
		}
	},

	createTreeCombo: function () {
		var root = {
			text: '',
			id: 'root',
			expanded: true,
			visible: false,
			children: []
		}
		Ext.each(this.contentForms, function (item) {
			this.processItem(root, item);
		}, this);
		var treeStore = Ext.create('Ext.data.TreeStore', {
			root: root,
			folderSort: false
		}),
		me = this;
		return {
			xtype: 'treepicker',
			displayField: 'text',
			canSelectFolders: false,
			editable: false,
			rootVisible: false,
			width: 300,
			store: treeStore,
			hideEmptyLabel: true,
			rootVisible: false,
			listeners: {
				select: function (combo, record, oldValue) {
					me.changeForm(this, record.get('id'));
				}
			}
		};
	},

	initComponent: function () {
		var items = [],
			comboSelector = this.createTreeCombo();
		// Ext.each(this.flatContentForms, function (itm) {
		// 	items.push({
		// 		xtype: 'panel',
		// 		layout: 'fit',
		// 		hidden: true,
		// 		border: 0,
		// 		kind: itm.xtype,
		// 		items: [{
		// 			xtype: itm.xtype,
		// 			border: 0,
		// 			layout: 'anchor',
		// 			anchor: '100%',
		// 			autoScroll: true,
		// 			prevNextHidden: true,
		// 			bodyPadding: 10
		// 		}]
		// 	});
		// }, this);
		Ext.apply(this, {
			title: this.tabTitle,
			tbar: {
				items: [comboSelector, '->', {
          xtype: 'button',
          text: 'Печать',
          iconCls: 'icon-print',
          action: 'print'
        }, {
          xtype: 'button',
          text: 'Помощь',
          iconCls: 'icon-help',
          action: 'show_help'
        }]
			},
			items: items
		});
		this.callParent(arguments);
		this.on('afterrender', this.setupSaveHandlers, this);
		this.addEvents('formsaved');
		this.addEvents('collapseSidebar');
		this.addEvents('expandSidebar');
	},

	submitForm: function (form, cmp, itm, me) {
		form.submit({
        success: function (form, action) {
            Cement.Msg.info('Успешно сохранено');
            var master = null;
            Ext.each(me.flatContentForms, function (cf) {
            	if (itm.xtype == cf.xtype) {
            		master = cf.master;
            	}
            });
            if (master) {
            	me.fireEvent('formsaved', master);
            }
            cmp.down('basicform').getForm().reset();
            if (cmp.down('basicform').afterSubmit) {
            	cmp.down('basicform').afterSubmit();
            }
        },
        failure: function () {
            Cement.Msg.error('Ошибка при сохранении информации');
        }
    });
	},

	submitPrintForm: function (form, url) {
		form.submit({
			url: url,
			clientValidation: false,
			success: function (form, action) {
				window.open(action.result.url);
			},
			failure: function () {
				Cement.Msg.error('Ошибка при формировании предварительного просмотра');
			}
		});
	},

	setupSaveHandler: function (itm) {
		var cmp = itm,
			me = this;
		if (cmp) {
			cmp.down('button[action=form_save]').on('click', function () {
				var form = cmp.down('basicform').getForm();
				if (form.isValid()) {
					if (me.activePanel.down('basicform').beforeSubmit) { //Call before submit if exists
						me.activePanel.down('basicform').beforeSubmit(function () {
							me.submitForm(form, cmp, itm, me);
						});
					}
					else {
						me.submitForm(form, cmp, itm, me);
					}
    	}
			}, this);
			cmp.down('button[action=form_cancel]').on('click', function () {
				cmp.down('basicform').getForm().reset();
			}, this);
		}
	},

	setupSaveHandlers: function () {
		var me = this;
		// Ext.each(this.flatContentForms, function (itm) {
		// 	me.setupSaveHandler(itm);
		// }, this);

		this.down('button[action=show_help]').on('click', function () {
			window.open(
          		Cement.Config.url.creatorHelp
      		);
		}, this);

		this.down('button[action=print]').on('click', function () {
			var form = this.down('basicform').getForm();
			if (this.beforeSubmit) {
				this.beforeSubmit(function () {
					submitPrintForm(form, me.printUrl);
				});
			}
			else {
				this.submitPrintForm(form)
			}
		}, this);
	}
});