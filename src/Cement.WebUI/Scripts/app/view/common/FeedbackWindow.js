Ext.define('Cement.view.common.FeedbackWindow', {
    extend: 'Ext.window.Window',
    alias: 'widget.feedback_window',
    width: 500,
    height: 300,
    title: 'Обратная связь',
    modal: true,
    items: [{
    	xtype: 'form',
    	layout: 'fit',
    	bodyPadding: 10,
    	items: [{
    		xtype: 'textarea',
    		layout: 'anchor',
    		anchor: '100%',
    		height: 220,
    		id: 'feedback_msg_text',
    		name: 'msg',
    		fieldLabel: 'Текст сообщения'
    	}]
    }],
    bbar: [{
    		xtype: 'button',
    		action: 'feedback_close',
    		text: 'Отмена'
    	}, {
    		xtype: 'button',
    		action: 'feedback_save',
    		text: 'Отправить'
    }]
});