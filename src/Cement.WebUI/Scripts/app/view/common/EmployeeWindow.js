Ext.define('Cement.view.common.EmployeeWindow', {
	extend: 'Cement.view.common.SignWindow',
	xtype: 'common_employee_window',
	title: 'Выбор сотрудника',
	hideCommentPanel: true,

	bbar: [{
		text: 'Сохранить',
		action: 'confirm'
	}, {
		text: 'Отмена',
		action: 'cancel'
	}],
	structure_storeId: 'FirmStructure',

	sign: function (send) {
		var store = this.down('*[role=sign-dst-grid]').getStore(),
			data = [],
			me = this,
			status = null,
			status_val = null;
		store.each(function (item) {
			status = me.statusStore.getById(item.get('status'));
			if (status) {
				status_val = status.get('p_name');
			}
			else {
				status_val = '';
			}
			data.push({
				p_employee_id: item.get('id'),
				p_employee: item.get('name'),
				p_status_id: item.get('status'),
				p_status: status_val
			});
		});
		this.fireEvent('signitem', data, send);
	},

	loadData: function (data) {
		if (data) {
			var fstore = Ext.getStore(this.structure_storeId);
			Ext.each(data, function (item) {
				var r = fstore.getById(item.p_employee);
				if (!this.dstStore) {
					this.setupEvents();
				}
				if (r) {
					this.dstStore.add({
						id: r.get('id'),
						name: r.get('text'),
						position: r.get('position'),
						department: r.parentNode.get('text'),
						department_id: r.parentNode.get('id'),
						status: item.p_status
					});
				}
			}, this);
		}
	}
});