Ext.define('Cement.view.common.TotalCreator', {
	extend: 'Ext.panel.Panel',
	alias: 'widget.totalcreator',
	treeSelector: true,
	isCreator: true,
	collapseSidebar: true,
	layout: 'fit',
	tabTitle: 'Создать',
	cls: 'simple-form',

	contentForms: [
		Cement.Creators.corporate,
		Cement.Creators.transport,
		Cement.Creators.products,
		Cement.Creators.clients,
		Cement.Creators.contracts,
		Cement.Creators.goods,
		Cement.Creators.cargo,
		Cement.Creators.office_work,
		Cement.Creators.personal,
		Cement.Creators.accounting,
		Cement.Creators.navigation,
        Cement.Creators.warehouse
	],
	flatContentForms: [],

	//Load Record to actual form
	loadForEdit: function (record, cls, gridCls) {
	    var me = this,
	        className = record.$className,
	        formKind = record.get('p_form_kind'),
	        contractKind = record.get('p_contract_kind');
		
		if (className == 'Cement.model.accounting.Document') {
		    className = className + record.get("p_document_kind");
		}

		var editorForm = Ext.Array.findBy(this.flatContentForms, function (itm) {
		    return itm.model == className
		        && itm.form_kind == formKind
                && itm.contractKind == contractKind;
		});

		if (editorForm) {
		    var treepicker = me.down('treepicker');
		    treepicker.setValue(editorForm.xtype);
		    this.changeForm(treepicker, editorForm.xtype);
		    if (me.activePanel) {
		        me.activePanel.down('basicform').loadRecord(record);
		    }
	    }
	},

	loadForCreate: function (modelName, masterCls, xtype) {
	    var me = this,
	        editorForm = null;

	    if (xtype) {
	        editorForm = Ext.Array.findBy(this.flatContentForms, function (itm) {
	             return itm.xtype == xtype;
	        });
	    } else {
	        editorForm = Ext.Array.findBy(this.flatContentForms, function (itm) {
	            return itm.model == modelName || itm.master == masterCls;
	        });
	    }

	    if (editorForm) {
	        var treepicker = me.down('treepicker');
	        treepicker.setValue(editorForm.xtype);
	        this.changeForm(treepicker, editorForm.xtype);
	        if (me.activePanel) {
	            var basicform = me.activePanel.down('basicform');
	            
	            //basicform.getForm().reset();
	            basicform.reset();
	            if (basicform.onCreate) {
	                basicform.onCreate();
	            }
	        }
	    }
	},

	//Change active form depending on comboBox selection
	changeForm: function (combo, newValue, isUserSelect) {
		var panel = combo.up('totalcreator'),
			dirRoot = null,
			curItem = null;
		if (panel) {
			panel.items.each(function (item) {
				item.hide();
			});
			combo.store.getRootNode().removeAll();
			Ext.each(this.flatContentForms, function (item) {
			    if (item.xtype == newValue) {
					Ext.each(this.contentForms, function (ccf) {
					    if (ccf.treeId == item.topLevel) {
						    dirRoot = ccf;
						    return false;
						}
					});

					curItem = item;
					if (item.sidebarCollapsed) {
					    this.fireEvent('collapseSidebar');
					}
					else {
					    this.fireEvent('expandSidebar');
					}

				    return false;
				}
			}, this);

			if (dirRoot) {
				var newRoot = [];
			    this.appendItem(newRoot, dirRoot);
				combo.store.getRootNode().appendChild(newRoot);
			}

			//Ext.each(this.flatContentForms, function (item) {
			//	if (item.xtype == newValue) {
			//		curItem = item;
			//		if (item.sidebarCollapsed) {
			//			this.fireEvent('collapseSidebar');
			//		}
			//		else {
			//			this.fireEvent('expandSidebar');
			//		}
			//	}
			//}, this);

			combo.setValue(newValue);

			panel.activePanel = panel.down('[kind=' + newValue + ']');
			if (!panel.activePanel) {
                var padding = '20px 80px';
				if (curItem.removePadding) {
					padding = 0;
				}
                
				var newPanel = Ext.createByAlias('widget.panel', {
					layout: 'fit',
					hidden: false,
					border: 0,
					kind: newValue,
					items: [{
					    xtype: newValue,
					    border: 0,
					    layout: curItem.layout || 'anchor',
					    anchor: '100%',
					    autoScroll: true,
					    prevNextHidden: true,
					    bodyPadding: padding,
					    formKind: curItem.form_kind,
					    contractKind: curItem.contractKind
					}]
				});
				panel.items.add(newPanel);
				panel.activePanel = newPanel;
				panel.doLayout();
				panel.setupSaveHandler(newPanel);
			}
			if (panel.activePanel) {
			    panel.activePanel.show();

			    var form = panel.activePanel.down && panel.activePanel.down(newValue);

				if (form && form.onAfterShow) {
				    form.onAfterShow();
				}
			}

			if (isUserSelect && this.activePanel) {
			    var basicform = this.activePanel.down('basicform');
			    if (basicform) {
			        basicform.reset();
			        if (basicform.onCreate) {
			            basicform.onCreate();
			        }
			    }
		    }
		}
	},

	//Load Record to actual form
	// loadForEdit: function (record, cls) {
	// 	var me = this;
	// 	Ext.each(this.flatContentForms, function (itm) {
	// 		if (itm.master == cls) {
	// 			if (this.down('combobox')) {
	// 				this.down('combobox').setValue(itm.xtype);
	// 			}
	// 			else {
	// 				this.down('treepicker').setValue(itm.xtype);
	// 			}
	// 			if (me.activePanel) {
	// 				me.activePanel.down('basicform').loadRecord(record);
	// 			}
	// 		}
	// 	}, this);
	// },

	appendItem: function (root, item) {
		if (!item.leaf) {
		    var sroot = {
		        text: item.text,
		        expanded: item.expanded === undefined ? item.expanded : true,
		        tid: item.treeId,
		        leaf: false,
		        children: []
		    };

			Ext.each(item.children, function (it) {
				if (!it.leaf) {
					this.appendItem(sroot, it);
				}
				else {
					sroot.children.push({
						id: it.xtype,
						text: it.text,
						master: it.master,
						leaf: true
					});
				}
			}, this);

			if (root.children) {
				root.children.push(sroot);
			}
			else {
				root.push(sroot);
			}
		}
		else {
			if (root.children) {
				root.children.push({
					id: item.xtype,
					text: item.text,
					master: item.master,
					leaf: true
				});
			}
			else {
				root.push({
					id: item.xtype,
					text: item.text,
					master: item.master,
					leaf: true
				});
			}
		}
	},

	processItem: function (root, item) {
		if (!item.leaf) {
		    var sroot = {
		        text: item.text,
		        expanded: true,
		        tid: item.treeId,
		        leaf: false,
		        children: []
		    };

			Ext.each(item.children, function (it) {
				if (!it.leaf) {
					this.processItem(sroot, it);
				}
				else {
					sroot.children.push({
						id: it.xtype,
						text: it.text,
						master: it.master,
						leaf: true
					});
					this.flatContentForms.push(it);
				}
			}, this);

			root.children.push(sroot);
		}
		else {
			root.children.push({
				id: item.xtype,
				text: item.text,
				master: item.master,
				leaf: true
			});

			this.flatContentForms.push(item);
		}
	},

	createTreeCombo: function () {
	    var root = {
	        text: '',
	        id: 'root',
	        expanded: true,
	        visible: false,
	        children: []
	    };

		Ext.each(this.contentForms, function (item) {
			this.processItem(root, item);
		}, this);

		var treeStore = Ext.create('Ext.data.TreeStore', {
		    root: root,
			folderSort: false
		}),
		me = this;
		return {
			xtype: 'treepicker',
			displayField: 'text',
			cls: 'creator-picker',
			openCls: 'creator-picker-open',
			canSelectFolders: false,
			editable: false,
			width: 300,
			store: treeStore,
			hideEmptyLabel: true,
			rootVisible: false,
			listeners: {
			    select: function (combo, record) {
			        var id = record.get('id');

			        if (id && id != root.id) {
			            me.changeForm(this, record.get('id'), true);
			        }
			    }
			}
		};
	},

	initComponent: function () {
	    this.flatContentForms = [];

		var items = [],
			comboSelector = this.createTreeCombo();
		Ext.apply(this, {
			title: this.tabTitle,
			tbar: {
				items: [comboSelector, '->', {
		          xtype: 'button',
		          text: 'Печать',
		          iconCls: 'icon-print',
		          action: 'print'
		        }, {
		          xtype: 'button',
		          text: 'Помощь',
		          iconCls: 'icon-help',
		          action: 'show_help'
		        }]
			},
			items: items
		});
		this.callParent(arguments);
		this.on('afterrender', this.setupSaveHandlers, this);
		this.addEvents('formsaved');
		this.addEvents('collapseSidebar');
		this.addEvents('expandSidebar');
	},

	submitForm: function (form, cmp, itm, me, additionalEvent) {

	    //var totalCreator = Ext.ComponentQuery.query('totalcreator')[0];
	    
	    //totalCreator.setLoading("Сохранение");
	    form.submit({
	        waitMsg: "Сохранение...",
	        success: function () {
	            //totalCreator.setLoading(false);
	            Cement.Msg.info('Успешно сохранено');
	            var master = null,
        	        xt = cmp.down('basicform').xtype;

	            Ext.each(me.flatContentForms, function (cf) {
	                if (xt == cf.xtype && master == null) {
	                    master = cf.master;
	                }
	            });

	            if (master) {
	                me.fireEvent('formsaved', master, additionalEvent);
	            }

	            var bf = cmp.down('basicform');

	            if (bf.afterSubmit) {
	                bf.afterSubmit();
	            }

	            if (additionalEvent) {
	                me.close();
	            } else {
	                bf.reset();
	            }

	            
	            //bf.getForm().reset();
	            //if (bf.reset) {
	            //    bf.reset();
	            //}
	        },
	        failure: function (self, result) {
	            //totalCreator.setLoading(false);

	            var message = null;

	            try {
	                message = Ext.decode(result.response.responseText).Message;
	            } catch(e) {
	            }

	            Cement.Msg.error(message || 'Ошибка при сохранении информации');
	        }
	    });
	},

	submitPrintForm: function (form, url) {
		form.submit({
			url: url,
			clientValidation: false,
			success: function (form, action) {
				window.open(action.result.url);
			},
			failure: function () {
				Cement.Msg.error('Ошибка при формировании предварительного просмотра');
			}
		});
	},

	setupSaveHandler: function (itm) {
		var cmp = itm,
			me = this;
		if (cmp) {
			cmp.down('menuitem[action=form_save]').on('click', function () {
				var form = cmp.down('basicform').getForm();
				if (form.isValid()) {
					if (me.activePanel.down('basicform').beforeSubmit) { //Call before submit if exists
						me.activePanel.down('basicform').beforeSubmit(function () {
							me.submitForm(form, cmp, itm, me);
						});
					}
					else {
						me.submitForm(form, cmp, itm, me);
					}
				}
			}, this);
			cmp.down('menuitem[action=form_save_and_return]').on('click', function () {
				var form = cmp.down('basicform').getForm();
				if (form.isValid()) {
					if (me.activePanel.down('basicform').beforeSubmit) { //Call before submit if exists
						me.activePanel.down('basicform').beforeSubmit(function () {
							me.submitForm(form, cmp, itm, me, true);
						});
					}
					else {
						me.submitForm(form, cmp, itm, me, true);
					}
				}
			}, this);
			if (cmp.down('menuitem[action=form_save_and_sign]')) {
				cmp.down('menuitem[action=form_save_and_sign]').on('click', function () {
					var form = cmp.down('basicform').getForm();
					if (form.isValid()) {
						if (me.activePanel.down('basicform').beforeSubmit) { //Call before submit if exists
							me.activePanel.down('basicform').beforeSubmit(function () {
								me.submitForm(form, cmp, itm, me);
							});
						}
						else {
							me.submitForm(form, cmp, itm, me);
						}
					}
				}, this);
			}
			cmp.down('button[action=form_cancel]').on('click', function () {
			    var bf = cmp.down('basicform');
			    bf.reset();
				//bf.getForm().reset();
				//if (bf.reset) {
				//	bf.reset();
				//}
			}, this);
		}
	},

	setupSaveHandlers: function () {
		var me = this;
		// Ext.each(this.flatContentForms, function (itm) {
		// 	me.setupSaveHandler(itm);
		// }, this);

		this.down('button[action=show_help]').on('click', function () {
			window.open(
          		Cement.Config.url.creatorHelp
      		);
		}, this);

		this.down('button[action=print]').on('click', function () {
			var form = this.down('basicform').getForm();
			if (this.beforeSubmit) {
				this.beforeSubmit(function () {
					submitPrintForm(form, me.printUrl);
				});
			}
			else {
				this.submitPrintForm(form)
			}
		}, this);
	}
});