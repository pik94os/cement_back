Ext.define('Cement.view.common.SignWindow', {
	extend: 'Ext.window.Window',
	xtype: 'sign_window',
	title: 'Отправить на согласование',
	layout: {
		type: 'border'
	},
	width: 900,
	height: 400,
	hideCommentPanel: false,
	bbar: [{
		text: 'Согласовать',
		action: 'confirm'
	}, {
		text: 'Согласовать и отправить',
		action: 'confirm_and_send'
	}, {
		text: 'Отмена',
		action: 'cancel'
	}],
	modal: true,
	leftFrameTitle: 'Отделы и сотрудники',
	topGridTitle: 'Отдел',
	bottomGridTitle: 'Выбранные сотрудники',
	structure_storeId: 'FirmStructure',
	loadRightFromTree: true,
	storeFields: ['id', 'text', 'position', 'department', 'department_id', 'inn', 'p_comment'],
	displayField: 'text',
	singleSelection: false,
	cls: 'white-window',
	signersStatusesStoreId: 'SignerStatuses',
	rootVisible: false,
    closeAction: 'hide',

	getCenterMargin: function () {
		if (this.hideCommentPanel) {
			return '10 10 10 0';
		}
		else {
			return '10 0';
		}
	},

	loadData: function (data) {
		if (data) {
			var fstore = Ext.getStore(this.structure_storeId);
			fstore.load({
				callback: function () {
					Ext.each(data, function (item) {
						var r = fstore.getNodeById(item);
						if (!this.dstStore) {
							this.setupEvents();
						}
						if (r) {
							this.dstStore.add(r.copy());
						}
					}, this);
				},
				scope: this
			});
		}
	},

	getTopColumnsConfig: function () {
		return [
			{
				xtype: 'rowactions',
				hideable: false,
				resizeable: false,
				width: 18,
				keepSelection: true,
				actions: [{
	                iconCls: 'icon-add-people',
	                qtip: 'Добавить',
	                callback: this.addEmployee
	            }]
			},
			{ text: 'ФИО', dataIndex: 'text', flex: 1 },
			{ text: 'Должность', dataIndex: 'position', flex: 1 },
			{ text: 'Отдел', dataIndex: 'department', flex: 1 }
		];
	},

	getBottomColumnsConfig: function () {
		return [
			{
				xtype: 'rowactions',
				hideable: false,
				resizeable: false,
				width: 18,
				actions: [{
	                iconCls: 'icon-remove-people',
	                qtip: 'Удалить',
	                callback: this.removeEmployee
	            }]
			},
			{ text: 'ФИО', dataIndex: 'text', flex: 1 },
			{ text: 'Должность', dataIndex: 'position', flex: 1 },
			{ text: 'Отдел', dataIndex: 'department', flex: 1 },
			{
	            text: 'Статус',
	            dataIndex: 'status',
	            flex: 1,
	            renderer: this.getComboRenderer(this.statusStore),
	            field: {
		            xtype: 'combobox',
		            triggerAction: 'all',
		            selectOnTab: true,
		            displayField: 'p_name',
		            valueField: 'id',
		            editable: false,
		            queryMode: 'local',
		            store: this.statusStore,
		            lazyRender: true
		        }
	        }
		];
	},

	getTopGridFilters: function () {
		return [{
            text: 'Выделить все',
            checked: true,
            checkHandler: this.toggleFilterSelection
        }, '-', {
            text: 'ФИО',
            kind: 'selector',
            field_name: 'text',
            checked: true
        }, {
            text: 'Отдел',
            kind: 'selector',
            field_name: 'department',
            checked: false
        }, {
            text: 'Должность',
            kind: 'selector',
            field_name: 'position',
            checked: false
        }];
	},

	getSrcStore: function () {
		return Ext.create('Ext.data.Store', {
			fields: this.storeFields
		});
	},

	getSrcGrid: function () {
		var m_items = [{
		            xtype: 'cleartrigger',
	                emptyText: 'Поиск',
	                name: 'search_input',
	                onTrigger1Click: this.clearFilter,
	                onTrigger2Click: this.applyFilter
		        }, {
		            text: 'Выделить все',
		            checked: false,
		            checkHandler: this.toggleFilterSelection
		        }, '-'];
		Ext.each(this.getTopGridFilters(), function (itm) {
			m_items.push(itm);
		});
		return {
			xtype: 'panel',
			flex: 1,
			border: 1,
			layout: 'fit',
			role: 'sign-src-panel',
			tbar: [{
				xtype: 'label',
				text: this.topGridTitle,
				padding: '2 0 4 10'
			}, '->', {
                xtype: 'button',
                text: 'Поиск',
                menu: {
                    xtype: 'menu',
                    plain: true,
                    items: m_items
                }
            }],
			items: [{
				xtype: 'grid',
				layout: 'fit',
				// id: 'sign-src-grid',
				role: 'sign-src-grid',
				border: 0,
				store: this.getSrcStore(),
				columns: this.getTopColumnsConfig()
			}]
		};
	},

	getComboRenderer: function (store) {
        return function (val) {
            if (!val) return '';
            return store.getById(val).get('p_name');
        };
    },

	getDstGrid: function () {
		this.statusStore = Ext.getStore(this.signersStatusesStoreId);
		this.editor = Ext.create('Ext.grid.plugin.CellEditing', {
            clicksToEdit: 1,
            listeners: {
                edit: function (editor, e) {
                    e.grid.getSelectionModel().deselectAll(true);
                    e.grid.getSelectionModel().select(e.rowIdx);
                }
            }
        });
		return {
			xtype: 'panel',
			flex: 1,
			border: 1,
			layout: 'fit',
			tbar: [{
				xtype: 'label',
				text: this.bottomGridTitle,
				padding: '2 0 4 10'
			}],
			items: [{
				xtype: 'grid',
				// id: 'sign-dst-grid',
				role: 'sign-dst-grid',
				layout: 'fit',
				border: 0,
				store: Ext.create('Ext.data.Store', {
					fields: this.storeFields
				}),
				columns: this.getBottomColumnsConfig(),
				plugins: [
                    this.editor
                ]
			}]
		};
	},

	loadInitialStore: function () {
	    Ext.getStore(this.structure_storeId).load();
	    this.down('grid[role=sign-src-grid]').getStore().removeAll();
	    this.down('grid[role=sign-dst-grid]').getStore().removeAll();
	},
    
	initComponent: function () {
	    this.on('beforeshow', function () {
	        this.loadInitialStore();
	    }, this);

	    var strucStore = Ext.getStore(this.structure_storeId);
		strucStore.on('load', function () {
		    strucStore.getRootNode().expand();
		}, this);

	    Ext.apply(this, {
			items: [{
				title: this.leftFrameTitle,
				region: 'west',
				xtype: 'panel',
				cls: 'flat-title',
				layout: 'fit',
				margin: 10,
				width: 200,
				items: [{
					border: 0,
					xtype: 'treepanel',
					autoScroll: true,
					layout: 'fit',
					cls: 'tree-no-border-bottom tree-no-borders',
					displayField: this.displayField,
					store: strucStore,
					rootVisible: this.rootVisible
				}]
			}, {
				region: 'center',
				xtype: 'panel',
				margin: this.getCenterMargin(),
				border: 0,
				layout: {
					type: 'vbox',
					align : 'stretch'
				},
				items: [this.getSrcGrid(), {
					xtype: 'panel',
					border: 0,
					margin: '5 0',
					defaults: {
						xtype: 'button'
					},
					items: [{
						text: 'Создать всех',
						action: 'create_all',
						iconCls: 'icon-move-bottom',
						hidden: this.singleSelection,
						margin: '0 5 0 0'
					}, {
						text: 'Создать',
						action: 'create',
						iconCls: 'icon-move-down',
						margin: '0 5 0 0'
					}, {
						text: 'Удалить всех',
						action: 'delete_all',
						hidden: this.singleSelection,
						iconCls: 'icon-move-top',
						margin: '0 5 0 0'
					}, {
						text: 'Удалить',
						action: 'delete',
						iconCls: 'icon-move-up'
					}]
				}, this.getDstGrid()]
			}, {
				title: 'Комментарии',
				hidden: this.hideCommentPanel,
				region: 'east',
				cls: 'flat-title',
				xtype: 'panel',
				width: 200,
				margin: 10,
				bodyPadding: 10,
				layout: 'fit',
				items: [{
					xtype: 'textarea',
					name: 'p_comment',
					maxLength: 2000,
					enforceMaxLength: true
				}]
			}]
		});
		
		this.callParent(arguments);
		this.setupEvents();
		// this.on('afterrender', this.setupEvents, this);
		this.addEvents('signitem');
	},

	addToSrcStore: function (item) {
		if (!this.dstStore.getById(item.get('id'))) {
			var r = {};
			Ext.each(this.storeFields, function (i) {
				r[i] = item.get(i);
			});
			r['department_id'] = item.parentNode.get('id');
			r['department'] = item.parentNode.get('text');
			this.srcStore.add(r);
		}
	},

	setupEvents: function () {
		var commentField = this.down('textarea[name=p_comment]');
		this.srcStore = this.down('*[role=sign-src-grid]').getStore();
		this.dstStore = this.down('*[role=sign-dst-grid]').getStore();
		var dstGrid = this.down('*[role=sign-dst-grid]');
		this.curModel = null;
		var me = this;

		if (this.loadRightFromTree) {
			this.down('treepanel').on('itemclick', function (tree, record) {
				var items = [];
				this.currentTreeRecord = record;
				this.srcStore.removeAll();
				if (record.get('leaf')) {
					this.currentDepartment = null;
					items.push(record);
				}
				else {
					this.currentDepartment = record.get('id');
					Ext.each(record.childNodes, function (child) {
						if (child.get('leaf')) {
							items.push(child);
						}
					});
				}
				Ext.each(items, function (item) {
					this.addToSrcStore(item);
				}, this);
			}, this);
		}

		this.down('*[role=sign-dst-grid]').on('beforeselect', function (grid, record) {
			if (me.curModel) {
				me.curModel.set('p_comment', commentField.getValue());
			}
			commentField.setValue(record.get('p_comment'));
			me.curModel = record;
		}, this);

		this.down('button[action=cancel]').on('click', function () {
			this.closeWindow();
		}, this);

		this.down('button[action=confirm]').on('click', function () {
			if (me.curModel) {
				me.curModel.set('p_comment', commentField.getValue());
			}
			this.sign(false);
		}, this);

		if (this.down('button[action=confirm_and_send]')) {
			this.down('button[action=confirm_and_send]').on('click', function () {
				if (me.curModel) {
					me.curModel.set('p_comment', commentField.getValue());
				}
				this.sign(true);
			}, this);
		}

		this.down('button[action=delete_all]').on('click', function () {
			this.dstStore.each(function (record) {
				if (record.get('department_id') == this.currentDepartment) {
					this.srcStore.add(record.copy());
				}
			}, this);
			this.dstStore.removeAll();
		}, this);
		this.down('button[action=delete]').on('click', function () {
		    var grid = this.down('*[role=sign-dst-grid]'),
		        selectionModel = grid.getSelectionModel();

			selectionModel.refresh();
			var sel = selectionModel.getSelection();

			if (sel.length > 0) {
				this.removeE(sel[0]);
			}
		}, this);
		this.down('button[action=create_all]').on('click', function () {
			this.srcStore.each(function (record) {
				this.dstStore.add(record.copy());
			}, this);
			this.srcStore.removeAll();
		}, this);
		this.down('button[action=create]').on('click', function () {
		    var grid = this.down('*[role=sign-src-grid]'),
		        selectionModel = grid.getSelectionModel();

		    selectionModel.refresh();
		    var sel = selectionModel.getSelection();

			if (sel.length > 0) {
				this.addE(sel[0]);
			}
		}, this);
	},

	closeWindow: function () {
		this.close();
	},

	sign: function (send) {
		var store = this.dstStore,
			data = [];
		store.each(function (item) {
			data.push({
				p_employee_id: item.get('id'),
				p_status: item.get('status'),
				p_comment: item.get('p_comment')
			});
		});
		this.fireEvent('signitem', data, send);
	},

	addE: function (record) {
		if (this.singleSelection) {
			if (this.dstStore.getCount() > 0) {
				return;
			}
		}
		this.curModel = record.copy();
		this.dstStore.add(this.curModel);
		this.srcStore.remove(record);
	},

	removeE: function (record) {
		if (record) {
			if (record.get('department_id') == this.currentDepartment) {
				this.srcStore.add(record.copy());
			}
			this.dstStore.remove(record);
		}
	},

	addEmployee: function (grid, record) {
		grid.up('sign_window').addE(record);
	},

	removeEmployee: function (grid, record) {
		grid.up('sign_window').removeE(record);
	},

	toggleFilterSelection: function (item, checked) {
        var toolbar = item.up('toolbar'),
            items = toolbar.query('menu menucheckitem[kind=selector]');
        Ext.each(items, function (it) {
            it.setChecked(item.checked);
        });
    },

    getFilterFields: function () {
        var items = this.down('toolbar').query('menu menucheckitem[kind=selector]'),
            val = this.down('toolbar cleartrigger[name=search_input]').getValue(),
            result = [];
        Ext.each(items, function (item) {
            if (item.checked) {
                result.push({
                    property: item.field_name,
                    value: new RegExp('.*' + val + '.*')
                });
            }
        });
        return result;
    },

    applyFilter: function () {
        var grid = this.up('*[role=sign-src-panel]').down('*[role=sign-src-grid]'),
            filter = grid.up('sign_window').getFilterFields();
        grid.getStore().clearFilter(true);
        grid.getStore().filter(filter);
    },

    clearFilter: function () {
    	this.up('*[role=sign-src-panel]').down('toolbar cleartrigger[name=search_input]').setValue('');
    	this.up('*[role=sign-src-panel]').down('*[role=sign-src-grid]').getStore().clearFilter(false);
    }
});