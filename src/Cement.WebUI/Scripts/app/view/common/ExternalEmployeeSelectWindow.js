Ext.define('Cement.view.common.ExternalEmployeeSelectWindow', {
    extend: 'Cement.view.common.EmployeeSelectWindow',
    xtype: 'widget.external_employee_select_window',
    structure_storeId: 'ExternalFirmStructure'
});