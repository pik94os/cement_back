Ext.define('Cement.view.common.RepeatWindow', {
	extend: 'Ext.window.Window',
	xtype: 'common_repeat_window',
	title: 'Повторение',
	width: 690,
	height: 430,
	layout: 'fit',
	resizable: false,
	modal: true,

	items: [{
		xtype: 'form',
		border: 0,
		layout: 'absolute',
		items: [{
			xtype: 'panel',
			title: 'Повторять',
			x: 10,
			y: 10,
			width: 221,
			height: 200,
			bodyPadding: 10,
			items: [{
				xtype: 'fieldcontainer',
				defaultType: 'radiofield',
				layout: 'vbox',
				items: [{
					boxLabel: 'Каждый день',
					inputValue: Cement.Config.office_work.repeat_kinds.EVERY_DAY,
					name: 'p_repeat_kind'
				}, {
					boxLabel: 'Каждую неделю',
					inputValue: Cement.Config.office_work.repeat_kinds.EVERY_WEEK,
					name: 'p_repeat_kind'
				}, {
					boxLabel: 'Каждый месяц',
					inputValue: Cement.Config.office_work.repeat_kinds.EVERY_MONTH,
					name: 'p_repeat_kind'
				}, {
					boxLabel: 'Каждый год',
					inputValue: Cement.Config.office_work.repeat_kinds.EVERY_YEAR,
					name: 'p_repeat_kind'
				}]
			}]
		}, {
			xtype: 'panel',
			width: 426,
			height: 200,
			x: 241,
			y: 10,
			bodyPadding: '35 0 0 10',
			items: [{
				xtype: 'fieldcontainer',
				defaultType: 'radiofield',
				layout: 'vbox',
				hidden: true,
				role: 'repeat_details',
				itemId: 'repeat_details_' + Cement.Config.office_work.repeat_kinds.EVERY_DAY,
				items: [{
					xtype: 'fieldcontainer',
					layout: 'hbox',
					items: [{
						xtype: 'radiofield',
						name: 'p_repeat_day_value',
						inputValue: Cement.Config.office_work.repeat_day_values.EVERY_N_DAY,
						boxLabel: 'Каждый'
					}, {
						xtype: 'textfield',
						name: 'p_repeat_day_days',
						margin: '0 10'
					}, {
						xtype: 'label',
						padding: '1 0 4 0',
						text: 'день'
					}]
				}, {
					boxLabel: 'Каждый будний день',
					inputValue: Cement.Config.office_work.repeat_day_values.EVERY_WORK_DAY,
					name: 'p_repeat_day_value'
				}]
			}, {
				xtype: 'panel',
				hidden: true,
				layout: 'anchor',
				role: 'repeat_details',
				itemId: 'repeat_details_' + Cement.Config.office_work.repeat_kinds.EVERY_WEEK,
				border: 0,
				items: [{
					xtype: 'fieldcontainer',
					layout: 'hbox',
					items: [{
						xtype: 'radiofield',
						name: 'p_repeat_week_value',
						inputValue: Cement.Config.office_work.repeat_week_values.EVERY_WEEK,
						boxLabel: 'Каждую'
					}, {
						xtype: 'textfield',
						name: 'p_repeat_week_week',
						margin: '0 10'
					}, {
						xtype: 'label',
						padding: '1 0 4 0',
						text: 'неделю'
					}]
				}, {
					xtype: 'panel',
					width: 300,
					layout: 'column',
					border: 0,
					items: [{
						xtype: 'fieldcontainer',
						defaultType: 'checkbox',
						columnWidth: 0.5,
						items: [{
							name: 'p_repeat_week_monday',
							boxLabel: 'Понедельник'
						}, {
							name: 'p_repeat_week_tuesday',
							boxLabel: 'Вторник'
						}, {
							name: 'p_repeat_week_wednesday',
							boxLabel: 'Среда'
						}, {
							name: 'p_repeat_week_thursday',
							boxLabel: 'Четверг'
						}, {
							name: 'p_repeat_week_friday',
							boxLabel: 'Пятница'
						}]
					}, {
						xtype: 'fieldcontainer',
						defaultType: 'checkbox',
						columnWidth: 0.5,
						items: [{
							name: 'p_repeat_week_saturday',
							boxLabel: 'Суббота'
						}, {
							name: 'p_repeat_week_sunday',
							boxLabel: 'Воскресенье'
						}]
					}]
				}]
			}, {
				xtype: 'fieldcontainer',
				defaultType: 'radiofield',
				layout: 'vbox',
				hidden: true,
				role: 'repeat_details',
				itemId: 'repeat_details_' + Cement.Config.office_work.repeat_kinds.EVERY_MONTH,
				items: [{
					xtype: 'fieldcontainer',
					layout: 'hbox',
					items: [{
						xtype: 'radiofield',
						name: 'p_repeat_month_value',
						inputValue: Cement.Config.office_work.repeat_month_values.EVERY_N_DAY,
						boxLabel: 'Повторять'
					}, {
						xtype: 'textfield',
						width: 50,
						name: 'p_repeat_month_days',
						margin: '0 10'
					}, {
						xtype: 'label',
						padding: '1 0 4 0',
						text: 'день месяц'
					}, {
						xtype: 'textfield',
						name: 'p_repeat_month_month',
						width: 50,
						margin: '0 10'
					}, {
						xtype: 'label',
						padding: '1 0 4 0',
						text: 'месяц'
					}]
				}, {
					xtype: 'fieldcontainer',
					layout: 'hbox',
					items: [{
						xtype: 'radiofield',
						boxLabel: 'В',
						inputValue: Cement.Config.office_work.repeat_month_values.EVERY_N_M_DAY,
						name: 'p_repeat_month_value'
					}, {
						xtype: 'combo',
				        store: Ext.data.StoreManager.lookup('office_work_message_repeat_months_days_Store'),
				        editable: false,
				        queryMode: 'local',
				        displayField: 'p_name',
				        valueField: 'id',
				        name: 'p_repeat_month_days2',
				        width: 76,
						margin: '0 10'
					}, {
						xtype: 'combo',
				        store: Ext.data.StoreManager.lookup('office_work_message_repeat_months_day_names_Store'),
				        editable: false,
				        queryMode: 'local',
				        displayField: 'p_name',
				        valueField: 'id',
				        name: 'p_repeat_month_week_days',
				        width: 90,
						margin: '0 10'
					}, {
						xtype: 'label',
						padding: '1 0 4 0',
						text: 'каждый'
					}, {
						xtype: 'textfield',
						width: 76,
						name: 'p_repeat_month_month2',
						margin: '0 10'
					}, {
						xtype: 'label',
						padding: '1 0 4 0',
						text: 'месяц'
					}]
				}]
			}, {
				xtype: 'fieldcontainer',
				defaultType: 'radiofield',
				layout: 'vbox',
				hidden: true,
				role: 'repeat_details',
				itemId: 'repeat_details_' + Cement.Config.office_work.repeat_kinds.EVERY_YEAR,
				items: [{
					xtype: 'fieldcontainer',
					layout: 'hbox',
					items: [{
						xtype: 'radiofield',
						name: 'p_repeat_year_value',
						inputValue: Cement.Config.office_work.repeat_year_values.EVERY_DAY_MONTH,
						boxLabel: 'Повторять'
					}, {
						xtype: 'textfield',
						width: 50,
						name: 'p_repeat_year_days',
						margin: '0 10'
					}, {
						xtype: 'label',
						padding: '1 0 4 0',
						text: 'день каждый'
					}, {
						xtype: 'combo',
				        store: Ext.data.StoreManager.lookup('office_work_message_repeat_year_months_Store'),
				        editable: false,
				        queryMode: 'local',
				        displayField: 'p_name',
				        valueField: 'id',
				        name: 'p_repeat_year_month',
				        width: 90,
						margin: '0 10'
					}, {
						xtype: 'label',
						padding: '1 0 4 0',
						text: 'месяц'
					}]
				}, {
					xtype: 'fieldcontainer',
					layout: 'hbox',
					items: [{
						xtype: 'radiofield',
						boxLabel: 'В',
						inputValue: 2,
						inputValue: Cement.Config.office_work.repeat_year_values.EVERY_DAY_DAY_MONTH,
						name: 'p_repeat_year_value'
					}, {
						xtype: 'combo',
				        store: Ext.data.StoreManager.lookup('office_work_message_repeat_year_days_Store'),
				        editable: false,
				        queryMode: 'local',
				        displayField: 'p_name',
				        valueField: 'id',
				        name: 'p_repeat_year_days2',
				        width: 76,
						margin: '0 10'
					}, {
						xtype: 'combo',
				        store: Ext.data.StoreManager.lookup('office_work_message_repeat_year_day_names_Store'),
				        editable: false,
				        queryMode: 'local',
				        displayField: 'p_name',
				        valueField: 'id',
				        name: 'p_repeat_year_week_days',
				        width: 90,
						margin: '0 10'
					}, {
						xtype: 'label',
						padding: '1 0 4 0',
						text: 'месяц'
					}, {
						xtype: 'combo',
				        store: Ext.data.StoreManager.lookup('office_work_message_repeat_year_months_Store'),
				        editable: false,
				        queryMode: 'local',
				        displayField: 'p_name',
				        valueField: 'id',
				        name: 'p_repeat_year_month2',
				        width: 76,
						margin: '0 10'
					}]
				}]
			}]
		}, {
			xtype: 'panel',
			title: 'Начало',
			bodyPadding: 10,
			x: 10,
			y: 220,
			width: 221,
			height: 140,
			defaults: {
				labelWidth: 45
			},
			items: [{
				xtype: 'datefield',
				name: 'p_repeat_begin_date',
				fieldLabel: 'Дата'
			}, {
				xtype: 'timefield',
				name: 'p_repeat_begin_time',
				format: 'H:i',
				fieldLabel: 'Время'
			}]
		}, {
			xtype: 'panel',
			title: 'Окончание',
			x: 241,
			y: 220,
			width: 426,
			height: 140,
			bodyPadding: 10,
			items: [{
				xtype: 'fieldcontainer',
				defaultType: 'radiofield',
				layout: 'vbox',
				items: [{
					xtype: 'fieldcontainer',
					layout: 'hbox',
					items: [{
						xtype: 'radiofield',
						name: 'p_repeat_end_kind',
						boxLabel: 'Завершить после',
						inputValue: Cement.Config.office_work.repeat_end_kinds.AFTER_N_REPEATS
					}, {
						xtype: 'textfield',
						name: 'p_repeat_end_count',
						margin: '0 10',
						width: 120
					}, {
						xtype: 'label',
						padding: '1 0 4 0',
						text: 'повторений'
					}]
				}, {
					xtype: 'fieldcontainer',
					layout: 'hbox',
					items: [{
						xtype: 'radiofield',
						boxLabel: 'Дата окончания',
						inputValue: 2,
						name: 'p_repeat_end_kind',
						inputValue: Cement.Config.office_work.repeat_end_kinds.DATE
					}, {
						xtype: 'datefield',
						name: 'p_repeat_end_date',
						margin: '0 17',
						width: 120
					}]
				}, {
					boxLabel: 'Нет даты окончания',
					name: 'p_repeat_end_kind',
					inputValue: Cement.Config.office_work.repeat_end_kinds.NO_END
				}]
			}]
		}]
	}],

	bbar: [{
		text: 'Сохранить',
		action: 'save'
	}, {
		text: 'Отмена',
		action: 'cancel'
	}, {
		text: 'Удалить повторение',
		action: 'delete'
	}],

	getStr: function() {
		var values = this.down('form').getForm().getValues(),
			str = '',
			repeat_str = '';
		if (values['p_repeat_kind']) {
			if (values['p_repeat_kind'] == Cement.Config.office_work.repeat_kinds.EVERY_DAY) {
				if (values['p_repeat_day_value']) {
					if (values['p_repeat_day_value'] == Cement.Config.office_work.repeat_day_values.EVERY_N_DAY) {
						repeat_str = Ext.String.format("Каждый {0} день", values['p_repeat_day_days']);
					}
					else {
						repeat_str = 'Каждый будний день';
					}
				}
				str = repeat_str;
			}
			if (values['p_repeat_kind'] == Cement.Config.office_work.repeat_kinds.EVERY_WEEK) {
				var days = [];
				if (values['p_repeat_week_monday']) {
					days.push('Понедельник');
				}
				if (values['p_repeat_week_tuesday']) {
					days.push('Вторник');
				}
				if (values['p_repeat_week_wednesday']) {
					days.push('Среда');
				}
				if (values['p_repeat_week_thursday']) {
					days.push('Четверг');
				}
				if (values['p_repeat_week_friday']) {
					days.push('Пятница');
				}
				if (values['p_repeat_week_saturday']) {
					days.push('Суббота');
				}
				if (values['p_repeat_week_sunday']) {
					days.push('Воскресенье');
				}
				str = Ext.String.format("Каждую {0} неделю в дни: {1}", values['p_repeat_week_week'], days.join(', '));
			}
			if (values['p_repeat_kind'] == Cement.Config.office_work.repeat_kinds.EVERY_MONTH) {
				str = 'Каждый месяц';
				if (values['p_repeat_month_value']) {
					if (values['p_repeat_month_value'] == Cement.Config.office_work.repeat_month_values.EVERY_N_DAY) {
						str = Ext.String.format('Повторять {0} день каждый {1} месяц', values['p_repeat_month_days'], values['p_repeat_month_month']);
					}
					else {
						var v1, v2 = '';
						if (values['p_repeat_month_week_days']) {
							v1 = Ext.getStore('office_work_message_repeat_months_day_names_Store').getById(values['p_repeat_month_week_days']).get('p_name');
						}
						if (values['p_repeat_month_days2']) {
							v2 = Ext.getStore('office_work_message_repeat_months_days_Store').getById(values['p_repeat_month_days2']).get('p_name');
						}
						str = Ext.String.format('В {0} {1} каждый {2} месяц', v2, v1, values['p_repeat_month_month2']);
					}
				}
			}
			if (values['p_repeat_kind'] == Cement.Config.office_work.repeat_kinds.EVERY_YEAR) {
				str = 'Каждый год';
				if (values['p_repeat_year_value'] == Cement.Config.office_work.repeat_year_values.EVERY_DAY_MONTH) {
					var v = '';
					if (values['p_repeat_year_month']) {
						v = Ext.getStore('office_work_message_repeat_year_months_Store').getById(values['p_repeat_year_month']).get('p_name');
					}
					str = Ext.String.format('Повторять {0} день каждый {1} месяц', values['p_repeat_year_days'], v);
				}
				else {
					var v1, v2, v3 = '';
					if (values['p_repeat_year_days2']) {
						v1 = Ext.getStore('office_work_message_repeat_year_days_Store').getById(values['p_repeat_year_days2']).get('p_name');
					}
					if (values['p_repeat_year_week_days']) {
						v2 = Ext.getStore('office_work_message_repeat_year_day_names_Store').getById(values['p_repeat_year_week_days']).get('p_name');
					}
					if (values['p_repeat_year_month2']) {
						v3 = Ext.getStore('office_work_message_repeat_year_months_Store').getById(values['p_repeat_year_month2']).get('p_name');
					}
					str = Ext.String.format('В {0} {1} месяц {2}', v1, v2, v3);
				}
			}
			if (values['p_repeat_begin_date']) {
				str = str + ', Дата начала: ' + values['p_repeat_begin_date'];
			}
			if (values['p_repeat_begin_time']) {
				str = str + ', Время начала: ' + values['p_repeat_begin_time'];
			}
			if (values['p_repeat_end_kind']) {
				if (values['p_repeat_end_kind'] == Cement.Config.office_work.repeat_end_kinds.AFTER_N_REPEATS) {
					str = str + ', Завершить после ' + values['p_repeat_end_count'] + ' повторений';
				}
				if (values['p_repeat_end_kind'] == Cement.Config.office_work.repeat_end_kinds.DATE) {
					str = str + ', Дата окончания: ' + values['p_repeat_end_date'];
				}
				if (values['p_repeat_end_kind'] == Cement.Config.office_work.repeat_end_kinds.NO_END) {
					str = str + ', Нет даты окончания';
				}
			}
		}
		return str;
	},

	getJSON: function() {
		return Ext.JSON.encode(this.down('form').getForm().getValues());
	},

	initComponent: function () {
		this.callParent(arguments);
		this.addEvents('repeatcreated');
		Ext.each(this.query('radiofield[name=p_repeat_kind]'), function (item) {
			item.on('change', function () {
				var values = this.down('form').getForm().getValues();
				if (values['p_repeat_kind']) {
					if (!values['p_repeat_kind'].length) {
						Ext.each(this.query('*[role=repeat_details]'), function (container) {
							container.hide();
						});
						this.down('[itemId=repeat_details_' + values['p_repeat_kind'] + ']').show();
					}
				}
			}, this);
		}, this);
		this.down('button[action=save]').on('click', function () {
			this.fireEvent('repeatcreated', this.getJSON(), this.getStr());
		}, this);
		this.down('button[action=cancel]').on('click', function () {
			this.hide();
		}, this);
		this.down('button[action=delete]').on('click', function () {
			this.down('form').getForm().reset();
			this.fireEvent('repeatcreated', '', '');
		}, this);
		this.on('beforeclose', function () {
			this.hide();
			return false;
		});
	},

	closeWindow: function () {
		this.hide();
	},

	loadData: function (data) {
		if (data) {
			this.down('form').getForm().setValues(data);
		}
	}
});