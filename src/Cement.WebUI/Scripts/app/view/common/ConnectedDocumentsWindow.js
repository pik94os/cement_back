Ext.define('Cement.view.common.ConnectedDocumentsWindow', {
	extend: 'Cement.view.common.SignWindow',
	xtype: 'widget.common_connected_documents_window',
	title: 'Выбор связанных документов',
	hideCommentPanel: true,
	leftFrameTitle: 'Документы',
	topGridTitle: 'Внутренние документы',
	bottomGridTitle: 'Выбранные документы',

	bbar: [{
		text: 'Сохранить',
		action: 'confirm'
	}, {
		text: 'Отмена',
		action: 'cancel'
	}],

	storeFields: ['id', 'p_name' ],
	displayField: 'p_name',

	structure_storeId: 'office_work.auxiliary.ConnectedDocuments',

	getBottomColumnsConfig: function () {
		return [{
			xtype: 'rowactions',
			hideable: false,
			resizeable: false,
			width: 18,
			actions: [{ 
                iconCls: 'icon-remove-document', 
                qtip: 'Удалить', 
                callback: this.removeEmployee
            }]
		},
		{ text: 'Документ', dataIndex: 'p_name', flex: 1 }];
	},

	getTopColumnsConfig: function () {
		return [
			{
				xtype: 'rowactions',
				hideable: false,
				resizeable: false,
				width: 18,
				keepSelection: true,
				actions: [{ 
	                iconCls: 'icon-add-document', 
	                qtip: 'Добавить',
	                callback: this.addEmployee
	            }]
			},
			{ text: 'Документ', dataIndex: 'p_name', flex: 1 }
		];
	},

	sign: function (send) {
		var store = this.down('*[role=sign-dst-grid]').getStore(),
			names = [],
			ids = [];
		store.each(function (item) {
			ids.push(item.get('id'));
			names.push(item.get('p_name'));
		});
		this.fireEvent('documentsselected', ids, names);
	},

	closeWindow: function () {
		this.hide();
	},

	loadData: function (data) {
		if (data) {
			var fstore = Ext.getStore(this.structure_storeId);
			fstore.load({
				callback: function () {
					Ext.each(data, function (item) {
						var r = fstore.getNodeById(item);
						if (!this.dstStore) {
							this.setupEvents();
						}
						if (r) {
							this.dstStore.add({
								id: r.get('id'),
								p_name: r.get('p_name')
							});
						}
					}, this);
				},
				scope: this
			});
		}
	},

	initComponent: function () {
		this.callParent(arguments);
		this.addEvents('documentsselected');
	}
});