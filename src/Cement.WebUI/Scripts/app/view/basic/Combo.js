// Ext.form.field.ComboBox.override( {
//     setValue: function(v) {
//         v = (v && v.toString) ? v.toString() : v;
//         if(!this.store.isLoaded && this.queryMode == 'remote') {
//             this.store.addListener('load', function() {
//                 this.store.isLoaded = true;
//                 this.setValue(v);
//             }, this);
//            this.store.load();
//         } else {
//             this.callOverridden(arguments);
//         }
//     }
// });

Ext.define('Cement.view.basic.Combo', {
  getComboSelText: function (name) {
    var combo = this.down('combo[name=' + name + ']'),
      result = '';
    if (combo) {
      var store = combo.getStore(),
        rec = store.getById(combo.getValue());
      if (rec) {
        result = rec.get('p_name');
      }
    }
    return result;
  },

  getCombo: function (label, name, store, callback, hidden, listConfig, allowBlank, queryMode) {
    var realStore = null;
    if (!this.disabledFields) {
      this.disabledFields = {};
    }
    var disable = this.fieldsDisabled || (this.disabledFields[name] !== undefined),
      allowBlank = allowBlank || this.allowBlankFields,
      queryMode = queryMode || 'local',
      result = {
        xtype: 'combo',
        editable: false,
        queryMode: queryMode,
        displayField: 'p_name',
        forceSelection: true,
        valueField: 'id',
        name: name,
        fieldLabel: label,
        allowBlank: allowBlank,
        disabled: disable
      };
    if (listConfig) {
      result.listConfig = listConfig;
    }
    if (callback) {
      result.listeners = {
        change: callback
      };
    }
    if (disable) {
      result = {
        name: name + '_display',
        fieldLabel: label,
        xtype: 'textfield',
        disabled: disable
      };
    }
    if (hidden) {
      result.hidden = true;
    }
    if (!disable) {
      if (store.load) {
        realStore = store;
      }
      else {
        realStore = Ext.getStore(store);
      }
      result.store = realStore;
    }
    return result;
  },

    getColoredCombo: function (label, name, store) {
    var realStore = null;
    
    var result = {
        xtype: 'combobox',
        anchor: '100%',
        queryMode: 'local',
        listConfig: {
            getInnerTpl: function (displayField) {
                return '<div class="colored-icon-combo colored-{flag}"></div> {' + displayField + '}';
            }
        },
        fieldLabel: label,
        name: name,
        triggerAction: 'all',
        editable: false,
        valueField: 'id',
        displayField: 'p_name'
    };

        if (store.load) {
            realStore = store;
        }
        else {
            realStore = Ext.getStore(store);
        }
        result.store = realStore;
    
    return result;
}
});