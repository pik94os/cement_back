Ext.Msg.msgButtons[0].text = 'OK';
Ext.Msg.msgButtons[1].text = 'Да';
Ext.Msg.msgButtons[2].text = 'Нет';
Ext.Msg.msgButtons[3].text = 'Отмена';

Ext.define('Cement.view.basic.Msg', {
});

Ext.define('Cement.Msg', {
	singleton: true,

/** Translatable fields */
	msgConfirmation: 'Подтверждение',
	msgInformation: 'Информация',
	msgWarning: 'Внимание',
	msgEvent: 'Событие',
	msgError: 'Ошибка',

/**
	* Notify container
	* @private
	*/
	notifyContainer: null,

	opened: new Ext.util.MixedCollection(),

/**
	* Displays a new message box, or reinitializes an existing message box,
	* based on the config options passed in.
	* @param {Object} config Config options of message box @see Ext.Msg
	* Example usage:
	* <pre><code>
O.msg.showDialog({
	title: 'Info',
	msg: 'Please be careful!',
	icon: Ext.window.MessageBox.INFO
});
</code></pre>
	* @return {Ext.window.MessageBox} this
	*/
	dialog: function(config) {
		Ext.Msg.show(Ext.apply({
			buttons: Ext.Msg.OK,
			animEl: 'header'
		}, config));
	},

/**
	* Returns a HTML representation of message box
	* @param {Object} config Config object
	* @return {String} HTML code
	* @private
	*/
	getNotifyBox: function(config) {
		config = Ext.apply({title: '', msg: '', cls: ''}, config);
		return '<div class="msg ' + config.cls + '">' +
			'<h3>' + config.title + '</h3>' +
			'<p>' + config.msg + '</p>' +
		'</div>';
	},

/**
	* Returns container for notify box
	* @return Ext.Element
	* @private
	*/
	getNotifyContainer: function() {
		if (!this.notifyContainer) {
			this.notifyContainer = Ext.core.DomHelper.insertFirst(
				document.body, {id: 'msg-div'}, true);
		}
		return this.notifyContainer;
	},

/**
	* Shows popup notify box
	* @param {Object} config Config object
	*/
	notify: function(config) {
		var me = this;
		var m = Ext.core.DomHelper.append(
			this.getNotifyContainer(),
			this.getNotifyBox(config),
			true
		);
		if (config.msgKey)
			this.opened.add(config.msgKey, m);
		if (config.callback && Ext.isFunction(config.callback))
			m.on('click', function() {
				config.callback.call(config.callback.scope || this);
				m.addCls('display-none');
				if (config.msgKey && me.getOpenedByKey(config.msgKey))
					me.opened.removeAtKey(config.msgKey);
			}, this);
		else
			m.on('click', function() {
				m.addCls('display-none');
				if (config.msgKey && me.getOpenedByKey(config.msgKey))
					me.opened.removeAtKey(config.msgKey);
			});
		m.hide().slideIn('t').ghost('t', {
			delay: (config.delay === 0) ?
				86400000 : (config.delay || 4000), 
			remove: config.remove || true
		});
	},

/**
	* Shows confirmation dialog
	* @param {Object/String} data Configuration object or message string 
	*/
	confirm: function(data) {
		this.show(Ext.apply({
			title: this.msgConfirmation,
			icon: Ext.window.MessageBox.prototype.QUESTION,
			buttons: Ext.Msg.YESNO,
			type: 'dialog'
		}, this.makeConfig(data)));
	},

/**
	* Shows prompt dialog
	* @param {Object/String} data Configuration object or message string 
	*/
	prompt: function(data) {
		// TODO O.msg.prompt()
	},

/**
	* Show message depending on type of dialog
	* @param {Object} config Configuration object
	* @private
	*/
	show: function(config) {
		if (!config) {
			return;
		}
		if (config.type === 'notify') {
			// lets show notify box
			this.notify(config);
		} else {
			// lets show dialog box
			this.dialog(config);
		}
	},

/**
	* Makes config object from string
	* Is input parameter is String, then returned object whould contain
	* field fromString = true, otherwise fromString = false
	* @param {Object/String} data Configuration object or message string 
	*/
	makeConfig: function(data) {
		var config = {fromString: true};
		if (data) {
			if (Ext.isString(data)) {
				config.msg = data;
			} else {
				if (data.errors) { // Falcon_Answer object ?
					data = {msg: O.err.fmtAll(data.errors)};
				}
				config = Ext.apply(data, {fromString: false});
			}
		}
		return config;
	},

/**
	* Event message needs user callback
	* @param {Object/String} data Configuration object or message string 
	*/
	event: function(data) {
		this.show(Ext.apply({
			title: this.msgEvent,
			icon: Ext.window.MessageBox.prototype.INFO,
			delay: 0,
			cls: 'event',
			type: 'notify'
		}, this.makeConfig(data)));
	},

/**
	* Information
	* @param {Object/String} data Configuration object or message string 
	*/
	info: function(data) {
		this.show(Ext.apply({
			title: this.msgInformation,
			icon: Ext.window.MessageBox.prototype.INFO,
			cls: 'info',
			type: 'notify'
		}, this.makeConfig(data)));
	},

/**
	* Warning
	* @param {Object/String} data Configuration object or message string 
	*/
	warning: function(data) {
		this.show(Ext.apply({
			title: this.msgWarning,
			icon: Ext.window.MessageBox.prototype.WARNING,
			cls: 'warn',
			type: 'notify'
		}, this.makeConfig(data)));
	},

/**
	* Error
	* @param {Object/String} data Configuration object or message string 
	*/
	error: function (data, alternate) {
	    var msg;

	    if (Ext.isString(data)) {
	        msg = data;
	    } else {
	        var message = null;

            try {
                message = Ext.decode(data.responseText).Message;
            } catch (e) {
            }

	        msg = message || alternate;
	    }

	    msg = msg || 'Ошибка';

	    this.show(Ext.apply({
			title: this.msgError,
			icon: Ext.window.MessageBox.prototype.ERROR,
			cls: 'error',
			type: 'dialog'
		}, this.makeConfig(msg)));
	},

/**
	* Get opened message by key
	* @param {String} key Key
	* @return {Object} HTMLElement
	*/
	getOpenedByKey: function(key) {
		return this.opened.getByKey(key) || null;
	}
});