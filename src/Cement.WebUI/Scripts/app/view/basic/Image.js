﻿Ext.define('Cement.view.basic.Image', {
    extend: 'Ext.form.Panel',
    alias: 'widget.cementimage',
    margin: '0 0 10 0',
    border: 0,
    cls: 'product-view-image-block',
    layout: 'hbox',
    isFormField: true,
    name: 'p_img',
    imgStyle: 'null',

    reset: function () {
        this.setImg();
    },

    setValue: function (value) {
        this.setImg(value);
    },

    setImg: function(imgSrc) {
        var img = this.down('image');
        
        if (img) {
            var src = imgSrc || Cement.Config.default_photo;

            if (img.src === src) {
                return;
            }
            
            img.setSrc(Cement.Config.default_photo);

            if (src !== Cement.Config.default_photo) {
                img.setLoading("Загрузка...");
                img.setSrc(src);
            }
        }
    },

    validate: function() {
        return true;
    },

    isFileUpload: function() {
        return false;
    },

    getSubmitData: function() {
        return null;
    },

    initComponent: function () {
        var me = this;
        me.getName = function () { return me.name; };

        me.items = [
            {
                xtype: 'image',
                src: Cement.Config.default_photo,
                imgCls: 'product-view-image',
                style: me.imgStyle,
                listeners: {
                    render: function(image) {
                        var panel = image.up();
                        image.getEl().on('load', function() {
                            image.setLoading(false);
                            panel.doLayout();
                        });
                        image.getEl().on('error', function () {
                            image.setLoading(false);
                            image.setSrc(Cement.Config.default_photo);
                        });
                    }
                }
            }
        ];

        me.callParent(arguments);
    }
});