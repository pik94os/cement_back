Ext.override(Ext.view.Table, {
    onRowSelect: function (rowIdx) {
        var me = this;

        me.addRowCls(rowIdx, me.selectedItemCls);
        if (me.isRowStyleFirst(rowIdx)) {
            var g = me.getRowStyleTableEl(rowIdx);
            if (g) {
                g.addCls(me.tableSelectedFirstCls);
            }
        } else {
            me.addRowCls(rowIdx - 1, me.beforeSelectedItemCls);
        }
    },

    onRowFocus: function(rowIdx, highlight, supressFocus) {
        var me = this;

        if (highlight) {
            me.addRowCls(rowIdx, me.focusedItemCls);
            if (me.isRowStyleFirst(rowIdx)) {
                var g = me.getRowStyleTableEl(rowIdx);
                if (g) {
                    g.addCls(me.tableFocusedFirstCls);
                }
            } else {
                me.addRowCls(rowIdx - 1, me.beforeFocusedItemCls);
            }
            if (!supressFocus) {
                me.focusRow(rowIdx);
            }
            //this.el.dom.setAttribute('aria-activedescendant', row.id);
        } else {
            me.removeRowCls(rowIdx, me.focusedItemCls);
            if (me.isRowStyleFirst(rowIdx)) {
                var g = me.getRowStyleTableEl(rowIdx);
                if (g) {
                    g.removeCls(me.tableFocusedFirstCls);
                }
            } else {
                me.removeRowCls(rowIdx - 1, me.beforeFocusedItemCls);
            }
        }

        if ((Ext.isIE6 || Ext.isIE7) && !me.ownerCt.rowLines) {
            me.repaintRow(rowIdx)
        }
    },

    onRowDeselect : function(rowIdx) {
        var me = this;
        me.removeRowCls(rowIdx, [me.selectedItemCls, me.focusedItemCls]);
        if (me.isRowStyleFirst(rowIdx)) {
            var g = me.getRowStyleTableEl(rowIdx);
            if (g) {
                g.removeCls([me.tableFocusedFirstCls, me.tableSelectedFirstCls]);
            }
        } else {
            me.removeRowCls(rowIdx - 1, [me.beforeFocusedItemCls, me.beforeSelectedItemCls]);
        }
    }
});
Ext.define('Cement.view.basic.GridFix', {
    override: 'Ext.view.Table',
    setHighlightedItem: function(item) {
        return;
    }
});

/*
 * Ключевой контрол всего проекта - "Универсальный грид"
 * Настраивается наследованием с некоторым изменением конфигов и
 * функции getGridColumns
 */
Ext.define('Cement.view.basic.Grid', {
    extend: 'Ext.panel.Panel',
    alias: 'widget.basicgrid',
    bbarText: 'Показаны пользователи {0} - {1} из {2}',
    bbarEmptyMsg: 'Нет пользователей',
    bbarUsersText: 'Пользователей на странице: ',
    title: 'Пользователи',
    createWindowTitle: '',
    gridStore: 'Users',
    gridStateId: 'stateUsersList',
    gridStateful: true,
    printItemUrl: '/{0}',
    printUrl: '/',
    helpUrl: '/',
    deleteUrl: '/',
    filterControl: '',
    formControl: '',
    signItemUrl: '',
    autoLoadStore: false,
    enableDetail: true,
    enableLocking: true,
    showBottomBar: true,
    showLeftBar: true,
    showCreateButton: true,
    showFilterButton: true,
    showSimpleButtonsOnTop: false,
    showSettings: true,
    removeAfterSendToSign: true,
    fixedColumnHeaderHeight: 20,
    overrideColumnHeaderHeight: true,

    gridViewConfig: {
        getRowClass: function(rec, index, rowParams, store) {
            return (rec.get('p_grid_row_cls')) ? rec.get('p_grid_row_cls') : '';
        }
    },

    mixins: [
        'Cement.view.basic.GridRenderers'
    ],
    
    getFilterItems: function () {
        return [
            {
                text: 'Выделить все',
                checked: false,
                checkHandler: this.toggleFilterSelection
            },
            '-',
            {
                text: 'Расчетный счет',
                kind: 'selector',
                field_name: 'account_number',
                checked: false
                // checkHandler: onItemCheck
            },
            {
                text: 'Корреспондентский счет',
                kind: 'selector',
                field_name: 'corr_number',
                checked: false
            },
            {
                text: 'БИК',
                kind: 'selector',
                field_name: 'bik',
                checked: false
            },
            {
                text: 'Наименование',
                kind: 'selector',
                field_name: 'name',
                checked: false
            }
        ];
    },

    archiveItem: function (grid, record) {
        if (record) {
            Ext.Msg.confirm('Внимание', 'Вы действительно хотите отправить элемент в архив?', function (btn) {
                if (btn == 'yes') {
                    Ext.Ajax.request({
                        url: Ext.String.format(grid.up('basicgrid').archiveItemUrl, record.get('id')),
                        method: 'POST',
                        success: function (response) {
                            var json = Ext.JSON.decode(response.responseText);
                            if (json.success) {
                                grid.getStore().remove(record);
                                Cement.Msg.info(json.msg);
                            }
                            else {
                                Cement.Msg.error(json.msg);
                            }
                        },
                        failure: function () {
                            Cement.Msg.error('Ошибка при отправке в архив');
                        }
                    });
                }
            });
        }
    },

    unArchiveItem: function (grid, record) {
        if (record) {
            Ext.Msg.confirm('Внимание', 'Вы действительно хотите вернуть элемент из архива?', function (btn) {
                if (btn == 'yes') {
                    Ext.Ajax.request({
                        url: Ext.String.format(grid.up('basicgrid').unArchiveItemUrl, record.get('id')),
                        method: 'POST',
                        success: function (response) {
                            var json = Ext.JSON.decode(response.responseText);
                            if (json.success) {
                                grid.getStore().remove(record);
                                Cement.Msg.info(json.msg);
                            }
                            else {
                                Cement.Msg.error(json.msg);
                            }
                        },
                        failure: function () {
                            Cement.Msg.error('Ошибка при отправке в архив');
                        }
                    });
                }
            });
        }
    },

    docStatusColRender: function (value, obj, record) {
        var result = "<div class='doc-status-icon";
        if (record.get('p_security') == Cement.Config.office_work.writ.security.normal) {
            result += ' security-normal';
        }
        if (record.get('p_security') == Cement.Config.office_work.writ.security.high) {
            result += ' security-high';
        }
        if (record.get('p_security') == Cement.Config.office_work.writ.security.paranoidal) {
            result += ' security-paranoidal';
        }
        result += "'></div>";
        result += "<div class='doc-status-icon";
        if (record.get('p_importance') == Cement.Config.office_work.writ.importance.normal) {
            result += ' importance-normal';
        }
        if (record.get('p_importance') == Cement.Config.office_work.writ.importance.high) {
            result += ' importance-high';
        }
        result += "'></div>";
        return result;
    },

    getToolbarItems: function () {
        var me = this,
            applySize = function (item) {
            var store = me.getStore();
            store.pageSize = item.sZ;
            store.load();
        };
        var result = [
            {
                xtype: 'container',
                width: 20,
                height: 20,
                html: '&nbsp;'
            }, {
                xtype: 'button',
                text: 'Создать',
                iconCls: 'icon-add-item',
                action: 'create_item',
                disabled: false,
                hidden: !this.showCreateButton
            }, {
                xtype: 'button',
                text: 'Поиск',
                hidden: !this.showFilterButton,
                iconCls: 'icon-filter',
                menu: {
                    xtype: 'menu',
                    plain: true,
                    items: this.mergeSearchMenu(this.getFilterItems())
                }
            }
        ];

        var additional = this.getAdditionalButtons();
        for (var i = 0; i < additional.length; i++) {
            additional[i].hidden = me.accessType != Cement.Config.permission.readwrite;
            result.push(additional[i]);
        }

        result.push('->');
        if (this.showSimpleButtonsOnTop) {
            var x = [{
                xtype: 'button',
                iconCls: 'icon-reload',
                action: 'reload_grid',
                hidden: !this.showSimpleButtonsOnTop
            }, '-',{
                xtype: 'button',
                iconCls: 'icon-print',
                action: 'print_grid',
                hidden: !this.showSimpleButtonsOnTop
            }, '-',{
                xtype: 'button',
                iconCls: 'icon-help',
                action: 'show_help',
                hidden: !this.showSimpleButtonsOnTop
            }, '-'];
            for (var i = 0; i < x.length; i++) {
                result.push(x[i]);
            }
        }
        if (this.showSettings) {
            result.push({
                xtype: 'button',
                iconCls: 'icon-settings',
                text: 'Настройка',
                menu: [
                    {
                        text: 'Строк на странице',
                        menu: [
                            {
                                text: '5',
                                sZ: 5,
                                handler: applySize
                            }, {
                                text: '10',
                                sZ: 10,
                                handler: applySize
                            }, {
                                text: '15',
                                sZ: 15,
                                handler: applySize
                            }, {
                                text: '20',
                                sZ: 20,
                                handler: applySize
                            }, {
                                text: '25',
                                sZ: 25,
                                handler: applySize
                            }, {
                                text: '50',
                                sZ: 50,
                                handler: applySize
                            }, {
                                text: '100',
                                sZ: 100,
                                handler: applySize
                            }
                            //, {
                            //    text: 'Все',
                            //    sZ: 100000000,
                            //    handler: applySize
                            //}
                        ]
                    }, {
                        text: 'Фильтр',
                        iconCls: 'icon-filter-advanced',
                        handler: this.showFilteringWindow
                    }, {
                        text: 'Сортировка',
                        iconCls: 'icon-sort',
                        handler: this.showSortingWindow
                    }, {
                        text: 'Подсветка',
                        iconCls: 'icon-highlight',
                        handler: this.showHighlightWindow
                    }
                ]
            });
        }
        return {
            items: result
        };
    },

    getActionColumns: function () {
        return {
            xtype: 'rowactions',
            hideable: false,
            resizeable: false,
            locked: true,
            width: 46,
            actions: [
                {
                    iconCls: 'icon-edit-item',
                    qtip: 'Редактировать',
                    callback: this.editItem
                },
                {
                    iconCls: 'icon-delete-item',
                    qtip: 'Удалить',
                    callback: this.deleteItem
                }
            ],
            keepSelection: true
        };
    },

    mergeActions: function (items) {
        var actions = null,
            res = [];

        if (this.accessType == Cement.Config.permission.readwrite) {
            actions = this.getActionColumns();
        }

        if (actions) {
            if (actions.length) {
                res = res.concat(actions);
            }
            else {
                res.push(actions);
            }
        }
        for (var i = 0; i < items.length; i++) {
            res.push(items[i]);
        }
        return res;
    },

    mergeSearchMenu: function (items) {
        var result = [{
            xtype: 'cleartrigger',
            emptyText: 'Поиск',
            name: 'search_input',
            hidden: !this.showFilterButton,
            padding: '4 4 0 4',
            onTrigger2Click: this.applyFilter
        }, {
            text: 'Выделить все',
            checked: false,
            checkHandler: this.toggleFilterSelection
        }, '-'];
        for (var i = 0; i < items.length; i++) {
            result.push(items[i]);
        }
        return result;
    },

    getGridColumns: function () {
        return [
            this.getActionColumns(),
            { text: 'Имя', dataIndex: 'name', flex: 2 },
            { text: 'E-mail', dataIndex: 'email', flex: 1 },
            {
                text: 'Служебная информация',
                columns: [
                    { text: 'Отдел', dataIndex: 'department', flex: 1.5 },
                    { text: 'Должность', dataIndex: 'department', flex: 0.5 }
                ]
            }
        ];
    },

    getGridColumnsWithHeight: function () {

        var columns = this.getGridColumns();
        var height = this.fixedColumnHeaderHeight;

        if (this.overrideColumnHeaderHeight) {
            Ext.each(columns, function (item) { item.height = height; });
        }

        return columns;
    },

    getGridPlugins: function () {
        return [{ ptype: 'gridheaderfilters' }];
    },

    createSignWindow: function (title) {
        var win = Ext.create('Ext.window.Window', {
            title: 'Отправить на согласование',
            layout: 'fit',
            modal: true,
            width: 350,
            items: [
                {
                    xtype: 'form',
                    bodyPadding: 10,
                    layout: 'anchor',
                    items: [
                        {
                            fieldLabel: 'Сотрудник',
                            anchor: '100%',
                            xtype: 'combo',
                            store: Ext.getStore(this.directorsStoreId),
                            editable: false,
                            queryMode: 'local',
                            displayField: 'p_name',
                            valueField: 'id',
                            name: 'p_director_id',
                            allowBlank: false,
                            listeners: {
                                change: function (self, newValue) {
                                    if (newValue) {
                                        self.up('form').down('button[action=employee_selected]').enable();
                                    }
                                },
                                scope: this
                            }
                        }
                    ],
                    bbar: [
                        {
                            xtype: 'button',
                            text: 'Отправить',
                            action: 'employee_selected',
                            disabled: true,
                            iconCls: 'icon-save-item'
                        },
                        {
                            xtype: 'button',
                            text: 'Отмена',
                            action: 'form_cancel',
                            iconCls: 'icon-cancel'
                        }
                    ]
                }
            ]
        });
        win.down('button[action=form_cancel]').on('click', function () {
            win.close();
        });
        return win;
    },

    signItem: function (grid, record) {
        var c = grid.up('basicgrid'),
            win = Ext.create('Cement.view.common.SignWindow', {
                singleSelection: true
            });
        Cement.util.Animation.createWindowAnimation(win);
        win.show();
        win.on('signitem', function (data, send) {
            Ext.Ajax.request({
                method: 'POST',
                url: c.signItemUrl,
                params: {
                    p_item: record.get('id'),
                    p_data: Ext.JSON.encode(data),
                    p_send: send
                },
                success: function (response) {
                    var json = Ext.JSON.decode(response.responseText);
                    if (json.success) {
                        if (c.removeAfterSendToSign) {
                            grid.getStore().remove(record);
                        }
                        Cement.Msg.info(json.msg);
                    }
                    else {
                        Cement.Msg.error(json.msg);
                    }
                },
                failure: function () {
                    Cement.Msg.error('Ошибка');
                }
            });
            win.close();
        }, this);
    },

    writeItem: function (grid, record) {
        var c = grid.up('basicgrid'),
            win = Ext.create('Cement.view.WriteWindow').show();
        Cement.util.Animation.createWindowAnimation(win);
        win.on('signitem', function (data, send) {
            Ext.Ajax.request({
                method: 'POST',
                url: c.writeItemUrl,
                params: {
                    p_item: record.get('id'),
                    p_data: Ext.JSON.encode(data),
                    p_send: send
                },
                success: function (response) {
                    var json = Ext.JSON.decode(response.responseText);
                    if (json.success) {
                        if (c.removeAfterSendToSign) {
                            grid.getStore().remove(record);
                        }
                        Cement.Msg.info(json.msg);
                    }
                    else {
                        Cement.Msg.error(json.msg);
                    }
                },
                failure: function () {
                    Cement.Msg.error('Ошибка');
                }
            });
            win.close();
        }, this);
    },

    getAdditionalButtons: function () {
        return [];
    },

    getGridFeatures: function () {
        return null;
        this.groupingFeature = Ext.create('Ext.grid.feature.Grouping');
        return [
            this.groupingFeature, {
            ftype: 'remotesummary',
            dock: 'bottom'
        }];
    },

    createPagingCombo: function() {
        return Ext.create('Ext.form.ComboBox', {
            // name : 'perpage',
            width: 60,
            store: new Ext.data.ArrayStore({
                fields: ['id'],
                data: [
                    ['15'],
                    ['25'],
                    ['50'],
                    ['100']
                ]
            }),
            mode: 'local',
            value: '15',
            listWidth: 40,
            triggerAction: 'all',
            displayField: 'id',
            valueField: 'id',
            editable: false,
            forceSelection: true
        });
    },

    createBbar: function (combo) {
        return Ext.create('Ext.PagingToolbar', {
            store: this.gridStore,
            displayInfo: true,
            displayMsg: this.bbarText,
            emptyMsg: this.bbarEmptyMsg,
            beforePageText: "Страница",
            items: ['-',
                this.bbarUsersText,
                combo
            ]
        });
    },

    initComponent: function () {
        var combo = null,
            bbar = null,
            me = this,
            lbar = null;

        me.showCreateButton = me.showCreateButton && me.accessType == Cement.Config.permission.readwrite;

        if (this.showBottomBar) {
            combo = me.createPagingCombo();
            bbar = me.createBbar(combo);
            bbar.down('#refresh').hide();
            bbar.down('#refresh').nextSibling().hide();
            combo.on('select', function (self, record) {
                var store = me.getStore();
                store.pageSize = parseInt(record[0].get('id'), 10);
                store.load();
            }, this);
        }
        if (this.showLeftBar) {
            lbar = [{
                xtype: 'button',
                iconCls: 'icon-help',
                action: 'show_help'
            }, '-',{
                xtype: 'button',
                iconCls: 'icon-print',
                action: 'print_grid'
            }, '-',{
                xtype: 'button',
                iconCls: 'icon-reload',
                action: 'reload_grid'
            }];
        }
        var renderer = function(rec, meta) {
            // return '123';
            return val;
        };
            //gridColumns = this.getGridColumnsWithHeight();
        // Apply all renderers:
        // Ext.each(gridColumns, function (col) {
        //     if (col.columns) {
        //         Ext.each(col.columns, function (c) {
        //             if (!c.renderer) {
        //                 c.renderer = renderer;
        //             }
        //         }, this);
        //     }
        //     else {
        //         if (!col.renderer) {
        //             col.renderer = renderer;
        //         }
        //     }
        // }, this);
        Ext.apply(this, {
            title: this.title,
            cls: 'basic-grid',
            items: [
                this.createMainComponent(bbar, lbar)
            ],
            tbar: this.getToolbarItems()
        });

        this.callParent(arguments);
        if (this.down('button[action=reload_grid]')) {
            this.down('button[action=reload_grid]').on('click', this.reloadGrid, this);
        }
        if (this.down('button[action=print_grid]')) {
            this.down('button[action=print_grid]').on('click', this.printGrid, this);
        }
        if (this.down('button[action=show_help]')) {
            this.down('button[action=show_help]').on('click', this.showHelp, this);
        }
        if (this.down('button[action=show_filter]')) {
            this.down('button[action=show_filter]').on('click', this.showFilter, this);
        }
        if (this.down('button[action=show_form]')) {
            this.down('button[action=show_form]').on('click', this.showForm, this);
        }
        if (this.down('button[action=create_item]')) {
            this.down('button[action=create_item]').on('click', this.createItem, this);
        }
        if (this.autoLoadStore) {
            if (this.constantFilter) {
                Ext.getStore(this.gridStore).filter(this.constantFilter, true);
            }
            else {
                Ext.data.StoreManager.lookup(this.gridStore).load();
            }
        }

        var grid = this.down('grid');

        if (grid) {
            grid.on('itemdblclick', this.printItem, this);
        }
        this.addEvents('edititem');
        this.addEvents('createitem');
    },

    createMainComponent: function (bbar, lbar) {
        return {
            stateful: this.gridStateful,
            stateId: this.gridStateId,
            store: this.gridStore,
            xtype: 'grid',
            border: 0,
            layout: 'fit',
            // id: this.gridId,
            // features: [
            //     { ftype: 'grouping' }
            // ],
            features: this.getGridFeatures(),
            plugins: this.getGridPlugins(),
            columns: this.getGridColumnsWithHeight(),
            viewConfig: this.gridViewConfig,
            bbar: bbar,
            lbar: lbar
        };
    },

    printItem: function (self, record) {
        window.open(
            Ext.String.format(this.printItemUrl, record.get('id'))
        );
    },

    loadStore: function () {
        if (this.constantFilter) {
            Ext.getStore(this.gridStore).filter(this.constantFilter, true);
        }
        else {
            Ext.data.StoreManager.lookup(this.gridStore).load();
        }
    },

    loadStoreFirst: function () {
        this.loadStore();
    },

    showHighlightWindow: function (btn) {
        if (!this.highlightWindow) {
            var grid = btn.up('basicgrid'),
                store = grid.getStore(),
                items = grid.getFilterItems(),
                fields = [],
                i = 0;
            Ext.each(items, function (itm) {
                fields.push({
                    name: itm.field_name,
                    label: itm.text
                });
            });
            win = Ext.create('Cement.view.basic.HightLightWindow', {
                sort_fields: fields
            });
            win.on('highlight_set', function (filters) {
                if (this.constantFilter) {
                    for (var i = 0; i < this.constantFilter.length; i++) {
                        filters.push(this.constantFilter[i]);
                    }
                }
                store.filter(filters, true);
                win.hide();
            }, this);
            this.highlightWindow = win;
        }
        this.highlightWindow.show();
    },

    showFilteringWindow: function (btn) {
        if (!this.filteringWindow) {
            var grid = btn.up('basicgrid'),
                store = grid.getStore(),
                items = grid.getFilterItems(),
                fields = [],
                i = 0;
            Ext.each(items, function (itm) {
                fields.push({
                    name: itm.field_name,
                    label: itm.text
                });
            });
            win = Ext.create('Cement.view.basic.FilterWindow', {
                sort_fields: fields
            });
            win.on('filter_set', function (filters) {
                if (this.constantFilter) {
                    for (var i = 0; i < this.constantFilter.length; i++) {
                        filters.push(this.constantFilter[i]);
                    }
                }
                store.filter(filters, true);
                win.hide();
            }, this);
            this.filteringWindow = win;
        }
        this.filteringWindow.show();
    },

    showSortingWindow: function (btn) {
        if (!this.sortingWindow) {
            var grid = btn.up('basicgrid'),
                store = grid.getStore(),
                items = grid.getFilterItems(),
                fields = [],
                i = 0;
            Ext.each(items, function (itm) {
                fields.push({
                    name: itm.field_name,
                    label: itm.text
                });
            });
            win = Ext.create('Cement.view.basic.SortWindow', {
                sort_fields: fields
            });
            win.on('sorting_set', function (sorting) {
                store.sorters.removeAll();
                win.hide();
                if (sorting.length) {
                    Ext.each(sorting, function (s) {
                        store.sorters.add(s);
                    }, this);
                    store.load();
                }
            }, this);
            this.sortingWindow = win;
        }
        this.sortingWindow.show();
    },

    createItem: function () {
        if (this.creatorTree) {
            if (!this.creatorStore) {
                this.creatorStore = Ext.create('Ext.data.TreeStore', {
                    fields: ['text', 'topLevel', 'master', 'model'],
                    root: {
                        expanded: true,
                        topLevel: 'abc',
                        children: this.creatorTree.children
                    }
                });
            }
            var win = Ext.create('Ext.window.Window', {
                    width: 500,
                    height: 500,
                    layout: 'fit',
                    cls: 'creator-window',
                    title: this.createWindowTitle,
                    items: [
                        {
                            xtype: 'panel',
                            layout: 'fit',
                            border: 0,
                            cls: 'tree-no-border-bottom',
                            allowDeselect: false,
                            items: [
                                {
                                    xtype: 'treepanel',
                                    border: 0,
                                    rootVisible: false,
                                    store: this.creatorStore
                                }
                            ]
                        }
                    ],
                    bbar: [
                        {
                            xtype: 'button',
                            text: 'Создать',
                            action: 'form_save',
                            iconCls: 'icon-add-item',
                            disabled: true
                        },
                        {
                            xtype: 'button',
                            text: 'Отмена',
                            action: 'form_cancel',
                            iconCls: 'icon-cancel'
                        }
                    ]
                }),
                rec = null,
                me = this;
            win.down('button[action=form_cancel]').on('click', function () {
                win.close();
            });
            win.down('treepanel').on('select', function (panel, record) {
                if (record.get('leaf')) {
                    win.down('button[action=form_save]').enable();
                    rec = record;
                }
                else {
                    win.down('button[action=form_save]').disable();
                    rec = null;
                }
            });
            win.down('treepanel').on('itemdblclick', function (panel, record) {
                if (record.get('leaf')) {
                    rec = record;
                    me.fireEvent('createitem', rec.get('model'), rec.get('master'), rec.raw && rec.raw.xtype);
                    win.close();
                }
            });
            win.down('button[action=form_save]').on('click', function () {
                if (rec) {
                    me.fireEvent('createitem', rec.get('model'), rec.get('master'), rec.raw && rec.raw.xtype);
                    win.close();
                }
            });
            Cement.util.Animation.createWindowAnimation(win);
            win.show();
        }
        else {
            this.fireEvent('createitem', Ext.getStore(this.gridStore).model.$className);
        }
    },

    deleteItem: function (grid, record) {
        var me = this;
        Ext.Msg.confirm('Внимание', 'Вы действительно хотите удалить элемент?', function (btn) {
            if (btn == 'yes') {
                Ext.Ajax.request({
                    url: Ext.String.format(grid.up('basicgrid').deleteUrl, record.get('id')),
                    method: 'POST',
                    success: function (response) {
                        var obj = Ext.decode(response.responseText);
                        if (obj.success || response.responseText == '') {
                            if (obj.msg) {
                                Cement.Msg.info(obj.msg);
                            }
                            else {
                                Cement.Msg.info('Элемент удален');
                            }
                            grid.getStore().load();
                        }
                        else {
                            Cement.Msg.error(obj.msg);
                        }
                        if (me.afterRecordDelete) {
                            me.afterRecordDelete();
                        }
                    },
                    failure: function () {
                        Cement.Msg.error('Ошибка при удалении элемента');
                    }
                });
            }
        });
    },

    declineItem: function (grid, record) {
        if (grid.up('basicgrid').declineItemUrl) {
            var me = this,
                declineWindow = Ext.create('Cement.view.DeclineWindow');
            // this.declineItemUrl = '/saa';
            declineWindow.setDocumentId(record);
            declineWindow.on('declineitem', function (paramsObj) {
                Ext.Ajax.request({
                    url: grid.up('basicgrid').declineItemUrl,
                    method: 'POST',
                    params: paramsObj,
                    success: function (response) {
                        var obj = Ext.decode(response.responseText);
                        if (obj.success) {
                            if (obj.msg) {
                                Cement.Msg.info(obj.msg);
                            }
                            else {
                                Cement.Msg.info('Успешно отправлено');
                            }
                            grid.getStore().load();
                            declineWindow.close();
                        }
                        else {
                            Cement.Msg.error(obj.msg);
                        }
                    },
                    failure: function () {
                        Cement.Msg.error('Ошибка при отправке');
                    }
                });
            }, this);
            Cement.util.Animation.createWindowAnimation(declineWindow);
            declineWindow.show();
        }
    },

    editItem: function (grid, record) {
        var bacisGrid = grid.up('basicgrid');

        if (bacisGrid.transformRecord) {
            record = bacisGrid.transformRecord(record);
        }
        if (bacisGrid.loadItemOnEdit) {
            Ext.Ajax.request({
                url: Ext.String.format(bacisGrid.loadItemUrl, record.get('id')),
                method: 'POST',
                success: function (response) {
                    var r = Ext.create(record.$className, Ext.JSON.decode(response.responseText));
                    // me.setLoading(false);
                    bacisGrid.fireEvent('edititem', r, bacisGrid.$className);
                },
                failure: function () {
                    Cement.Msg.error('Ошибка при загрузке элемента');
                }
            });
        }
        else {
            bacisGrid.fireEvent('edititem', record, bacisGrid.$className);
        }
    },

    showHelp: function () {
        window.open(
            this.appendConstantFilterParams(this.helpUrl)
        );
    },

    appendConstantFilterParams: function (url) {
        var params = [];
        Ext.getStore(this.gridStore).filters.each(function (filter) {
            params.push(filter.property + '=' + filter.value)
        });
        if (this.constantFilter) {
            for (var i = 0; i < this.constantFilter.length; i++) {
                params.push(this.constantFilter[i].property + '=' + this.constantFilter[i].value);
            }
        }
        if (params.length > 0) {
            url = url + '?' + params.join('&');
        }
        return url;
    },

    printGrid: function () {
        window.open(
            this.appendConstantFilterParams(
                this.printUrl
            )
        );
    },

    reloadGrid: function (btn) {
        var grid = this,
            store = grid.getStore();
        //grid.setLoading(true);
        if (this.constantFilter) {
            store.clearFilter(true);
            store.filter(this.constantFilter, true);
            //grid.setLoading(false);
        }
        else {
            store.load();
            //store.load(function () {
            //    grid.setLoading(false);
            //});
        }
    },

    getFilterFields: function () {
        var items = this.down('toolbar').query('menu menucheckitem[kind=selector]'),
            val = this.down('toolbar cleartrigger[name=search_input]').getValue(),
            result = [];
        if (this.constantFilter) {
            for (var i = 0; i < this.constantFilter.length; i++) {
                result.push(this.constantFilter[i]);
            }
        }
        Ext.each(items, function (item) {
            if (item.checked) {
                result.push({
                    property: item.field_name,
                    value: val
                });
            }
        });
        return result;
    },

    toggleFilterSelection: function (item, checked) {
        var toolbar = item.up('toolbar'),
            items = toolbar.query('menu menucheckitem[kind=selector]');
        Ext.each(items, function (it) {
            it.setChecked(item.checked);
        });
    },

    applyFilter: function () {
        var grid = this.up('basicgrid'),
            filter = grid.getFilterFields();
        grid.getStore().clearFilter(true);
        grid.getStore().filter(filter);
    },

    getStore: function() {
        var me = this,
            grid = me.down && me.down('grid');

        if (grid) {
            return grid.getStore();
        }
        
        return Ext.getStore(me.gridStore);
    }
});