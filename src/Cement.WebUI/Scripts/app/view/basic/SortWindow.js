Ext.define('Cement.view.basic.SortWindow', {
	extend: 'Ext.window.Window',
	width: 550,
	height: 280,
	layout: 'fit',
	title: 'Сортировка',
	fieldsCount: 6,
	modal: true,
	resizable: false,

	sort_fields: [{
		name: 'p_field_1',
		label: 'Поле 1'
	}, {
		name: 'p_field_2',
		label: 'Поле 2'
	}, {
		name: 'p_field_3',
		label: 'Поле 3'
	}, {
		name: 'p_field_4',
		label: 'Поле 4'
	}, {
		name: 'p_field_5',
		label: 'Поле 5'
	}, {
		name: 'p_field_6',
		label: 'Поле 6'
	}],

	initComponent: function () {
		var items = [{
			width: 20,
			xtype: 'container'
		}, {
			width: 200,
			xtype: 'container',
			html: 'Столбец',
			padding: '0 0 5 10'
		}, {
			width: 140,
			xtype: 'container',
			html: 'Порядок',
			padding: '0 0 5 10'
		}, {
			width: 140,
			xtype: 'container',
			html: 'Сортировка нулей',
			padding: '0 0 5 10'
		}],
			nullStore = Ext.create('Ext.data.Store', {
				fields: ['id', 'name', 'code'],
				data: [
					{ id: 1, name: 'не сортировать', code: 'null_ignore' },
					{ id: 2, name: 'в начале списка', code: 'null_first' },
					{ id: 3, name: 'в конце списка', code: 'null_last' }
				]
			}),
			orderStore = Ext.create('Ext.data.Store', {
				fields: ['id', 'name', 'code'],
				data: [
					{ id: 1, name: 'по возрастанию', code: 'asc' },
					{ id: 2, name: 'по убыванию', code: 'desc' }
				]
			}),
			fieldsStore = Ext.create('Ext.data.Store', {
				fields: ['name', 'label'],
				data: this.sort_fields
			});
		for (var i = 1; i < this.fieldsCount + 1; i++) {
			items.push({
				xtype: 'container',
				html: i
			}, {
				xtype: 'combo',
				width: 180,
				margin: '3 10',
				queryMode: 'local',
				displayField: 'label',
				valueField: 'name',
				editable: false,
				store: fieldsStore,
				role: 'field_' + i + '_name'
			}, {
				xtype: 'combo',
				width: 130,
				margin: '3 10',
				queryMode: 'local',
				displayField: 'name',
				valueField: 'code',
				editable: false,
				store: orderStore,
				role: 'field_' + i + '_order'
			}, {
				xtype: 'combo',
				width: 130,
				margin: '3 10',
				queryMode: 'local',
				displayField: 'name',
				valueField: 'code',
				editable: false,
				store: nullStore,
				role: 'field_' + i + '_nulls'
			});
		}
		Ext.apply(this, {
			items: [{
				xtype: 'panel',
				border: 0,
				bodyPadding: 10,
				layout: {
					type: 'table',
					columns: 4
				},
				items: items
			}],
			bbar: [{
				xtype: 'button',
				text: 'Сохранить',
				action: 'save'
			}, {
				xtype: 'button',
				text: 'Отмена',
				action: 'cancel'
			}]
		});
		this.callParent(arguments);
		this.addEvents('sorting_set');
		this.down('button[action=save]').on('click', function () {
			var sorters = [];
			for (i = 1; i < this.fieldsCount + 1; i++) {
				var comboVal = this.down('combo[role=field_' + i + '_name]').getValue();
				if (comboVal) {
					var order = this.down('combo[role=field_' + i + '_order]').getValue(),
						nulls = this.down('combo[role=field_' + i + '_nulls]').getValue();
					if (order) {
						if (nulls) {
							sorters.push({
								property: comboVal + '_' + nulls,
								direction: order
							});
						}
					}
				}
			}
			this.fireEvent('sorting_set', sorters);
		}, this);
		this.down('button[action=cancel]').on('click', function () {
			this.hide();
		}, this);
		this.on('beforeclose', function () {
			this.hide();
			return false;
		});
	}
});