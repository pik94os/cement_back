Ext.define('Cement.view.basic.Tabs', {
	extend: 'Ext.tab.Panel',
    tabPosition: 'left',
    alias: 'widget.customtabs',
    enableDetail: false,

    initComponent: function () {
        var me = this;

        if (me.tabs && me.tabs.length) {
            me.items = me.tabs;
        }

        if (me.items) {
            me.items.forEach(function(item) {
                item.layout = 'fit';

                if (item.title) {
                    item.tabTitle = item.title;
                }
            });
        }

        this.callParent(arguments);
    	this.addEvents('showDetails');
    	Ext.each(this.query('basicgrid'), function (grid) {
    		grid.down('grid').on('itemclick', function (g, record) {
                if (grid.detailType) {
                    me.fireEvent('showDetails', grid.detailType, record);
                }
    		});
    	});
        this.on('tabchange', function (panel, newTab) {
            var grid = newTab.down('basicgrid');
            if (newTab.getActionColumns) {
                grid = newTab;
            }
            if (grid) {
                var sel = grid.down('grid').getSelectionModel().getSelection(),
                    model = {};
                if (sel.length) {
                    model = sel[0];
                }
                if (grid.detailType) {
                    this.fireEvent('showDetails', grid.detailType, model);
                }
            }
        }, this);
    }
});