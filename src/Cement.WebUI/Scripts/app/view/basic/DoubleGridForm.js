Ext.define('Cement.view.basic.DoubleGridForm', {
    extend: 'Cement.view.basic.Form',
    alias: 'widget.doublegridform',
    fieldsDisabled: false,

    hasTopPanel: true,
    hasBottomPanel: true,
    singleSelection: true,

    storeFields: ['id'],

    getTopItems: function () {
        return [];
    },

    getTopPanel: function () {
        return {
            xtype: 'panel',
            bodyCls: 'custom-background',
            margin: '0 0 5 0',
            items: [
                {
                    xtype: 'fieldset',
                    border: 0,
                    margin: 0,
                    padding: '5 5 0 5',
                    defaults: {
                        xtype: 'textfield',
                        width: 500,
                        labelAlign: 'right'
                    },
                    items: this.getTopItems()
                }
            ]
        };
    },

    getBottomPanel: function () {
        return {
            xtype: 'panel',
            bodyCls: 'custom-background',
            margin: '5 0 0 0',
            items: [
                {
                    xtype: 'fieldset',
                    border: 0,
                    margin: '5 0 0 0',
                    padding: 0,
                    defaults: {
                        xtype: 'textfield',
                        width: 500,
                        labelAlign: 'right'
                    },
                    items: this.getBottomItems()
                }
            ]
        };
    },

    getBottomItems: function() {
        return [];
    },

    getItems: function () {
        var items = [{
            xtype: 'hiddenfield',
            name: 'id'
        }];

        if (this.hasTopPanel) {
            items.push(this.getTopPanel());
        }

        items.push(this.getTopGrid());
        items.push({
            xtype: 'panel',
            bodyCls: 'custom-background',
            margin: '5 0',
            defaults: {
                xtype: 'button',
                margin: '2 0 2 2'
            },
            items: [
                {
                    text: 'Создать',
                    action: 'create',
                    iconCls: 'icon-move-up',
                    listeners: {
                        click: this.toDstGridFromSelected,
                        scope: this
                    }
                },
                {
                    text: 'Удалить',
                    action: 'delete',
                    iconCls: 'icon-move-down',
                    listeners: {
                        click: this.toSrcGridFromSelected,
                        scope: this
                    }
                }
            ]
        });
        items.push(this.getBottomGrid());

        if (this.hasBottomPanel) {
            items.push(this.getBottomPanel());
        }

        return [
        {
            padding: 5,
            layout: {
                type: 'vbox',
                align: 'stretch'
            },
            xtype: 'container',
            border: 0,
            items: items
        }];
    },

    getTopGrid: function () {
        var editor = Ext.create('Ext.grid.plugin.CellEditing', { clicksToEdit: 1 });

        return {
            xtype: 'panel',
            flex: 1,
            border: 1,
            layout: 'fit',
            height: 200,
            minHeight: 50,
            items: [{
                xtype: 'grid',
                role: 'sign-dst-grid',
                layout: 'fit',
                border: 0,
                bodyStyle: {
                    background: '#ffffff'
                },
                store: Ext.create('Ext.data.Store', {
                    fields: this.storeFields
                }),
                columns: this.getTopColumnsConfig(),
                plugins: [editor]
                
            }]
        };
    },

    getBottomGridStore: function () {
        return Ext.create('Ext.data.Store', {
            fields: this.storeFields
        });
    },

    getBottomGrid: function () {
        return {
            xtype: 'panel',
            flex: 1,
            border: 1,
            layout: 'fit',
            height: 200,
            minHeight: 50,
            items: [{
                xtype: 'grid',
                role: 'sign-src-grid',
                layout: 'fit',
                border: 0,
                bodyStyle: {
                    background: '#ffffff'
                },
                store: this.getBottomGridStore(),
                columns: this.getBottomColumnsConfig()
            }]
        };
    },

    getCommonColumns: function () {
        return [];
    },

    getTopGridRowActionsColumn: function () {
        var me = this;

        return [
			{
			    xtype: 'actioncolumn',
			    width: 24,
			    stopSelection: true,
			    resizeable: false,
			    hideable: false,
			    iconCls: 'icon-cancel',
			    handler: function (grid, rowIndex, colIndex, el, e, record) {
			        me.toSrcGridImplementer(record);
			    }
			}
        ];
    },

    getTopColumnsConfig: function () {
        return this.mergeColumns(this.getTopGridRowActionsColumn(), this.getCommonColumns(false));
    },

    getBottomGridRowActionsColumn: function () {
        var me = this;

        return [
			{
			    xtype: 'actioncolumn',
			    width: 24,
			    resizeable: false,
			    hideable: false,
			    iconCls: 'icon-add-item',
			    handler: function (grid, rowIndex, colIndex, el, e, record) {
			        me.toDstGridImplementer(record);
			    }
			}
        ];
    },

    getBottomColumnsConfig: function () {
        return this.mergeColumns(this.getBottomGridRowActionsColumn(), this.getCommonColumns(true));
    },

    mergeColumns: function (actions, items) {
        var res = [];

        if (actions && actions.length) {
            for (var i = 0; i < actions.length; i++) {
                res.push(actions[i]);
            }
        }

        if (items && items.length) {
            for (var j = 0; j < items.length; j++) {
                res.push(items[j]);
            }
        }

        return res;
    },

    toSrcGridFromSelected: function () {
        var selectionModel = this.dstGrid.getSelectionModel();

        selectionModel.refresh();

        var selection = selectionModel.getSelection();

        if (selection.length > 0) {
            this.toSrcGridImplementer(selection[0]);
        }
    },

    toSrcGridImplementer: function (record) {
        if (record) {
            this.srcStore.add(record.copy());
            this.dstStore.remove(record);
            this.onDstUnPopulate(record);
        }
    },

    toDstGridFromSelected: function () {
        var selectionModel = this.srcGrid.getSelectionModel();

        selectionModel.refresh();

        var selection = selectionModel.getSelection();

        if (selection.length > 0) {
            this.toDstGridImplementer(selection[0]);
        }
    },

    toDstGridImplementer: function (record) {
        if (record) {
            if (this.singleSelection) {
                if (this.dstStore.getCount() > 0) {
                    return;
                }
            }
            this.dstStore.add(record.copy());
            this.srcStore.remove(record);
            this.onDstPopulate(record);
        };
    },
    
    onDstPopulate: function (record) {
    },
    
    onDstUnPopulate: function (record) {
    },

    initComponent: function () {
        this.callParent(arguments);
        var me = this;

        me.srcGrid = me.down('*[role=sign-src-grid]');
        me.dstGrid = me.down('*[role=sign-dst-grid]');

        me.srcStore = me.srcGrid.getStore();
        me.dstStore = me.dstGrid.getStore();
    },

    reset: function () {
        this.dstStore.removeAll();
        this.srcStore.removeAll();
        this.callParent(arguments);
    }
});