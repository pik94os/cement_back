Ext.define('Cement.view.basic.HtmlViewer', {
    extend: 'Ext.form.field.HtmlEditor',
    alias: 'widget.htmlviewer',
    
    initComponent: function () {
        var me = this;
        me.callParent(arguments);
        me.setReadOnly(true);
    },

    createToolbar: function () {
        var me = this;
        me.callParent(arguments);
        me.toolbar.setHeight(1);
        return me.toolbar;
    },

    enableFormat: false,
    enableFontSize: false,
    enableColors: false,
    enableAlignments: false,
    enableLists: false,
    enableSourceEdit: false,
    enableLinks: false,
    enableFont: false,

    syncValue: function () {
        var me = this,
            body, changed, html, bodyStyle, match;

        if (me.initialized) {
            body = me.getEditorBody();
            html = body.innerHTML;
            if (Ext.isWebKit) {
                bodyStyle = body.getAttribute('style'); // Safari puts text-align styles on the body element!

                if (bodyStyle) {
                    match = bodyStyle.match(/text-align:(.*?);/i);
                    if (match && match[1]) {
                        html = '<div style="' + match[0] + '">' + html + '</div>';
                    }
                } 
            }
            html = me.cleanHtml(html);
            if (me.fireEvent('beforesync', me, html) !== false) {
                if (me.textareaEl.dom.value != html) {
                    me.textareaEl.dom.value = html;
                    changed = true;
                }

                me.fireEvent('sync', me, html);

                if (changed) {
                    // we have to guard this to avoid infinite recursion because getValue
                    // calls this method...
                    me.checkChange();
                }
            }
        }
    },
});