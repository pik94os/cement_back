Ext.define('Cement.view.basic.Complex', {
	extend: 'Ext.panel.Panel',
	alias: 'widget.basiccomplex',
  border: 0,
  enableDetail: false,
  autoLoadStore: false,
  isComplexView: true,
  tabTitle: 'Товары',
  topXType: 'panel',
  topDetailType: 'Cement.view.product.View',
  bottomTabs: [{
  	title: 'Товары',
  	xtype: 'panel',
  	detailType: 'Cement.view.product.View',
  	constantFilterParam: 'product_id'
  }, {
  	title: 'Услуги',
  	xtype: 'panel',
  	detailType: 'Cement.view.service.View',
  	constantFilterParam: 'product_id'
  }],
  bottomTitle: 'Справочник',
  subBottomTitle: '',
  rowsCount: 2,
  childFilterColumn: 'p_base_doc',

  getActiveTab: function () {
    return this.down('*[role=main]');
  },

  initComponent: function () {
      var tabs = this.bottomTabs;
      Ext.each(tabs, function (tab) {
          tab.layout = 'fit';
      });
      if (this.rowsCount == 2) {
          Ext.apply(this, {
              title: this.tabTitle,
              layout: 'border',
              border: 0,
              defaults: {
                  split: true
              },
              items: [{
                  region: 'center',
                  border: 0,
                  layout: 'fit',
                  role: 'main',
                  title: null,
                  cls: 'with-bottom-border no-side-borders',
                  detailType: this.topDetailType,
                  xtype: this.topXType,
                  accessType: this.accessType

              }, {
                  region: 'south',
                  minHeight: 120,
                  height: 200,
                  xtype: 'tabpanel',
                  cls: 'bottom-collapsible-header last no-side-borders bottom-grids',
                  role: 'bottom-panel',
                  collapsible: true,
                  collapsed: true,
                  title: this.bottomTitle,
                  items: tabs
              }]
          });
      }
      else {
          var bottom = null,
            subBottom = null;
          if (this.bottomTabs) {
              bottom = {
                  region: 'south',
                  minHeight: 120,
                  height: 200,
                  xtype: 'tabpanel',
                  cls: 'bottom-collapsible-header no-side-borders bottom-grids',
                  collapsedCls: 'collapsed-with-bottom-border',
                  role: 'bottom-panel',
                  collapsible: true,
                  collapsed: true,
                  title: this.bottomTitle,
                  items: tabs
              };
          }
          else {
              bottom = {
                  region: 'south',
                  minHeight: 120,
                  height: 200,
                  title: this.bottomTitle,
                  xtype: 'panel',
                  layout: 'fit',
                  cls: 'bottom-collapsible-header no-side-borders bottom-grids',
                  collapsedCls: 'collapsed-with-bottom-border',
                  role: 'bottom-panel',
                  collapsible: true,
                  collapsed: true,
                  items: [this.bottomControl || {
                      border: 0,
                      layout: 'fit',
                      title: null,
                      shownTitle: null
                  }]
              };
          }
          Ext.each(this.subBottomTabs, function (itm) {
              itm.layout = 'fit';
          });
          subBottom = {
              region: 'south',
              minHeight: 120,
              height: 200,
              title: this.subBottomTitle,
              xtype: 'tabpanel',
              layout: 'fit',
              cls: 'bottom-collapsible-header no-side-borders bottom-grids',
              collapsedCls: 'collapsed-with-bottom-border',
              role: 'sub-bottom-panel',
              collapsible: true,
              collapsed: true,
              items: this.subBottomTabs
          };
          Ext.apply(this, {
              title: this.tabTitle,
              layout: 'border',
              border: 0,
              defaults: {
                  split: true
              },
              items: [{
                  region: 'center',
                  border: 0,
                  layout: 'fit',
                  role: 'main',
                  title: null,
                  cls: 'with-bottom-border no-side-borders bottom-grids',
                  xtype: this.topXType,
                  detailType: this.topDetailType,
                  accessType: this.accessType
              }, bottom, subBottom]
          });
      }
      this.callParent(arguments);
      this.addEvents('showDetails');
      this.on('afterrender', this.setupEvents, this);
  },

  setupEvents: function () {
      var main = this.down('*[role=main]').down('grid'),
          bottomPanel = this.down('*[role=bottom-panel]'),
          subBottomPanel = this.down('*[role=sub-bottom-panel]');
           
      if (main) {
          main.on('itemclick', this.topSelectionChanged, this);
      }
      var me = this;
      if (!this.bottomTabs) {
          this.bottomTabs = [{
              xtype: this.bottomControl.xtype,
              detailType: this.bottomControl.detailType,
              constantFilterParam: this.bottomControl.constantFilterParam
          }];
      }
      
      Ext.each(this.bottomTabs, function (item) {
          var grids = this.down(item.xtype).query('basicgrid');
          if (grids.length == 0) {
              grids = this.down(item.xtype);
          }
          Ext.each(grids, function (bgrid) {
              bgrid.down('grid').on('itemclick', function (grid, record) {
                  me.fireEvent('showDetails', bgrid.detailType, record);
              }, this);
          }, this);
      }, this);

      if (this.subBottomTabs) {
          Ext.each(bottomPanel.query('grid'), function (grid) {
              grid.on('itemclick', this.centerSelectionChanged, this);
          }, this);
          Ext.each(this.subBottomTabs, function (item) {
              var xitem = this.down(item.xtype),
                grid = null;
              if (xitem) {
                  grid = xitem.down('grid');
              }
              
              if (grid && (!xitem.isXType('basicgrid'))) {
                  grid.on('itemclick', function (grid, record) {
                      me.fireEvent('showDetails', item.detailType, record);
                  });
              }
          }, this);
      }

      bottomPanel.on('collapse', function () {
          this.gridSelection(this.down('basicgrid'));
      }, this);

      var onBottomPanelView = function() {
          var activeTab = bottomPanel.getActiveTab();
          activeTab.reloadGrid();
          this.gridSelection(activeTab);
      };

      bottomPanel.on('expand', onBottomPanelView, this);
      bottomPanel.on('float', onBottomPanelView, this);
      
      if (subBottomPanel) {
          subBottomPanel.on('collapse', function () {
              this.gridSelection(this.down('basicgrid'));
          }, this);

          var onSubBottomPanelView = function () {
              var activeTab = subBottomPanel.getActiveTab();
              activeTab.reloadGrid();
              this.gridSelection(activeTab);
          };

          subBottomPanel.on('expand', onSubBottomPanelView, this);
          subBottomPanel.on('float', onSubBottomPanelView, this);
      }

      Ext.each(this.query('tabpanel'), function (tabpanel) {
          tabpanel.on('tabchange', function (panel, newTab) {
              var grid = newTab.down('basicgrid');
              if (newTab.getActionColumns) {
                  grid = newTab;
              }

              if (typeof grid.reloadGrid === "function") {
                  grid.reloadGrid();
              }
              this.gridSelection(grid);
          }, this);
      }, this);
  },

  gridSelection: function (grid) {
    if (grid) {
        var sel = grid.down('grid').getSelectionModel().getSelection(),
            model = {};
        if (sel.length) {
            model = sel[0];
        }
        if (grid.detailType) {
            this.fireEvent('showDetails', grid.detailType, model);
        }
    }
  },

  topSelectionChanged: function (grid, record) {
      var me = this;
      // if (selected.length == 0) return;
      this.topRecord = record;
      // Пока убрали, т.к. дублируется при табах
      //this.fireEvent('showDetails', this.topDetailType, record);
      var bottomPanel = this.down('*[role=bottom-panel]');

      var cFilter = null;
      Ext.each(bottomPanel.query('basicgrid'), function (item) {
          // var control = this.down(item.xtype);
          var control = item;
          if (control) {
              if (item.constantFilterParam) {
                  if (cFilter == null) {
                      cFilter = {
                          property: item.constantFilterParam,
                          value: record.get(me.childFilterColumn) || record.get('id')
                      };
                  }
                  control.constantFilter = [{
                      property: item.constantFilterParam,
                      value: record.get(me.childFilterColumn) || record.get('id')
                  }];

                  if (!bottomPanel.collapsed) {
                      control.reloadGrid();
                  }
              }
          }
      }, this);
      // this.centerSelectionChanged(grid, record, cFilter);
      this.loadSubBottom(grid, record, cFilter);
  },

  loadSubBottom: function (grid, record, cFilter) {
      var me = this;
      var subBottomTabPanel = this.down('*[role=sub-bottom-panel]');

      if (subBottomTabPanel == null) {
          return;
      }

      var currentControl = grid.up('basicgrid');
      var activeTab = subBottomTabPanel.getActiveTab();

      Ext.each(this.subBottomTabs, function (item) {
          var control = this.down(item.xtype);
          if (control) {
              if (cFilter) {
                  control.constantFilter = [cFilter];
              }
              else {
                  if (currentControl.childrenConstantFilterParam) { //КАкой фильтр использовать - родительский или нет
                      control.constantFilter = [{
                          property: currentControl.childrenConstantFilterParam,
                          value: record.get(me.childFilterColumn) || record.get('id')
                      }];
                  }
                  else {
                      if (item.constantFilterParam) {
                          control.constantFilter = [{
                              property: item.constantFilterParam,
                              value: record.get(me.childFilterColumn) || record.get('id')
                          }];
                      }
                  }
              }

              if (control === activeTab && !subBottomTabPanel.collapsed) {
                  control.reloadGrid();
              }
          }
      }, this);
  },

  centerSelectionChanged: function (grid, record) {
    this.topRecord = record;
    this.loadSubBottom(grid, record);
  }
});