Ext.define('Cement.view.basic.HasImg', {
	setupImgCentering: function () {
		var fs = this.query('fieldset');
		if (fs[1]) {
			fs[1].expand();
			fs[1].collapse();
		}
		this.on('resize', function (obj, width, height) {
			var images = this.query('image');
			if (images) {
				Ext.each(images, function (img) {
					if (img) {
						var panel = img.up('panel'),
						pos = (panel.getWidth() - img.getWidth()) / 2 - 16;
						img.setPosition(pos);
					}
				});
			}
		}, this);
	}
});