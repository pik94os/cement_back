Ext.define('Cement.view.basic.SimpleCreatorWindow', {
  extend: 'Ext.window.Window',
  modal: true,
  width: 400,
  height: 125,
  resizable: false,
  layout: 'fit',

  createUrl: Cement.Config.url.corporate.employes.create_department,
  title: 'Создать отдел',
  comboName: 'p_department',
  comboLabel: 'Отдел',
  parent: null,
  storeId: 'corporate.auxiliary.Departments',

  initComponent: function () {
    Ext.apply(this, {
      items: [
        {
          xtype: 'panel',
          bodyPadding: 10,
          border: 0,
          layout: 'fit',
          items: [
            {
              xtype: 'panel',
              layout: 'anchor',
              bodyPadding: 10,
              items: [
                {
                  xtype: 'textfield',
                  fieldLabel: this.comboLabel,
                  labelWidth: 120,
                  anchor: '100%'
                }
              ]
            }
          ]
        }
      ],
      bbar: [
        {
          text: 'Сохранить',
          action: 'save'
        },
        {
          text: 'Отмена',
          action: 'cancel'
        }
      ]
    });
    this.callParent(arguments);
    var me = this;
    me.down('button[action=cancel]').on('click', function () {
      me.close();
    });
    me.down('button[action=save]').on('click', function () {
      var value = me.down('textfield').getValue();
      if (value != '') {
        Ext.Ajax.request({
          url: me.createUrl,
          method: 'POST',
          params: {
            p_name: value
          },
          success: function (response) {
            var data = Ext.decode(response.responseText);
            if (data.success) {
                Ext.getStore(me.storeId).add({
                  p_name: value,
                  id: data.id
                });
                me.parent.down('combo[name=' + me.comboName + ']').setValue(data.id);
                me.close();
            }
            else {
                Cement.Msg.error(data.msg);
            }
          },
          failure: function () {
            Cement.Msg.error('Ошибка при создании');
          }
        });
      }
    }, this);
  }
});