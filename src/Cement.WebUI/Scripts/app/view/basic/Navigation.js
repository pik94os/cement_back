Ext.define("Cement.view.basic.Navigation", {
    extend: 'Ext.panel.Panel',
    alias: 'widget.navigation',

    initComponent: function () {
        Ext.apply(this, {
            id: 'navigation',
            cls: 'main-nav',
            width: 200,
            minWidth: 150,
            maxWidth: 400,
            split: true,
            collapsible: true,
            layout:  'accordion'
        });
        this.callParent(arguments);
    }
});