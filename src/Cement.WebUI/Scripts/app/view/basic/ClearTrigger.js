Ext.define('Cement.view.basic.ClearTrigger', {
    extend: 'Ext.form.field.Trigger',
    alias: 'widget.cleartrigger',
    initComponent: function () {
        var me = this;
        me.trigger1Cls = 'x-form-clear-trigger';
        me.trigger2Cls = 'x-form-search-trigger';
        me.callParent(arguments);
    },

    onTrigger1Click: function() {
        this.setRawValue('');
    }
});