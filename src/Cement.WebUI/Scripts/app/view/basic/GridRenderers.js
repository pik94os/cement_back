Ext.define('Cement.view.basic.GridRenderers', {
    percentRenderer: function (value) {
        if (value < 0) {
            return '<span class="red">' + value + '%</span>';
        }
        else {
            return '<span class="green">+' + value + '%</span>';
        }
    },

    checkBoxRenderer: function (value) {
        return "<input onclick='return false;' type='checkbox' " + ((value == 1) ? "checked='checked'" : "") + ">";
    },

    radioButtonRenderer1: function(value) {
        return "<input onclick='javascript: return false;' type='radio' " + ((value == 1) ? "checked='checked'" : "") + ">";
    },

    radioButtonRenderer2: function(value) {
        return "<input onclick='javascript: return false;' onclick='return;' type='radio' " + ((value == 2) ? "checked='checked'" : "") + ">";
    },

    radioButtonRenderer3: function(value) {
        return "<input onclick='javascript: return false;' onclick='return;' type='radio' " + ((value == 3) ? "checked='checked'" : "") + ">";
    }
});