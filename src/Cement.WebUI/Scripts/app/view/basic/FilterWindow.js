Ext.define('Cement.view.basic.FilterWindow', {
	extend: 'Ext.window.Window',
	width: 550,
	height: 280,
	layout: 'fit',
	title: 'Сортировка',
	fieldsCount: 6,
	modal: true,
	resizable: false,

	sort_fields: [{
		name: 'p_field_1',
		label: 'Поле 1'
	}, {
		name: 'p_field_2',
		label: 'Поле 2'
	}, {
		name: 'p_field_3',
		label: 'Поле 3'
	}, {
		name: 'p_field_4',
		label: 'Поле 4'
	}, {
		name: 'p_field_5',
		label: 'Поле 5'
	}, {
		name: 'p_field_6',
		label: 'Поле 6'
	}],

	initComponent: function () {
		var items = [{
			width: 20,
			xtype: 'container'
		}, {
			width: 200,
			xtype: 'container',
			html: 'Столбец',
			padding: '0 0 5 10'
		}, {
			width: 140,
			xtype: 'container',
			html: 'Фильтры',
			padding: '0 0 5 10'
		}, {
			width: 140,
			xtype: 'container',
			html: 'Выражение',
			padding: '0 0 5 10'
		}],
			filtersStore = Ext.create('Ext.data.Store', {
				fields: ['id', 'name', 'code'],
				data: [
					{ id: 1, name: 'равно', code: 'eql' },
					{ id: 2, name: 'не равно', code: 'not_eql' },
					{ id: 3, name: 'начинается с', code: 'starts_with' },
					{ id: 4, name: 'заканчивается на', code: 'ends_with' },
					{ id: 5, name: 'содержит', code: 'contains' },
					{ id: 6, name: 'не содержит', code: 'not_contains' },
					{ id: 7, name: 'больше', code: 'much' },
					{ id: 8, name: 'больше или равно', code: 'much_or_eql' },
					{ id: 9, name: 'меньше', code: 'less' },
					{ id: 10, name: 'меньше или равно', code: 'less_or_eql' },
					{ id: 11, name: 'между', code: 'between' },
					{ id: 12, name: 'первые десять', code: 'first_ten' },
					{ id: 13, name: 'выше среднего', code: 'much_middle' },
					{ id: 14, name: 'ниже среднего', code: 'less_middle' }
				]
			}),
			fieldsStore = Ext.create('Ext.data.Store', {
				fields: ['name', 'label'],
				data: this.sort_fields
			});
		for (var i = 1; i < this.fieldsCount + 1; i++) {
			items.push({
				xtype: 'container',
				html: i
			}, {
				xtype: 'combo',
				width: 180,
				margin: '3 10',
				queryMode: 'local',
				displayField: 'label',
				valueField: 'name',
				editable: false,
				store: fieldsStore,
				role: 'field_' + i + '_name'
			}, {
				xtype: 'combo',
				width: 130,
				margin: '3 10',
				queryMode: 'local',
				displayField: 'name',
				valueField: 'code',
				editable: false,
				store: filtersStore,
				role: 'field_' + i + '_filter'
			}, {
				xtype: 'textfield',
				width: 130,
				margin: '3 10',
				role: 'field_' + i + '_value'
			});
		}
		Ext.apply(this, {
			items: [{
				xtype: 'panel',
				border: 0,
				bodyPadding: 10,
				layout: {
					type: 'table',
					columns: 4
				},
				items: items
			}],
			bbar: [{
				xtype: 'button',
				text: 'Сохранить',
				action: 'save'
			}, {
				xtype: 'button',
				text: 'Отмена',
				action: 'cancel'
			}]
		});
		this.callParent(arguments);
		this.addEvents('filter_set');
		this.down('button[action=save]').on('click', function () {
			var filters = [];
			for (i = 1; i < this.fieldsCount + 1; i++) {
				var comboVal = this.down('combo[role=field_' + i + '_name]').getValue();
				if (comboVal) {
					var filt = this.down('combo[role=field_' + i + '_filter]').getValue(),
						val = this.down('textfield[role=field_' + i + '_value]').getValue();
					if (filt) {
						filters.push({
							property: comboVal + '_' + filt,
							value: val
						});
					}
				}
			}
			this.fireEvent('filter_set', filters);
		}, this);
		this.down('button[action=cancel]').on('click', function () {
			this.hide();
		}, this);
		this.on('beforeclose', function () {
			this.hide();
			return false;
		});
	}
});