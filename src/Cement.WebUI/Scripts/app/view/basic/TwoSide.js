Ext.define('Cement.view.basic.TwoSide', {
	extend: 'Ext.panel.Panel',
    alias: 'widget.twoside',
    enableDetail: false,
    layout: 'border',
    cls: 'two-side',
    defaults: {
        split: true
    },

    leftControl: {
        xtype: 'panel',
        html: 'left'
    },
    rightControl: {
        xtype: 'panel',
        html: 'right'
    },

    initComponent: function () {
    	var me = this;
        this.leftControl.region = 'center';
        this.rightControl.region = 'south';
        this.leftControl.border = 0;
        this.rightControl.border = 0;
        // this.leftControl.width = '50%';
        // this.rightControl.width = '50%';
        this.leftControl.cls += ' left-panel';
        this.rightControl.cls += ' right-panel';
        this.leftControl.role = 'leftControl';
        this.rightControl.role = 'rightControl';
        this.leftControl.height = '50%';
        this.rightControl.height = '50%';
        this.rightControl.cls = 'bottom-collapsible-header no-side-borders bottom-grids';
        this.rightControl.collapsedCls = 'collapsed-with-bottom-border no-side-borders';
        this.rightControl.collapsed = true;
        Ext.apply(this, {
            items: [
                this.leftControl,
                this.rightControl
            ]
        });
    	this.callParent(arguments);
    	this.addEvents('showDetails');
        this.down('*[role=leftControl] grid').on('itemclick', function (g, record) {
            this.down('*[role=rightControl]').enable();
        }, this);

        this.down('*[role=rightControl]').on('linkitem', function (rec) {
            var sel = this.down('*[role=leftControl] grid').getSelectionModel().getSelection(),
                me = this;
            // me.linkUrl = '/server/link_items.json?id1={0}&id2={1}';
            if (sel.length) {
                Ext.Msg.confirm('Внимание', 'Вы действительно хотите связать выбранные элементы?', function (btn) {
                    if (btn == 'yes') {
                        Ext.Ajax.request({
                            url: Ext.String.format(me.linkUrl, sel[0].get('id'), rec.get('id')),
                            success: function (response) {
                                var json = Ext.JSON.decode(response.responseText);
                                if (json.success) {
                                    me.down('*[role=leftControl] grid').getStore().load();
                                    me.down('*[role=rightControl] grid').getStore().load();
                                    Cement.Msg.info(json.msg);
                                }
                                else {
                                    Cement.Msg.error(json.msg);
                                }
                            },
                            failure: function () {
                                Cement.Msg.error('Ошибка при связывании');
                            }
                        });
                    }
                });
            }
        }, this);
    }
});