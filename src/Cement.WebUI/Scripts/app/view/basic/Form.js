Ext.define('Cement.view.basic.Form', {
	extend: 'Ext.form.Panel',
	alias: 'widget.basicform',
  bodyCls: 'model-form',
  fieldsDisabled: true,
  validationsActive: true,
  layout: 'autocontainer',
  // autoScroll: true,
  overflowY: 'hidden',
  overflowX: 'hidden',
  width: 450,
  url: '/bank_accounts.json',
  printUrl: '',
  loadItemProperty: 'id',
  loadItemModel: null,
  getNewNumberUrl: null,
  newNumberFieldSelector: 'textfield[name=p_number]',
  // method: 'POST',
  border: 0,
  noUserFieldsetsControl: true,
  notCollapseFieldsetIndex: 0,
  allowBlankFields: false,
  saveButtonDisabled: true,
  prevNextHidden: true,
  showSaveAndSignButton: false,
  disabledFields: {},

  mixins: [
    'Cement.view.basic.HasImg',
    'Cement.view.basic.Combo'
  ],

  getHTMLxtype: function () {
      return 'textarea';
  },

  getCheckBoxField: function (name, label) {
    return {
      xtype: 'checkbox',
      name: name,
      inputValue: 1,
      boxLabel: label,
      disabled: this.fieldsDisabled,
      padding: '5 110 5 0'
    };
  },

  getBbar: function () {
      return [
          {
      xtype: 'button',
      text: 'Сохранить без валидации',
      iconCls: 'icon-save-item',
      action: 'form_save_no_valid'
    },
    {
      text: 'Сохранить',
      iconCls: 'icon-save-item',
      disabled: this.saveButtonDisabled,
      action: 'form_save',
      menu: [{
        text: 'Сохранить и остаться',
        iconCls: 'icon-save-item',
        action: 'form_save'
      }, {
        text: 'Сохранить и перейти',
        iconCls: 'icon-save-item',
        action: 'form_save_and_return'
      },
      //{
      //  text: 'Сохранить и согласовать',
      //  iconCls: 'icon-save-item',
      //  action: 'form_save_and_sign',
      //  hidden: !this.showSaveAndSignButton
      //}
      ]
    }, {
      text: 'Отмена',
      action: 'form_cancel',
      iconCls: 'icon-cancel'
    }];
  },

  // Splits arr into count parts, govnocode (c) algorithm
  splitArray: function (arr, count) {
    count = (count === undefined) ? 2 : count;
    var result = [],
      arr2 = [],
      i = 0,
      arrlen = Math.round(arr.length / count);
    Ext.each(arr, function (itm) {
        if (i < arrlen) {
          arr2.push(itm);
        }
        else {
          i = 0;
          result.push(arr2);
          arr2 = [];
          arr2.push(itm);
        }
        i++;
    });
    if (arr2.length > 0) {
      result.push(arr2);
    }
    return result;
  },

  getMovers: function () {
    return [{
      margin: '114 0 4 0',
      iconCls: 'icon-move-top',
      action: 'make_first'
    }, {
      margin: '0 0 4 0',
      iconCls: 'icon-move-up',
      action: 'move_up'
    },  {
      margin: '0 0 4 0',
      iconCls: 'icon-move-right',
      action: 'move_right'
    }, {
      margin: '0 0 4 0',
      iconCls: 'icon-move-left',
      action: 'move_left'
    }, {
      margin: '0 0 4 0',
      iconCls: 'icon-move-down',
      action: 'move_down'
    }, {
      iconCls: 'icon-move-bottom',
      action: 'make_last'
    }];
  },

  //Get Panel items with two columns
  getTwoColumns: function (title, items1, items2, hide_title, align) {
    align = (align === undefined) ? 'left' : align;
    var result = {
      xtype: 'fieldset',
      title: title,
      collapsible: true,
      layout: {
        type: 'column',
        align: 'stretch',
        padding: 0
      },
      items: [{
        xtype: 'container',
        padding: '0 10 0 0',
        layout: 'anchor',
        columnWidth: 0.5,
        border: 0,
        anchor: '100%',
        defaults: {
          labelAlign: align,
          xtype: 'textfield',
          anchor: '100%'
        },
        items: items1
      }, {
        xtype: 'container',
        layout: 'anchor',
        columnWidth: 0.5,
        border: 0,
        anchor: '100%',
        padding: '0 0 0 10',
        defaults: {
          labelAlign: align,
          xtype: 'textfield',
          anchor: '100%'
        },
        items: items2
      }]
    };
    if (hide_title) {
      result.collapsible = false;
      result.title = null;
      result.border = 0;
    }
    return result;
  },

  getSelectorField: function (label, name, action, labelWidth, labelAlign) {
    var lw = (labelWidth === undefined) ? 200 : labelWidth;
    return {
      xtype: 'fieldcontainer',
      layout: 'hbox',
      role: 'container_' + name,
      items:[{
        xtype: 'hiddenfield',
        name: name
      }, {
          xtype: 'textfield',
          readOnly: true,
        labelWidth: lw,
        labelAlign: labelAlign || 'right',
        name: name + '_display',
        fieldLabel: label,
        flex: 1
      }, {
        xtype: 'button',
        text: 'Выбрать',
        margin: '0 0 0 4',
        action: action,
        width: 60
      }]
    };
  },

  setDragDropGrid: function (parent) {
    parent = (parent === undefined)? this : parent;
    parent.down('grid[role=source-grid]').getStore().load();
    parent.down('grid[role=dest-grid]').on('selectionchange', function (grid, selected) {
      if (parent.destSelected) {
        this.destSelected(grid, selected);
      }
    }, this);
    parent.down('button[action=make_first]').on('click', function () {
      this.moveToPos(0, parent);
    }, this);
    parent.down('button[action=make_last]').on('click', function () {
      this.moveToPos(parent.down('grid[role=dest-grid]').getStore().count() - 1, parent);
    }, this);
    parent.down('button[action=move_up]').on('click', function () {
      var grid = parent.down('grid[role=dest-grid]'),
        store = grid.getStore(),
        selection = grid.getSelectionModel().getSelection();
      if (selection) {
        if (selection.length > 0) {
          pos = store.indexOf(selection[0]);
          if (pos > 0) {
            this.moveToPos(pos - 1, parent);
          }
        }
      }
    }, this);
    parent.down('button[action=move_down]').on('click', function () {
      var grid = parent.down('grid[role=dest-grid]'),
        store = grid.getStore(),
        selection = grid.getSelectionModel().getSelection();
      if (selection) {
        if (selection.length > 0) {
          pos = store.indexOf(selection[0]);
          if (pos < store.count() - 1) {
            this.moveToPos(pos + 1, parent);
          }
        }
      }
    }, this);
    parent.down('button[action=move_right]').on('click', function () {
      var left_grid = parent.down('grid[role=dest-grid]'),
        right_grid = parent.down('grid[role=source-grid]'),
        left_store = left_grid.getStore(),
        right_store = right_grid.getStore(),
        selection = left_grid.getSelectionModel().getSelection();
      if (selection) {
        if (selection.length > 0) {
          var newModel = selection[0].copy();
          left_store.remove(selection[0]);
          right_store.add(newModel);
        }
      }
    }, this);
    parent.down('button[action=move_left]').on('click', function () {
      var left_grid = parent.down('grid[role=dest-grid]'),
        right_grid = parent.down('grid[role=source-grid]'),
        left_store = left_grid.getStore(),
        right_store = right_grid.getStore(),
        selection = right_grid.getSelectionModel().getSelection();
      if (selection) {
        if (selection.length > 0) {
          var newModel = selection[0].copy();
          right_store.remove(selection[0]);
          left_store.add(newModel);
        }
      }
    }, this);
  },

  //Move item in drag drop grid
  moveToPos: function (pos, parent) {
    var grid = parent.down('grid[role=dest-grid]'),
      store = grid.getStore(),
      selection = grid.getSelectionModel().getSelection();
    if (selection) {
      if (selection.length > 0) {
        var newModel = selection[0].copy();
        store.remove(selection[0]);
        store.insert(pos, newModel);
        grid.getSelectionModel().select(newModel);
      }
    }
  },

  getDateFieldXType: function () {
    var result = 'datefield';
    if (this.fieldsDisabled) {
      result = 'textfield';
    }
    return result;
  },

  getItems: function () {
    return [];
  },

  loadRecordInternal: function (rec) {
    if (rec.get) {
      this.getForm().loadRecord(rec);
      if (this.afterRecordLoad) {
        this.afterRecordLoad(rec);
      }
    }
    else {
      this.reset();
    }
  },

  getNavPanel: function () {
    return {
      xtype: 'panel',
      border: 0,
      cls: 'panel-center-aligned',
      html: 'Шаг 1 из 5'
    }
  },

  getFileField: function (label, name, margin) {
    if (this.isFilter) return null;
    var result = {
        name: name,
        fieldLabel: label,
        xtype: 'filefield',
        anchor: '100%',
        buttonText: 'Загрузить'
      };
    if (this.fieldsDisabled) {
      result = {
        xtype: 'downloadfield',
        name: name,
        fieldLabel: label
      }
    }
    if (margin) {
      result.margin = margin;
    }
    return result;
  },

  initComponent: function () {
      var me = this;
      //var printableField = {
      //    xtype: 'hiddenfield',
      //    name: 'p_is_printable_only'
      //},
      //totalItems = this.getItems();
      //totalItems.splice(0, 0, printableField);
      if (this.noUserFieldsetsControl) {

          var children = {
              xtype: 'panel',
              border: 0,
              items: this.getItems(),
              height: this.formHeight
          };

          if (children.items && children.items.length === 1 && this.layout === 'fit') {
              children.layout = 'fit';
          }

          Ext.apply(this, {
              bbar: this.getBbar(),
              items: [children]
          });
      }
      this.callParent(arguments);
      if (!this.fieldsDisabled && this.validationsActive) {
          this.on('validitychange', function (f, valid) {
              if (valid) {
                  this.down('button[action=form_save]').enable();
              }
              else {
                  this.down('button[action=form_save]').disable();
              }
          }, this);
      }

      if (this.down('button[action=form_save_no_valid]')) {
          this.down('button[action=form_save_no_valid]').on('click', function () {
              //var totalCreator = Ext.ComponentQuery.query('totalcreator')[0];

              var callback = function () {
                  //totalCreator.setLoading("Сохранение");
                  me.getForm().submit({
                      waitMsg: "Сохранение...",
                      clientValidation: false,
                      success: function () {
                          //totalCreator.setLoading(false);
                      },
                      failure: function (self, result) {
                          //totalCreator.setLoading(false);

                          var message = null;

                          try {
                              message = Ext.decode(result.response.responseText).Message;
                          } catch (e) {
                          }

                          Cement.Msg.error(message || 'Ошибка при сохранении информации');
                      }
                  });
              };

              if (me.beforeSubmit) {
                  me.beforeSubmit(function () {
                      callback();
                  });
              } else {
                  callback();
              }
              
          }, this);
      }
  },

  getPrintUrl: function () {
    var id = '',
    record = this.getForm().getRecord();
    if (record) {
      id = record.get('id');
    }
    return Ext.String.format(this.printUrl, id);
  },

  setupFieldsets: function () {
    var me = this;
    this.fieldsets = this.query('fieldset');
    this.currentFieldset = 0;
    Ext.each(this.fieldsets, function (f) {
      f.on('beforeexpand', function () {
        if (!me.enableExpand) {
          return false;
        }
      }, this);
      f.on('beforecollapse', function () {
        if (!me.enableExpand) {
          return false;
        }
      }, this);
    });
    if (me.down('panel[cls=panel-center-aligned]')) {
      me.down('panel[cls=panel-center-aligned]').body.setHTML(
        Ext.String.format(
          'Шаг {0} из {1}',
          me.currentFieldset + 1,
          me.fieldsets.length
          )
        );
    }
  },

  loadRecord: function (record) {
      var me = this;

      if (me.loadItemUrl) {

          if (record.get) {
              me.setLoading("Загрузка");
              Ext.Ajax.request({
                  url: Ext.String.format(me.loadItemUrl, record.get(me.loadItemProperty)),
                  method: 'GET',
                  success: function (response) {
                      var rec = Ext.create(me.loadItemModel || record.$className, Ext.JSON.decode(response.responseText));
                      me.setLoading(false);
                      me.loadRecordInternal(rec);
                  },
                  failure: function () {
                      me.setLoading(false);
                      Cement.Msg.error('Ошибка при загрузке элемента');

                  }
              });
          }
          else {
              me.reset();
          }
          
      } else {
          me.loadRecordInternal(record);
      }
  },

  reset: function() {
      this.getForm().reset();

      this.getNewNumber();
  },

  getNewNumber: function () {
      var me = this;

      if (me.getNewNumberUrl && me.newNumberFieldSelector) {
          Ext.Ajax.request({
              url: me.getNewNumberUrl,
              method: 'GET',
              success: function (response) {
                  if (response && response.responseText) {
                      me.down(me.newNumberFieldSelector).setValue(response.responseText.replace(/"/g, ''));
                  }
              }
          });
      }
  }
});