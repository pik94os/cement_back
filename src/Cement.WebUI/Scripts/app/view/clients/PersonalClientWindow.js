Ext.define('Cement.view.clients.PersonalClientWindow', {
    extend: 'Cement.view.clients.OrganizationClientWindow',
    xtype: 'clients_personal_client_window',
    structure_storeId: 'clients.PersonalClientsStructure'
});