Ext.define('Cement.view.clients.personal.Complex', {
    extend: 'Cement.view.basic.Complex',
    alias: 'widget.clients_personal_complex',
    border: 0,
    enableDetail: false,
    autoLoadStore: false,
    priceId: null,
    bodyPadding: 0,
    padding: 0,
    header: false,
    cls: 'no-side-borders',
    title: 'Персональный каталог',
    tabTitle: 'Персональный каталог',
    topXType: 'clients_personal_list_clients',
    topDetailType: 'Cement.view.clients.View',
    rowsCount: 3,
    childFilterColumn: 'p_client',
    bottomTitle: 'Связанные организации и лица',
    bottomTabs: [{
        title: 'Дочерние организации',
        shownTitle: 'Дочерние организации',
        xtype: 'clients_personal_list_firms',
        detailType: 'Cement.view.corporate.firm.View',
        constantFilterParam: 'p_client_current',
        childrenConstantFilterParam: 'p_clients_personal_connected_firm',
        border: 0
    }, {
        title: 'Связанные лица',
        shownTitle: 'Связанные лица',
        xtype: 'clients_personal_list_people',
        detailType: 'Cement.view.corporate.employee.View',
        constantFilterParam: 'p_client_current',
        childrenConstantFilterParam: 'p_client_personal_connected_person',
        border: 0
    }],
    subBottomTitle: 'Справочник',
    subBottomTabs: [{
        title: 'Реквизиты счетов',
        xtype: 'clients_personal_bottom_bank_accounts',
        detailType: 'Cement.view.corporate.bank_account.View'
    }, {
        title: 'Склады',
        xtype: 'clients_personal_bottom_warehouses',
        detailType: 'Cement.view.corporate.warehouse.View'
    }, {
        title: 'Сотрудники',
        xtype: 'clients_personal_bottom_employees',
        detailType: 'Cement.view.corporate.employee.View'
    }, {
        title: 'Подписи',
        xtype: 'clients_personal_bottom_signatures',
        detailType: 'Cement.view.corporate.signature.View'
    },
    {
        title: 'Товары',
        xtype: 'clients_bottom_products',
        detailType: 'Cement.view.products.products.View'
    },
    {
        title: 'Услуги',
        xtype: 'clients_bottom_services',
        detailType: 'Cement.view.products.services.View'
    },
    {
        title: 'ТЕ',
        xtype: 'clients_bottom_transportunits',
        detailType: 'Cement.view.transport.units.View'
    },
    {
        title: 'Внутренние документы',
        xtype: 'clients_bottom_internal_documents',
        detailType: 'Cement.view.office_work.documents.View'
    },
    {
        title: 'Корреспонденция',
        xtype: 'clients_bottom_correspondence',
        detailType: 'Cement.view.office_work.correspondention.View'
    },
    // , {
    //     title: 'ТЕ Действующие',
    //     xtype: 'clients_personal_bottom_transport_units',
    //     detailType: 'Cement.view.client.view.TransportUnit'
    // }, {
    //     title: 'Вагон',
    //     xtype: 'clients_personal_bottom_carriages',
    //     detailType: 'Cement.view.transport.view.Carriage'
    // }, {
    //     title: 'Товары',
    //     xtype: 'clients_personal_bottom_products',
    //     detailType: 'Cement.view.product.View'
    // }, {
    //     title: 'Услуги',
    //     xtype: 'clients_personal_bottom_services',
    //     detailType: 'Cement.view.service.View'
    // },
    ],

    initComponent: function () {
        Ext.apply(this, {
            title: 'Клиенты действующие'
        });
        this.callParent(arguments);
    }
});