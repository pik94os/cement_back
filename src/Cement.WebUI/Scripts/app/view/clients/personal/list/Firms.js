Ext.define('Cement.view.clients.personal.list.Firms', {
    extend: 'Cement.view.clients.current.list.Firms',
    alias: 'widget.clients_personal_list_firms',

    gridStore: 'clients.personal.Firms',
    gridStateId: 'stateClientsPersonalFirms'
});