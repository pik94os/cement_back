Ext.define('Cement.view.clients.personal.list.People', {
    extend: 'Cement.view.clients.current.list.People',
    alias: 'widget.clients_personal_list_people',

    gridStore: 'clients.personal.People',
    gridStateId: 'stateClientsPersonalPeople'
});