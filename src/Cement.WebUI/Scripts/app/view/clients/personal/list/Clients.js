Ext.define('Cement.view.clients.personal.list.Clients', {
	extend: 'Cement.view.clients.corporate.list.Clients',
	alias: 'widget.clients_personal_list_clients',

	gridStore: 'clients.Personal',
	gridStateId: 'stateClientsPersonal',

	printUrl: Cement.Config.url.clients.personal.printGrid,
	helpUrl: Cement.Config.url.clients.personal.help,
    printItemUrl: Cement.Config.url.clients.personal.printItem,

    getActionColumns: function () {
        return {
            xtype: 'rowactions',
            hideable: false,
            resizeable: false,
            locked: true,
            width: 46,
            actions: [
                {
                    iconCls: 'icon-client-event-add',
                    qtip: 'Создать событие',
                    callback: this.createEvent
                },
                {
                iconCls: 'icon-edit-item',
                qtip: 'Редактировать',
                callback: this.editItem
            }, {
                iconCls: 'icon-archive-item',
                qtip: 'В архив',
                callback: this.archiveItem
            }],
            keepSelection: true
        };
    },

    createEvent: function (grid, record) {
        var bacisGrid = grid.up('basicgrid');
        
        var rec = Ext.create('Cement.model.clients.Management');

        rec.set('p_personal_client', record.get('id'));
        rec.set('p_personal_client_display', record.get('p_firm_name'));

        bacisGrid.fireEvent('edititem', rec);
    },
});