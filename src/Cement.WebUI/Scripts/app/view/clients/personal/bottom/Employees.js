Ext.define('Cement.view.clients.personal.bottom.Employees', {
	extend: 'Cement.view.corporate.employee.List',
	gridStore: 'clients.personal.bottom.Employees',
	gridStateId: 'stateClientpersonalEmployeesList',
	xtype: 'clients_personal_bottom_employees',
	tbar: null,
	showBottomBar: false,
	autoLoadStore: false,
	cls: 'nested-form',
	getToolbarItems: function () { return null; },
	getGridFeatures: function () { return null },
	getActionColumns: function () { return null; }
});