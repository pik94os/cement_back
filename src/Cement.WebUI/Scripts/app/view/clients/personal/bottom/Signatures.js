Ext.define('Cement.view.clients.personal.bottom.Signatures', {
	extend: 'Cement.view.corporate.signature.List',
	gridStore: 'clients.personal.bottom.Signatures',
	gridStateId: 'stateClientpersonalSignaturesList',
	xtype: 'clients_personal_bottom_signatures',
	tbar: null,
	showBottomBar: false,
	autoLoadStore: false,
	cls: 'nested-form',
	getToolbarItems: function () { return null; },
	getGridFeatures: function () { return null },
	getActionColumns: function () { return null; }
});