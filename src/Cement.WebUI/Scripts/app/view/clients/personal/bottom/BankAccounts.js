Ext.define('Cement.view.clients.personal.bottom.BankAccounts', {
	extend: 'Cement.view.corporate.bank_account.List',
	gridStore: 'clients.personal.bottom.BankAccounts',
	gridStateId: 'stateClientpersonalBankAccountsList',
	xtype: 'clients_personal_bottom_bank_accounts',
	tbar: null,
	showBottomBar: false,
	cls: 'nested-form',
	autoLoadStore: false,
	getToolbarItems: function () { return null; },
	getGridFeatures: function () { return null },
	getActionColumns: function () { return null; }
});