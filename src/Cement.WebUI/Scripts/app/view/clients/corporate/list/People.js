Ext.define('Cement.view.clients.corporate.list.People', {
    extend: 'Cement.view.clients.current.list.People',
    alias: 'widget.clients_corporate_list_people',

    gridStore: 'clients.corporate.People',
    gridStateId: 'stateClientsCorporatePeople'
});