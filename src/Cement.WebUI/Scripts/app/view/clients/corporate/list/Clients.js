Ext.define('Cement.view.clients.corporate.list.Clients', {
	extend: 'Cement.view.basic.Grid',
	alias: 'widget.clients_corporate_list_clients',

	gridStore: 'clients.Corporate',
	gridStateId: 'stateClientsCorporate',

	printUrl: Cement.Config.url.clients.corporate.printGrid,
	helpUrl: Cement.Config.url.clients.corporate.help,
    deleteUrl: Cement.Config.url.clients.corporate.deleteUrl,
    printItemUrl: Cement.Config.url.clients.corporate.printItem,
    archiveItemUrl: Cement.Config.url.clients.corporate.archiveItem,

    bbarText: 'Показаны клиенты {0} - {1} из {2}',
    bbarEmptyMsg: 'Нет клиентов',
    bbarUsersText: 'Клиентов на странице: ',

    shownTitle: null,
    showCreateButton: false,
    cls: 'no-border-bottom',

    creatorTree: Ext.clone(Cement.Creators.clients),
    createWindowTitle: 'Создать',

    getActionColumns: function () {
        return {
            xtype: 'rowactions',
            hideable: false,
            resizeable: false,
            locked: true,
            width: 46,
            actions: [{
                iconCls: 'icon-edit-item',
                qtip: 'Редактировать',
                callback: this.editItem
            }, {
                iconCls: 'icon-archive-item',
                qtip: 'В архив',
                callback: this.archiveItem
            }],
            keepSelection: true
        };
    },

    getFilterItems: function () {
        return [{
            text: 'Группа',
            kind: 'selector',
            field_name: 'p_group',
            checked: true
        }, {
            text: 'Подгруппа',
            kind: 'selector',
            field_name: 'p_sub_group',
            checked: true
        }, {
            text: 'Наименование',
            kind: 'selector',
            field_name: 'p_firm_name',
            checked: true
        }, {
            text: 'Адрес',
            kind: 'selector',
            field_name: 'p_address',
            checked: true
        }, {
            text: 'КПП',
            kind: 'selector',
            field_name: 'p_reg_kpp',
            checked: true
        }, {
            text: 'ИНН',
            kind: 'selector',
            field_name: 'p_reg_inn',
            checked: true
        }, {
            text: 'ОГРН',
            kind: 'selector',
            field_name: 'p_reg_ogrn',
            checked: true
        }, {
            text: 'Куратор',
            kind: 'selector',
            field_name: 'p_curator',
            checked: true
        }, {
            text: 'Статус',
            kind: 'selector',
            field_name: 'p_status',
            checked: true
        }, {
            text: 'Дата первого контакта',
            kind: 'selector',
            field_name: 'p_first_contact',
            checked: true
        }, {
            text: 'Дата последнего контакта',
            kind: 'selector',
            field_name: 'p_last_contact',
            checked: true
        }];
    },

    getGridColumns: function () {
        var result = [
            { text: 'Группа', dataIndex: 'p_group_display', width: 100, locked: true },
            { text: 'Подгруппа', dataIndex: 'p_sub_group_display', width: 100, locked: true },
            { text: 'Наименование', dataIndex: 'p_firm_name', width: 200, locked: true },
            { text: 'Адрес', dataIndex: 'p_address_display', width: 250 },
            { text: 'ИНН', dataIndex: 'p_reg_inn', width: 150 },
            { text: 'КПП', dataIndex: 'p_reg_kpp', width: 150 },
            { text: 'ОГРН', dataIndex: 'p_reg_ogrn', width: 150 },
            { text: 'Куратор', dataIndex: 'p_curator_display', width: 150 },
            { text: 'Статус', dataIndex: 'p_status_display', width: 150 },
            { text: 'Дата первого контакта', dataIndex: 'p_first_contact', width: 150 },
            { text: 'Дата последнего контакта', dataIndex: 'p_last_contact', width: 150 }
        ];
        return this.mergeActions(result);
    },

    initComponent: function () {
        Ext.apply(this, {
            title: this.shownTitle
        });
        this.callParent(arguments);
    }
});