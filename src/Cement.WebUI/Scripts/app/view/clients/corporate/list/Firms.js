Ext.define('Cement.view.clients.corporate.list.Firms', {
    extend: 'Cement.view.clients.current.list.Firms',
    alias: 'widget.clients_corporate_list_firms',

    gridStore: 'clients.corporate.Firms',
    gridStateId: 'stateClientsCorporateFirms'
});