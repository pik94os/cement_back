Ext.define('Cement.view.clients.corporate.bottom.Signatures', {
	extend: 'Cement.view.corporate.signature.List',
	gridStore: 'clients.corporate.bottom.Signatures',
	gridStateId: 'stateClientCorporateSignaturesList',
	xtype: 'clients_corporate_bottom_signatures',
	tbar: null,
	showBottomBar: false,
	autoLoadStore: false,
	cls: 'nested-form',
	getToolbarItems: function () { return null; },
	getGridFeatures: function () { return null },
	getActionColumns: function () { return null; }
});