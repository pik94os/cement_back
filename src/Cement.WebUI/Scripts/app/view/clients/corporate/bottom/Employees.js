Ext.define('Cement.view.clients.corporate.bottom.Employees', {
	extend: 'Cement.view.corporate.employee.List',
	gridStore: 'clients.corporate.bottom.Employees',
	gridStateId: 'stateClientCorporateEmployeesList',
	xtype: 'clients_corporate_bottom_employees',
	tbar: null,
	showBottomBar: false,
	autoLoadStore: false,
	cls: 'nested-form',
	getToolbarItems: function () { return null; },
	getGridFeatures: function () { return null },
	getActionColumns: function () { return null; }
});