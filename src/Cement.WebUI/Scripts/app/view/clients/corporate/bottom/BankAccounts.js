Ext.define('Cement.view.clients.corporate.bottom.BankAccounts', {
	extend: 'Cement.view.corporate.bank_account.List',
	gridStore: 'clients.corporate.bottom.BankAccounts',
	gridStateId: 'stateClientCorporateBankAccountsList',
	xtype: 'clients_corporate_bottom_bank_accounts',
	tbar: null,
	showBottomBar: false,
	cls: 'nested-form',
	autoLoadStore: false,
	getToolbarItems: function () { return null; },
	getGridFeatures: function () { return null },
	getActionColumns: function () { return null; }
});