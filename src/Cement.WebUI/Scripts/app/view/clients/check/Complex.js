Ext.define('Cement.view.clients.check.Complex', {
	extend: 'Cement.view.basic.Complex',
	alias: 'widget.clients_check_complex',
    border: 0,
    enableDetail: false,
    autoLoadStore: false,
    priceId: null,
    bodyPadding: 0,
    padding: 0,
    header: false,
    cls: 'no-side-borders simple-form clients',
    bodyCls: 'clients-checker',
    title: 'Клиенты проверка',
    tabTitle: 'Клиенты проверка',
    topXType: 'clients_check_checker',
    topDetailType: 'Cement.view.clients.View',
    rowsCount: 3,
    bottomTitle: 'Связанные организации и лица',
    bottomTabs: [{
        title: 'Дочерние организации',
        shownTitle: 'Дочерние организации',
        xtype: 'clients_check_list_firms',
        detailType: 'Cement.view.corporate.firm.View',
        constantFilterParam: 'p_client_check',
        childrenConstantFilterParam: 'p_client_check_connected_firm',
        border: 0
    }, {
        title: 'Связанные лица',
        shownTitle: 'Связанные лица',
        xtype: 'clients_check_list_people',
        detailType: 'Cement.view.corporate.employee.View',
        constantFilterParam: 'p_client_check',
        childrenConstantFilterParam: 'p_client_check_connected_person',
        border: 0
    }],
    subBottomTitle: 'Справочник',
    subBottomTabs: [{
        title: 'Реквизиты счетов',
        xtype: 'clients_check_bottom_bank_accounts',
        detailType: 'Cement.view.corporate.bank_account.View'
    }, {
        title: 'Склады',
        xtype: 'clients_check_bottom_warehouses',
        detailType: 'Cement.view.corporate.warehouse.View'
    }, {
        title: 'Сотрудники',
        xtype: 'clients_check_bottom_employees',
        detailType: 'Cement.view.corporate.employee.View'
    }, {
        title: 'Подписи',
        xtype: 'clients_check_bottom_signatures',
        detailType: 'Cement.view.corporate.signature.View'
    },
    // , {
    //     title: 'ТЕ Действующие',
    //     xtype: 'client_check_bottom_transport_units',
    //     detailType: 'Cement.view.client.view.TransportUnit'
    // }, {
    //     title: 'Вагон',
    //     xtype: 'client_check_bottom_carriages',
    //     detailType: 'Cement.view.transport.view.Carriage'
    // }, {
    //     title: 'Товары',
    //     xtype: 'client_check_bottom_products',
    //     detailType: 'Cement.view.product.View'
    // }, {
    //     title: 'Услуги',
    //     xtype: 'client_check_bottom_services',
    //     detailType: 'Cement.view.service.View'
    // },
    {
        title: 'Задачи',
        xtype: 'clients_check_bottom_tasks',
        detailType: 'Cement.view.clients.TaskView'
    }],

    initComponent: function () {
    	Ext.apply(this, {
    		title: 'Клиенты проверка'
    	});
    	this.callParent(arguments);
    }
});