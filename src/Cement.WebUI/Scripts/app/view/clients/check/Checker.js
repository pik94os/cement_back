Ext.define('Cement.view.clients.check.Checker', {
	extend: 'Ext.panel.Panel',
	alias: 'widget.clients_check_checker',
	items: [{
		xtype: 'panel',
		border: 0,
		layout: {
			type: 'vbox',
			align : 'stretch',
			pack  : 'start'
		},
		items: [{
			xtype: 'form',
			role: 'check-form',
			margin: '12',
			height: 140,
			cls: 'model-form',
			border: 0,
			buttons: [{
				action: 'check-client',
				iconCls: 'icon-find-item',
				text: 'Найти'
			}],
			items: [{
				xtype: 'fieldset',
				defaults: {
					xtype: 'textfield',
					anchor: '100%',
					labelWidth: 110
				},
				title: 'Юридический адрес',
				collapsible: false,
				items: [{
					xtype: 'panel',
					layout: 'column',
					border: 0,
					items: [{
						xtype: 'panel',
						margin: '0 10 0 0',
						columnWidth: 0.5,
						layout: 'anchor',
						border: 0,
						defaults: {
							xtype: 'textfield',
							anchor: '100%',
							labelWidth: 110
						},
						items: [{
							fieldLabel: 'Наименование',
							name: 'p_name'
						}, {
							fieldLabel: 'Код организации (ИНН, ОГРН)',
							name: 'p_code'
						}]
					}, {
						xtype: 'panel',
						margin: '0 0 0 10',
						columnWidth: 0.5,
						border: 0,
						layout: 'anchor',
						defaults: {
							xtype: 'textfield',
							anchor: '100%',
							labelWidth: 110
						},
						items: [{
							xtype: 'textfield',
							fieldLabel: 'Регион',
							name: 'p_region'
						}, {
							xtype: 'textfield',
							fieldLabel: 'Руководитель',
							name: 'p_director'
						}]
					}]
				}, {
					fieldLabel: 'Адрес',
					name: 'p_address'
				}]
			}]
		}, {
			xtype: 'panel',
            layout: {
                type: 'hbox',
                align: 'stretch'
            },
            border: 0,
            flex: 1,
            items: [{
				flex: 1,
				border: 0,
				cls: 'top-border',
				xtype: 'clients_check_list_clients',
				layout: 'fit'
            }]
		}]
	}],

	initComponent: function () {
		this.callParent(arguments);
		this.down('button[action=check-client]').on('click', function () {
			var store = this.down('clients_check_list_clients').down('grid').getStore(),
				formValues = this.down('form[role=check-form]').getForm().getValues(),
				filter = [];
			if (store) {
				store.clearFilter(true);
			}
			for (var i in formValues) {
				if (formValues.hasOwnProperty(i)) {
					filter.push({
						property: i,
						value: formValues[i]
					});
				}
			}
			store.filter(filter);
		}, this);
	}
});