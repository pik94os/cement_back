Ext.define('Cement.view.clients.check.bottom.Employees', {
	extend: 'Cement.view.corporate.employee.List',
	gridStore: 'clients.check.bottom.Employees',
	gridStateId: 'stateClientCheckEmployeesList',
	xtype: 'clients_check_bottom_employees',
	tbar: null,
	autoLoadStore: false,
	cls: 'nested-form',
	getToolbarItems: function () { return null; },
	getGridFeatures: function () { return null },
	getActionColumns: function () { return null; }
});