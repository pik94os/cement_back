Ext.define('Cement.view.clients.check.bottom.Tasks', {
	extend: 'Cement.view.clients.current.bottom.Tasks',
	alias: 'widget.clients_check_bottom_tasks',
	gridStore: 'clients.check.bottom.Tasks',
	gridStateId: 'stateClientsCheckTasksList'
});