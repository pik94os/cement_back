Ext.define('Cement.view.clients.check.bottom.Signatures', {
	extend: 'Cement.view.corporate.signature.List',
	gridStore: 'clients.check.bottom.Signatures',
	gridStateId: 'stateClientCheckSignaturesList',
	xtype: 'clients_check_bottom_signatures',
	tbar: null,
	autoLoadStore: false,
	cls: 'nested-form',
	getToolbarItems: function () { return null; },
	getGridFeatures: function () { return null },
	getActionColumns: function () { return null; }
});