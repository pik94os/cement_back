Ext.define('Cement.view.clients.check.bottom.Warehouses', {
	extend: 'Cement.view.corporate.warehouse.List',
	gridStore: 'clients.check.bottom.Warehouses',
	gridStateId: 'stateClientCheckWarehousesList',
	xtype: 'clients_check_bottom_warehouses',
	tbar: null,
	autoLoadStore: false,
    cls: 'nested-form',
	getToolbarItems: function () { return null; },
    getGridFeatures: function () { return null },
	getGridColumns: function () {
        return [
            { text: '������������',  dataIndex: 'p_name', flex: 2 },
            { text: '�����', dataIndex: 'p_address', flex: 1 },
            { text: '��� �����������', dataIndex: 'p_firm_code', flex: 1 },
            { text: '�������', dataIndex: 'p_station_display', flex: 1 },
            { text: '��� �������', dataIndex: 'p_station_code', flex: 1 },
            { text: '������', dataIndex: 'p_road', flex: 1 }
        ];
    }
});