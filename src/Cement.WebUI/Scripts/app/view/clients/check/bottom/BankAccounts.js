Ext.define('Cement.view.clients.check.bottom.BankAccounts', {
	extend: 'Cement.view.corporate.bank_account.List',
	gridStore: 'clients.check.bottom.BankAccounts',
	gridStateId: 'stateClientCheckBankAccountsList',
	xtype: 'clients_check_bottom_bank_accounts',
	tbar: null,
	autoLoadStore: false,
	cls: 'nested-form',
	getToolbarItems: function () { return null; },
	getGridFeatures: function () { return null },
	getActionColumns: function () { return null; }
});