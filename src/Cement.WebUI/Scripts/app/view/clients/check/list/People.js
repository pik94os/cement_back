Ext.define('Cement.view.clients.check.list.People', {
	extend: 'Cement.view.clients.current.list.People',
	alias: 'widget.clients_check_list_people',

	gridStore: 'clients.check.People',
	gridStateId: 'stateClientsCheckPeople'
});