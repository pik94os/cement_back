Ext.define('Cement.view.clients.check.list.Firms', {
	extend: 'Cement.view.clients.current.list.Firms',
	alias: 'widget.clients_check_list_firms',

	gridStore: 'clients.check.Firms',
	gridStateId: 'stateClientsCheckFirms'
});