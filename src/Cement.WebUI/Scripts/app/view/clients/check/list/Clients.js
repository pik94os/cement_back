Ext.define('Cement.view.clients.check.list.Clients', {
	extend: 'Cement.view.clients.current.list.Clients',
	alias: 'widget.clients_check_list_clients',

	gridStore: 'clients.Check',
	gridStateId: 'stateClientsCheck',

	//printUrl: Cement.Config.url.clients.check.printGrid,
	//helpUrl: Cement.Config.url.clients.check.help,
    //deleteUrl: Cement.Config.url.clients.check.deleteUrl,
    //printItemUrl: Cement.Config.url.clients.check.printItem,
    autoLoadStore: false,
    cls: 'clients',

    getActionColumns: function () {
        return {
            xtype: 'rowactions',
            hideable: false,
            resizeable: false,
            locked: true,
            width: 46,
            actions: [{ 
                iconCls: 'icon-add-item', 
                qtip: 'Добавить', 
                callback: this.addItem
            }],
            keepSelection: true
        };
    },

    getToolbarItems: function () {
    	return null;
    },

    addItem: function (grid, record) {
        grid.up('basicgrid').fireEvent('edititem', record, 'Cement.model.Client');
    }
});