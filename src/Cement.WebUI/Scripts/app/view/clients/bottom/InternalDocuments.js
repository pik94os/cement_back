Ext.define('Cement.view.clients.bottom.InternalDocuments', {
    extend: 'Cement.view.office_work.documents.lists.New',
    gridStore: 'clients.bottom.InternalDocuments',
    gridStateful: false,
    //gridStateId: 'stateClientsBottomProducts',
    xtype: 'clients_bottom_internal_documents',
    tbar: null,
    showBottomBar: false,
    cls: 'nested-form',
    autoLoadStore: false,
    getToolbarItems: function () { return null; },
    getGridFeatures: function () { return null },
    getActionColumns: function () { return null; }
});