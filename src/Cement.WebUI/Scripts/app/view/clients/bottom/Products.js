Ext.define('Cement.view.clients.bottom.Products', {
    extend: 'Cement.view.products.products.lists.Moderation',
    gridStore: 'clients.bottom.Products',
    gridStateful: false,
    //gridStateId: 'stateClientsBottomProducts',
    xtype: 'clients_bottom_products',
    tbar: null,
    showBottomBar: false,
    cls: 'nested-form',
    autoLoadStore: false,
    getToolbarItems: function () { return null; },
    getGridFeatures: function () { return null },
    getActionColumns: function () { return null; }
});