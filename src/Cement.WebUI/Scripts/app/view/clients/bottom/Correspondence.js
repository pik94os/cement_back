Ext.define('Cement.view.clients.bottom.Correspondence', {
    extend: 'Cement.view.office_work.correspondention.lists.New',
    gridStore: 'clients.bottom.Correspondence',
    gridStateful: false,
    //gridStateId: 'stateClientsBottomProducts',
    xtype: 'clients_bottom_correspondence',
    tbar: null,
    showBottomBar: false,
    cls: 'nested-form',
    autoLoadStore: false,
    getToolbarItems: function () { return null; },
    getGridFeatures: function () { return null },
    getActionColumns: function () { return null; }
});