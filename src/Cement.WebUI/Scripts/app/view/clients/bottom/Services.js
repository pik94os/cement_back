Ext.define('Cement.view.clients.bottom.Services', {
    extend: 'Cement.view.products.services.lists.Moderation',
    gridStore: 'clients.bottom.Services',
    gridStateful: false,
    //gridStateId: 'stateClientsBottomProducts',
    xtype: 'clients_bottom_services',
    tbar: null,
    showBottomBar: false,
    cls: 'nested-form',
    autoLoadStore: false,
    getToolbarItems: function () { return null; },
    getGridFeatures: function () { return null },
    getActionColumns: function () { return null; }
});