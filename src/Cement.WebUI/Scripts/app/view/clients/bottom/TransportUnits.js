Ext.define('Cement.view.clients.bottom.TransportUnits', {
    extend: 'Cement.view.transport.units.lists.Owned',
    gridStore: 'clients.bottom.TransportUnits',
    gridStateful: false,
    //gridStateId: 'stateClientsBottomProducts',
    xtype: 'clients_bottom_transportunits',
    tbar: null,
    showBottomBar: false,
    cls: 'nested-form',
    autoLoadStore: false,
    getToolbarItems: function () { return null; },
    getGridFeatures: function () { return null },
    getActionColumns: function () { return null; }
});