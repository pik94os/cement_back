Ext.define('Cement.view.clients.statuses.Form', {
  extend: 'Cement.view.basic.Form',
  alias: 'widget.clients_statuses_form',
  fieldsDisabled: false,
  url: Cement.Config.url.clients.statuses.saveUrl,
  method: Cement.Config.url.clients.statuses.saveMethod,
  printUrl: Cement.Config.url.clients.statuses.printItem,
  activeItem: 0,
  isFilter: false,
  padding: '0',
    
  getItems: function () {
    return [{
      layout: 'anchor',
      xtype: 'panel',
      border: 0,
      defaults: {
        anchor: '100%',
        labelWidth: 110
      },
      items: [{
        xtype: 'fieldset',
        border: 0,
        margin: 0,
        padding: 10,
        defaults: {
          xtype: 'textfield',
          anchor: '100%'
        },
        items: [{
          xtype: 'hiddenfield',
          name: 'id'
        }, {
          xtype: 'textfield',
          fieldLabel: 'Наименование',
          disabled: this.fieldsDisabled,
          name: 'p_name'
        }, {
          fieldLabel: 'Описание',
          xtype: 'textarea',
          name: 'p_description',
          height: 150
          //disabled: this.fieldsDisabled,
        }]
      }]
    }];
  }
});