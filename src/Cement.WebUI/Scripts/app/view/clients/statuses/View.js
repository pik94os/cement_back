Ext.define('Cement.view.clients.statuses.View', {
  extend: 'Cement.view.clients.statuses.Form',
  alias: 'widget.clients_statuses_view',
  fieldsDisabled: true,
  buttons: null,
  border: 0,
  tbar: Cement.Config.modelViewButtons,
  layout: 'autocontainer',
  bbar: null,
  noUserFieldsetsControl: false,

  initComponent: function ()  {
    Ext.apply(this, {
      items: [{
        xtype: 'panel',
        border: 0,
        layout: 'anchor',
        defaultType: 'textfield',
        anchor: '100%',
        bodyPadding: '10 10 10 5',
        autoScroll: true,
        defaults: {
          anchor: '100%',
          labelWidth: 110
        },
        items: this.getItems()
      }]
    });
    this.callParent(arguments);
  }
});