Ext.define('Cement.view.clients.statuses.list.Statuses', {
    extend: 'Cement.view.basic.Grid',
    alias: 'widget.clients_statuses_list_statuses',

    gridStore: 'clients.statuses.List',
    gridStateId: 'stateStatusesList',

    printUrl: Cement.Config.url.clients.statuses.printGrid,
    helpUrl: Cement.Config.url.clients.statuses.help,
    deleteUrl: Cement.Config.url.clients.statuses.deleteUrl,
    printItemUrl: Cement.Config.url.clients.statuses.printItem,

    bbarText: 'Показаны статусы {0} - {1} из {2}',
    bbarEmptyMsg: 'Нет статусов',
    bbarUsersText: 'Статусов на странице: ',

    shownTitle: "Статус",
    cls: 'simple-form',

    //creatorTree: Ext.clone(Cement.Creators.clients),
    //createWindowTitle: 'Создать',

    autoLoadStore: true,

    getActionColumns: function () {
        return {
            xtype: 'rowactions',
            hideable: false,
            resizeable: false,
            locked: true,
            width: 46,
            actions: [{
                iconCls: 'icon-edit-item',
                qtip: 'Редактировать',
                callback: this.editItem
            }, {
                iconCls: 'icon-delete-item',
                qtip: 'Удалить',
                callback: this.deleteItem
            }],
            keepSelection: true
        };
    },

    getFilterItems: function () {
        return [{
            text: 'Наименование',
            kind: 'selector',
            field_name: 'p_name',
            checked: true
        }
        //, {
        //    text: 'Описание',
        //    kind: 'selector',
        //    field_name: 'p_description',
        //    checked: true
        //}
        ];
    },

    getGridColumns: function () {
        var result = [
            { text: 'Наименование', dataIndex: 'p_name', flex: 1 },
            //{ text: 'Описание', dataIndex: 'p_description', flex: 1 }
        ];
        return this.mergeActions(result);
    },

    initComponent: function () {
        Ext.apply(this, {
            title: this.shownTitle
        });
        this.callParent(arguments);
    }
});