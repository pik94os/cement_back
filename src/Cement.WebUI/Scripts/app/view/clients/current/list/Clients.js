Ext.define('Cement.view.clients.current.list.Clients', {
	extend: 'Cement.view.basic.Grid',
	alias: 'widget.clients_current_list_clients',

	gridStore: 'clients.Current',
	gridStateId: 'stateClientsCurrent2',

	printUrl: Cement.Config.url.clients.current.printGrid,
	helpUrl: Cement.Config.url.clients.current.help,
    deleteUrl: Cement.Config.url.clients.current.deleteUrl,
    printItemUrl: Cement.Config.url.clients.current.printItem,

    bbarText: 'Показаны клиенты {0} - {1} из {2}',
    bbarEmptyMsg: 'Нет клиентов',
    bbarUsersText: 'Клиентов на странице: ',

    shownTitle: null,
    showCreateButton: false,
    cls: 'no-border-bottom',

    creatorTree: Ext.clone(Cement.Creators.clients),
    createWindowTitle: 'Создать',

    getActionColumns: function () {
        return {
            xtype: 'rowactions',
            hideable: false,
            resizeable: false,
            locked: true,
            width: 46,
            actions: [{
                iconCls: 'icon-add-item',
                qtip: 'Добавить',
                callback: this.addItem
            }],
            keepSelection: true
        };
    },

    transformRecord: function (record) {
        var result = Ext.clone(record);
        result.set('id', 0);
        return result;
    },

    addItem: function (grid, record) {
        var bg = grid.up('basicgrid');
        grid.up('basicgrid').fireEvent('edititem', bg.transformRecord(record), 'Cement.model.Client');
    },

    getFilterItems: function () {
        return [{
            text: 'Наименование',
            kind: 'selector',
            field_name: 'p_firm_name',
            checked: true
        }, {
            text: 'Адрес',
            kind: 'selector',
            field_name: 'p_address',
            checked: true
        }, {
            text: 'КПП',
            kind: 'selector',
            field_name: 'p_reg_kpp',
            checked: true
        }, {
            text: 'ИНН',
            kind: 'selector',
            field_name: 'p_reg_inn',
            checked: true
        }, {
            text: 'ОГРН',
            kind: 'selector',
            field_name: 'p_reg_ogrn',
            checked: true
        }];
    },

    getGridColumns: function () {
        var result = [
            { text: 'Наименование', dataIndex: 'p_firm_name', width: 200, locked: true },
            { text: 'Адрес', dataIndex: 'p_address_display', width: 250 },
            { text: 'ИНН', dataIndex: 'p_reg_inn', width: 150 },
            { text: 'КПП', dataIndex: 'p_reg_kpp', width: 150 },
            { text: 'ОГРН', dataIndex: 'p_reg_ogrn', width: 150 }
        ];
        return this.mergeActions(result);
    },

    initComponent: function () {
        Ext.apply(this, {
            title: this.shownTitle
        });
        this.callParent(arguments);
    }
});