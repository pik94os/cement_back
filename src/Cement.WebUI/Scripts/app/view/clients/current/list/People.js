Ext.define('Cement.view.clients.current.list.People', {
	extend: 'Cement.view.basic.Grid',
	alias: 'widget.clients_current_list_people',

	gridStore: 'clients.current.People',
	gridStateId: 'stateClientsCurrentPeople',

	printUrl: Cement.Config.url.clients.people.printGrid,
	helpUrl: Cement.Config.url.clients.people.help,
    deleteUrl: Cement.Config.url.clients.people.deleteUrl,
    printItemUrl: Cement.Config.url.clients.people.printItem,

    bbarText: 'Показаны организации {0} - {1} из {2}',
    bbarEmptyMsg: 'Нет организаций',
    bbarUsersText: 'Организаций на странице: ',

    shownTitle: null,
    autoLoadStore: false,
    cls: 'no-border-bottom no-border-bottom-grid-view',
    tbar: null,
    showBottomBar: false,

    getGridFeatures: function () { return null },

    getToolbarItems: function () {
        return null;
    },

    getGridColumns: function () {
        var result = [
            { text: 'ФИО', dataIndex: 'fio', width: 200, locked: true },
            { text: 'Вид участия', dataIndex: 'p_part_kind_display', width: 150 },
            { text: 'Наименование', dataIndex: 'p_firm_name', width: 150 },
            { text: 'Адрес', dataIndex: 'address', width: 150 },
            { text: 'КПП', dataIndex: 'p_reg_kpp', width: 150 },
            { text: 'ИНН', dataIndex: 'p_reg_inn', width: 150 },
            { text: 'ОГРН', dataIndex: 'p_reg_ogrn', width: 150 },
            { text: 'Вид деятельности', dataIndex: 'p_activity_kind_display', width: 150 }
        ];
        return result;
    },

    initComponent: function () {
        Ext.apply(this, {
            title: this.shownTitle,
            tbar: null
        });
        this.callParent(arguments);
    }
});