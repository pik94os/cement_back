Ext.define('Cement.view.clients.current.list.Firms', {
	extend: 'Cement.view.basic.Grid',
	alias: 'widget.clients_current_list_firms',

	gridStore: 'clients.current.Firms',
	gridStateId: 'stateClientsCurrentFirms',

	printUrl: Cement.Config.url.clients.firms.printGrid,
	helpUrl: Cement.Config.url.clients.firms.help,
    deleteUrl: Cement.Config.url.clients.firms.deleteUrl,
    printItemUrl: Cement.Config.url.clients.firms.printItem,

    bbarText: 'Показаны организации {0} - {1} из {2}',
    bbarEmptyMsg: 'Нет организаций',
    bbarUsersText: 'Организаций на странице: ',

    shownTitle: null,
    autoLoadStore: false,
    cls: 'no-border-bottom no-border-bottom-grid-view',
    tbar: null,
    showBottomBar: false,

    getGridFeatures: function () { return null },

    getToolbarItems: function () {
        return null;
    },

    getGridColumns: function () {
        var result = [
            { text: 'Наименование', dataIndex: 'p_name', width: 250, locked: true },
            { text: 'Адрес', dataIndex: 'p_fact_address', width: 150 },
            { text: 'КПП', dataIndex: 'p_kpp', width: 150 },
            { text: 'ИНН', dataIndex: 'p_inn', width: 150 },
            { text: 'ОГРН', dataIndex: 'p_ogrn', width: 150 },
            { text: 'Вид деятельности', dataIndex: 'p_activity_kind_display', width: 150 }
        ];
        return result;
    },

    initComponent: function () {
        Ext.apply(this, {
            title: this.shownTitle,
            tbar: null
        });
        this.callParent(arguments);
    }
});