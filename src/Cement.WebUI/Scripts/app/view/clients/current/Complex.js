Ext.define('Cement.view.clients.current.Complex', {
    extend: 'Cement.view.basic.Complex',
    alias: 'widget.clients_current_complex',
    border: 0,
    enableDetail: false,
    autoLoadStore: false,
    priceId: null,
    bodyPadding: 0,
    padding: 0,
    header: false,
    cls: 'no-side-borders',
    title: 'Общий каталог',
    tabTitle: 'Общий каталог',
    topXType: 'clients_current_list_clients',
    topDetailType: 'Cement.view.clients.SimpleView',
    rowsCount: 3,
    childFilterColumn: 'id',
    bottomTitle: 'Связанные организации и лица',
    bottomTabs: [{
        title: 'Дочерние организации',
        shownTitle: 'Дочерние организации',
        xtype: 'clients_current_list_firms',
        detailType: 'Cement.view.corporate.firm.View',
        constantFilterParam: 'p_client_current',
        childrenConstantFilterParam: 'p_clients_current_connected_firm',
        border: 0
    }, {
        title: 'Связанные лица',
        shownTitle: 'Связанные лица',
        xtype: 'clients_current_list_people',
        detailType: 'Cement.view.corporate.employee.View',
        constantFilterParam: 'p_client_current',
        childrenConstantFilterParam: 'p_client_current_connected_person',
        border: 0
    }],
    subBottomTitle: 'Справочник',
    subBottomTabs: [{
        title: 'Реквизиты счетов',
        xtype: 'clients_current_bottom_bank_accounts',
        detailType: 'Cement.view.corporate.bank_account.View'
    }, {
        title: 'Склады',
        xtype: 'clients_current_bottom_warehouses',
        detailType: 'Cement.view.corporate.warehouse.View'
    }, {
        title: 'Сотрудники',
        xtype: 'clients_current_bottom_employees',
        detailType: 'Cement.view.corporate.employee.View'
    }, {
        title: 'Подписи',
        xtype: 'clients_current_bottom_signatures',
        detailType: 'Cement.view.corporate.signature.View'
    }, {
        title: 'Товары',
        xtype: 'clients_bottom_products',
        detailType: 'Cement.view.products.products.View'
    },
    {
        title: 'Услуги',
        xtype: 'clients_bottom_services',
        detailType: 'Cement.view.products.services.View'
    },
    {
        title: 'ТЕ',
        xtype: 'clients_bottom_transportunits',
        detailType: 'Cement.view.transport.units.View'
    },
    {
        title: 'Внутренние документы',
        xtype: 'clients_bottom_internal_documents',
        detailType: 'Cement.view.office_work.documents.View'
    },
    {
        title: 'Корреспонденция',
        xtype: 'clients_bottom_correspondence',
        detailType: 'Cement.view.office_work.correspondention.View'
    },
     //}, {
     //    title: 'Услуги',
     //    xtype: 'clients_current_bottom_services',
     //    detailType: 'Cement.view.service.View'
     //}, {
     //    title: 'ТЕ',
     //    xtype: 'clients_current_bottom_transport_units',
     //    detailType: 'Cement.view.client.view.TransportUnit'
    // }, {
    //     title: 'Вагон',
    //     xtype: 'clients_current_bottom_carriages',
    //     detailType: 'Cement.view.transport.view.Carriage'

    ],

    initComponent: function () {
        Ext.apply(this, {
            title: 'Клиенты действующие'
        });
        this.callParent(arguments);
    }
});