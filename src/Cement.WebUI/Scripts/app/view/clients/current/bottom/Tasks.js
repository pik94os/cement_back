Ext.define('Cement.view.clients.current.bottom.Tasks', {
	extend: 'Cement.view.basic.Grid',
	alias: 'widget.clients_current_bottom_tasks',

	gridStore: 'clients.current.bottom.Tasks',
	gridStateId: 'stateClientsTasksList',

	printUrl: Cement.Config.url.clients.tasks.printGrid,
	helpUrl: Cement.Config.url.clients.tasks.help,
    deleteUrl: Cement.Config.url.clients.tasks.deleteUrl,
    printItemUrl: Cement.Config.url.clients.tasks.printItem,

    bbarText: 'Показаны задачи {0} - {1} из {2}',
    bbarEmptyMsg: 'Нет задач',
    bbarUsersText: 'Задач на странице: ',

    shownTitle: 'Задачи',
    autoLoadStore: false,
    cls: 'nested-form',
    tbar: null,
    showBottomBar: false,
    getGridFeatures: function () { return null },

    getToolbarItems: function () {
        return {
            items: [{
                xtype: 'button',
                text: 'Создать',
                iconCls: 'icon-add-item',
                action: 'create_item',
                disabled: false,
                hidden: !this.showCreateButton
            }]
        };
    },

    getGridColumns: function () {
        var result = [
            { text: 'Тема', dataIndex: 'p_theme', flex: 1 },
            { text: 'Содержание', dataIndex: 'p_content', flex: 1 },
            { text: 'Повторение', dataIndex: 'p_repeat_display', flex: 1 },
            { text: 'Срок', dataIndex: 'p_date', flex: 1 }
        ];
        return result;
    },

    initComponent: function () {
        Ext.apply(this, {
            title: this.shownTitle,
            tbar: null
        });
        this.callParent(arguments);
    }
});