Ext.define('Cement.view.clients.current.bottom.Signatures', {
	extend: 'Cement.view.corporate.signature.List',
	gridStore: 'clients.current.bottom.Signatures',
	gridStateId: 'stateClientCurrentSignaturesList',
	xtype: 'clients_current_bottom_signatures',
	tbar: null,
	showBottomBar: false,
	autoLoadStore: false,
	cls: 'nested-form',
	getToolbarItems: function () { return null; },
	getGridFeatures: function () { return null },
	getActionColumns: function () { return null; }
});