Ext.define('Cement.view.clients.current.bottom.Employees', {
	extend: 'Cement.view.corporate.employee.List',
	gridStore: 'clients.current.bottom.Employees',
	gridStateId: 'stateClientCurrentEmployeesList',
	xtype: 'clients_current_bottom_employees',
	tbar: null,
	showBottomBar: false,
	autoLoadStore: false,
	cls: 'nested-form',
	getToolbarItems: function () { return null; },
	getGridFeatures: function () { return null },
	getActionColumns: function () { return null; }
});