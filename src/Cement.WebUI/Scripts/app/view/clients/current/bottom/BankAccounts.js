Ext.define('Cement.view.clients.current.bottom.BankAccounts', {
	extend: 'Cement.view.corporate.bank_account.List',
	gridStore: 'clients.current.bottom.BankAccounts',
	gridStateId: 'stateClientCurrentBankAccountsList',
	xtype: 'clients_current_bottom_bank_accounts',
	tbar: null,
	showBottomBar: false,
	cls: 'nested-form',
	autoLoadStore: false,
	getToolbarItems: function () { return null; },
	getGridFeatures: function () { return null },
	getActionColumns: function () { return null; }
});