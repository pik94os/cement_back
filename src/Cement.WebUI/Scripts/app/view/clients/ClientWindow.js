Ext.define('Cement.view.clients.ClientWindow', {
	extend: 'Cement.view.common.SignWindow',
	xtype: 'clients_client_window',
	leftFrameTitle: 'Клиенты',
	title: 'Выбрать клиента',
	topGridTitle: 'Клиенты',
	bottomGridTitle: 'Выбор клиента',
	structure_storeId: 'clients.Structure',
	hideCommentPanel: true,
	singleSelection: true,
	storeFields: ['id', 'p_name', 'p_address', 'p_kpp', 'p_inn'],
	displayField: 'p_name',

	bbar: [{
		text: 'Сохранить',
		action: 'confirm'
	}, {
		text: 'Отмена',
		action: 'cancel'
	}],

	sign: function (send) {
		var store = this.down('*[role=sign-dst-grid]').getStore();
		if (store.getCount() > 0) {
			this.fireEvent('signitem', store.getAt(0));
		}
	},

	addToSrcStore: function (item) {
		if (!this.dstStore.getById(item.get('id'))) {
			this.srcStore.add({
				id: item.get('id'),
				p_inn: item.get('p_inn'),
				p_name: item.get('p_name'),
				p_kpp: item.get('p_kpp'),
				p_address: item.get('p_address')
			});
		}
	},

	getTopColumnsConfig: function () {
		return [
			{
				xtype: 'rowactions',
				hideable: false,
				resizeable: false,
				width: 18,
				keepSelection: true,
				actions: [{ 
	                iconCls: 'icon-add-people', 
	                qtip: 'Добавить',
	                callback: this.addEmployee
	            }]
			},
			{ text: 'Наименование', dataIndex: 'p_name', flex: 1 },
			{ text: 'Адрес', dataIndex: 'p_address', flex: 1 },
			{ text: 'ИНН', dataIndex: 'p_inn', flex: 1 },
			{ text: 'КПП', dataIndex: 'p_kpp', flex: 1 }
		];
	},

	getBottomColumnsConfig: function () {
		return [
			{
				xtype: 'rowactions',
				hideable: false,
				resizeable: false,
				width: 18,
				actions: [{ 
	                iconCls: 'icon-remove-people', 
	                qtip: 'Удалить', 
	                callback: this.removeEmployee
	            }]
			},
			{ text: 'Наименование', dataIndex: 'p_name', flex: 1 },
			{ text: 'Адрес', dataIndex: 'p_address', flex: 1 },
			{ text: 'ИНН', dataIndex: 'p_inn', flex: 1 },
			{ text: 'КПП', dataIndex: 'p_kpp', flex: 1 }
		];
	},

	loadData: function (data) {
		if (data) {
			var fstore = Ext.getStore(this.structure_storeId);
			fstore.load({
				scope: this,
				callback: function () {
					Ext.each(data, function (item) {
						var r = fstore.getById(item);
						if (!this.dstStore) {
							this.setupEvents();
						}
						if (r) {
							this.dstStore.add({
								id: r.get('id'),
								p_address: r.get('p_address'),
								p_inn: r.get('p_inn'),
								p_kpp: r.get('p_kpp'),
								p_name: r.get('p_name')
							});
						}
					}, this);
				}
			});
		}
	},

	closeWindow: function () {
		this.hide();
	},

	initComponent: function () {
		this.callParent(arguments);
		this.on('beforeclose', function () {
			this.hide();
			return false;
		});
		this.on('afterrender', function () {
			Ext.getStore(this.structure_storeId).getRootNode().expand();
		}, this);
	}
});