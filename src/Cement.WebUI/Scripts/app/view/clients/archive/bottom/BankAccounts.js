Ext.define('Cement.view.clients.archive.bottom.BankAccounts', {
	extend: 'Cement.view.corporate.bank_account.List',
	gridStore: 'clients.archive.bottom.BankAccounts',
	gridStateId: 'stateClientarchiveBankAccountsList',
	xtype: 'clients_archive_bottom_bank_accounts',
	tbar: null,
	showBottomBar: false,
	cls: 'nested-form',
	autoLoadStore: false,
	getToolbarItems: function () { return null; },
	getGridFeatures: function () { return null },
	getActionColumns: function () { return null; }
});