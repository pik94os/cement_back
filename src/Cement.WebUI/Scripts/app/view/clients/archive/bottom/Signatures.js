Ext.define('Cement.view.clients.archive.bottom.Signatures', {
	extend: 'Cement.view.corporate.signature.List',
	gridStore: 'clients.archive.bottom.Signatures',
	gridStateId: 'stateClientarchiveSignaturesList',
	xtype: 'clients_archive_bottom_signatures',
	tbar: null,
	showBottomBar: false,
	autoLoadStore: false,
	cls: 'nested-form',
	getToolbarItems: function () { return null; },
	getGridFeatures: function () { return null },
	getActionColumns: function () { return null; }
});