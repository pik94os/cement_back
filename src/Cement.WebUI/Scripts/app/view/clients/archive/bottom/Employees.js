Ext.define('Cement.view.clients.archive.bottom.Employees', {
	extend: 'Cement.view.corporate.employee.List',
	gridStore: 'clients.archive.bottom.Employees',
	gridStateId: 'stateClientarchiveEmployeesList',
	xtype: 'clients_archive_bottom_employees',
	tbar: null,
	showBottomBar: false,
	autoLoadStore: false,
	cls: 'nested-form',
	getToolbarItems: function () { return null; },
	getGridFeatures: function () { return null },
	getActionColumns: function () { return null; }
});