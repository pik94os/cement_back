Ext.define('Cement.view.clients.archive.list.Firms', {
    extend: 'Cement.view.clients.current.list.Firms',
    alias: 'widget.clients_archive_list_firms',

    gridStore: 'clients.archive.Firms',
    gridStateId: 'stateClientsarchiveFirms'
});