Ext.define('Cement.view.clients.archive.list.Clients', {
	extend: 'Cement.view.clients.corporate.list.Clients',
	alias: 'widget.clients_archive_list_clients',

	gridStore: 'clients.Archive',
	gridStateId: 'stateClientsArchive',

	printUrl: Cement.Config.url.clients.archive.printGrid,
	helpUrl: Cement.Config.url.clients.archive.help,
    deleteUrl: Cement.Config.url.clients.archive.deleteUrl,
    unArchiveItemUrl: Cement.Config.url.clients.archive.unArchiveItem,
    printItemUrl: Cement.Config.url.clients.archive.printItem,

    getActionColumns: function () {
        return {
            xtype: 'rowactions',
            hideable: false,
            resizeable: false,
            locked: true,
            width: 46,
            actions: [{
                iconCls: 'icon-unarchive-item',
                qtip: 'Из архива',
                callback: this.unArchiveItem
            }, {
                iconCls: 'icon-delete-item',
                qtip: 'Удалть',
                callback: this.deleteItem
            }],
            keepSelection: true
        };
    }
});