Ext.define('Cement.view.clients.archive.list.People', {
    extend: 'Cement.view.clients.current.list.People',
    alias: 'widget.clients_archive_list_people',

    gridStore: 'clients.archive.People',
    gridStateId: 'stateClientsarchivePeople'
});