Ext.define('Cement.view.clients.clients.Total', {
  extend: 'Cement.view.basic.Tabs',
  title: 'Клиенты',

  items: [{
    xtype: 'clients_current_complex',
    title: 'Общий каталог'
  }, {
    xtype: 'clients_corporate_complex',
    title: 'Корпоративный каталог'
  }, {
    xtype: 'clients_personal_complex',
    title: 'Персональный каталог'
  }, {
    xtype: 'clients_archive_complex',
    title: 'Архив'
  }],

  initComponent: function () {
    Ext.apply(this, {
      title: 'Клиенты'
    });
    this.callParent(arguments);
  }
});