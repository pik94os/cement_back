Ext.define('Cement.view.clients.Form', {
	extend: 'Cement.view.basic.Form',
	alias: 'widget.clientform',
	fieldsDisabled: false,
	url: Cement.Config.url.clients.current.saveUrl,
	method: Cement.Config.url.clients.saveMethod,
	printUrl: Cement.Config.url.clients.printItem,
	activeItem: 0,
	isFilter: false,
	padding: '0',
	// totalPanelHeight: 630,
	clientGroupsStoreId: 'clients.Groups',
	// clientSubGroupsStoreId: 'clients.Groups',
	clientStatusesStoreId: 'clients.Statuses',

	getFileField: function (label, name) {
		return {
			xtype: 'downloadfield',
			name: name,
			fieldLabel: label
		};
	},

	getPhotoFieldShow: function () {
		if (this.isFilter) return null;
		var result = {
			xtype: 'image',
			src: Cement.Config.default_photo,
			maxWidth: 83,
			maxHeight: 100,
			x: 83,
			cls: 'pos-relative'
		};
		if (!this.fieldsDisabled) {
			result = null;
		}
		return result;
	},

	getPhotoField: function () {
		if (this.isFilter) return null;
		var result = {
			name: 'p_firm_img',
			fieldLabel: 'Фото',
			allowBlank: this.allowBlankFields,
			xtype: 'filefield',
			anchor: '80%',
			buttonText: 'Загрузить',
			disabled: this.fieldsDisabled
		};
		if (this.fieldsDisabled) {
			result = null;
		}
		return result;
	},

	getBankAccount: function () {
		var result = {
			layout: 'anchor',
			xtype: 'fieldset',
			collapsed: false,
			collapsible: true,
			title: 'Банковские реквизиты',
			defaults: {
				xtype: 'textfield',
				anchor: '100%'
			},
			anchor: '100%',
			items: [{
				fieldLabel: 'Расчетный счет',
				name: 'p_bank_account',
				disabled: true
			}, {
				fieldLabel: 'Корреспондентский счет',
				name: 'p_bank_corr',
				disabled: true
			}, {
				fieldLabel: 'Наименование',
				name: 'p_bank_name',
				disabled: true
			}, {
				fieldLabel: 'БИК',
				name: 'p_bank_bik',
				disabled: true
			}]
		};
		if (this.fieldsDisabled) {
			result = null;
		}
		return result;
	},

	getDirector: function () {
		var result = {
			layout: 'anchor',
			xtype: 'fieldset',
			collapsed: true,
			collapsible: true,
			title: 'Куратор',
			defaults: {
				xtype: 'textfield',
				anchor: '100%'
			},
			anchor: '100%',
			items: [{
				fieldLabel: 'Фамилия',
				name: 'p_curator_surname',
				disabled: true
			}, {
				fieldLabel: 'Имя',
				name: 'p_curator_name',
				disabled: true
			}, {
				fieldLabel: 'Отчество',
				name: 'p_curator_lastname',
				disabled: true
			}, {
				fieldLabel: 'Должность',
				name: 'p_curator_position',
				disabled: true
			}, {
				fieldLabel: 'На основании',
				name: 'p_curator_for',
				disabled: true
			}]
		};
		if (!this.fieldsDisabled) {
			result = null;
		}
		return result;
	},

	getUrAddress: function () {
		return {
			layout: 'anchor',
			xtype: 'fieldset',
			collapsed: true,
			collapsible: true,
			title: 'Юридический адрес',
			defaults: {
				xtype: 'textfield',
				anchor: '100%'
			},
			anchor: '100%',
			items: [{
				fieldLabel: 'Индекс',
				name: 'p_ur_index',
				disabled: true
			}, {
				fieldLabel: 'Страна',
				name: 'p_ur_country',
				disabled: true
			}, {
				fieldLabel: 'Регион',
				name: 'p_ur_region',
				disabled: true
			}, {
				fieldLabel: 'Район',
				name: 'p_ur_district',
				disabled: true
			}, {
				fieldLabel: 'Населенный пункт',
				name: 'p_ur_city',
				disabled: true
			}, {
				fieldLabel: 'Улица',
				name: 'p_ur_street',
				disabled: true
			}, {
				fieldLabel: 'Дом',
				name: 'p_ur_house',
				disabled: true
			}, {
				fieldLabel: 'Корпус',
				name: 'p_ur_house_part',
				disabled: true
			}, {
				fieldLabel: 'Строение',
				name: 'p_ur_building',
				disabled: true
			}, {
				fieldLabel: 'Офис',
				name: 'p_ur_office',
				disabled: true
			}]
		};
	},

	getFactAddress: function () {
		return {
			layout: 'anchor',
			xtype: 'fieldset',
			collapsed: true,
			collapsible: true,
			title: 'Фактический адрес',
			defaults: {
				xtype: 'textfield',
				anchor: '100%'
			},
			anchor: '100%',
			items: [{
				fieldLabel: 'Индекс',
				name: 'p_fact_index',
				disabled: true
			}, {
				fieldLabel: 'Страна',
				name: 'p_fact_country',
				disabled: true
			}, {
				fieldLabel: 'Регион',
				name: 'p_fact_region',
				disabled: true
			}, {
				fieldLabel: 'Район',
				name: 'p_fact_district',
				disabled: true
			}, {
				fieldLabel: 'Населенный пункт',
				name: 'p_fact_city',
				disabled: true
			}, {
				fieldLabel: 'Улица',
				name: 'p_fact_street',
				disabled: true
			}, {
				fieldLabel: 'Дом',
				name: 'p_fact_house',
				disabled: true
			}, {
				fieldLabel: 'Корпус',
				name: 'p_fact_house_part',
				disabled: true
			}, {
				fieldLabel: 'Строение',
				name: 'p_fact_building',
				disabled: true
			}, {
				fieldLabel: 'Офис',
				name: 'p_fact_office',
				disabled: true
			}]
		};
	},

	getConnectData: function () {
		return {
			layout: 'anchor',
			xtype: 'fieldset',
			collapsed: true,
			collapsible: true,
			title: 'Связь',
			defaults: {
				xtype: 'textfield',
				anchor: '100%'
			},
			anchor: '100%',
			items: [{
				fieldLabel: 'Телефон',
				name: 'p_cont_phone',
				disabled: true
			}, {
				fieldLabel: 'Факс',
				name: 'p_cont_fax',
				disabled: true
			}, {
				fieldLabel: 'E-mail',
				name: 'p_cont_email',
				disabled: true
			}, {
				fieldLabel: 'WEB',
				name: 'p_cont_web',
				disabled: true
			}]
		};
	},

	getRegDataEx: function () {
		return {
			layout: 'anchor',
			xtype: 'fieldset',
			collapsed: true,
			collapsible: true,
			title: 'Регистрационные данные',
			defaults: {
				xtype: 'textfield',
				anchor: '100%'
			},
			anchor: '100%',
			items: [{
				fieldLabel: 'ОГРН',
				name: 'p_reg_ogrn',
				disabled: true
			}, this.getFileField('ОГРН', 'p_reg_ogrn_img'), {
				fieldLabel: 'ИНН',
				name: 'p_reg_inn',
				disabled: true
			}, this.getFileField('ИНН', 'p_reg_inn_img'), {
				fieldLabel: 'КПП',
				name: 'p_reg_kpp',
				disabled: true
			}, {
				fieldLabel: 'ОКПО',
				name: 'p_reg_okpo',
				disabled: true
			}, {
				fieldLabel: 'ОКВЭД',
				name: 'p_reg_okved',
				disabled: true
			}, {
				fieldLabel: 'ОКАТО',
				name: 'p_reg_okato',
				disabled: true
			}, {
				fieldLabel: 'ОКОНХ',
				name: 'p_reg_okonh',
				disabled: true
			}, this.getFileField('Устав', 'p_reg_charter_doc') ]
		};
	},

	getRegData: function () {
		return {
			layout: 'anchor',
			xtype: 'fieldset',
			collapsed: true,
			collapsible: true,
			title: 'Регистрационные данные',
			defaults: {
				xtype: 'textfield',
				anchor: '100%'
			},
			anchor: '100%',
			items: [{
				fieldLabel: 'ОГРН',
				name: 'p_reg_ogrn',
				disabled: true
			}, {
				fieldLabel: 'ИНН',
				name: 'p_reg_inn',
				disabled: true
			}, {
				fieldLabel: 'КПП',
				name: 'p_reg_kpp',
				disabled: true
			}, {
				fieldLabel: 'ОКПО',
				name: 'p_reg_okpo',
				disabled: true
			}, {
				fieldLabel: 'ОКВЭД',
				name: 'p_reg_okved',
				disabled: true
			}, {
				fieldLabel: 'ОКАТО',
				name: 'p_reg_okato',
				disabled: true
			}, {
				fieldLabel: 'ОКОНХ',
				name: 'p_reg_okonh',
				disabled: true
			}]
		};
	},

	getBonusData: function () {
		return {
			layout: 'anchor',
			xtype: 'fieldset',
			collapsed: true,
			collapsible: true,
			title: 'Дополнительные данные',
			defaults: {
				xtype: 'textfield',
				anchor: '100%'
			},
			anchor: '100%',
			items: [
				this.getCombo('Статус', 'p_status', this.clientStatusesStoreId), {
					fieldLabel: 'Откуда клиент',
					name: 'p_from',
					disabled: this.fieldsDisabled
				}, {
					fieldLabel: 'Комментарии',
					name: 'p_comment',
					xtype: 'textarea',
					disabled: this.fieldsDisabled,
					height: 150
				}
			]
		};
	},

	categoryChange: function (combo, newValue) {
		if (newValue == '') return;
		var	catTree = Ext.getStore(combo.up('basicform').clientGroupsStoreId),
			subCatStore = combo.up('basicform').down('combo[name=p_sub_group]').getStore(),
			selected = catTree.getById(newValue);
		combo.up('basicform').down('combo[name=p_sub_group]').clearValue();
		subCatStore.removeAll();
		if (selected) {
			selected.eachChild(function (child) {
				subCatStore.add({
					id: child.get('id'),
					p_name: child.get('text')
				});
			});
		}
	},

	getItems: function () {
		var	groupsTree = Ext.getStore(this.clientGroupsStoreId),
			catStore = Ext.create('Ext.data.Store', {
				fields: ['id', 'p_name']
			}),
			subCatStore = Ext.create('Ext.data.Store', {
				fields: ['id', 'p_name']
			});
		this.subCatStore = subCatStore;
		var populateStore = function () {
			catStore.removeAll();
			groupsTree.getRootNode().eachChild(function (child) {
				catStore.add({
					id: child.get('id'),
					p_name: child.get('text')
				});
			});
		};
		groupsTree.on('load', populateStore);
		populateStore();
		return [{
			layout: 'anchor',
			xtype: 'panel',
			border: 0,
			defaults: {
				anchor: '100%',
				labelWidth: 110
			},
			items: [{
				xtype: 'fieldset',
				border: 0,
				margin: 0,
				padding: 10,
				defaults: {
					xtype: 'textfield',
					anchor: '100%'
				},
				items: [{
					xtype: 'hiddenfield',
					name: 'id'
				}, {
					xtype: 'hiddenfield',
					name: 'p_client'
				}, this.getCombo('Группа', 'p_group', catStore, this.categoryChange),
					this.getCombo('Подгруппа', 'p_sub_group', subCatStore), {
					xtype: 'textfield',
					fieldLabel: 'Форма собственности',
					disabled: true,
					name: 'p_firm_form'
				}, {
					xtype: 'textfield',
					fieldLabel: 'Наименование',
					anchor: '100%',
					disabled: true,
					name: 'p_firm_name'
				}, {
					fieldLabel: 'Полное наименование',
					name: 'p_firm_full_name',
					disabled: true
				}, {
					fieldLabel: 'Бренд',
					name: 'p_firm_brand',
					disabled: true
				}]
			}]
		}, this.getUrAddress(),
			this.getFactAddress(),
			this.getRegData(),
			this.getConnectData(),
			this.getDirector(),
			this.getBonusData()
			// this.getBankAccount(),
			//
		];
	},

	initComponent: function () {
		this.callParent(arguments);
		if (!this.fieldsDisabled) {
			Ext.getStore(this.clientStatusesStoreId).load();
		}
	},

	afterRecordLoad: function (rec) {
		var newValue = rec.get('p_group'),
			catTree = Ext.getStore(this.clientGroupsStoreId),
			subCatStore = this.subCatStore,
			selected = catTree.getById(newValue);
		if (subCatStore) {
			subCatStore.removeAll();
			if (selected) {
				selected.eachChild(function (child) {
					subCatStore.add({
						id: child.get('id'),
						p_name: child.get('text')
					});
				});
			}
		}
	}
});