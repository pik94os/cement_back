Ext.define('Cement.view.clients.management.list.Archive', {
    extend: 'Cement.view.clients.management.list.Events',
    alias: 'widget.clients_management_list_archive',
    
    gridStore: 'clients.management.archive',
    gridStateId: 'stateClientsManagementArchive',

    printUrl: Cement.Config.url.clients.management.archive.printGrid,
    helpUrl: Cement.Config.url.clients.management.archive.help,
    deleteUrl: Cement.Config.url.clients.management.archive.deleteUrl,
    unArchiveItemUrl: Cement.Config.url.clients.management.archive.unArchiveItem,

    shownTitle: null,

    getActionColumns: function () {
        return {
            xtype: 'rowactions',
            hideable: false,
            resizeable: false,
            locked: true,
            width: 46,
            actions: [
            {
                iconCls: 'icon-unarchive-item',
                qtip: 'Из архива',
                callback: this.unArchiveItem
            },
            {
                iconCls: 'icon-delete-item',
                qtip: 'Удалить',
                callback: this.deleteItem
            }
            ],
            keepSelection: true
        };
    },

});