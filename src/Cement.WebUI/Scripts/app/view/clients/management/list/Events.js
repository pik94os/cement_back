Ext.define('Cement.view.clients.management.list.Events', {
    extend: 'Cement.view.basic.Grid',
    alias: 'widget.clients_management_list_events',

    //cls: 'simple-form',

    autoLoadStore: false,

    bbarText: 'Показаны клиенты {0} - {1} из {2}',
    bbarEmptyMsg: 'Нет клиентов',
    bbarUsersText: 'Клиентов на странице: ',

    gridStore: 'clients.management.events',
    gridStateId: 'stateClientsManagementEvent',

    printUrl: Cement.Config.url.clients.management.events.printGrid,
    helpUrl: Cement.Config.url.clients.management.events.help,
    deleteUrl: Cement.Config.url.clients.management.events.deleteUrl,
    archiveItemUrl: Cement.Config.url.clients.management.events.archiveItem,
    commentItemUrl: Cement.Config.url.clients.management.events.commentItem,


    shownTitle: null,

    gridViewConfig: {
        getRowClass: function (record) {
            return record.get('p_description') || record.get('p_comments') ? '' : 'expander-hidden';
        }
    },

    getGridPlugins: function () {
        return [
        {
            ptype: 'rowexpander',
            rowBodyTpl: new Ext.XTemplate(
                '<table>',
                    '<tr>',
                         '<td class="icon-client-mgt-event" style="width: 20px;padding: 0;height: 16px;"></td>',
                         '<td colspan="3"><p style="margin:0">{p_description}</p></td>',
                    '</tr>',
                    '<tpl for="p_comments">',
                          '<tr>',
                             '<td></td>',
                             '<td class="icon-client-mgt-event-comment" style="width: 20px;padding: 0;height: 16px;"></td>',
                             '<td style="width: 110px;">{date}</td>',
                             '<td>{text}</td>',
                        '</tr>',
                    '</tpl>',
                '</table>'
            )
        }];
    },

    getActionColumns: function () {
        return {
            xtype: 'rowactions',
            hideable: false,
            resizeable: false,
            locked: true,
            width: 46,
            actions: [
                {
                    iconCls: 'icon-client-mgt-event-comment-add',
                    qtip: 'Добавить комментарий',
                    callback: this.addComment
                },
                {
                    iconCls: 'icon-archive-item',
                    qtip: 'В архив',
                    callback: this.archiveItem
                }
            ],
            keepSelection: true
        };
    },

    getFilterItems: function () {
        return [/*{
        text: 'Тип',
        kind: 'selector',
        field_name: 'p_type_display',
        checked: true
    }, {
        text: 'Номер',
        kind: 'selector',
        field_name: 'p_supplier_name',
        checked: true
    }, {
        text: 'Дата',
        kind: 'selector',
        field_name: 'p_date',
        checked: true
    }, {
        text: 'Автор',
        kind: 'selector',
        field_name: 'p_author_display',
        checked: true
    }, {
        text: 'На подпись',
        kind: 'selector',
        field_name: 'p_for_sign_display',
        checked: true
    }, {
        text: 'Тема',
        kind: 'selector',
        field_name: 'p_theme',
        checked: true
    }*/];
    },

    getGridColumns: function () {
        var result = [
            { text: 'Клиент', dataIndex: 'p_client_display', width: 200 },
            { text: 'Адрес', dataIndex: 'p_address_display', width: 200 },
            { text: 'Дата', dataIndex: 'p_date', width: 80 },
            { text: 'Время', dataIndex: 'p_time', width: 80, sortable: false },
            { text: 'Напоминания', dataIndex: 'p_reminder', width: 150 },
            { text: 'Тема события', dataIndex: 'p_theme', minWidth: 200, flex: 1 }
        ];
        return this.mergeActions(result);
    },

    initComponent: function () {
        Ext.apply(this, {
            title: this.shownTitle
        });
        this.callParent(arguments);

        var columns = this.items.items[0].columns;
        var height = this.fixedColumnHeaderHeight;

        if (this.overrideColumnHeaderHeight) {
            Ext.each(columns, function (item) { item.height = height; });
        }
    },

    addComment: function(grid, record) {
        var basicgrid = grid.up('basicgrid');
        
        var me = this;
        var win = Ext.create('Ext.window.Window', {
            title: "Комментарий",
            width: 400,
            height: 200,
            layout: 'fit',
            bodyPadding: 10,
            modal: true,
            bodyStyle: "background: #f7f7f7;",
            items: [
            {
                xtype: 'textarea',
                labelAlign: 'top',
                anchor: '100%',
                height: 150,
                allowBlank: false
            }],
            bbar: [{
                xtype: 'button',
                text: 'Сохранить',
                role: 'save',
                iconCls: 'icon-save-item'
            },  {
                xtype: 'button',
                text: 'Отмена',
                role: 'close',
                iconCls: 'icon-cancel'
            }]
        });
        win.down('button[role=save]').on('click', function () {

            var textarea = win.down('textarea');
            if (!textarea.isValid()) {
                textarea.validate();
                return;
            }

            var params = {
                id: record.get('id'),
                comment: textarea.getValue()
            };

            win.setLoading("Сохранение");

            Ext.Ajax.request({
                url: basicgrid.commentItemUrl,
                params: params,
                success: function (response) {
                    var data = Ext.JSON.decode(response.responseText);

                    if (!data.success) {
                        Cement.Msg.error('Произошла непредусмотренная ошибка');
                    } else {
                        record.data.p_comments = data.data;
                        grid.getStore().update();
                        Cement.Msg.info("Успешно!");
                    }
                    
                    win.setLoading(false);
                    win.close();
                    delete win;
                },
                failure: function () {
                    win.setLoading(false);
                    Cement.Msg.error('Произошла ошибка');
                }
            });
        });
        win.down('button[role=close]').on('click', function () {
            win.close();
            delete win;
        });
        win.show();
    }
});