Ext.define('Cement.view.clients.management.bottom.BankAccounts', {
	extend: 'Cement.view.corporate.bank_account.List',
	gridStore: 'clients.management.bottom.BankAccounts',
	gridStateId: 'stateClientManagementBankAccountsList',
	xtype: 'clients_management_bottom_bank_accounts',
	tbar: null,
	autoLoadStore: false,
	cls: 'nested-form',
	getToolbarItems: function () { return null; },
	getGridFeatures: function () { return null },
	getActionColumns: function () { return null; }
});