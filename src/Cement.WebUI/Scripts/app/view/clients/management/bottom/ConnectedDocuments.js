Ext.define('Cement.view.clients.management.bottom.ConnectedDocuments', {
	extend: 'Cement.view.goods.auto.lists.ConnectedDocuments',
	alias: 'widget.clients_management_bottom_connected_documents',

	gridStore: 'clients.management.ConnectedDocuments',
	gridStateId: 'stateClientsManagementConnectedDocumentsConnectedDocuments',

	printUrl: Cement.Config.url.clients.management.documents.printGrid,
	helpUrl: Cement.Config.url.clients.management.documents.help
});