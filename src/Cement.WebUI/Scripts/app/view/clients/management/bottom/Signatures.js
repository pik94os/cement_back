Ext.define('Cement.view.clients.management.bottom.Signatures', {
	extend: 'Cement.view.corporate.signature.List',
	gridStore: 'clients.management.bottom.Signatures',
	gridStateId: 'stateClientManagementSignaturesList',
	xtype: 'clients_management_bottom_signatures',
	tbar: null,
	autoLoadStore: false,
	cls: 'nested-form',
	getToolbarItems: function () { return null; },
	getGridFeatures: function () { return null },
	getActionColumns: function () { return null; }
});