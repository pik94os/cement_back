Ext.define('Cement.view.clients.management.bottom.Employees', {
	extend: 'Cement.view.corporate.employee.List',
	gridStore: 'clients.management.bottom.Employees',
	gridStateId: 'stateClientManagementEmployeesList',
	xtype: 'clients_management_bottom_employees',
	tbar: null,
	autoLoadStore: false,
	cls: 'nested-form',
	getToolbarItems: function () { return null; },
	getGridFeatures: function () { return null },
	getActionColumns: function () { return null; }
});