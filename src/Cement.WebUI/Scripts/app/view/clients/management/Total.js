Ext.define('Cement.view.clients.management.Total', {
  extend: 'Cement.view.basic.Tabs',
  
  requires: [
      'Cement.view.clients.management.complex.Events',
      'Cement.view.clients.management.complex.Archive',
      'Cement.view.clients.management.View',
      'Cement.view.clients.management.bottom.ConnectedDocuments',
      'Cement.view.clients.management.bottom.BankAccounts',
      'Cement.view.clients.management.bottom.Employees',
      'Cement.view.clients.management.bottom.Signatures',
      'Cement.view.clients.management.bottom.Warehouses'
  ],

  items: [{
      xtype: 'clients_management_complex_events',
      tabTitle: 'События'
  }, {
      xtype: 'clients_management_complex_archive',
      tabTitle: 'Архив'
  }],

  initComponent: function () {
    Ext.apply(this, {
        title: 'Управление клиентами'
    });
    this.callParent(arguments);
  }
});