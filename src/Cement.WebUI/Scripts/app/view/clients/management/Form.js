Ext.define('Cement.view.clients.management.Form', {
	extend: 'Cement.view.basic.Form',
	alias: 'widget.clients_management_form',
	fieldsDisabled: false,
	url: Cement.Config.url.clients.management.events.saveUrl,
	method: Cement.Config.url.clients.management.events.saveMethod,
	printUrl: Cement.Config.url.clients.management.events.printItem,
	activeItem: 0,
	isFilter: false,
	notCollapseFieldsetIndex: 0,
	width: 500,
	allowBlankFields: false,

	//cars_store_Id: 'transport.auto_control.auxiliary.Cars',
    //car_drivers_store_Id: 'transport.auto_control.auxiliary.Drivers',
	remindersStoreId: 'clients.auxiliary.EventReminderTypes',

	getItems: function () {
		return [
            {
			xtype: 'panel',
			layout: 'anchor',
			border: 0,
			defaults:
            {
				anchor: '100%',
				xtype: 'textfield',
				labelAlign: 'right',
				labelWidth: 130
			},
			items:[
                {
				    xtype: 'hiddenfield',
				    name: 'id'
                },
                this.getSelectorField('Клиент', 'p_personal_client', 'show_clients_window', 130),
                {
				    xtype: this.getDateFieldXType(),
				    fieldLabel: 'Дата',
				    name: 'p_date',
				    allowBlank: this.allowBlankFields,
				    disabled: this.fieldsDisabled
                },
                {
                    xtype: 'timefield',
                    name: 'p_time',
                    format: 'H:i',
                    fieldLabel: 'Время',
                    allowBlank: this.allowBlankFields,
                    disabled: this.fieldsDisabled
                },
                this.getCombo('Напоминания', 'p_reminder', this.remindersStoreId),
                {
                    fieldLabel: 'Тема события',
                    name: 'p_theme',
                    allowBlank: this.allowBlankFields,
                    disabled: this.fieldsDisabled
                },
                {
                    xtype: 'textarea',
                    fieldLabel: 'Описание события',
                    name: 'p_description',
                    maxLength: 2000,
                    allowBlank: this.allowBlankFields,
                    disabled: this.fieldsDisabled
                },
                this.getSelectorField('Связанные документы', 'p_documents', 'show_documents_window', 130)
			]
		}];
	},

	initComponent: function () {
	    this.callParent(arguments);
	    this.down('button[action=show_documents_window]').on('click', function () {
	        this.createDocsWindow();
	        this.docsWindow.show();
	    }, this);
	    this.down('button[action=show_clients_window]').on('click', function () {
	        this.createClientsWindow();
	        this.clientsWindow.show();
	    }, this);
	},

	createDocsWindow: function () {
	    var me = this;
	    if (!me.docsWindow) {
	        me.docsWindow = Ext.create('Cement.view.common.ConnectedDocumentsWindow');
	        me.docsWindow.on('documentsselected', function (ids, names) {
	            me.down('textfield[name=p_documents_display]').setValue(names.join(', '));
	            me.down('hiddenfield[name=p_documents]').setValue(Ext.JSON.encode(ids));
	            me.docsWindow.hide();
	        }, this);
	    }
	},

	createClientsWindow: function () {
	    var me = this;
	    if (!me.clientsWindow) {
	        me.clientsWindow = Ext.create('Cement.view.clients.OrganizationClientWindow');
	        me.clientsWindow.on('signitem', function (model) {
	            me.down('hiddenfield[name=p_personal_client]').setValue(model.get('id'));
	            me.down('textfield[name=p_personal_client_display]').setValue(model.get('p_name'));
	            me.clientsWindow.hide();
	        }, this);
	    }
	},
    
	afterRecordLoad: function (rec) {
	    this.createDocsWindow();
	    this.createClientsWindow();

	    if (rec.get('p_personal_client') > 0) {
	        this.manageFields(true);
	    }
	},

    onCreate: function() {
        this.manageFields(false);
    },

    manageFields: function(disableFields) {
        var item = this.down('button[action=show_clients_window]');

        disableFields ? item.disable() : item.enable();
    }
});