Ext.define('Cement.view.clients.management.complex.Events', {
    extend: 'Cement.view.basic.Complex',
    requires: [
        'Cement.view.clients.management.list.Events',
        'Cement.view.clients.management.Form',

    ],
  alias: 'widget.clients_management_complex_events',
  border: 0,
  enableDetail: false,
  autoLoadStore: false,
  bodyPadding: 0,
  padding: 0,
  header: false,
  rowsCount: 3,
  cls: 'no-side-borders',
  topXType: 'clients_management_list_events',
  topDetailType: 'Cement.view.clients.management.View',
  bottomTitle: 'Связанные документы',
  bottomTabs: [
      {
          title: 'Связанные документы',
          xtype: 'clients_management_bottom_connected_documents',
          detailType: 'Cement.view.goods.auto.view.ConnectedDocument',
          constantFilterParam: 'p_base_doc',
          childrenConstantFilterParam: 'p_base_doc',
          border: 0
      }
  ],
  subBottomTitle: 'Справочник',
  subBottomTabs: [
      {
          title: 'Реквизиты счетов',
          xtype: 'clients_management_bottom_bank_accounts',
          detailType: 'Cement.view.corporate.bank_account.View'
      }, {
          title: 'Склады',
          xtype: 'clients_management_bottom_warehouses',
          detailType: 'Cement.view.corporate.warehouse.View'
      }, {
          title: 'Сотрудники',
          xtype: 'clients_management_bottom_employees',
          detailType: 'Cement.view.corporate.employee.View'
      }, {
          title: 'Подписи',
          xtype: 'clients_management_bottom_signatures',
          detailType: 'Cement.view.corporate.signature.View'
      }
  ]
});