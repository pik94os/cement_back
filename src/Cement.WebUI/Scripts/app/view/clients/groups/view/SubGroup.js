Ext.define('Cement.view.clients.groups.view.SubGroup', {
  extend: 'Cement.view.clients.groups.form.SubGroup',
  alias: 'widget.clients_sub_groups_view',
  fieldsDisabled: true,
  buttons: null,
  border: 0,
  tbar: Cement.Config.modelViewButtons,
  layout: 'autocontainer',
  bbar: null,
  noUserFieldsetsControl: false,

  initComponent: function ()  {
    Ext.apply(this, {
      items: [{
        xtype: 'panel',
        border: 0,
        layout: 'anchor',
        defaultType: 'textfield',
        anchor: '100%',
        bodyPadding: '10 10 10 5',
        autoScroll: true,
        defaults: {
          anchor: '100%',
          labelWidth: 110
        },
        items: this.getItems()
      }]
    });
    this.callParent(arguments);
  }
});