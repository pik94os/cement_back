Ext.define('Cement.view.clients.groups.Total', {
    extend: 'Cement.view.basic.Tabs',

    initComponent: function () {
        Ext.apply(this, {
            title: 'Группы/подгруппы',
            items: [{
                title: 'Группы',
                layout: 'fit',
                xtype: 'clients_groups_list_groups',
                detailType: 'Cement.view.clients.groups.view.Group'
            }, {
                title: 'Подгруппы',
                layout: 'fit',
                xtype: 'clients_groups_list_sub_groups',
                detailType: 'Cement.view.clients.groups.view.SubGroup'
            }]
        });
        this.callParent(arguments);
    }
});