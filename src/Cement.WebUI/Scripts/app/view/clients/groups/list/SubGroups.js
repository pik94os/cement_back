Ext.define('Cement.view.clients.groups.list.SubGroups', {
    extend: 'Cement.view.basic.Grid',
    alias: 'widget.clients_groups_list_sub_groups',

    gridStore: 'clients.subgroups.List',
    gridStateId: 'stateGroupsList',

    printUrl: Cement.Config.url.clients.groups.printGrid,
    helpUrl: Cement.Config.url.clients.groups.help,
    deleteUrl: Cement.Config.url.clients.groups.deleteUrl,
    printItemUrl: Cement.Config.url.clients.groups.printItem,

    bbarText: 'Показаны подгруппы {0} - {1} из {2}',
    bbarEmptyMsg: 'Нет подгрупп',
    bbarUsersText: 'Подгрупп на странице: ',

    shownTitle: 'Подгруппы',
    //cls: 'simple-form',

    //creatorTree: Ext.clone(Cement.Creators.clients),
    //createWindowTitle: 'Создать',
    autoLoadStore: true,

    getActionColumns: function () {
        return {
            xtype: 'rowactions',
            hideable: false,
            resizeable: false,
            locked: true,
            width: 46,
            actions: [{
                iconCls: 'icon-edit-item',
                qtip: 'Редактировать',
                callback: this.editItem
            }, {
                iconCls: 'icon-delete-item',
                qtip: 'Удалить',
                callback: this.deleteItem
            }],
            keepSelection: true
        };
    },

    getFilterItems: function () {
        return [{
            text: 'Группа',
            kind: 'selector',
            field_name: 'p_group',
            checked: true
        }, {
            text: 'Наименование',
            kind: 'selector',
            field_name: 'p_name',
            checked: true
        }
        //, {
        //    text: 'Описание',
        //    kind: 'selector',
        //    field_name: 'p_description',
        //    checked: true
        //}
        ];
    },

    getGridColumns: function () {
        var result = [
            { text: 'Группа', dataIndex: 'p_group_display', width: 200 },
            { text: 'Наименование', dataIndex: 'p_name', flex: 1 },
            //{ text: 'Описание', dataIndex: 'p_description', flex: 1 }
        ];
        return this.mergeActions(result);
    },

    initComponent: function () {
        Ext.apply(this, {
            title: this.shownTitle
        });
        this.callParent(arguments);
    },

    afterRecordDelete: function () {
        Ext.getStore('clients.Groups').load();
    }
});