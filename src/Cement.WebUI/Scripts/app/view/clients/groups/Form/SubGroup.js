Ext.define('Cement.view.clients.groups.form.SubGroup', {
  extend: 'Cement.view.basic.Form',
  alias: 'widget.clients_groups_form_sub_group',

  fieldsDisabled: false,
  url: Cement.Config.url.clients.sub_groups.saveUrl,
  method: Cement.Config.url.clients.sub_groups.saveMethod,
  printUrl: Cement.Config.url.clients.sub_groups.printItem,
  activeItem: 0,
  isFilter: false,
  padding: '0',
  // totalPanelHeight: 630,
  //clientGroupsStoreId: 'clients.Groups',
  clientsGroupsStoreId: 'clients.groups.List',

  getItems: function () {
    /*this.topLevelStore = Ext.create('Ext.data.Store', {
      fields: ['id', 'p_name']
    });*/
    return [{
      layout: 'anchor',
      xtype: 'panel',
      border: 0,
      defaults: {
        anchor: '100%',
        labelWidth: 110
      },
      items: [{
        xtype: 'fieldset',
        border: 0,
        margin: 0,
        padding: 10,
        defaults: {
          xtype: 'textfield',
          anchor: '100%'
        },
        items: [{
          xtype: 'hiddenfield',
          name: 'id'
        }, {
          xtype: 'textfield',
          fieldLabel: 'Наименование',
          disabled: this.fieldsDisabled,
          name: 'p_name'
        },
        this.getCombo('Группа', 'p_group', this.clientsGroupsStoreId, null, false, null, true, 'remote'),
        {
          fieldLabel: 'Описание',
          xtype: 'textarea',
          name: 'p_description',
          height: 150
          //disabled: this.fieldsDisabled
        }]
      }]
    }];
  },

  initComponent: function () {
      this.callParent(arguments);

      //var store = Ext.getStore(this.clientsGroupsStoreId);
      //store.reload();

       /*var store = Ext.getStore(this.clientGroupsStoreId),
        me = this;
        var populateStore = function () {
        me.topLevelStore.removeAll();
        store.getRootNode().eachChild(function (node) {
        me.topLevelStore.add({
              id: node.get('id'),
              p_name: node.get('text')
            });
          });
        };
        store.on('load', populateStore);
        populateStore();*/
  },

  afterSubmit: function () {
     // Ext.getStore('clients.Groups').load();
  }
});