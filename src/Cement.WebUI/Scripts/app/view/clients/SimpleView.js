Ext.define('Cement.view.clients.SimpleView', {
  extend: 'Cement.view.clients.Form',
  alias: 'widget.clients_simple_view',
  fieldsDisabled: true,
  buttons: null,
  border: 0,
  tbar: Cement.Config.modelViewButtons,
  layout: 'autocontainer',
  bbar: null,
  noUserFieldsetsControl: false,

  getItems: function () {
    return [{
      layout: 'anchor',
      xtype: 'panel',
      border: 0,
      defaults: {
        anchor: '100%',
        labelWidth: 110
      },
      items: [{
        xtype: 'fieldset',
        border: 0,
        margin: 0,
        padding: 10,
        defaults: {
          xtype: 'textfield',
          anchor: '100%'
        },
        items: [{
          xtype: 'textfield',
          fieldLabel: 'Форма собственности',
          disabled: true,
          name: 'p_firm_form'
        }, {
          xtype: 'textfield',
          fieldLabel: 'Наименование',
          anchor: '100%',
          disabled: true,
          name: 'p_firm_name'
        }, {
          fieldLabel: 'Полное наименование',
          name: 'p_firm_full_name',
          disabled: true
        }, {
          fieldLabel: 'Бренд',
          name: 'p_firm_brand',
          disabled: true
        }]
      }]
    }, this.getUrAddress(),
      this.getFactAddress(),
      this.getRegData(),
      this.getConnectData()
    ];
  },

  initComponent: function ()  {
    Ext.apply(this, {
      items: [{
        xtype: 'panel',
        border: 0,
        layout: 'anchor',
        defaultType: 'textfield',
        anchor: '100%',
        bodyPadding: '10 10 10 5',
        autoScroll: true,
        defaults: {
          anchor: '100%',
          labelWidth: 110
        },
        items: this.getItems()
      }]
    });
    this.callParent(arguments);
  }
});