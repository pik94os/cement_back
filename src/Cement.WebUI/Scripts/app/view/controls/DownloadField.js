Ext.define('Cement.view.controls.DownloadField', {
    extend: 'Ext.form.field.Base',
    alias: 'widget.downloadfield',
    fieldSubTpl: [ // note: {id} here is really {inputId}, but {cmpId} is available
        // '<input id="{id}" type="text" {inputAttrTpl}',
        //     ' size="1"', // allows inputs to fully respect CSS widths across all browsers
        //     '<tpl if="name"> name="{name}"</tpl>',
        //     '<tpl if="value"> value="{[Ext.util.Format.htmlEncode(values.value)]}"</tpl>',
        //     '<tpl if="readOnly"> readonly="readonly"</tpl>',
        //     '<tpl if="disabled"> disabled="disabled"</tpl>',
        //     '<tpl if="tabIdx"> tabIndex="{tabIdx}"</tpl>',
        //     '<tpl if="fieldStyle"> style="{fieldStyle}"</tpl>',
        // ' class="{fieldCls} {typeCls} {editableCls}"/>',
        '<a id="{id}" {inputAttrTpl} target="_blank" class="x-btn x-btn-default-small link-button"',
        '<tpl if="value"> href="{[Ext.util.Format.htmlEncode(values.value)]}"</tpl>',
        '>скачать</a>',
        {
            disableFormats: true
        }
    ],

    setRawValue: function (value) {
        var me = this;
        value = Ext.value(me.transformRawValue(value), '');
        me.rawValue = value;

        // Some Field subclasses may not render an inputEl
        if (me.inputEl) {
            if (value == '') {
                value = '#';
                me.inputEl.dom.target = "_self";
            }
            else {
                me.inputEl.dom.target = "_blank";
            }
            me.inputEl.dom.href = value;
        }
        return value;
    },

    getRawValue: function () {
        var me = this,
            v = (me.inputEl ? me.inputEl.href : Ext.value(me.rawValue, ''));
        me.rawValue = v;
        return v;
    }
});