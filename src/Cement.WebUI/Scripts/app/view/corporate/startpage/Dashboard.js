Ext.define('Cement.view.corporate.startpage.Dashboard', {
    extend: 'Ext.panel.Panel',
    xtype: 'corporate_startpage_dashboard',
    title: 'Личная страница',
    cls: 'simple-form dashboard',
    autoScroll: true,

    layout: {
        type: 'vbox',
        align: 'stretch',
        pack: 'start'
    },
    bodyPadding: '10 20',

    items: [{
        xtype: 'form',
        layout: 'hbox',
        border: 0,
        margin: '0 0 10 0',
        items: [{
            xtype: 'image',
            role: 'photo',
            cls: 'person-photo',
            src: '',
            margin: '8 0 0 0',
            width: 200,
            height: 'auto'
        }, {
            xtype: 'fieldset',
            title: 'Сотрудник',
            collapsed: false,
            collapsible: false,
            flex: 1,
            margin: '0 0 0 10',
            defaults: {
                anchor: '100%',
                xtype: 'textfield',
                disabled: true,
                labelWidth: 90,
                labelAlign: 'right'
            },
            items: [{
                name: 'p_emp_name',
                fieldLabel: 'Ф.И.О.'
            }, {
                name: 'p_emp_department',
                fieldLabel: 'Отдел'
            }, {
                name: 'p_emp_position',
                fieldLabel: 'Должность'
            }, {
                name: 'p_emp_phone',
                fieldLabel: 'Телефон'
            }, {
                name: 'p_emp_mobile',
                fieldLabel: 'Моб. телефон'
            }, {
                name: 'p_emp_email',
                fieldLabel: 'E-mail'
            }]
        }, {
            xtype: 'fieldset',
            title: 'Организация',
            role: 'firm',
            collapsed: false,
            collapsible: false,
            flex: 1,
            margin: '0 0 0 10',
            defaults: {
                anchor: '100%',
                xtype: 'textfield',
                disabled: true,
                labelWidth: 150,
                labelAlign: 'right'
            },
            items: [{
                xtype: 'container',
                border: 0,
                height: 'auto',
                margin: '0 0 10 0',
                style: {
                    'text-align': 'center',
                    'background': 'transparent'
                },
                items: [{
                    xtype: 'image',
                    src: '',
                    width: 150,
                    role: 'logo',
                    height: 'auto',
                    margin: '0 auto',
                    cls: 'firm-logo',
                    maskOnDisable: false,
                    style: {
                        'display': 'inline'
                    }
                }]
            }, {
                name: 'p_firm_name',
                fieldLabel: 'Наименование'
            }, {
                name: 'p_firm_ur_address',
                fieldLabel: 'Юридический адрес'
            }, {
                name: 'p_firm_fact_address',
                fieldLabel: 'Фактический адрес'
            }, {
                name: 'p_firm_web',
                fieldLabel: 'Сайт'
            }, {
                name: 'p_firm_inn',
                fieldLabel: 'ИНН'
            }]
        }]
    }, {
        xtype: 'fieldset',
        collapsible: true,
        collapsed: false,
        title: 'События',
        items: [{
            xtype: 'treepanel',
            role: 'events-panel',
            cls: 'tree-no-border',
            border: 0,
            rootVisible: true,
            store: Ext.create('Ext.data.TreeStore', {
                fields: ['id', 'p_name',
                  'p_new_total', 'p_new',
                  'p_work_total', 'p_work',
                  'p_sign_total', 'p_sign',
                  'p_agree_total', 'p_agree',
                  'p_control_total', 'p_control',
                  'p_route_1', 'p_route_2',
                  'p_route_3', 'p_route_4',
                  'p_route_5'
                ],
                autoLoad: false,
                root: {
                    expanded: true
                }
            }),
            header: false,
            hideHeaders: true,
            columns: [{
                xtype: 'treecolumn',
                text: 'Task',
                flex: 2,
                sortable: true,
                dataIndex: 'p_name'
            }, {
                flex: 1,
                dataIndex: 'p_new_total',
                renderer: function (v, rec) {
                    return '<span class="dashboard-nav">Новые ' + rec.record.get('p_new_total') + ' <span class="green">(' + rec.record.get('p_new') + ')</span></span>';
                }
            }, {
                flex: 1,
                dataIndex: 'p_work_total',
                renderer: function (v, rec) {
                    return '<span class="dashboard-nav">Исполнение ' + rec.record.get('p_work_total') + ' <span class="green">(' + rec.record.get('p_work') + ')</span></span>';
                }
            }, {
                flex: 1,
                dataIndex: 'p_sign_total',
                renderer: function (v, rec) {
                    return '<span class="dashboard-nav">Подписание ' + rec.record.get('p_sign_total') + ' <span class="green">(' + rec.record.get('p_sign') + ')</span></span>';
                }
            }, {
                flex: 1,
                dataIndex: 'p_agree_total',
                renderer: function (v, rec) {
                    return '<span class="dashboard-nav">Согласование ' + rec.record.get('p_agree_total') + ' <span class="green">(' + rec.record.get('p_agree') + ')</span></span>';
                }
            }, {
                flex: 1,
                dataIndex: 'p_control_total',
                renderer: function (v, rec) {
                    return '<span class="dashboard-nav">Контроль ' + rec.record.get('p_control_total') + ' <span class="green">(' + rec.record.get('p_control') + ')</span></span>';
                }
            }, {
                xtype: 'actioncolumn',
                hideable: false,
                resizeable: false,
                width: 23,
                items: [{
                    iconCls: 'icon-edit-item',
                    tooltip: 'Редактировать',
                    handler: function (grid, rowIndex, colIndex) {
                        var rec = grid.getStore().getAt(rowIndex);
                        alert("Edit " + rec.get('firstname'));
                    }
                }]
            }]
        }]
    }, {
        xtype: 'fieldset',
        collapsible: true,
        collapsed: false,
        title: 'Новости',
        items: [{
            xtype: 'grid',
            margin: '10 0 0 0',
            hideHeaders: true,
            store: Ext.create('Ext.data.Store', {
                fields: ['id', 'p_name', 'p_date']
            }),
            columns: [{
                dataIndex: 'p_name',
                flex: 5
            }, {
                dataIndex: 'p_date',
                flex: 0.5
            }]
        }]
    }],

    initComponent: function () {
        this.callParent(arguments);

        var me = this,
          photoImg = this.down('image[role=photo]'),
          logoImg = this.down('image[role=logo]'),
          //fs = this.down('fieldset[role=firm]'),
          form = this.down('form'),
          evTree = this.down('treepanel');

        logoImg.on('render', function (c) {
            c.getEl().on('load', function () {
                if (form.getHeight() < form.getHeight()) {
                    form.setHeight(form.getHeight() + 20);
                }
            });
        });

        photoImg.on('render', function (c) {
            c.getEl().on('load', function () {
                if (form.getHeight() < photoImg.getEl().dom.height) {
                    form.setHeight(photoImg.getEl().dom.height + 20);
                }
            });
        });

        Ext.Ajax.request({
            url: Cement.Config.url.corporate.dashboard,
            method: 'POST',
            success: function (response) {
                var obj = Ext.decode(response.responseText);

                photoImg.setSrc(obj.info.p_photo || Cement.Config.user_default_photo);
                logoImg.setSrc(obj.info.p_logo || Cement.Config.default_logo);
                form.getForm().setValues(obj.info);
                evTree.getStore().setRootNode(obj.events[0]);
                me.down('grid').getStore().loadData(obj.news);
            }
        });

        this.down('treepanel[role=events-panel]').on('cellclick', function (panel, td, cellIndex, record) {
            this.fireEvent('sysnav', record.get('p_route_' + cellIndex));
        }, this);
    }
});
