Ext.define('Cement.view.corporate.firm.View', {
    extend: 'Cement.view.basic.Form',
    alias: 'widget.firmview',
    bodyPadding: 10,
    fieldsDisabled: true,
    buttons: null,
    border: 0,
    tbar: Cement.Config.modelViewButtons,
    layout: 'autocontainer',
    bbar: null,
    noUserFieldsetsControl: false,
    bodyCls: 'model-form',
    loadItemUrl: Cement.Config.url.corporate.firms.loadItem,
    
    getBasicFieldSet: function () {
        return [
            {
                xtype: 'hiddenfield',
                name: 'id'
            },
            {
                xtype: 'textfield',
                disabled: this.fieldsDisabled,
                fieldLabel: 'Форма собственности',
                name: 'p_ownership'
            },
            {
                xtype: 'textfield',
                fieldLabel: 'Наименование',
                name: 'p_name',
                allowBlank: false,
                disabled: this.fieldsDisabled
            },
            {
                xtype: 'textfield',
                fieldLabel: 'Полное наименование',
                name: 'p_full_name',
                allowBlank: false,
                disabled: this.fieldsDisabled
            },
            {
                xtype: 'textfield',
                fieldLabel: 'Бренд',
                name: 'p_brand',
                allowBlank: false,
                disabled: this.fieldsDisabled
            }
        ];
    },
    
    getJurAddressFields: function () {
        return [
            {
                fieldLabel: 'Индекс',
                disabled: this.fieldsDisabled,
                name: 'p_law_index'
            },
            {
                fieldLabel: 'Страна',
                disabled: this.fieldsDisabled,
                name: 'p_law_country'
            },
            {
                fieldLabel: 'Регион',
                disabled: this.fieldsDisabled,
                name: 'p_law_region'
            },
            {
                fieldLabel: 'Район',
                disabled: this.fieldsDisabled,
                name: 'p_law_district'
            },
            {
                fieldLabel: 'Населенный пункт',
                disabled: this.fieldsDisabled,
                name: 'p_law_city'
            },
            {
                fieldLabel: 'Улица',
                disabled: this.fieldsDisabled,
                name: 'p_law_street'
            },
            {
                fieldLabel: 'Дом',
                disabled: this.fieldsDisabled,
                name: 'p_law_house'
            },
            {
                fieldLabel: 'Корпус',
                disabled: this.fieldsDisabled,
                name: 'p_law_housepart'
            },
            {
                fieldLabel: 'Строение',
                disabled: this.fieldsDisabled,
                name: 'p_law_building'
            },
            {
                fieldLabel: 'Офис',
                disabled: this.fieldsDisabled,
                name: 'p_law_office'
            }
        ];
    },
    
    getFactAddressFields: function () {
        return [
            {
                fieldLabel: 'Индекс',
                disabled: this.fieldsDisabled,
                name: 'p_fact_index'
            },
            {
                fieldLabel: 'Страна',
                disabled: this.fieldsDisabled,
                name: 'p_fact_country'
            },
            {
                fieldLabel: 'Регион',
                disabled: this.fieldsDisabled,
                name: 'p_fact_region'
            },
            {
                fieldLabel: 'Район',
                disabled: this.fieldsDisabled,
                name: 'p_fact_district'
            },
            {
                fieldLabel: 'Населенный пункт',
                disabled: this.fieldsDisabled,
                name: 'p_fact_city'
            },
            {
                fieldLabel: 'Улица',
                disabled: this.fieldsDisabled,
                name: 'p_fact_street'
            },
            {
                fieldLabel: 'Дом',
                disabled: this.fieldsDisabled,
                name: 'p_fact_house'
            },
            {
                fieldLabel: 'Корпус',
                disabled: this.fieldsDisabled,
                name: 'p_fact_housepart'
            },
            {
                fieldLabel: 'Строение',
                disabled: this.fieldsDisabled,
                name: 'p_fact_building'
            },
            {
                fieldLabel: 'Офис',
                disabled: this.fieldsDisabled,
                name: 'p_fact_office'
            }
        ];
    },
    
    getContactFields: function () {
        return [
            {
                fieldLabel: 'Телефон',
                disabled: this.fieldsDisabled,
                name: 'p_phone'
            },
            {
                fieldLabel: 'Факс',
                disabled: this.fieldsDisabled,
                name: 'p_fax'
            },
            {
                fieldLabel: 'E-mail',
                disabled: this.fieldsDisabled,
                name: 'p_email'
            },
            {
                fieldLabel: 'WEB',
                disabled: this.fieldsDisabled,
                name: 'p_web'
            }
        ];
    },
    
    getRegDataFields: function () {
        return [
            {
                fieldLabel: 'ОГРН',
                disabled: this.fieldsDisabled,
                name: 'p_ogrn'
            },
            this.getFileField('p_ogrn_file', 'ОГРН'),
            {
                fieldLabel: 'ИНН',
                disabled: this.fieldsDisabled,
                name: 'p_inn'
            },
            this.getFileField('p_inn_file', 'ИНН'),
            {
                fieldLabel: 'КПП',
                disabled: this.fieldsDisabled,
                name: 'p_kpp'
            },
            {
                fieldLabel: 'ОКПО',
                disabled: this.fieldsDisabled,
                name: 'p_okpo'
            },
            {
                fieldLabel: 'ОКВЭД',
                disabled: this.fieldsDisabled,
                name: 'p_okved'
            },
            {
                fieldLabel: 'ОКАТО',
                disabled: this.fieldsDisabled,
                name: 'p_okato'
            },
            {
                fieldLabel: 'ОКОНХ',
                disabled: this.fieldsDisabled,
                name: 'p_okonh'
            },
            this.getFileField('Устав', 'p_charter', '0 66 5 0')
        ];
    },
    
    getShippingFields: function () {
        return [
            {
                fieldLabel: 'Код предприятия',
                disabled: this.fieldsDisabled,
                name: 'p_firm_code'
            },
            {
                xtype: 'textfield',
                disabled: this.fieldsDisabled,
                fieldLabel: 'Станция',
                name: 'p_station'
            },
            {
                xtype: 'textfield',
                disabled: this.fieldsDisabled,
                fieldLabel: 'Код станции',
                name: 'p_station_code'
            },
            {
                xtype: 'textfield',
                disabled: this.fieldsDisabled,
                fieldLabel: 'Дорога',
                name: 'p_road'
            }
        ];
    },

    getItems: function() {
        return [
            {
                xtype: 'cementimage'
            },
            {
                xtype: 'fieldset',
                border: 0,
                margin: 0,
                // padding: '0 110',
                defaults: {
                    xtype: 'textfield',
                    anchor: '100%'
                },
                items: this.getBasicFieldSet()
            }, {
                xtype: 'fieldset',
                collapsible: true,
                collapsed: true,
                title: 'Юридический адрес',
                defaults: {
                    xtype: 'textfield',
                    anchor: '100%',
                    labelWidth: 110
                },
                items: this.getJurAddressFields()
            }, {
                xtype: 'fieldset',
                collapsible: true,
                collapsed: true,
                title: 'Фактический адрес',
                defaults: {
                    xtype: 'textfield',
                    anchor: '100%',
                    labelWidth: 110
                },
                items: this.getFactAddressFields()
            }, {
                xtype: 'fieldset',
                collapsible: true,
                collapsed: true,
                title: 'Связь',
                defaults: {
                    xtype: 'textfield',
                    anchor: '100%',
                    labelWidth: 110
                },
                items: this.getContactFields()
            }, {
                xtype: 'fieldset',
                collapsible: true,
                collapsed: true,
                title: 'Регистрационные данные',
                defaults: {
                    xtype: 'textfield',
                    anchor: '100%',
                    labelWidth: 110
                },
                items: this.getRegDataFields()
            }, {
                xtype: 'fieldset',
                collapsible: true,
                collapsed: true,
                title: 'Отгрузочные реквизиты',
                defaults: {
                    xtype: 'textfield',
                    anchor: '100%',
                    labelWidth: 110
                },
                items: this.getShippingFields()
            }
        ];
    },

    initComponent: function () {
        Ext.apply(this, {
            items: [
                {
                    xtype: 'panel',
                    border: 0,
                    layout: 'anchor',
                    defaultType: 'textfield',
                    anchor: '100%',
                    bodyPadding: '10 10 10 5',
                    autoScroll: true,
                    defaults: {
                        anchor: '100%',
                        labelWidth: 110
                    },
                    items: this.getItems()
                }
            ]
        });
        this.callParent(arguments);
    }
});