Ext.define('Cement.view.corporate.firm.Form', {
    extend: 'Ext.form.Panel',
    alias: 'widget.firmform',
    bodyCls: 'model-form',
    fieldsDisabled: true,
    bankPropertyMargin: '5px 0 5px 40px',
    bankPropertyHeaderMargin: '5px 5px 15px 5px',
    printUrl: Cement.Config.url.corporate.firms.print,

    getPrintUrl: function () {
        return this.printUrl;
    },

    getFileField: function (fieldName, fieldLabel, fieldValue) {
        var result = {
            fieldLabel: fieldLabel,
            xtype: 'filefield',
            anchor: '80%',
            buttonText: 'Загрузить',
            name: fieldName
        };
        if (this.fieldsDisabled) {
            result = {
                fieldLabel: fieldLabel,
                xtype: 'downloadfield',
                value: fieldValue,
                name: fieldName
            };
        }
        return result;
    },

    getImgField: function (fieldName, fieldLabel) {
        var result = null;
        if (!this.fieldsDisabled) {
            result = {
                fieldLabel: fieldLabel,
                xtype: 'filefield',
                anchor: '80%',
                buttonText: 'Загрузить',
                name: fieldName
            };
        }
        return result;
    },

    loadRecord: function (rec) {
        if (!rec.get) return;
        var src = rec.get('p_img');
        if (src != '') {
            this.down('image').setSrc(rec.get('p_img'));
            this.down('image').doComponentLayout();
        }
        this.getForm().loadRecord(rec);
    },

    createTopRow: function () {
        return {
            xtype: 'panel',
            layout: 'hbox',
            border: 0,
            items: [
                {
                    xtype: 'fieldset',
                    flex: 1,
                    border: 0,
                    layout: 'anchor',
                    defaults: {
                        anchor: '99%',
                        labelWidth: 140
                    },
                    items: [
                        {
                            xtype: 'hiddenfield',
                            name: 'id'
                        },
                        {
                            xtype: 'textfield',
                            fieldLabel: 'Форма собственности',
                            disabled: this.fieldsDisabled,
                            name: 'p_ownership'
                        },
                        {
                            xtype: 'textfield',
                            fieldLabel: 'Наименование',
                            name: 'p_name',
                            allowBlank: false,
                            disabled: this.fieldsDisabled
                        },
                        {
                            xtype: 'textfield',
                            fieldLabel: 'Полное наименование',
                            name: 'p_full_name',
                            allowBlank: false,
                            disabled: this.fieldsDisabled
                        },
                        {
                            xtype: 'textfield',
                            fieldLabel: 'Бренд',
                            disabled: this.fieldsDisabled,
                            name: 'p_brand'
                        },
                        this.getImgField('p_img', 'Логотип')
                    ]
                },
                {
                    xtype: 'image',
                    src: Cement.Config.default_photo,
                    maxWidth: 83,
                    maxHeight: 100,
                    margin: '20 0 0 0'
                }
            ]
        };
    },

    initComponent: function () {
        Ext.apply(this, {
            bodyPadding: '10 26 10 15',
            trackResetOnLoad: true,
            layout: 'anchor',
            defaultType: 'textfield',
            // overflowY: 'scroll',
            autoScroll: true,
            defaults: {
                anchor: '100%',
                labelWidth: 150
            },
            items: [this.createTopRow(), {
                xtype: 'fieldset',
                title: 'Юридический адрес',
                defaultType: 'textfield',
                collapsible: true,
                collapsed: true,
                disabled: this.fieldsDisabled,
                defaults: {
                    anchor: '100%',
                    labelWidth: 140
                },
                layout: 'anchor',
                items: [
                    {
                        fieldLabel: 'Индекс',
                        disabled: this.fieldsDisabled,
                        name: 'p_law_index'
                    },
                    {
                        fieldLabel: 'Страна',
                        disabled: this.fieldsDisabled,
                        name: 'p_law_country'
                    },
                    {
                        fieldLabel: 'Регион',
                        disabled: this.fieldsDisabled,
                        name: 'p_law_region'
                    },
                    {
                        fieldLabel: 'Район',
                        disabled: this.fieldsDisabled,
                        name: 'p_law_district'
                    },
                    {
                        fieldLabel: 'Населенный пункт',
                        disabled: this.fieldsDisabled,
                        name: 'p_law_city'
                    },
                    {
                        fieldLabel: 'Улица',
                        disabled: this.fieldsDisabled,
                        name: 'p_law_street'
                    },
                    {
                        fieldLabel: 'Дом',
                        disabled: this.fieldsDisabled,
                        name: 'p_law_house'
                    },
                    {
                        fieldLabel: 'Корпус',
                        disabled: this.fieldsDisabled,
                        name: 'p_law_housepart'
                    },
                    {
                        fieldLabel: 'Строение',
                        disabled: this.fieldsDisabled,
                        name: 'p_law_building'
                    },
                    ,
                    {
                        fieldLabel: 'Офис',
                        disabled: this.fieldsDisabled,
                        name: 'p_law_office'
                    }
                ]
            }, {
                xtype: 'fieldset',
                title: 'Фактический адрес',
                defaultType: 'textfield',
                collapsible: true,
                collapsed: true,
                defaults: {
                    anchor: '100%',
                    labelWidth: 140
                },
                layout: 'anchor',
                items: [
                    {
                        fieldLabel: 'Индекс',
                        disabled: this.fieldsDisabled,
                        name: 'p_fact_index'
                    },
                    {
                        fieldLabel: 'Страна',
                        disabled: this.fieldsDisabled,
                        name: 'p_fact_country'
                    },
                    {
                        fieldLabel: 'Регион',
                        disabled: this.fieldsDisabled,
                        name: 'p_fact_region'
                    },
                    {
                        fieldLabel: 'Район',
                        disabled: this.fieldsDisabled,
                        name: 'p_fact_district'
                    },
                    {
                        fieldLabel: 'Населенный пункт',
                        disabled: this.fieldsDisabled,
                        name: 'p_fact_city'
                    },
                    {
                        fieldLabel: 'Улица',
                        disabled: this.fieldsDisabled,
                        name: 'p_fact_street'
                    },
                    {
                        fieldLabel: 'Дом',
                        disabled: this.fieldsDisabled,
                        name: 'p_fact_house'
                    },
                    {
                        fieldLabel: 'Корпус',
                        disabled: this.fieldsDisabled,
                        name: 'p_fact_housepart'
                    },
                    {
                        fieldLabel: 'Строение',
                        disabled: this.fieldsDisabled,
                        name: 'p_fact_building'
                    },
                    ,
                    {
                        fieldLabel: 'Офис',
                        disabled: this.fieldsDisabled,
                        name: 'p_fact_office'
                    }
                ]
            }, {
                xtype: 'fieldset',
                title: 'Связь',
                defaultType: 'textfield',
                collapsible: true,
                collapsed: true,
                defaults: {
                    anchor: '100%',
                    labelWidth: 140
                },
                layout: 'anchor',
                items: [
                    {
                        fieldLabel: 'Телефон',
                        disabled: this.fieldsDisabled,
                        name: 'p_phone'
                    },
                    {
                        fieldLabel: 'Факс',
                        disabled: this.fieldsDisabled,
                        name: 'p_fax'
                    },
                    {
                        fieldLabel: 'E-mail',
                        disabled: this.fieldsDisabled,
                        name: 'p_email'
                    },
                    {
                        fieldLabel: 'WEB',
                        disabled: this.fieldsDisabled,
                        name: 'p_web'
                    }
                ]
            }, {
                xtype: 'fieldset',
                title: 'Регистрационные данные',
                defaultType: 'textfield',
                collapsible: true,
                collapsed: true,
                defaults: {
                    anchor: '100%',
                    labelWidth: 140
                },
                layout: 'anchor',
                items: [
                    {
                        fieldLabel: 'ОГРН',
                        disabled: this.fieldsDisabled,
                        name: 'p_ogrn'
                    },
                    this.getFileField('p_ogrn_file', 'ОГРН'),
                    {
                        fieldLabel: 'ИНН',
                        disabled: this.fieldsDisabled,
                        name: 'p_inn'
                    },
                    this.getFileField('p_inn_file', 'ИНН'),
                    {
                        fieldLabel: 'КПП',
                        disabled: this.fieldsDisabled,
                        name: 'p_kpp'
                    },
                    {
                        fieldLabel: 'ОКПО',
                        disabled: this.fieldsDisabled,
                        name: 'p_okpo'
                    },
                    {
                        fieldLabel: 'ОКВЭД',
                        disabled: this.fieldsDisabled,
                        name: 'p_okved'
                    },
                    {
                        fieldLabel: 'ОКАТО',
                        disabled: this.fieldsDisabled,
                        name: 'p_okato'
                    },
                    {
                        fieldLabel: 'ОКОНХ',
                        disabled: this.fieldsDisabled,
                        name: 'p_okonh'
                    },
                    this.getFileField('p_charter', 'Устав')
                ]
            }, {
                xtype: 'fieldset',
                title: 'Отгрузочные реквизиты',
                defaultType: 'textfield',
                collapsible: true,
                collapsed: true,
                defaults: {
                    anchor: '100%',
                    labelWidth: 140
                },
                layout: 'anchor',
                items: [
                    {
                        fieldLabel: 'Код предприятия',
                        disabled: this.fieldsDisabled,
                        name: 'p_firm_code'
                    },
                    {
                        xtype: 'textfield',
                        disabled: this.fieldsDisabled,
                        fieldLabel: 'Станция',
                        name: 'p_station'
                    },
                    {
                        xtype: 'textfield',
                        disabled: this.fieldsDisabled,
                        fieldLabel: 'Код станции',
                        name: 'p_station_code'
                    },
                    {
                        xtype: 'textfield',
                        disabled: this.fieldsDisabled,
                        fieldLabel: 'Дорога',
                        name: 'p_road'
                    }
                ]
            }]
        });
        this.callParent(arguments);
    }
});