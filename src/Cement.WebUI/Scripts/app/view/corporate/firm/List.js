Ext.define('Cement.view.corporate.firm.List', {
    extend: 'Cement.view.basic.Grid',
    alias: 'widget.firmlist',
    bbarText: 'Показаны организации {0} - {1} из {2}',
    bbarEmptyMsg: 'Нет организаций',
    bbarUsersText: 'Организаций на странице: ',
    gridStore: 'Carriages',
    gridStoreId: 'carriagesStore',
    gridStateId: 'stateCarriagesList',
    gridId: 'carriages_grid',
    printUrl: Cement.Config.url.corporate.firms.printGrid,
    helpUrl: Cement.Config.url.corporate.firms.help,
    deleteUrl: Cement.Config.url.corporate.firms.deleteUrl,
    printItemUrl: Cement.Config.url.corporate.firms.printItem,
    filterControl: 'Cement.view.carriage.Filter',
    formControl: 'Cement.view.carriage.Form',
    windowParams: {
        height: 370
    },
    formParams: {
        formHeight: 270
    },
    listTitle: 'Вагон',

    getFilterItems: function () {
        return [
            {
                text: 'Наименование',
                kind: 'selector',
                field_name: 'p_name',
                checked: true
            },
            {
                text: 'Адрес',
                kind: 'selector',
                field_name: 'p_address',
                checked: true
            },
            {
                text: 'ИНН',
                kind: 'selector',
                field_name: 'p_inn',
                checked: true
            },
            {
                text: 'КПП',
                kind: 'selector',
                field_name: 'p_kpp',
                checked: true
            },
            {
                text: 'ОГРН',
                kind: 'selector',
                field_name: 'p_ogrn',
                checked: true
            }
        ];
    },

    getGridColumns: function () {
        var result = [
            { text: 'Наименование', dataIndex: 'p_name', flex: 1 },
            { text: 'Адрес', dataIndex: 'p_address', flex: 1 },
            { text: 'ИНН', dataIndex: 'p_inn', flex: 1 },
            { text: 'КПП', dataIndex: 'p_kpp', flex: 1 },
            { text: 'ОГРН', dataIndex: 'p_ogrn', flex: 1 }
        ];
        return this.mergeActions(result);
    }
});