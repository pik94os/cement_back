Ext.define('Cement.view.corporate.firm.ClientView', {
    extend: 'Cement.view.corporate.firm.Form',
    alias: 'widget.firmclientview',
    method: 'POST',
    cls: 'simple-form',

    initComponent: function () {
        Ext.apply(this, {
            border: 0,
            fieldsDisabled: true,
            title: 'Реквизиты',
            dockedItems: [
                {
                    xtype: 'toolbar',
                    dock: 'top',
                    items: ['->', {
                        text: 'Печать',
                        iconCls: 'icon-print',
                        action: 'print'
                    }, {
                        text: 'Помощь',
                        iconCls: 'icon-help',
                        action: 'help'
                    }]
                }
            ]
        });
        this.callParent(arguments);
        this.down('button[action=help]').on('click', function () {
            window.open(
                Cement.Config.url.corporate.firms.help
            );
        }, true);
        var me = this;
        Ext.Ajax.request({
            url: Cement.Config.url.corporate.my_firm,
            success: function (response) {
                var data = Ext.JSON.decode(response.responseText);
                me.getForm().setValues(data);
            }
        });
    }
});