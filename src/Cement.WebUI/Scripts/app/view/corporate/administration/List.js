Ext.define('Cement.view.corporate.administration.List', {
    extend: 'Cement.view.basic.Grid',
    alias: 'widget.administrationlist',
    bbarText: 'Показаны записи {0} - {1} из {2}',
    bbarEmptyMsg: 'Нет записей',
    bbarUsersText: 'Записей на странице: ',
    gridStore: 'corporate.Permissions',
    gridStateId: 'statePermissionsList',
    printUrl: Cement.Config.url.corporate.bank_accounts.printGrid,
    helpUrl: Cement.Config.url.corporate.bank_accounts.help,
    deleteUrl: Cement.Config.url.corporate.bank_accounts.deleteUrl,
    printItemUrl: Cement.Config.url.corporate.bank_accounts.printItem,
    overrideColumnHeaderHeight: false,
    cls: 'simple-form',

    createBbar: function (combo) {
        return Ext.create('Ext.PagingToolbar', {
            store: this.gridStore,
            displayInfo: true,
            displayMsg: this.bbarText,
            emptyMsg: this.bbarEmptyMsg,
            beforePageText: "Страница",
            items: ['-',
                this.bbarUsersText,
                combo,
                { xtype: 'tbseparator' },
                {
                    xtype: 'container',
                    height: 16,
                    padding: "1 2 0 2",
                    html: "<div class='tb-icon permission-denied'></div> Ограничено"
                },
                {
                    xtype: 'container',
                    height: 16,
                    padding: "1 2 0 2",
                    html: "<div class='tb-icon permission-read'></div> Просмотр"
                },
                {
                    xtype: 'container',
                    height: 16,
                    padding: "1 2 0 2",
                    html: "<div class='tb-icon permission-readwrite'></div> Редактирование"
                }
            ]
        });
    },

    getFilterItems: function () {
        return [];
    },

    columnRenderer: function (value) {
        var result = "<div class='permission-icon";

        switch (value) {
            case Cement.Config.permission.readwrite:
                result += ' permission-readwrite';
                break;

            case Cement.Config.permission.read:
                result += ' permission-read';
                break;

            default:
                result += ' permission-denied';
        }

        result += "'></div>";
        return result;
    },

    getGridColumns: function () {
        var me = this;

        me.columnRenderer = function (value) {
            var result = "<div class='permission-icon";

            switch (value) {
                case Cement.Config.permission.readwrite:
                    result += ' permission-readwrite';
                    break;

                case Cement.Config.permission.read:
                    result += ' permission-read';
                    break;

                default:
                    result += ' permission-denied';
            }

            result += "'></div>";
            return result;
        };

        var result = [
            { text: 'Сотрудник (ФИО)', dataIndex: 'p_name', width: 200, locked: true },
            { text: 'Должность', dataIndex: 'p_position_name', width: 120, locked: true },
            { text: 'Отдел', dataIndex: 'p_department_name', width: 150, locked: true },
            {
                text: 'Персональные данные',
                columns: [
                    { text: 'В', dataIndex: 'Personal_Documents' },
                    { text: 'К', dataIndex: 'Personal_Corresspondence' },
                    { text: 'Кл', dataIndex: 'Personal_Calendar' },
                    { text: 'С', dataIndex: 'Personal_Messages' },
                    { text: 'О', dataIndex: 'Personal_Adverts' },
                ]
            },
            {
                text: 'Корпоративные данные',
                columns: [
                    { text: 'Рк', dataIndex: 'Corporate_Requisite' },
                    { text: 'Рс', dataIndex: 'Corporate_PaymentAccounts' },
                    { text: 'Н', dataIndex: 'Corporate_Nomenklature' },
                    { text: 'Ст', dataIndex: 'Corporate_Employee' },
                    { text: 'П', dataIndex: 'Corporate_Signature' },
                    { text: 'А', dataIndex: 'Corporate_Administration' },
                ]
            },
            {
                text: 'Транспорт',
                columns: [
                    { text: 'Ат', dataIndex: 'Transport_Auto' },
                    { text: 'У', dataIndex: 'Transport_AutoControl' },
                    { text: 'ТЕ', dataIndex: 'Transport_TransportUnit' },
                    { text: 'Ту', dataIndex: 'Transport_Timesheet' },
                ]
            },
            {
                text: 'Товары (услуги)',
                columns: [
                    { text: 'Т', dataIndex: 'Products_Product' },
                    { text: 'У', dataIndex: 'Products_Service' },
                    { text: 'Пр', dataIndex: 'Products_PriceList' },
                    { text: 'Ту', dataIndex: 'Products_PackType' },
                ]
            },
            {
                text: 'Клиенты',
                columns: [
                    { text: 'Кл', dataIndex: 'Clients_Clients' },
                    { text: 'У', dataIndex: 'Clients_Management' },
                    { text: 'Гр', dataIndex: 'Clients_Groups' },
                    { text: 'Ст', dataIndex: 'Clients_Statuses' },
                ]
            },
            {
                text: 'Договора',
                columns: [
                    { text: 'Д', dataIndex: 'Contracts_Contract' },
                    { text: 'П', dataIndex: 'Contracts_ContractAnnex' },
                ]
            },
            {
                text: 'Товарооборот',
                columns: [
                    { text: 'К', dataIndex: 'Goods_Catalogue' },
                    { text: 'Зт', dataIndex: 'Goods_ProductRequest' },
                    { text: 'Зу', dataIndex: 'Goods_Groups' },
                ]
            },
            {
                text: 'Логистика',
                columns: [
                    { text: 'ЛГ', dataIndex: 'CarGo_GoodsLot' },
                    { text: 'ЛТ', dataIndex: 'CarGo_TransportLot' },
                    { text: 'БГ', dataIndex: 'CarGo_GoodsExchange' },
                    { text: 'БТ', dataIndex: 'CarGo_TransportExchange' },
                    { text: 'У', dataIndex: 'CarGo_Participants' },
                ]
            },
            {
                text: 'Склад',
                columns: [
                    { text: 'С', dataIndex: 'Warehouse_Warehouse' },
                    { text: 'СВ', dataIndex: 'Warehouse_Internal' },
                    { text: 'СИ', dataIndex: 'Warehouse_Inventory' },
                    { text: 'СД', dataIndex: 'Warehouse_Movement' },
                    { text: 'СП', dataIndex: 'Warehouse_Receipt' },
                    { text: 'СР', dataIndex: 'Warehouse_Expense' },
                ]
            },
            {
                text: 'Бухгалтерия',
                columns: [
                    { text: 'Д', dataIndex: 'Accounting_Documents' },
                    { text: 'И', dataIndex: 'Accounting_Indicators' },
                    { text: 'ИН', dataIndex: 'Accounting_Other' },
                    { text: 'ПР', dataIndex: 'Accounting_Receiptexpense' },
                ]
            },
            {
                text: 'Делопроизводство',
                columns: [
                    { text: 'В', dataIndex: 'OfficeWork_Documents' },
                    { text: 'К', dataIndex: 'OfficeWork_Correspondence' },
                    { text: 'П', dataIndex: 'OfficeWork_Disposal' },
                ]
            }
        ];

        Ext.each(result, function (column) {
            column.height = 2 * me.fixedColumnHeaderHeight;
            if (column.columns) {
                Ext.each(column.columns, function (subcolumn) {
                    //subcolumn.height = me.fixedColumnHeaderHeight;
                    //subcolumn.width = 30;
                    subcolumn.flex = 1;
                    subcolumn.renderer = me.columnRenderer;
                });
            }
        });

        return result;
    }
});