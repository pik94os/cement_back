Ext.define('Cement.view.corporate.administration.Form', {
    extend: 'Cement.view.basic.Form',
    alias: 'widget.administrationform',
    fieldsDisabled: false,
    url: Cement.Config.url.corporate.administration.saveUrl,
    method: Cement.Config.url.corporate.administration.saveMethod,
    printUrl: Cement.Config.url.corporate.administration.printItem,

    api: {
        submit: function (form, onCompleteCallback, options) {
            var treepanel = options.form.owner.down('treepanel');
            var store = treepanel.getStore();
            
            store.sync({
                success: function () {
                    onCompleteCallback(true);
                    store.reload();
                },
                failure: function () {
                    onCompleteCallback(false);
                }
            });
        }
    },

    beforeSubmit: function (callback) {
        var me = this,
            treepanel = me.down('treepanel'),
            store = treepanel.getStore();

        Ext.each(store.getNewRecords(), function (rec) {
            rec.commit();
        });

        if (store.getModifiedRecords().length > 0) {
            callback.call();
        }
    },

    getGrid: function () {
        var me = this;

        var combo = Ext.create('Ext.form.ComboBox', {
            // name : 'perpage',
            width: 60,
            store: new Ext.data.ArrayStore({
                fields: ['id'],
                data: [
                    ['15'],
                    ['25'],
                    ['50'],
                    ['100']
                ]
            }),
            mode: 'local',
            value: '15',
            listWidth: 40,
            triggerAction: 'all',
            displayField: 'id',
            valueField: 'id',
            editable: false,
            forceSelection: true
        });

        var baselist = Ext.create('Cement.view.corporate.administration.List');

        var storeName = 'corporate.PermissionsInEdit';

        var bbar = Ext.create('Ext.PagingToolbar', {
            store: storeName,
            displayInfo: true,
            displayMsg: 'Показаны записи {0} - {1} из {2}',
            emptyMsg: 'Нет записей',
            border: false,
            beforePageText: "Страница",
            items: ['-',
                'Записей на странице: ',
                combo,
                { xtype: 'tbseparator' },
                {
                    xtype: 'container',
                    height: 16,
                    padding: "1 2 0 2",
                    html: "<div class='tb-icon permission-denied'></div> Ограничено"
                },
                {
                    xtype: 'container',
                    height: 16,
                    padding: "1 2 0 2",
                    html: "<div class='tb-icon permission-read'></div> Просмотр"
                },
                {
                    xtype: 'container',
                    height: 16,
                    padding: "1 2 0 2",
                    html: "<div class='tb-icon permission-readwrite'></div> Редактирование"
                }
            ]
        });

        combo.on('select', function (_, record) {
            var store = Ext.getStore(storeName);
            store.pageSize = parseInt(record[0].get('id'), 10);
            store.load();
        }, this);

        var grid = Ext.create('Ext.grid.Panel', {
            store: storeName,
            style: 'border: solid 1px #bcbcbc',
            layout: 'fit',
            region: 'north',
            columns: baselist.getGridColumnsWithHeight(),
            height: 420,
            split: true,
            bbar: bbar
        });

        grid.on('selectionchange', function (self, selected) {
            var treepanel = me.down('treepanel');
            var store = treepanel.getStore();

            if (selected == null || selected.length == 0) {
                store.getRootNode().removeAll();
            } else {
                var item = selected[0];

                store.load({
                    params: {
                        id: item.get('id')
                    }
                });
            }
        });

        return grid;
    },
    
    getTree: function () {

        var proxy = Cement.Config.url.corporate.administration.permissionTree;

        proxy.actionMethods.read = "GET";
        proxy.writer = {
            type: 'json',
            allowSingle: false
        };
        
        var store = Ext.create('Ext.data.TreeStore', {
            fields: ['id', 'text', 'leaf', 'permission', 'description', 'user_id'],
            root: {
                id: '',
                expanded: false
            },
            autoLoad: false,
            proxy: proxy
        });

        var treepanel = Ext.create('Ext.tree.Panel', {
            border: true,
            region: 'center',
            itemId: 'permissionsTreePanel',
            xtype: 'treepanel',
            style: 'border: solid 1px #bcbcbc',
            autoScroll: true,
            store: store,
            rootVisible: false,
            plugins: [
                Ext.create('Ext.grid.plugin.CellEditing', {
                    clicksToEdit: 1,
                    listeners: {
                        'beforeedit': function(e, data) {
                            return data.record.get('leaf');
                        }
                    }
                })
            ],
            columns: [
                {
                    xtype: 'treecolumn', //this is so we know which column will show the tree
                    text: 'Форма',
                    flex: 2,
                    sortable: false,
                    dataIndex: 'text',
                    height: 20
                }, {
                    text: 'Режим',
                    height: 20,
                    flex: 1,
                    dataIndex: 'permission',
                    sortable: false,
                    renderer: function(value) {
                        var result = "<div class='permission-icon";
                        var name = '';
                        switch (value) {
                            case Cement.Config.permission.readwrite:
                                result += ' permission-readwrite';
                                name = 'Редактирование';
                                break;

                            case Cement.Config.permission.read:
                                result += ' permission-read';
                                name = 'Просмотр';
                                break;

                            case Cement.Config.permission.denied:
                                result += ' permission-denied';
                                name = 'Ограничено';
                                break;
                            default:
                                return null;
                        }

                        result += "'></div> " + name;
                        return result;
                    },
                    editor: {
                        xtype: 'combobox',
                        anchor: '100%',
                        listConfig: {
                            getInnerTpl: function(displayField) {
                                return '<div class="permission-icon-combo permission-{flag}"></div> {' + displayField + '}';
                            }
                        },
                        //listeners: {
                        //    render: function (ct, position) {
                        //        //this.callParent(arguments);
                        //        // add div for icon
                        //        //this.icon = Ext.DomHelper.append(this.el.up('div.x-form-field-wrap'), {
                        //        //    tag: 'div', style: 'position:absolute'
                        //        //});
                        //    },

                        //    change: function() {

                        //    }

                        //}

                        triggerAction: 'all',
                        editable: false,
                        valueField: 'id',
                        displayField: 'text',
                        store: new Ext.data.SimpleStore({
                            fields: ['id', 'text', 'flag'],
                            data: [
                                [Cement.Config.permission.readwrite, 'Редактирование', 'readwrite'],
                                [Cement.Config.permission.read, 'Просмотр', 'read'],
                                [Cement.Config.permission.denied, 'Ограничено', 'denied']
                            ]
                        })
                    }
                    //editor: Ext.create('Ext.ux.IconCombo', {
                    //    triggerAction: 'all',
                    //    anchor: '100%',
                    //    editable: false,
                    //    iconClsField: 'flag',
                    //    valueField: 'text',
                    //    displayField: 'name',

                    //    store: new Ext.data.SimpleStore({
                    //        fields: ['text', 'name', 'flag'],
                    //        data: [['Shade', 'Shade', 'ux-flag-permission-denied'],
                    //        ['Mostly Shady', 'Mostly Shady', 'ux-flag-permission-read'],
                    //        ['Sun or Shade', 'Sun or Shade', 'ux-flag-permission-readwrite']]
                    //    })
                    //})
                }, {
                    text: 'Описание',
                    height: 20,
                    flex: 2,
                    dataIndex: 'description',
                    editor: { xtype: 'textfield' },
                    sortable: false
                }
            ],
            cellEditing: new Ext.grid.plugin.CellEditing({
                clicksToEdit: 1
            })
        });

        treepanel.headerCt.setBorder(false);

        store.on('load', function () {
            store.getRootNode().expand();
            treepanel.unmask();
        }, this);

        store.on('beforeload', function () {
            treepanel.getEl().mask("Загрузка...", 'x-mask-loading');
        }, this);

        return treepanel;
    },

    getItems: function () {
        return [
        {
            xtype: 'panel',
            height: 796,
            padding: 5,
            border:0,
            layout: 'border',
            items: [
                this.getGrid(),
                this.getTree()
            ]
        }];
    },

    getNavPanel: function () {
        return null;
    },

    onCreate: function() {
        this.down('grid').getStore().load();
    }
});