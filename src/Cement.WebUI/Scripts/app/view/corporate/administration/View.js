Ext.define('Cement.view.corporate.administration.View', {
    extend: 'Cement.view.corporate.employee.View',
    alias: 'widget.administrationview',
    loadItemProperty: 'p_employee_id',
    loadItemModel: 'Cement.model.Employee'
});