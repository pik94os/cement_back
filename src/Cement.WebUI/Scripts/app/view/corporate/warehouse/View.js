Ext.define('Cement.view.corporate.warehouse.View', {
    extend: 'Cement.view.corporate.warehouse.Form',
    alias: 'widget.warehouseview',
    fieldsDisabled: true,
    buttons: null,
    border: 0,
    tbar: Cement.Config.modelViewButtons,
    layout: 'autocontainer',
    bbar: null,
    noUserFieldsetsControl: false,
    loadItemUrl: Cement.Config.url.corporate.warehouses.loadItem,

    afterRecordLoad: function (rec) {
        if (rec.get('p_kind') == 2) { //rail
            if (this.down('fieldset[role=railwayowner]')) {
                this.down('fieldset[role=railwayowner]').show();
            }
            if (this.down('fieldset[role=routes]')) {
                this.down('fieldset[role=routes]').show();
            }
        }
        else {
            if (this.down('fieldset[role=railwayowner]')) {
                this.down('fieldset[role=railwayowner]').hide();
            }
            if (this.down('fieldset[role=routes]')) {
                this.down('fieldset[role=routes]').hide();
            }
        }
    },

    initComponent: function () {
        Ext.apply(this, {
            items: [
                {
                    xtype: 'panel',
                    border: 0,
                    layout: 'anchor',
                    defaultType: 'textfield',
                    anchor: '100%',
                    bodyPadding: '10 10 10 5',
                    autoScroll: true,
                    defaults: {
                        anchor: '100%',
                        labelWidth: 110
                    },
                    items: this.getItems()
                }
            ]
        });
        this.callParent(arguments);
    }
});