Ext.define('Cement.view.corporate.warehouse.FormAuto', {
    extend: 'Cement.view.basic.DoubleGridForm',
    alias: 'widget.warehouseformauto',
    fieldsDisabled: false,
    url: Cement.Config.url.corporate.warehouses.saveUrl,
    method: Cement.Config.url.corporate.warehouses.saveMethod,
    printUrl: Cement.Config.url.corporate.warehouses.printItem,
    singleSelection: false,
    loadItemUrl: Cement.Config.url.corporate.warehouses.loadItem,
    //loadItemProperty: 'p_base_doc',

    formHeight: 711,

    storeFields: ['Id', 'Name', 'PositionName', 'DepartmentName'],
    bottomGridStoreProxy: Cement.Config.url.corporate.warehouses.employeeProxy,
    hasBottomPanel: false,

    getTopPanel: function () {
        return {
            xtype: 'panel',
            margin: '0 0 5 0',
            border: false,
            items: this.getTopItems()
        };
    },

    getTopItems: function () {
        return [
            {
                xtype: 'hiddenfield',
                value: Cement.Config.warehouse.warehouse_kinds.auto,
                name: 'p_kind'
            },
            {
                xtype: 'hiddenfield',
                name: 'p_employes'
            },
            {
                xtype: 'panel',
                bodyCls: 'custom-background',
                margin: '0 0 5 0',
                layout: 'anchor',
                items: [{
                    xtype: 'textfield',
                    fieldLabel: 'Наименование',
                    margin: '2 0 2 5',
                    name: 'p_name',
                    labelAlign: 'right',
                    anchor: '50%',
                    allowBlank: false
                }]
            },
            {
                xtype: 'panel',
                bodyCls: 'custom-background',
                items: [this.getAddressFieldSet()]
            }
        ];
    },

    getIndexField: function () {
        if (this.fieldsDisabled) {
            return {
                xtype: 'textfield',
                name: 'p_index',
                fieldLabel: 'Индекс',
                disabled: true
            };
        }
        else {
            var store = Ext.create('Ext.data.Store', {
                pageSize: 10,
                fields: [
                    'id', 'p_index', 'p_country', 'p_region',
                    'p_district', 'p_city', 'p_street', 'p_houses'
                ],
                proxy: getProxy(Cement.Config.url.index_autocomplete, 'places')
            });
            return {
                allowBlank: false,
                xtype: 'combo',
                store: store,
                minChars: 4,
                displayField: 'p_index',
                name: 'p_index',
                fieldLabel: 'Индекс',
                typeAhead: false,
                hideTrigger: true,
                anchor: '100%',
                listConfig: {
                    loadingText: 'Поиск...',
                    emptyText: 'Адресов не найдено',
                    getInnerTpl: function () {
                        return '<b>{p_index}</b> - {p_country}, {p_district}, {p_city}, {p_street}';
                    }
                },
                pageSize: 10,
                listeners: {
                    select: function (combo, selection) {
                        var post = selection[0],
                            form = this;

                        form.down('hiddenfield[name=p_country]').setValue(post.get('p_country'));
                        form.down('textfield[name=p_country_display]').setValue(post.get('p_country'));
                        form.down('hiddenfield[name=p_region]').setValue(post.get('p_region'));
                        form.down('textfield[name=p_region_display]').setValue(post.get('p_region'));
                        form.down('hiddenfield[name=p_district]').setValue(post.get('p_district'));
                        form.down('textfield[name=p_district_display]').setValue(post.get('p_district'));
                        form.down('hiddenfield[name=p_city]').setValue(post.get('p_city'));
                        form.down('textfield[name=p_city_display]').setValue(post.get('p_city'));
                        form.down('hiddenfield[name=p_street]').setValue(post.get('p_street'));
                        form.down('textfield[name=p_street_display]').setValue(post.get('p_street'));
                    },
                    scope: this
                }
            };
        }
    },

    getFieldSuffix: function () { return '_display'; },

    getHiddenFieldSuffix: function () { return ''; },

    getAddressFieldSet: function () {
        return {
            xtype: 'fieldset',
            layout: 'anchor',
            margin: 0,
            padding: '2 5 0 5',
            border: false,
            items: [
                {
                    xtype: 'fieldset',
                    layout:
                    {
                        type: 'column',
                        align: 'stretch'
                    },
                    padding: 0,
                    margin: 0,
                    border: false,
                    items: [
                    {
                        xtype: 'container',
                        layout: 'anchor',
                        columnWidth: 0.5,
                        border: 0,
                        anchor: '100%',
                        defaults: {
                            labelAlign: 'right',
                            xtype: 'textfield',
                            anchor: '100%'
                        },
                        items: [
                            this.getIndexField(),
                            {
                                name: 'p_country' + this.getFieldSuffix(),
                                fieldLabel: 'Страна',
                                disabled: true
                            },
                            {
                                name: 'p_region' + this.getHiddenFieldSuffix(),
                                xtype: 'hiddenfield'
                            },
                            {
                                name: 'p_region' + this.getFieldSuffix(),
                                fieldLabel: 'Регион',
                                disabled: true
                            },
                            {
                                name: 'p_district' + this.getHiddenFieldSuffix(),
                                xtype: 'hiddenfield'
                            },
                            {
                                name: 'p_district' + this.getFieldSuffix(),
                                fieldLabel: 'Район',
                                disabled: true
                            },
                            {
                                name: 'p_city' + this.getFieldSuffix(),
                                fieldLabel: 'Населенный пункт',
                                disabled: true
                            },
                            {
                                name: 'p_street' + this.getFieldSuffix(),
                                fieldLabel: 'Улица',
                                disabled: true
                            }
                        ]
                    },
                    {
                        xtype: 'container',
                        layout: 'anchor',
                        columnWidth: 0.5,
                        border: 0,
                        anchor: '100%',
                        defaults: {
                            labelAlign: 'right',
                            xtype: 'textfield',
                            anchor: '100%'
                        },
                        items: [
                            {
                                name: 'p_street' + this.getHiddenFieldSuffix(),
                                xtype: 'hiddenfield'
                            },
                            {
                                name: 'p_country' + this.getHiddenFieldSuffix(),
                                xtype: 'hiddenfield'
                            },
                            {
                                name: 'p_city' + this.getHiddenFieldSuffix(),
                                xtype: 'hiddenfield'
                            },
                            {
                                name: 'p_house',
                                fieldLabel: 'Дом',
                                allowBlank: this.allowBlankFields,
                                disabled: this.fieldsDisabled
                            },
                            {
                                name: 'p_housepart',
                                fieldLabel: 'Корпус',
                                allowBlank: true,
                                disabled: this.fieldsDisabled
                            },
                            {
                                name: 'p_building',
                                fieldLabel: 'Строение',
                                allowBlank: true,
                                disabled: this.fieldsDisabled
                            },
                            {
                                name: 'p_office',
                                fieldLabel: 'Офис',
                                disabled: this.fieldsDisabled,
                                vtype: 'warehouse_office_number'
                            },
                            {
                                name: 'p_longitude',
                                fieldLabel: 'Широта',
                                disabled: this.fieldLabel,
                                xtype: 'numberfield',
                                hideTrigger: true
                            },
                            {
                                name: 'p_latitude',
                                fieldLabel: 'Долгота',
                                disabled: this.fieldLabel,
                                xtype: 'numberfield',
                                hideTrigger: true
                            }]
                        }
                    ]
                },
                {
                    xtype: 'textarea',
                    name: 'p_address_comment',
                    margin: 0,
                    fieldLabel: 'Комментарий к адресу',
                    disabled: this.fieldsDisabled,
                    labelAlign: 'right',
                    anchor: '100%'
                }
            ]
        };
    },
    
    getBottomGridStore: function () {
        return Ext.create('Ext.data.Store', {
            fields: this.storeFields,
            autoLoad: false,
            proxy: this.bottomGridStoreProxy
        });
    },

    getCommonColumns: function (isSrc) {
        return [
            { text: 'Отдел', dataIndex: 'DepartmentName', width: 200 },
            { text: 'Должность', dataIndex: 'PositionName', width: 200 },
            { text: 'ФИО', dataIndex: 'Name', flex: 1 }
        ];
    },

    beforeSubmit: function (callback) {
        var me = this;
        var employes = [];
        me.dstStore.each(function (item) {
            employes.push(item.get('Id'));
        });

        me.down('hiddenfield[name=p_employes]').setValue(Ext.encode(employes));

        callback.call();
    },

    afterRecordLoad: function (rec) {
        this.dstStore.removeAll();
        this.dstStore.add(rec.get('p_warehouse_employee'));
        this.srcStore.load({
            params: {
                warehouseId: rec.get('id')
            }
        });
    },

    onCreate: function () {
        this.srcStore.load();
    }
});