Ext.define('Cement.view.corporate.warehouse.List', {
    extend: 'Cement.view.basic.Grid',
    alias: 'widget.warehouselist',
    bbarText: 'Показаны склады {0} - {1} из {2}',
    bbarEmptyMsg: 'Нет складов',
    bbarUsersText: 'Складов на странице: ',
    gridStore: 'corporate.Warehouses',
    gridStateId: 'stateWarehouseList',
    printUrl: Cement.Config.url.corporate.warehouses.printGrid,
    helpUrl: Cement.Config.url.corporate.warehouses.help,
    deleteUrl: Cement.Config.url.corporate.warehouses.deleteUrl,
    printItemUrl: Cement.Config.url.corporate.warehouses.printItem,
    filterControl: 'Cement.view.warehouse.Filter',
    formControl: 'Cement.view.warehouse.Form',
    cls: 'simple-form',

    creatorTree: Ext.clone(Cement.Creators.warehouse.children[Cement.Creators.warehouse.warehouseIndex]),
    createWindowTitle: 'Создать',

    getFilterItems: function () {
        return [
            {
                text: 'Наименование',
                kind: 'selector',
                field_name: 'p_name',
                checked: true
            },
            {
                text: 'Адрес',
                kind: 'selector',
                field_name: 'p_address',
                checked: true
            },
            {
                text: 'ИНН',
                kind: 'selector',
                field_name: 'p_railway_owner_inn',
                checked: true
            },
            {
                text: 'КПП',
                kind: 'selector',
                field_name: 'p_railway_owner_kpp',
                checked: true
            },
            {
                text: 'ОКПО',
                kind: 'selector',
                field_name: 'p_railway_owner_okpo',
                checked: true
            },
            {
                text: 'Код предприятия',
                kind: 'selector',
                field_name: 'p_firm_code',
                checked: true
            },
            {
                text: 'Станция',
                kind: 'selector',
                field_name: 'p_station',
                checked: true
            },
            {
                text: 'Код станции',
                kind: 'selector',
                field_name: 'p_station_code',
                checked: true
            },
            {
                text: 'Дорога',
                kind: 'selector',
                field_name: 'p_road',
                checked: true
            }
        ];
    },

    getGridColumns: function () {
        var result = [
            { text: 'Наименование', dataIndex: 'p_name', width: 250, locked: true },
            { text: 'Тип', dataIndex: 'p_kind_display', width: 150, locked: true },
            { text: 'Адрес', dataIndex: 'p_address', width: 150 },
            { text: 'ИНН', dataIndex: 'p_railway_owner_inn', width: 150 },
            { text: 'КПП', dataIndex: 'p_railway_owner_kpp', width: 150 },
            { text: 'ОКПО', dataIndex: 'p_railway_owner_okpo', width: 150 },
            { text: 'Код предприятия', dataIndex: 'p_firm_code', width: 150 },
            { text: 'Станция', dataIndex: 'p_station_display', width: 150 },
            { text: 'Код станции', dataIndex: 'p_station_code_display', width: 150 },
            { text: 'Дорога', dataIndex: 'p_road_display', width: 150 }
        ];
        return this.mergeActions(result);
    }
});