Ext.define('Cement.view.corporate.warehouse.Form', {
    extend: 'Cement.view.basic.Form',
    alias: 'widget.warehouseform',
    fieldsDisabled: false,
    prevNextHidden: true,
    url: Cement.Config.url.corporate.warehouses.saveUrl,
    method: Cement.Config.url.corporate.warehouses.saveMethod,
    printUrl: Cement.Config.url.corporate.warehouses.printItem,
    activeItem: 0,
    streetsStoreId: 'corporate.auxiliary.Streets',

    getBasicFieldSet: function () {
        return {
            xtype: 'fieldset',
            border: 0,
            margin: 0,
            padding: 0,
            defaults: {
                xtype: 'textfield',
                anchor: '100%'
            },
            items: [
                {
                    xtype: 'hiddenfield',
                    name: 'id'
                },
                {
                    xtype: 'textfield',
                    fieldLabel: 'Наименование',
                    anchor: '100%',
                    allowBlank: this.allowBlankFields,
                    disabled: this.fieldsDisabled,
                    name: 'p_name'
                }, {
                    xtype: 'fieldcontainer',
                    fieldLabel: 'Тип склада',
                    defaultType: 'radiofield',
                    defaults: {
                        flex: 1
                    },
                    layout: 'hbox',
                    items: [{
                        disabled: this.fieldsDisabled,
                        checked: true,
                        boxLabel: 'Авто',
                        name: 'p_kind',
                        value: 1,
                        inputValue: 1
                    }, {
                        disabled: this.fieldsDisabled,
                        boxLabel: 'Ж/Д',
                        name: 'p_kind',
                        value: 2,
                        inputValue: 2
                    }]
                }
            ]
        };
    },

    fieldsSuffix: function () {
        result = '_display';
        if (this.fieldsDisabled) {
            result = '';
        }
        return result;
    },

    getOwnerFieldSet: function () {
        var f = this.getSelectorField('Наименование', 'p_railway_owner', 'select_owner', 100, 'left');
        if (this.fieldsDisabled) {
            f = {
                disabled: true,
                fieldLabel: 'Наименование',
                name: 'p_railway_owner_display'
            };
        }
        return [{
            xtype: 'checkbox',
            name: 'p_railway_is_own',
            inputValue: 'true',
            uncheckedValue: 'false',
            boxLabel: 'Собственные подъездные пути',
            disabled: this.fieldsDisabled,
            padding: '5 110 5 0'
        }, f, {
            disabled: true,
            name: 'p_railway_owner_inn',
            fieldLabel: 'ИНН'
        }, {
            disabled: true,
            name: 'p_railway_owner_kpp',
            fieldLabel: 'КПП'
        }, {
            disabled: true,
            name: 'p_railway_owner_okpo',
            fieldLabel: 'ОКПО'
        }];
    },

    getStationField: function () {
        if (this.fieldsDisabled) {
            return {
                xtype: 'textfield',
                name: 'p_station',
                fieldLabel: 'Индекс',
                disabled: true
            };
        }
        else {
            var store = Ext.create('Ext.data.Store', {
                pageSize: 10,
                fields: [
                    'id', 'p_station', 'p_station_code', 'p_road'
                ],
                proxy: getProxy(Cement.Config.url.corporate.warehouses.stations, '')
            });
            return {
                xtype: 'combo',
                store: store,
                displayField: 'p_station',
                name: 'p_station_display',
                minChars: 2,
                fieldLabel: 'Станция',
                typeAhead: false,
                hideTrigger: true,
                anchor: '100%',
                listConfig: {
                    loadingText: 'Поиск...',
                    emptyText: 'Станций не найдено',
                    getInnerTpl: function () {
                        return '<b>{p_station}</b> - {p_road}';
                    }
                },
                pageSize: 10,
                listeners: {
                    select: function (combo, selection) {
                        var post = selection[0],
                            form = combo.up('warehouseform');
                        if (form) {
                            form.down('textfield[name=p_station_code_display]').setValue(post.get('p_station_code'));
                            form.down('hiddenfield[name=p_station]').setValue(post.get('id'));
                            form.down('textfield[name=p_road_display]').setValue(post.get('p_road'));
                        }
                    }
                }
            };
        }
    },

    getIndexField: function () {
        if (this.fieldsDisabled) {
            return {
                xtype: 'textfield',
                name: 'p_index',
                fieldLabel: 'Индекс',
                disabled: true
            };
        }
        else {
            var store = Ext.create('Ext.data.Store', {
                pageSize: 10,
                fields: [
                    'id', 'p_index', 'p_country', 'p_region',
                    'p_district', 'p_city', 'p_street', 'p_houses'
                ],
                proxy: getProxy(Cement.Config.url.index_autocomplete, 'places')
            });
            return {
                allowBlank: false,
                xtype: 'combo',
                store: store,
                minChars: 2,
                displayField: 'p_index',
                name: 'p_index',
                fieldLabel: 'Индекс',
                typeAhead: false,
                hideTrigger: true,
                anchor: '100%',
                listConfig: {
                    loadingText: 'Поиск...',
                    emptyText: 'Адресов не найдено',
                    getInnerTpl: function () {
                        return '<b>{p_index}</b> - {p_country}, {p_district}, {p_city}, {p_street}';
                    }
                },
                pageSize: 10,
                listeners: {
                    select: function (combo, selection) {
                        var post = selection[0],
                            form = combo.up('warehouseform'),
                            streetStore = Ext.getStore(form.streetsStoreId);
                        if (form) {
                            form.down('hiddenfield[name=p_country]').setValue(post.get('p_country'));
                            form.down('textfield[name=p_country_display]').setValue(post.get('p_country'));
                            form.down('hiddenfield[name=p_region]').setValue(post.get('p_region'));
                            form.down('textfield[name=p_region_display]').setValue(post.get('p_region'));
                            form.down('hiddenfield[name=p_district]').setValue(post.get('p_district'));
                            form.down('textfield[name=p_district_display]').setValue(post.get('p_district'));
                            form.down('hiddenfield[name=p_city]').setValue(post.get('p_city'));
                            form.down('textfield[name=p_city_display]').setValue(post.get('p_city'));
                            form.down('hiddenfield[name=p_street]').setValue(post.get('p_street'));
                            form.down('textfield[name=p_street_display]').setValue(post.get('p_street'));
                            if (form.housesStore) {
                                form.housesStore.loadData(post.get('p_houses'));
                                form.down('combo[name=p_house]').enable();
                            }
                        }
                    }
                }
            };
        }
    },

    houseChanged: function (combo, newValue, oldValue) {
        var form = combo.up('warehouseform');
        if (form) {
            form.housePartsStore.filter({
                "property": "p_house",
                "value": newValue
            });
            form.down('combo[name=p_housepart]').enable();
        }
    },

    housePartChanged: function (combo, newValue, oldValue) {
        var form = combo.up('warehouseform');
        if (form) {
            form.houseBuildingStore.filter({
                "property": "p_housepart",
                "value": newValue
            });
            form.down('combo[name=p_building]').enable();
        }
    },

    getFieldSuffix: function () {
        return '_display';
        if (this.fieldsDisabled) {
            return '_display';
        }
        else {
            return '_display';
        }
    },

    getHiddenFieldSuffix: function () {
        return '';
        if (this.fieldsDisabled) {
            return '';
        }
        else {
            return '';
        }
    },

    getAddressFieldSet: function () {
        return [this.getIndexField(), {
            name: 'p_country' + this.getFieldSuffix(),
            fieldLabel: 'Страна',
            disabled: true
        }, {
            name: 'p_region' + this.getHiddenFieldSuffix(),
            xtype: 'hiddenfield'
        }, {
            name: 'p_region' + this.getFieldSuffix(),
            fieldLabel: 'Регион',
            disabled: true
        }, {
            name: 'p_district' + this.getHiddenFieldSuffix(),
            xtype: 'hiddenfield'
        }, {
            name: 'p_district' + this.getFieldSuffix(),
            fieldLabel: 'Район',
            disabled: true
        }, {
            name: 'p_city' + this.getFieldSuffix(),
            fieldLabel: 'Населенный пункт',
            disabled: true
        }, {
            name: 'p_street' + this.getFieldSuffix(),
            fieldLabel: 'Улица',
            disabled: true
        }, {
            name: 'p_street' + this.getHiddenFieldSuffix(),
            xtype: 'hiddenfield'
        }, {
            name: 'p_country' + this.getHiddenFieldSuffix(),
            xtype: 'hiddenfield'
        }, {
            name: 'p_city' + this.getHiddenFieldSuffix(),
            xtype: 'hiddenfield'
        },
        {
        	name: 'p_house',
        	fieldLabel: 'Дом',
        	allowBlank: this.allowBlankFields,
        	disabled: this.fieldsDisabled
        },
        {
            name: 'p_housepart',
            fieldLabel: 'Корпус',
            allowBlank: true,
            disabled: this.fieldsDisabled
        },
        {
            name: 'p_building',
            fieldLabel: 'Строение',
            allowBlank: true,
            disabled: this.fieldsDisabled
        },
        {
            name: 'p_office',
            fieldLabel: 'Офис',
            disabled: this.fieldsDisabled,
            vtype: 'warehouse_office_number'
        },
        {
            name: 'p_longitude',
            fieldLabel: 'Широта',
            disabled: this.fieldLabel,
            vtype: 'numeric_float'
        },
        {
            name: 'p_latitude',
            fieldLabel: 'Долгота',
            disabled: this.fieldLabel,
            vtype: 'numeric_float'
        },
        {
            xtype: 'textarea',
            name: 'p_address_comment',
            fieldLabel: 'Комментарий к адресу',
            disabled: this.fieldsDisabled
        }];
    },

    getLoadFieldSet: function () {
        return [
            {
                name: 'p_firm_code',
                fieldLabel: 'Код предприятия',
                disabled: this.fieldsDisabled,
                vtype: 'warehouse_firm_code'
            },
            this.getStationField(),
            {
                name: 'p_station',
                xtype: 'hiddenfield'
            },
            {
                name: 'p_station_code' + this.getFieldSuffix(),
                fieldLabel: 'Код станции',
                disabled: true
            },
            {
                name: 'p_road' + this.getFieldSuffix(),
                fieldLabel: 'Дорога',
                disabled: true
            }
        ];
    },

    getItems: function () {
        return [
            {
                xtype: 'panel',
                layout: 'anchor',
                border: 0,
                bodyPadding: 0,
                padding: 0,
                margin: 0,
                defaults: {
                    anchor: '100%',
                    labelWidth: 110
                },
                items: [
                    this.getBasicFieldSet()
                ]
            },
            {
                layout: 'anchor',
                xtype: 'fieldset',
                collapsed: false,
                collapsible: true,
                title: 'Владелец подъездных путей',
                defaults: {
                    xtype: 'textfield',
                    anchor: '100%'
                },
                role: 'railwayowner',
                anchor: '100%',
                items: this.getOwnerFieldSet()
            },
            {
                layout: 'anchor',
                xtype: 'fieldset',
                collapsed: false,
                collapsible: true,
                title: 'Адрес',
                defaults: {
                    xtype: 'textfield',
                    anchor: '100%'
                },
                anchor: '100%',
                items: this.getAddressFieldSet()
            },
            {
                xtype: 'fieldset',
                collapsed: false,
                collapsible: true,
                title: 'Отгрузочные реквизиты',
                defaults: {
                    xtype: 'textfield',
                    anchor: '100%'
                },
                role: 'routes',
                items: this.getLoadFieldSet()
            }
        ];
    },

    initComponent: function () {
        this.callParent(arguments);
        if (this.down('button[action=select_owner]')) {
            this.down('button[action=select_owner]').on('click', function () {
                this.createFirmWindow();
            }, this);
        }
        this.down('checkbox[name=p_railway_is_own]').on('change', function () {
            if (this.down('fieldcontainer[role=container_p_railway_owner]')) {
                if (this.down('checkbox[name=p_railway_is_own]').getValue()) {
                    this.down('fieldcontainer[role=container_p_railway_owner]').disable();
                }
                else {
                    this.down('fieldcontainer[role=container_p_railway_owner]').enable();
                }
            }
        }, this);

        this.down('radiofield[name=p_kind]').on('change', function () {
            if (this.down('radiofield[name=p_kind]').getValue()) {
                this.down('fieldset[role=railwayowner]').hide();
                this.down('fieldset[role=routes]').hide();
            }
            else {
                this.down('fieldset[role=railwayowner]').show();
                this.down('fieldset[role=routes]').show();
            }
        }, this);
    },

    createFirmWindow: function () {
        if (!this.firmWindow) {
            this.firmWindow = Ext.create('Cement.view.corporate.warehouse.FirmWindow');
            this.firmWindow.on('selected', function (rec) {
                this.down('textfield[name=p_railway_owner_inn_display]').setValue(rec.get('p_inn'));
                // this.down('hiddenfield[name=p_railway_owner_inn]').setValue(rec.get('p_inn'));
                this.down('textfield[name=p_railway_owner_okpo_display]').setValue(rec.get('p_okpo'));
                // this.down('hiddenfield[name=p_railway_owner_okpo]').setValue(rec.get('p_okpo'));
                this.down('textfield[name=p_railway_owner_kpp_display]').setValue(rec.get('p_kpp'));
                // this.down('hiddenfield[name=p_railway_owner_kpp]').setValue(rec.get('p_kpp'));
                this.down('hiddenfield[name=p_railway_owner]').setValue(rec.get('id'));
                this.down('textfield[name=p_railway_owner_display]').setValue(rec.get('p_name'));
                if (this.down('checkbox[name=p_railway_is_own]')) {
                    this.down('checkbox[name=p_railway_is_own]').disable();
                }
                this.firmWindow.hide();
            }, this);
        }
        this.firmWindow.show();
    },

    afterRecordLoad: function (rec) {
        if (this.down('combo[name=p_index]')) {
            this.down('combo[name=p_index]').setRawValue(rec.get('p_index'));
        }
        if (this.down('combo[name=p_station]')) {
            this.down('combo[name=p_station]').setRawValue(rec.get('p_station'));
        }
    }
});