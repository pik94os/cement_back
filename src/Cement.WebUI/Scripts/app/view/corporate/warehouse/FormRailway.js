Ext.define('Cement.view.corporate.warehouse.FormRailway', {
    extend: 'Cement.view.corporate.warehouse.FormAuto',
    alias: 'widget.warehouseformrailway',
    formHeight: 918,
    hasBottomPanel: false,

    getTopItems: function () {
        return [
            {
                xtype: 'hiddenfield',
                value: Cement.Config.warehouse.warehouse_kinds.railway,
                name: 'p_kind'
            },
            {
                xtype: 'hiddenfield',
                name: 'p_employes'
            },
            {
                xtype: 'panel',
                bodyCls: 'custom-background',
                margin: '0 0 5 0',
                layout: 'anchor',
                items: [{
                    xtype: 'textfield',
                    fieldLabel: 'Наименование',
                    margin: '2 0 2 5',
                    name: 'p_name',
                    labelAlign: 'right',
                    anchor: '50%',
                    allowBlank: false
                }]
            },
            {
                xtype: 'panel',
                margin: '0 0 5 0',
                bodyCls: 'custom-background',
                items: [this.getOwnerFieldSet()]
            },
            {
                xtype: 'panel',
                margin: '0 0 5 0',
                bodyCls: 'custom-background',
                items: [this.getAddressFieldSet()]
            },
            {
                xtype: 'panel',
                bodyCls: 'custom-background',
                items: [this.getLoadFieldSet()]
            }
        ];
    },

    getOwnerFieldSet: function () {
        return {
            xtype: 'fieldset',
            layout: 'anchor',
            margin: 0,
            padding: '2 5 0 5',
            border: false,
            defaults: {
                labelAlign: 'right',
                xtype: 'textfield',
                anchor: '100%'
            },
            items: [
                {
                    xtype: 'checkbox',
                    name: 'p_railway_is_own',
                    inputValue: 'true',
                    uncheckedValue: 'false',
                    boxLabel: 'Собственные подъездные пути',
                    disabled: this.fieldsDisabled,
                    padding: '0 0 0 105'
                },
                this.getSelectorField('Наименование', 'p_railway_owner', 'select_owner', 100, 'right'),
                {
                    disabled: true,
                    name: 'p_railway_owner_inn',
                    fieldLabel: 'ИНН'
                },
                {
                    disabled: true,
                    name: 'p_railway_owner_kpp',
                    fieldLabel: 'КПП'
                },
                {
                    disabled: true,
                    name: 'p_railway_owner_okpo',
                    fieldLabel: 'ОКПО'
                }
            ]
        };
    },

    getStationField: function () {
        var store = Ext.create('Ext.data.Store', {
            pageSize: 10,
            fields: [
                'id', 'p_station', 'p_station_code', 'p_road'
            ],
            proxy: getProxy(Cement.Config.url.corporate.warehouses.stations, '')
        });
        return {
            xtype: 'combo',
            store: store,
            displayField: 'p_station',
            name: 'p_station_display',
            minChars: 4,
            fieldLabel: 'Станция',
            typeAhead: false,
            hideTrigger: true,
            anchor: '100%',
            listConfig: {
                loadingText: 'Поиск...',
                emptyText: 'Станций не найдено',
                getInnerTpl: function () {
                    return '<b>{p_station}</b> - {p_road}';
                }
            },
            pageSize: 10,
            listeners: {
                select: function (combo, selection) {
                    var post = selection[0],
                        form = this;

                    form.down('textfield[name=p_station_code_display]').setValue(post.get('p_station_code'));
                    form.down('hiddenfield[name=p_station]').setValue(post.get('id'));
                    form.down('textfield[name=p_road_display]').setValue(post.get('p_road'));
                },
                scope: this
                
            }
        };
    },

    getLoadFieldSet: function () {
        return {
            xtype: 'fieldset',
            layout: {
                type: 'column',
                align: 'stretch'
            },
            margin: 0,
            padding: '2 5 0 5',
            border: false,
            items: [
                {
                    xtype: 'container',
                    layout: 'anchor',
                    columnWidth: 0.5,
                    border: 0,
                    anchor: '100%',
                    defaults: {
                        labelAlign: 'right',
                        xtype: 'textfield',
                        anchor: '100%'
                    },
                    items: [
                        {
                            name: 'p_firm_code',
                            fieldLabel: 'Код предприятия',
                            disabled: this.fieldsDisabled,
                            vtype: 'warehouse_firm_code'
                        },
                        this.getStationField(),
                        {
                            name: 'p_station',
                            xtype: 'hiddenfield'
                        }
                    ]
                },
                {
                    xtype: 'container',
                    layout: 'anchor',
                    columnWidth: 0.5,
                    border: 0,
                    anchor: '100%',
                    defaults: {
                        labelAlign: 'right',
                        xtype: 'textfield',
                        anchor: '100%'
                    },
                    items: [
                        {
                            name: 'p_station_code' + this.getFieldSuffix(),
                            fieldLabel: 'Код станции',
                            disabled: true
                        },
                        {
                            name: 'p_road' + this.getFieldSuffix(),
                            fieldLabel: 'Дорога',
                            disabled: true
                        }
                    ]
                }
            ]
        };
    },

    initComponent: function () {
        this.callParent(arguments);
        if (this.down('button[action=select_owner]')) {
            this.down('button[action=select_owner]').on('click', function () {
                this.createFirmWindow();
            }, this);
        }
        this.down('checkbox[name=p_railway_is_own]').on('change', function () {
            if (this.down('fieldcontainer[role=container_p_railway_owner]')) {
                if (this.down('checkbox[name=p_railway_is_own]').getValue()) {
                    this.down('fieldcontainer[role=container_p_railway_owner]').disable();
                }
                else {
                    this.down('fieldcontainer[role=container_p_railway_owner]').enable();
                }
            }
        }, this);
    },

    createFirmWindow: function () {
        if (!this.firmWindow) {
            this.firmWindow = Ext.create('Cement.view.corporate.warehouse.FirmWindow');
            this.firmWindow.on('selected', function (rec) {
                this.down('textfield[name=p_railway_owner_inn]').setValue(rec.get('p_inn'));
                this.down('textfield[name=p_railway_owner_okpo]').setValue(rec.get('p_okpo'));
                this.down('textfield[name=p_railway_owner_kpp]').setValue(rec.get('p_kpp'));
                this.down('hiddenfield[name=p_railway_owner]').setValue(rec.get('id'));
                this.down('textfield[name=p_railway_owner_display]').setValue(rec.get('p_name'));
                this.firmWindow.hide();
            }, this);
        }
        this.firmWindow.show();
    }
});