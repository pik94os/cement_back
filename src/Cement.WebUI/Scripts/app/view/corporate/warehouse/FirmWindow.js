Ext.define('Cement.view.corporate.warehouse.FirmWindow', {
	extend: 'Cement.view.goods.auto.form.SupplierWindow',
	xtype: 'corporate_warehouse_firm_window',
	title: 'Выбор собственника',
	leftFrameTitle: 'Собственник',
  	topGridTitle: 'Выбрать собственника',
  	topGridTitle: 'Выбранный собственник',
	structure_storeId: 'corporate.auxiliary.WarehouseOwners',
	storeFields: ['id', 'p_name', 'p_inn', 'p_kpp', 'p_okpo']
});