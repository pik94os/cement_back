Ext.define('Cement.view.corporate.warehouse.NewForm', {
    extend: 'Cement.view.corporate.warehouse.Form',
    alias: 'widget.new_warehouseform',

    getItems: function () {

        var addressFields = this.splitArray(this.getAddressFieldSet()),
        // regFields = this.splitArray(this.getRegFieldSet()),
            loadFields = this.splitArray(this.getLoadFieldSet()),
            routes = this.getTwoColumns('Отгрузочные реквизиты', loadFields[0], loadFields[1]);
        routes.role = 'routes';
        routes.hidden = true;
        return [
            {
                layout: 'anchor',
                xtype: 'container',
                border: 0,
                defaults: {
                    anchor: '100%',
                    labelWidth: 110
                },
                items: [
                    this.getBasicFieldSet()
                ]
            },
            {
                layout: 'anchor',
                xtype: 'fieldset',
                collapsed: false,
                collapsible: true,
                hidden: true,
                role: 'railwayowner',
                title: 'Владелец подъездных путей',
                defaults: {
                    xtype: 'textfield',
                    anchor: '100%'
                },
                anchor: '100%',
                items: this.getOwnerFieldSet()
            },
            this.getTwoColumns('Адрес', addressFields[0], addressFields[1]),
            // this.getTwoColumns('Регистрационные данные', regFields[0], regFields[1]),
            routes
        ];
    },

    reset: function () {
        this.callParent(arguments);

        // this.down('textfield[name=p_street]').disable();
        if (this.down('checkbox[name=p_railway_is_own]')) {
            this.down('checkbox[name=p_railway_is_own]').enable();
        }
        if (this.down('fieldcontainer[role=container_p_railway_owner]')) {
            this.down('fieldcontainer[role=container_p_railway_owner]').enable();
        }
    }
});