Ext.define('Cement.view.corporate.signature.ReasonsWindow', {
    extend: 'Cement.view.common.ConnectedDocumentsWindow',
    xtype: 'personal_signature_reasons_window',
    title: 'Выбор документа',
    hideCommentPanel: true,
    leftFrameTitle: 'Документы',
    topGridTitle: 'Документы',
    bottomGridTitle: 'Выбранные документы',
    structure_storeId: 'corporate.auxiliary.SignaturesReasonDocuments',
    storeFields: ['id', 'text' ],
    displayField: 'text',
    singleSelection: true,

    getBottomColumnsConfig: function () {
    return [{
      xtype: 'rowactions',
      hideable: false,
      resizeable: false,
      width: 18,
      actions: [{
                iconCls: 'icon-remove-document',
                qtip: 'Удалить',
                callback: this.removeEmployee
            }]
    },
    { text: 'Документ', dataIndex: 'text', flex: 1 }];
  },

  getTopColumnsConfig: function () {
    return [
      {
        xtype: 'rowactions',
        hideable: false,
        resizeable: false,
        width: 18,
        keepSelection: true,
        actions: [{
                  iconCls: 'icon-add-document',
                  qtip: 'Добавить',
                  callback: this.addEmployee
              }]
      },
      { text: 'Документ', dataIndex: 'text', flex: 1 }
    ];
  },

  sign: function (send) {
    var store = this.down('*[role=sign-dst-grid]').getStore();
    if (store.getCount() > 0) {
      this.fireEvent('documentsselected', store.getAt(0));
    }
  }
});