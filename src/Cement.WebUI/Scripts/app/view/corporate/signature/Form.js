Ext.define('Cement.view.corporate.signature.Form', {
    extend: 'Cement.view.basic.Form',
    alias: 'widget.signaturesform',
    fieldsDisabled: false,
    url: Cement.Config.url.corporate.signatures.saveUrl,
    method: Cement.Config.url.corporate.signatures.saveMethod,
    printUrl: Cement.Config.url.corporate.signatures.printItem,
    activeItem: 0,
    isFilter: false,
    notCollapseFieldsetIndex: 0,

    getEmployeeField: function () {
        if (this.fieldsDisabled) {
            return {
                xtype: 'textfield',
                name: 'p_employee_display',
                fieldLabel: 'Сотрудник'
            };
        }
        else {
            return {
                xtype: 'fieldcontainer',
                layout: 'hbox',
                margin: '0 0 5 -1',
                items: [
                    {
                        xtype: 'hidden',
                        name: 'p_employee'
                    },
                    {
                        xtype: 'textfield',
                        name: 'p_employee_display',
                        fieldLabel: 'Сотрудник',
                        flex: 1
                    },
                    {
                        xtype: 'button',
                        text: 'Выбрать',
                        margin: '0 0 0 4',
                        action: 'show_employee_window',
                        width: 60
                    }
                ]
            };
        }
    },

    getBasicFieldSet: function () {
        return [
            {
                xtype: 'fieldset',
                border: 0,
                margin: 0,
                padding: 0,
                defaults: {
                    xtype: 'textfield',
                    anchor: '100%'
                },
                items: [
                    {
                        xtype: 'hiddenfield',
                        name: 'id'
                    },
                    this.getEmployeeField(),
                    {
                        name: 'p_position',
                        disabled: true,
                        allowBlank: this.allowBlankFields,
                        fieldLabel: 'Должность'
                    },
                    {
                        name: 'p_department',
                        disabled: true,
                        allowBlank: this.allowBlankFields,
                        fieldLabel: 'Отдел'
                    },
                    {
                        name: 'p_inn',
                        disabled: true,
                        allowBlank: this.allowBlankFields,
                        fieldLabel: 'ИНН'
                    }
                ]
            }
        ];
    },

    getPassportFieldSet: function () {
        return [
            {
                name: 'p_passport_number',
                fieldLabel: 'Номер',
                allowBlank: this.allowBlankFields,
                disabled: true
            },
            {
                name: 'p_passport_serie',
                fieldLabel: 'Серия',
                allowBlank: this.allowBlankFields,
                disabled: true
            },
            {
                name: 'p_passport_get',
                fieldLabel: 'Выдан',
                allowBlank: this.allowBlankFields,
                disabled: true
            },
            {
                name: 'p_passport_date',
                fieldLabel: 'Дата',
                allowBlank: this.allowBlankFields,
                disabled: true
            },
            {
                name: 'p_passport_birth',
                fieldLabel: 'Дата рождения',
                allowBlank: this.allowBlankFields,
                disabled: true
            },
            {
                name: 'p_passport_place',
                fieldLabel: 'Место рождения',
                allowBlank: this.allowBlankFields,
                disabled: true
            }
        ];
    },

    getDriverFieldSet: function () {
        return [
            {
                name: 'p_drive_number',
                fieldLabel: 'Номер',
                disabled: true
            },
            {
                name: 'p_drive_serie',
                fieldLabel: 'Серия',
                disabled: true
            },
            {
                name: 'p_drive_get',
                fieldLabel: 'Выдано',
                disabled: true
            },
            {
                name: 'p_drive_category_display',
                fieldLabel: 'Категории',
                disabled: true
            },
            {
                name: 'p_drive_date',
                fieldLabel: 'Дата выдачи',
                disabled: true
            },
            {
                name: 'p_drive_date_end',
                fieldLabel: 'Действительно до',
                disabled: true
            },
            {
                name: 'p_drive_age_display',
                fieldLabel: 'Стаж',
                disabled: true
            }
        ];
    },

    getAddressFieldSet: function () {
        return [
            {
                name: 'p_address_index',
                fieldLabel: 'Индекс',
                disabled: true
            },
            {
                name: 'p_address_country',
                fieldLabel: 'Страна',
                disabled: true
            },
            {
                name: 'p_address_region',
                fieldLabel: 'Регион',
                disabled: true
            },
            {
                name: 'p_address_district',
                fieldLabel: 'Район',
                disabled: true
            },
            {
                name: 'p_address_city',
                fieldLabel: 'Населенный пункт',
                disabled: true
            },
            {
                name: 'p_address_street',
                fieldLabel: 'Улица',
                disabled: true
            },
            {
                name: 'p_address_house',
                fieldLabel: 'Дом',
                disabled: true
            },
            {
                name: 'p_address_housepart',
                fieldLabel: 'Корпус',
                disabled: true
            },
            {
                name: 'p_address_building',
                fieldLabel: 'Строение',
                disabled: true
            },
            {
                name: 'p_address_flat',
                fieldLabel: 'Квартира',
                disabled: this.fieldsDisabled
            }
        ];
    },

    getConnectionFieldSet: function () {
        return [
            {
                name: 'p_connect_phone1',
                fieldLabel: 'Телефон 1',
                disabled: true
            },
            {
                name: 'p_connect_phone2',
                fieldLabel: 'Телефон 2',
                disabled: true
            },
            {
                name: 'p_connect_phone3',
                fieldLabel: 'Телефон 3',
                disabled: true
            },
            {
                name: 'p_connect_fax',
                fieldLabel: 'Факс',
                disabled: true
            },
            {
                name: 'p_connect_mobile1',
                fieldLabel: 'Моб. телефон 1',
                disabled: true
            },
            {
                name: 'p_connect_mobile2',
                fieldLabel: 'Моб. телефон 2',
                disabled: true
            },
            {
                name: 'p_connect_mobile3',
                fieldLabel: 'Моб. телефон 3',
                disabled: true
            },
            {
                name: 'p_connect_email1',
                fieldLabel: 'E-mail 1',
                disabled: true
            },
            {
                name: 'p_connect_email2',
                fieldLabel: 'E-mail 2',
                disabled: true
            },
            {
                name: 'p_connect_email3',
                fieldLabel: 'E-mail 3',
                disabled: true
            }
        ];
    },

    getReasonField: function () {
        if (this.fieldsDisabled) {
            return {
                name: 'p_sign_reason_display',
                fieldLabel: 'На основании',
                disabled: this.fieldsDisabled
            };
        }
        else {
            return {
                xtype: 'fieldcontainer',
                layout: 'hbox',
                margin: '0 0 5 -1',
                items: [
                    {
                        xtype: 'hidden',
                        name: 'p_sign_reason'
                    },
                    {
                        xtype: 'textfield',
                        name: 'p_sign_reason_display',
                        fieldLabel: 'На основании',
                        flex: 1
                    },
                    {
                        xtype: 'button',
                        text: 'Выбрать',
                        margin: '0 0 0 4',
                        action: 'show_sign_reason_window',
                        width: 60
                    }
                ]
            };
        }
    },

    getFromWhoField: function () {
        if (this.fieldsDisabled) {
            return {
                name: 'p_sign_for_who',
                fieldLabel: 'За кого',
                disabled: this.fieldsDisabled
            };
        }
        else {
            return {
                xtype: 'fieldcontainer',
                layout: 'hbox',
                margin: '0 0 5 -1',
                items: [
                    {
                        xtype: 'hidden',
                        name: 'p_sign_for_who'
                    },
                    {
                        xtype: 'textfield',
                        name: 'p_sign_for_who_display',
                        fieldLabel: 'За кого',
                        flex: 1
                    },
                    {
                        xtype: 'button',
                        text: 'Выбрать',
                        margin: '0 0 0 4',
                        action: 'show_sign_for_who_window',
                        width: 60
                    }
                ]
            };
        }
    },

    getSignatureFieldSet: function () {
        return [this.getReasonField(),
            this.getFromWhoField(), {
                name: 'p_sign_for_who_position',
                fieldLabel: 'Должность',
                disabled: true
            }, {
                name: 'p_sign_date_from',
                fieldLabel: 'Действует от',
                xtype: this.getDateFieldXType(),
                disabled: this.fieldsDisabled
            }, {
                name: 'p_sign_date_end',
                fieldLabel: 'Действительно до',
                xtype: this.getDateFieldXType(),
                disabled: this.fieldsDisabled
            }];
    },

    getItems: function () {
        return [
            {
                layout: 'anchor',
                xtype: 'panel',
                border: 0,
                defaults: {
                    anchor: '100%',
                    labelWidth: 110
                },
                items: this.getBasicFieldSet()
            },
            {
                layout: 'anchor',
                xtype: 'fieldset',
                collapsed: true,
                collapsible: true,
                title: 'Паспорт',
                defaults: {
                    xtype: 'textfield',
                    anchor: '100%'
                },
                anchor: '100%',
                items: this.getPassportFieldSet()
            },
            {
                xtype: 'fieldset',
                title: 'Водительское удостоверение',
                collapsed: true,
                collapsible: true,
                defaults: {
                    xtype: 'textfield',
                    anchor: '100%'
                },
                items: this.getDriverFieldSet()
            },
            {
                xtype: 'fieldset',
                collapsed: true,
                collapsible: true,
                title: 'Прописка',
                defaults: {
                    xtype: 'textfield',
                    anchor: '100%'
                },
                items: this.getAddressFieldSet()
            },
            {
                xtype: 'fieldset',
                collapsed: true,
                collapsible: true,
                title: 'Связь',
                defaults: {
                    xtype: 'textfield',
                    anchor: '100%'
                },
                items: this.getConnectionFieldSet()
            },
            {
                xtype: 'fieldset',
                collapsed: true,
                collapsible: true,
                title: 'Подпись',
                defaults: {
                    xtype: 'textfield',
                    anchor: '100%'
                },
                items: this.getSignatureFieldSet()
            }
        ];
    },

    initComponent: function () {
        this.callParent(arguments);
        if (this.down('button[action=show_employee_window]')) {
            this.down('button[action=show_employee_window]').on('click', function () {
                this.createEmplWindow();
                this.emplWin.show();
            }, this);
        }
        if (this.down('button[action=show_sign_for_who_window]')) {
            this.down('button[action=show_sign_for_who_window]').on('click', function () {
                this.createSignForWhoWindow();
                this.signForWhoWin.show();
            }, this);
        }
        if (this.down('button[action=show_sign_reason_window]')) {
            this.down('button[action=show_sign_reason_window]').on('click', function () {
                this.createSignReasonWindow();
                this.signReasonWin.show();
            }, this);
        }

        var store = Ext.getStore('corporate.auxiliary.SignatureEmployees');
        store.on('load', function () {
            this.setLoading(false);
            var rec = store.getAt(0);
            if (rec) {
                this.down('textfield[name=p_inn]').setValue(rec.get('p_inn'));
                this.down('textfield[name=p_position]').setValue(rec.get('p_position_display'));
                this.down('textfield[name=p_department]').setValue(rec.get('p_department_display'));

                this.down('textfield[name=p_passport_number]').setValue(rec.get('p_passport_number'));
                this.down('textfield[name=p_passport_serie]').setValue(rec.get('p_passport_serie'));
                this.down('textfield[name=p_passport_get]').setValue(rec.get('p_passport_get'));
                this.down('textfield[name=p_passport_date]').setValue(rec.get('p_passport_date'));
                this.down('textfield[name=p_passport_birth]').setValue(rec.get('p_passport_birth'));
                this.down('textfield[name=p_passport_place]').setValue(rec.get('p_passport_place'));

                this.down('textfield[name=p_drive_number]').setValue(rec.get('p_drive_number'));
                this.down('textfield[name=p_drive_serie]').setValue(rec.get('p_drive_serie'));
                this.down('textfield[name=p_drive_get]').setValue(rec.get('p_drive_get'));
                this.down('textfield[name=p_drive_category_display]').setValue(rec.get('p_drive_category_display'));
                this.down('textfield[name=p_drive_date]').setValue(rec.get('p_drive_date'));
                this.down('textfield[name=p_drive_date_end]').setValue(rec.get('p_drive_date_end'));
                this.down('textfield[name=p_drive_age_display]').setValue(rec.get('p_drive_age_display'));

                this.down('textfield[name=p_address_index]').setValue(rec.get('p_address_index'));
                this.down('textfield[name=p_address_country]').setValue(rec.get('p_address_country'));
                this.down('textfield[name=p_address_region]').setValue(rec.get('p_address_region'));
                this.down('textfield[name=p_address_district]').setValue(rec.get('p_address_district'));
                this.down('textfield[name=p_address_city]').setValue(rec.get('p_address_city'));
                this.down('textfield[name=p_address_street]').setValue(rec.get('p_address_street'));
                this.down('textfield[name=p_address_house]').setValue(rec.get('p_address_house'));
                this.down('textfield[name=p_address_housepart]').setValue(rec.get('p_address_housepart'));
                this.down('textfield[name=p_address_building]').setValue(rec.get('p_address_building'));
                this.down('textfield[name=p_address_flat]').setValue(rec.get('p_address_flat'));

                this.down('textfield[name=p_connect_phone1]').setValue(rec.get('p_connect_phone1'));
                this.down('textfield[name=p_connect_phone2]').setValue(rec.get('p_connect_phone2'));
                this.down('textfield[name=p_connect_phone3]').setValue(rec.get('p_connect_phone3'));
                this.down('textfield[name=p_connect_fax]').setValue(rec.get('p_connect_fax'));
                this.down('textfield[name=p_connect_mobile1]').setValue(rec.get('p_connect_mobile1'));
                this.down('textfield[name=p_connect_mobile2]').setValue(rec.get('p_connect_mobile2'));
                this.down('textfield[name=p_connect_mobile3]').setValue(rec.get('p_connect_mobile3'));
                this.down('textfield[name=p_connect_email1]').setValue(rec.get('p_connect_email1'));
                this.down('textfield[name=p_connect_email2]').setValue(rec.get('p_connect_email2'));
                this.down('textfield[name=p_connect_email3]').setValue(rec.get('p_connect_email3'));
            }
        }, this);
    },

    createEmplWindow: function () {
        if (!this.emplWin) {
            this.emplWin = Ext.create('Cement.view.common.EmployeeSelectWindow');
            this.emplWin.on('signitem', function (id, name) {
                this.down('textfield[name=p_employee_display]').setValue(name);
                if (this.down('hidden[name=p_employee]')) {
                    this.down('hidden[name=p_employee]').setValue(id);
                }
                this.setLoading(true);
                Ext.getStore('corporate.auxiliary.SignatureEmployees').clearFilter(true);
                Ext.getStore('corporate.auxiliary.SignatureEmployees').filter('id', id);
                this.emplWin.hide();
            }, this);
        }
    },

    createSignForWhoWindow: function () {
        if (!this.signForWhoWin) {
            this.signForWhoWin = Ext.create('Cement.view.common.EmployeeSelectWindow');
            this.signForWhoWin.on('signitem', function (id, name, position) {
                this.down('textfield[name=p_sign_for_who_display]').setValue(name);
                this.down('hidden[name=p_sign_for_who]').setValue(Ext.JSON.encode(id));
                this.down('textfield[name=p_sign_for_who_position]').setValue(position);
                this.signForWhoWin.hide();
            }, this);
        }
    },

    createSignReasonWindow: function () {
        if (!this.signReasonWin) {
            this.signReasonWin = Ext.create('Cement.view.corporate.signature.ReasonsWindow');
            this.signReasonWin.on('documentsselected', function (model) {
                this.down('textfield[name=p_sign_reason_display]').setValue(model.get('text'));
                this.down('hidden[name=p_sign_reason]').setValue(model.get('id'));
                this.signReasonWin.hide();
            }, this);
        }
    },

    afterRecordLoad: function (rec) {
        if (!this.fieldsDisabled) {
            this.createEmplWindow();
            this.createSignForWhoWindow();
            this.createSignReasonWindow();

            this.emplWin.loadData(rec.get('p_employee'));
            this.signForWhoWin.loadData(rec.get('p_sign_for_who'));
            this.signReasonWin.loadData(rec.get('p_sign_reason'));
        }

        if (this.down('hidden[name=p_employee]')) {
            this.down('hidden[name=p_employee]').setValue(Ext.JSON.encode(rec.get('p_employee')));
        }
        if (this.down('hidden[name=p_sign_for_who]')) {
            this.down('hidden[name=p_sign_for_who]').setValue(Ext.JSON.encode(rec.get('p_sign_for_who')));
        }
        if (this.down('hidden[name=p_sign_reason]')) {
            this.down('hidden[name=p_sign_reason]').setValue(Ext.JSON.encode(rec.get('p_sign_reason')));
        }
    }
});

