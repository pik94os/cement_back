Ext.define('Cement.view.corporate.signature.NewForm', {
    extend: 'Cement.view.corporate.signature.Form',
    alias: 'widget.new_signatureform',

    getItems: function () {
        var addressFields = this.splitArray(this.getAddressFieldSet()),
            passportFields = this.splitArray(this.getPassportFieldSet()),
            driverFields = this.splitArray(this.getDriverFieldSet()),
            connectionFields = this.splitArray(this.getConnectionFieldSet());
        // signatureFields = this.splitArray(this.getSignatureFieldSet());
        return [
            {
                layout: 'anchor',
                xtype: 'panel',
                border: 0,
                defaults: {
                    anchor: '100%',
                    labelWidth: 110
                },
                items: this.getBasicFieldSet()
            },
            this.getTwoColumns('Паспорт', passportFields[0], passportFields[1]),
            this.getTwoColumns('Водительское удостоверение', driverFields[0], driverFields[1]),
            this.getTwoColumns('Прописка', addressFields[0], addressFields[1]),
            this.getTwoColumns('Связь', connectionFields[0], connectionFields[1]),
            {
                xtype: 'fieldset',
                title: 'Подпись',
                collapsed: false,
                collapsible: true,
                defaults: {
                    xtype: 'textfield',
                    anchor: '100%'
                },
                items: this.getSignatureFieldSet()
            }
            // this.getTwoColumns('Подпись', signatureFields[0], signatureFields[1])
        ];
    }
});