Ext.define('Cement.view.corporate.signature.List', {
    extend: 'Cement.view.basic.Grid',
    alias: 'widget.signaturelist',
    bbarText: 'Показаны подписи {0} - {1} из {2}',
    bbarEmptyMsg: 'Нет подписей',
    bbarUsersText: 'Подписей на странице: ',
    gridStore: 'corporate.Signatures',
    gridStateId: 'stateSignaturesList',
    printUrl: Cement.Config.url.corporate.signatures.printGrid,
    helpUrl: Cement.Config.url.corporate.signatures.help,
    deleteUrl: Cement.Config.url.corporate.signatures.deleteUrl,
    printItemUrl: Cement.Config.url.corporate.signatures.printItem,
    cls: 'simple-form',

    getFilterItems: function () {
        return [
            {
                text: 'Фамилия Имя Отчество',
                kind: 'selector',
                field_name: 'p_fio',
                checked: true
            },
            {
                text: 'Должность',
                kind: 'selector',
                field_name: 'p_position',
                checked: true
            },
            {
                text: 'На основании',
                kind: 'selector',
                field_name: 'p_sign_reason',
                checked: true
            },
            {
                text: 'За кого',
                kind: 'selector',
                field_name: 'p_sign_for_who',
                checked: true
            },
            {
                text: 'Действует от',
                kind: 'selector',
                field_name: 'p_sign_date_from',
                checked: true
            },
            {
                text: 'Действительно до',
                kind: 'selector',
                field_name: 'p_sign_date_end',
                checked: true
            }
        ];
    },

    getGridColumns: function () {
        var result = [
            { text: 'Фамилия Имя Отчество', dataIndex: 'p_fio', width: 200, locked: true },
            { text: 'Должность', dataIndex: 'p_position', width: 200 },
            { text: 'На основании', dataIndex: 'p_sign_reason_display', width: 200 },
            { text: 'За кого', dataIndex: 'p_sign_for_who_display', width: 200 },
            { text: 'Действует от', dataIndex: 'p_sign_date_from', width: 200 },
            { text: 'Действительно до', dataIndex: 'p_sign_date_end', width: 200 }
        ];
        return this.mergeActions(result);
    },

    initComponent: function () {
        Ext.apply(this, {
            title: 'Подписи'
        });
        this.callParent(arguments);
    }
});