Ext.define('Cement.view.corporate.bank_account.List', {
    extend: 'Cement.view.basic.Grid',
    alias: 'widget.bankaccountlist',
    bbarText: 'Показаны счета {0} - {1} из {2}',
    bbarEmptyMsg: 'Нет счетов',
    bbarUsersText: 'Счетов на странице: ',
    gridStore: 'corporate.BankAccounts',
    gridStateId: 'stateBankAccountsList',
    printUrl: Cement.Config.url.corporate.bank_accounts.printGrid,
    helpUrl: Cement.Config.url.corporate.bank_accounts.help,
    deleteUrl: Cement.Config.url.corporate.bank_accounts.deleteUrl,
    printItemUrl: Cement.Config.url.corporate.bank_accounts.printItem,
    cls: 'simple-form',

    getFilterItems: function () {
        return [
            {
                text: 'Расчетный счет',
                kind: 'selector',
                field_name: 'p_number',
                checked: true
            },
            {
                text: 'Корреспондентский счет',
                kind: 'selector',
                field_name: 'p_corr',
                checked: true
            },
            {
                text: 'БИК',
                kind: 'selector',
                field_name: 'p_bik',
                checked: true
            },
            {
                text: 'Наименование',
                kind: 'selector',
                field_name: 'p_name',
                checked: true
            }
        ];
    },

    getGridColumns: function () {
        var result = [
            { text: 'Расчетный счет', dataIndex: 'p_number', width: 250, locked: true },
            { text: 'Корреспондентский счет', dataIndex: 'p_corr', width: 250 },
            { text: 'БИК', dataIndex: 'p_bik', width: 250 },
            { text: 'Наименование', dataIndex: 'p_name', width: 250 }
        ];
        return this.mergeActions(result);
    },

    initComponent: function () {
        Ext.apply(this, {
            title: 'Реквизиты счетов'
        });
        this.callParent(arguments);
    }
});