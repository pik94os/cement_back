Ext.define('Cement.view.corporate.bank_account.Form', {
    extend: 'Cement.view.basic.Form',
    alias: 'widget.bankaccountform',
    fieldsDisabled: false,
    url: Cement.Config.url.corporate.bank_accounts.saveUrl,
    method: Cement.Config.url.corporate.bank_accounts.saveMethod,
    printUrl: Cement.Config.url.corporate.bank_accounts.printItem,

    getBikField: function () {
        if (this.fieldsDisabled) {
            return {
                xtype: 'textfield',
                name: 'p_bik',
                disabled: true,
                fieldLabel: 'БИК'
            }
        }
        else {
            var store = Ext.create('Ext.data.Store', {
                pageSize: 10,
                fields: ['id', 'p_bik', 'p_corr', 'p_name'],
                proxy: getProxy(Cement.Config.url.corporate.bank_accounts.autocomplete, 'banks')
            });
            return {
                vtype: 'bank_account_bik',
                allowBlank: false,
                xtype: 'combo',
                store: store,
                displayField: 'p_bik',
                name: 'p_bik',
                fieldLabel: 'БИК',
                typeAhead: false,
                hideTrigger: true,
                anchor: '100%',
                listConfig: {
                    loadingText: 'Поиск...',
                    emptyText: 'Банков не найдено',
                    getInnerTpl: function () {
                        return '<b>{p_bik}</b> - {p_name}';
                        // return '<div class="search-item">' +
                        //     '<h3><span>{[Ext.Date.format(values.lastPost, "M j, Y")]}<br />by {author}</span>{title}</h3>' +
                        //     '{excerpt}' +
                        // '</div>';
                    }
                },
                pageSize: 10,
                listeners: {
                    select: function (combo, selection) {
                        var post = selection[0],
                            form = combo.up('bankaccountform');
                        if (form) {
                            form.down('textfield[name=p_name]').setValue(post.get('p_name'));
                            form.down('textfield[name=p_corr]').setValue(post.get('p_corr'));
                        }
                    }
                }
            };
        }
    },

    getItems: function () {
        return [
            {
                xtype: 'panel',
                border: 0,
                collapsible: false,
                layout: 'anchor',
                anchor: '100%',
                defaults: {
                    anchor: '100%',
                    labelWidth: 110
                },
                items: [
                    {
                        xtype: 'hiddenfield',
                        name: 'id'
                    },
                    {
                        xtype: 'textfield',
                        fieldLabel: 'Расчетный счет',
                        allowBlank: this.allowBlankFields,
                        disabled: this.fieldsDisabled,
                        name: 'p_number',
                        vtype: 'bank_account_number'
                    },
                    {
                        xtype: 'textfield',
                        fieldLabel: 'Кор. счет',
                        allowBlank: this.allowBlankFields,
                        disabled: true,
                        name: 'p_corr'
                    },
                    this.getBikField(),
                    {
                        xtype: 'textfield',
                        fieldLabel: 'Наименование',
                        allowBlank: this.allowBlankFields,
                        disabled: true,
                        name: 'p_name'
                    }
                ]
            }
        ];
    },

    getNavPanel: function () {
        return null;
    },

    afterRecordLoad: function (rec) {
        if (this.down('combo[name=p_bik]')) {
            this.down('combo[name=p_bik]').setRawValue(rec.get('p_bik'));
        }
    },

    initComponent: function () {
        this.callParent(arguments);
    }
});