Ext.define('Cement.view.corporate.employee.TractorCategorieWindow', {
    extend: 'Cement.view.corporate.employee.DrivingCategorieWindow',
    xtype: 'tractor_categories_window',
    title: 'Выбор категории',
    width: 200,
    height: 400,
    modal: true,
    resizable: false,
    layout: 'fit',
    categoriesStore: 'corporate.auxiliary.TractorCategories'
});