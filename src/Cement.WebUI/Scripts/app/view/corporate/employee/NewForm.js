Ext.define('Cement.view.corporate.employee.NewForm', {
    extend: 'Cement.view.corporate.employee.Form',
    alias: 'widget.new_employeeform',

    getItems: function () {
        var passportFields = this.splitArray(this.getPassportFieldSet()),
            driverFields = this.splitArray(this.getDriverFieldSet()),
            tractorFields = this.splitArray(this.getTractorFieldSet()),
            addressFields = this.splitArray(this.getAddressFieldSet()),
            connectionFields = this.splitArray(this.getConnectionFieldSet());
        return [
            {
                layout: 'anchor',
                xtype: 'panel',
                border: 0,
                defaults: {
                    anchor: '100%',
                    labelWidth: 110
                },
                items: [
                    this.getBasicFieldSet()
                ]
            },
            this.getTwoColumns('Паспорт', passportFields[0], passportFields[1]),
            this.getTwoColumns('Водительское удостоверение', driverFields[0], driverFields[1]),
            this.getTwoColumns('Удостоверение тракториста', tractorFields[0], tractorFields[1]),
            this.getTwoColumns('Прописка', addressFields[0], addressFields[1]),
            this.getTwoColumns('Связь', connectionFields[0], connectionFields[1])
        ];
    }
});