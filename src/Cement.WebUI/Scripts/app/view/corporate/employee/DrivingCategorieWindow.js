Ext.define('Cement.view.corporate.employee.DrivingCategorieWindow', {
    extend: 'Ext.window.Window',
    xtype: 'driving_categories_window',
    title: 'Выбор категории',
    width: 200,
    height: 500,
    modal: true,
    resizable: false,
    layout: 'fit',
    categoriesStore: 'corporate.auxiliary.DrivingCategories',

    bbar: [
        {
            text: 'Сохранить',
            action: 'confirm'
        },
        {
            text: 'Отмена',
            action: 'cancel'
        }
    ],

    closeWindow: function () {
        this.hide();
    },

    loadData: function (data) {
        if (data) {
            Ext.each(this.query('checkboxfield'), function (item) {
                item.setValue(false);
                Ext.each(data, function (d) {
                    if (d == item.inputValue) {
                        item.setValue(true);
                    }
                });
            });
        }
    },

    initComponent: function () {
        var cats = Ext.getStore(this.categoriesStore),
            items = [];
        cats.each(function (item) {
            items.push({
                boxLabel: item.get('p_name'),
                name: 'p_categorie',
                xtype: 'checkboxfield',
                inputValue: item.get('id')
            });
        });
        Ext.apply(this, {
            height: items.length * 22 + 155,
            items: [
                {
                    xtype: 'panel',
                    layout: 'autocontainer',
                    border: 0,
                    bodyPadding: 10,
                    items: [
                        {
                            xtype: 'panel',
                            layout: 'autocontainer',
                            border: 1,
                            bodyPadding: 10,
                            items: [
                                {
                                    xtype: 'fieldcontainer',
                                    defaultType: 'checkboxfield',
                                    items: items
                                }
                            ]
                        }
                    ]
                }
            ]
        });
        this.callParent(arguments);
        this.on('beforeclose', function () {
            this.hide();
            return false;
        });
        this.down('button[action=cancel]').on('click', function () {
            this.closeWindow();
        }, this);

        this.down('button[action=confirm]').on('click', function () {
            this.save();
        }, this);

        this.addEvents('save');
    },

    closeWindow: function () {
        this.close();
    },

    save: function (send) {
        var result = [],
            name = [];
        Ext.each(this.query('checkboxfield'), function (item) {
            if (item.getValue()) {
                result.push(item.inputValue);
                name.push(item.boxLabel);
            }
        });
        this.fireEvent('save', result, name);
        this.close();
    }
});