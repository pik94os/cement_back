Ext.define('Cement.view.corporate.employee.Form', {
    extend: 'Cement.view.basic.Form',
    alias: 'widget.employeeform',
    fieldsDisabled: false,
    url: Cement.Config.url.corporate.employes.saveUrl,
    method: Cement.Config.url.corporate.employes.saveMethod,
    printUrl: Cement.Config.url.corporate.employes.printItem,
    activeItem: 0,
    isFilter: false,
    drive_from_StoreId: 'corporate.auxiliary.DriveFrom',
    employee_positions_StoreId: 'corporate.auxiliary.EmployeePositions',
    departmentsStoreId: 'corporate.auxiliary.Departments',

    getPhotoFieldShow: function () {
        if (this.isFilter) return null;
        var result = {
            xtype: 'image',
            src: Cement.Config.default_photo,
            maxWidth: 83,
            maxHeight: 100,
            x: 83,
            cls: 'pos-relative'
        };
        if (!this.fieldsDisabled) {
            result = null;
        }
        return result;
    },

    loadRecordInternal: function (rec) {
        if (rec.get) {
            var src = rec.get('p_photo'),
            img = this.down('image');
            if (img) {
                if (src === undefined || src == '') {
                    src = Cement.Config.default_photo;
                }
                img.setSrc(src);
                img.doComponentLayout();
            }
            this.getForm().loadRecord(rec);
            if (!this.fieldsDisabled) {
                this.createDriveCategoriesWindow();
                this.categoriesWindow.loadData(rec.get('p_drive_category'));
            }
            if (!this.fieldsDisabled) {
                this.createTractorCategoriesWindow();
                this.tractorCategoriesWindow.loadData(rec.get('p_tractor_category'));
            }
            if (this.afterRecordLoad) {
                this.afterRecordLoad(rec);
            }
        }
        else {
            this.reset();
        }
    },

    getPhotoField: function () {
        return null;
        if (this.isFilter) return null;
        var result = {
            name: 'p_photo',
            fieldLabel: 'Фото',
            allowBlank: this.allowBlankFields,
            xtype: 'filefield',
            anchor: '100%',
            buttonText: 'Загрузить',
            disabled: this.fieldsDisabled
        };
        if (this.fieldsDisabled) {
            result = null;
        }
        return result;
    },

    getPassportField: function (num) {
        return null;
        if (this.isFilter) return null;
        var result = {
            name: 'p_passport_page' + num,
            fieldLabel: 'Страница ' + num,
            xtype: 'filefield',
            anchor: '100%',
            buttonText: 'Загрузить',
            disabled: this.fieldsDisabled
        };
        if (this.fieldsDisabled) {
            result = {
                xtype: 'downloadfield',
                name: 'p_passport_page' + num,
                fieldLabel: 'Страница ' + num
            }
        }
        return result;
    },

    getRightsField: function (num) {
        return null;
        if (this.isFilter) return null;
        var result = {
            name: 'p_drive_page' + num,
            fieldLabel: 'Сторона ' + num,
            xtype: 'filefield',
            buttonText: 'Загрузить',
            anchor: '100%',
            disabled: this.fieldsDisabled
        };
        if (this.fieldsDisabled) {
            result = {
                xtype: 'downloadfield',
                name: 'p_drive_page' + num,
                fieldLabel: 'Сторона ' + num
            }
        }
        return result;
    },

    getTractorField: function (num) {
        return null;
        if (this.isFilter) return null;
        var result = {
            name: 'p_tractor_page' + num,
            fieldLabel: 'Страница ' + num,
            xtype: 'filefield',
            buttonText: 'Загрузить',
            anchor: '100%',
            disabled: this.fieldsDisabled
        };
        if (this.fieldsDisabled) {
            result = {
                xtype: 'downloadfield',
                name: 'p_tractor_page' + num,
                fieldLabel: 'Страница ' + num
            }
        }
        return result;
    },

    getDriveCategoryField: function () {
        if (this.fieldsDisabled) {
            return {
                name: 'p_drive_category_display',
                fieldLabel: 'Категории',
                disabled: true
            };
        }
        else {
            return {
                xtype: 'fieldcontainer',
                layout: 'hbox',
                margin: '0 0 5 -1',
                items: [
                    {
                        xtype: 'hidden',
                        name: 'p_drive_category'
                    },
                    {
                        xtype: 'textfield',
                        disabled: true,
                        name: 'p_drive_category_display',
                        fieldLabel: 'Категории',
                        flex: 1
                    },
                    {
                        xtype: 'button',
                        text: 'Выбрать',
                        margin: '0 0 0 4',
                        action: 'show_categories_window',
                        width: 60
                    }
                ]
            };
        }
    },

    getDriverFieldSet: function () {
        return [
            {
                name: 'p_drive_serie',
                fieldLabel: 'Серия',
                disabled: this.fieldsDisabled,
                vtype: 'driving_serie'
            },
            {
                name: 'p_drive_number',
                fieldLabel: 'Номер',
                disabled: this.fieldsDisabled,
                vtype: 'driving_number'
            },
            {
                name: 'p_drive_get',
                fieldLabel: 'Выдано',
                disabled: this.fieldsDisabled,
                vtype: 'driving_get'
            },
            this.getDriveCategoryField(),
            {
                name: 'p_drive_date',
                fieldLabel: 'Дата выдачи',
                xtype: this.getDateFieldXType(),
                disabled: this.fieldsDisabled
            },
            {
                name: 'p_drive_date_end',
                fieldLabel: 'Действительно до',
                xtype: this.getDateFieldXType(),
                disabled: true
            },
            this.getCombo('Стаж', 'p_drive_age', this.drive_from_StoreId, null, null, null, true),
            this.getRightsField(1),
            this.getRightsField(2)
        ];
    },

    getPassportFieldSet: function () {
        return [
            {
                name: 'p_passport_serie',
                fieldLabel: 'Серия',
                allowBlank: this.allowBlankFields,
                disabled: this.fieldsDisabled,
                vtype: 'passport_serie'
            },
            {
                name: 'p_passport_number',
                fieldLabel: 'Номер',
                allowBlank: this.allowBlankFields,
                disabled: this.fieldsDisabled,
                vtype: 'passport_number'
            },
            {
                name: 'p_passport_get',
                fieldLabel: 'Выдан',
                allowBlank: this.allowBlankFields,
                disabled: this.fieldsDisabled,
                vtype: 'passport_get'
            },
            {
                name: 'p_passport_date',
                fieldLabel: 'Дата',
                xtype: this.getDateFieldXType(),
                allowBlank: this.allowBlankFields,
                disabled: this.fieldsDisabled
            },
            {
                name: 'p_passport_birth',
                fieldLabel: 'Дата рождения',
                xtype: this.getDateFieldXType(),
                allowBlank: this.allowBlankFields,
                disabled: this.fieldsDisabled
            },
            {
                name: 'p_passport_place',
                fieldLabel: 'Место рождения',
                allowBlank: this.allowBlankFields,
                disabled: this.fieldsDisabled,
                vtype: 'passport_get'
            },
            this.getPassportField(1),
            this.getPassportField(2)
        ];
    },

    getTractorCategoriesField: function () {
        if (this.fieldsDisabled) {
            return {
                name: 'p_tractor_category_display',
                fieldLabel: 'Категории',
                disabled: this.fieldsDisabled
            };
        }
        else {
            return {
                xtype: 'fieldcontainer',
                layout: 'hbox',
                margin: '0 0 5 -1',
                items: [
                    {
                        xtype: 'hidden',
                        name: 'p_tractor_category'
                    },
                    {
                        xtype: 'textfield',
                        disabled: true,
                        name: 'p_tractor_category_display',
                        fieldLabel: 'Категории',
                        flex: 1
                    },
                    {
                        xtype: 'button',
                        text: 'Выбрать',
                        margin: '0 0 0 4',
                        action: 'show_tractor_categories_window',
                        width: 60
                    }
                ]
            };
        }
    },

    getTractorFieldSet: function () {
        return [
            {
                name: 'p_tractor_serie',
                fieldLabel: 'Серия',
                disabled: this.fieldsDisabled,
                vtype: 'tractor_serie'
            },
            {
                name: 'p_tractor_number',
                fieldLabel: 'Номер',
                disabled: this.fieldsDisabled,
                vtype: 'driving_number'
            },
            {
                name: 'p_tractor_code',
                fieldLabel: 'Код',
                disabled: this.fieldsDisabled,
                vtype: 'tractor_code'
            },
            this.getTractorCategoriesField(),
            {
                name: 'p_tractor_date',
                fieldLabel: 'Дата выдачи',
                xtype: this.getDateFieldXType(),
                disabled: this.fieldsDisabled
            },
            {
                name: 'p_tractor_date_end',
                fieldLabel: 'Действительно до',
                xtype: this.getDateFieldXType(),
                disabled: true
            },
            this.getTractorField(1),
            this.getTractorField(2)
        ];
    },

    getIndexField: function () {
        if (this.fieldsDisabled) {
            return {
                xtype: 'textfield',
                name: 'p_index',
                fieldLabel: 'Индекс',
                disabled: true
            };
        }
        else {
            var store = Ext.create('Ext.data.Store', {
                pageSize: 10,
                fields: [
                    'id', 'p_index', 'p_country', 'p_region',
                    'p_district', 'p_city', 'p_street', 'p_houses',
                    'p_country_display', 'p_region_display',
                    'p_district_display', 'p_city_display', 'p_street_display', 'p_houses_display'
                ],
                proxy: getProxy(Cement.Config.url.index_autocomplete, 'places')
            });
            return {
                allowBlank: false,
                xtype: 'combo',
                store: store,
                minChars: 2,
                displayField: 'p_index',
                name: 'p_index',
                fieldLabel: 'Индекс',
                typeAhead: false,
                hideTrigger: true,
                anchor: '100%',
                listConfig: {
                    loadingText: 'Поиск...',
                    emptyText: 'Адресов не найдено',
                    getInnerTpl: function () {
                        return '<b>{p_index}</b> - {p_country}, {p_district}, {p_city}, {p_street}';
                    }
                },
                pageSize: 10,
                listeners: {
                    select: function (combo, selection) {
                        var post = selection[0],
                            form = combo.up('employeeform');
                        if (form) {
                            form.down('hiddenfield[name=p_country]').setValue(post.get('p_country'));
                            form.down('textfield[name=p_country_display]').setValue(post.get('p_country_display'));
                            form.down('hiddenfield[name=p_region]').setValue(post.get('p_region'));
                            form.down('textfield[name=p_region_display]').setValue(post.get('p_region_display'));
                            form.down('hiddenfield[name=p_district]').setValue(post.get('p_district'));
                            form.down('textfield[name=p_district_display]').setValue(post.get('p_district_display'));
                            form.down('hiddenfield[name=p_city]').setValue(post.get('p_city'));
                            form.down('textfield[name=p_city_display]').setValue(post.get('p_city_display'));
                            form.down('hiddenfield[name=p_street]').setValue(post.get('p_street'));
                            form.down('textfield[name=p_street_display]').setValue(post.get('p_street_display'));
                            if (form.housesStore) {
                                form.housesStore.loadData(post.get('p_houses'));
                                form.down('combo[name=p_house]').enable();
                            }
                        }
                    }
                }
            };
        }
    },

    houseChanged: function (combo, newValue, oldValue) {
        var form = combo.up('employeeform');
        if (form) {
            form.housePartsStore.filter({
                "property": "p_house",
                "value": newValue
            });
            form.down('combo[name=p_address_housepart]').enable();
        }
    },

    housePartChanged: function (combo, newValue, oldValue) {
        var form = combo.up('employeeform');
        if (form) {
            form.houseBuildingStore.filter({
                "property": "p_housepart",
                "value": newValue
            });
            form.down('combo[name=p_address_building]').enable();
        }
    },

    getFieldSuffix: function () {
        return 'display';
        // if (this.fieldsDisabled) {
        //     return '';
        // }
        // else {
        //     return '_display';
        // }
    },

    getHiddenFieldSuffix: function () {
        return '';
        // if (this.fieldsDisabled) {
        //     return '_display';
        // }
        // else {
        //     return '';
        // }
    },

    getAddressFieldSet: function () {
        return [this.getIndexField(), {
            name: 'p_country_display',
            fieldLabel: 'Страна',
            disabled: true
        }, {
            name: 'p_region',
            xtype: 'hiddenfield'
        }, {
            name: 'p_region_display',
            fieldLabel: 'Регион',
            disabled: true
        }, {
            name: 'p_district',
            xtype: 'hiddenfield'
        }, {
            name: 'p_district_display',
            fieldLabel: 'Район',
            disabled: true
        }, {
            name: 'p_city_display',
            fieldLabel: 'Населенный пункт',
            disabled: true
        }, {
            name: 'p_street_display',
            fieldLabel: 'Улица',
            disabled: true
        }, {
            name: 'p_street',
            xtype: 'hiddenfield'
        }, {
            name: 'p_country',
            xtype: 'hiddenfield'
        }, {
            name: 'p_city',
            xtype: 'hiddenfield'
        },
        {
            name: 'p_house',
            fieldLabel: 'Дом',
            allowBlank: this.allowBlankFields,
            disabled: this.fieldsDisabled
        }, {
            name: 'p_flat',
            fieldLabel: 'Квартира',
            disabled: this.fieldsDisabled
        }];
    },

    getConnectionFieldSet: function () {
        return [
            {
                name: 'p_connect_phone1',
                fieldLabel: 'Телефон 1',
                disabled: this.fieldsDisabled
            },
            {
                name: 'p_connect_phone2',
                fieldLabel: 'Телефон 2',
                disabled: this.fieldsDisabled
            },
            {
                name: 'p_connect_phone3',
                fieldLabel: 'Телефон 3',
                disabled: this.fieldsDisabled
            },
            {
                name: 'p_connect_fax',
                fieldLabel: 'Факс',
                disabled: this.fieldsDisabled
            },
            {
                name: 'p_connect_mobile1',
                fieldLabel: 'Моб. телефон 1',
                disabled: this.fieldsDisabled
            },
            {
                name: 'p_connect_mobile2',
                fieldLabel: 'Моб. телефон 2',
                disabled: this.fieldsDisabled
            },
            {
                name: 'p_connect_mobile3',
                fieldLabel: 'Моб. телефон 3',
                disabled: this.fieldsDisabled
            },
            {
                name: 'p_connect_email2',
                fieldLabel: 'E-mail 2',
                disabled: this.fieldsDisabled
            },
            {
                name: 'p_connect_email3',
                fieldLabel: 'E-mail 3',
                disabled: this.fieldsDisabled
            }
        ];
    },

    getDepartmentFields: function () {
        var employeeDepartmentCombo = this.getCombo('Отдел', 'p_department', this.departmentsStoreId);
        if (!this.fieldsDisabled) {
            employeeDepartmentCombo.flex = 1;
            employeeDepartmentCombo.labelWidth = 100;
            return {
                xtype: 'fieldcontainer',
                layout: 'hbox',
                items: [employeeDepartmentCombo, {
                    xtype: 'button',
                    text: 'Создать',
                    margin: '0 0 0 4',
                    action: 'show_create_department_window',
                    width: 60
                }]
            };
        }
        else {
            return employeeDepartmentCombo;
        }
    },

    getLoginPasswordContainer: function() {
        return {
            xtype: 'fieldcontainer',
            layout: 'hbox',
            anchor: '100%',
            items: [
                {
                    xtype: 'textfield',
                    fieldLabel: 'Email/Login',
                    allowBlank: false,
                    disabled: this.fieldsDisabled,
                    name: 'p_connect_email1',
                    vtype: 'email',
                    flex: 1
                },
                {
                    xtype: 'button',
                    text: 'Генерировать новый пароль',
                    margin: '0 0 0 4',
                    name: 'btn_generate_pass',
                    width: 160,
                    hidden: true
                }]
        };
    },

    getBasicFieldSet: function () {
        return {
            xtype: 'fieldset',
            border: 0,
            margin: 0,
            padding: 0,
            defaults: {
                xtype: 'textfield',
                anchor: '100%'
            },
            items: [this.getPhotoFieldShow(), {
                xtype: 'hiddenfield',
                name: 'id'
            },
            this.getLoginPasswordContainer(), {
                xtype: 'textfield',
                fieldLabel: 'Фамилия',
                anchor: '100%',
                allowBlank: this.allowBlankFields,
                disabled: this.fieldsDisabled,
                name: 'p_surname',
                vtype: 'fio'
            }, {
                xtype: 'textfield',
                fieldLabel: 'Имя',
                anchor: '100%',
                allowBlank: this.allowBlankFields,
                disabled: this.fieldsDisabled,
                name: 'p_name',
                vtype: 'fio'
            }, {
                xtype: 'textfield',
                fieldLabel: 'Отчество',
                anchor: '100%',
                allowBlank: this.allowBlankFields,
                disabled: this.fieldsDisabled,
                name: 'p_middlename',
                vtype: 'fio'
            }, this.getCombo('Должность', 'p_position', this.employee_positions_StoreId),
                this.getDepartmentFields(), {
                    xtype: 'textfield',
                    fieldLabel: 'ИНН',
                    anchor: '100%',
                    allowBlank: this.allowBlankFields,
                    disabled: this.fieldsDisabled,
                    name: 'p_inn',
                    vtype: 'inn'
                }, this.getPhotoField()]
        };
    },

    getItems: function () {
        return [
            {
                layout: 'anchor',
                xtype: 'container',
                border: 0,
                defaults: {
                    anchor: '100%',
                    labelWidth: 110
                },
                items: [
                    this.getBasicFieldSet()
                ]
            },
            {
                layout: 'anchor',
                xtype: 'fieldset',
                collapsible: true,
                collapsed: true,
                title: 'Паспорт',
                defaults: {
                    xtype: 'textfield',
                    anchor: '100%'
                },
                anchor: '100%',
                items: this.getPassportFieldSet()
            },
            {
                xtype: 'fieldset',
                title: 'Водительское удостоверение',
                collapsible: true,
                collapsed: true,
                defaults: {
                    xtype: 'textfield',
                    anchor: '100%'
                },
                items: this.getDriverFieldSet()
            },
            {
                xtype: 'fieldset',
                title: 'Удостоверение тракториста',
                collapsible: true,
                collapsed: true,
                defaults: {
                    xtype: 'textfield',
                    anchor: '100%'
                },
                items: this.getTractorFieldSet()
            },
            {
                xtype: 'fieldset',
                collapsible: true,
                collapsed: true,
                title: 'Прописка',
                defaults: {
                    xtype: 'textfield',
                    anchor: '100%'
                },
                items: this.getAddressFieldSet()
            },
            {
                xtype: 'fieldset',
                collapsible: true,
                collapsed: true,
                title: 'Связь',
                defaults: {
                    xtype: 'textfield',
                    anchor: '100%'
                },
                items: this.getConnectionFieldSet()
            }
        ];
    },

    initComponent: function () {
        this.callParent(arguments);
        if (!this.fieldsDisabled) {
            Ext.getStore(this.departmentsStoreId).load();
        }
        var me = this,
            tr_date = this.down('datefield[name=p_tractor_date]'),
            dr_date = this.down('datefield[name=p_drive_date]');
        if (tr_date) {
            tr_date.on('change', function () {
                var dt = tr_date.getValue();
                if (dt == null) {
                    return;
                }
                me.down('datefield[name=p_tractor_date_end]').setValue(new Date(new Date(dt).setFullYear(dt.getFullYear() + 10)));
            });
        }
        if (dr_date) {
            dr_date.on('change', function () {
                var dt = dr_date.getValue();
                if (dt == null) {
                    return;
                }
                me.down('datefield[name=p_drive_date_end]').setValue(new Date(new Date(dt).setFullYear(dt.getFullYear() + 10)));
            });
        }
        if (this.down('button[action=show_categories_window]')) {
            this.down('button[action=show_categories_window]').on('click', function () {
                this.createDriveCategoriesWindow();
                this.categoriesWindow.show();
            }, this);
        }
        if (this.down('button[action=show_tractor_categories_window]')) {
            this.down('button[action=show_tractor_categories_window]').on('click', function () {
                this.createTractorCategoriesWindow();
                this.tractorCategoriesWindow.show();
            }, this);
        }
        if (this.down('button[name=btn_generate_pass]')) {
            this.down('button[name=btn_generate_pass]').on('click', function () {
                Ext.MessageBox.confirm('Внимание', 'Вы действительно хотите сгенерировать новый пароль?', function(btn) {
                    if (btn == 'yes') {
                        var employeeId = me.down('hiddenfield[name=id]').value;
                        var mainView = Ext.ComponentQuery.query('app-main')[0].getEl();

                        mainView.mask('Обновление...', 'x-mask-loading');
                        Ext.Ajax.request({
                            url: '/api/employee/generate_new_pass',
                            method: 'POST',
                            params: {
                                id: employeeId
                            },
                            callback: function() {
                                mainView.unmask();
                            },
                            success: function () {
                                Cement.Msg.info('Успешно сохранено');
                            },
                            failure: function () {
                                Cement.Msg.error('Произошла ошибка при генерации нового пароля');
                            }
                        });
                    }
                });
            }, this);
        }
        var me = this;
        if (this.down('button[action=show_create_department_window]')) {
            this.down('button[action=show_create_department_window]').on('click', function () {
                var win = Ext.create('Ext.window.Window', {
                    modal: true,
                    title: 'Создать отдел',
                    width: 400,
                    height: 125,
                    resizable: false,
                    layout: 'fit',
                    items: [
                        {
                            xtype: 'panel',
                            bodyPadding: 10,
                            border: 0,
                            layout: 'fit',
                            items: [
                                {
                                    xtype: 'panel',
                                    layout: 'anchor',
                                    bodyPadding: 10,
                                    items: [
                                        {
                                            xtype: 'textfield',
                                            fieldLabel: 'Отдел',
                                            labelWidth: 70,
                                            anchor: '100%'
                                        }
                                    ]
                                }
                            ]
                        }
                    ],
                    bbar: [
                        {
                            text: 'Сохранить',
                            action: 'save'
                        },
                        {
                            text: 'Отмена',
                            action: 'cancel'
                        }
                    ]
                });
                win.down('button[action=cancel]').on('click', function () {
                    win.close();
                });
                win.down('button[action=save]').on('click', function () {
                    var value = win.down('textfield').getValue();
                    if (value != '') {
                        Ext.Ajax.request({
                            url: Cement.Config.url.corporate.employes.create_department,
                            method: 'POST',
                            params: {
                                p_name: value
                            },
                            success: function (response) {
                                var data = Ext.decode(response.responseText);
                                if (data.success) {
                                    Ext.getStore(me.departmentsStoreId).add({
                                        p_name: value,
                                        id: data.id
                                    });
                                    me.down('combo[name=p_department]').setValue(data.id);
                                    win.close();
                                }
                                else {
                                    Cement.Msg.error(data.msg);
                                }
                            },
                            failure: function () {
                                Cement.Msg.error('Ошибка при создании группы');
                            }
                        });
                    }
                }, this);
                win.show();
            }, this);
        }
    },

    createDriveCategoriesWindow: function () {
        if (!this.categoriesWindow) {
            this.categoriesWindow = Ext.create('Cement.view.corporate.employee.DrivingCategorieWindow');
            this.categoriesWindow.on('save', function (data, name) {
                this.down('hidden[name=p_drive_category]').setValue(Ext.JSON.encode(data));
                this.down('textfield[name=p_drive_category_display]').setValue(name);
            }, this);
        }
    },

    createTractorCategoriesWindow: function () {
        if (!this.tractorCategoriesWindow) {
            this.tractorCategoriesWindow = Ext.create('Cement.view.corporate.employee.TractorCategorieWindow');
            this.tractorCategoriesWindow.on('save', function (data, name) {
                this.down('hidden[name=p_tractor_category]').setValue(Ext.JSON.encode(data));
                this.down('textfield[name=p_tractor_category_display]').setValue(name);
            }, this);
        }
    },

    afterRecordLoad: function (rec) {
        if (this.down('combo[name=p_index]')) {
            this.down('combo[name=p_index]').setRawValue(rec.get('p_index'));
        }
        
        if (rec.get('id') > 0) {
            this.down('button[name=btn_generate_pass]').show();
        }
    }
});