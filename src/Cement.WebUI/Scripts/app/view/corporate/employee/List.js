Ext.define('Cement.view.corporate.employee.List', {
    extend: 'Cement.view.basic.Grid',
    alias: 'widget.employeelist',
    bbarText: 'Показаны сотрудники {0} - {1} из {2}',
    bbarEmptyMsg: 'Нет сотрудников',
    bbarUsersText: 'Сотрудников на странице: ',
    gridStore: 'corporate.Employees',
    gridStateId: 'stateEmployesList',
    printUrl: Cement.Config.url.corporate.employes.printGrid,
    helpUrl: Cement.Config.url.corporate.employes.help,
    deleteUrl: Cement.Config.url.corporate.employes.deleteUrl,
    printItemUrl: Cement.Config.url.corporate.employes.printItem,
    cls: 'simple-form',

    getFilterItems: function () {
        return [
            {
                text: 'Фамилия Имя Отчество',
                kind: 'selector',
                field_name: 'p_fio',
                checked: true
            },
            {
                text: 'Должность',
                kind: 'selector',
                field_name: 'p_position',
                checked: true
            },
            {
                text: 'Отдел',
                kind: 'selector',
                field_name: 'p_department',
                checked: true
            },
            {
                text: 'Телефон',
                kind: 'selector',
                field_name: 'p_connect_phone1',
                checked: true
            },
            {
                text: 'E-mail',
                kind: 'selector',
                field_name: 'p_connect_email1',
                checked: true
            }
        ];
    },

    getGridColumns: function () {
        var result = [
            { text: 'Фамилия Имя Отчество', dataIndex: 'p_fio', width: 200, locked: true },
            { text: 'Должность', dataIndex: 'p_position_display', width: 200 },
            { text: 'Отдел', dataIndex: 'p_department_display', width: 200 },
            { text: 'Телефон', dataIndex: 'p_connect_phone1', width: 200 },
            { text: 'E-mail', dataIndex: 'p_connect_email1', width: 200 }
        ];
        return this.mergeActions(result);
    },

    initComponent: function () {
        Ext.apply(this, {
            title: 'Сотрудники'
        });
        this.callParent(arguments);
    }
});