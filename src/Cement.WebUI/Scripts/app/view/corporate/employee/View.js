Ext.define('Cement.view.corporate.employee.View', {
    extend: 'Cement.view.corporate.employee.Form',
    alias: 'widget.employeeview',
    fieldsDisabled: true,
    buttons: null,
    border: 0,
    tbar: Cement.Config.modelViewButtons,
    layout: 'autocontainer',
    bbar: null,
    noUserFieldsetsControl: false,
    loadItemUrl: Cement.Config.url.corporate.employes.loadItem,

    initComponent: function () {
        Ext.apply(this, {
            items: [
                {
                    xtype: 'panel',
                    border: 0,
                    layout: 'anchor',
                    defaultType: 'textfield',
                    anchor: '100%',
                    bodyPadding: '10 10 10 5',
                    autoScroll: true,
                    defaults: {
                        anchor: '100%',
                        labelWidth: 110
                    },
                    items: this.getItems()
                }
            ]
        });
        this.callParent(arguments);
        this.on('resize', function (obj, width, height) {
            var img = this.down('image'),
                panel = img.up('panel'),
                pos = (panel.getWidth() - img.getWidth()) / 2 - 2;
            img.setPosition(pos);
        }, this)
        var fs = this.query('fieldset');
        if (fs[1]) {
            fs[1].expand();
            fs[1].collapse();
        }
    },
    
    getLoginPasswordContainer: function () {
        return null;
    }
});