Ext.define('Cement.view.corporate.nomenklature.Form', {
    extend: 'Cement.view.basic.Form',
    alias: 'widget.nomeklature_form',
    fieldsDisabled: false,
    url: Cement.Config.url.corporate.nomenklatures.saveUrl,
    method: Cement.Config.url.corporate.nomenklatures.saveMethod,
    printUrl: Cement.Config.url.corporate.nomenklatures.printItem,
    bbar: Cement.Config.modelCreateButtonsOnly,

    getItems: function () {
        return [
            {
                xtype: 'panel',
                border: 0,
                collapsible: false,
                layout: 'anchor',
                anchor: '100%',
                defaults: {
                    anchor: '100%',
                    xtype: 'textfield',
                    labelWidth: 110
                },
                items: [
                    {
                        xtype: 'hiddenfield',
                        name: 'id'
                    },
                    {
                        fieldLabel: 'Индекс дела',
                        allowBlank: this.allowBlankFields,
                        disabled: this.fieldsDisabled,
                        name: 'p_index'//,
                        // vtype: 'nomenklature'
                    },
                    {
                        fieldLabel: 'Заголовок дела',
                        allowBlank: this.allowBlankFields,
                        disabled: this.fieldsDisabled,
                        name: 'p_title',
                        vtype: 'nomenklature'
                    },
                    {
                        fieldLabel: 'Кол-во ед.хр',
                        allowBlank: this.allowBlankFields,
                        disabled: this.fieldsDisabled,
                        name: 'p_count',
                        xtype: 'numberfield'
                    },
                    {
                        fieldLabel: 'Срок хранения дела и № статей по перечню',
                        allowBlank: this.allowBlankFields,
                        disabled: this.fieldsDisabled,
                        xtype: 'numberfield',
                        name: 'p_deadline'
                    },
                    {
                        fieldLabel: 'Примечания',
                        allowBlank: this.allowBlankFields,
                        disabled: this.fieldsDisabled,
                        name: 'p_comment',
                        vtype: 'nomenklature'
                    },
                    {
                        fieldLabel: 'Год',
                        allowBlank: this.allowBlankFields,
                        disabled: this.fieldsDisabled,
                        name: 'p_year',
                        xtype: 'numberfield',
                        minValue: 1990
                    }
                ]
            }
        ];
    },

    getNavPanel: function () {
        return null;
    }
});