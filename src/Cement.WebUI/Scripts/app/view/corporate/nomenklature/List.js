Ext.define('Cement.view.corporate.nomenklature.List', {
    extend: 'Cement.view.basic.Grid',
    alias: 'widget.nomenklature_list',
    bbarText: 'Показаны номенклатуры дел {0} - {1} из {2}',
    bbarEmptyMsg: 'Нет номенклатур',
    bbarUsersText: 'Номенклатур на странице: ',
    gridStore: 'corporate.Nomenklatures',
    gridStateId: 'stateNomenklaturesList',
    printUrl: Cement.Config.url.corporate.nomenklatures.printGrid,
    helpUrl: Cement.Config.url.corporate.nomenklatures.help,
    deleteUrl: Cement.Config.url.corporate.nomenklatures.deleteUrl,
    printItemUrl: Cement.Config.url.corporate.nomenklatures.printItem,
    listTitle: 'Номенклатура дел',
    cls: 'simple-form',

    getFilterItems: function () {
        return [{
                text: 'Индекс дела',
                kind: 'selector',
                field_name: 'p_index',
                checked: true
            },
            {
                text: 'Заголовок дела',
                kind: 'selector',
                field_name: 'p_title',
                checked: true
            },
            {
                text: 'Кол-во ед.хр',
                kind: 'selector',
                field_name: 'p_count',
                checked: true
            },
            {
                text: 'Срок хранения дела и № статей по перечню',
                kind: 'selector',
                field_name: 'p_deadline',
                checked: true
            },
            {
                text: 'Примечания',
                kind: 'selector',
                field_name: 'p_comment',
                checked: true
            },
            {
                text: 'Год',
                kind: 'selector',
                field_name: 'p_year',
                checked: true
            }
        ];
    },

    getGridColumns: function () {
        var result = [
            { text: 'Индекс дела', dataIndex: 'p_index', width: 200 },
            { text: 'Заголовок дела', dataIndex: 'p_title', width: 200 },
            { text: 'Кол-во ед.хр', dataIndex: 'p_count', width: 200 },
            { text: 'Срок хранения дела и № статей по перечню', dataIndex: 'p_deadline', width: 400 },
            { text: 'Примечания', dataIndex: 'p_comment', width: 200 },
            { text: 'Год', dataIndex: 'p_year', width: 200 }
        ];
        return this.mergeActions(result);
    },

    initComponent: function () {
        Ext.apply(this, {
            title: 'Номенклатура дел'
        });
        this.callParent(arguments);
    }
});