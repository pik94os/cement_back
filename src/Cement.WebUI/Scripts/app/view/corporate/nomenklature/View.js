Ext.define('Cement.view.corporate.nomenklature.View', {
    extend: 'Cement.view.basic.Form',
    alias: 'widget.nomenklature_view',
    fieldsDisabled: true,
    buttons: null,
    border: 0,
    tbar: Cement.Config.modelViewButtons,
    printUrl: Cement.Config.url.corporate.nomenklatures.printItem,
    layout: 'autocontainer',
    bbar: null,
    noUserFieldsetsControl: false,
    bodyCls: 'model-form',

    initComponent: function () {
        Ext.apply(this, {
            items: [
                {
                    xtype: 'panel',
                    border: 0,
                    layout: 'anchor',
                    defaultType: 'textfield',
                    anchor: '100%',
                    bodyPadding: '10 10 10 5',
                    autoScroll: true,
                    defaults: {
                        anchor: '100%',
                        labelWidth: 110
                    },
                    items: [
	                {
	                    xtype: 'fieldset',
	                    layout: 'anchor',
	                    defaults: {
	                        xtype: 'textfield',
	                        anchor: '100%'
	                    },
	                    border: 0,
	                    items: [
				            {
				                fieldLabel: 'Индекс дела',
				                allowBlank: this.allowBlankFields,
				                disabled: this.fieldsDisabled,
				                name: 'p_index'
				            },
                            {
                                fieldLabel: 'Заголовок дела',
                                allowBlank: this.allowBlankFields,
                                disabled: this.fieldsDisabled,
                                name: 'p_title',
                                vtype: 'nomenklature'
                            },
                            {
                                fieldLabel: 'Кол-во ед.хр',
                                allowBlank: this.allowBlankFields,
                                disabled: this.fieldsDisabled,
                                name: 'p_count'
                            },
                            {
                                fieldLabel: 'Срок хранения дела и № статей по перечню',
                                allowBlank: this.allowBlankFields,
                                disabled: this.fieldsDisabled,
                                name: 'p_deadline'
                            },
                            {
                                fieldLabel: 'Примечания',
                                allowBlank: this.allowBlankFields,
                                disabled: this.fieldsDisabled,
                                name: 'p_comment',
                                vtype: 'nomenklature'
                            },
                            {
                                fieldLabel: 'Год',
                                allowBlank: this.allowBlankFields,
                                disabled: this.fieldsDisabled,
                                name: 'p_year',
                                minValue: 1990
                            }
	                    ]
	                }
                    ]
                }
            ]
        });
        this.callParent(arguments);
    }
});