Ext.define('Cement.view.personal.documents.lists.Incoming', {
    extend: 'Cement.view.basic.Grid',
    alias: 'widget.personal_documents_lists_incoming',
    bbarText: 'Показаны документы {0} - {1} из {2}',
    bbarEmptyMsg: 'Нет документов',
    bbarUsersText: 'Документов на странице: ',
    gridStore: 'personal.documents.Incoming',
    gridStateId: 'statePersonalDocumentsListsIncoming',
    printUrl: Cement.Config.url.personal.documents.incoming.printGrid,
    helpUrl: Cement.Config.url.personal.documents.incoming.help,
    printItemUrl: Cement.Config.url.personal.documents.incoming.printItem,
    packetAgreeUrl: Cement.Config.url.personal.documents.incoming.packet_agree,
    packetSignUrl: Cement.Config.url.personal.documents.incoming.packet_sign,
    packetCancelUrl: Cement.Config.url.personal.documents.incoming.packet_cancel,
    showCreateButton: false,

    getSelectedIds: function () {
        var store = this.down('grid').getStore(),
            result = [];
        store.each(function (item) {
            if (item.get('p_selected')) {
                result.push({
                    id: item.get('id'),
                    DocumentType: item.get('DocumentType'),
                });
            }
        });
        return result;
    },

    selectionChanged: function (self, rowIndex, checked) {
        var store = self.up('grid').getStore(),
            hasChecked = false;
        store.each(function (item) {
            if (item.get('p_selected')) {
                hasChecked = true;
            }
        });
        Ext.each(self.up('basicgrid').query('button[role=group_control]'), function (ctl) {
            ctl.setDisabled(!hasChecked);
        });
    },

    getActionColumns: function () {
        return [{
            xtype: 'checkcolumn',
            text: '',
            dataIndex: 'p_selected',
            width: 32,
            locked: true,
            listeners: {
                checkchange: this.selectionChanged
            }
        }, {
            xtype: 'rowactions',
            hideable: false,
            resizeable: false,
            locked: true,
            width: 63,
            hideMode: 'display',
            actions: [
                //{
                //    iconCls: 'icon-toreg-item',
                //    qtip: 'На регистрацию',
                //    hideIndex: 'p_hide_reg'
                //},
                {
                    iconCls: 'icon-confirm-item',
                    hideIndex: 'p_hide_agree',
                    qtip: 'Согласовать',
                    callback: this.singleAgree
                },
                {
                    iconCls: 'icon-dsign-item',
                    qtip: 'Подписать',
                    hideIndex: 'p_hide_sign',
                    callback: this.singleSign
                },
                {
                    iconCls: 'icon-cancel-item',
                    qtip: 'Отменить',
                    hideIndex: 'p_hide_cancel',
                    callback: this.singleCancel
                }
            ],
            keepSelection: true
        }];
    },

    getAdditionalButtons: function () {
        return [{
            xtype: 'button',
            text: 'Согласовать',
            action: 'packet_agree',
            role: 'group_control',
            iconCls: 'icon-confirm-item',
            disabled: true
        }, {
            xtype: 'button',
            iconCls: 'icon-dsign-item',
            text: 'Подписать',
            action: 'packet_sign',
            role: 'group_control',
            disabled: true
        }, {
            xtype: 'button',
            text: 'Отменить',
            iconCls: 'icon-cancel-item',
            action: 'packet_cancel',
            role: 'group_control',
            disabled: true
        }
        //, {
        //    xtype: 'button',
        //    text: 'Группировка',
        //    menu: [{
        //        text: 'Пакет',
        //        checked: false,
        //        name: 'p_packet',
        //        kind: 'group_enabler'
        //    }, {
        //        text: 'Адресат',
        //        checked: false,
        //        name: 'p_addressee',
        //        kind: 'group_enabler'
        //    }]
        //}
        ]
    },

    getFilterItems: function () {
        return [
            {
                text: 'Вид',
                kind: 'selector',
                field_name: 'p_kind',
                checked: true
            },
            {
                text: '№ П/п',
                kind: 'selector',
                field_name: 'p_number',
                checked: true
            },
            {
                text: 'Дата',
                kind: 'selector',
                field_name: 'p_date',
                checked: true
            },
            {
                text: 'Автор',
                kind: 'selector',
                field_name: 'p_addressee',
                checked: true
            },
            {
                text: 'Наименование',
                kind: 'selector',
                field_name: 'p_name',
                checked: true
            }
        ];
    },

    getGridColumns: function () {
        var result = [
            { text: 'Вид', dataIndex: 'p_kind', width: 100, locked: true },
            { text: '№ П/п', dataIndex: 'p_number', width: 150 },
            { text: 'Дата', dataIndex: 'p_date', width: 150 },
            { text: 'Автор', dataIndex: 'p_author_name', width: 200 },
            { text: 'Наименование', dataIndex: 'p_name', width: 350 }
        ];
        return this.mergeActions(result);
    },

    docAction: function (url, text, ids) {
        var me = this;
        var win = Ext.create('Ext.window.Window', {
            title: text,
            width: 400,
            height: 200,
            layout: 'fit',
            bodyPadding: 10,
            modal: true,
            bodyStyle: "background: #f7f7f7",
            items: [{
                xtype: 'textarea',
                fieldLabel: 'Комментарий',
                labelAlign: 'top',
                anchor: '100%',
                height: 150,
                maxLength: 2000,
                enforceMaxLength: true
            }],
            bbar: [{
                xtype: 'button',
                text: 'Да',
                iconCls: 'icon-accept',
                role: 'doit'
            }, '->', {
                xtype: 'button',
                text: 'Нет',
                iconCls: "icon-decline",
                role: 'close'
            }]
        });
        win.down('button[role=doit]').on('click', function () {
            var jsonData = {
                docs: ids,
                comment: win.down('textarea').getValue()
            };

            win.setLoading("Сохранение");

            Ext.Ajax.request({
                method: 'POST',
                url: url,
                headers: { 'Content-Type': 'application/json; charset=UTF-8' },
                jsonData: jsonData,
                success: function (response) {
                    var json = Ext.JSON.decode(response.responseText);
                    me.down('grid').getStore().load();
                    Cement.Msg.info(json.msg);
                    win.setLoading(false);
                    win.close();
                    delete win;
                },
                failure: function (response) {
                    win.setLoading(false);
                    Cement.Msg.error(response, 'Ошибка');
                }
            });
        });
        win.down('button[role=close]').on('click', function () {
            win.close();
            delete win;
        });
        win.show();
    },

    packetAction: function (url, text) {
        this.docAction(url, text, this.getSelectedIds());
    },

    showAgreeWindow: function (selectedIds) {
        var me = this;

        if (!me.agreeWin) {
            var win = Ext.create('Cement.view.common.SignWindow', { singleSelection: true });
            Cement.util.Animation.createWindowAnimation(win);
            win.on('signitem', function (data, send) {

                if (data == null || data.length != 1) {
                    Cement.Msg.warning("Необходимо выбрать сотрудника!");
                    return;
                }

                win.setLoading("Сохранение");

                var jsonData = {
                    docs: win.selectedIds,
                    employeeId: data[0].p_employee_id,
                    comment: data[0].p_comment,
                    send: send
                };
                
                Ext.Ajax.request({
                    method: 'POST',
                    url: me.packetAgreeUrl,
                    headers: { 'Content-Type': 'application/json; charset=UTF-8' },
                    jsonData: jsonData,
                    success: function (response) {
                        var json = Ext.JSON.decode(response.responseText);
                        if (json.success) {
                            if (me.removeAfterSendToSign) {
                                me.down('grid').getStore().reload();
                            }
                            Cement.Msg.info(json.msg);
                        }
                        else {
                            Cement.Msg.error(json.msg);
                        }
                        win.setLoading(false);
                        win.close();
                    },
                    failure: function (response) {
                        win.setLoading(false);
                        win.close();
                        Cement.Msg.error(response, 'Ошибка');
                    }
                });
                
            }, this);
            me.agreeWin = win;
        }
        
        me.agreeWin.down('*[role=sign-src-grid]').getStore().removeAll();
        me.agreeWin.down('*[role=sign-dst-grid]').getStore().removeAll();
        me.agreeWin.down('textarea[name=p_comment]').setValue(null);

        me.agreeWin.selectedIds = selectedIds;
        me.agreeWin.show();
    },

    packetAgree: function () {
        var me = this;
        me.showAgreeWindow(me.getSelectedIds());
    },

    packetCancel: function () {
        this.packetAction(this.packetCancelUrl, 'Вы действительно хотите отменить выбранные элементы?');
    },

    packetSign: function () {
        this.packetAction(this.packetSignUrl, 'Вы действительно хотите подписать выбранные элементы?');
    },

    singleAgree: function (grid, record) {
        var me = grid.up('basicgrid');
        var doc = {
            id: record.get('id'),
            DocumentType: record.get('DocumentType'),
        };
        me.showAgreeWindow([doc]);
    },

    singleCancel: function (grid, record) {
        var bg = grid.up('basicgrid');
        var doc = {
            id: record.get('id'),
            DocumentType: record.get('DocumentType'),
        };
        bg.docAction(bg.packetCancelUrl, 'Вы действительно хотите отменить данный элемент?', [doc]);
    },

    singleSign: function (grid, record) {
        var bg = grid.up('basicgrid');
        var doc = {
            id: record.get('id'),
            DocumentType: record.get('DocumentType'),
        };
        bg.docAction(bg.packetSignUrl, 'Вы действительно хотите подписать данный элемент?', [doc]);
    },

    printItem: function (grid, record) {
        var win = window.open(
            Ext.String.format(this.printItemUrl, record.get('DocumentType'), record.get('id'))
        );

        win.onload = function() {
            grid.getStore().reload();
        };
    },

    initComponent: function () {
        this.callParent(arguments);
        var items = this.down('toolbar').query('menu menucheckitem[kind=group_enabler]'),
            me = this;
        Ext.each(items, function (item) {
            item.on('checkchange', function (self, checked) {
                var store = me.down('grid').getStore();
                store.clearGrouping();
                if (checked) {
                    store.group(self.name);
                }
            });
        });
        this.down('button[action=packet_cancel]').on('click', this.packetCancel, this);
        this.down('button[action=packet_agree]').on('click', this.packetAgree, this);
        this.down('button[action=packet_sign]').on('click', this.packetSign, this);
    }
});