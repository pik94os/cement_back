Ext.define('Cement.view.personal.documents.lists.Outcoming', {
    extend: 'Cement.view.personal.documents.lists.Incoming',
    alias: 'widget.personal_documents_lists_outcoming',
    gridStore: 'personal.documents.Outcoming',
    gridStateId: 'statePersonalDocumentsListsOutcoming',
    printUrl: Cement.Config.url.personal.documents.outcoming.printGrid,
    helpUrl: Cement.Config.url.personal.documents.outcoming.help,
    printItemUrl: Cement.Config.url.personal.documents.outcoming.printItem,
    packetAgreeUrl: Cement.Config.url.personal.documents.outcoming.packet_agree,
    packetSignUrl: Cement.Config.url.personal.documents.outcoming.packet_sign,
    packetCancelUrl: Cement.Config.url.personal.documents.outcoming.packet_cancel,

    getFilterItems: function () {
        return [
            {
                text: 'Вид',
                kind: 'selector',
                field_name: 'p_kind',
                checked: true
            },
            {
                text: '№ П/п',
                kind: 'selector',
                field_name: 'p_number',
                checked: true
            },
            {
                text: 'Дата',
                kind: 'selector',
                field_name: 'p_date',
                checked: true
            },
            {
                text: 'Автор',
                kind: 'selector',
                field_name: 'p_correspondent',
                checked: true
            },
            {
                text: 'Наименование',
                kind: 'selector',
                field_name: 'p_name',
                checked: true
            }
        ];
    },

    getGridColumns: function () {
        var result = [
            { text: 'Вид', dataIndex: 'p_kind', width: 100, locked: true },
            { text: '№ П/п', dataIndex: 'p_number', width: 150 },
            { text: 'Дата', dataIndex: 'p_date', width: 150 },
            { text: 'Автор', dataIndex: 'p_author_name', width: 200 },
            { text: 'Наименование', dataIndex: 'p_name', width: 350 }
        ];
        return this.mergeActions(result);
    },

    initComponent: function () {
        Ext.apply(this, {
            title: 'Исходящие'
        });
        this.callParent(arguments);
    }
});