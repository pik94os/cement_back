Ext.define('Cement.view.personal.messages.lists.Incoming', {
	extend: 'Cement.view.personal.messages.lists.New',
	alias: 'widget.personal_messages_lists_incoming',
    autoLoadStore: true,

	gridStore: 'personal.messages.Incoming',
	gridStateId: 'statePersonalMessagesIncoming',

	printUrl: Cement.Config.url.personal.messages.incoming.printGrid,
	helpUrl: Cement.Config.url.personal.messages.incoming.help,
    deleteUrl: Cement.Config.url.personal.messages.incoming.deleteUrl,
    signItemUrl: Cement.Config.url.personal.messages.incoming.signUrl,
    archiveItemUrl: Cement.Config.url.personal.messages.incoming.archiveItem,

    shownTitle: null,

    getActionColumns: function () {
        return {
            xtype: 'rowactions',
            hideable: false,
            resizeable: false,
            locked: true,
            width: 21,
            actions: [
                //{
                //    iconCls: 'icon-delete-item',
                //    qtip: 'Удалить',
                //    callback: this.deleteItem
                //},
                //{
                //    iconCls: 'icon-sign-item',
                //    qtip: 'На согласование',
                //    hideIndex: 'p_hide_sign',
                //    callback: this.signItem
                //},
                {
                    iconCls: 'icon-archive-item',
                    qtip: 'В архив',
                    callback: this.archiveItem
                }
            ],
            keepSelection: true
        };
    }
});