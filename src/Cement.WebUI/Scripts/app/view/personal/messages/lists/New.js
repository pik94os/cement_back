Ext.define('Cement.view.personal.messages.lists.New', {
	extend: 'Cement.view.basic.Grid',
	alias: 'widget.personal_messages_lists_new',
    autoLoadStore: true,

    bbarText: 'Показаны сообщения {0} - {1} из {2}',
    bbarEmptyMsg: 'Нет сообщений',
    bbarUsersText: 'Сообщений на странице: ',

	gridStore: 'personal.messages.New',
	gridStateId: 'statePersonalMessagesNew',

	printUrl: Cement.Config.url.personal.messages.new_m.printGrid,
	helpUrl: Cement.Config.url.personal.messages.new_m.help,
    deleteUrl: Cement.Config.url.personal.messages.new_m.deleteUrl,
    signItemUrl: Cement.Config.url.personal.messages.new_m.signUrl,

    shownTitle: null,

    getActionColumns: function () {
        return {
            xtype: 'rowactions',
            hideable: false,
            resizeable: false,
            locked: true,
            width: 42,
            actions: [
                {
                    iconCls: 'icon-edit-item',
                    qtip: 'Редактировать',
                    callback: this.editItem
                },
                {
                    iconCls: 'icon-delete-item',
                    qtip: 'Удалить',
                    callback: this.deleteItem
                }
                //,{
                //    iconCls: 'icon-sign-item',
                //    hideIndex: 'p_hide_sign',
                //    qtip: 'На согласование',
                //    callback: this.signItem
                //}
            ],
            keepSelection: true
        };
    },

    getFilterItems: function () {
        return [{
            text: 'Выделить все',
            checked: true,
            checkHandler: this.toggleFilterSelection
        }, '-', {
            text: 'Вид',
            kind: 'selector',
            field_name: 'p_kind',
            checked: true
        }, {
            text: 'Дата',
            kind: 'selector',
            field_name: 'p_date',
            checked: true
        }, {
            text: 'Автор',
            kind: 'selector',
            field_name: 'p_author',
            checked: true
        }, {
            text: 'Тема',
            kind: 'selector',
            field_name: 'p_theme',
            checked: true
        }];
    },

    getGridColumns: function () {
        var result = [
            { text: 'Вид', dataIndex: 'p_kind_display', width: 90, locked: true },
            { text: 'Дата', dataIndex: 'p_date', width: 90 },
            { text: 'Автор', dataIndex: 'p_author_display', width: 200 },
            { text: 'Тема', dataIndex: 'p_theme', width: 460 },
            { text: '', dataIndex: 'p_security', renderer: this.docStatusColRender, width: 43 }
        ];
        return this.mergeActions(result);
    },

    initComponent: function () {
        Ext.apply(this, {
            title: this.shownTitle
        });
        this.callParent(arguments);
    }
});