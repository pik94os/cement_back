Ext.define('Cement.view.personal.messages.lists.Outcoming', {
	extend: 'Cement.view.personal.messages.lists.New',
	alias: 'widget.personal_messages_lists_outcoming',
    autoLoadStore: true,

	gridStore: 'personal.messages.Outcoming',
	gridStateId: 'statePersonalMessagesOutcoming',

	printUrl: Cement.Config.url.personal.messages.outcoming.printGrid,
	helpUrl: Cement.Config.url.personal.messages.outcoming.help,
    deleteUrl: Cement.Config.url.personal.messages.outcoming.deleteUrl,
    archiveItemUrl: Cement.Config.url.personal.messages.outcoming.archiveItem,

    shownTitle: null,

    getActionColumns: function () {
        return {
            xtype: 'rowactions',
            hideable: false,
            resizeable: false,
            locked: true,
            width: 21,
            actions: [
                //{
                //    iconCls: 'icon-delete-item',
                //    qtip: 'Удалить',
                //    callback: this.deleteItem
                //},
                {
                    iconCls: 'icon-archive-item',
                    qtip: 'В архив',
                    callback: this.archiveItem
                }
            ],
            keepSelection: true
        };
    }
});