Ext.define('Cement.view.personal.messages.lists.Archive', {
    extend: 'Cement.view.basic.Grid',
    alias: 'widget.personal_messages_lists_archive',
    autoLoadStore: true,

    bbarText: 'Показаны сообщения {0} - {1} из {2}',
    bbarEmptyMsg: 'Нет сообщений',
    bbarUsersText: 'Сообщений на странице: ',

    gridStore: 'personal.messages.Archive',
    gridStateId: 'statePersonalMessagesArchive1',

    printUrl: Cement.Config.url.personal.messages.archive.printGrid,
    helpUrl: Cement.Config.url.personal.messages.archive.help,
    deleteUrl: Cement.Config.url.personal.messages.archive.deleteUrl,
    unArchiveItemUrl: Cement.Config.url.personal.messages.archive.unArchiveItem,

    shownTitle: null,

    getActionColumns: function () {
        return {
            xtype: 'rowactions',
            hideable: false,
            resizeable: false,
            locked: true,
            width: 42,
            actions: [
                {
                    iconCls: 'icon-unarchive-item',
                    qtip: 'Из архива',
                    callback: this.unArchiveItem
                },
                {
                    iconCls: 'icon-delete-item',
                    qtip: 'Удалить',
                    callback: this.deleteItem
                }
            ],
            keepSelection: true
        };
    },

    getFilterItems: function () {
        return [{
            text: 'Тип',
            kind: 'selector',
            field_name: 'p_type',
            checked: true
        }, {
            text: 'Вид',
            kind: 'selector',
            field_name: 'p_kind',
            checked: true
        }, {
            text: 'Дата',
            kind: 'selector',
            field_name: 'p_date',
            checked: true
        }, {
            text: 'Автор',
            kind: 'selector',
            field_name: 'p_author',
            checked: true
        }, {
            text: 'Тема',
            kind: 'selector',
            field_name: 'p_theme',
            checked: true
        }];
    },

    getGridColumns: function () {
        var result = [
            { text: 'Тип', dataIndex: 'p_type_display', width: 90, locked: true },
            { text: 'Вид', dataIndex: 'p_kind_display', width: 90, locked: true },
            { text: 'Дата', dataIndex: 'p_date', width: 90 },
            { text: 'Автор', dataIndex: 'p_author_display', width: 200 },
            { text: 'Тема', dataIndex: 'p_theme', width: 460 },
            { text: '', dataIndex: 'p_security', renderer: this.docStatusColRender, width: 43 }
        ];
        return this.mergeActions(result);
    },

    initComponent: function () {
        Ext.apply(this, {
            title: this.shownTitle
        });
        this.callParent(arguments);
    }
});