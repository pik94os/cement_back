Ext.define('Cement.view.personal.messages.complex.Incoming', {
	extend: 'Cement.view.basic.Complex',
	alias: 'widget.personal_messages_complex_incoming',
    border: 0,
    enableDetail: false,
    autoLoadStore: false,
    priceId: null,
    bodyPadding: 0,
    padding: 0,
    header: false,
    title: null,
    rowsCount: 3,
    title: 'Входящие',
    tabTitle: 'Входящие',
    cls: 'no-side-borders',
    topXType: 'personal_messages_lists_incoming',
    topDetailType: 'Cement.view.personal.messages.view.View',
    bottomTitle: 'Связанные документы',
    bottomTabs: [{
        title: 'Связанные документы',
        xtype: 'personal_messages_bottom_incoming_connected_documents_incoming',
        detailType: 'Cement.view.goods.auto.view.ConnectedDocument',
        constantFilterParam: 'p_base_doc',
        childrenConstantFilterParam: 'p_base_doc',
        border: 0
    }],
    subBottomTitle: 'Справочник',
    subBottomTabs: [{
        title: 'Маршрут',
        shownTitle: 'Маршрут',
        xtype: 'personal_messages_bottom_incoming_routes',
        detailType: 'Cement.view.products.price.RouteView'
    }]
});