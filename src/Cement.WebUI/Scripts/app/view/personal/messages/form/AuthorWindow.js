Ext.define('Cement.view.personal.messages.form.AuthorWindow', {
  extend: 'Cement.view.goods.auto.form.SupplierContactWindow',
  xtype: 'widget.personal_messages_form_author_window',
  title: 'Выбор автора',
  leftFrameTitle: 'Сотрудники',
  topGridTitle: 'Выбрать автора',
  bottomGridTitle: 'Выбранный автор',
  structure_storeId: 'personal.messages.auxiliary.Authors',
  hideCommentPanel: true,
  singleSelection: true
});