Ext.define('Cement.view.personal.messages.form.EmployeeWindow', {
  extend: 'Cement.view.goods.auto.form.SupplierContactWindow',
  xtype: 'widget.personal_messages_form_employee_window',
  title: 'Выбор сотрудника лица',
  leftFrameTitle: 'Сотрудники',
  topGridTitle: 'Выбрать сотрудника',
  bottomGridTitle: 'Выбранный сотрудник',
  structure_storeId: 'personal.messages.auxiliary.Employees',
  hideCommentPanel: true,
  singleSelection: false,

  sign: function (send) {
    if (this.dstStore.getCount() > 0) {
      this.fireEvent('selected', this.dstStore);
    }
  }
});