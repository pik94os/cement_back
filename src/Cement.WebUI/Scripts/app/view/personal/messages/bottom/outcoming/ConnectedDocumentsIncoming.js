Ext.define('Cement.view.personal.messages.bottom.outcoming.ConnectedDocumentsIncoming', {
	extend: 'Cement.view.goods.auto.lists.ConnectedDocuments',
	alias: 'widget.personal_messages_bottom_outcoming_connected_documents_incoming',

	gridStore: 'personal.messages.bottom.outcoming.ConnectedDocumentsIncoming',
	gridStateId: 'statePersonalMessagesBottomOutcomingConnectedDocumentsIncoming',

    printUrl: Cement.Config.url.personal.messages.documents.printGrid,
    helpUrl: Cement.Config.url.personal.messages.documents.help
});