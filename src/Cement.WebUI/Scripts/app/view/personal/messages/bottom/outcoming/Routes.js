Ext.define('Cement.view.personal.messages.bottom.outcoming.Routes', {
    extend: 'Cement.view.products.price.bottom.NewRoutes',
    alias: 'widget.personal_messages_bottom_outcoming_routes',
    gridStore: 'personal.messages.bottom.outcoming.Routes',
    gridStateId: 'statePersonalMessagesBottomOutcomingRoutes'
});