Ext.define('Cement.view.personal.messages.bottom.new_m.Routes', {
    extend: 'Cement.view.products.price.bottom.NewRoutes',
    alias: 'widget.personal_messages_bottom_new_routes',
    gridStore: 'personal.messages.bottom.new_m.Routes',
    gridStateId: 'statePersonalMessagesBottomNewRoutes'
});