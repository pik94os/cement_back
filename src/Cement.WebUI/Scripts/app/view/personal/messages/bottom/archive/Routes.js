Ext.define('Cement.view.personal.messages.bottom.archive.Routes', {
    extend: 'Cement.view.products.price.bottom.NewRoutes',
    alias: 'widget.personal_messages_bottom_archive_routes',
    gridStore: 'personal.messages.bottom.archive.Routes',
    gridStateId: 'statePersonalMessagesBottomArchiveRoutes'
});