Ext.define('Cement.view.personal.messages.bottom.new_m.ConnectedDocumentsIncoming', {
	extend: 'Cement.view.goods.auto.lists.ConnectedDocuments',
	alias: 'widget.personal_messages_bottom_new_connected_documents_incoming',

	gridStore: 'personal.messages.bottom.new_m.ConnectedDocumentsIncoming',
	gridStateId: 'statePersonalMessagesBottomNewConnectedDocumentsIncoming',

    printUrl: Cement.Config.url.personal.messages.documents.printGrid,
    helpUrl: Cement.Config.url.personal.messages.documents.help
});