Ext.define('Cement.view.personal.messages.bottom.archive.ConnectedDocumentsIncoming', {
	extend: 'Cement.view.goods.auto.lists.ConnectedDocuments',
	alias: 'widget.personal_messages_bottom_archive_connected_documents_incoming',

	gridStore: 'personal.messages.bottom.archive.ConnectedDocumentsIncoming',
	gridStateId: 'statePersonalMessagesBottomArchiveConnectedDocumentsIncoming',

    printUrl: Cement.Config.url.personal.messages.documents.printGrid,
    helpUrl: Cement.Config.url.personal.messages.documents.help
});