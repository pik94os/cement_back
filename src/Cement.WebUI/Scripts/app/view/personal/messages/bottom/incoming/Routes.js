Ext.define('Cement.view.personal.messages.bottom.incoming.Routes', {
    extend: 'Cement.view.products.price.bottom.NewRoutes',
    alias: 'widget.personal_messages_bottom_incoming_routes',
    gridStore: 'personal.messages.bottom.incoming.Routes',
    gridStateId: 'statePersonalMessagesBottomIncomingRoutes'
});