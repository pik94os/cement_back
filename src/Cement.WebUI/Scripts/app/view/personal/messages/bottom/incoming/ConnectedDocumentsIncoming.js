Ext.define('Cement.view.personal.messages.bottom.incoming.ConnectedDocumentsIncoming', {
	extend: 'Cement.view.goods.auto.lists.ConnectedDocuments',
	alias: 'widget.personal_messages_bottom_incoming_connected_documents_incoming',

	gridStore: 'personal.messages.bottom.incoming.ConnectedDocumentsIncoming',
	gridStateId: 'statePersonalMessagesBottomIncomingConnectedDocumentsIncoming',

    printUrl: Cement.Config.url.personal.messages.documents.printGrid,
    helpUrl: Cement.Config.url.personal.messages.documents.help
});