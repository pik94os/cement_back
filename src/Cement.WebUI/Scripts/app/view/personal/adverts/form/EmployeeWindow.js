Ext.define('Cement.view.personal.adverts.form.EmployeeWindow', {
  extend: 'Cement.view.goods.auto.form.SupplierContactWindow',
  xtype: 'widget.personal_adverts_form_employee_window',
  title: 'Выбор сотрудника',
  leftFrameTitle: 'Сотрудники',
  topGridTitle: 'Выбрать сотрудника',
  bottomGridTitle: 'Выбранный сотрудник',
  structure_storeId: 'personal.adverts.auxiliary.Employees',
  hideCommentPanel: true,
  singleSelection: false,

  sign: function (send) {
    if (this.dstStore.getCount() > 0) {
      this.fireEvent('selected', this.dstStore);
    }
  }
});