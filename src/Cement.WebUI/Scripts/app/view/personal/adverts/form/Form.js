Ext.define('Cement.view.personal.adverts.form.Form', {
    extend: 'Cement.view.basic.Form',
    alias: 'widget.personal_adverts_form_form',
    fieldsDisabled: false,
    url: Cement.Config.url.personal.adverts.new_a.saveUrl,
    method: Cement.Config.url.personal.adverts.new_a.saveMethod,
    layout: 'fit',
    allowBlankFields: true,
    saveButtonDisabled: false,

    importancesStoreId: 'office_work.auxiliary.Importances',
    securitiesStoreId: 'office_work.auxiliary.Securities',

    getItems: function () {
        return [{
            xtype: 'fieldset',
            border: 0,
            margin: 0,
            padding: '0 40',
            collapsed: false,
            collapsible: false,
            defaults: {
                xtype: 'textfield',
                anchor: '100%',
                labelWidth: 150,
                labelAlign: 'right'
            },
            items: [{
                xtype: 'hiddenfield',
                name: 'id'
            }, {
                xtype: 'datefield',
                name: 'p_date',
                fieldLabel: 'Дата показа'
            }, {
                name: 'p_theme',
                fieldLabel: 'Тема документа'
            }, {
        xtype: 'textarea',
                name: 'p_content',
        fieldLabel: 'Краткое содержание',
        height: 150
            },
              this.getSelectorField('Автор', 'p_author', 'show_author_window', 150),
            this.getSelectorField('Сотрудники', 'p_employees', 'show_employees_window', 150),
            this.getCombo('Важность', 'p_importance', this.importancesStoreId),
            this.getCombo('Секретность', 'p_security', this.securitiesStoreId),
            this.getFileField('Импорт', 'p_file', '')]
        }];
    },

    initComponent: function () {
        this.callParent(arguments);

        this.down('button[action=show_author_window]').on('click', function () {
            this.createAuthorWindow();
            this.authorWindow.show();
        }, this);

        this.down('button[action=show_employees_window]').on('click', function () {
            this.createEmployeeWindow();
            this.employeeWindow.show();
        }, this);
    },

    createAuthorWindow: function () {
        if (!this.authorWindow) {
            this.authorWindow = Ext.create('Cement.view.personal.adverts.form.AuthorWindow');
            this.authorWindow.on('selected', function (rec) {
                this.down('textfield[name=p_author_display]').setValue(rec.get('p_name'));
                this.down('hiddenfield[name=p_author]').setValue(rec.get('id'));
                this.authorWindow.hide();
            }, this);
        }
    },

    createEmployeeWindow: function () {
        if (!this.employeeWindow) {
            this.employeeWindow = Ext.create('Cement.view.personal.adverts.form.EmployeeWindow');
            this.employeeWindow.on('selected', function (store) {
                var json = [],
                  str = [];
                store.each(function (rec) {
                    json.push(rec.get('id'));
                    str.push(rec.get('p_name'));
                });
                this.down('textfield[name=p_employees_display]').setValue(str);
                this.down('hiddenfield[name=p_employees]').setValue(json);
                this.employeeWindow.hide();
            }, this);
        }
    },

    //afterRecordLoad: function (rec) {
    //	this.createEmployeeWindow();
    //	this.createAuthorWindow();
    //this.authorWindow.loadData([rec.get('p_author')]);
    //this.employeeWindow.loadData(Ext.decode(rec.get('p_employees')));
    //}
});