Ext.define('Cement.view.personal.adverts.form.AuthorWindow', {
  extend: 'Cement.view.goods.auto.form.SupplierContactWindow',
  xtype: 'widget.personal_adverts_form_author_window',
  title: 'Выбор автора',
  leftFrameTitle: 'Сотрудники',
  topGridTitle: 'Выбрать автора',
  bottomGridTitle: 'Выбранный автор',
  structure_storeId: 'personal.adverts.auxiliary.Authors',
  hideCommentPanel: true,
  singleSelection: true
});