Ext.define('Cement.view.personal.adverts.complex.Outcoming', {
	extend: 'Cement.view.basic.Complex',
	alias: 'widget.personal_adverts_complex_outcoming',
    border: 0,
    enableDetail: false,
    autoLoadStore: false,
    priceId: null,
    bodyPadding: 0,
    padding: 0,
    header: false,
    title: null,
    rowsCount: 3,
    title: 'Исходящие',
    tabTitle: 'Исходящие',
    cls: 'no-side-borders',
    topXType: 'personal_adverts_lists_outcoming',
    topDetailType: 'Cement.view.personal.adverts.view.View',
    bottomTitle: 'Связанные документы',
    bottomTabs: [{
        title: 'Связанные документы',
        xtype: 'personal_adverts_bottom_outcoming_connected_documents_incoming',
        detailType: 'Cement.view.goods.auto.view.ConnectedDocument',
        constantFilterParam: 'p_base_doc',
        childrenConstantFilterParam: 'p_base_doc',
        border: 0
    }],
    subBottomTitle: 'Справочник',
    subBottomTabs: [{
        title: 'Маршрут',
        shownTitle: 'Маршрут',
        xtype: 'personal_adverts_bottom_outcoming_routes',
        detailType: 'Cement.view.products.price.RouteView'
    }]
});