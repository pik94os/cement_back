Ext.define('Cement.view.personal.adverts.bottom.archive.ConnectedDocumentsIncoming', {
	extend: 'Cement.view.goods.auto.lists.ConnectedDocuments',
	alias: 'widget.personal_adverts_bottom_archive_connected_documents_incoming',

	gridStore: 'personal.adverts.bottom.archive.ConnectedDocumentsIncoming',
	gridStateId: 'statePersonalAdvertsBottomArchiveConnectedDocumentsIncoming',

    printUrl: Cement.Config.url.personal.adverts.documents.printGrid,
    helpUrl: Cement.Config.url.personal.adverts.documents.help
});