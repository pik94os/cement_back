Ext.define('Cement.view.personal.adverts.bottom.new_a.Routes', {
    extend: 'Cement.view.products.price.bottom.NewRoutes',
    alias: 'widget.personal_adverts_bottom_new_routes',
    gridStore: 'personal.adverts.bottom.new_a.Routes',
    gridStateId: 'statePersonalAdvertsBottomNewRoutes'
});