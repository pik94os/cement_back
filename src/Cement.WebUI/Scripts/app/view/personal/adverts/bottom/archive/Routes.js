Ext.define('Cement.view.personal.adverts.bottom.archive.Routes', {
    extend: 'Cement.view.products.price.bottom.NewRoutes',
    alias: 'widget.personal_adverts_bottom_archive_routes',
    gridStore: 'personal.adverts.bottom.archive.Routes',
    gridStateId: 'statePersonalAdvertsBottomArchiveRoutes'
});