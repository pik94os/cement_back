Ext.define('Cement.view.personal.adverts.bottom.incoming.ConnectedDocumentsIncoming', {
	extend: 'Cement.view.goods.auto.lists.ConnectedDocuments',
	alias: 'widget.personal_adverts_bottom_incoming_connected_documents_incoming',

	gridStore: 'personal.adverts.bottom.incoming.ConnectedDocumentsIncoming',
	gridStateId: 'statePersonalAdvertsBottomIncomingConnectedDocumentsIncoming',

    printUrl: Cement.Config.url.personal.adverts.documents.printGrid,
    helpUrl: Cement.Config.url.personal.adverts.documents.help
});