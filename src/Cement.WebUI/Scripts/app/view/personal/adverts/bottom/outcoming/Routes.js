Ext.define('Cement.view.personal.adverts.bottom.outcoming.Routes', {
    extend: 'Cement.view.products.price.bottom.NewRoutes',
    alias: 'widget.personal_adverts_bottom_outcoming_routes',
    gridStore: 'personal.adverts.bottom.outcoming.Routes',
    gridStateId: 'statePersonalAdvertsBottomOutcomingRoutes'
});