Ext.define('Cement.view.personal.adverts.bottom.outcoming.ConnectedDocumentsIncoming', {
	extend: 'Cement.view.goods.auto.lists.ConnectedDocuments',
	alias: 'widget.personal_adverts_bottom_outcoming_connected_documents_incoming',

	gridStore: 'personal.adverts.bottom.outcoming.ConnectedDocumentsIncoming',
	gridStateId: 'statePersonalAdvertsBottomOutcomingConnectedDocumentsIncoming',

    printUrl: Cement.Config.url.personal.adverts.documents.printGrid,
    helpUrl: Cement.Config.url.personal.adverts.documents.help
});