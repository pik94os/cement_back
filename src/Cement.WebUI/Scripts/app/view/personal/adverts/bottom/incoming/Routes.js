Ext.define('Cement.view.personal.adverts.bottom.incoming.Routes', {
    extend: 'Cement.view.products.price.bottom.NewRoutes',
    alias: 'widget.personal_adverts_bottom_incoming_routes',
    gridStore: 'personal.adverts.bottom.incoming.Routes',
    gridStateId: 'statePersonalAdvertsBottomIncomingRoutes'
});