Ext.define('Cement.view.personal.adverts.bottom.new_a.ConnectedDocumentsIncoming', {
	extend: 'Cement.view.goods.auto.lists.ConnectedDocuments',
	alias: 'widget.personal_adverts_bottom_new_connected_documents_incoming',

	gridStore: 'personal.adverts.bottom.new_a.ConnectedDocumentsIncoming',
	gridStateId: 'statePersonalAdvertsBottomNewConnectedDocumentsIncoming',

    printUrl: Cement.Config.url.personal.adverts.documents.printGrid,
    helpUrl: Cement.Config.url.personal.adverts.documents.help
});