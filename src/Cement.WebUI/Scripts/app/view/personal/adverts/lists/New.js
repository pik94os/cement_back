Ext.define('Cement.view.personal.adverts.lists.New', {
	extend: 'Cement.view.basic.Grid',
	alias: 'widget.personal_adverts_lists_new',
    autoLoadStore: true,

    bbarText: 'Показаны объявления {0} - {1} из {2}',
    bbarEmptyMsg: 'Нет объявлений',
    bbarUsersText: 'Объявлений на странице: ',

	gridStore: 'personal.adverts.New',
	gridStateId: 'statePersonalAdvertsNew',

	printUrl: Cement.Config.url.personal.adverts.new_a.printGrid,
	helpUrl: Cement.Config.url.personal.adverts.new_a.help,
    deleteUrl: Cement.Config.url.personal.adverts.new_a.deleteUrl,
    signItemUrl: Cement.Config.url.personal.adverts.new_a.signUrl,

    shownTitle: null,

    getActionColumns: function () {
        return {
            xtype: 'rowactions',
            hideable: false,
            resizeable: false,
            locked: true,
            width: 42,
            actions: [
                {
                    iconCls: 'icon-edit-item',
                    qtip: 'Редактировать',
                    callback: this.editItem
                },
                {
                    iconCls: 'icon-delete-item',
                    qtip: 'Удалить',
                    callback: this.deleteItem
                }
                //,{
                //    iconCls: 'icon-sign-item',
                //    qtip: 'На согласование',
                //    hideIndex: 'p_hide_sign',
                //    callback: this.signItem
                //}
            ],
            keepSelection: true
        };
    },

    getFilterItems: function () {
        return [{
            text: 'Вид',
            kind: 'selector',
            field_name: 'p_kind',
            checked: true
        }, {
            text: 'Дата',
            kind: 'selector',
            field_name: 'p_date',
            checked: true
        }, {
            text: 'Автор',
            kind: 'selector',
            field_name: 'p_author',
            checked: true
        }, {
            text: 'Тема',
            kind: 'selector',
            field_name: 'p_theme',
            checked: true
        }];
    },

    getGridColumns: function () {
        var result = [
            { text: 'Дата', dataIndex: 'p_date', width: 90 },
            { text: 'Тема', dataIndex: 'p_theme', width: 960 },
            { text: '', dataIndex: 'p_security', renderer: this.docStatusColRender, width: 43 }
        ];
        return this.mergeActions(result);
    },

    initComponent: function () {
        Ext.apply(this, {
            title: this.shownTitle
        });
        this.callParent(arguments);
    }
});