Ext.define('Cement.view.personal.adverts.lists.Archive', {
    extend: 'Cement.view.basic.Grid',
    alias: 'widget.personal_adverts_lists_archive',
    autoLoadStore: true,

    bbarText: 'Показаны объявления {0} - {1} из {2}',
    bbarEmptyMsg: 'Нет объявлений',
    bbarUsersText: 'Объявлений на странице: ',

    gridStore: 'personal.adverts.Archive',
    gridStateId: 'statePersonalAdvertsArchive',

    printUrl: Cement.Config.url.personal.adverts.archive.printGrid,
    helpUrl: Cement.Config.url.personal.adverts.archive.help,
    deleteUrl: Cement.Config.url.personal.adverts.archive.deleteUrl,
    unArchiveItemUrl: Cement.Config.url.personal.adverts.archive.unArchiveItem,

    shownTitle: null,

    getActionColumns: function () {
        return {
            xtype: 'rowactions',
            hideable: false,
            resizeable: false,
            locked: true,
            width: 42,
            actions: [
                {
                    iconCls: 'icon-unarchive-item',
                    qtip: 'Из архива',
                    callback: this.unArchiveItem
                },
                {
                    iconCls: 'icon-delete-item',
                    qtip: 'Удалить',
                    callback: this.deleteItem
                }
            ],
            keepSelection: true
        };
    },

    getFilterItems: function () {
        return [{
            text: 'Тип',
            kind: 'selector',
            field_name: 'p_type',
            checked: true
        }, {
            text: 'Вид',
            kind: 'selector',
            field_name: 'p_kind',
            checked: true
        }, {
            text: 'Дата',
            kind: 'selector',
            field_name: 'p_date',
            checked: true
        }, {
            text: 'Автор',
            kind: 'selector',
            field_name: 'p_author',
            checked: true
        }, {
            text: 'Тема',
            kind: 'selector',
            field_name: 'p_theme',
            checked: true
        }];
    },

    getGridColumns: function () {
        var result = [
            { text: 'Тип', dataIndex: 'p_kind', width: 90, locked: true },
            { text: 'Дата', dataIndex: 'p_date', width: 90 },
            { text: 'Тема', dataIndex: 'p_theme', width: 860 },
            { text: '', dataIndex: 'p_security', renderer: this.docStatusColRender, width: 43 }
        ];
        return this.mergeActions(result);
    },

    initComponent: function () {
        Ext.apply(this, {
            title: this.shownTitle
        });
        this.callParent(arguments);
    }
});