Ext.define('Cement.view.personal.adverts.lists.Outcoming', {
	extend: 'Cement.view.personal.adverts.lists.New',
	alias: 'widget.personal_adverts_lists_outcoming',
    autoLoadStore: true,

	gridStore: 'personal.adverts.Outcoming',
	gridStateId: 'statePersonalAdvertsOutcoming',

	printUrl: Cement.Config.url.personal.adverts.outcoming.printGrid,
	helpUrl: Cement.Config.url.personal.adverts.outcoming.help,
    deleteUrl: Cement.Config.url.personal.adverts.outcoming.deleteUrl,
    archiveItemUrl: Cement.Config.url.personal.adverts.outcoming.archiveItem,

    shownTitle: null,

    getActionColumns: function () {
        return {
            xtype: 'rowactions',
            hideable: false,
            resizeable: false,
            locked: true,
            width: 21,
            actions: [
                //{
                //    iconCls: 'icon-delete-item',
                //    qtip: 'Удалить',
                //    callback: this.deleteItem
                //},
                {
                    iconCls: 'icon-archive-item',
                    qtip: 'В архив',
                    callback: this.archiveItem
                }
            ],
            keepSelection: true
        };
    }
});