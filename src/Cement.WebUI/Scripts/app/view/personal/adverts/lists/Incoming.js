Ext.define('Cement.view.personal.adverts.lists.Incoming', {
	extend: 'Cement.view.personal.adverts.lists.New',
	alias: 'widget.personal_adverts_lists_incoming',
    autoLoadStore: true,

	gridStore: 'personal.adverts.Incoming',
	gridStateId: 'statePersonalAdvertsIncoming',

	printUrl: Cement.Config.url.personal.adverts.incoming.printGrid,
	helpUrl: Cement.Config.url.personal.adverts.incoming.help,
    deleteUrl: Cement.Config.url.personal.adverts.incoming.deleteUrl,
    signItemUrl: Cement.Config.url.personal.adverts.incoming.signUrl,
    archiveItemUrl: Cement.Config.url.personal.adverts.incoming.archiveItem,

    shownTitle: null,

    getActionColumns: function () {
        return {
            xtype: 'rowactions',
            hideable: false,
            resizeable: false,
            locked: true,
            width: 21,
            actions: [
                //{
                //    iconCls: 'icon-delete-item',
                //    qtip: 'Удалить',
                //    callback: this.deleteItem
                //},
                //{
                //    iconCls: 'icon-sign-item',
                //    qtip: 'На согласование',
                //    hideIndex: 'p_hide_sign',
                //    callback: this.signItem
                //},
                {
                    iconCls: 'icon-archive-item',
                    qtip: 'В архив',
                    callback: this.archiveItem
                }
            ],
            keepSelection: true
        };
    }
});