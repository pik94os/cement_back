Ext.define('Cement.view.personal.correspondention.lists.Incoming', {
    extend: 'Cement.view.personal.documents.lists.Incoming',
    alias: 'widget.personal_correspondention_lists_incoming',
    gridStore: 'personal.correspondention.Incoming',
    gridStateId: 'statePersonalСorrespondentionListsIncoming',
    printUrl: Cement.Config.url.personal.correspondention.incoming.printGrid,
    helpUrl: Cement.Config.url.personal.correspondention.incoming.help,
    printItemUrl: Cement.Config.url.personal.correspondention.incoming.printItem,
    packetAgreeUrl: Cement.Config.url.personal.correspondention.incoming.packet_agree,
    packetSignUrl: Cement.Config.url.personal.correspondention.incoming.packet_sign,
    packetCancelUrl: Cement.Config.url.personal.correspondention.incoming.packet_cancel
});