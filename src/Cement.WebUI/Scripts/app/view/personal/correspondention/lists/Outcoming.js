Ext.define('Cement.view.personal.correspondention.lists.Outcoming', {
    extend: 'Cement.view.personal.documents.lists.Outcoming',
    alias: 'widget.personal_correspondention_lists_outcoming',
    gridStore: 'personal.correspondention.Outcoming',
    gridStateId: 'statePersonalCorrespondentionListsOutcoming',
    printUrl: Cement.Config.url.personal.correspondention.outcoming.printGrid,
    helpUrl: Cement.Config.url.personal.correspondention.outcoming.help,
    printItemUrl: Cement.Config.url.personal.correspondention.outcoming.printItem,
    packetAgreeUrl: Cement.Config.url.personal.correspondention.outcoming.packet_agree,
    packetSignUrl: Cement.Config.url.personal.correspondention.outcoming.packet_sign,
    packetCancelUrl: Cement.Config.url.personal.correspondention.outcoming.packet_cancel
});