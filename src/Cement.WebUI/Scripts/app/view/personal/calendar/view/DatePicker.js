Ext.define('Cement.view.personal.calendar.view.DatePicker', {
  extend: 'Ext.picker.Date',
  alias: 'widget.personal_calendar_view_date_picker',
  border: 0
});