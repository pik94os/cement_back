//@define Cement.view.personal.calendar.data.EventMappings
/**
 * @class Cement.view.personal.calendar.data.EventMappings
 * @extends Object
 * A simple object that provides the field definitions for Event records so that they can be easily overridden.
 */
Ext.ns('Cement.view.personal.calendar.data');

Cement.view.personal.calendar.data.EventMappings = {
    EventId: {
        name: 'EventId',
        mapping: 'id',
        type: 'int'
    },
    CalendarId: {
        name: 'CalendarId',
        mapping: 'p_color',
        type: 'int'
    },
    Title: {
        name: 'Title',
        mapping: 'p_title',
        type: 'string'
    },
    StartDate: {
        name: 'StartDate',
        mapping: 'p_start',
        type: 'date',
        dateFormat: 'd.m.Y H:i'
    },
    EndDate: {
        name: 'EndDate',
        mapping: 'p_end',
        type: 'date',
        dateFormat: 'd.m.Y H:i'
    },
    Location: {
        name: 'Location',
        mapping: 'loc',
        type: 'string'
    },
    Notes: {
        name: 'Notes',
        mapping: 'notes',
        type: 'string'
    },
    Url: {
        name: 'Url',
        mapping: 'url',
        type: 'string'
    },
    IsAllDay: {
        name: 'IsAllDay',
        mapping: 'ad',
        type: 'boolean'
    },
    Reminder: {
        name: 'Reminder',
        mapping: 'rem',
        type: 'string'
    },
    IsNew: {
        name: 'IsNew',
        mapping: 'n',
        type: 'boolean'
    }
};
