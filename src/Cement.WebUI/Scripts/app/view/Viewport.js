Ext.define('Cement.view.Viewport', {
    extend: 'Ext.container.Viewport',
    requires: [
        'Ext.layout.container.Fit',
        'Cement.view.Main'
    ],

    layout: {
        type: 'fit'
    },

    items: [
        {
            xtype: 'app-main'
        }
    ]
});
