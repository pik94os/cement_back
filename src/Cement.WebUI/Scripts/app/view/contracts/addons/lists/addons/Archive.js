Ext.define('Cement.view.contracts.addons.lists.addons.Archive', {
    extend: 'Cement.view.contracts.contracts.lists.addons.New',
    alias: 'widget.contracts_addons_lists_addons_archive',
    autoLoadStore: false,

    gridStore: 'addons.addons.Archive',
    gridStateId: 'stateAddonsAddonsArchive',

    printUrl: Cement.Config.url.contracts.addons.addons.printGrid,
	helpUrl: Cement.Config.url.contracts.addons.addons.help,
    deleteUrl: Cement.Config.url.contracts.addons.addons.deleteUrl
});