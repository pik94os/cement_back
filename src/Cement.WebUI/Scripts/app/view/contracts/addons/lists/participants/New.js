Ext.define('Cement.view.contracts.addons.lists.participants.New', {
    extend: 'Cement.view.contracts.contracts.lists.participants.New',
    alias: 'widget.contracts_addons_lists_participants_new',
    autoLoadStore: false,

    gridStore: 'addons.participants.New',
    gridStateId: 'stateAddonsParticipantsNew',

    printUrl: Cement.Config.url.contracts.addons.participants.printGrid,
	helpUrl: Cement.Config.url.contracts.addons.participants.help,
    deleteUrl: Cement.Config.url.contracts.addons.participants.deleteUrl
});