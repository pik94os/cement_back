Ext.define('Cement.view.contracts.addons.lists.Outcoming', {
	extend: 'Cement.view.contracts.addons.lists.New',
	alias: 'widget.contracts_addons_lists_outcoming',
    autoLoadStore: false,

	gridStore: 'addons.Outcoming',
	gridStateId: 'stateAddonsOutcoming',

	printUrl: Cement.Config.url.contracts.addons.outcoming.printGrid,
	helpUrl: Cement.Config.url.contracts.addons.outcoming.help,
    archiveItemUrl: Cement.Config.url.contracts.addons.outcoming.send_to_archive,

    shownTitle: null,

    getActionColumns: function () {
        return {
            xtype: 'rowactions',
            hideable: false,
            resizeable: false,
            locked: true,
            width: 23,
            actions: [
                //{
                //    iconCls: 'icon-edit-item', 
                //    qtip: 'Редактировать', 
                //    callback: this.editItem
                //}, { 
                //    iconCls: 'icon-delete-item', 
                //    qtip: 'Удалить', 
                //    callback: this.deleteItem
                //},
                {
                    iconCls: 'icon-archive-item',
                    qtip: 'Отправить в архив',
                    callback: this.archiveItem
                }
            ],
            keepSelection: true
        };
    }
});