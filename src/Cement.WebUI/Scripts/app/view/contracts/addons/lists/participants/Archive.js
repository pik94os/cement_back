Ext.define('Cement.view.contracts.addons.lists.participants.Archive', {
    extend: 'Cement.view.contracts.addons.lists.participants.New',
    alias: 'widget.contracts_addons_lists_participants_archive',
    autoLoadStore: false,

    gridStore: 'addons.participants.Archive',
    gridStateId: 'stateAddonsParticipantsArchive'
});