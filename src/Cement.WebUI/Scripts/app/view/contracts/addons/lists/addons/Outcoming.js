Ext.define('Cement.view.contracts.addons.lists.addons.Outcoming', {
    extend: 'Cement.view.contracts.addons.lists.addons.Incoming',
    alias: 'widget.contracts_addons_lists_addons_outcoming',
    autoLoadStore: false,

    gridStore: 'addons.addons.Outcoming',
    gridStateId: 'stateAddonsAddonsOutcoming'
});