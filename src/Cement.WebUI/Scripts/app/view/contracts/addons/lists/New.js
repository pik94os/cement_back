Ext.define('Cement.view.contracts.addons.lists.New', {
	extend: 'Cement.view.basic.Grid',
	alias: 'widget.contracts_addons_lists_new',
    autoLoadStore: false,

    bbarText: 'Показаны приложения {0} - {1} из {2}',
    bbarEmptyMsg: 'Нет приложений',
    bbarUsersText: 'Приложений на странице: ',

	gridStore: 'addons.New',
	gridStateId: 'stateAddonsNew',

	printUrl: Cement.Config.url.contracts.addons.addons_new.printGrid,
	helpUrl: Cement.Config.url.contracts.addons.addons_new.help,
    deleteUrl: Cement.Config.url.contracts.addons.addons_new.deleteUrl,

    directorsStoreId: 'contract_new_directors_Store',

    shownTitle: null,

    creatorTree: Ext.clone(Cement.Creators.contracts),
    createWindowTitle: 'Создать договор',

    getActionColumns: function () {
        return {
            xtype: 'rowactions',
            hideable: false,
            locked: true,
            resizeable: false,
            width: 46,
            actions: [{ 
                iconCls: 'icon-edit-item', 
                qtip: 'Редактировать', 
                callback: this.editItem
            }, { 
                iconCls: 'icon-delete-item', 
                qtip: 'Удалить', 
                callback: this.deleteItem
            }],
            keepSelection: true
        };
    },

    getFilterItems: function () {
        return [{
            text: 'Вид документа',
            kind: 'selector',
            field_name: 'p_document_kind_display',
            checked: true
        }, {
            text: 'К документу',
            kind: 'selector',
            field_name: 'p_document_to_display',
            checked: true
        }, {
            text: '№',
            kind: 'selector',
            field_name: 'p_number',
            checked: true
        }, {
            text: 'Дата',
            kind: 'selector',
            field_name: 'p_date',
            checked: true
        }, {
            text: 'Наименование',
            kind: 'selector',
            field_name: 'p_name',
            checked: true
        }, {
            text: 'Подписант',
            kind: 'selector',
            field_name: 'p_sign_employee',
            checked: true
        }, {
            text: 'Наименование',
            kind: 'selector',
            field_name: 'p_name_2',
            checked: true
        }, {
            text: 'Подписант',
            kind: 'selector',
            field_name: 'p_sign_employee_2',
            checked: true
        }, {
            text: 'Начало',
            kind: 'selector',
            field_name: 'p_date_start',
            checked: true
        }, {
            text: 'Окончание',
            kind: 'selector',
            field_name: 'p_date_end',
            checked: true
        }];
    },

    getGridColumns: function () {
        var result = [
            { text: 'Вид документа', dataIndex: 'p_document_kind_display', width: 150, locked: true },
            { text: 'К документу', dataIndex: 'p_document_to_display', width: 150, locked: true },
            { text: '№', dataIndex: 'p_number', width: 70, locked: true },
            { text: 'Дата', dataIndex: 'p_date', width: 60 },
            { text: 'Наименование', dataIndex: 'p_name', flex: 1, minWidth: 150 },
            { text: 'Подписант', dataIndex: 'p_sign_employee_display', width: 150 },
            { text: 'Наименование', dataIndex: 'p_name_2', width: 150 },
            { text: 'Подписант', dataIndex: 'p_sign_employee_2_display', width: 150 },
            { text: 'Начало', dataIndex: 'p_date_start', width: 60 },
            { text: 'Окончание', dataIndex: 'p_date_end', width: 60 }
        ];
        return this.mergeActions(result);
    },

    initComponent: function () {
        Ext.apply(this, {
            title: this.shownTitle
        });
        this.callParent(arguments);
    }
});