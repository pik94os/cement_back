Ext.define('Cement.view.contracts.addons.lists.Archive', {
	extend: 'Cement.view.contracts.contracts.lists.New',
	alias: 'widget.contracts_addons_lists_archive',
    autoLoadStore: false,

	gridStore: 'addons.Archive',
	gridStateId: 'stateAddonsArchive',

	printUrl: Cement.Config.url.contracts.addons.archive.printGrid,
	helpUrl: Cement.Config.url.contracts.addons.archive.help,
    deleteUrl: Cement.Config.url.contracts.addons.archive.deleteUrl,
    unArchiveItemUrl: Cement.Config.url.contracts.addons.archive.unArchiveItem,

    shownTitle: null,

    getActionColumns: function () {
        return {
            xtype: 'rowactions',
            hideable: false,
            resizeable: false,
            width: 46,
            locked: true,
            actions: [{
                iconCls: 'icon-unarchive-item',
                qtip: 'Из архива',
                callback: this.unArchiveItem
            }, {
                iconCls: 'icon-delete-item',
                qtip: 'Удалить',
                callback: this.deleteItem
            }],
            keepSelection: true
        };
    },

    getGridColumns: function () {
        var result = [
            { text: 'Тип', dataIndex: 'p_type_display', width: 150, locked: true },
            { text: 'Вид документа', dataIndex: 'p_document_kind_display', width: 150, locked: true },
            { text: '№', dataIndex: 'p_number', width: 70, locked: true },
            { text: 'К документу', dataIndex: 'p_document_to_display', width: 150 },
            { text: 'Дата', dataIndex: 'p_date', width: 150 },
            { text: 'Наименование', dataIndex: 'p_name', width: 150 },
            { text: 'Подписант', dataIndex: 'p_sign_employee_display', width: 150 },
            { text: 'Наименование', dataIndex: 'p_name_2', width: 150 },
            { text: 'Подписант', dataIndex: 'p_sign_employee_2_display', width: 150 },
            { text: 'Начало', dataIndex: 'p_date_start', width: 150 },
            { text: 'Окончание', dataIndex: 'p_date_end', width: 150 }
        ];
        return this.mergeActions(result);
    }
});