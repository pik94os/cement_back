Ext.define('Cement.view.contracts.addons.lists.participants.Outcoming', {
    extend: 'Cement.view.contracts.addons.lists.participants.New',
    alias: 'widget.contracts_addons_lists_participants_outcoming',
    autoLoadStore: false,

    gridStore: 'addons.participants.Outcoming',
    gridStateId: 'stateAddonsParticipantsOutcoming'
});