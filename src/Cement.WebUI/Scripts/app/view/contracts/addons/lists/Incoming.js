Ext.define('Cement.view.contracts.addons.lists.Incoming', {
	extend: 'Cement.view.contracts.addons.lists.New',
	alias: 'widget.contracts_addons_lists_incoming',
    autoLoadStore: false,

	gridStore: 'addons.Incoming',
	gridStateId: 'stateAddonsIncoming',

	printUrl: Cement.Config.url.contracts.addons.incoming.printGrid,
	helpUrl: Cement.Config.url.contracts.addons.incoming.help,
	archiveItemUrl: Cement.Config.url.contracts.addons.incoming.send_to_archive,

    directorsStoreId: 'contract_incoming_directors_Store',

    shownTitle: null,

    getActionColumns: function () {
        return {
            xtype: 'rowactions',
            hideable: false,
            resizeable: false,
            locked: true,
            width: 23,
            actions: [
                {
                    iconCls: 'icon-archive-item',
                    qtip: '��������� � �����',
                    callback: this.archiveItem
                }
            ],
            keepSelection: true
        };
    }
});