Ext.define('Cement.view.contracts.addons.lists.addons.New', {
	extend: 'Cement.view.contracts.contracts.lists.New',
	alias: 'widget.contracts_addons_lists_addons_new',
    autoLoadStore: false,

    bbarText: 'Показаны приложения {0} - {1} из {2}',
    bbarEmptyMsg: 'Нет приложений',
    bbarUsersText: 'Приложений на странице: ',

	gridStore: 'addons.addons.New',
	gridStateId: 'stateAddonsAddonsNew',

	printUrl: Cement.Config.url.contracts.addons.addons_new.printGrid,
	helpUrl: Cement.Config.url.contracts.addons.addons_new.help,
    deleteUrl: Cement.Config.url.contracts.addons.addons_new.deleteUrl,

    shownTitle: 'Приложения',
    showFilterButton: false,

    getGridFeatures: function () { return null },

    getActionColumns: function () {
        return null;
    },

    getToolbarItems: function () {
        return null;
    },

    getGridColumns: function () {
        var result = [
            { text: 'Тип', dataIndex: 'p_type_display', width: 150, locked: true },
            { text: 'Вид документа', dataIndex: 'p_document_kind_display', width: 150, locked: true },
            { text: '№', dataIndex: 'p_number', width: 70, locked: true },
            { text: 'Дата', dataIndex: 'p_date', width: 150 },
            { text: 'Наименование', dataIndex: 'p_name', width: 150 },
            { text: 'Подписант', dataIndex: 'p_sign_employee_display', width: 150 },
            { text: 'Наименование', dataIndex: 'p_name_2', width: 150 },
            { text: 'Подписант', dataIndex: 'p_sign_employee_2_display', width: 150 },
            { text: 'Начало', dataIndex: 'p_date_start', width: 150 },
            { text: 'Окончание', dataIndex: 'p_date_end', width: 150 }
        ];
        return this.mergeActions(result);
    },

    initComponent: function () {
        Ext.apply(this, {
            title: this.shownTitle
        });
        this.callParent(arguments);
    }
});