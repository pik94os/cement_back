Ext.define('Cement.view.contracts.addons.lists.participants.Incoming', {
    extend: 'Cement.view.contracts.addons.lists.participants.New',
    alias: 'widget.contracts_addons_lists_participants_incoming',
    autoLoadStore: false,

    gridStore: 'addons.participants.Incoming',
    gridStateId: 'stateAddonsParticipantsIncoming'
});