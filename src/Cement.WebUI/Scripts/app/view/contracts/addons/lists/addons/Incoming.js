Ext.define('Cement.view.contracts.addons.lists.addons.Incoming', {
    extend: 'Cement.view.contracts.contracts.lists.addons.New',
    alias: 'widget.contracts_addons_lists_addons_incoming',
    autoLoadStore: false,

    gridStore: 'addons.addons.Incoming',
    gridStateId: 'stateAddonsAddonsIncoming',

    printUrl: Cement.Config.url.contracts.addons.addons.printGrid,
	helpUrl: Cement.Config.url.contracts.addons.addons.help,
    deleteUrl: Cement.Config.url.contracts.addons.addons.deleteUrl
});