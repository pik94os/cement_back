Ext.define('Cement.view.contracts.addons.complex.Incoming', {
	extend: 'Cement.view.basic.Complex',
	alias: 'widget.contracts_addons_complex_incoming',
    border: 0,
    enableDetail: false,
    autoLoadStore: false,
    priceId: null,
    bodyPadding: 0,
    padding: 0,
    header: false,
    title: null,
    rowsCount: 3,
    title: 'Входящие',
    tabTitle: 'Входящие',
    cls: 'no-side-borders',
    topXType: 'contracts_addons_lists_incoming',
    topDetailType: 'Cement.view.contracts.contracts.view.Contract',
    bottomTitle: 'Связанные документы',
    bottomTabs: [{
        title: 'Приложения',
        xtype: 'contracts_addons_lists_addons_incoming',
        detailType: 'Cement.view.contracts.contracts.view.Contract',
        constantFilterParam: 'p_contract_addon_incoming',
        childrenConstantFilterParam: 'p_contract_addon_incoming_addon'
    }],
    subBottomTitle: 'Справочник',
    subBottomTabs: [{
        title: 'Участники',
        shownTitle: 'Участники',
        xtype: 'contracts_addons_lists_participants_incoming',
        detailType: 'Cement.view.contracts.contracts.view.Participant'
    }]
});