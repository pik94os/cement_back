Ext.define('Cement.view.contracts.addons.complex.Archive', {
	extend: 'Cement.view.basic.Complex',
	alias: 'widget.contracts_addons_complex_archive',
    border: 0,
    enableDetail: false,
    autoLoadStore: false,
    priceId: null,
    bodyPadding: 0,
    padding: 0,
    header: false,
    title: null,
    rowsCount: 3,
    title: 'Архив',
    tabTitle: 'Архив',
    cls: 'no-side-borders',
    topXType: 'contracts_addons_lists_archive',
    topDetailType: 'Cement.view.contracts.contracts.view.Contract',
    bottomTitle: 'Связанные документы',
    bottomTabs: [{
        title: 'Приложения',
        xtype: 'contracts_addons_lists_addons_archive',
        detailType: 'Cement.view.contracts.contracts.view.Contract',
        constantFilterParam: 'p_contract_addon_archive',
        childrenConstantFilterParam: 'p_contract_addon_archive_addon'
    }],
    subBottomTitle: 'Справочник',
    subBottomTabs: [{
        title: 'Участники',
        shownTitle: 'Участники',
        xtype: 'contracts_addons_lists_participants_archive',
        detailType: 'Cement.view.contracts.contracts.view.Participant'
    }]
});