Ext.define('Cement.view.contracts.addons.complex.New', {
	extend: 'Cement.view.basic.Complex',
	alias: 'widget.contracts_addons_complex_new',
    border: 0,
    enableDetail: false,
    autoLoadStore: false,
    priceId: null,
    bodyPadding: 0,
    padding: 0,
    header: false,
    title: null,
    rowsCount: 3,
    title: 'Новые',
    tabTitle: 'Новые',
    topXType: 'contracts_addons_lists_new',
    cls: 'no-side-borders',
    topDetailType: 'Cement.view.contracts.contracts.view.Contract',
    bottomTitle: 'Связанные документы',
    bottomTabs: [{
        title: 'Приложения',
        xtype: 'contracts_addons_lists_addons_new',
        detailType: 'Cement.view.contracts.contracts.view.Contract',
        constantFilterParam: 'p_contract_addon_new',
        childrenConstantFilterParam: 'p_contract_addon_new_addon'
    }],
    subBottomTitle: 'Справочник',
    subBottomTabs: [{
        title: 'Участники',
        shownTitle: 'Участники',
        xtype: 'contracts_addons_lists_participants_new',
        detailType: 'Cement.view.contracts.contracts.view.Participant'
    }]
});