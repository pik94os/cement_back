Ext.define('Cement.view.contracts.contracts.complex.Outcoming', {
	extend: 'Cement.view.basic.Complex',
	alias: 'widget.contracts_contracts_complex_outcoming',
    border: 0,
    enableDetail: false,
    autoLoadStore: false,
    priceId: null,
    bodyPadding: 0,
    padding: 0,
    header: false,
    title: null,
    rowsCount: 3,
    title: 'Исходящие',
    tabTitle: 'Исходящие',
    cls: 'no-side-borders',
    topXType: 'contracts_contracts_lists_outcoming',
    topDetailType: 'Cement.view.contracts.contracts.view.Contract',
    bottomTitle: 'Связанные документы',
    bottomTabs: [{
        title: 'Приложения',
        xtype: 'contracts_contracts_lists_addons_outcoming',
        detailType: 'Cement.view.contracts.contracts.view.Contract',
        constantFilterParam: 'p_contract_outcoming',
        childrenConstantFilterParam: 'p_contract_outcoming_addon'
    }],
    subBottomTitle: 'Справочник',
    subBottomTabs: [{
        title: 'Участники',
        shownTitle: 'Участники',
        xtype: 'contracts_contracts_lists_participants_outcoming',
        detailType: 'Cement.view.contracts.contracts.view.Participant'
    }]
});