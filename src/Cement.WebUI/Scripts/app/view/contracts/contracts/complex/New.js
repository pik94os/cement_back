Ext.define('Cement.view.contracts.contracts.complex.New', {
	extend: 'Cement.view.basic.Complex',
	alias: 'widget.contracts_contracts_complex_new',
    border: 0,
    enableDetail: false,
    autoLoadStore: false,
    priceId: null,
    bodyPadding: 0,
    padding: 0,
    header: false,
    title: null,
    rowsCount: 3,
    title: 'Новые',
    tabTitle: 'Новые',
    cls: 'no-side-borders',
    topXType: 'contracts_contracts_lists_new',
    topDetailType: 'Cement.view.contracts.contracts.view.Contract',
    bottomTitle: 'Связанные документы',
    bottomTabs: [{
        title: 'Приложения',
        xtype: 'contracts_contracts_lists_addons_new',
        detailType: 'Cement.view.contracts.contracts.view.Contract',
        constantFilterParam: 'p_contract_new',
        childrenConstantFilterParam: 'p_contract_new_addon',
        border: 0
    }],
    subBottomTitle: 'Справочник',
    subBottomTabs: [{
        title: 'Участники',
        shownTitle: 'Участники',
        xtype: 'contracts_contracts_lists_participants_new',
        detailType: 'Cement.view.contracts.contracts.view.Participant'
    }]
});