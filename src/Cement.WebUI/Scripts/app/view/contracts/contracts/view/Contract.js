Ext.define('Cement.view.contracts.contracts.view.Contract', {
	extend: 'Cement.view.basic.Form',
	alias: 'widget.contracts_contracts_view_contract',
	fieldsDisabled: true,
	buttons: null,
	border: 0,
	tbar: Cement.Config.modelViewButtons,
	layout: 'autocontainer',
	bbar: null,
	noUserFieldsetsControl: false,
	bodyCls: 'model-form',

	initComponent: function ()  {
		Ext.apply(this, {
			items: [{
				xtype: 'panel',
				border: 0,
				layout: 'anchor',
				defaultType: 'textfield',
				anchor: '100%',
				bodyPadding: '10 10 10 5',
				autoScroll: true,
				defaults: {
					anchor: '100%',
					labelWidth: 110
				},
				items: [{
					xtype: 'textfield',
					name: 'p_document_kind_display',
					fieldLabel: 'Вид документа',
					disabled: true
				}, {
					xtype: 'textfield',
					name: 'p_document_to_display',
					fieldLabel: 'К документу',
					disabled: true
				}, {
					xtype: 'textfield',
					name: 'p_number',
					fieldLabel: '№',
					disabled: true
				}, {
					xtype: 'textfield',
					name: 'p_date',
					fieldLabel: 'Дата',
					disabled: true
				}, {
					xtype: 'textfield',
					name: 'p_subject',
					fieldLabel: 'Предмет',
					disabled: true
				}, {
					xtype: 'textfield',
					name: 'p_count',
					fieldLabel: 'Количество',
					disabled: true
				}, {
					xtype: 'textfield',
					name: 'p_sum',
					fieldLabel: 'Сумма',
					disabled: true
				}, {
					xtype: 'textfield',
					name: 'p_unload_method',
					fieldLabel: 'Способ отгрузки',
					disabled: true
				}, {
					xtype: 'textfield',
					name: 'p_pay_method',
					fieldLabel: 'Способ оплаты',
					disabled: true
				}, {
					xtype: 'textfield',
					name: 'p_delay',
					fieldLabel: 'Отсрочка',
					disabled: true
				}, {
					xtype: 'textfield',
					name: 'p_limit',
					fieldLabel: 'Лимит',
					disabled: true
				}, {
					xtype: 'textfield',
					name: 'p_start',
					fieldLabel: 'Начало',
					disabled: true
				}, {
					xtype: 'textfield',
					name: 'p_end',
					fieldLabel: 'Окончание',
					disabled: true
				}, {
					xtype: 'textfield',
					name: 'p_add_date',
					fieldLabel: 'Дата добавления',
					disabled: true
				}, {
					xtype: 'textfield',
					name: 'p_project_display',
					fieldLabel: 'Проект',
					disabled: true
				}, {
					xtype: 'textfield',
					name: 'p_comment',
					fieldLabel: 'Примечание',
					disabled: true
				}]
			}]
		});
		this.callParent(arguments);
	}
});