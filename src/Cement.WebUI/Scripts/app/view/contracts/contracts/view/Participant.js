Ext.define('Cement.view.contracts.contracts.view.Participant', {
	extend: 'Cement.view.basic.Form',
	alias: 'widget.contracts_contracts_view_participant',
	fieldsDisabled: true,
	buttons: null,
	border: 0,
	tbar: Cement.Config.modelViewButtons,
	layout: 'autocontainer',
	bbar: null,
	noUserFieldsetsControl: false,
	bodyCls: 'model-form',

	loadRecord: function (record) {
		if (!record.get) return;
		var me = this;
		this.setLoading(true);
		Ext.Ajax.request({
			url: Ext.String.format(Cement.Config.url.contracts.contracts.participants.loadItem, record.get('id')),
			success: function (response) {
				var json = Ext.JSON.decode(response.responseText),
					client = Ext.create('Cement.model.Client'),
					employee = Ext.create('Cement.model.Employee');
				for (i in json.client) {
					if (json.client.hasOwnProperty(i)) {
						client.set(i, json.client[i]);
					}
				}
				for (i in json.employee) {
					if (json.employee.hasOwnProperty(i)) {
						employee.set(i, json.employee[i]);
					}
				}
				me.down('clientview').loadRecord(client);
				me.down('employeeview').loadRecord(employee);
				me.setLoading(false);
			}
		});
	},

	initComponent: function ()  {
		Ext.apply(this, {
			items: [{
				xtype: 'panel',
				border: 0,
				layout: 'accordion',
				defaultType: 'textfield',
				anchor: '100%',
				bodyPadding: '0',
				autoScroll: true,
				oveflowX: 'hidden',
				defaults: {
					anchor: '100%',
					labelWidth: 110
				},
				items: [{
					xtype: 'clientview',
					cls: 'truck',
					title: 'Клиент',
					layout: 'fit',
					tbar: null
				}, {
					xtype: 'employeeview',
					cls: 'trailer',
					title: 'Сотрудник',
					layout: 'fit',
					tbar: null
				}]
			}]
		});
		this.callParent(arguments);
		this.setupImgCentering();
	}
});