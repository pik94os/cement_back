Ext.define('Cement.view.contracts.contracts.lists.participants.Archive', {
	extend: 'Cement.view.contracts.contracts.lists.participants.New',
	alias: 'widget.contracts_contracts_lists_participants_archive',
    autoLoadStore: false,
	gridStore: 'contracts.participants.Archive',
	gridStateId: 'stateContractIncomingParticipantsArchive'
});