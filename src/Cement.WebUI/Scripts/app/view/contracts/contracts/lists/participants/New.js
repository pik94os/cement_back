Ext.define('Cement.view.contracts.contracts.lists.participants.New', {
	extend: 'Cement.view.basic.Grid',
	alias: 'widget.contracts_contracts_lists_participants_new',
    autoLoadStore: false,

    bbarText: 'Показаны участники {0} - {1} из {2}',
    bbarEmptyMsg: 'Нет участников',
    bbarUsersText: 'Участников на странице: ',

	gridStore: 'contracts.participants.New',
	gridStateId: 'stateContractsParticipantsNew',

	printUrl: Cement.Config.url.contracts.contracts.participants.printGrid,
	helpUrl: Cement.Config.url.contracts.contracts.participants.help,
    deleteUrl: Cement.Config.url.contracts.contracts.participants.deleteUrl,

    shownTitle: null,

    getGridFeatures: function () { return null },

    creatorTree: Ext.clone(Cement.Creators.contracts),
    createWindowTitle: 'Создать договор',

    getActionColumns: function () {
        return null;
    },

    getToolbarItems: function () {
        return null;
    },

    getGridColumns: function () {
        var result = [
            { text: 'Наименование', dataIndex: 'p_name', width: 200, locked: true },
            { text: 'Именуемое', dataIndex: 'p_call_name', width: 150 },
            { text: 'Адрес', dataIndex: 'p_address', width: 150 },
            { text: 'ИНН', dataIndex: 'p_inn', width: 150 },
            { text: 'КПП', dataIndex: 'p_kpp', width: 150 },
            { text: 'Подписант', dataIndex: 'p_signer', width: 150 },
            { text: 'Должность', dataIndex: 'p_signer_position', width: 150 },
            { text: 'Основание', dataIndex: 'p_signer_reason', width: 150 }
        ];
        return this.mergeActions(result);
    },

    initComponent: function () {
        Ext.apply(this, {
            title: this.shownTitle
        });
        this.callParent(arguments);
    }
});