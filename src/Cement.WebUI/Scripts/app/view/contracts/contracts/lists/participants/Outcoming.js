Ext.define('Cement.view.contracts.contracts.lists.participants.Outcoming', {
	extend: 'Cement.view.contracts.contracts.lists.participants.New',
	alias: 'widget.contracts_contracts_lists_participants_outcoming',
    autoLoadStore: false,
	gridStore: 'contracts.participants.Outcoming',
	gridStateId: 'stateContractIncomingParticipantsOutcoming'
});