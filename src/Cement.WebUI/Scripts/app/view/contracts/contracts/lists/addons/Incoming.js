Ext.define('Cement.view.contracts.contracts.lists.addons.Incoming', {
    extend: 'Cement.view.contracts.contracts.lists.addons.New',
    alias: 'widget.contracts_contracts_lists_addons_incoming',
    autoLoadStore: false,

    gridStore: 'contracts.addons.Incoming',
    gridStateId: 'stateContractsAddonsIncoming'
});