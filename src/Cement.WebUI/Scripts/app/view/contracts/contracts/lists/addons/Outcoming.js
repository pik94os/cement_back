Ext.define('Cement.view.contracts.contracts.lists.addons.Outcoming', {
    extend: 'Cement.view.contracts.contracts.lists.addons.New',
    alias: 'widget.contracts_contracts_lists_addons_outcoming',
    autoLoadStore: false,

    gridStore: 'contracts.addons.Outcoming',
    gridStateId: 'stateContractsAddonsOutcoming'
});