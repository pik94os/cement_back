Ext.define('Cement.view.contracts.contracts.lists.addons.Archive', {
    extend: 'Cement.view.contracts.contracts.lists.addons.New',
    alias: 'widget.contracts_contracts_lists_addons_archive',
    autoLoadStore: false,

    gridStore: 'contracts.addons.Archive',
    gridStateId: 'stateContractsAddonsArchive'
});