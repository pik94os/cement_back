Ext.define('Cement.view.contracts.contracts.lists.participants.Incoming', {
	extend: 'Cement.view.contracts.contracts.lists.participants.New',
	alias: 'widget.contracts_contracts_lists_participants_incoming',
    autoLoadStore: false,
	gridStore: 'contracts.participants.Incoming',
	gridStateId: 'stateContractIncomingParticipantsIncoming'
});