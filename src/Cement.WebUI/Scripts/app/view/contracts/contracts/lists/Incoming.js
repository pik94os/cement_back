Ext.define('Cement.view.contracts.contracts.lists.Incoming', {
	extend: 'Cement.view.contracts.contracts.lists.New',
	alias: 'widget.contracts_contracts_lists_incoming',
    autoLoadStore: false,

	gridStore: 'contracts.Incoming',
	gridStateId: 'stateContractsIncoming',

	printUrl: Cement.Config.url.contracts.contracts.incoming.printGrid,
	helpUrl: Cement.Config.url.contracts.contracts.incoming.help,
    archiveItemUrl: Cement.Config.url.contracts.contracts.incoming.send_to_archive,

  shownTitle: null,

  getActionColumns: function () {
    return {
      xtype: 'rowactions',
      hideable: false,
      resizeable: false,
      width: 46,
      locked: true,
      actions: [{
          iconCls: 'icon-archive-item',
          qtip: 'Отправить в архив',
          callback: this.archiveItem
      }],
      keepSelection: true
    };
  }
});