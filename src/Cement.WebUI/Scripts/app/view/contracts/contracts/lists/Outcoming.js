Ext.define('Cement.view.contracts.contracts.lists.Outcoming', {
	extend: 'Cement.view.contracts.contracts.lists.New',
	alias: 'widget.contracts_contracts_lists_outcoming',
    autoLoadStore: false,

	gridStore: 'contracts.Outcoming',
	gridStateId: 'stateContractsOutcoming',

	printUrl: Cement.Config.url.contracts.contracts.outcoming.printGrid,
	helpUrl: Cement.Config.url.contracts.contracts.outcoming.help,
    archiveItemUrl: Cement.Config.url.contracts.contracts.outcoming.send_to_archive,

    shownTitle: null,

    getActionColumns: function () {
        return {
            xtype: 'rowactions',
            hideable: false,
            resizeable: false,
            width: 46,
            locked: true,
            actions: [{
                iconCls: 'icon-archive-item',
                qtip: 'Отправить в архив',
                callback: this.archiveItem
            }],
            keepSelection: true
        };
    }
});