Ext.define('Cement.view.contracts.contracts.form.Addon', {
	extend: 'Cement.view.basic.Form',
	xtype: 'contracts_contracts_form_addon',
	fieldsDisabled: false,
	urlTemplate: Cement.Config.url.contracts.contracts.save.addons,

	price1StoreId: 'contracts.auxiliary.AllPrices',
	price2StoreId: 'contracts.auxiliary.AllPrices2',

	getNumberColumn: function () {
		return {
			xtype: 'textfield',
			fieldLabel: 'Номер',
			allowBlank: false,
			labelWidth: 100,
			width: 150,
			disabled: this.fieldsDisabled,
			name: 'p_number'
		};
	},

	getDateColumn: function () {
		return {
			xtype: this.getDateFieldXType(),
			fieldLabel: 'Дата',
			allowBlank: false,
			labelWidth: 100,
			width: 150,
			disabled: this.fieldsDisabled,
			name: 'p_date'
		};
	},

	getItems: function () {
		var client_columns = this.getTwoColumns('', [{
				name: 'p_client_inn',
				fieldLabel: 'ИНН',
				disabled: true
			}], [{
				name: 'p_client_kpp',
				fieldLabel: 'КПП',
				disabled: true
			}], true, 'right');
		client_columns.padding = '0';
		client_columns.margin = '0';
		return [{
		      xtype: 'panel',
		      border: 0,
		      layout: {
		        type: 'column',
		        align: 'stretch',
		        padding: 0
		      },
		      items: [{
		      	xtype: 'panel',
		      	border: 0,
		      	columnWidth: 0.25,
		      	html: '&nbsp;'
		      }, {
		      	xtype: 'panel',
		        padding: '0 10 0 0',
		        layout: 'anchor',
		        columnWidth: 0.25,
		        border: 0,
		        defaults: {
		          xtype: 'textfield',
		          labelAlign: 'right',
		          anchor: '100%'
		        },
		      	items: [
			      	this.getNumberColumn()
		      	]
		      }, {
		      	xtype: 'panel',
		        padding: '0 0 0 10',
		        layout: 'anchor',
		        columnWidth: 0.25,
		        border: 0,
		        defaults: {
		        	labelAlign: 'right',
		          xtype: 'textfield',
		          anchor: '100%'
		        },
		      	items: [
			      	this.getDateColumn()
		      	]
		      }, {
		      	xtype: 'panel',
		      	border: 0,
		      	columnWidth: 0.25
		      }]
		    }, {
				xtype: 'fieldset',
				title: 'Общие данные',
				collapsible: true,
				collapsed: false,
				bodyPadding: 0,
				defaults: {
					labelAlign: 'right',
					xtype: 'textfield',
					anchor: '100%'
				},
				items: [{
					xtype: 'fieldcontainer',
					layout: 'hbox',
					anchor: '100%',
					align: 'stretch',
					items:[{
						xtype: 'hidden',
						name: 'p_client'
					}, {
						xtype: 'textfield',
						name: 'p_client_display',
						fieldLabel: 'Клиент',
						labelAlign: 'right',
						flex: 1
					}, {
						xtype: 'button',
						text: 'Выбрать',
						margin: '0 0 0 4',
						action: 'show_clients_window',
						width: 60
		            }]
		        }, {
					xtype: 'fieldcontainer',
					layout: 'hbox',
					anchor: '100%',
					align: 'stretch',
					items:[{
						xtype: 'hidden',
						name: 'p_contract_to'
					}, {
						xtype: 'textfield',
						name: 'p_contract_to_display',
						fieldLabel: 'К договору',
						labelAlign: 'right',
						flex: 1
					}, {
						xtype: 'button',
						text: 'Выбрать',
						margin: '0 0 0 4',
						action: 'show_contracts_window',
						width: 60
		            }]
		        },
				client_columns, {
					name: 'p_client_address',
					fieldLabel: 'Адрес',
					disabled: true
				}]
			}, {
				xtype: 'fieldset',
				title: 'Приложение 3',
				collapsible: true,
				collapsed: false,
				bodyPadding: 0,
				defaults: {
					labelAlign: 'right',
					xtype: 'textfield',
					anchor: '100%'
				},
				items: [{
					xtype: 'fieldcontainer',
					layout: 'hbox',
					anchor: '100%',
					align: 'stretch',
					items:[{
						xtype: 'hidden',
						name: 'p_price1'
					}, {
						xtype: 'textfield',
						name: 'p_price1_display',
						fieldLabel: 'Прайс-лист',
						labelAlign: 'right',
						flex: 1
					}, {
						xtype: 'button',
						text: 'Выбрать',
						margin: '0 0 0 4',
						action: 'show_addon1_price_window',
						width: 60
		            }]
				}, {
					xtype: 'fieldcontainer',
					layout: 'hbox',
					anchor: '100%',
					align: 'stretch',
					items:[{
						xtype: 'hidden',
						name: 'p_price2'
					}, {
						xtype: 'textfield',
						name: 'p_price2_display',
						fieldLabel: 'Прайс-лист',
						labelAlign: 'right',
						flex: 1
					}, {
						xtype: 'button',
						text: 'Выбрать',
						margin: '0 0 0 4',
						action: 'show_addon2_price_window',
						width: 60
		            }]
				}]
			}
		];
	},

	populateStores: function () {
		Ext.getStore(this.price1StoreId).each(function (item) {
			Ext.getStore(this.price2StoreId).add(item.copy());
		}, this);
	},

	initComponent: function () {
	    var me = this;
	    me.url = Ext.String.format(me.urlTemplate, me.formKind);

		this.callParent(arguments);

		Ext.getStore(this.price1StoreId).load({
			scope: this,
			callback: this.populateStores
		});

		this.down('button[action=show_contracts_window]').on('click', function () {
			this.createContractWindow();
			this.contract_win.show();
		}, this);

		this.down('button[action=show_clients_window]').on('click', function () {
			this.createClientWindow();
			this.client_win.show();
		}, this);

		this.down('button[action=show_addon1_price_window]').on('click', function () {
			this.createPrice1Window();
			this.price1_window.show();
		}, this);

		this.down('button[action=show_addon2_price_window]').on('click', function () {
			this.createPrice2Window();
			this.price2_window.show();
		}, this);
	},

	createContractWindow: function () {
		var me = this;
		if (!this.contract_win) {
			this.contract_win = Ext.create('Cement.view.contracts.contracts.form.ContractWindow');
			this.contract_win.on('signitem', function (rec) {
				me.down('textfield[name=p_contract_to_display]').setValue(rec.get('name'));
				me.down('hidden[name=p_contract_to]').setValue(rec.get('id'));
				me.contract_win.hide();
			}, this);
		}
	},

	createPrice2Window: function () {
		var me = this;
		if (!this.price2_window) {
			this.price2_window = Ext.create('Cement.view.contracts.contracts.form.PricelistWindow', {
				mainStoreId: this.price2StoreId
			});
			this.price2_window.on('priceselected', function (rec) {
				this.down('hidden[name=p_price2]').setValue(rec.get('id'));
				this.down('textfield[name=p_price2_display]').setValue(rec.get('p_name'));
				me.price2_window.hide();
			}, this);
		}
	},

	createPrice1Window: function () {
		var me = this;
		if (!this.price1_window) {
			this.price1_window = Ext.create('Cement.view.contracts.contracts.form.PricelistWindow', {
				mainStoreId: this.price1StoreId
			});
			this.price1_window.on('priceselected', function (rec) {
				this.down('hidden[name=p_price1]').setValue(rec.get('id'));
				this.down('textfield[name=p_price1_display]').setValue(rec.get('p_name'));
				me.price1_window.hide();
			}, this);
		}
	},

	createClientWindow: function () {
		var me = this;
		if (!this.client_win) {
			this.client_win = Ext.create('Cement.view.clients.ClientWindow');
			this.client_win.on('signitem', function (rec) {
				me.down('textfield[name=p_client_display]').setValue(rec.get('p_name'));
				me.down('hidden[name=p_client]').setValue(rec.get('id'));
				me.down('textfield[name=p_client_inn]').setValue(rec.get('p_inn'));
				me.down('textfield[name=p_client_kpp]').setValue(rec.get('p_kpp'));
				me.down('textfield[name=p_client_address]').setValue(rec.get('p_address'));
				me.client_win.hide();
			}, this);
		}
	},

	afterRecordLoad: function (rec) {
		Ext.getStore(this.price1StoreId).load({
			scope: this,
			callback: function () {
				this.populateStores();
				this.createClientWindow();
				this.createPrice1Window();
				this.createPrice2Window();
				this.createContractWindow();
				
				this.price1_window.loadData([rec.get('p_price1')]);
				this.price2_window.loadData([rec.get('p_price2')]);
				this.contract_win.loadData([rec.get('p_contract_to')]);
				this.client_win.loadData([rec.get('p_client')]);
				this.down('hidden[name=p_client]').setValue(Ext.JSON.encode(rec.get('p_client')));
			}
		});
	}
});