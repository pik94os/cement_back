Ext.define('Cement.view.contracts.contracts.form.Spec', {
	extend: 'Cement.view.basic.Form',
	xtype: 'contracts_contracts_form_spec',
	fieldsDisabled: false,
	urlTemplate: Cement.Config.url.contracts.contracts.save.specs,
	priceStoreId: 'contracts.auxiliary.AllPrices',

	getNumberColumn: function () {
		return {
			xtype: 'textfield',
			fieldLabel: 'Номер',
			allowBlank: false,
			labelWidth: 100,
			width: 150,
			disabled: this.fieldsDisabled,
			name: 'p_number'
		};
	},

	getDateColumn: function () {
		return {
			xtype: this.getDateFieldXType(),
			fieldLabel: 'Дата',
			allowBlank: false,
			labelWidth: 100,
			width: 150,
			disabled: this.fieldsDisabled,
			name: 'p_date'
		};
	},

	getItems: function () {
		var client_columns = this.getTwoColumns('', [{
				name: 'p_client_inn',
				fieldLabel: 'ИНН',
				disabled: true
			}], [{
				name: 'p_client_kpp',
				fieldLabel: 'КПП',
				disabled: true
			}], true, 'right'),
			agreement_columns = this.getTwoColumns('', [{
				name: 'p_agreement_delay',
				fieldLabel: 'Отсрочка',
				allowBlank: false,
				fieldsDisabled: this.fieldsDisabled
			}], [{
				name: 'p_agreement_char',
				fieldLabel: 'Хар-ка дней',
				allowBlank: false,
				fieldsDisabled: this.fieldsDisabled
			}], true, 'right');
		agreement_columns.padding = '0';
		agreement_columns.margin = '0';
		client_columns.padding = '0';
		client_columns.margin = '0';
		return [{
		      xtype: 'panel',
		      border: 0,
		      layout: {
		        type: 'column',
		        align: 'stretch',
		        padding: 0
		      },
		      items: [{
		      	xtype: 'panel',
		      	border: 0,
		      	columnWidth: 0.25,
		      	html: '&nbsp;'
		      }, {
		      	xtype: 'panel',
		        padding: '0 10 0 0',
		        layout: 'anchor',
		        columnWidth: 0.25,
		        border: 0,
		        defaults: {
		          xtype: 'textfield',
		          labelAlign: 'right',
		          anchor: '100%'
		        },
		      	items: [
			      	this.getNumberColumn()
		      	]
		      }, {
		      	xtype: 'panel',
		        padding: '0 0 0 10',
		        layout: 'anchor',
		        columnWidth: 0.25,
		        border: 0,
		        defaults: {
		        	labelAlign: 'right',
		          xtype: 'textfield',
		          anchor: '100%'
		        },
		      	items: [
			      	this.getDateColumn()
		      	]
		      }, {
		      	xtype: 'panel',
		      	border: 0,
		      	columnWidth: 0.25
		      }]
		    }, {
				xtype: 'fieldset',
				title: 'Общие данные',
                name: 'common',
				collapsible: true,
				collapsed: false,
				bodyPadding: 0,
				defaults: {
					labelAlign: 'right',
					xtype: 'textfield',
					anchor: '100%'
				},
				items: [{
					xtype: 'fieldcontainer',
					layout: 'hbox',
					anchor: '100%',
					align: 'stretch',
					items:[{
						xtype: 'hidden',
						name: 'p_client'
					}, {
						xtype: 'textfield',
						name: 'p_client_display',
						fieldLabel: 'Клиент',
						labelAlign: 'right',
						flex: 1
					}, {
						xtype: 'button',
						text: 'Выбрать',
						margin: '0 0 0 4',
						action: 'show_clients_window',
						width: 60
		            }]
		        }, {
					xtype: 'fieldcontainer',
					layout: 'hbox',
					anchor: '100%',
					align: 'stretch',
					items:[{
						xtype: 'hidden',
						name: 'p_contract_to'
					}, {
						xtype: 'textfield',
						name: 'p_contract_to_display',
						fieldLabel: 'К договору',
						labelAlign: 'right',
						flex: 1
					}, {
						xtype: 'button',
						text: 'Выбрать',
						margin: '0 0 0 4',
						action: 'show_contracts_window',
						width: 60
		            }]
		        }, client_columns, {
					name: 'p_client_address',
					fieldLabel: 'Адрес',
					disabled: true
				}]
			}, {
				xtype: 'fieldset',
				title: 'Спецификация',
				collapsible: true,
				collapsed: false,
				bodyPadding: 0,
				defaults: {
					labelAlign: 'right',
					xtype: 'textfield',
					anchor: '100%'
				},
				items: [{
					xtype: 'fieldcontainer',
					layout: 'hbox',
					anchor: '100%',
					align: 'stretch',
					items:[{
						xtype: 'hidden',
						name: 'p_price'
					}, {
						xtype: 'textfield',
						name: 'p_price_display',
						fieldLabel: 'Прайс-лист',
						labelAlign: 'right',
						flex: 1
					}, {
						xtype: 'button',
						text: 'Выбрать',
						margin: '0 0 0 4',
						action: 'show_price_window',
						width: 60
		            }]
				}]
			}
		];
	},

	initComponent: function () {
	    var me = this;
        me.url = Ext.String.format(me.urlTemplate, me.formKind);
        
		this.callParent(arguments);
		Ext.getStore(this.priceStoreId).load();

		this.down('button[action=show_contracts_window]').on('click', function () {
			this.createContractWindow();
			this.contract_win.show();
		}, this);

		this.down('button[action=show_clients_window]').on('click', function () {
			this.createClientWindow();
			this.client_win.show();
		}, this);

		this.down('button[action=show_price_window]').on('click', function () {
			this.createPrice1Window();
			this.price1_window.show();
		}, this);
	},


	createContractWindow: function () {
		var me = this;
		if (!this.contract_win) {
			this.contract_win = Ext.create('Cement.view.contracts.contracts.form.ContractWindow');
			this.contract_win.on('signitem', function (rec) {
				me.down('textfield[name=p_contract_to_display]').setValue(rec.get('name'));
				me.down('hidden[name=p_contract_to]').setValue(rec.get('id'));
				me.contract_win.hide();
			}, this);
		}
	},

	createPrice1Window: function () {
		var me = this;
		if (!this.price1_window) {
			this.price1_window = Ext.create('Cement.view.contracts.contracts.form.PricelistWindow', {
				mainStoreId: this.priceStoreId
			});
			this.price1_window.on('priceselected', function (data) {
				this.down('hidden[name=p_price]').setValue(data.get('id'));
				this.down('textfield[name=p_price_display]').setValue(data.get('p_name'));
				me.price1_window.hide();
			}, this);
		}
	},

	createClientWindow: function () {
		var me = this;
		if (!this.client_win) {
			this.client_win = Ext.create('Cement.view.clients.OrganizationClientWindow');
			this.client_win.on('signitem', function (rec) {
				me.down('textfield[name=p_client_display]').setValue(rec.get('p_name'));
				me.down('hidden[name=p_client]').setValue(rec.get('id'));
				me.down('textfield[name=p_client_inn]').setValue(rec.get('p_inn'));
				me.down('textfield[name=p_client_kpp]').setValue(rec.get('p_kpp'));
				me.down('textfield[name=p_client_address]').setValue(rec.get('p_address'));
				me.client_win.hide();
			}, this);
		}
	},

	afterRecordLoad: function (rec) {
	    if (rec.get('id') > 0) {
	        this.manageFields(true);
	    }

	    Ext.getStore(this.priceStoreId).load({
			scope: this,
			callback: function () {
				this.createClientWindow();
				this.createPrice1Window();
				this.createContractWindow();
				
				this.price1_window.loadData([rec.get('p_price')]);
				this.contract_win.loadData([rec.get('p_contract_to')]);
				this.client_win.loadData([rec.get('p_client')]);
			}
		});
	},

    onCreate: function() {
        this.manageFields(false);
    },

    manageFields: function(disableFields) {
        var fieldset = this.down('fieldset[name=common]');

        disableFields ? fieldset.disable() : fieldset.enable();
    }
});