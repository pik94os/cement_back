Ext.define('Cement.view.contracts.contracts.form.PricelistWindow', {
	extend: 'Cement.view.products.price.PricelistsWindow',
	xtype: 'contracts_contracts_form_pricelist_window',

	mainStoreId: 'contracts.auxiliary.AllPrices'
});