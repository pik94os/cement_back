Ext.define('Cement.view.contracts.contracts.form.PaymentRulesWindow', {
	extend: 'Ext.window.Window',
	xtype: 'contracts_contracts_form_payment_rules_window',
	title: 'Условия платежа',
	width: 690,
	height: 230,
	layout: 'fit',
	resizable: false,
	modal: true,

	items: [{
		xtype: 'form',
		border: 0,
		layout: 'absolute',
		items: [{
			xtype: 'panel',
			x: 10,
			y: 10,
			width: 221,
			height: 150,
			bodyPadding: 10,
			items: [{
				xtype: 'fieldcontainer',
				defaultType: 'radiofield',
				layout: 'vbox',
				items: [{
					boxLabel: 'Количество дней',
					inputValue: Cement.Config.contract.payment_rules_kinds.DAYS_COUNT,
					name: 'p_pay_kind'
				}, {
					boxLabel: 'День следующего месяца',
					inputValue: Cement.Config.contract.payment_rules_kinds.DAY_NEXT_MONTH,
					name: 'p_pay_kind'
				}, {
					boxLabel: 'Число',
					inputValue: Cement.Config.contract.payment_rules_kinds.DAY_NUMBER,
					name: 'p_pay_kind'
				}, {
					boxLabel: 'Акредитив',
					inputValue: Cement.Config.contract.payment_rules_kinds.ACCREDITIVE,
					name: 'p_pay_kind'
				}, {
					boxLabel: 'Предоплата',
					inputValue: Cement.Config.contract.payment_rules_kinds.PREPAY,
					name: 'p_pay_kind'
				}]
			}]
		}, {
			xtype: 'panel',
			width: 426,
			height: 150,
			x: 241,
			y: 10,
			bodyPadding: '35 0 0 10',
			items: [{
				xtype: 'fieldcontainer',
				defaultType: 'radiofield',
				layout: 'vbox',
				hidden: true,
				role: 'repeat_details',
				elem_uid: 'repeat_details_' + Cement.Config.contract.payment_rules_kinds.DAYS_COUNT,
				items: [{
					xtype: 'fieldcontainer',
					layout: 'hbox',
					items: [{
						xtype: 'radiofield',
						name: 'p_pay_days_kind',
						inputValue: Cement.Config.contract.repeat_days_kinds.WORK,
						boxLabel: 'Рабочих'
					}, {
						xtype: 'textfield',
						name: 'p_pay_days_work',
						margin: '0 10 0 27'
					}, {
						xtype: 'label',
						padding: '1 0 4 0',
						text: 'дней'
					}]
				}, {
					xtype: 'fieldcontainer',
					layout: 'hbox',
					items: [{
						xtype: 'radiofield',
						name: 'p_pay_days_kind',
						inputValue: Cement.Config.contract.repeat_days_kinds.BANK,
						boxLabel: 'Банковских'
					}, {
						xtype: 'textfield',
						name: 'p_pay_days_bank',
						margin: '0 10'
					}, {
						xtype: 'label',
						padding: '1 0 4 0',
						text: 'дней'
					}]
				}]
			}, {
				xtype: 'panel',
				hidden: true,
				layout: 'anchor',
				role: 'repeat_details',
				elem_uid: 'repeat_details_' + Cement.Config.contract.payment_rules_kinds.DAY_NEXT_MONTH,
				border: 0,
				items: [{
					xtype: 'fieldcontainer',
					layout: 'hbox',
					items: [{
						xtype: 'radiofield',
						name: 'p_pay_next_day_kind',
						inputValue: Cement.Config.office_work.repeat_week_values.EVERY_WEEK,
						boxLabel: 'Число'
					}, {
						xtype: 'textfield',
						name: 'p_pay_next_day_value',
						margin: '0 10'
					}]
				}]
			}, {
				xtype: 'fieldcontainer',
				defaultType: 'radiofield',
				layout: 'vbox',
				hidden: true,
				role: 'repeat_details',
				elem_uid: 'repeat_details_' + Cement.Config.contract.payment_rules_kinds.DAY_NUMBER,
				items: [{
					xtype: 'fieldcontainer',
					layout: 'hbox',
					items: [{
						xtype: 'radiofield',
						name: 'p_pay_day_number_kind',
						inputValue: Cement.Config.contract.pay_day_number_kinds.DAY,
						boxLabel: 'Число'
					}, {
						xtype: 'textfield',
						width: 50,
						name: 'p_pay_day_number_day',
						margin: '0 10'
					}]
				}, {
					xtype: 'fieldcontainer',
					layout: 'hbox',
					items: [{
						xtype: 'radiofield',
						name: 'p_pay_day_number_kind',
						inputValue: Cement.Config.contract.pay_day_number_kinds.FROM_TO,
						boxLabel: 'От'
					}, {
						xtype: 'datefield',
						width: 80,
						name: 'p_pay_day_number_from',
						margin: '0 10 0 30'
					}, {
						xtype: 'label',
						margin: '4 5 0 5',
						text: 'До'
					}, {
						xtype: 'datefield',
						width: 80,
						name: 'p_pay_day_number_to',
						margin: '0 10'
					}]
				}]
			}, {
				xtype: 'fieldcontainer',
				defaultType: 'radiofield',
				layout: 'vbox',
				hidden: true,
				role: 'repeat_details',
				elem_uid: 'repeat_details_' + Cement.Config.contract.payment_rules_kinds.ACCREDITIVE
			}, {
				xtype: 'fieldcontainer',
				defaultType: 'radiofield',
				layout: 'vbox',
				hidden: true,
				role: 'repeat_details',
				elem_uid: 'repeat_details_' + Cement.Config.contract.payment_rules_kinds.PREPAY
			}]
		}]
	}],

	bbar: [{
		text: 'Сохранить',
		action: 'save'
	}, {
		text: 'Отмена',
		action: 'cancel'
	}],

	getStr: function() {
		var values = this.down('form').getForm().getValues(),
			str = '',
			repeat_str = '';
		if (values['p_pay_kind']) {
			if (values['p_pay_kind'] == Cement.Config.contract.payment_rules_kinds.ACCREDITIVE) {
				str = 'Акредитив';
			}
			if (values['p_pay_kind'] == Cement.Config.contract.payment_rules_kinds.PREPAY) {
				str = 'Предоплата';
			}
			if (values['p_pay_kind'] == Cement.Config.contract.payment_rules_kinds.DAY_NUMBER) {
				str = 'Предоплата';
				if (values['p_pay_day_number_kind'] == Cement.Config.contract.pay_day_number_kinds.DAY) {
					str = Ext.String.format('{0} числа', values['p_pay_day_number_day']);
				}
				else {
					str = Ext.String.format('С {0} по {1}', values['p_pay_day_number_from'], values['p_pay_day_number_to']);
				}
			}
			if (values['p_pay_kind'] == Cement.Config.contract.payment_rules_kinds.DAY_NEXT_MONTH) {
				str = Ext.String.format('{0} число следующего месяца', values['p_pay_next_day_value']);
			}
			if (values['p_pay_kind'] == Cement.Config.contract.payment_rules_kinds.DAYS_COUNT) {
				if (values['p_pay_days_kind'] == Cement.Config.contract.repeat_days_kinds.WORK) {
					str = Ext.String.format('{0} рабочих дней', values['p_pay_days_work']);
				}
				else {
					str = Ext.String.format('{0} банковских дней', values['p_pay_days_bank']);
				}
			}
		}
		return str;
	},

	getJSON: function() {
		return Ext.JSON.encode(this.down('form').getForm().getValues());
	},

	initComponent: function () {
		this.callParent(arguments);
		this.addEvents('paymentrulescreated');
		Ext.each(this.query('radiofield[name=p_pay_kind]'), function (item) {
			item.on('change', function () {
				var values = this.down('form').getForm().getValues();
				if (values['p_pay_kind']) {
					if (!values['p_pay_kind'].length) {
						Ext.each(this.query('*[role=repeat_details]'), function (container) {
							container.hide();
						});
						this.down('*[elem_uid=repeat_details_' + values['p_pay_kind'] + ']').show();
					}
				}
			}, this);
		}, this);
		this.down('button[action=save]').on('click', function () {
			this.fireEvent('paymentrulescreated', this.getJSON(), this.getStr());
		}, this);
		this.down('button[action=cancel]').on('click', function () {
			this.hide();
		}, this);
		this.on('beforeclose', function () {
			this.hide();
			return false;
		});
	},

	closeWindow: function () {
		this.hide();
	},

	loadData: function (data) {
		if (data) {
			this.down('form').getForm().setValues(data);
		}
	}
});