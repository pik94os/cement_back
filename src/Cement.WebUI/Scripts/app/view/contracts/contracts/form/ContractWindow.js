Ext.define('Cement.view.contracts.contracts.form.ContractWindow', {
	extend: 'Cement.view.common.SignWindow',
	xtype: 'contracts_contracts_contract_window',
	leftFrameTitle: 'Договор',
	topGridTitle: 'Выбрать договор',
	bottomGridTitle: 'Выбор договоров',
	structure_storeId: 'contracts.auxiliary.AllContracts',
	hideCommentPanel: true,
	singleSelection: true,
	storeFields: ['id', 'name'],

	bbar: [{
		text: 'Сохранить',
		action: 'confirm'
	}, {
		text: 'Отмена',
		action: 'cancel'
	}],

	sign: function (send) {
		var store = this.down('*[role=sign-dst-grid]').getStore();
		if (store.getCount() > 0) {
			this.fireEvent('signitem', store.getAt(0));
		}
	},

	addToSrcStore: function (item) {
		if (!this.dstStore.getById(item.get('id'))) {
			this.srcStore.add({
				id: item.get('id'),
				name: item.get('text')
			});
		}
	},

	getTopColumnsConfig: function () {
		return [
			{
				xtype: 'rowactions',
				hideable: false,
				resizeable: false,
				width: 18,
				keepSelection: true,
				actions: [{ 
	                iconCls: 'icon-add-people', 
	                qtip: 'Добавить',
	                callback: this.addEmployee
	            }]
			},
			{ text: 'Документ', dataIndex: 'name', flex: 1 }
		];
	},

	getBottomColumnsConfig: function () {
		return [
			{
				xtype: 'rowactions',
				hideable: false,
				resizeable: false,
				width: 18,
				actions: [{ 
	                iconCls: 'icon-remove-people', 
	                qtip: 'Удалить', 
	                callback: this.removeEmployee
	            }]
			},
			{ text: 'Документ', dataIndex: 'name', flex: 1 }
		];
	},

	loadData: function (data) {
		if (data) {
			var fstore = Ext.getStore(this.structure_storeId);
			fstore.load({
				scope: this,
				callback: function () {
					Ext.each(data, function (item) {
						var r = fstore.getById(item);
						if (!this.dstStore) {
							this.setupEvents();
						}
						if (r) {
							this.dstStore.add({
								id: r.get('id'),
								name: r.get('text')
							});
						}
					}, this);
				}
			});
		}
	},

	closeWindow: function () {
		this.hide();
	},

	initComponent: function () {
		this.callParent(arguments);
		this.on('beforeclose', function () {
			this.hide();
			return false;
		});
	}
});