Ext.define('Cement.view.contracts.contracts.form.Agreement', {
	extend: 'Cement.view.basic.Form',
	xtype: 'contracts_contracts_form_agreement',
	fieldsDisabled: false,
	urlTemplate: Cement.Config.url.contracts.contracts.save.agreements,


	getNumberColumn: function () {
		return {
			xtype: 'textfield',
			fieldLabel: 'Номер',
			allowBlank: false,
			labelWidth: 100,
			width: 150,
			disabled: this.fieldsDisabled,
			name: 'p_number'
		};
	},

	getDateColumn: function () {
		return {
			xtype: this.getDateFieldXType(),
			fieldLabel: 'Дата',
			allowBlank: false,
			labelWidth: 100,
			width: 150,
			disabled: this.fieldsDisabled,
			name: 'p_date'
		};
	},

	getItems: function () {
		var client_columns = this.getTwoColumns('', [{
				name: 'p_client_inn',
				fieldLabel: 'ИНН',
				disabled: true
			}], [{
				name: 'p_client_kpp',
				fieldLabel: 'КПП',
				disabled: true
			}], true, 'right'),
			agreement_columns = this.getTwoColumns('', [{
				name: 'p_agreement_delay',
				fieldLabel: 'Отсрочка',
				allowBlank: false,
				fieldsDisabled: this.fieldsDisabled
			}], [{
				name: 'p_agreement_char',
				fieldLabel: 'Хар-ка дней',
				allowBlank: false,
				fieldsDisabled: this.fieldsDisabled
			}], true, 'right');
		agreement_columns.padding = '0';
		agreement_columns.margin = '0';
		client_columns.padding = '0';
		client_columns.margin = '0';
		return [{
		      xtype: 'panel',
		      border: 0,
		      layout: {
		        type: 'column',
		        align: 'stretch',
		        padding: 0
		      },
		      items: [{
		      	xtype: 'panel',
		      	border: 0,
		      	columnWidth: 0.25,
		      	html: '&nbsp;'
		      }, {
		      	xtype: 'panel',
		        padding: '0 10 0 0',
		        layout: 'anchor',
		        columnWidth: 0.25,
		        border: 0,
		        defaults: {
		          xtype: 'textfield',
		          labelAlign: 'right',
		          anchor: '100%'
		        },
		      	items: [
			      	this.getNumberColumn()
		      	]
		      }, {
		      	xtype: 'panel',
		        padding: '0 0 0 10',
		        layout: 'anchor',
		        columnWidth: 0.25,
		        border: 0,
		        defaults: {
		        	labelAlign: 'right',
		          xtype: 'textfield',
		          anchor: '100%'
		        },
		      	items: [
			      	this.getDateColumn()
		      	]
		      }, {
		      	xtype: 'panel',
		      	border: 0,
		      	columnWidth: 0.25
		      }]
		    }, {
				xtype: 'fieldset',
				title: 'Общие данные',
				collapsible: true,
				collapsed: false,
				bodyPadding: 0,
				defaults: {
					labelAlign: 'right',
					xtype: 'textfield',
					anchor: '100%'
				},
				items: [{
					xtype: 'fieldcontainer',
					layout: 'hbox',
					anchor: '100%',
					align: 'stretch',
					items:[{
						xtype: 'hidden',
						name: 'p_client'
					}, {
						xtype: 'textfield',
						name: 'p_client_display',
						fieldLabel: 'Клиент',
						labelAlign: 'right',
						flex: 1
					}, {
						xtype: 'button',
						text: 'Выбрать',
						margin: '0 0 0 4',
						action: 'show_clients_window',
						width: 60
		            }]
		        }, {
					xtype: 'fieldcontainer',
					layout: 'hbox',
					anchor: '100%',
					align: 'stretch',
					items:[{
						xtype: 'hidden',
						name: 'p_contract_to'
					}, {
						xtype: 'textfield',
						name: 'p_contract_to_display',
						fieldLabel: 'К договору',
						labelAlign: 'right',
						flex: 1
					}, {
						xtype: 'button',
						text: 'Выбрать',
						margin: '0 0 0 4',
						action: 'show_contracts_window',
						width: 60
		            }]
		        },
					client_columns, {
						name: 'p_client_address',
						fieldLabel: 'Адрес',
						disabled: true
					}
				]
			}, {
				xtype: 'fieldset',
				title: 'Дополнительное соглашение',
				collapsible: true,
				collapsed: false,
				bodyPadding: 0,
				defaults: {
					labelAlign: 'right',
					xtype: 'textfield',
					anchor: '100%'
				},
				items: [{
					xtype: 'fieldcontainer',
					layout: 'hbox',
					anchor: '100%',
					align: 'stretch',
					items:[{
						xtype: 'hidden',
						name: 'p_payment_rules'
					}, {
						xtype: 'textfield',
						name: 'p_payment_rules_display',
						fieldLabel: 'Условия платежа',
						labelAlign: 'right',
						flex: 1
					}, {
						xtype: 'button',
						text: 'Выбрать',
						margin: '0 0 0 4',
						action: 'show_payment_rules_window',
						width: 60
		            }]
				}]
			}
		];
	},

	initComponent: function () {
	    var me = this;
	    me.url = Ext.String.format(me.urlTemplate, me.formKind);
		this.callParent(arguments);

		this.down('button[action=show_clients_window]').on('click', function () {
			this.createClientWindow();
			this.client_win.show();
		}, this);

		this.down('button[action=show_payment_rules_window]').on('click', function () {
			this.createPaymentRulesWindow();
			this.payment_rules_win.show();
		}, this);

		this.down('button[action=show_contracts_window]').on('click', function () {
			this.createContractWindow();
			this.contract_win.show();
		}, this);
	},

	createContractWindow: function () {
		var me = this;
		if (!this.contract_win) {
			this.contract_win = Ext.create('Cement.view.contracts.contracts.form.ContractWindow');
			this.contract_win.on('signitem', function (rec) {
				me.down('textfield[name=p_contract_to_display]').setValue(rec.get('name'));
				me.down('hidden[name=p_contract_to]').setValue(rec.get('id'));
				me.contract_win.hide();
			}, this);
		}
	},

	createPaymentRulesWindow: function () {
		var me = this;
		if (!this.payment_rules_win) {
			this.payment_rules_win = Ext.create('Cement.view.contracts.contracts.form.PaymentRulesWindow');
			this.payment_rules_win.on('paymentrulescreated', function (json, str) {
				me.down('textfield[name=p_payment_rules_display]').setValue(str);
				me.down('hidden[name=p_payment_rules]').setValue(json);
				me.payment_rules_win.hide();
			}, this);
		}
	},

	createClientWindow: function () {
		var me = this;
		if (!this.client_win) {
			this.client_win = Ext.create('Cement.view.clients.ClientWindow');
			this.client_win.on('signitem', function (rec) {
				me.down('textfield[name=p_client_display]').setValue(rec.get('p_name'));
				me.down('hidden[name=p_client]').setValue(rec.get('id'));
				me.down('textfield[name=p_client_inn]').setValue(rec.get('p_inn'));
				me.down('textfield[name=p_client_kpp]').setValue(rec.get('p_kpp'));
				me.down('textfield[name=p_client_address]').setValue(rec.get('p_address'));
				me.client_win.hide();
			}, this);
		}
	},

	afterRecordLoad: function (rec) {
		this.createClientWindow();
		this.createPaymentRulesWindow();
		this.createContractWindow();
		
		this.client_win.loadData([rec.get('p_client')]);
		this.contract_win.loadData([rec.get('p_contract_to')]);
		this.payment_rules_win.loadData(rec.get('p_payment_rules'));

		this.down('hidden[name=p_payment_rules]').setValue(Ext.JSON.encode(rec.get('p_payment_rules')));
	}
});