Ext.define('Cement.view.contracts.contracts.form.Contract', {
	extend: 'Cement.view.basic.Form',
	xtype: 'contacts_contracts_form_contract',
	fieldsDisabled: false,
	//url: Cement.Config.url.contracts.contracts.save.contracts,
	urlTemplate: Cement.Config.url.contracts.contracts.save.contracts,
	loadItemUrlTemplate: Cement.Config.url.contracts.contracts.load.contracts,

	priceStoreId: 'contracts.auxiliary.AllPrices',
	price1StoreId: 'contracts.auxiliary.AllPrices2',
	price2StoreId: 'contracts.auxiliary.AllPrices3',

	getNumberColumn: function () {
		return {
			xtype: 'textfield',
			fieldLabel: 'Номер',
			allowBlank: false,
			labelWidth: 100,
			width: 150,
			disabled: this.fieldsDisabled,
			name: 'p_number'
		};
	},

	getDateColumn: function () {
		return {
			xtype: this.getDateFieldXType(),
			fieldLabel: 'Дата',
			allowBlank: false,
			labelWidth: 100,
			width: 150,
			disabled: this.fieldsDisabled,
			name: 'p_date'
		};
	},

	getItems: function () {
	    var client_columns = this.getTwoColumns('', [{
	        name: 'p_client_inn',
	        fieldLabel: 'ИНН',
	        disabled: true
	    }], [{
	        name: 'p_client_kpp',
	        fieldLabel: 'КПП',
	        disabled: true
	    }], true, 'right');
	    client_columns.padding = '0';
	    client_columns.margin = '0';

	    return [
            {
                xtype: 'hiddenfield',
                name: 'id'
            },
            {
                xtype: 'panel',
                border: 0,
                layout: 'hbox',
                defaults: {
                    flex: 1,
                    labelAlign: 'right',
                    border: 0,
                },
                items: [{}, this.getNumberColumn(), this.getDateColumn(), {}]
            },
			{
			    xtype: 'fieldset',
			    title: 'Общие данные',
			    collapsible: true,
			    collapsed: false,
			    bodyPadding: 0,
			    defaults: {
			        labelAlign: 'right',
			        xtype: 'textfield',
			        anchor: '100%'
			    },
			    items: [{
			        xtype: 'fieldcontainer',
			        layout: 'hbox',
			        anchor: '100%',
			        align: 'stretch',
			        items: [{
			            xtype: 'hidden',
			            name: 'p_client'
			        }, {
			            xtype: 'textfield',
			            name: 'p_client_display',
			            fieldLabel: 'Клиент',
			            labelAlign: 'right',
			            flex: 1
			        }, {
			            xtype: 'button',
			            text: 'Выбрать',
			            margin: '0 0 0 4',
			            action: 'show_clients_window',
			            width: 60
			        }]
			    },
					client_columns, {
					    name: 'p_client_address',
					    fieldLabel: 'Адрес',
					    disabled: true
					}
			    ]
			}, {
			    xtype: 'fieldset',
			    title: 'Приложение 1',
			    collapsible: true,
			    collapsed: false,
			    bodyPadding: 0,
			    defaults: {
			        labelAlign: 'right',
			        xtype: 'textfield',
			        anchor: '100%'
			    },
			    items: [
					{
					    name: 'p_addon1_client',
					    fieldLabel: 'Клиент',
					    disabled: true,
					    allowBlank: false
					}, {
					    name: 'p_addon1_contract',
					    fieldLabel: 'К договору',
					    disabled: true
					}
			    ]
			}, {
			    xtype: 'fieldset',
			    title: 'Приложение 2',
			    collapsible: true,
			    collapsed: false,
			    bodyPadding: 0,
			    defaults: {
			        labelAlign: 'right',
			        xtype: 'textfield',
			        anchor: '100%'
			    },
			    items: [
					{
					    name: 'p_addon2_client',
					    fieldLabel: 'Клиент',
					    disabled: true,
					    allowBlank: false
					}, {
					    name: 'p_addon2_contract',
					    fieldLabel: 'К договору',
					    disabled: true
					}
			    ]
			}, {
			    xtype: 'fieldset',
			    title: 'Приложение 3',
			    collapsible: true,
			    collapsed: false,
			    bodyPadding: 0,
			    defaults: {
			        labelAlign: 'right',
			        xtype: 'textfield',
			        anchor: '100%'
			    },
			    items: [
					{
					    name: 'p_addon3_client',
					    fieldLabel: 'Клиент',
					    disabled: true,
					    allowBlank: false
					}, {
					    name: 'p_addon3_contract',
					    fieldLabel: 'К договору',
					    disabled: true
					}, {
					    xtype: 'fieldcontainer',
					    layout: 'hbox',
					    anchor: '100%',
					    align: 'stretch',
					    items: [{
					        xtype: 'hidden',
					        name: 'p_addon_price1'
					    }, {
					        xtype: 'textfield',
					        name: 'p_addon_price1_display',
					        fieldLabel: 'Прайс-лист',
					        labelAlign: 'right',
					        flex: 1
					    }, {
					        xtype: 'button',
					        text: 'Выбрать',
					        margin: '0 0 0 4',
					        action: 'show_addon1_price_window',
					        width: 60
					    }]
					}, {
					    xtype: 'fieldcontainer',
					    layout: 'hbox',
					    anchor: '100%',
					    align: 'stretch',
					    items: [{
					        xtype: 'hidden',
					        name: 'p_addon_price2'
					    }, {
					        xtype: 'textfield',
					        name: 'p_addon_price2_display',
					        fieldLabel: 'Прайс-лист',
					        labelAlign: 'right',
					        flex: 1
					    }, {
					        xtype: 'button',
					        text: 'Выбрать',
					        margin: '0 0 0 4',
					        action: 'show_addon2_price_window',
					        width: 60
					    }]
					}
			    ]
			}, {
			    xtype: 'fieldset',
			    title: 'Дополнительное соглашение',
			    collapsible: true,
			    collapsed: false,
			    bodyPadding: 0,
			    defaults: {
			        xtype: 'textfield',
			        labelAlign: 'right',
			        anchor: '100%'
			    },
			    items: [{
			        xtype: 'fieldcontainer',
			        layout: 'hbox',
			        anchor: '100%',
			        align: 'stretch',
			        items: [{
			            xtype: 'hidden',
			            name: 'p_payment_rules'
			        }, {
			            xtype: 'textfield',
			            name: 'p_payment_rules_display',
			            fieldLabel: 'Условия платежа',
			            labelAlign: 'right',
			            flex: 1
			        }, {
			            xtype: 'button',
			            text: 'Выбрать',
			            margin: '0 0 0 4',
			            action: 'show_payment_rules_window',
			            width: 60
			        }]
			    }]
			}, {
			    xtype: 'fieldset',
			    title: 'Спецификация',
			    collapsible: true,
			    collapsed: false,
			    bodyPadding: 0,
			    defaults: {
			        labelAlign: 'right',
			        xtype: 'textfield',
			        anchor: '100%'
			    },
			    items: [{
			        xtype: 'fieldcontainer',
			        layout: 'hbox',
			        anchor: '100%',
			        align: 'stretch',
			        items: [{
			            xtype: 'hidden',
			            name: 'p_spec_price'
			        }, {
			            xtype: 'textfield',
			            name: 'p_spec_price_display',
			            fieldLabel: 'Прайс-лист',
			            labelAlign: 'right',
			            flex: 1
			        }, {
			            xtype: 'button',
			            text: 'Выбрать',
			            margin: '0 0 0 4',
			            action: 'show_spec_price_window',
			            width: 60
			        }]
			    }]
			}
	    ];
	},

	populateStores: function () {
		Ext.getStore(this.priceStoreId).each(function (item) {
			Ext.getStore(this.price1StoreId).add(item.copy());
			Ext.getStore(this.price2StoreId).add(item.copy());
		}, this);
	},

	initComponent: function () {
	    var me = this;
	    me.url = Ext.String.format(me.urlTemplate, me.contractKind);
	    me.loadItemUrl = Ext.String.format(me.loadItemUrlTemplate, me.contractKind) + '/{0}';

		this.callParent(arguments);
		Ext.getStore(this.priceStoreId).load({
			scope: this,
			callback: this.populateStores
		});
		// Ext.getStore(this.pricelists3StoreId).load();
		this.down('button[action=show_clients_window]').on('click', function () {
			this.createClientWindow();
			this.client_win.show();
		}, this);

		this.down('button[action=show_payment_rules_window]').on('click', function () {
			this.createPaymentRulesWindow();
			this.payment_rules_win.show();
		}, this);

		this.down('button[action=show_spec_price_window]').on('click', function () {
			this.createSpecPriceWindow();
			this.price_spec_window.show();
		}, this);

		this.down('button[action=show_addon1_price_window]').on('click', function () {
			this.createPrice1Window();
			this.price1_window.show();
		}, this);

		this.down('button[action=show_addon2_price_window]').on('click', function () {
			this.createPrice2Window();
			this.price2_window.show();
		}, this);

		this.down('textfield[name=p_number]').on('change', this.setContractNames, this);
		this.down('textfield[name=p_date]').on('change', this.setContractNames, this);
	},

	setContractNames: function () {
		var cName = Ext.String.format(
			'Договор поставки Ж/Д № {0} от {1}',
			this.down('textfield[name=p_number]').getValue(),
			Ext.Date.format(this.down('textfield[name=p_date]').getValue(), 'd.m.Y')
		);
		this.down('textfield[name=p_addon1_contract]').setValue(cName);
		this.down('textfield[name=p_addon2_contract]').setValue(cName);
	    this.down('textfield[name=p_addon3_contract]').setValue(cName);
	},

	createSpecPriceWindow: function () {
		var me = this;
		if (!this.price_spec_window) {
			this.price_spec_window = Ext.create('Cement.view.contracts.contracts.form.PricelistWindow');
			this.price_spec_window.on('priceselected', function (rec) {
				this.down('hidden[name=p_spec_price]').setValue(rec.get('id'));
				this.down('textfield[name=p_spec_price_display]').setValue(rec.get('p_name'));
				me.price_spec_window.hide();
			}, this);
		}
	},

	createPrice2Window: function () {
		var me = this;
		if (!this.price2_window) {
			this.price2_window = Ext.create('Cement.view.contracts.contracts.form.PricelistWindow', {
				mainStoreId: 'contracts.auxiliary.AllPrices2'
			});
			this.price2_window.on('priceselected', function (rec) {
				this.down('hidden[name=p_addon_price2]').setValue(rec.get('id'));
				this.down('textfield[name=p_addon_price2_display]').setValue(rec.get('p_name'));
				me.price2_window.hide();
			}, this);
		}
	},

	createPrice1Window: function () {
		var me = this;
		if (!this.price1_window) {
			this.price1_window = Ext.create('Cement.view.contracts.contracts.form.PricelistWindow', {
				mainStoreId: 'contracts.auxiliary.AllPrices3'
			});
			this.price1_window.on('priceselected', function (rec) {
				this.down('hidden[name=p_addon_price1]').setValue(rec.get('id'));
				this.down('textfield[name=p_addon_price1_display]').setValue(rec.get('p_name'));
				me.price1_window.hide();
			}, this);
		}
	},

	createClientWindow: function () {
		var me = this;
		if (!this.client_win) {
		    this.client_win = Ext.create('Cement.view.clients.PersonalClientWindow');
			this.client_win.on('signitem', function (rec) {
			    me.down('hidden[name=p_client]').setValue(rec.get('p_client'));
				me.down('textfield[name=p_client_display]').setValue(rec.get('p_name'));
				me.down('textfield[name=p_addon1_client]').setValue(rec.get('p_name'));
				me.down('textfield[name=p_addon2_client]').setValue(rec.get('p_name'));
				me.down('textfield[name=p_addon3_client]').setValue(rec.get('p_name'));
				me.down('textfield[name=p_client_inn]').setValue(rec.get('p_inn'));
				me.down('textfield[name=p_client_kpp]').setValue(rec.get('p_kpp'));
				me.down('textfield[name=p_client_address]').setValue(rec.get('p_address'));
				me.client_win.hide();
			}, this);
		}
	},

	createPaymentRulesWindow: function () {
		var me = this;
		if (!this.payment_rules_win) {
			this.payment_rules_win = Ext.create('Cement.view.contracts.contracts.form.PaymentRulesWindow');
			this.payment_rules_win.on('paymentrulescreated', function (json, str) {
				me.down('textfield[name=p_payment_rules_display]').setValue(str);
				me.down('hidden[name=p_payment_rules]').setValue(json);
				me.payment_rules_win.hide();
			}, this);
		}
	},

	afterRecordLoad: function (rec) {
	    var me = this;
	    var clientName = rec.get('p_client_display');

	    me.down('textfield[name=p_addon1_client]').setValue(clientName);
	    me.down('textfield[name=p_addon2_client]').setValue(clientName);
	    me.down('textfield[name=p_addon3_client]').setValue(clientName);

	    var json = Ext.JSON.decode(rec.get('p_payment_rules'));
	    me.down('hidden[name=p_payment_rules]').setValue(Ext.JSON.encode(json));
	    me.createPaymentRulesWindow();
	    me.payment_rules_win.loadData(json);
	    

	    //Ext.getStore(this.priceStoreId).load({
	    //	scope: this,
	    //	callback: function () {
	    //		this.populateStores();
	    //		this.createClientWindow();
	    //		this.createPaymentRulesWindow();
	    //		this.createSpecPriceWindow();
	    //		this.createPrice1Window();
	    //		this.createPrice2Window();

	    //		this.client_win.loadData([rec.get('p_client')]);
	    //		this.payment_rules_win.loadData(rec.get('p_payment_rules'));

	    //		this.price_spec_window.loadData([rec.get('p_spec_price')]);
	    //		this.price1_window.loadData([rec.get('p_addon_price1')]);
	    //		this.price2_window.loadData([rec.get('p_addon_price2')]);

	    //		this.down('hidden[name=p_payment_rules]').setValue(Ext.JSON.encode(rec.get('p_payment_rules')));
	    //	}
	    //});
	}
});