Ext.define('Cement.view.accounting.other.complex.Incoming', {
	extend: 'Cement.view.basic.Complex',
	alias: 'widget.accounting_other_complex_incoming',
    border: 0,
    enableDetail: false,
    autoLoadStore: false,
    priceId: null,
    bodyPadding: 0,
    padding: 0,
    header: false,
    rowsCount: 3,
    title: 'Входящие',
    tabTitle: 'Входящие',
    cls: 'no-side-borders',
    topXType: 'accounting_other_lists_incoming',
    topDetailType: 'Cement.view.accounting.other.view.View',
    bottomTitle: 'Связанные документы',
    bottomTabs: [{
        title: 'Связанные документы',
        xtype: 'accounting_other_bottom_connected_documents',
        detailType: 'Cement.view.accounting.other.view.View',
        constantFilterParam: 'p_base_doc',
        childrenConstantFilterParam: 'p_base_doc',
        border: 0
    }],
    subBottomTitle: 'Справочник',
    subBottomTabs: [
        {
            title: 'Склады',
            shownTitle: 'Склады',
            xtype: 'accounting_other_bottom_new_warehouses',
            detailType: 'Cement.view.corporate.warehouse.View'
        },
        {
            title: 'Товары',
            shownTitle: 'Товары',
            xtype: 'accounting_other_bottom_new_products',
            detailType: 'Cement.view.products.products.View'
        },
        {
            title: 'Услуги',
            shownTitle: 'Услуги',
            xtype: 'accounting_other_bottom_new_services',
            detailType: 'Cement.view.products.services.View'
        },
        {
            title: 'Маршруты',
            shownTitle: 'Маршруты',
            xtype: 'accounting_other_bottom_new_routes',
            detailType: 'Cement.view.products.price.RouteView'
        }
    ]
});