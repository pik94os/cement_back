Ext.define('Cement.view.accounting.other.lists.New', {
	extend: 'Cement.view.basic.Grid',
	alias: 'widget.accounting_other_lists_new',
    autoLoadStore: false,

    bbarText: 'Показаны документы {0} - {1} из {2}',
    bbarEmptyMsg: 'Нет документов',
    bbarUsersText: 'Документы на странице: ',

    gridStore: 'accounting.other.New',
    gridStateId: 'stateAccountingOtherNew',

    printUrl: Cement.Config.url.accounting.other.new_o.printGrid,
    helpUrl: Cement.Config.url.accounting.other.new_o.help,
    deleteUrl: Cement.Config.url.accounting.other.new_o.deleteUrl,

    shownTitle: null,

    creatorTree: Ext.clone(Cement.Creators.accounting.children[Cement.Creators.accounting.otherDocumentIndex]),
    createWindowTitle: 'Создать',

    getActionColumns: function () {
        return {
            xtype: 'rowactions',
            hideable: false,
            resizeable: false,
            locked: true,
            width: 46,
            actions: [
                {
                    iconCls: 'icon-edit-item',
                    qtip: 'Редактировать',
                    callback: this.editItem
                },
                {
                    iconCls: 'icon-delete-item',
                    qtip: 'Удалить',
                    callback: this.deleteItem
                }
            ],
            keepSelection: true
        };
    },

    getFilterItems: function () {
        return [{
            text: 'Выделить все',
            checked: true,
            checkHandler: this.toggleFilterSelection
        }, '-', {
            text: '№ п/п',
            kind: 'selector',
            field_name: 'p_number',
            checked: true
        }];
    },

    getGridColumns: function () {
        var result = [
            { text: '№ п/п', dataIndex: 'p_number', width: 100 },
            { text: 'Дата', dataIndex: 'p_date', width: 80 },
            { text: 'Вид', dataIndex: 'p_kind_display', width: 80 },
            { text: 'Сумма', dataIndex: 'p_sum', width: 150 },
            { text: 'Статус', dataIndex: 'p_state', flex: 1 }
        ];

        result = this.mergeActions(result);
        return result;
    },

    initComponent: function () {
        Ext.apply(this, {
            title: this.shownTitle
        });
        this.callParent(arguments);
    }
});