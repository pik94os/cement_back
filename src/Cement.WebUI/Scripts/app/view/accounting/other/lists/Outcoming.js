Ext.define('Cement.view.accounting.other.lists.Outcoming', {
    extend: 'Cement.view.accounting.other.lists.New',
    alias: 'widget.accounting_other_lists_outcoming',

    gridStore: 'accounting.other.Outcoming',
    gridStateId: 'stateAccountingOtherOutcoming',

    printUrl: Cement.Config.url.accounting.other.outcoming.printGrid,
    helpUrl: Cement.Config.url.accounting.other.outcoming.help,
    deleteUrl: Cement.Config.url.accounting.other.outcoming.deleteUrl,
    archiveItemUrl: Cement.Config.url.accounting.other.outcoming.archiveItem,
    showCreateButton: false,
    shownTitle: null,

    getActionColumns: function () {
        return {
            xtype: 'rowactions',
            hideable: false,
            resizeable: false,
            locked: true,
            width: 46,
            actions: [
                {
                    iconCls: 'icon-archive-item',
                    qtip: 'В архив',
                    callback: this.archiveItem
                }
            ],
            keepSelection: true
        };
    }
});