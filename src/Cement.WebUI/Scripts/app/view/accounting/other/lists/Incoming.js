Ext.define('Cement.view.accounting.other.lists.Incoming', {
    extend: 'Cement.view.accounting.other.lists.New',
    alias: 'widget.accounting_other_lists_incoming',
    
    gridStore: 'accounting.other.Incoming',
    gridStateId: 'stateAccountingOtherIncoming',

    printUrl: Cement.Config.url.accounting.other.incoming.printGrid,
    helpUrl: Cement.Config.url.accounting.other.incoming.help,
    deleteUrl: Cement.Config.url.accounting.other.incoming.deleteUrl,
    archiveItemUrl: Cement.Config.url.accounting.other.incoming.archiveItem,
    showCreateButton: false,
    shownTitle: null,

    getActionColumns: function () {
        return {
            xtype: 'rowactions',
            hideable: false,
            resizeable: false,
            locked: true,
            width: 46,
            actions: [
                {
                    iconCls: 'icon-archive-item',
                    qtip: 'В архив',
                    callback: this.archiveItem
                }
            ],
            keepSelection: true
        };
    }
});