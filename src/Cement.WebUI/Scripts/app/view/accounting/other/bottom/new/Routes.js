Ext.define('Cement.view.accounting.other.bottom.new.Routes', {
    extend: 'Cement.view.office_work.correspondention.bottom.new_c.Routes',
    alias: 'widget.accounting_other_bottom_new_routes',
    gridStore: 'accounting.other.bottom.new.Routes',
    gridStateId: 'stateAccountingOtherBottomNewRoutes'
});