Ext.define('Cement.view.accounting.other.bottom.new.Services', {
    extend: 'Cement.view.products.price.bottom.NewServices',
    alias: 'widget.accounting_other_bottom_new_services',
    gridStore: 'accounting.other.bottom.new.Services',
    gridStateId: 'stateAccountingOtherBottomNewServices'
});