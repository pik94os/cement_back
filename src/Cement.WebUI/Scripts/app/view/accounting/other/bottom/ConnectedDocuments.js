Ext.define('Cement.view.accounting.other.bottom.ConnectedDocuments', {
	extend: 'Cement.view.basic.Grid',
	alias: 'widget.accounting_other_bottom_connected_documents',
    autoLoadStore: false,

    gridStore: 'accounting.other.bottom.ConnectedDocuments',
    gridStateId: 'stateWarehouseAccountingOtherConnectedDocuments',

    showBottomBar: false,
    showCreateButton: false,

    getActionColumns: function () {
        return null;
    },

    getToolbarItems: function () {
        return null;
    },

    getGridColumns: function () {
        return [
            { text: '№ П/п', dataIndex: 'p_number', width: 60, locked: true },
            { text: 'Дата', dataIndex: 'p_date', width: 60, locked: true },
            { text: 'Вид', dataIndex: 'p_kind', width: 80, locked: true },
            { text: 'Статус', dataIndex: 'p_status', width: 250 },
            { text: 'Наименование', dataIndex: 'p_name', width: 550 }
        ];
    }
});