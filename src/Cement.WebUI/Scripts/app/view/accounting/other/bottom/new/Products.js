Ext.define('Cement.view.accounting.other.bottom.new.Products', {
    extend: 'Cement.view.products.price.bottom.NewProducts',
    alias: 'widget.accounting_other_bottom_new_products',
    gridStore: 'accounting.other.bottom.new.Products',
    gridStateId: 'stateAccountingOtherBottomNewProducts'
});