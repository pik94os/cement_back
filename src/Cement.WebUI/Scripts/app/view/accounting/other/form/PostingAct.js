Ext.define('Cement.view.accounting.other.form.PostingAct', {
    extend: 'Cement.view.accounting.other.form.BaseForm',
    alias: 'widget.accounting_other_form_postingact',
    
    formType: Cement.Config.accounting.other_kinds.postingact,

    storeFields: ['id', 'p_number', 'p_date', 'p_kind', 'p_product_display', 'p_measure_unit_display', 'p_count', 'p_product_count_fact', 'p_product_price', 'p_product_tax', 'p_product_sum', 'p_description'],

    bottomGridStoreProxy: Cement.Config.url.accounting.other.new_o.postingRequestProxy,
    warningOnSave: 'Выберите хотя бы одну заявку на оприходование!',

    getTopItems: function () {
        return [
            {
                xtype: 'hiddenfield',
                name: 'p_type',
                value: this.formType
            },
	        {
	            xtype: 'hiddenfield',
	            name: this.dstFieldName
	        },
            {
                fieldLabel: 'Номер',
                readOnly: true,
                name: 'p_number'
            },
            {
                xtype: 'datefield',
                fieldLabel: 'Дата',
                name: 'p_date'
            },
            this.getSelectorField('Основание', 'p_cause', 'show_cause_window', 100)
        ];
    },

    getCommonColumns: function (isSrc) {
        return [
	        { text: '№ п/п', dataIndex: 'p_number', width: 80 },
            { text: 'Дата', dataIndex: 'p_date', width: 60 },
            { text: 'Вид', dataIndex: 'p_kind', width: 250 },
            {
                text: 'Товар',
                columns: [
                    { text: 'Наименование', dataIndex: 'p_product_display', width: 100 },
                    { text: 'Ед.изм.', dataIndex: 'p_measure_unit_display', width: 50 },
                    { text: 'Кол-во', dataIndex: 'p_count', width: 80 },
                    { text: 'Цена', dataIndex: 'p_product_price', width: 80 },
                    { text: 'Налог', dataIndex: 'p_product_tax', width: 50 },
                    { text: 'Сумма', dataIndex: 'p_product_sum', width: 80 }
                ]
            },
	        //{ text: 'Статус', dataIndex: 'p_state', width: 150 },
            { text: 'Примечание', dataIndex: 'p_description', flex: 1 }
        ];
    },

    initComponent: function () {
        this.callParent(arguments);
        var me = this;
        this.down('button[action=show_cause_window]').on('click', function () {
            me.createCauseWindow();
            me.causeWindow.show();
        }, this);
    },

    createCauseWindow: function () {
        if (!this.causeWindow) {
            this.causeWindow = Ext.create('Cement.view.warehouse.internal.form.CausesWindow');
            this.causeWindow.on('causesselected', function (json, str) {
                this.down('textfield[name=p_cause_display]').setValue(str);
                this.down('hiddenfield[name=p_cause]').setValue(Ext.JSON.encode(json));
                this.causeWindow.hide();
            }, this);
        }
    }
});