﻿Ext.define('Cement.view.accounting.other.form.BaseForm', {
    extend: 'Cement.view.basic.DoubleGridForm',
	fieldsDisabled: false,
	hasBottomPanel: false,
	singleSelection: false,
	url: Cement.Config.url.accounting.other.new_o.saveUrl,
	method: Cement.Config.url.accounting.other.new_o.saveMethod,
	printUrl: Cement.Config.url.accounting.other.new_o.printItem,

	loadItemUrl: Cement.Config.url.accounting.other.new_o.loadItem,
	loadItemProperty: 'p_base_doc',
	getNewNumberUrl: Cement.Config.url.accounting.other.new_o.newNumberUrl,
	dstFieldName: 'p_dst',
	warningOnSave: 'Выберите хотя бы одну запись!',
    
	getBottomGridStore: function () {
	    return Ext.create('Ext.data.Store', {
	        fields: this.storeFields,
	        autoLoad: false,
	        proxy: this.bottomGridStoreProxy
	    });
	},

	beforeSubmit: function (callback) {
	    var me = this;

	    if (me.dstStore.getCount() == 0) {
	        Cement.Msg.warning(me.warningOnSave);
	        return;
	    }

	    var dstItems = [];
	    me.dstStore.each(function (item) {
	        dstItems.push(item.get('id'));
	    });

	    me.down('hiddenfield[name=' + me.dstFieldName + ']').setValue(Ext.encode(dstItems));

	    callback.call();
	},

	afterRecordLoad: function (rec) {
	    this.dstStore.removeAll();
	    this.srcStore.removeAll();

	    var dst = rec.get(this.dstFieldName);
	    var src = rec.get('p_src');

	    dst && dst.data && this.dstStore.add(dst.data);
	    src && src.data && this.srcStore.add(src.data);
	},

	onCreate: function () {
	    this.srcStore.load();
	}
});