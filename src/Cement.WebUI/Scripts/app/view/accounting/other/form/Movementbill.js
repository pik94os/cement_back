Ext.define('Cement.view.accounting.other.form.Movementbill', {
    extend: 'Cement.view.accounting.other.form.BaseForm',
	alias: 'widget.accounting_other_form_movementbill',

	formType: Cement.Config.accounting.other_kinds.movementbill,

	storeFields: ['id', 'p_number', 'p_date', 'p_kind', 'p_warehouse_name', 'p_warehouse_address', 'p_product_display', 'p_measure_unit_display', 'p_count', 'p_product_count_fact', 'p_product_price', 'p_product_tax', 'p_product_sum', 'p_description'],
	kind_store_Id: 'accounting.other.auxiliary.MovementbillKinds',
	warningOnSave: 'Выберите хотя бы один расходный складской ордер!',

	bottomGridStoreProxy: Cement.Config.url.accounting.other.new_o.expenseMovementProxy,
    
	getTopItems: function () {
	    return [
            {
                xtype: 'hiddenfield',
                name: 'p_type',
                value: this.formType
            },
	        {
	            xtype: 'hiddenfield',
	            name: this.dstFieldName
	        },
            {
                fieldLabel: 'Номер',
                readOnly: true,
                name: 'p_number'
            },
            {
                xtype: 'datefield',
                fieldLabel: 'Дата',
                name: 'p_date'
            },
	        this.getCombo('Вид', 'p_movementbill_kind', this.kind_store_Id)
	    ];
	},

	getCommonColumns: function (isSrc) {
	    return [
	        { text: '№ п/п', dataIndex: 'p_number', width: 80 },
            { text: 'Дата', dataIndex: 'p_date', width: 60 },
            { text: 'Вид', dataIndex: 'p_kind', width: 250 },
            {
                text: 'Склад',
                columns: [
                    { text: 'Наименование', dataIndex: 'p_warehouse_name', width: 100 },
                    { text: 'Адрес', dataIndex: 'p_warehouse_address', width: 100 }
                ]
            },
            {
                text: 'Товар',
                columns: [
                    { text: 'Наименование', dataIndex: 'p_product_display', width: 100 },
                    { text: 'Ед.изм.', dataIndex: 'p_measure_unit_display', width: 50 },
                    { text: 'Кол-во', dataIndex: 'p_count', width: 80 },
                    { text: 'Цена', dataIndex: 'p_product_price', width: 80 },
                    { text: 'Налог', dataIndex: 'p_product_tax', width: 50 },
                    { text: 'Сумма', dataIndex: 'p_product_sum', width: 80 }
                ]
            },
	        //{ text: 'Статус', dataIndex: 'p_state', width: 100 },
            { text: 'Примечание', dataIndex: 'p_description', flex: 1 }
	    ];
	}
});