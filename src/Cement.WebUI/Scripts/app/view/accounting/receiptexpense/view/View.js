Ext.define('Cement.view.accounting.receiptexpense.view.View', {
    extend: 'Cement.view.basic.Form',
    alias: 'widget.accounting_receiptexpense_view_view',
    fieldsDisabled: true,
    buttons: null,
    border: 0,
    tbar: Cement.Config.modelViewButtons,
    layout: 'autocontainer',
    bbar: null,
    noUserFieldsetsControl: false,
    bodyCls: 'model-form',

    getItems: function () {
        return [
            {
                xtype: 'panel',
                border: 0,
                layout: 'anchor',
                defaultType: 'textfield',
                anchor: '100%',
                bodyPadding: '10 10 10 5',
                autoScroll: true,
                defaults: {
                    anchor: '100%',
                    labelWidth: 110
                },
                items: [
                    {
                        xtype: 'fieldset',
                        layout: 'anchor',
                        defaults: {
                            xtype: 'textfield',
                            anchor: '100%'
                        },
                        border: 0,
                        items: [
                            {
                                fieldLabel: 'Номер',
                                disabled: true,
                                name: 'p_number'
                            },
                            {
                                fieldLabel: 'Дата',
                                disabled: true,
                                name: 'p_date'
                            },
                            {
                                fieldLabel: 'Вид',
                                disabled: true,
                                name: 'p_kind_display'
                            },
                            {
                                fieldLabel: 'Контрагент',
                                disabled: true,
                                name: 'p_contractor_display'
                            },
                            {
                                fieldLabel: 'Сумма',
                                disabled: true,
                                name: 'p_sum'
                            }
                        ]
                    }
                ]
            }
        ];
    },

    initComponent: function () {
        Ext.apply(this, {
            items: [
                {
                    xtype: 'panel',
                    border: 0,
                    layout: 'anchor',
                    defaultType: 'textfield',
                    anchor: '100%',
                    bodyPadding: '10 10 10 5',
                    autoScroll: true,
                    defaults: {
                        anchor: '100%',
                        labelWidth: 110
                    },
                    items: this.getItems()
                }
            ]
        });
        this.callParent(arguments);
    }
});
