Ext.define('Cement.view.accounting.receiptexpense.form.CompletionAct', {
    extend: 'Cement.view.accounting.receiptexpense.form.Form',
    alias: 'widget.accounting_receiptexpense_form_completionact',
    formType: Cement.Config.accounting.receiptexpense_kinds.completionact,

    storeFields: ['id', 'p_number', 'p_date', 'p_kind', 'p_getter_display', 'p_getter_address', 'p_product_display', 'p_measure_unit_display', 'p_count', 'p_product_count_fact', 'p_product_price', 'p_product_tax', 'p_product_sum', 'p_description', 'p_state'],

    bottomGridStoreProxy: Cement.Config.url.accounting.receiptexpense.new_re.expensesCompletionActProxy,

    getTopPanel: function () {
        return {
            xtype: 'panel',
            bodyCls: 'custom-background',
            margin: '0 0 5 0',
            layout: {
                type: 'hbox',
                pack: 'start',
                align: 'stretch'
            },
            items: [
                {
                    xtype: 'fieldset',
                    border: 0,
                    margin: 0,
                    padding: '5 5 0 5',
                    defaults: {
                        xtype: 'textfield',
                        width: 500,
                        labelAlign: 'right'
                    },
                    items: [
                        {
                            xtype: 'hiddenfield',
                            name: 'p_type',
                            value: this.formType
                        },
                        {
                            xtype: 'hiddenfield',
                            name: this.dstFieldName
                        },
                        {
                            fieldLabel: 'Номер',
                            name: 'p_number'
                        },
                        {
                            xtype: 'datefield',
                            fieldLabel: 'Дата',
                            name: 'p_date'
                        }
                    ]
                },
	            {
	                xtype: 'fieldset',
	                border: 0,
	                margin: 0,
	                padding: '5 5 0 5',
	                defaults: {
	                    xtype: 'textfield',
	                    width: 500,
	                    labelAlign: 'right',
	                    disabled: true
	                },
	                items: [
                        {
                            fieldLabel: 'Доверенность',
                            name: 'p_procuration_dispaly'
                        },
                        {
                            fieldLabel: 'Склад',
                            name: 'p_warehouse_display'
                        },
                        {
                            fieldLabel: 'Адрес',
                            name: 'p_warehouse_address'
                        }
	                ]
	            }
            ]
        };
    },

    getCommonColumns: function (isSrc) {
        return [
	         { text: '№ п/п', dataIndex: 'p_number', width: 60 },
            { text: 'Дата', dataIndex: 'p_date', width: 60 },
            { text: 'Вид', dataIndex: 'p_kind', width: 140 },
            {
                text: 'Покупатель',
                columns: [
                    { text: 'Наименование', dataIndex: 'p_getter_display', width: 100 },
                    { text: 'Адрес', dataIndex: 'p_getter_address', width: 100 }
                ]
            },
            {
                text: 'Услуга',
                columns: [
                    { text: 'Наименование', dataIndex: 'p_product_name', width: 100 },
                    { text: 'Ед.изм.', dataIndex: 'p_product_unit', width: 50 },
                    { text: 'Кол-во', dataIndex: 'p_product_count', width: 80 },
                    { text: 'Цена', dataIndex: 'p_product_price', width: 80 },
                    { text: 'Налог', dataIndex: 'p_product_tax', width: 50 },
                    { text: 'Сумма', dataIndex: 'p_product_sum', width: 80 }
                ]
            },
	        { text: 'Статус', dataIndex: 'p_state', width: 100 },
            { text: 'Примечание', dataIndex: 'p_description', flex: 1 }
        ];
    }
});