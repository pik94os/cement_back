Ext.define('Cement.view.accounting.receiptexpense.form.Invoice', {
    extend: 'Cement.view.accounting.receiptexpense.form.Form',
    alias: 'widget.accounting_receiptexpense_form_invoice',
    formType: Cement.Config.accounting.receiptexpense_kinds.invoice,

    storeFields: ['id', 'p_number', 'p_date', 'p_kind', 'p_contractor_display', 'p_sum', 'p_state'],

    bottomGridStoreProxy: Cement.Config.url.accounting.receiptexpense.new_re.accountingBillProxy,


    getTopPanel: function () {
        return {
            xtype: 'panel',
            bodyCls: 'custom-background',
            margin: '0 0 5 0',
            items: [
                {
                    xtype: 'fieldset',
                    border: 0,
                    margin: 0,
                    padding: '5 5 0 5',
                    defaults: {
                        xtype: 'textfield',
                        width: 600,
                        labelAlign: 'right',
                        labelWidth: 200
                    },
                    items: [
                        {
                            xtype: 'hiddenfield',
                            name: 'p_type',
                            value: this.formType
                        },
                        {
                            xtype: 'hiddenfield',
                            name: this.dstFieldName
                        },
                        {
                            fieldLabel: 'Номер',
                            name: 'p_number'
                        },
                        {
                            xtype: 'datefield',
                            fieldLabel: 'Дата',
                            name: 'p_date'
                        },
                        this.getSelectorField('К платежно-расчетному документу', 'p_payment_document', 'show_paymentdoc_window', 200)
                    ]
                }
            ]
        };
    },

    getCommonColumns: function (isSrc) {
        return [
	        { text: '№ п/п', dataIndex: 'p_number', width: 100 },
            { text: 'Дата', dataIndex: 'p_date', width: 60 },
            { text: 'Вид', dataIndex: 'p_kind', width: 140 },
            { text: 'Контрагент', dataIndex: 'p_contractor_display', flex: 1 },
            { text: 'Сумма', dataIndex: 'p_sum', width: 140 },
	        //{ text: 'Статус', dataIndex: 'p_state', flex: 1 }
        ];
    },
    
    initComponent: function () {
        this.callParent(arguments);
        this.down('button[action=show_paymentdoc_window]').on('click', function () {
            this.createPaymentdocWindow();
            this.paymentdocWindow.show();
        }, this);
    },

    createPaymentdocWindow: function () {
        if (!this.paymentdocWindow) {
            this.paymentdocWindow = Ext.create('Cement.view.accounting.receiptexpense.form.PaymentdocWindow');
            this.paymentdocWindow.on('selected', function (rec) {
                this.down('textfield[name=p_payment_document_display]').setValue(rec.get('p_name'));
                this.down('hiddenfield[name=p_payment_document]').setValue(rec.get('id'));
                this.paymentdocWindow.hide();
            }, this);
        }
    }
});