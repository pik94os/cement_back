Ext.define('Cement.view.accounting.receiptexpense.form.PaymentdocWindow', {
	extend: 'Cement.view.common.SignWindow',
	xtype: 'goods_auto_form_paymentdoc_window',
	title: 'Выбор документа',
	leftFrameTitle: 'Документ',
	topGridTitle: 'Выбрать документ',
	bottomGridTitle: 'Выбранный документ',
	structure_storeId: 'accounting.receiptexpense.auxiliary.PaymentDocuments',
	hideCommentPanel: true,
	singleSelection: true,
	displayField: 'p_name',
	storeFields: ['id', 'p_name'],

	bbar: [{
		text: 'Сохранить',
		action: 'confirm'
	}, {
		text: 'Отмена',
		action: 'cancel'
	}],

	sign: function (send) {
		if (this.dstStore.getCount() > 0) {
			this.fireEvent('selected', this.dstStore.getAt(0));
		}
	},

	getTopColumnsConfig: function () {
		return [
			{
				xtype: 'rowactions',
				hideable: false,
				resizeable: false,
				width: 18,
				keepSelection: true,
				actions: [{ 
	                iconCls: 'icon-add-people', 
	                qtip: 'Добавить',
	                callback: this.addEmployee
	            }]
			},
			{ text: 'Наименование', dataIndex: 'p_name', flex: 1 }
		];
	},

	getBottomColumnsConfig: function () {
		return [
			{
				xtype: 'rowactions',
				hideable: false,
				resizeable: false,
				width: 18,
				actions: [{ 
	                iconCls: 'icon-remove-people', 
	                qtip: 'Удалить', 
	                callback: this.removeEmployee
	            }]
			},
			{ text: 'Наименование', dataIndex: 'p_name', flex: 1 }
		];
	}
});