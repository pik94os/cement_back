Ext.define('Cement.view.accounting.receiptexpense.lists.Incoming', {
    extend: 'Cement.view.accounting.receiptexpense.lists.New',
    alias: 'widget.accounting_receiptexpense_lists_incoming',
    
    gridStore: 'accounting.receiptexpense.Incoming',
    gridStateId: 'stateAccountingReceiptexpenseIncoming',

    printUrl: Cement.Config.url.accounting.receiptexpense.incoming.printGrid,
    helpUrl: Cement.Config.url.accounting.receiptexpense.incoming.help,
    deleteUrl: Cement.Config.url.accounting.receiptexpense.incoming.deleteUrl,
    archiveItemUrl: Cement.Config.url.accounting.receiptexpense.incoming.archiveItem,
    showCreateButton: false,
    shownTitle: null,

    contractorColumnName: 'p_organization_from',

    getActionColumns: function () {
        return {
            xtype: 'rowactions',
            hideable: false,
            resizeable: false,
            locked: true,
            width: 46,
            actions: [
                {
                    iconCls: 'icon-archive-item',
                    qtip: 'В архив',
                    callback: this.archiveItem
                }
            ],
            keepSelection: true
        };
    }
});