Ext.define('Cement.view.accounting.receiptexpense.lists.Outcoming', {
    extend: 'Cement.view.accounting.receiptexpense.lists.New',
    alias: 'widget.accounting_receiptexpense_lists_outcoming',

    gridStore: 'accounting.receiptexpense.Outcoming',
    gridStateId: 'stateAccountingReceiptexpenseOutcoming',

    printUrl: Cement.Config.url.accounting.receiptexpense.outcoming.printGrid,
    helpUrl: Cement.Config.url.accounting.receiptexpense.outcoming.help,
    deleteUrl: Cement.Config.url.accounting.receiptexpense.outcoming.deleteUrl,
    archiveItemUrl: Cement.Config.url.accounting.receiptexpense.outcoming.archiveItem,
    showCreateButton: false,
    shownTitle: null,

    getActionColumns: function () {
        return {
            xtype: 'rowactions',
            hideable: false,
            resizeable: false,
            locked: true,
            width: 46,
            actions: [
                {
                    iconCls: 'icon-archive-item',
                    qtip: 'В архив',
                    callback: this.archiveItem
                }
            ],
            keepSelection: true
        };
    }
});