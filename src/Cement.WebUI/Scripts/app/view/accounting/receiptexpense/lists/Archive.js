Ext.define('Cement.view.accounting.receiptexpense.lists.Archive', {
    extend: 'Cement.view.accounting.receiptexpense.lists.New',
    alias: 'widget.accounting_receiptexpense_lists_archive',

    gridStore: 'accounting.receiptexpense.Archive',
    gridStateId: 'stateAccountingReceiptexpenseArchive',
    gridStateful: false,

    printUrl: Cement.Config.url.accounting.receiptexpense.archive.printGrid,
    helpUrl: Cement.Config.url.accounting.receiptexpense.archive.help,
    deleteUrl: Cement.Config.url.accounting.receiptexpense.archive.deleteUrl,
    unArchiveItemUrl: Cement.Config.url.accounting.receiptexpense.archive.unArchiveItem,
    showCreateButton: false,
    shownTitle: null,

    getActionColumns: function () {
        return {
            xtype: 'rowactions',
            hideable: false,
            resizeable: false,
            locked: true,
            width: 46,
            actions: [
                {
                    iconCls: 'icon-unarchive-item',
                    qtip: 'Из архива',
                    callback: this.unArchiveItem
                }
            ],
            keepSelection: true
        };
    },

    getGridColumns: function () {
        var result = [
            { text: 'Тип', dataIndex: 'p_type', width: 80 },
            { text: '№ п/п', dataIndex: 'p_number', width: 100 },
            { text: 'Дата', dataIndex: 'p_date', width: 80 },
            { text: 'Вид', dataIndex: 'p_kind_display', width: 80 },
            { text: 'Контрагент', dataIndex: 'p_contractor_display', width: 200 },
            { text: 'Сумма', dataIndex: 'p_sum', width: 150 },
            { text: 'Статус', dataIndex: 'p_state', flex: 1 }
        ];

        result = this.mergeActions(result);
        return result;
    }
});