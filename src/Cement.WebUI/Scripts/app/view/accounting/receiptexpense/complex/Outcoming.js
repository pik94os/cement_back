Ext.define('Cement.view.accounting.receiptexpense.complex.Outcoming', {
	extend: 'Cement.view.basic.Complex',
	alias: 'widget.accounting_receiptexpense_complex_outcoming',
    border: 0,
    enableDetail: false,
    autoLoadStore: false,
    priceId: null,
    bodyPadding: 0,
    padding: 0,
    header: false,
    rowsCount: 3,
    title: 'Исходящие',
    tabTitle: 'Исходящие',
    cls: 'no-side-borders',
    topXType: 'accounting_receiptexpense_lists_outcoming',
    topDetailType: 'Cement.view.accounting.receiptexpense.view.View',
    bottomTitle: 'Связанные документы',
    bottomTabs: [{
        title: 'Связанные документы',
        xtype: 'accounting_receiptexpense_bottom_connected_documents',
        detailType: 'Cement.view.accounting.receiptexpense.view.View',
        constantFilterParam: 'p_base_doc',
        childrenConstantFilterParam: 'p_base_doc',
        border: 0
    }],
    subBottomTitle: 'Справочник',
    subBottomTabs: [
        {
            title: 'Грузоотправитель',
            shownTitle: 'Грузоотправитель',
            xtype: 'accounting_receiptexpense_bottom_new_senders',
            detailType: 'Cement.view.corporate.firm.View'
        },
        {
            title: 'Грузополучатель',
            shownTitle: 'Грузополучатель',
            xtype: 'accounting_receiptexpense_bottom_new_getters',
            detailType: 'Cement.view.corporate.firm.View'
        },
        {
            title: 'Плательщик',
            shownTitle: 'Плательщик',
            xtype: 'accounting_receiptexpense_bottom_new_payers',
            detailType: 'Cement.view.corporate.firm.View'
        },
        {
            title: 'Поставщик',
            shownTitle: 'Поставщик',
            xtype: 'accounting_receiptexpense_bottom_new_suppliers',
            detailType: 'Cement.view.corporate.firm.View'
        },
        {
            title: 'Продавец',
            shownTitle: 'Продавец',
            xtype: 'accounting_receiptexpense_bottom_new_sellers',
            detailType: 'Cement.view.corporate.firm.View'
        },
        {
            title: 'Товары',
            shownTitle: 'Товары',
            xtype: 'accounting_receiptexpense_bottom_new_products',
            detailType: 'Cement.view.products.products.View'
        },
        {
            title: 'Услуги',
            shownTitle: 'Услуги',
            xtype: 'accounting_receiptexpense_bottom_new_services',
            detailType: 'Cement.view.products.services.View'
        },
        {
            title: 'Маршруты',
            shownTitle: 'Маршруты',
            xtype: 'accounting_receiptexpense_bottom_new_routes',
            detailType: 'Cement.view.products.price.RouteView'
        }
    ]
});