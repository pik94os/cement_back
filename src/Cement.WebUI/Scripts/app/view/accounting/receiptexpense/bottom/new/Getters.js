Ext.define('Cement.view.accounting.receiptexpense.bottom.new.Getters', {
    extend: 'Cement.view.accounting.receiptexpense.bottom.new.Senders',
    alias: 'widget.accounting_receiptexpense_bottom_new_getters',
    gridStore: 'accounting.receiptexpense.bottom.new.Getters',
    gridStateId: 'stateAccountingReceiptexpenseBottomNewGetters',
  showFilterButton: false,

  printItemUrl: Cement.Config.url.accounting.receiptexpense.getters.printItem,
  printUrl: Cement.Config.url.accounting.receiptexpense.getters.printGrid,
  helpUrl: Cement.Config.url.accounting.receiptexpense.getters.help
});