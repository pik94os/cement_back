Ext.define('Cement.view.accounting.receiptexpense.bottom.new.Products', {
    extend: 'Cement.view.products.price.bottom.NewProducts',
    alias: 'widget.accounting_receiptexpense_bottom_new_products',
    gridStore: 'accounting.receiptexpense.bottom.new.Products',
    gridStateId: 'stateAccountingReceiptexpenseBottomNewProducts'
});