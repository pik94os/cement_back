Ext.define('Cement.view.accounting.receiptexpense.bottom.new.Suppliers', {
    extend: 'Cement.view.accounting.receiptexpense.bottom.new.Senders',
    alias: 'widget.accounting_receiptexpense_bottom_new_suppliers',
    gridStore: 'accounting.receiptexpense.bottom.new.Suppliers',
    gridStateId: 'stateAccountingReceiptexpenseBottomNewSuppliers',
    showFilterButton: false,

    printItemUrl: Cement.Config.url.accounting.receiptexpense.suppliers.printItem,
    printUrl: Cement.Config.url.accounting.receiptexpense.suppliers.printGrid,
    helpUrl: Cement.Config.url.accounting.receiptexpense.suppliers.help
});