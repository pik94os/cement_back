Ext.define('Cement.view.accounting.receiptexpense.bottom.new.Services', {
    extend: 'Cement.view.products.price.bottom.NewServices',
    alias: 'widget.accounting_receiptexpense_bottom_new_services',
    gridStore: 'accounting.receiptexpense.bottom.new.Services',
    gridStateId: 'stateAccountingReceiptexpenseBottomNewServices'
});