Ext.define('Cement.view.accounting.receiptexpense.bottom.new.Routes', {
    extend: 'Cement.view.office_work.correspondention.bottom.new_c.Routes',
    alias: 'widget.accounting_receiptexpense_bottom_new_routes',
    gridStore: 'accounting.receiptexpense.bottom.new.Routes',
    gridStateId: 'stateAccountingReceiptexpenseBottomNewRoutes'
});