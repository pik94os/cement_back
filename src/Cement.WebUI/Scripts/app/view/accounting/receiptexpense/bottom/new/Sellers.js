Ext.define('Cement.view.accounting.receiptexpense.bottom.new.Sellers', {
    extend: 'Cement.view.accounting.receiptexpense.bottom.new.Senders',
    alias: 'widget.accounting_receiptexpense_bottom_new_sellers',
    gridStore: 'accounting.receiptexpense.bottom.new.Sellers',
    gridStateId: 'stateAccountingReceiptexpenseBottomNewSellers',
  showFilterButton: false,

  printItemUrl: Cement.Config.url.accounting.receiptexpense.sellers.printItem,
  printUrl: Cement.Config.url.accounting.receiptexpense.sellers.printGrid,
  helpUrl: Cement.Config.url.accounting.receiptexpense.sellers.help
});