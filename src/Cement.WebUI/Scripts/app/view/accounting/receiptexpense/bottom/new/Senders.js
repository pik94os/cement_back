Ext.define('Cement.view.accounting.receiptexpense.bottom.new.Senders', {
  extend: 'Cement.view.basic.Grid',
  alias: 'widget.accounting_receiptexpense_bottom_new_senders',
  gridStore: 'accounting.receiptexpense.bottom.new.Senders',
  gridStateId: 'stateAccountingReceiptexpenseBottomNewSenders',
  showFilterButton: false,
  autoLoadStore: false,

  printItemUrl: Cement.Config.url.accounting.receiptexpense.senders.printItem,
  printUrl: Cement.Config.url.accounting.receiptexpense.senders.printGrid,
  helpUrl: Cement.Config.url.accounting.receiptexpense.senders.help,

  getActionColumns: function () {
    return null;
  },

  getToolbarItems: function () {
    return null;
  },

  getGridColumns: function () {
    return [
      { text: 'Группа', dataIndex: 'p_group', width: 200, locked: true },
      { text: 'Наименование', dataIndex: 'p_name', width: 200, locked: true },
      { text: 'Адрес', dataIndex: 'p_address', width: 200 },
      { text: 'ИНН', dataIndex: 'p_inn', width: 200 },
      { text: 'КПП', dataIndex: 'p_kpp', width: 200 },
      { text: 'ОГРН', dataIndex: 'p_ogrn', width: 200 },
      { text: 'Вид деятельности', dataIndex: 'p_kind', width: 200 }
    ];
  }
});