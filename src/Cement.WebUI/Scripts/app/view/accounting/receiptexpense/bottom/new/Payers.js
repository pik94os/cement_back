Ext.define('Cement.view.accounting.receiptexpense.bottom.new.Payers', {
    extend: 'Cement.view.accounting.receiptexpense.bottom.new.Senders',
    alias: 'widget.accounting_receiptexpense_bottom_new_payers',
    gridStore: 'accounting.receiptexpense.bottom.new.Payers',
    gridStateId: 'stateAccountingReceiptexpenseBottomNewPayers',
    showFilterButton: false,

    printItemUrl: Cement.Config.url.accounting.receiptexpense.payers.printItem,
    printUrl: Cement.Config.url.accounting.receiptexpense.payers.printGrid,
    helpUrl: Cement.Config.url.accounting.receiptexpense.payers.help
});