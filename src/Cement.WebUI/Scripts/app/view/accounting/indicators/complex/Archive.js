Ext.define('Cement.view.accounting.indicators.complex.Archive', {
	extend: 'Cement.view.basic.Complex',
	alias: 'widget.accounting_indicators_complex_archive',
    border: 0,
    enableDetail: false,
    autoLoadStore: false,
    priceId: null,
    bodyPadding: 0,
    padding: 0,
    header: false,
    title: null,
    rowsCount: 3,
    title: 'Архив',
    tabTitle: 'Архив',
    cls: 'no-side-borders',
    topXType: 'accounting_indicators_lists_archive',
    topDetailType: 'Cement.view.accounting.indicators.view.View',
    bottomTitle: 'Связанные документы',
    bottomTabs: [{
        title: 'Связанные документы',
        xtype: 'accounting_indicators_bottom_archive_connected_documents_incoming',
        detailType: 'Cement.view.goods.auto.view.ConnectedDocument',
        constantFilterParam: 'p_base_doc',
        childrenConstantFilterParam: 'p_base_doc',
        border: 0
    }],
    subBottomTitle: 'Справочник',
    subBottomTabs: [{
        title: 'Маршрут',
        shownTitle: 'Маршрут',
        xtype: 'accounting_indicators_bottom_archive_routes',
        detailType: 'Cement.view.products.price.RouteView'
    }]
});