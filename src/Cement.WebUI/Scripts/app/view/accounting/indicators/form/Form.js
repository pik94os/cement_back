Ext.define('Cement.view.accounting.indicators.form.Form', {
  extend: 'Cement.view.basic.Form',
  alias: 'widget.accounting_indicators_form_form',
  fieldsDisabled: false,
  url: Cement.Config.url.accounting.indicators.new_i.saveUrl,
  method: Cement.Config.url.accounting.indicators.new_i.saveMethod,
  isFilter: false,
  layout: 'fit',
  allowBlankFields: true,
  showSaveAndSignButton: true,
  saveButtonDisabled: false,

  getItems: function () {
    return [{
                xtype: 'fieldset',
                border: 0, 
                margin: 0,
                defaults: {
                    xtype: 'textfield',
                    anchor: '100%',
                    labelWidth: 200,
                    labelAlign: 'right'
                },
                items: [{
                  fieldLabel: 'Дата начала',
                  name: 'p_date_start',
                  xtype: 'datefield'
                }, {
                  fieldLabel: 'Дата окончания',
                  name: 'p_date_end',
                  xtype: 'datefield'
                },
                this.getSelectorField('Наименование', 'p_name', 'show_client_window'),
                this.getSelectorField('Условия платежа', 'p_payment_rules', 'show_payment_rules_window'), {
                  fieldLabel: 'Лимит',
                  name: 'p_limit'
                },
                this.getSelectorField('Основание', 'p_basis', 'show_basis_window'),
                this.getFileField('Основание', 'p_basis_file', '0 0 5 0'), {
                  xtype: 'textarea',
                  fieldLabel: 'Комментарий',
                  name: 'p_comment',
                  height: 150
                }
              ]
            }
        ];
  },

  initComponent: function () {
    this.callParent(arguments);

    this.down('button[action=show_client_window]').on('click', function () {
      this.createClientWindow();
      this.clientWindow.show();
    }, this);

    this.down('button[action=show_payment_rules_window]').on('click', function () {
      this.createPaymentRulesWindow();
      this.paymentRulesWindow.show();
    }, this);

    this.down('filefield').on('change', function () {
      this.down('fieldcontainer[role=container_p_basis]').disable();
    }, this);

    this.down('button[action=show_basis_window]').on('click', function () {
      this.createConnectedDocumentsWindow();
      this.connectedDocumentsWindow.show();
    }, this);
  },

  createClientWindow: function () {
    if (!this.clientWindow) {
      this.clientWindow = Ext.create('Cement.view.accounting.indicators.form.ClientsWindow');
      this.clientWindow.on('signitem', function (rec) {
        this.down('textfield[name=p_name_display]').setValue(rec.get('p_name'));
        this.down('hiddenfield[name=p_name]').setValue(rec.get('id'));
        this.clientWindow.hide();
      }, this);
    }
  },

  createConnectedDocumentsWindow: function () {
    if (!this.connectedDocumentsWindow) {
      this.connectedDocumentsWindow = Ext.create('Cement.view.accounting.indicators.form.ConnectedDocumentsWindow');
      this.connectedDocumentsWindow.on('docselected', function (name, ids) {
        this.down('textfield[name=p_basis_display]').setValue(name);
        this.down('hiddenfield[name=p_basis]').setValue(Ext.JSON.encode(ids));
        this.down('filefield').disable();
        this.connectedDocumentsWindow.hide();
      }, this);
    }
  },

  createPaymentRulesWindow: function () {
    if (!this.paymentRulesWindow) {
      this.paymentRulesWindow = Ext.create('Cement.view.contracts.contracts.form.PaymentRulesWindow');
      this.paymentRulesWindow.on('paymentrulescreated', function (json, str) {
        this.down('textfield[name=p_payment_rules_display]').setValue(str);
        this.down('hiddenfield[name=p_payment_rules]').setValue(json);
        this.paymentRulesWindow.hide();
      }, this);
    }
  },

  afterRecordLoad: function (rec) {
    this.createPaymentRulesWindow();
    this.createClientWindow();
    this.createConnectedDocumentsWindow();
    this.paymentRulesWindow.loadData(Ext.JSON.decode(rec.get('p_payment_rules')));
  },

  reset: function () {
    this.down('fieldcontainer[role=container_show_basis_window]').enable();
    this.down('filefield').enable();
  }
});