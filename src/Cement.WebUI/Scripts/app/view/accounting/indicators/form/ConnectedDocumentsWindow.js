Ext.define('Cement.view.accounting.indicators.form.ConnectedDocumentsWindow', {
  extend: 'Cement.view.goods.plans.form.ConnectedDocumentsWindow',
  xtype: 'accounting_indicators_connected_documents_window',
  structure_storeId: 'accounting.auxiliary.IndicatorsConnectedDocuments'
});