Ext.define('Cement.view.accounting.indicators.form.ClientsWindow', {
  extend: 'Cement.view.clients.ClientWindow',
  xtype: 'accounting_indicators_form_client_window',
  structure_storeId: 'clients.Structure'
});