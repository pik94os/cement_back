Ext.define('Cement.view.accounting.indicators.ToolbarMixin', {
    extend: 'Cement.view.accounting.documents.ToolbarMixin',
    filterStoreId: 'accounting.auxiliary.IndicatorKinds',
    filterParamName: 'p_indicator_kinds'
});