Ext.define('Cement.view.accounting.indicators.view.View', {
  extend: 'Cement.view.basic.Form',
  alias: 'widget.accounting_indicators_view_view',
  fieldsDisabled: true,
  buttons: null,
  border: 0,
  tbar: Cement.Config.modelViewButtons,
  layout: 'autocontainer',
  bbar: null,
  noUserFieldsetsControl: false,
  bodyCls: 'model-form',

  initComponent: function ()  {
    Ext.apply(this, {
      items: []
    });
    this.callParent(arguments);
  }
});