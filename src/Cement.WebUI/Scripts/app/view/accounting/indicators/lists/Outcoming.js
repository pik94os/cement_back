Ext.define('Cement.view.accounting.indicators.lists.Outcoming', {
  extend: 'Cement.view.accounting.indicators.lists.New',
  alias: 'widget.accounting_indicators_lists_outcoming',
    autoLoadStore: true,

  gridStore: 'accounting.indicators.Outcoming',
  gridStateId: 'stateAccountingIndicatorsOutcoming',

  printUrl: Cement.Config.url.accounting.indicators.outcoming.printGrid,
  helpUrl: Cement.Config.url.accounting.indicators.outcoming.help,
    deleteUrl: Cement.Config.url.accounting.indicators.outcoming.deleteUrl,
    archiveItemUrl: Cement.Config.url.accounting.indicators.outcoming.archiveItem,

    shownTitle: null,

    getActionColumns: function () {
        return {
            xtype: 'rowactions',
            hideable: false,
            resizeable: false,
            locked: true,
            width: 46,
            actions: [
                {
                    iconCls: 'icon-delete-item',
                    qtip: 'Удалить',
                    callback: this.deleteItem
                },
                {
                    iconCls: 'icon-archive-item',
                    qtip: 'В архив',
                    callback: this.archiveItem
                }
            ],
            keepSelection: true
        };
    }
});