Ext.define('Cement.view.accounting.indicators.lists.Incoming', {
	extend: 'Cement.view.accounting.indicators.lists.New',
	alias: 'widget.accounting_indicators_lists_incoming',
    autoLoadStore: true,

	gridStore: 'accounting.indicators.Incoming',
	gridStateId: 'stateAccountingIndicatorsIncoming',

	printUrl: Cement.Config.url.accounting.indicators.incoming.printGrid,
	helpUrl: Cement.Config.url.accounting.indicators.incoming.help,
    deleteUrl: Cement.Config.url.accounting.indicators.incoming.deleteUrl,
    archiveItemUrl: Cement.Config.url.accounting.indicators.incoming.archiveItem,

    shownTitle: null,

    getActionColumns: function () {
        return {
            xtype: 'rowactions',
            hideable: false,
            resizeable: false,
            locked: true,
            width: 46,
            actions: [
                {
                    iconCls: 'icon-delete-item',
                    qtip: 'Удалить',
                    callback: this.deleteItem
                },
                {
                    iconCls: 'icon-archive-item',
                    qtip: 'В архив',
                    callback: this.archiveItem
                }
            ],
            keepSelection: true
        };
    }
});