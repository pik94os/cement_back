Ext.define('Cement.view.accounting.indicators.lists.Archive', {
  extend: 'Cement.view.basic.Grid',
  alias: 'widget.accounting_indicators_lists_archive',
    autoLoadStore: true,

    bbarText: 'Показаны индикаторы {0} - {1} из {2}',
    bbarEmptyMsg: 'Нет индикаторов',
    bbarUsersText: 'Индикаторов на странице: ',

    gridStore: 'accounting.indicators.Archive',
    gridStateId: 'stateAccountingIndicatorsArchive',

    printUrl: Cement.Config.url.accounting.indicators.archive.printGrid,
    helpUrl: Cement.Config.url.accounting.indicators.archive.help,
    unArchiveItemUrl: Cement.Config.url.accounting.indicators.archive.unArchiveItem,

    shownTitle: null,

    getActionColumns: function () {
        return {
            xtype: 'rowactions',
            hideable: false,
            resizeable: false,
            locked: true,
            width: 23,
            actions: [
                {
                    iconCls: 'icon-unarchive-item',
                    qtip: 'Из архива',
                    callback: this.unArchiveItem
                }
            ],
            keepSelection: true
        };
    },

    getFilterItems: function () {
        return [{
            text: 'Наименование',
            kind: 'selector',
            field_name: 'p_name',
            checked: true
        }, {
            text: 'Тип',
            kind: 'selector',
            field_name: 'p_kind',
            checked: true
        }, {
            text: 'Отгружено',
            kind: 'selector',
            field_name: 'p_loaded',
            checked: true
        }, {
            text: 'Заявлено',
            kind: 'selector',
            field_name: 'p_requested',
            checked: true
        }, {
            text: 'Итого',
            kind: 'selector',
            field_name: 'p_total',
            checked: true
        }, {
            text: 'Лимит',
            kind: 'selector',
            field_name: 'p_limit',
            checked: true
        }, {
            text: 'Отсрочка',
            kind: 'selector',
            field_name: 'p_delay',
            checked: true
        }, {
            text: '+-',
            kind: 'selector',
            field_name: 'p_plus_minus',
            checked: true
        }, {
            text: 'Дата начала',
            kind: 'selector',
            field_name: 'p_date_start',
            checked: true
        }, {
            text: 'Дата окончания',
            kind: 'selector',
            field_name: 'p_date_end',
            checked: true
        }];
    },

    getGridColumns: function () {
        var result = [
            { text: 'Тип', dataIndex: 'p_kind', width: 60, locked: true },
            { text: 'Наименование', dataIndex: 'p_name', width: 150, locked: true },
            { text: 'Дебет/Кредит',
                columns: [
                    { text: 'Отгружено', dataIndex: 'p_loaded', width: 150 },
                    { text: 'Заявлено', dataIndex: 'p_requested', width: 150 },
                    { text: 'Итого', dataIndex: 'p_total', width: 150 }
                ]
            },
            { text: 'Лимит', dataIndex: 'p_limit', width: 150 },
            { text: 'Отсрочка', dataIndex: 'p_delay', width: 150 },
            { text: '+-', dataIndex: 'p_plus_minus', width: 150 },
            { text: 'Дата начала', dataIndex: 'p_date_start', width: 150 },
            { text: 'Дата окончания', dataIndex: 'p_date_end', width: 150 }
        ];
        return this.mergeActions(result);
    },

    initComponent: function () {
        Ext.apply(this, {
            title: this.shownTitle
        });
        this.callParent(arguments);
    }
});