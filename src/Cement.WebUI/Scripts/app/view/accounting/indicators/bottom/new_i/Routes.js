Ext.define('Cement.view.accounting.indicators.bottom.new_i.Routes', {
    extend: 'Cement.view.products.price.bottom.NewRoutes',
    alias: 'widget.accounting_indicators_bottom_new_routes',
    gridStore: 'accounting.indicators.bottom.new_i.Routes',
    gridStateId: 'stateAccountingIndicatorsBottomNewRoutes'
});