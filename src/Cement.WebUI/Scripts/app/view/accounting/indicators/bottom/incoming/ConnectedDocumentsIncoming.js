Ext.define('Cement.view.accounting.indicators.bottom.incoming.ConnectedDocumentsIncoming', {
	extend: 'Cement.view.goods.auto.lists.ConnectedDocuments',
	alias: 'widget.accounting_indicators_bottom_incoming_connected_documents_incoming',

	gridStore: 'accounting.indicators.bottom.incoming.ConnectedDocumentsIncoming',
	gridStateId: 'stateAccountingIndicatorsBottomIncomingConnectedDocumentsIncoming',

    printUrl: Cement.Config.url.accounting.indicators.documents.printGrid,
    helpUrl: Cement.Config.url.accounting.indicators.documents.help
});