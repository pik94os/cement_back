Ext.define('Cement.view.accounting.indicators.bottom.archive.ConnectedDocumentsIncoming', {
	extend: 'Cement.view.goods.auto.lists.ConnectedDocuments',
	alias: 'widget.accounting_indicators_bottom_archive_connected_documents_incoming',

	gridStore: 'accounting.indicators.bottom.archive.ConnectedDocumentsIncoming',
	gridStateId: 'stateAccountingIndicatorsBottomArchiveConnectedDocumentsIncoming',

    printUrl: Cement.Config.url.accounting.indicators.documents.printGrid,
    helpUrl: Cement.Config.url.accounting.indicators.documents.help
});