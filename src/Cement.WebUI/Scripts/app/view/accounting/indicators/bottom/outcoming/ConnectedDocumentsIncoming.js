Ext.define('Cement.view.accounting.indicators.bottom.outcoming.ConnectedDocumentsIncoming', {
	extend: 'Cement.view.goods.auto.lists.ConnectedDocuments',
	alias: 'widget.accounting_indicators_bottom_outcoming_connected_documents_incoming',

	gridStore: 'accounting.indicators.bottom.outcoming.ConnectedDocumentsIncoming',
	gridStateId: 'stateAccountingIndicatorsBottomOutcomingConnectedDocumentsIncoming',

    printUrl: Cement.Config.url.accounting.indicators.documents.printGrid,
    helpUrl: Cement.Config.url.accounting.indicators.documents.help
});