Ext.define('Cement.view.accounting.indicators.bottom.archive.Routes', {
    extend: 'Cement.view.products.price.bottom.NewRoutes',
    alias: 'widget.accounting_indicators_bottom_archive_routes',
    gridStore: 'accounting.indicators.bottom.archive.Routes',
    gridStateId: 'stateAccountingIndicatorsBottomArchiveRoutes'
});