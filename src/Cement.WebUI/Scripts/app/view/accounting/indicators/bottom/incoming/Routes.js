Ext.define('Cement.view.accounting.indicators.bottom.incoming.Routes', {
    extend: 'Cement.view.products.price.bottom.NewRoutes',
    alias: 'widget.accounting_indicators_bottom_incoming_routes',
    gridStore: 'accounting.indicators.bottom.incoming.Routes',
    gridStateId: 'stateAccountingIndicatorsBottomIncomingRoutes'
});