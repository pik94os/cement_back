Ext.define('Cement.view.accounting.indicators.bottom.outcoming.Routes', {
    extend: 'Cement.view.products.price.bottom.NewRoutes',
    alias: 'widget.accounting_indicators_bottom_outcoming_routes',
    gridStore: 'accounting.indicators.bottom.outcoming.Routes',
    gridStateId: 'stateAccountingIndicatorsBottomOutcomingRoutes'
});