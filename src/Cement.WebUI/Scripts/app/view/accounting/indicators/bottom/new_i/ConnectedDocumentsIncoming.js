Ext.define('Cement.view.accounting.indicators.bottom.new_i.ConnectedDocumentsIncoming', {
	extend: 'Cement.view.goods.auto.lists.ConnectedDocuments',
	alias: 'widget.accounting_indicators_bottom_new_connected_documents_incoming',

	gridStore: 'accounting.indicators.bottom.new_i.ConnectedDocumentsIncoming',
	gridStateId: 'stateAccountingIndicatorsBottomNewConnectedDocumentsIncoming',

    printUrl: Cement.Config.url.accounting.indicators.documents.printGrid,
    helpUrl: Cement.Config.url.accounting.indicators.documents.help
});