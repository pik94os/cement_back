Ext.define('Cement.view.accounting.indicators.Total', {
	extend: 'Cement.view.basic.Tabs',
	title: 'Индикаторы',

	items: [{
		xtype: 'accounting_indicators_complex_new',
		title: 'Новые'
	}, {
		xtype: 'accounting_indicators_complex_incoming',
		title: 'Входящие'
	}, {
		xtype: 'accounting_indicators_complex_outcoming',
		title: 'Исходящие'
	}, {
		xtype: 'accounting_indicators_complex_archive',
		title: 'Архив'
	}
	],

	initComponent: function () {
		Ext.apply(this, {
			title: 'Индикаторы'
		});
		this.callParent(arguments);
	}
});