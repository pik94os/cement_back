Ext.define('Cement.view.accounting.documents.complex.ToForm', {
  extend: 'Cement.view.basic.Complex',
  alias: 'widget.accounting_documents_complex_to_form',
    border: 0,
    enableDetail: false,
    autoLoadStore: false,
    priceId: null,
    bodyPadding: 0,
    padding: 0,
    header: false,
    title: null,
    rowsCount: 3,
    title: 'На формирование',
    tabTitle: 'На формирование',
    cls: 'no-side-borders',
    topXType: 'accounting_documents_lists_to_form',
    topDetailType: 'Cement.view.accounting.documents.view.View',
    bottomTitle: 'Связанные документы',
    bottomTabs: [{
        title: 'Связанные документы',
        xtype: 'accounting_documents_bottom_to_form_connected_documents_incoming',
        detailType: 'Cement.view.goods.auto.view.ConnectedDocument',
        constantFilterParam: 'p_base_doc',
        childrenConstantFilterParam: 'p_base_doc',
        border: 0
    }],
    subBottomTitle: 'Справочник',
    subBottomTabs: [{
        title: 'Маршрут',
        shownTitle: 'Маршрут',
        xtype: 'accounting_documents_bottom_to_form_routes',
        detailType: 'Cement.view.products.price.RouteView'
    }]
});