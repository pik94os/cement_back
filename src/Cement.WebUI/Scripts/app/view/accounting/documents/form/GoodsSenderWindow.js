Ext.define('Cement.view.accounting.documents.form.GoodsSenderWindow', {
  extend: 'Cement.view.goods.auto.form.SupplierWindow',
  xtype: 'accounting_documents_form_goods_sender_window',
  title: 'Выбор грузоотправителя',
  leftFrameTitle: 'Грузоотправитель',
  topGridTitle: 'Выбрать грузоотправителя',
  bottomGridTitle: 'Выбранный грузоотправитель',
  structure_storeId: 'accounting.documents.auxiliary.GoodsSenders',
  storeFields: ['id', 'p_name', 'p_address', 'p_inn']
});