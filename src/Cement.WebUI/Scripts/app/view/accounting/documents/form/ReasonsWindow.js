Ext.define('Cement.view.accounting.documents.form.ReasonsWindow', {
  extend: 'Cement.view.goods.plans.form.ConnectedDocumentsWindow',
  xtype: 'accounting_documents_form_reasons_window',
  structure_storeId: 'accounting.documents.auxiliary.Reasons',
  title: 'Документ',
  leftFrameTitle: 'Документы',
  topGridTitle: 'Выбрать документ',
  bottomGridTitle: 'Выбранные документы'
});