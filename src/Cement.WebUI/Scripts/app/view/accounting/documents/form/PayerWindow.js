Ext.define('Cement.view.accounting.documents.form.PayerWindow', {
  extend: 'Cement.view.goods.auto.form.SupplierWindow',
  xtype: 'accounting_documents_form_payer_window',
  title: 'Выбор плательщика',
  leftFrameTitle: 'Плательщик',
  topGridTitle: 'Выбрать плательщика',
  bottomGridTitle: 'Выбранный плательщик',
  structure_storeId: 'accounting.documents.auxiliary.Payers',
  storeFields: ['id', 'p_name', 'p_address', 'p_inn']
});