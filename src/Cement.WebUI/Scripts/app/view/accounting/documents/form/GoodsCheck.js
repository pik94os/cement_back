Ext.define('Cement.view.accounting.documents.form.GoodsCheck', {
  extend: 'Cement.view.accounting.documents.form.Bill',
  xtype: 'accounting_documents_form_goods_check',
  fieldsDisabled: false,
  bodyPadding: '20px 0 0 0',
  url: Cement.Config.url.accounting.documents.save.goods_check
});