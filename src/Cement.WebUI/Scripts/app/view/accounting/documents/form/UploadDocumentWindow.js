Ext.define('Cement.view.accounting.documents.form.UploadDocumentWindow', {
  extend: 'Ext.window.Window',
  modal: true,
  width: 500,
  height: 100,
  resizable: false,
  modal: true,
  title: 'Загрузить документ',
  submitUrl: Cement.Config.url.accounting.documents.save.upload,

  closeWindow: function(btn) {
    btn.up('window').close();
  },

  submitForm: function (btn) {
    var win = btn.up('window');
    win.down('form').submit({
      success: function (form, action) {
        win.close();
        Cement.Msg.info('Успешно загружено');
      },
      failure: function () {
        Cement.Msg.error('Ошибка при сохранении информации');
      }
    });
  },

  initComponent: function () {
    Ext.apply(this, {
        bbar: [{
          xtype: 'button',
          text: 'Загрузить',
          iconCls: 'icon-save-item',
          handler: this.submitForm
        }, {
          xtype: 'button',
          text: 'Отмена',
          iconCls: 'icon-cancel',
          handler: this.closeWindow
        }],
        items: [{
          xtype: 'form',
          url: this.submitUrl,
          bodyPadding: '10',
          border: 0,
          items: [{
            name: 'p_document',
            fieldLabel: 'Выберите документ',
            labelWidth: 150,
            xtype: 'filefield',
            anchor: '100%',
            buttonText: 'Загрузить'
          }]
        }]
    });
    this.callParent(arguments);
  }
});