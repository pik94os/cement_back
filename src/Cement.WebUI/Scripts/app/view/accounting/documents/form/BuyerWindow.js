Ext.define('Cement.view.accounting.documents.form.BuyerWindow', {
  extend: 'Cement.view.goods.auto.form.SupplierWindow',
  xtype: 'accounting_documents_form_buyer_window',
  title: 'Выбор покупателя',
  leftFrameTitle: 'Покупатель',
  topGridTitle: 'Выбрать покупателя',
  bottomGridTitle: 'Выбранный покупатель',
  structure_storeId: 'accounting.documents.auxiliary.Buyers',
  storeFields: ['id', 'p_name', 'p_address', 'p_inn']
});