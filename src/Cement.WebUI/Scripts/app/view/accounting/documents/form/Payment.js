Ext.define('Cement.view.accounting.documents.form.Payment', {
  extend: 'Cement.view.basic.Form',
  xtype: 'accounting_documents_form_payment',
  fieldsDisabled: false,
  url: Cement.Config.url.accounting.documents.save.payment,

  getItems: function () {
    return [{
      xtype: 'panel',
      border: 0,
      layout: 'anchor',
      anchor: '100%',
      bodyPadding: '0 140 0 120',
      defaults: {
        anchor: '100%',
        labelAlign: 'right',
        labelWidth: 150
      },
      items: [{
        xtype: 'textfield',
        fieldLabel: 'Номер',
        name: 'p_number'
      }, {
        xtype: 'datefield',
        fieldLabel: 'Дата',
        name: 'p_date'
      }]
    }, {
      xtype: 'fieldset',
      title: 'Плательщик',
      collapsible: true,
      collapsed: false,
      bodyPadding: 0,
      defaults: {
        labelAlign: 'right',
        labelWidth: 200,
        xtype: 'textfield',
        anchor: '100%'
      },
      items: [
        this.getSelectorField('Наименование', 'p_payer', 'show_payer_window'),
        {
          name: 'p_payer_address',
          fieldLabel: 'Адрес',
          disabled: true
        },
        {
          name: 'p_payer_inn',
          fieldLabel: 'ИНН/КПП',
          disabled: true
        },
        this.getSelectorField('Банк', 'p_payer_bank', 'show_payer_banks_window')
      ]
    }, {
      xtype: 'fieldset',
      title: 'Получатель',
      collapsible: true,
      collapsed: false,
      bodyPadding: 0,
      defaults: {
        labelAlign: 'right',
        labelWidth: 200,
        xtype: 'textfield',
        anchor: '100%'
      },
      items: [
        this.getSelectorField('Наименование', 'p_getter', 'show_getter_window'),
        {
          name: 'p_getter_address',
          fieldLabel: 'Адрес',
          disabled: true
        },
        {
          name: 'p_getter_inn',
          fieldLabel: 'ИНН/КПП',
          disabled: true
        },
        this.getSelectorField('Банк', 'p_getter_bank', 'show_getter_banks_window')
      ]
    }, {
      xtype: 'fieldset',
      title: 'Дополнительные данные',
      collapsible: true,
      collapsed: false,
      bodyPadding: 0,
      defaults: {
        labelAlign: 'right',
        labelWidth: 200,
        xtype: 'textfield',
        anchor: '100%'
      },
      items: [
        this.getCombo('Вид платежа', 'p_payment_kind', 'accounting.auxiliary.PaymentKind'),
        {
          name: 'p_payment_status',
          fieldLabel: 'Статус',
          xtype: 'numberfield'
        },
        {
          name: 'p_payment_pay_kind' ,
          fieldLabel: 'Вид оплаты',
          xtype: 'numberfield'
        },
        {
          name: 'p_payment_queue',
          fieldLabel: 'Очередность',
          xtype: 'numberfield'
        },
        {
          name: 'p_payment_code',
          fieldLabel: 'Код',
          xtype: 'numberfield'
        },
        {
          name: 'p_payment_identifier',
          fieldLabel: 'Идентификатор платежа'
        },
        {
          name: 'p_sum',
          fieldLabel: 'Сумма'
        },
        this.getCombo('НДС', 'p_nds', 'accounting.auxiliary.NDS'),
        {
          name: 'p_nds_sum',
          fieldLabel: 'Сумма НДС'
        },
        {
          name: 'p_pay_purpose',
          fieldLabel: 'Назначение платежа',
          xtype: 'textarea'
        }
      ]
    }];
  },

  initComponent: function () {
    this.callParent(arguments);

    this.down('button[action=show_payer_window]').on('click', function () {
      this.createPayerWindow();
      this.payerWindow.show();
    }, this);

    this.down('button[action=show_getter_window]').on('click', function () {
      this.createGetterWindow();
      this.getterWindow.show();
    }, this);
  },


  createPayerWindow: function () {
    var me = this;
    if (!this.payerWindow) {
      this.payerWindow = Ext.create('Cement.view.accounting.documents.form.PayerWindow');
      this.payerWindow.on('selected', function (rec) {
        me.down('textfield[name=p_payer_address]').setValue(rec.get('p_address'));
        me.down('textfield[name=p_payer_inn]').setValue(rec.get('p_inn'));
        me.down('textfield[name=p_payer_display]').setValue(rec.get('p_name'));
        me.down('hidden[name=p_payer]').setValue(rec.get('id'));
        me.payerWindow.hide();
      }, this);
    }
  },

  createGetterWindow: function () {
    var me = this;
    if (!this.getterWindow) {
      this.getterWindow = Ext.create('Cement.view.accounting.documents.form.GetterWindow');
      this.getterWindow.on('selected', function (rec) {
        me.down('textfield[name=p_getter_address]').setValue(rec.get('p_address'));
        me.down('textfield[name=p_getter_inn]').setValue(rec.get('p_inn'));
        me.down('textfield[name=p_getter_display]').setValue(rec.get('p_name'));
        me.down('hidden[name=p_getter]').setValue(rec.get('id'));
        me.getterWindow.hide();
      }, this);
    }
  },

  afterRecordLoad: function (rec) {
    this.createPayerWindow();
    this.createGetterWindow();
  }
});