Ext.define('Cement.view.accounting.documents.form.Act', {
  extend: 'Cement.view.basic.Form',
  xtype: 'accounting_documents_form_act',
  fieldsDisabled: false,
  bodyPadding: '20px 0 0 0',
  url: Cement.Config.url.accounting.documents.save.act,

  getItems: function () {
    var trigger = Ext.create('Ext.ux.CustomTrigger'),
      me = this;
    trigger.onTriggerClick = function(t) {
      var win = Ext.create('Cement.view.goods.auto.form.ProductWindow');
      win.on('productselected', function (rec) {
        trigger.setValue(rec.get('p_name'));
        var sel = me.down('grid').getSelectionModel().getSelection();
        if (sel.length) {
          sel[0].set('p_product_id', rec.get('id'));
          sel[0].set('p_name', rec.get('p_name'));
          sel[0].set('p_unit', rec.get('p_product_unit_display'));
          sel[0].set('p_price', rec.get('p_price'));
          sel[0].set('p_nds', rec.get('p_tax'));
        }
        win.close();
      }, this);
      win.show();
    };
    return [{
      xtype: 'panel',
      border: 0,
      layout: 'anchor',
      anchor: '100%',
      bodyPadding: '0 140 0 120',
      defaults: {
        anchor: '100%',
        labelAlign: 'right',
        labelWidth: 200
      },
      items: [{
        xtype: 'textfield',
        fieldLabel: 'Номер',
        name: 'p_number'
      }, {
        xtype: 'datefield',
        fieldLabel: 'Дата',
        name: 'p_date'
      },
      this.getSelectorField('Заполнить по:', 'p_fill', 'show_fill_window')
      ]
    }, {
      xtype: 'fieldset',
      title: 'Покупатель',
      collapsible: true,
      collapsed: false,
      margin: '0 80',
      defaults: {
        labelAlign: 'right',
        labelWidth: 200,
        xtype: 'textfield',
        anchor: '100%'
      },
      items: [
        this.getSelectorField('Наименование', 'p_buyer', 'show_buyer_window'),
        {
          name: 'p_buyer_address',
          fieldLabel: 'Адрес',
          disabled: true
        },
        {
          name: 'p_buyer_inn',
          fieldLabel: 'ИНН/КПП',
          disabled: true
        }
      ]
    }, {
      xtype: 'fieldset',
      title: 'Дополнительные данные',
      collapsible: true,
      collapsed: false,
      margin: '0 80',
      defaults: {
        labelAlign: 'right',
        labelWidth: 200,
        xtype: 'textfield',
        anchor: '100%'
      },
      items: [
        this.getSelectorField('Основание', 'p_reason', 'show_reason_window')
      ]
    }, {
      xtype: 'fieldset',
      title: 'Товары/услуги',
      collapsible: true,
      collapsed: false,
      margin: '0 80 10 80',
      defaults: {
        labelAlign: 'right',
        labelWidth: 200,
        xtype: 'textfield',
        anchor: '100%'
      },
      items: [{
        xtype: 'hidden',
        name: 'p_items'
      }]
    }, {
      xtype: 'grid',
      cls: 'no-side-borders no-side-borders-in-header',
      tbar: [{
        xtype: 'button',
        iconCls: 'icon-add-item',
        text: 'Добавить',
        handler: this.appendItem
      }],
      height: 400,
      store: Ext.create('Ext.data.Store', {
        fields: [ 'id', 'p_name', 'p_unit', 'p_count', 'p_price', 'p_nds', 'p_sum', 'p_product_id' ]
      }),
      plugins: [
        Ext.create('Ext.grid.plugin.CellEditing', {
          clicksToEdit: 1
        })
      ],
      columns: [
        { dataIndex: 'p_name', text: 'Наименование', flex: 4, editor: trigger },
        { dataIndex: 'p_unit', text: 'Ед. изм.', flex: 1 },
        { dataIndex: 'p_count', text: 'Кол-во', flex: 1, editor: 'numberfield' },
        { dataIndex: 'p_price', text: 'Цена', flex: 1 },
        { dataIndex: 'p_nds', text: 'НДС', flex: 1 },
        { dataIndex: 'p_sum', text: 'Сумма', flex: 1 },
        {
          xtype: 'rowactions',
          hideable: false,
          resizeable: false,
          width: 23,
          actions: [
              {
                  iconCls: 'icon-remove-item',
                  qtip: 'Удалить',
                  callback: me.removeItem
              }
          ],
          keepSelection: true
        }
      ]
    }];
  },

  removeItem: function (grid, record) {
    var me = this;
    Ext.Msg.confirm('Внимание', 'Вы действительно хотите удалить элемент?', function (btn) {
      if (btn == 'yes') {
        grid.getStore().remove(record);
      }
    });
  },

  appendItem: function (btn) {
    var grid = btn.up('grid');
    grid.getStore().add({
      p_count: 1
    });
  },

  initComponent: function () {
    this.callParent(arguments);

    this.down('button[action=show_buyer_window]').on('click', function () {
      this.createBuyerWindow();
      this.buyerWindow.show();
    }, this);

    this.down('button[action=show_reason_window]').on('click', function () {
      this.createReasonWindow();
      this.reasonWindow.show();
    }, this);

    this.down('button[action=show_fill_window]').on('click', function () {
      this.createFillWindow();
      this.fillWindow.show();
    }, this);
  },

  createFillWindow: function () {
    var me = this;
    if (!this.fillWindow) {
      this.fillWindow = Ext.create('Cement.view.accounting.documents.form.FillByWindow');
      this.fillWindow.on('docselected', function (rec) {
        me.getForm().loadRecord(rec);
        me.down('textfield[name=p_fill_display]').setValue(rec.get('p_name'));
        me.fillWindow.hide();
      }, this);
    }
  },

  createBuyerWindow: function () {
    var me = this;
    if (!this.buyerWindow) {
      this.buyerWindow = Ext.create('Cement.view.accounting.documents.form.BuyerWindow');
      this.buyerWindow.on('selected', function (rec) {
        me.down('textfield[name=p_buyer_address]').setValue(rec.get('p_address'));
        me.down('textfield[name=p_buyer_inn]').setValue(rec.get('p_inn'));
        me.down('textfield[name=p_buyer_display]').setValue(rec.get('p_name'));
        me.down('hidden[name=p_buyer]').setValue(rec.get('id'));
        me.buyerWindow.hide();
      }, this);
    }
  },

  createReasonWindow: function () {
    var me = this;
    if (!this.reasonWindow) {
      this.reasonWindow = Ext.create('Cement.view.accounting.documents.form.ReasonsWindow');
      this.reasonWindow.on('docselected', function (name, ids) {
        me.down('textfield[name=p_reason_display]').setValue(name);
        me.down('hidden[name=p_reason]').setValue(Ext.JSON.encode(ids));
        me.reasonWindow.hide();
      }, this);
    }
  },

  afterRecordLoad: function (rec) {
    this.createBuyerWindow();
    this.createReasonWindow();
    this.createFillWindow();
  }
});