Ext.define('Cement.view.accounting.documents.form.FillByWindow', {
  extend: 'Cement.view.goods.plans.form.ConnectedDocumentsWindow',
  xtype: 'accounting_documents_form_fill_by_window',
  structure_storeId: 'accounting.documents.auxiliary.FillByDocuments',
  title: 'Документ',
  leftFrameTitle: 'Документы',
  topGridTitle: 'Выбрать документ',
  bottomGridTitle: 'Выбранные документы',
  singleSelection: true,

  sign: function () {
    if (this.dstStore.getCount() > 0) {
      this.fireEvent('docselected', this.dstStore.getAt(0));
    }
  }
});