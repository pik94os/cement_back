Ext.define('Cement.view.accounting.documents.form.GetterWindow', {
  extend: 'Cement.view.goods.auto.form.SupplierWindow',
  xtype: 'accounting_documents_form_getter_window',
  title: 'Выбор плательщика',
  leftFrameTitle: 'Плательщик',
  topGridTitle: 'Выбрать плательщика',
  bottomGridTitle: 'Выбранный плательщик',
  structure_storeId: 'accounting.documents.auxiliary.Getters',
  storeFields: ['id', 'p_name', 'p_address', 'p_inn']
});