Ext.define('Cement.view.accounting.documents.form.EmployeeWindow', {
  extend: 'Cement.view.personal.adverts.form.EmployeeWindow',
  xtype: 'widget.accounting_documents_form_employee_window',
  structure_storeId: 'accounting.documents.auxiliary.Employees'
});