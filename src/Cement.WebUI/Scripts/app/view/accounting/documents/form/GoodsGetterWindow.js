Ext.define('Cement.view.accounting.documents.form.GoodsGetterWindow', {
  extend: 'Cement.view.goods.auto.form.SupplierWindow',
  xtype: 'accounting_documents_form_goods_getter_window',
  title: 'Выбор грузополучателя',
  leftFrameTitle: 'Грузополучатель',
  topGridTitle: 'Выбрать грузополучателя',
  bottomGridTitle: 'Выбранный грузополучатель',
  structure_storeId: 'accounting.documents.auxiliary.GoodsGetters',
  storeFields: ['id', 'p_name', 'p_address', 'p_inn']
});