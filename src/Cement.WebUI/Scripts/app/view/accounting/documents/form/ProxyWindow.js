Ext.define('Cement.view.accounting.documents.form.ProxyWindow', {
  extend: 'Cement.view.goods.plans.form.ConnectedDocumentsWindow',
  xtype: 'accounting_documents_form_proxy_window',
  structure_storeId: 'accounting.documents.auxiliary.Proxis',
  title: 'Выбор доверенности'
});