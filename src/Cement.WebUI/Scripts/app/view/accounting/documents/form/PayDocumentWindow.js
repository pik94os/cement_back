Ext.define('Cement.view.accounting.documents.form.PayDocumentWindow', {
  extend: 'Cement.view.goods.plans.form.ConnectedDocumentsWindow',
  xtype: 'accounting_documents_form_pay_documents_window',
  structure_storeId: 'accounting.documents.auxiliary.PayDocuments',
  title: 'Документ',
  leftFrameTitle: 'Документы',
  topGridTitle: 'Выбрать документ',
  bottomGridTitle: 'Выбранные документы'
});