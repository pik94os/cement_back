Ext.define('Cement.view.accounting.documents.ToolbarMixin', {
    filterStoreId: 'accounting.auxiliary.DocumentKinds',
    filterParamName: 'p_document_kinds',

    buildAdditionalButtons: function () {
        var store = Ext.getStore(this.filterStoreId),
            items = [{
                text: 'Выделить все',
                checked: true,
                checkHandler: this.filterDocumentKindToggle
            }, '-'],
            me = this;
        store.each(function (item) {
            items.push({
                checked: true,
                checkHandler: me.filterDocumentKind,
                kindId: item.get('id'),
                text: item.get('p_name'),
                kind: 'doc_type_selector'
            });
        });
        return items;
    },

    filterDocumentKindToggle: function (item, checked) {
        var toolbar = item.up('toolbar'),
            grid = toolbar.up('basicgrid'),
            items = toolbar.query('menu menucheckitem[kind=doc_type_selector]');
        Ext.each(items, function (it) {
            it.setChecked(item.checked, true);
        });
        grid.filterDocumentKind(item);
    },

    filterDocumentKind: function (item, checked) {
        var toolbar = item.up('toolbar'),
            grid = toolbar.up('basicgrid'),
            items = toolbar.query('menu menucheckitem[kind=doc_type_selector]'),
            params = [];
        Ext.each(items, function (it) {
            if (it.checked === true) {
                if (it.kindId) {
                    if (params.indexOf(it.kindId) < 0) {
                        params.push(it.kindId);
                    }
                }
            }
        });
        grid.constantFilter = [{
            property: grid.filterParamName,
            value: params
        }];
        grid.reloadGrid();
    }
});