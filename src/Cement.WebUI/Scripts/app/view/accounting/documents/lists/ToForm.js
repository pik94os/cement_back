Ext.define('Cement.view.accounting.documents.lists.ToForm', {
  extend: 'Cement.view.basic.Grid',
  alias: 'widget.accounting_documents_lists_to_form',
    autoLoadStore: true,

    bbarText: 'Показаны документы {0} - {1} из {2}',
    bbarEmptyMsg: 'Нет документов',
    bbarUsersText: 'Документов на странице: ',

    gridStore: 'accounting.documents.ToForm',
    gridStateId: 'stateAccountingDocumentsToForm',

    printUrl: Cement.Config.url.accounting.documents.to_form.printGrid,
    helpUrl: Cement.Config.url.accounting.documents.to_form.help,
    deleteUrl: Cement.Config.url.accounting.documents.to_form.deleteUrl,
    formUrl: Cement.Config.url.accounting.documents.to_form.formUrl,

    shownTitle: null,

    mixins: [
        'Cement.view.accounting.documents.ToolbarMixin'
    ],

    createItem: function () {
        var ids = this.getSelectedIds(),
            me = this;
        if (ids.length) {
            Ext.Ajax.request({
                url: this.formUrl,
                method: 'GET',
                params: {
                    p_ids: ids
                },
                success: function (response) {
                    var data = Ext.JSON.decode(response.responseText);
                    if (data.p_success) {
                        var record = Ext.create('Cement.model.accounting.Document', data.p_record);
                        me.fireEvent('edititem', record, me);
                    }
                    else {
                      Cement.Msg.error('Ошибка при формировании');  
                    }
                },
                failure: function (response) {
                    Cement.Msg.error('Ошибка при формировании');
                }
            })
        }
    },

    getAdditionalButtons: function () {
        return [{
            xtype: 'button',
            text: 'Выбор документов',
            iconCls: 'icon-select-document',
            menu: {
                xtype: 'menu',
                plain: true,
                items: this.buildAdditionalButtons()
            }
        }];
    },

    getSelectedIds: function () {
        var store = this.down('grid').getStore(),
            result = [];
        store.each(function (item) {
            if (item.get('p_selected')) {
                result.push(item.get('id'));
            }
        });
        return result;
    },

    selectionChanged: function (self, rowIndex, checked) {
        var store = self.up('grid').getStore(),
            cmp = self.up('basicgrid'),
            hasChecked = false,
            ids = cmp.getSelectedIds();
        if (cmp.disableSelectionReload) {
            return;
        }
        cmp.selectedIds = ids;
        store.each(function (item) {
            if (item.get('p_selected')) {
                hasChecked = true;
            }
        });
        store.clearFilter(true);
        cmp.disableSelectionReload = true;
        store.filter({
            property: 'p_selected',
            value: ids
        });
        // Ext.each(self.up('basicgrid').query('button[role=group_control]'), function (ctl) {
        //     ctl.setDisabled(!hasChecked);
        // });
    },

    getActionColumns: function () {
        return {
            xtype: 'checkcolumn',
            text: '',
            dataIndex: 'p_selected',
            width: 25,
            locked: true,
            listeners: {
                checkchange: this.selectionChanged
            }
        };
    },

    getFilterItems: function () {
        return [{
            text: '№П/п',
            kind: 'selector',
            field_name: 'p_number',
            checked: true
        }, {
            text: 'Дата',
            kind: 'selector',
            field_name: 'p_date',
            checked: true
        }, {
            text: 'Вид',
            kind: 'selector',
            field_name: 'p_kind',
            checked: true
        }, {
            text: 'Статус',
            kind: 'selector',
            field_name: 'p_status',
            checked: true
        }, {
            text: 'Наименование',
            kind: 'selector',
            field_name: 'p_name',
            checked: true
        }];
    },

    getGridColumns: function () {
        var result = [
            { text: '№П/п', dataIndex: 'p_number', width: 90, locked: true },
            { text: 'Дата', dataIndex: 'p_date', width: 90, locked: true },
            { text: 'Вид', dataIndex: 'p_kind', width: 90, locked: true },
            { text: 'Статус', dataIndex: 'p_status', width: 90 },
            { text: 'Наименование', dataIndex: 'p_name', width: 960 }
        ];
        return this.mergeActions(result);
    },

    initComponent: function () {
        Ext.apply(this, {
            title: this.shownTitle
        });
        this.callParent(arguments);
        var store = Ext.getStore(this.gridStore);
        store.on('load', function () {
            this.disableSelectionReload = true;
            Ext.each(this.selectedIds, function (item) {
                var it = store.getByID(item);
                if (it) {
                    it.set('p_selected', true);
                }
            });
        }, this);
    }
});