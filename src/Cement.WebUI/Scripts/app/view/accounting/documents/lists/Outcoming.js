Ext.define('Cement.view.accounting.documents.lists.Outcoming', {
  extend: 'Cement.view.accounting.documents.lists.New',
  alias: 'widget.accounting_documents_lists_outcoming',

    gridStore: 'accounting.documents.Outcoming',
    gridStateId: 'stateAccountingDocumentsOutcoming',

    printUrl: Cement.Config.url.accounting.documents.outcoming.printGrid,
    helpUrl: Cement.Config.url.accounting.documents.outcoming.help,
    deleteUrl: Cement.Config.url.accounting.documents.outcoming.deleteUrl,

    shownTitle: null,

    getActionColumns: function () {
        return {
            xtype: 'rowactions',
            hideable: false,
            resizeable: false,
            locked: true,
            width: 46,
            actions: [
                {
                    iconCls: 'icon-edit-item',
                    qtip: 'Редактировать',
                    callback: this.editItem
                },
                {
                    iconCls: 'icon-delete-item',
                    qtip: 'Удалить',
                    callback: this.deleteItem
                }
            ],
            keepSelection: true
        };
    }
});