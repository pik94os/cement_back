Ext.define('Cement.view.accounting.documents.lists.Incoming', {
  extend: 'Cement.view.accounting.documents.lists.New',
  alias: 'widget.accounting_documents_lists_incoming',

    gridStore: 'accounting.documents.Incoming',
    gridStateId: 'stateAccountingDocumentsIncoming',

    printUrl: Cement.Config.url.accounting.documents.incoming.printGrid,
    helpUrl: Cement.Config.url.accounting.documents.incoming.help,
    signItemUrl: Cement.Config.url.accounting.documents.incoming.signUrl,

    shownTitle: null,

    getActionColumns: function () {
        return null;
    }
});