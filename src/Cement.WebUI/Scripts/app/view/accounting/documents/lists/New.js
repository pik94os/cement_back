Ext.define('Cement.view.accounting.documents.lists.New', {
  extend: 'Cement.view.basic.Grid',
  alias: 'widget.accounting_documents_lists_new',
    autoLoadStore: true,

    bbarText: 'Показаны документы {0} - {1} из {2}',
    bbarEmptyMsg: 'Нет документов',
    bbarUsersText: 'Документов на странице: ',

    gridStore: 'accounting.documents.New',
    gridStateId: 'stateAccountingDocumentsNew',

    printUrl: Cement.Config.url.accounting.documents.new_d.printGrid,
    helpUrl: Cement.Config.url.accounting.documents.new_d.help,
    deleteUrl: Cement.Config.url.accounting.documents.new_d.deleteUrl,

    shownTitle: null,
    
    mixins: [
        'Cement.view.accounting.documents.ToolbarMixin'
    ],

    creatorTree: Ext.clone(Cement.Creators.accounting),
    createWindowTitle: 'Создать',

    getAdditionalButtons: function () {
        return [{
            xtype: 'button',
            text: 'Загрузить',
            iconCls: 'icon-browse',
            handler: this.uploadDocument
        }, {
            xtype: 'button',
            text: 'Выбор документов',
            iconCls: 'icon-select-document',
            menu: {
                xtype: 'menu',
                plain: true,
                items: this.buildAdditionalButtons()
            }
        }];
    },

    getActionColumns: function () {
        return {
            xtype: 'rowactions',
            hideable: false,
            resizeable: false,
            locked: true,
            width: 46,
            actions: [
                {
                    iconCls: 'icon-edit-item',
                    qtip: 'Редактировать',
                    callback: this.editItem
                },
                {
                    iconCls: 'icon-delete-item',
                    qtip: 'Удалить',
                    callback: this.deleteItem
                }
            ],
            keepSelection: true
        };
    },

    getFilterItems: function () {
        return [{
            text: '№П/п',
            kind: 'selector',
            field_name: 'p_number',
            checked: true
        }, {
            text: 'Дата',
            kind: 'selector',
            field_name: 'p_date',
            checked: true
        }, {
            text: 'Наименование',
            kind: 'selector',
            field_name: 'p_name',
            checked: true
        }, {
            text: 'Сумма',
            kind: 'selector',
            field_name: 'p_sum',
            checked: true
        }];
    },

    getGridColumns: function () {
        var result = [
            { text: '№П/п', dataIndex: 'p_number', width: 90, locked: true },
            { text: 'Дата', dataIndex: 'p_date', width: 90, locked: true },
            { text: 'Наименование', dataIndex: 'p_name', width: 960 },
            { text: 'Сумма', dataIndex: 'p_sum', width: 100 }
        ];
        return this.mergeActions(result);
    },

    editItem: function (grid, record) {
        grid.up('basicgrid').fireEvent('edititem', record, grid.up('basicgrid').$className);
    },

    uploadDocument: function () {
        var win = Ext.create('Cement.view.accounting.documents.form.UploadDocumentWindow');
        win.show();
    },

    initComponent: function () {
        Ext.apply(this, {
            title: this.shownTitle
        });
        this.callParent(arguments);
    }
});