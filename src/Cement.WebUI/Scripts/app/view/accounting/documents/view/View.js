Ext.define('Cement.view.accounting.documents.view.View', {
  extend: 'Cement.view.basic.Form',
  alias: 'widget.accounting_documents_view_view',
  fieldsDisabled: true,
  buttons: null,
  border: 0,
  tbar: Cement.Config.modelViewButtons,
  layout: 'autocontainer',
  bbar: null,
  noUserFieldsetsControl: false,
  bodyCls: 'model-form',

  initComponent: function ()  {
    Ext.apply(this, {
      items: [{
        xtype: 'panel',
        border: 0,
        layout: 'anchor',
        defaultType: 'textfield',
        anchor: '100%',
        bodyPadding: '10 10 10 5',
        autoScroll: true,
        defaults: {
          anchor: '100%',
          labelWidth: 110,
          disabled: true
        },
        items: [{
          name: 'p_number',
          fieldLabel: 'Номер'
        }, {
          name: 'p_date',
          fieldLabel: 'Дата'
        }, {
          name: 'p_pay_for',
          fieldLabel: 'Основание платежа'
        }, {
          name: 'p_name',
          fieldLabel: 'Наименование'
        }, {
          name: 'p_who',
          fieldLabel: 'Через кого'
        }, {
          name: 'p_sum',
          fieldLabel: 'Сумма'
        }]
      }]
    });
    this.callParent(arguments);
  }
});