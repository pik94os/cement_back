Ext.define('Cement.view.accounting.documents.Total', {
    extend: 'Cement.view.basic.Tabs',

    initComponent: function () {
        Ext.apply(this, {
            title: 'Документы',
            items: [{
                title: 'На формирование',
                layout: 'fit',
                xtype: 'accounting_documents_complex_to_form',
                detailType: ''
            }, {
                title: 'Новые',
                layout: 'fit',
                xtype: 'accounting_documents_complex_new',
                detailType: ''
            }, {
                title: 'Входящие',
                layout: 'fit',
                xtype: 'accounting_documents_complex_incoming',
                detailType: ''
            }, {
                title: 'Исходящие',
                layout: 'fit',
                xtype: 'accounting_documents_complex_outcoming',
                detailType: ''
            }]
        });
        this.callParent(arguments);
    }
});