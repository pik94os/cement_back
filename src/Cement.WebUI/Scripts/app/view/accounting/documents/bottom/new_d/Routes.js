Ext.define('Cement.view.accounting.documents.bottom.new_d.Routes', {
    extend: 'Cement.view.products.price.bottom.NewRoutes',
    alias: 'widget.accounting_documents_bottom_new_routes',
    gridStore: 'accounting.documents.bottom.new_d.Routes',
    gridStateId: 'stateAccountingDocumentsBottomNewRoutes'
});