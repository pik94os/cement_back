Ext.define('Cement.view.accounting.documents.bottom.to_form.ConnectedDocumentsIncoming', {
	extend: 'Cement.view.goods.auto.lists.ConnectedDocuments',
	alias: 'widget.accounting_documents_bottom_to_form_connected_documents_incoming',

	gridStore: 'accounting.documents.bottom.to_form.ConnectedDocumentsIncoming',
	gridStateId: 'stateAccountingDocumentsBottomToFormConnectedDocumentsIncoming',

    printUrl: Cement.Config.url.accounting.documents.documents.printGrid,
    helpUrl: Cement.Config.url.accounting.documents.documents.help
});