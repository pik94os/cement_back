Ext.define('Cement.view.accounting.documents.bottom.to_form.Routes', {
    extend: 'Cement.view.products.price.bottom.NewRoutes',
    alias: 'widget.accounting_documents_bottom_to_form_routes',
    gridStore: 'accounting.documents.bottom.to_form.Routes',
    gridStateId: 'stateAccountingDocumentsBottomToFormRoutes'
});