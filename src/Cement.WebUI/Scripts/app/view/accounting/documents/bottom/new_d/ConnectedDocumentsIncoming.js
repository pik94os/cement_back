Ext.define('Cement.view.accounting.documents.bottom.new_d.ConnectedDocumentsIncoming', {
	extend: 'Cement.view.goods.auto.lists.ConnectedDocuments',
	alias: 'widget.accounting_documents_bottom_new_connected_documents_incoming',

	gridStore: 'accounting.documents.bottom.new_d.ConnectedDocumentsIncoming',
	gridStateId: 'stateAccountingDocumentsBottomNewConnectedDocumentsIncoming',

    printUrl: Cement.Config.url.accounting.documents.documents.printGrid,
    helpUrl: Cement.Config.url.accounting.documents.documents.help
});