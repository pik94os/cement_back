Ext.define('Cement.view.accounting.documents.bottom.outcoming.Routes', {
    extend: 'Cement.view.products.price.bottom.NewRoutes',
    alias: 'widget.accounting_documents_bottom_outcoming_routes',
    gridStore: 'accounting.documents.bottom.outcoming.Routes',
    gridStateId: 'stateAccountingDocumentsBottomOutcomingRoutes'
});