Ext.define('Cement.view.accounting.documents.bottom.incoming.ConnectedDocumentsIncoming', {
	extend: 'Cement.view.goods.auto.lists.ConnectedDocuments',
	alias: 'widget.accounting_documents_bottom_incoming_connected_documents_incoming',

	gridStore: 'accounting.documents.bottom.incoming.ConnectedDocumentsIncoming',
	gridStateId: 'stateAccountingDocumentsBottomIncomingConnectedDocumentsIncoming',

    printUrl: Cement.Config.url.accounting.documents.documents.printGrid,
    helpUrl: Cement.Config.url.accounting.documents.documents.help
});