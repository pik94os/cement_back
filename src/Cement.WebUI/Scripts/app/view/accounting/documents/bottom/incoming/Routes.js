Ext.define('Cement.view.accounting.documents.bottom.incoming.Routes', {
    extend: 'Cement.view.products.price.bottom.NewRoutes',
    alias: 'widget.accounting_documents_bottom_incoming_routes',
    gridStore: 'accounting.documents.bottom.incoming.Routes',
    gridStateId: 'stateAccountingDocumentsBottomIncomingRoutes'
});