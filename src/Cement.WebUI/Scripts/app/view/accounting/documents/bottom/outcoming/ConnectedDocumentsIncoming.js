Ext.define('Cement.view.accounting.documents.bottom.outcoming.ConnectedDocumentsIncoming', {
	extend: 'Cement.view.goods.auto.lists.ConnectedDocuments',
	alias: 'widget.accounting_documents_bottom_outcoming_connected_documents_incoming',

	gridStore: 'accounting.documents.bottom.outcoming.ConnectedDocumentsIncoming',
	gridStateId: 'stateAccountingDocumentsBottomOutcomingConnectedDocumentsIncoming',

    printUrl: Cement.Config.url.accounting.documents.documents.printGrid,
    helpUrl: Cement.Config.url.accounting.documents.documents.help
});