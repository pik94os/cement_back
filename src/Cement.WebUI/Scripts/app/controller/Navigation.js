Ext.define('Cement.controller.Navigation', {
    extend: 'Ext.app.Controller',

    refs: [
        {
            ref: 'navigationControl',
            selector: 'navigation'
        },
        {
            ref: 'tabControl',
            selector: '#master_region'
        },
        {
            ref: 'detailControl',
            selector: '#detail_region'
        },
        {
            ref: 'masterArea',
            selector: '#master_region'
        }
    ],

    onLaunch: function () {
        var me = this,
            navControl = this.getNavigationControl();
        Ext.state.Manager.setProvider(Ext.create('Ext.state.LocalStorageProvider'));
        Ext.Ajax.request({
            url: Cement.Config.url.interface_navigation,
            method: 'POST',
            success: function (response) {
                var json = Ext.JSON.decode(response.responseText);
                Ext.Array.each(json, function (item) {
                    var panel = Ext.create('Ext.panel.Panel', {
                        title: item.groupName,
                        layout: 'anchor',
                        cls: 'nav-btn-group'
                    });
                    Ext.Array.each(item.items, function (btn) {
                        var button = Ext.create('Ext.button.Button', {
                            text: btn.text,
                            anchor: '100%',
                            margin: 5,
                            action: btn.action,
                            textAlign: 'left',
                            data: {
                                master: btn.master,
                                detail: btn.detail,
                                tabs: btn.tabs,
                                text: btn.text,
                                accessType: btn.accessType
                            },
                            listeners: {
                                click: {
                                    scope: me,
                                    fn: me.navHandler
                                }
                            }
                        });
                        panel.add(button);
                    });
                    navControl.add(panel);
                });
                navControl.doLayout();
            }
        });

    },

    init: function (application) {
        this.control({
            '#master_region totalcreator': {
                formsaved: this.editFormSaved
            },
            '#master_region': {
                tabchange: this.changeDetail,
                remove: this.tabRemoved
            },
            '#detail_region button[action=print]': {
                click: this.sendPrint
            },
            '#detail_region button[action=feedback]': {
                click: this.showFeedbackWindow
            },
            '#detail_region personal_calendar_view_date_picker': {
                select: this.changeCalendar
            },
            '#detail_region navigation_indicators_date_picker': {
                select: this.changeCalendar
            },
            'feedback_window button[action=feedback_close]': {
                click: this.hideFeedbackWindow
            },
            '#master_region button[action=print]': {
                click: this.sendPrint
            },
            '#master_region basicgrid': {
                edititem: this.editItem,
                createitem: this.createItem
            },
            '#master_region basiccomplex': {
                showDetails: this.showComplexDetails
            },
            '#master_region customtabs': {
                showDetails: this.showComplexDetails,
                tabchange: this.storeReload
            },
            '#master_region navigation_map_map_panel': {
                showDetails: this.showComplexDetails
            },
            '#master_region basicform': {
                showDetails: this.showComplexDetails
            },
            '#master_region grid': {
                selectionchange: this.populateDetail
            },
            'corporate_startpage_dashboard': {
                sysnav: this.navigateTab
            }
        });
        this.existingTabs = [];
        this.complexDetails = [];
    },

    editFormSaved: function (path, navigate) {
        if (navigate) {
            this.navigateTab(path);
        }
        var tab = this.getTabControl().getActiveTab();
        if (tab.getActiveTab) {
            tab = tab.getActiveTab();
        }
        
        // ��� ���� ������ ���� ��� ������ ��� ��� 
        // if (tab) {
        if (tab instanceof Cement.view.common.TotalCreator) {
            var grid = tab.down('grid');
            if (grid) {
                grid.getStore().load();
            }
        }
    },

    changeCalendar: function (dp, dt) {
        this.getTabControl().getActiveTab().setStartDate(dt);
    },

    navigateTab: function (addr) {
        if (addr == null) {
            return;
        }

        var parts = addr.split('#'),
            me = this;
        me.getTabControl().setLoading(true);
        Ext.each(this.getNavigationControl().query('button'), function (btn) {
            if (btn.data.master == parts[0]) {
                me.navHandler(btn);
                if (parts[1]) {
                    if (/tab-(\d+)/.test(parts[1])) {
                        var tb = /tab-(\d+)/.exec(parts[1]);
                        me.getTabControl().getActiveTab().setActiveTab(parseInt(tb[1]));
                    }
                }
            }
        });
        me.getTabControl().setLoading(false);
    },

    tabRemoved: function () {
        var tabs = this.getTabControl(),
            detail = this.getDetailControl(),
            navControl = this.getNavigationControl();
        if (tabs.items.getCount() == 0) {
            detail.items.each(function (item) {
                item.hide();
                item.destroy();
                item = null;
            });
            detail.update('');
            navControl.items.each(function (itm) {
                itm.items.each(function (btn) {
                    btn.removeCls('hover');
                });
            });
        }
    },

    //Create total creator form and append it to tabs
    makeTotalCreator: function () {
        var tabs = this.getTabControl();
        if (!this.totalCreator) {
            this.totalCreator = Ext.create('Cement.view.common.TotalCreator', {
                layout: 'fit',
                closable: true
            });
            tabs.add(this.totalCreator);
        }
        else {
            if (this.totalCreator.isDestroyed) {
                this.totalCreator = Ext.create('Cement.view.common.TotalCreator', {
                    layout: 'fit',
                    closable: true
                });
                tabs.add(this.totalCreator);
            }
        }
        tabs.setActiveTab(this.totalCreator);
        tabs.doLayout();
    },

    storeReload: function () {
        var master = this.getMasterControl();
        this.loadStore(master);
    },

    showComplexDetails: function (detailType, record) {
        var master = this.getMasterControl(),
            detail = this.getDetailControl();
        //this.loadStore(master);
        if (detailType == '') {
            this.clearDetail();
            return;
        }
        //Create container for controls cache if not exists
        if (this.complexDetails[master.xtype] === undefined) {
            this.complexDetails[master.xtype] = {};
        }
        //Get control from cash if exists
        if (this.complexDetails[master.xtype][detailType] === undefined) {
            this.complexDetails[master.xtype][detailType] =
                Ext.create(detailType, {
                    layout: 'fit'
                });
            this.complexDetails[master.xtype].current =  //Save current detail control to restore it after tab change
                this.complexDetails[master.xtype][detailType];
        }
        //Append control to detail Area if neccessary
        if (detail.items.first() != this.complexDetails[master.xtype][detailType]) {
            if (this.complexDetails[master.xtype][detailType]) {
                this.clearDetail();
                detail.add(this.complexDetails[master.xtype][detailType]);
                detail.doLayout();
            }
        }
        //And load record if control exists
        if (this.complexDetails[master.xtype][detailType]) {
            this.complexDetails[master.xtype][detailType].loadRecord(record);
            detail.doLayout();
            detail.down('button[action=print]').enable();
        }
        this.complexDetails[master.xtype][detailType].current = this.complexDetails[master.xtype][detailType];
        // this.existingTabs[master.$className] = this.complexDetails[master.xtype][detailType];
    },

    //Handler to open empty create form
    createItem: function (cls, gridCls, xtype) {
        this.makeTotalCreator();
        this.totalCreator.loadForCreate(cls, gridCls, xtype);
    },

    //Handler to open create form and load record to it
    editItem: function (record, cls) {
        var masterCls = this.getMasterControl().$className,
            me = this;
        this.makeTotalCreator();
        this.totalCreator.loadForEdit(record, masterCls, cls);
    },

    hideFeedbackWindow: function () {
        this.feedbackWindow.close();
    },

    showFeedbackWindow: function (btn) {
        this.feedbackWindow = Ext.create('Cement.view.common.FeedbackWindow');
        this.feedbackWindow.show();
    },

    sendPrint: function (btn) {
        var form = btn.up('form');
        if (form) {
            if (form.getPrintUrl) {
                window.open(form.getPrintUrl());
            }
        }
    },

    populateDetail: function (self, selected, eOpts) {
        var detailControl = this.getDetailControl(),
            masterControl = this.getMasterControl(),
            masterRegion = this.getMasterArea(),
            tabs = this.existingTabs;
        if (selected.length > 0 && masterControl.enableDetail) {
            if (detailControl.down('form')) {
                detailControl.down('form').loadRecord(selected[0]);
            }
            if (detailControl.loadRecord) {
                detailControl.loadRecord(selected[0]);
            }
            if (detailControl.down('button[action=print]')) {
                detailControl.down('button[action=print]').enable();
            }
            detailControl.doLayout();
        }
    },

    getMasterControl: function () {
        return this.getTabControl().getActiveTab();
    },

    createTab: function (data) {
        var tabs = this.getTabControl();
        this.existingTabs[data.master] = {
            master: Ext.create(data.master, {
                title: data.text,
                tabs: data.tabs,
                accessType: data.accessType,
                layout: 'fit',
                closable: true
            }),
            masterCls: data.master
        };
        if (data.detail != '') {
            this.existingTabs[data.master].detail = Ext.create(data.detail, {
                layout: 'fit'
            });
        }
        tabs.add(this.existingTabs[data.master].master);
        // tabs.doLayout();
    },

    navHandler: function (btn) {
        var tabs = this.getTabControl(),
            detail = this.getDetailControl(),
            created = false;
        if (this.existingTabs[btn.data.master] === undefined) {
            this.createTab(btn.data);
            created = true;
        }
        else {
            if (this.existingTabs[btn.data.master].master.isDestroyed) {
                this.createTab(btn.data);
                created = true;
            }
        }
        this.clearDetail();
        if (detail) {
            detail.add(this.existingTabs[btn.data.master].detail);
            detail.doLayout();
            tabs.setActiveTab(this.existingTabs[btn.data.master].master);
        }
        return created;
    },

    loadStore: function (activeTab) {
        if (activeTab.loadStoreFirst) {
            activeTab.loadStoreFirst();
        }
        else {
            if (activeTab.getActiveTab) {
                activeTab = activeTab.getActiveTab();
                if (activeTab.loadStoreFirst) {
                    activeTab.loadStoreFirst();
                }
                else {
                    if (activeTab.getActiveTab) {
                        activeTab = activeTab.getActiveTab();
                        if (activeTab.loadStoreFirst) {
                            activeTab.loadStoreFirst();
                        }
                    }
                }
            }
        }
    },

    changeDetail: function (tabPanel, newCard) {
        var detail = this.getDetailControl(),
            navControl = this.getNavigationControl(),
            tabs = this.existingTabs;
        this.loadStore(newCard);
        this.clearDetail();
        for (i in tabs) {
            if (tabs.hasOwnProperty(i)) {
                if (tabs[i].master && newCard) {
                    if (tabs[i].master.id == newCard.id) {
                        if (tabs[i].master.isComplexView) {
                            if (this.complexDetails[tabs[i].master.xtype]) {
                                if (this.complexDetails[tabs[i].master.xtype].current) {
                                    detail.add(this.complexDetails[tabs[i].master.xtype].current);
                                }
                            }
                        }
                        else {
                            detail.add(tabs[i].detail);
                        }
                        detail.doLayout();
                        // Switching accordion groups to find currently active
                        navControl.items.each(function (itm) {
                            itm.items.each(function (btn) {
                                btn.removeCls('hover');
                                if (btn.data.master == tabs[i].masterCls) {
                                    itm.expand();
                                    btn.addCls('hover');
                                }
                            });
                        });
                    }
                }
            }
        }
    },

    clearDetail: function () {
        var detail = this.getDetailControl();
        this.getDetailControl().items.clear();
        this.getDetailControl().doLayout();
    }
});