URL_PREFIX = '';
URL_SERVER_PREFIX = '';

// Параметры конфигурации системы
Cement = {};
Cement.Config = {
    singleton: true,
    default_photo: URL_PREFIX + '/content/img/default_pic.png',
    user_default_photo: URL_PREFIX + '/content/img/default_user.png',
    default_logo: URL_PREFIX + '/content/img/company-logo-default.png',

    url: {
        logo: URL_PREFIX + '/content/img/logo.png',
        interface_navigation: URL_PREFIX + '/server/navigation',
        creatorHelp: URL_PREFIX + '/creator_help.html', //URL для помощи по форме создания сущности, если никакая из сущностей не выбрана

        // Справочники
        dictionaries: URL_PREFIX + '/api/dict/get', //Полный набор справочников, загружаемых на старте

        //Для адреса
        index_autocomplete: URL_SERVER_PREFIX + '/api/addressdict/list', //Автокомплит индексов
        streets_list: URL_SERVER_PREFIX + '/server/streets_list.json', //Список улиц по индексу
        house_parts: URL_PREFIX + '/server/house_parts.json', //Корпуса
        house_buildings: URL_PREFIX + '/server/house_parts.json', //Строения

        //Параметры limit, start и т.п. для справочников
        dict_startParam: 'p_start',
        dict_limitParam: 'p_limit',
        dict_filterParam: 'p_filter',
        dict_pageParam: 'p_page',
        dict_sortParam: 'p_sort',

        //Структура организации для согласования и выбора сотрудников
        firm_structure: getProxy(URL_PREFIX + '/api/employee/structure', ''),
        firm_structure_external: getProxy(URL_PREFIX + '/api/employee/external_structure', ''),

        administration: {
            organizations: {
                organizations_dict: getProxy(URL_PREFIX + '/api/organization/list', ''),
                clients_dict: getProxy(URL_PREFIX + '/api/client/free_clients_list', ''),
                loadItem: URL_PREFIX + '/api/organization/get/{0}',
                saveItem: URL_PREFIX + '/api/organization/save'
            },
            superusers: {
                proxy: getProxy(URL_PREFIX + '/api/user/list'),
                saveUrl: URL_PREFIX + '/api/user/save',
                saveMethod: 'POST',
                deleteUrl: URL_PREFIX + '/api/user/delete/{0}',
                manageStateUrl: URL_PREFIX + '/api/user/manage/{0}/{1}'
            },
            dictionaries: {
                proxy: getProxy(URL_PREFIX + '/api/dict/list'),
                saveUrl: URL_PREFIX + '/api/dict/save',
                deleteUrl: URL_PREFIX + '/api/dict/delete/{0}/{1}',
                entityListUrl: URL_PREFIX + '/api/dict/list_entities'
            }
        },

        //Навигация
        navigation: {
            geozones: {
                new_g: { //Новые
                    proxy: getProxy(URL_PREFIX + '/server/geozones.json', 'geozones'),
                    saveUrl: URL_PREFIX + '/server/office_work.json',
                    saveMethod: 'POST',
                    deleteUrl: URL_PREFIX + '/server/office_work/delete/{0}.json',
                    printItem: URL_PREFIX + '/server/office_work/{0}',
                    printGrid: URL_PREFIX + '/server/office_work',
                    help: URL_PREFIX + '/server/office_work'
                },
                current: { //Действующие
                    proxy: getProxy(URL_PREFIX + '/server/geozones.json', 'geozones'),
                    saveUrl: URL_PREFIX + '/server/office_work.json',
                    saveMethod: 'POST',
                    deleteUrl: URL_PREFIX + '/server/office_work/delete/{0}.json',
                    printItem: URL_PREFIX + '/server/office_work/{0}',
                    printGrid: URL_PREFIX + '/server/office_work',
                    help: URL_PREFIX + '/server/office_work',
                    archiveItem: URL_PREFIX + '/server/sign.json?id={0}'
                },
                archive: { //Архив
                    proxy: getProxy(URL_PREFIX + '/server/geozones.json', 'geozones'),
                    saveUrl: URL_PREFIX + '/server/office_work.json',
                    saveMethod: 'POST',
                    deleteUrl: URL_PREFIX + '/server/office_work/delete/{0}.json',
                    printItem: URL_PREFIX + '/server/office_work/{0}',
                    printGrid: URL_PREFIX + '/server/office_work',
                    help: URL_PREFIX + '/server/office_work',
                    unArchiveItem: URL_PREFIX + '/server/sign.json?id={0}'
                },
                documents: { //Связанные документы
                    proxy: getProxy(URL_PREFIX + '/server/connected_documents.json', 'documents'),
                    printItem: URL_PREFIX + '/server/documents/{0}',
                    printGrid: URL_PREFIX + '/server/documents',
                    help: URL_PREFIX + '/server/documents'
                }, //navigation geozones documents
                routes: {
                    proxy: getProxy(URL_PREFIX + '/server/price_routes.json', 'routes'),
                    saveUrl: URL_PREFIX + '/price_routes.json',
                    saveMethod: 'POST',
                    deleteUrl: URL_PREFIX + '/price_routes/delete/{0}.json',
                    printItem: URL_PREFIX + '/price_routes/{0}',
                    printGrid: URL_PREFIX + '/price_routes',
                    help: URL_PREFIX + '/price_routes'
                } //navigation geozones routes
            }, //navigation geozones
            map: { //Карта
                proxy: getProxy(URL_PREFIX + '/map.json', ''),
                car_marker: URL_PREFIX + '/content/img/marker_truck.png',
                house_marker: URL_PREFIX + '/content/img/marker_house.png'
            },
            indicators: { //Индикаторы
                proxy: getProxy(URL_PREFIX + '/server/navigation_indicators.json', '')
            },
            terminals: { //Терминалы
                transport_units_dict: URL_PREFIX + '/server/goods_transport_units.json',
                service_suppliers_dict: URL_PREFIX + '/server/service_suppliers.json',
                new_t: { //Новые
                    proxy: getProxy(URL_PREFIX + '/server/personal_messages.json', 'messages'),
                    saveUrl: URL_PREFIX + '/server/goods.json',
                    saveMethod: 'POST',
                    deleteUrl: URL_PREFIX + '/server/goods/delete/{0}.json',
                    printItem: URL_PREFIX + '/server/goods/{0}',
                    printGrid: URL_PREFIX + '/server/goods',
                    help: URL_PREFIX + '/server/goods'
                }, //navigation terminals new_t
                current: { //Действующие
                    proxy: getProxy(URL_PREFIX + '/server/personal_messages.json', 'messages'),
                    saveUrl: URL_PREFIX + '/server/goods.json',
                    saveMethod: 'POST',
                    deleteUrl: URL_PREFIX + '/server/goods/delete/{0}.json',
                    printItem: URL_PREFIX + '/server/goods/{0}',
                    printGrid: URL_PREFIX + '/server/goods',
                    help: URL_PREFIX + '/server/goods'
                }, //navigation terminals current
                archive: { //Архив
                    proxy: getProxy(URL_PREFIX + '/server/personal_messages.json', 'messages'),
                    saveUrl: URL_PREFIX + '/server/goods.json',
                    saveMethod: 'POST',
                    deleteUrl: URL_PREFIX + '/server/goods/delete/{0}.json',
                    printItem: URL_PREFIX + '/server/goods/{0}',
                    printGrid: URL_PREFIX + '/server/goods',
                    help: URL_PREFIX + '/server/goods'
                }, //navigation terminals archive
                documents: { //Связанные документы
                    proxy: getProxy(URL_PREFIX + '/server/connected_documents.json', 'documents'),
                    printItem: URL_PREFIX + '/server/documents/{0}',
                    printGrid: URL_PREFIX + '/server/documents',
                    help: URL_PREFIX + '/server/documents'
                }, //navigation terminals documents
                transport_units: { //ТЕ
                    proxy: getProxy(URL_PREFIX + '/server/transport_units.json', 'transport_units')
                } //navigation terminals transport units
            }//navigation terminals
        },

        calendar: {
            proxy: getProxy(URL_PREFIX + '/server/calendar.json', 'events')
        },

        accounting: { //Бухгалтерия
            documents: { //Документы
                payers_dict: URL_PREFIX + '/server/suppliers_struc_acc.json',    //Плательщики
                getters_dict: URL_PREFIX + '/server/suppliers_struc_acc.json',    //Получатели
                goods_getters_dict: URL_PREFIX + '/server/suppliers_struc_acc.json',    //ГрузоПолучатели
                goods_senders_dict: URL_PREFIX + '/server/suppliers_struc_acc.json',    //ГрузоОтправители
                buyers_dict: URL_PREFIX + '/server/suppliers_struc_acc.json',    //Покупатели
                reasons_dict: URL_PREFIX + '/server/connected_documents_tree.json', //Связанные документы
                employees_dict: URL_PREFIX + '/server/suppliers_contacts_struc.json', //Сотрудники
                proxis_dict: URL_PREFIX + '/server/connected_documents_tree.json', //Доверенности
                pay_documents_dict: URL_PREFIX + '/server/connected_documents_tree.json', //Платежно-расчетные документы
                fill_by_documents_dict: URL_PREFIX + '/server/connected_documents_tree.json', //Заполнить по
                save: {
                    upload: URL_PREFIX + '/server/goods.json', //Загрузить
                    payment: URL_PREFIX + '/server/goods.json', //Создать плат поручение
                    bill: URL_PREFIX + '/server/goods.json', //Создать счет
                    act: URL_PREFIX + '/server/goods.json', //Создать акт
                    proxy: URL_PREFIX + '/server/goods.json', //Создать доверенность
                    waybill: URL_PREFIX + '/server/goods.json', //Создать накладная
                    invoice: URL_PREFIX + '/server/goods.json', //Создать счет-фактура
                    goods_check: URL_PREFIX + '/server/goods.json', //Создать счет-фактура
                    universal_document: URL_PREFIX + '/server/goods.json' //Создать универсальный передаточный документ
                },
                to_form: { //Новые
                    proxy: getProxy(URL_PREFIX + '/server/accounting_documents.json', 'documents'),
                    deleteUrl: URL_PREFIX + '/server/goods/delete/{0}.json',
                    printItem: URL_PREFIX + '/server/goods/{0}',
                    printGrid: URL_PREFIX + '/server/goods',
                    formUrl: URL_PREFIX + '/server/accounting_form.json', //Формирование
                    help: URL_PREFIX + '/server/goods'
                }, //accounting documents to_form
                new_d: { //Новые
                    proxy: getProxy(URL_PREFIX + '/server/accounting_documents.json', 'documents'),
                    saveUrl: URL_PREFIX + '/server/goods.json',
                    saveMethod: 'POST',
                    deleteUrl: URL_PREFIX + '/server/goods/delete/{0}.json',
                    printItem: URL_PREFIX + '/server/goods/{0}',
                    printGrid: URL_PREFIX + '/server/goods',
                    help: URL_PREFIX + '/server/goods'
                }, //accounting documents new_d
                incoming: { //Входящие
                    proxy: getProxy(URL_PREFIX + '/server/accounting_documents.json', 'documents'),
                    printItem: URL_PREFIX + '/server/goods/{0}',
                    printGrid: URL_PREFIX + '/server/goods',
                    help: URL_PREFIX + '/server/goods'
                }, //accounting documents incoming
                outcoming: { //Исходящие
                    proxy: getProxy(URL_PREFIX + '/server/accounting_documents.json', 'documents'),
                    deleteUrl: URL_PREFIX + '/server/goods/delete/{0}.json',
                    printItem: URL_PREFIX + '/server/goods/{0}',
                    printGrid: URL_PREFIX + '/server/goods',
                    help: URL_PREFIX + '/server/goods'
                }, //accounting documents outcoming
                documents: { //Связанные документы
                    proxy: getProxy(URL_PREFIX + '/server/connected_documents.json', 'documents'),
                    printItem: URL_PREFIX + '/server/documents/{0}',
                    printGrid: URL_PREFIX + '/server/documents',
                    help: URL_PREFIX + '/server/documents'
                }, //accounting documents documents
                routes: {
                    proxy: getProxy(URL_PREFIX + '/server/price_routes.json', 'routes'),
                    saveUrl: URL_PREFIX + '/price_routes.json',
                    saveMethod: 'POST',
                    deleteUrl: URL_PREFIX + '/price_routes/delete/{0}.json',
                    printItem: URL_PREFIX + '/price_routes/{0}',
                    printGrid: URL_PREFIX + '/price_routes',
                    help: URL_PREFIX + '/price_routes'
                }
            },//accounting documents
            indicators: { //Индикаторы
                documents_dict: URL_PREFIX + '/server/connected_documents_tree.json', //Связанные документы
                clients_dict: URL_PREFIX + '/api/client/clients_structure',
                new_i: { //Новые
                    proxy: getProxy(URL_PREFIX + '/server/indicators.json', 'indicators'),
                    saveUrl: URL_PREFIX + '/server/goods.json',
                    saveMethod: 'POST',
                    deleteUrl: URL_PREFIX + '/server/goods/delete/{0}.json',
                    printItem: URL_PREFIX + '/server/goods/{0}',
                    printGrid: URL_PREFIX + '/server/goods',
                    help: URL_PREFIX + '/server/goods'
                }, //accounting indicators new_m
                incoming: { //Входящие
                    proxy: getProxy(URL_PREFIX + '/server/indicators.json', 'indicators'),
                    saveUrl: URL_PREFIX + '/server/goods.json',
                    saveMethod: 'POST',
                    deleteUrl: URL_PREFIX + '/server/goods/delete/{0}.json',
                    printItem: URL_PREFIX + '/server/goods/{0}',
                    printGrid: URL_PREFIX + '/server/goods',
                    help: URL_PREFIX + '/server/goods',
                    archiveItem: URL_PREFIX + '/server/sign.json?id={0}'
                }, //accounting indicators incoming
                outcoming: { //Исходящие
                    proxy: getProxy(URL_PREFIX + '/server/indicators.json', 'indicators'),
                    deleteUrl: URL_PREFIX + '/server/goods/delete/{0}.json',
                    printItem: URL_PREFIX + '/server/goods/{0}',
                    printGrid: URL_PREFIX + '/server/goods',
                    help: URL_PREFIX + '/server/goods',
                    archiveItem: URL_PREFIX + '/server/sign.json?id={0}'
                }, //accounting indicators outcoming
                archive: { //Архив
                    proxy: getProxy(URL_PREFIX + '/server/indicators.json', 'indicators'),
                    deleteUrl: URL_PREFIX + '/server/goods/delete/{0}.json',
                    printItem: URL_PREFIX + '/server/goods/{0}',
                    printGrid: URL_PREFIX + '/server/goods',
                    help: URL_PREFIX + '/server/goods',
                    unArchiveItem: URL_PREFIX + '/server/sign.json?id={0}'
                }, //accounting indicators archive
                documents: { //Связанные документы
                    proxy: getProxy(URL_PREFIX + '/server/connected_documents.json', 'documents'),
                    printItem: URL_PREFIX + '/server/documents/{0}',
                    printGrid: URL_PREFIX + '/server/documents',
                    help: URL_PREFIX + '/server/documents'
                }, //accounting indicators documents
                routes: {
                    proxy: getProxy(URL_PREFIX + '/server/price_routes.json', 'routes'),
                    saveUrl: URL_PREFIX + '/price_routes.json',
                    saveMethod: 'POST',
                    deleteUrl: URL_PREFIX + '/price_routes/delete/{0}.json',
                    printItem: URL_PREFIX + '/price_routes/{0}',
                    printGrid: URL_PREFIX + '/price_routes',
                    help: URL_PREFIX + '/price_routes'
                }
            },//accounting indicators
            other: { //иные документы
                new_o: buildConfig('accountingother', { get: true }, {
                    postingRequestProxy: getProxy(URL_PREFIX + '/api/accountingother/listsignedpostingrequest', ''),
                    expenseMovementProxy: getProxy(URL_PREFIX + '/api/accountingother/listsignedexpensemovement', ''),
                    expenseWriteoffProxy: getProxy(URL_PREFIX + '/api/accountingother/listsignedexpensewriteoff', '')
                }),
                incoming: buildConfig('accountingother', { listAction: 'listincoming' }),
                outcoming: buildConfig('accountingother', { listAction: 'listoutcoming' }),
                archive: buildConfig('accountingother', { listAction: 'listarchive' }),
                products: { proxy: getProxy(URL_PREFIX + '/api/accountingother/products', '') },
                services: { proxy: getProxy(URL_PREFIX + '/api/accountingother/services', '') },
                warehouses: { proxy: getProxy(URL_PREFIX + '/api/accountingother/warehouses', '') },
                routes: { proxy: getProxy(URL_PREFIX + '/api/accountingother/listroutes', '') },
                documents: { proxy: getProxy(URL_PREFIX + '/api/otherdoc/documents', '') }
            },//other
            receiptexpense: { //документы приход/расход
                new_re: buildConfig('accountinginoutdoc', { get: true }, {
                    expensesSalebillProxy: getProxy(URL_PREFIX + '/api/accountinginoutdoc/listsignedexpensesaleproduct', ''),
                    expensesCompletionActProxy: getProxy(URL_PREFIX + '/api/accountinginoutdoc/listsignedexpensesaleservice', ''),
                    accountingBillProxy: getProxy(URL_PREFIX + '/api/accountinginoutdoc/listsignedaccoutingbill', '')
                }),
                incoming: buildConfig('accountinginoutdoc', { listAction: 'listincoming' }),
                outcoming: buildConfig('accountinginoutdoc', { listAction: 'listoutcoming' }),
                archive: buildConfig('accountinginoutdoc', { listAction: 'listarchive' }),
                products: { proxy: getProxy(URL_PREFIX + '/api/accountinginoutdoc/products', '') },
                services: { proxy: getProxy(URL_PREFIX + '/api/accountinginoutdoc/services', '') },
                senders: { proxy: getProxy(URL_PREFIX + '/api/accountinginoutdoc/senders', '') },
                sellers: { proxy: getProxy(URL_PREFIX + '/api/accountinginoutdoc/sellers', '') },
                payers: { proxy: getProxy(URL_PREFIX + '/api/accountinginoutdoc/payers', '') },
                getters: { proxy: getProxy(URL_PREFIX + '/api/accountinginoutdoc/getters', '') },
                suppliers: { proxy: getProxy(URL_PREFIX + '/api/receiptexpense/suppliers', '') },
                routes: { proxy: getProxy(URL_PREFIX + '/api/accountinginoutdoc/listroutes', '') },
                documents: { proxy: getProxy(URL_PREFIX + '/api/receiptexpense/documents', '') },
                paymentdoc_dict: { proxy: getProxy(URL_PREFIX + '/api/receiptexpense/paymentdocuments', '') }
            }
        }, //accounting

        personal: {
            documents: {
                incoming: { //Входящие
                    proxy: getProxy(URL_PREFIX + '/api/personalinternaldoc/inlist', 'documents'),
                    packet_agree: URL_PREFIX + '/api/personalinternaldoc/agreeincoming',
                    packet_sign: URL_PREFIX + '/api/personalinternaldoc/signincoming',
                    packet_cancel: URL_PREFIX + '/api/personalinternaldoc/cancelincoming',
                    printItem: URL_PREFIX + '/api/personalinternaldoc/printincoming/{0}/{1}',
                    printGrid: URL_PREFIX + '/server/goods',
                    help: URL_PREFIX + '/server/goods'
                }, //personal documents incoming
                outcoming: { //Исходящие
                    proxy: getProxy(URL_PREFIX + '/api/personalinternaldoc/outlist', 'documents'),
                    packet_agree: URL_PREFIX + '/api/personalinternaldoc/agreeoutcoming',
                    packet_sign: URL_PREFIX + '/api/personalinternaldoc/signoutcoming',
                    packet_cancel: URL_PREFIX + '/api/personalinternaldoc/canceloutcoming',
                    printItem: URL_PREFIX + '/server/goods/{0}',
                    printGrid: URL_PREFIX + '/server/goods',
                    help: URL_PREFIX + '/server/goods'
                } //personal documents outcoming
            }, //personal documents
            correspondention: {
                incoming: { //Входящие
                    proxy: getProxy(URL_PREFIX + '/api/personalcorrespondence/inlist', 'documents'),
                    packet_agree: URL_PREFIX + '/api/personalcorrespondence/agreeincoming',
                    packet_sign: URL_PREFIX + '/api/personalcorrespondence/signincoming',
                    packet_cancel: URL_PREFIX + '/api/personalcorrespondence/cancelincoming',
                    printItem: URL_PREFIX + '/server/goods/{0}',
                    printGrid: URL_PREFIX + '/server/goods',
                    help: URL_PREFIX + '/server/goods'
                }, //personal correspondention incoming
                outcoming: { //Исходящие
                    proxy: getProxy(URL_PREFIX + '/api/personalcorrespondence/outlist', 'documents'),
                    packet_agree: URL_PREFIX + '/api/personalcorrespondence/agreeoutcoming',
                    packet_sign: URL_PREFIX + '/api/personalcorrespondence/signoutcoming',
                    packet_cancel: URL_PREFIX + '/api/personalcorrespondence/canceloutcoming',
                    printItem: URL_PREFIX + '/server/goods/{0}',
                    printGrid: URL_PREFIX + '/server/goods',
                    help: URL_PREFIX + '/server/goods'
                } //personal correspondention outcoming
            }, //personal correspondention
            messages: { //Сообщения
                employees_dict: URL_PREFIX + '/api/employee/structure_v2', //Сотрудники
                authors_dict: URL_PREFIX + '/api/employee/structure_v2', //Авторы
                new_m: { //Новые
                    proxy: getProxy(URL_PREFIX + '/api/message/list', 'messages'),
                    saveUrl: URL_PREFIX + '/api/message/save',
                    saveMethod: 'POST',
                    deleteUrl: URL_PREFIX + '/api/message/delete/{0}',
                    printItem: URL_PREFIX + '/server/goods/{0}',
                    printGrid: URL_PREFIX + '/server/goods',
                    help: URL_PREFIX + '/server/goods',
                    signUrl: URL_PREFIX + '/api/message/agreementForNew'
                }, //personal messages new_m
                incoming: { //Входящие
                    proxy: getProxy(URL_PREFIX + '/api/message/listincoming', 'messages'),
                    saveUrl: URL_PREFIX + '/server/goods.json',
                    saveMethod: 'POST',
                    deleteUrl: URL_PREFIX + '/server/goods/delete/{0}.json',
                    printItem: URL_PREFIX + '/server/goods/{0}',
                    printGrid: URL_PREFIX + '/server/goods',
                    help: URL_PREFIX + '/server/goods',
                    archiveItem: URL_PREFIX + '/api/message/archive/{0}',
                    signUrl: URL_PREFIX + '/api/message/agreementForIncoming'
                }, //personal messages incoming
                outcoming: { //Исходящие
                    proxy: getProxy(URL_PREFIX + '/api/message/listoutcoming', 'messages'),
                    saveUrl: URL_PREFIX + '/server/goods.json',
                    saveMethod: 'POST',
                    deleteUrl: URL_PREFIX + '/server/goods/delete/{0}.json',
                    printItem: URL_PREFIX + '/server/goods/{0}',
                    printGrid: URL_PREFIX + '/server/goods',
                    help: URL_PREFIX + '/server/goods',
                    archiveItem: URL_PREFIX + '/api/message/archive/{0}'
                }, //personal messages outcoming
                archive: { //Архив
                    proxy: getProxy(URL_PREFIX + '/api/message/listarchive', 'messages'),
                    saveUrl: URL_PREFIX + '/server/goods.json',
                    saveMethod: 'POST',
                    deleteUrl: URL_PREFIX + '/api/message/deleteArchive/{0}',
                    printItem: URL_PREFIX + '/server/goods/{0}',
                    printGrid: URL_PREFIX + '/server/goods',
                    help: URL_PREFIX + '/server/goods',
                    unArchiveItem: URL_PREFIX + '/api/message/unarchive/{0}'
                }, //personal messages archive
                documents: { //Связанные документы
                    proxy: getProxy(URL_PREFIX + '/api/docrelation/list', 'documents'),
                    printItem: URL_PREFIX + '/server/documents/{0}',
                    printGrid: URL_PREFIX + '/server/documents',
                    help: URL_PREFIX + '/server/documents'
                }, //personal messages documents
                routes: {
                    proxy: getProxy(URL_PREFIX + '/api/message/listroutes', 'routes'),
                    saveUrl: URL_PREFIX + '/price_routes.json',
                    saveMethod: 'POST',
                    deleteUrl: URL_PREFIX + '/price_routes/delete/{0}.json',
                    printItem: URL_PREFIX + '/price_routes/{0}',
                    printGrid: URL_PREFIX + '/price_routes',
                    help: URL_PREFIX + '/price_routes'
                }
            },//personal messages
            adverts: { //Объявления
                employees_dict: URL_PREFIX + '/api/employee/structure_v2', //Сотрудники
                authors_dict: URL_PREFIX + '/api/employee/structure_v2', //Авторы
                new_a: { //Новые
                    proxy: getProxy(URL_PREFIX + '/api/announcement/list', 'messages'),
                    saveUrl: URL_PREFIX + '/api/announcement/save',
                    saveMethod: 'POST',
                    deleteUrl: URL_PREFIX + '/api/announcement/delete/{0}',
                    printItem: URL_PREFIX + '/server/goods/{0}',
                    printGrid: URL_PREFIX + '/server/goods',
                    help: URL_PREFIX + '/server/goods',
                    signUrl: URL_PREFIX + '/api/announcement/agreementForNew'
                }, //personal messages new_m
                incoming: { //Входящие
                    proxy: getProxy(URL_PREFIX + '/api/announcement/listincoming', 'messages'),
                    saveUrl: URL_PREFIX + '/server/goods.json',
                    saveMethod: 'POST',
                    deleteUrl: URL_PREFIX + '/server/goods/delete/{0}.json',
                    printItem: URL_PREFIX + '/server/goods/{0}',
                    printGrid: URL_PREFIX + '/server/goods',
                    help: URL_PREFIX + '/server/goods',
                    archiveItem: URL_PREFIX + '/api/announcement/archive/{0}',
                    signUrl: URL_PREFIX + '/api/announcement/agreementForIncoming'
                }, //personal adverts incoming
                outcoming: { //Исходящие
                    proxy: getProxy(URL_PREFIX + '/api/announcement/listoutcoming', 'messages'),
                    saveUrl: URL_PREFIX + '/server/goods.json',
                    saveMethod: 'POST',
                    deleteUrl: URL_PREFIX + '/server/goods/delete/{0}.json',
                    printItem: URL_PREFIX + '/server/goods/{0}',
                    printGrid: URL_PREFIX + '/server/goods',
                    help: URL_PREFIX + '/server/goods',
                    archiveItem: URL_PREFIX + '/api/announcement/archive/{0}'
                }, //personal adverts outcoming
                archive: { //Архив
                    proxy: getProxy(URL_PREFIX + '/api/announcement/listarchive', 'messages'),
                    saveUrl: URL_PREFIX + '/server/goods.json',
                    saveMethod: 'POST',
                    deleteUrl: URL_PREFIX + '/api/announcement/deleteArchive/{0}',
                    printItem: URL_PREFIX + '/server/goods/{0}',
                    printGrid: URL_PREFIX + '/server/goods',
                    help: URL_PREFIX + '/server/goods',
                    unArchiveItem: URL_PREFIX + '/api/announcement/unarchive/{0}'
                }, //personal adverts archive
                documents: { //Связанные документы
                    proxy: getProxy(URL_PREFIX + '/api/docrelation/list', 'documents'),
                    printItem: URL_PREFIX + '/server/documents/{0}',
                    printGrid: URL_PREFIX + '/server/documents',
                    help: URL_PREFIX + '/server/documents'
                }, //personal adverts documents
                routes: {
                    proxy: getProxy(URL_PREFIX + '/api/announcement/listroutes', 'routes'),
                    saveUrl: URL_PREFIX + '/price_routes.json',
                    saveMethod: 'POST',
                    deleteUrl: URL_PREFIX + '/price_routes/delete/{0}.json',
                    printItem: URL_PREFIX + '/price_routes/{0}',
                    printGrid: URL_PREFIX + '/price_routes',
                    help: URL_PREFIX + '/price_routes'
                }
            }//personal adverts
        }, //personal

        corporate: {
            //Dashboard
            dashboard: URL_PREFIX + '/server/dashboard',
            my_firm: URL_SERVER_PREFIX + '/server/firmdetails',
            // Реквизиты
            firms: {
                proxy: getProxy(URL_PREFIX + '/firms.json', 'firms'),
                saveUrl: URL_PREFIX + '/firms.json',
                saveMethod: 'POST',
                deleteUrl: URL_PREFIX + '/firms/delete/{0}.json',
                printItem: URL_PREFIX + '/firms/{0}',
                printGrid: URL_PREFIX + '/firms',
                help: URL_PREFIX + '/firms',
                loadItem: URL_PREFIX + '/api/organization/get/{0}'
            },
            // Номенклатура дел
            nomenklatures: buildConfig('nomenclature'),

            // Склады
            warehouses: buildConfig('warehouse', { get: true }, {
                stations: URL_SERVER_PREFIX + '/api/warehouse/stationdict', //Станции
                owners: URL_PREFIX + '/api/client/clients_structure',    //Владельцы
                employeeProxy: getProxy(URL_PREFIX + '/api/warehouse/listemployee', ''),
                loadItem: URL_PREFIX + '/api/warehouse/get/{0}'
            }),

            // Сотрудники
            employes: buildConfig('employee', { get: true }, {
                create_department: URL_PREFIX + '/api/department/save' //Создать отдел
            }),
        
            // Отделы
            departments: buildConfig('department'),

            // Подписи
            signatures: buildConfig('signature', {}, {
                documents: getProxy(URL_PREFIX + '/api/document/internaldoctree', '') //Документы "на основании"
            }),

            // Администрирование
            administration: buildConfig('permission', {}, {
                permissionTree: getProxy(URL_PREFIX + '/api/permission/tree', '')
            }),

            // Банковские счета
            bank_accounts: buildConfig('paymentaccount', {}, {
                autocomplete: URL_SERVER_PREFIX + '/api/paymentaccount/banklist' //Автокомплит для БИКов
            })
        },

        // Клиенты
        clients: {
            structure: getProxy(URL_PREFIX + '/api/client/clients_structure', ''), //Структура клиентов для всплывающего окна - используется также в договорах
            orgClientsStructure: getProxy(URL_PREFIX + '/api/client/orgclients_structure', ''), //Структура клиентов для всплывающего окна - используется также в договорах
            personalClientsStructure: getProxy(URL_PREFIX + '/api/client/personalclients_structure', ''),
            //groups_tree: getProxy(URL_PREFIX + '/api/clientgroup/tree', ''),
            //sub_groups_tree: getProxy(URL_PREFIX + '/api/clientgroup/tree', ''),
            statuses_dict: getProxy(URL_PREFIX + '/api/clientstate/listall', ''), //Статусы для выпадалки в создавалке
            management: { // Управление клиентами
                events: { // События
                    proxy: getProxy(URL_PREFIX + '/api/personalclientevent/list', ''),
                    saveUrl: URL_PREFIX + '/api/personalclientevent/save',
                    saveMethod: 'POST',
                    deleteUrl: URL_PREFIX + '/api/personalclientevent/delete/{0}',
                    printItem: URL_PREFIX + '/clients/{0}',
                    printGrid: URL_PREFIX + '/clients',
                    help: URL_PREFIX + '/clients',
                    archiveItem: URL_PREFIX + '/api/personalclientevent/archive/{0}',
                    commentItem: URL_PREFIX + '/api/personalclientevent/comment'
                }, //clients management events 
                archive: { // Архив
                    proxy: getProxy(URL_PREFIX + '/api/personalclientevent/listarchived', ''),
                    saveUrl: URL_PREFIX + '/api/personalclientevent/save',
                    saveMethod: 'POST',
                    deleteUrl: URL_PREFIX + '/api/personalclientevent/delete/{0}',
                    printItem: URL_PREFIX + '/clients/{0}',
                    printGrid: URL_PREFIX + '/clients',
                    help: URL_PREFIX + '/clients',
                    unArchiveItem: URL_PREFIX + '/api/personalclientevent/unarchive/{0}'
                }, //clients management archive
                documents: { //Связанные документы
                    proxy: getProxy(URL_PREFIX + '/api/docrelation/list', 'documents'),
                    printItem: URL_PREFIX + '/server/documents/{0}',
                    printGrid: URL_PREFIX + '/server/documents',
                    help: URL_PREFIX + '/server/documents'
                } //clients management documents
            },
			groups: {
			    proxy: getProxy(URL_PREFIX + '/api/clientgroup/list', 'client_groups'),
			    saveUrl: URL_PREFIX + '/api/clientgroup/save',
                saveMethod: 'POST',
                deleteUrl: URL_PREFIX + '/api/clientgroup/delete/{0}',
                printItem: URL_PREFIX + '/clients/{0}',
                printGrid: URL_PREFIX + '/clients',
                help: URL_PREFIX + '/clients'
			},
			sub_groups: {
			    proxy: getProxy(URL_PREFIX + '/api/clientgroup/subgroupslist', 'client_groups'),
			    saveUrl: URL_PREFIX + '/api/clientgroup/save',
			    saveMethod: 'POST',
			    deleteUrl: URL_PREFIX + '/api/clientgroup/delete/{0}',
			    printItem: URL_PREFIX + '/clients/{0}',
			    printGrid: URL_PREFIX + '/clients',
			    help: URL_PREFIX + '/clients'
			},
            statuses: {
                proxy: getProxy(URL_PREFIX + '/api/clientstate/list', 'client_groups'),
                saveUrl: URL_PREFIX + '/api/clientstate/save',
                saveMethod: 'POST',
                deleteUrl: URL_PREFIX + '/api/clientstate/delete/{0}',
                printItem: URL_PREFIX + '/clients/{0}',
                printGrid: URL_PREFIX + '/clients',
                help: URL_PREFIX + '/clients'
            },
            tasks: {
                proxy: getProxy(URL_PREFIX + '/server/office_work_documents.json'),
                loadItem: URL_PREFIX + '/clients/{0}.json',
                saveUrl: URL_PREFIX + '/clients.json',
                saveMethod: 'POST',
                deleteUrl: URL_PREFIX + '/clients/delete/{0}.json',
                printItem: URL_PREFIX + '/clients/{0}',
                printGrid: URL_PREFIX + '/clients',
                help: URL_PREFIX + '/clients'
            },
            current: {   //Общий каталог
                proxy: getProxy(URL_PREFIX + '/api/client/list', 'clients'),
                loadItem: URL_PREFIX + '/clients/{0}.json',
                saveUrl: URL_PREFIX + '/api/personalclient/save',
                saveMethod: 'POST',
                deleteUrl: URL_PREFIX + '',
                printItem: URL_PREFIX + '/clients/{0}',
                printGrid: URL_PREFIX + '/clients',
                help: URL_PREFIX + '/clients'
            },
            corporate: {  //Корпоративный
                proxy: getProxy(URL_PREFIX + '/api/personalclient/listcorporate', 'clients'),
                loadItem: URL_PREFIX + '/clients/{0}.json',
                saveUrl: URL_PREFIX + '/clients.json',
                saveMethod: 'POST',
                printItem: URL_PREFIX + '/clients/{0}',
                printGrid: URL_PREFIX + '/clients',
                help: URL_PREFIX + '/clients',
                archiveItem: URL_PREFIX + '/api/personalclient/archive/{0}'
            },
            personal: {  //Корпоративный
                proxy: getProxy(URL_PREFIX + '/api/personalclient/list', 'clients'),
                loadItem: URL_PREFIX + '/clients/{0}.json',
                saveUrl: URL_PREFIX + '/clients.json',
                saveMethod: 'POST',
                printItem: URL_PREFIX + '/clients/{0}',
                printGrid: URL_PREFIX + '/clients',
                help: URL_PREFIX + '/clients',
                archiveItem: URL_PREFIX + '/api/personalclient/archive/{0}'
            },
            archive: {   //Архив
                proxy: getProxy(URL_PREFIX + '/api/personalclient/listarchived', 'clients'),
                loadItem: URL_PREFIX + '/clients/{0}.json',
                saveUrl: URL_PREFIX + '/clients.json',
                saveMethod: 'POST',
                deleteUrl: URL_PREFIX + '/api/personalclient/delete/{0}',
                printItem: URL_PREFIX + '/clients/{0}',
                printGrid: URL_PREFIX + '/clients',
                help: URL_PREFIX + '/clients',
                unArchiveItem: URL_PREFIX + '/api/personalclient/unarchive/{0}'
            },
            firms: {
                proxy: getProxy(URL_PREFIX + '/server/firms_json', 'firms'),
                loadItem: URL_PREFIX + '/clients/{0}.json',
                saveUrl: URL_PREFIX + '/clients.json',
                saveMethod: 'POST',
                deleteUrl: URL_PREFIX + '/clients/delete/{0}.json',
                printItem: URL_PREFIX + '/clients/{0}',
                printGrid: URL_PREFIX + '/clients',
                help: URL_PREFIX + '/clients'
            },
            people: {
                proxy: getProxy(URL_PREFIX + '/server/employes_json', 'employes'),
                loadItem: URL_PREFIX + '/clients/{0}.json',
                saveUrl: URL_PREFIX + '/clients.json',
                saveMethod: 'POST',
                deleteUrl: URL_PREFIX + '/clients/delete/{0}.json',
                printItem: URL_PREFIX + '/clients/{0}',
                printGrid: URL_PREFIX + '/clients',
                help: URL_PREFIX + '/clients'
            }
        },

        transport: {
            renters_structure: URL_PREFIX + '/api/client/renters_structure', //Структура фирм, в которые можно передать в аренду
            rent_documents_structure: URL_PREFIX + '/api/contract/rentcontractstree', //Структура документов для передачи в аренду
            all_transport_dict: URL_PREFIX + '/api/vehicle/listall', //Все авто ТС (в т.ч. прицеп)
            all_cars_dict: URL_PREFIX + '/api/vehicle/listwithouttrailers', //Все авто транспортные средства (не прицеп)
            all_trailers_dict: URL_PREFIX + '/api/vehicle/listtrailers', //Все авто прицепные транспортные
            all_drivers_dict: URL_PREFIX + '/api/employee/listdrivers', //Все водители
            all_units_dict: URL_PREFIX + '/api/transportunit/listall', //Все ТЕ
            auto: {
                models_dict: URL_PREFIX + '/api/vehiclemodification/brandmodeltree', //Марки авто
                modifications_dict: URL_PREFIX + '/api/vehiclemodification/listmodifications', //Модификации
                create_column: URL_PREFIX + '/api/vehiclecol/save', //Создать колонну
                columns_proxy: getProxy(URL_PREFIX + '/api/vehiclecol/list', 'departments'), //Список колонн
                create_subdivision: URL_PREFIX + '/api/vehicledept/save', //Создать подразделение
                subdivisions_proxy: getProxy(URL_PREFIX + '/api/vehicledept/list', 'departments'), //Список подразделений
                owned: {
                    proxy: getProxy(URL_PREFIX + '/api/vehicle/list', 'data'),
                    detailUrl: URL_PREFIX + '/api/vehicle/details/{0}',
                    saveUrl: URL_PREFIX + '/api/vehicle/save',
                    saveMethod: 'POST',
                    deleteUrl: URL_PREFIX + '/api/vehicle/delete/{0}',
                    printItem: URL_PREFIX + '/server/auto/{0}',
                    printGrid: URL_PREFIX + '/server/auto',
                    help: URL_PREFIX + '/server/auto'
                },
                sent_to_rent: {
                    proxy: getProxy(URL_PREFIX + '/api/vehiclearend/listgiventoarend', 'auto'),
                    detailUrl: URL_PREFIX + '/api/vehiclearend/details/{0}',
                    loadItem: URL_PREFIX + '/api/vehiclearend/get/{0}',
                    saveUrl: URL_PREFIX + '/api/vehiclearend/save',
                    saveMethod: 'POST',
                    deleteUrl: URL_PREFIX + '/api/vehiclearend/endarend/{0}',
                    printItem: URL_PREFIX + '/server/auto/{0}',
                    printGrid: URL_PREFIX + '/server/auto',
                    help: URL_PREFIX + '/server/auto'
                },
                got_in_rent: {
                    proxy: getProxy(URL_PREFIX + '/api/vehiclearend/listarended', 'auto'),
                    detailUrl: URL_PREFIX + '/api/vehiclearend/detailsarended/{0}',
                    loadItem: URL_PREFIX + '/api/vehiclearend/get/{0}',
                    saveUrl: URL_PREFIX + '/server/auto.json',
                    saveMethod: 'POST',
                    deleteUrl: URL_PREFIX + '/api/vehiclearend/endarend/{0}',
                    printItem: URL_PREFIX + '/server/auto/{0}',
                    printGrid: URL_PREFIX + '/server/auto',
                    help: URL_PREFIX + '/server/auto'
                }
            },
            railway: {
                models_dict: URL_PREFIX + '/server/carriage_models.json', //Модели
                manufacturers_dict: URL_PREFIX + '/server/carriage_models.json', //Производители
                owned_carriages_dict: URL_PREFIX + '/server/carriage_models.json', //Номера вагонов для сдачи в аренду
                owned: {
                    proxy: getProxy(URL_PREFIX + '/server/railway.json', 'railway'),
                    detailUrl: URL_PREFIX + '/server/auto_detail.json?id={0}',
                    saveUrl: URL_PREFIX + '/server/railway.json',
                    saveMethod: 'POST',
                    deleteUrl: URL_PREFIX + '/server/autodelete/{0}.json',
                    printItem: URL_PREFIX + '/server/railway/{0}',
                    printGrid: URL_PREFIX + '/server/railway',
                    help: URL_PREFIX + '/server/railway'
                },
                sent_to_rent: {
                    proxy: getProxy(URL_PREFIX + '/server/railway.json', 'railway'),
                    detailUrl: URL_PREFIX + '/server/auto_detail_sent_to_rent.json?id={0}',
                    saveUrl: URL_PREFIX + '/server/railway.json',
                    saveMethod: 'POST',
                    deleteUrl: URL_PREFIX + '/server/autodelete/{0}.json',
                    printItem: URL_PREFIX + '/server/railway/{0}',
                    printGrid: URL_PREFIX + '/server/railway',
                    help: URL_PREFIX + '/server/railway'
                },
                got_in_rent: {
                    proxy: getProxy(URL_PREFIX + '/server/railway.json', 'railway'),
                    detailUrl: URL_PREFIX + '/server/auto_detail_sent_to_rent.json?id={0}',
                    loadItem: URL_PREFIX + '/server/railway/{0}.json',
                    saveUrl: URL_PREFIX + '/server/railway.json',
                    saveMethod: 'POST',
                    deleteUrl: URL_PREFIX + '/server/autodelete/{0}.json',
                    printItem: URL_PREFIX + '/server/railway/{0}',
                    printGrid: URL_PREFIX + '/server/railway',
                    help: URL_PREFIX + '/server/railway'
                }
            },
            auto_control: {
                //Для создавалок
                cars_dict: URL_PREFIX + '/api/vehicle/dictionarylist',     //Автомобили
                drivers_dict: URL_PREFIX + '/api/employee/listdrivers',  //Водители
                work_names_dict: URL_PREFIX + '/server/getters_contacts.json',  //Наименования работ (Регламент)
                work_units_dict: URL_PREFIX + '/server/getters_contacts.json',  //Единицы измерения (Регламент)
                autoservices_dict: URL_PREFIX + '/api/client/clients_structure', //Автосервисы
                maintenance: { // Техобслуживание
                    proxy: getProxy(URL_PREFIX + '/api/vehiclemaintenance/list', 'maintenance'),
                    saveUrl: URL_PREFIX + '/api/vehiclemaintenance/save',
                    saveMethod: 'POST',
                    deleteUrl: URL_PREFIX + '/api/vehiclemaintenance/delete/{0}',
                    printItem: URL_PREFIX + '/carriages/{0}',
                    printGrid: URL_PREFIX + '/carriages',
                    help: URL_PREFIX + '/carriages'
                },
                regulation: { // Регламент
                    items: getProxy(URL_PREFIX + '/server/price_items.json', 'price_lists'), //Запчасти/услуги
                    proxy: getProxy(URL_PREFIX + '/server/regulation.json', 'regulation'),
                    saveUrl: URL_PREFIX + '/carriages.json',
                    saveMethod: 'POST',
                    deleteUrl: URL_PREFIX + '/carriages/delete/{0}.json',
                    printItem: URL_PREFIX + '/carriages/{0}',
                    printGrid: URL_PREFIX + '/carriages',
                    help: URL_PREFIX + '/carriages',
                    work_names_dict: URL_PREFIX + '/server/getters_contacts.json', //Наименования работ
                    work_units_dict: URL_PREFIX + '/server/getters_contacts.json'  //Единицы измерения
                },
                parts: { // Запчасть/услуга
                    proxy: getProxy(URL_PREFIX + '/api/vehiclesparepartservice/list', 'part'),
                    saveUrl: URL_PREFIX + '/api/vehiclesparepartservice/save',
                    saveMethod: 'POST',
                    deleteUrl: URL_PREFIX + '/api/vehiclesparepartservice/delete/{0}',
                    printItem: URL_PREFIX + '/carriages/{0}',
                    printGrid: URL_PREFIX + '/carriages',
                    help: URL_PREFIX + '/carriages'
                },
                fuel: { // Топливо
                    proxy: getProxy(URL_PREFIX + '/api/vehiclefuel/list', 'fuel'),
                    saveUrl: URL_PREFIX + '/api/vehiclefuel/save',
                    saveMethod: 'POST',
                    deleteUrl: URL_PREFIX + '/api/vehiclefuel/delete/{0}',
                    printItem: URL_PREFIX + '/carriages/{0}',
                    printGrid: URL_PREFIX + '/carriages',
                    help: URL_PREFIX + '/carriages'
                },
                odometer: { // Пробег
                    proxy: getProxy(URL_PREFIX + '/api/vehiclemileage/list', 'data'),
                    saveUrl: URL_PREFIX + '/api/vehiclemileage/save',
                    saveMethod: 'POST',
                    deleteUrl: URL_PREFIX + '/api/vehiclemileage/delete/{0}',
                    printItem: URL_PREFIX + '/carriages/{0}',
                    printGrid: URL_PREFIX + '/carriages',
                    help: URL_PREFIX + '/carriages'
                },
                insurance: { // Страховки
                    proxy: getProxy(URL_PREFIX + '/api/vehicleinsurance/list', 'insurance'),
                    saveUrl: URL_PREFIX + '/api/vehicleinsurance/save',
                    saveMethod: 'POST',
                    deleteUrl: URL_PREFIX + '/api/vehicleinsurance/delete/{0}',
                    printItem: URL_PREFIX + '/carriages/{0}',
                    printGrid: URL_PREFIX + '/carriages',
                    help: URL_PREFIX + '/carriages'
                },
                crash: { // ДТП
                    proxy: getProxy(URL_PREFIX + '/api/vehicleaccident/list', 'crash'),
                    saveUrl: URL_PREFIX + '/api/vehicleaccident/save',
                    saveMethod: 'POST',
                    deleteUrl: URL_PREFIX + '/api/vehicleaccident/delete/{0}',
                    printItem: URL_PREFIX + '/carriages/{0}',
                    printGrid: URL_PREFIX + '/carriages',
                    help: URL_PREFIX + '/carriages'
                }
            },
            units: {
                new_units: {
                    proxy: getProxy(URL_PREFIX + '/api/transportunit/list', 'transport_units'),
                    detailUrl: URL_PREFIX + '/api/transportunit/details/{0}',
                    loadItem: URL_PREFIX + '/api/transportunit/get/{0}',
                    saveUrl: URL_PREFIX + '/api/transportunit/save',
                    saveMethod: 'POST',
                    deleteUrl: URL_PREFIX + '/server/transport_units/delete/{0}.json',
                    printItem: URL_PREFIX + '/server/transport_units/{0}',
                    printGrid: URL_PREFIX + '/server/transport_units',
                    help: URL_PREFIX + '/server/transport_units'
                },
                owned: {
                    proxy: getProxy(URL_PREFIX + '/api/transportunit/list', 'transport_units'),
                    loadItem: URL_PREFIX + '/api/transportunit/get/{0}',
                    saveUrl: URL_PREFIX + '/api/transportunit/save',
                    saveMethod: 'POST',
                    deleteUrl: URL_PREFIX + '/api/transportunit/get/delete/{0}',
                    printItem: URL_PREFIX + '/server/transport_units/{0}',
                    printGrid: URL_PREFIX + '/server/transport_units',
                    help: URL_PREFIX + '/server/transport_units'
                },
                sent_to_rent: { //Переданы в управление
                    proxy: getProxy(URL_PREFIX + '/api/transportunitarend/listgiventoarend', 'transport_units'),
                    detailUrl: URL_PREFIX + '/api/transportunitarend/details/{0}',
                    loadItem: URL_PREFIX + '/server/transport_units/{0}.json',
                    saveUrl: URL_PREFIX + '/api/transportunitarend/save',
                    saveMethod: 'POST',
                    deleteUrl: URL_PREFIX + '/api/transportunitarend/endarend/{0}',
                    printItem: URL_PREFIX + '/server/transport_units/{0}',
                    printGrid: URL_PREFIX + '/server/transport_units',
                    help: URL_PREFIX + '/server/transport_units'
                },
                got_in_rent: {
                    proxy: getProxy(URL_PREFIX + '/api/transportunitarend/listarended', 'transport_units'),
                    detailUrl: URL_PREFIX + '/api/transportunitarend/detailsarended/{0}',
                    loadItem: URL_PREFIX + '/server/transport_units/{0}.json',
                    saveUrl: URL_PREFIX + '/server/transport_units.json',
                    saveMethod: 'POST',
                    deleteUrl: URL_PREFIX + '/api/transportunitarend/endarend/{0}',
                    printItem: URL_PREFIX + '/server/transport_units/{0}',
                    printGrid: URL_PREFIX + '/server/transport_units',
                    help: URL_PREFIX + '/server/transport_units'
                },
                archive: {
                    proxy: getProxy(URL_PREFIX + '/server/transport_units.json', 'transport_units'),
                    detailUrl: URL_PREFIX + '/server/transport_unit_detail_archive.json?id={0}',
                    loadItem: URL_PREFIX + '/server/transport_units/{0}.json',
                    saveUrl: URL_PREFIX + '/server/transport_units.json',
                    saveMethod: 'POST',
                    deleteUrl: URL_PREFIX + '/server/transport_units/delete/{0}.json',
                    printItem: URL_PREFIX + '/server/transport_units/{0}',
                    printGrid: URL_PREFIX + '/server/transport_units',
                    help: URL_PREFIX + '/server/transport_units'
                }
            },
            timesheet: { //Табель учета
                proxy: getProxy(URL_PREFIX + '/api/tunittimesheet/list', ''),
                printItem: URL_PREFIX + '/api/tunittimesheet/{0}',
                printGrid: URL_PREFIX + '/api/tunittimesheet',
                help: URL_PREFIX + '/api/tunittimesheet',

                transport_units_proxy: getSyncProxy(URL_PREFIX + '/api/tunittimesheet', null, 'unitlist')
            }
        }, //transport

        products: {
            complect_dict: URL_PREFIX + '/api/product/complect', ///Комплект
            danger_sub_classes_dict: URL_PREFIX + '/api/product/danger_subclasses', //Подклассы опасности
            products: {
                moderation: {
                    proxy: getProxy(URL_PREFIX + '/api/product/moderation', 'products')
                },
                general: {
                    proxy: getProxy(URL_PREFIX + '/api/product/general', 'products')
                },
                personal: {
                    proxy: getProxy(URL_PREFIX + '/api/product/personal', 'products')
                },
                archive: {
                    proxy: getProxy(URL_PREFIX + '/api/product/archive', 'products')
                },
                proxy: getProxy(URL_PREFIX + '/api/product/list', 'products'),
                loadItem: '/api/product/get/{0}',
                toPersonal: URL_PREFIX + '/api/product/topersonal/{0}',
                archiveItem: URL_PREFIX + '/api/product/archive/{0}',
                unArchiveItem: URL_PREFIX + '/api/product/unarchive/{0}',
                deleteItem: URL_PREFIX + '/api/product/deletePersonal/{0}',
                saveUrl: URL_PREFIX + '/api/product/save',
                saveMethod: 'POST',
                deleteUrl: URL_PREFIX + '/api/product/delete/{0}',
                printItem: URL_PREFIX + '/products/{0}',
                printGrid: URL_PREFIX + '/products',
                help: URL_PREFIX + '/products',
                complect_list: getProxy(URL_PREFIX + '/api/product/complectlist', 'products')
            },
            services: {
                proxy: getProxy(URL_PREFIX + '/api/service/list', 'services'),
                moderation: {
                    proxy: getProxy(URL_PREFIX + '/api/service/moderation', 'services')
                },
                general: {
                    proxy: getProxy(URL_PREFIX + '/api/service/general', 'services')
                },
                personal: {
                    proxy: getProxy(URL_PREFIX + '/api/service/personal', 'services')
                },
                archive: {
                    proxy: getProxy(URL_PREFIX + '/api/service/archive', 'services')
                },
                toPersonal: URL_PREFIX + '/api/service/topersonal/{0}',
                archiveItem: URL_PREFIX + '/api/service/archive/{0}',
                unArchiveItem: URL_PREFIX + '/api/service/unarchive/{0}',
                deleteItem: URL_PREFIX + '/api/service/deletePersonal/{0}',
                saveUrl: URL_PREFIX + '/api/service/save',
                saveMethod: 'POST',
                deleteUrl: URL_PREFIX + '/api/service/delete/{0}',
                printItem: URL_PREFIX + '/services/{0}',
                printGrid: URL_PREFIX + '/services',
                help: URL_PREFIX + '/services',
                complect_list: getProxy(URL_PREFIX + '/api/service/complectlist', 'products')
            },
            price: {
                all_prices_dict: URL_PREFIX + '/api/pricelist/list', //Все прайсы, для выбора замены
                all_products_dict: URL_PREFIX + '/api/pricelist/products', //Все товары, для выбора замены
                new_price: {
                    proxy: getProxy(URL_PREFIX + '/api/pricelist/newlist', 'price_lists'),
                    saveUrl: URL_PREFIX + '/api/pricelist/save',
                    saveMethod: 'POST',
                    deleteUrl: URL_PREFIX + '/api/pricelist/delete/{0}',
                    printItem: URL_PREFIX + '/prices/{0}',
                    printGrid: URL_PREFIX + '/prices',
                    help: URL_PREFIX + '/prices'
                },
                current: {
                    proxy: getProxy(URL_PREFIX + '/api/pricelist/inworklist', 'price_lists'),
                    saveUrl: URL_PREFIX + '/api/pricelist/save',
                    saveMethod: 'POST',
                    deleteUrl: URL_PREFIX + '/prices/delete/{0}.json',
                    printItem: URL_PREFIX + '/prices/{0}',
                    printGrid: URL_PREFIX + '/prices',
                    help: URL_PREFIX + '/prices',
                    archiveItem: URL_PREFIX + '/api/pricelist/archive/{0}'
                },
                archive: {
                    proxy: getProxy(URL_PREFIX + '/api/pricelist/archivelist', 'price_lists'),
                    saveUrl: URL_PREFIX + '/api/pricelist/save',
                    saveMethod: 'POST',
                    deleteUrl: URL_PREFIX + '/api/pricelist/delete/{0}',
                    printItem: URL_PREFIX + '/prices/{0}',
                    printGrid: URL_PREFIX + '/prices',
                    help: URL_PREFIX + '/prices',
                    unArchiveItem: URL_PREFIX + '/api/pricelist/unarchive/{0}'
                },
                routes: {
                    proxy: getProxy(URL_PREFIX + '/server/price_routes', 'routes'),
                    saveUrl: URL_PREFIX + '/price_routes.json',
                    saveMethod: 'POST',
                    deleteUrl: URL_PREFIX + '/price_routes/delete/{0}.json',
                    printItem: URL_PREFIX + '/price_routes/{0}',
                    printGrid: URL_PREFIX + '/price_routes',
                    help: URL_PREFIX + '/price_routes'
                }
            },//price
            packtype: {
                proxy: getProxy(URL_PREFIX + '/api/dict/packtypelist', 'packtype'),
                printItem: URL_PREFIX + '/packtype/{0}',
                printGrid: URL_PREFIX + '/packtype',
                help: URL_PREFIX + '/packtype'
            }
        }, //products
        contracts: {
            contracts: {
                all_prices_dict: URL_PREFIX + '/server/all_prices.json', //Все прайсы, для выбора замены
                structure: getProxy(URL_PREFIX + '/server/contracts_structure', ''), //Структура договоров для всплывающего окна - используется также в договорах
                save: { //URL для сохранения разных типов в создавалке, метод POST
                    contracts: URL_PREFIX + '/api/contract/save/{0}',
                    addons: URL_PREFIX + '/api/contractannex/save/{0}',
                    agreements: URL_PREFIX + '/api/contractannex/save/{0}',
                    specs: URL_PREFIX + '/api/contractannex/save/{0}'
                },
                load: { //load URL
                    contracts: URL_PREFIX + '/api/contract/get/{0}',
                    addons: URL_PREFIX + '/api/contractannex/get/{0}',
                    agreements: URL_PREFIX + '/api/contractannex/get/{0}',
                    specs: URL_PREFIX + '/api/contractannex/get/{0}'
                },
                pricelists_proxy: getProxy(URL_PREFIX + '/price_lists.json', 'price_lists'), //Прайс-листы (для создавалки)
                participants: { //Справочник - участники
                    proxy: getProxy(URL_PREFIX + '/server/participants.json', 'participants'),
                    deleteUrl: URL_PREFIX + '/contracts/delete/{0}.json',
                    printItem: URL_PREFIX + '/contracts/{0}',
                    printGrid: URL_PREFIX + '/contracts',
                    help: URL_PREFIX + '/contracts',
                    loadItem: URL_PREFIX + '/server/participant.json?id={0}'
                },
                contracts_new: {  //Новые
                    proxy: getProxy(URL_PREFIX + '/api/contract/list', 'contracts'),
                    deleteUrl: URL_PREFIX + '/api/contract/delete/{0}',
                    printItem: URL_PREFIX + '/contracts/{0}',
                    printGrid: URL_PREFIX + '/contracts',
                    help: URL_PREFIX + '/contracts',
                    directors: URL_PREFIX + '/server/directors.json'
                },
                incoming: { //Входящие
                    proxy: getProxy(URL_PREFIX + '/api/contract/listincoming', 'contracts'),
                    printItem: URL_PREFIX + '/server/contracts/{0}',
                    printGrid: URL_PREFIX + '/server/contracts',
                    help: URL_PREFIX + '/server/contracts',
                    directors: URL_PREFIX + '/server/directors.json',
                    send_to_archive: URL_PREFIX + '/api/contract/archive/{0}' //Отправить в архив
                },
                outcoming: { //Исходящие
                    proxy: getProxy(URL_PREFIX + '/api/contract/listoutcoming', 'contracts'),
                    printItem: URL_PREFIX + '/server/contracts/{0}',
                    printGrid: URL_PREFIX + '/server/contracts',
                    help: URL_PREFIX + '/contracts',
                    send_to_archive: URL_PREFIX + '/api/contract/archive/{0}' //Отправить в архив
                },
                archive: { //Архив
                    proxy: getProxy(URL_PREFIX + '/api/contract/listarchive', 'contracts'),
                    deleteUrl: URL_PREFIX + '/api/contract/deleteArchive/{0}',
                    printItem: URL_PREFIX + '/server/contracts/{0}',
                    printGrid: URL_PREFIX + '/server/contracts',
                    help: URL_PREFIX + '/server/contracts',
                    unArchiveItem: URL_PREFIX + '/api/contract/unarchive/{0}'
                },
                addons: {
                    proxy: getProxy(URL_PREFIX + '/server/contracts.json', 'contracts'),
                    saveUrl: URL_PREFIX + '/server/contracts.json',
                    saveMethod: 'POST',
                    deleteUrl: URL_PREFIX + '/server/contracts/delete/{0}.json',
                    printItem: URL_PREFIX + '/server/contracts/{0}',
                    printGrid: URL_PREFIX + '/server/contracts',
                    help: URL_PREFIX + '/server/contracts'
                }
            },//contracts
            addons: {
                participants: { //Справочник - участники
                    proxy: getProxy(URL_PREFIX + '/server/participants.json', 'participants'),
                    deleteUrl: URL_PREFIX + '/contracts/delete/{0}.json',
                    printItem: URL_PREFIX + '/contracts/{0}',
                    printGrid: URL_PREFIX + '/contracts',
                    help: URL_PREFIX + '/contracts',
                    loadItem: URL_PREFIX + '/server/participant.json?id={0}'
                },
                addons_new: {  //Новые
                    proxy: getProxy(URL_PREFIX + '/api/contractannex/list', 'contracts'),
                    deleteUrl: URL_PREFIX + '/api/contractannex/delete/{0}',
                    printItem: URL_PREFIX + '/contracts/{0}',
                    printGrid: URL_PREFIX + '/contracts',
                    help: URL_PREFIX + '/contracts',
                    directors: URL_PREFIX + '/server/directors.json'
                },
                incoming: { //Входящие
                    proxy: getProxy(URL_PREFIX + '/api/contractannex/listincoming', 'contracts'),
                    printItem: URL_PREFIX + '/server/contracts/{0}',
                    printGrid: URL_PREFIX + '/server/contracts',
                    help: URL_PREFIX + '/server/contracts',
                    directors: URL_PREFIX + '/server/directors.json',
                    send_to_archive: URL_PREFIX + '/api/contractannex/archive/{0}' //Отправить в архив
                },
                outcoming: { //Исходящие
                    proxy: getProxy(URL_PREFIX + '/api/contractannex/listoutcoming', 'contracts'),
                    printItem: URL_PREFIX + '/server/contracts/{0}',
                    printGrid: URL_PREFIX + '/server/contracts',
                    help: URL_PREFIX + '/contracts',
                    send_to_archive: URL_PREFIX + '/api/contractannex/archive/{0}' //Отправить в архив
                },
                archive: { //Архив
                    proxy: getProxy(URL_PREFIX + '/api/contractannex/listarchive', 'contracts'),
                    deleteUrl: URL_PREFIX + '/api/contractannex/deleteArchive/{0}',
                    printItem: URL_PREFIX + '/server/contracts/{0}',
                    printGrid: URL_PREFIX + '/server/contracts',
                    help: URL_PREFIX + '/server/contracts',
                    unArchiveItem: URL_PREFIX + '/api/contractannex/unarchive/{0}'
                },
                addons: {
                    proxy: getProxy(URL_PREFIX + '/server/contracts.json', 'contracts'),
                    saveUrl: URL_PREFIX + '/server/contracts.json',
                    saveMethod: 'POST',
                    deleteUrl: URL_PREFIX + '/server/contracts/delete/{0}.json',
                    printItem: URL_PREFIX + '/server/contracts/{0}',
                    printGrid: URL_PREFIX + '/server/contracts',
                    help: URL_PREFIX + '/server/contracts'
                }
            }
        },//contracts global
        goods: {//Товарооборот
            catalog: {
                saveUrl: URL_PREFIX + '/api/catalogue/save',
                saveMethod: 'POST',
                all_prices_dict: URL_PREFIX + '/api/pricelist/list', //Все прайсы, для выбора замены
                products: {
                    proxy: getProxy(URL_PREFIX + '/api/catalogue/productslist', 'products'),
                    printItem: URL_PREFIX + '/products/{0}',
                    printGrid: URL_PREFIX + '/products',
                    help: URL_PREFIX + '/products'
                }, //goods catalog products
                services: {
                    proxy: getProxy(URL_PREFIX + '/api/catalogue/serviceslist', 'services'),
                    printItem: URL_PREFIX + '/services/{0}',
                    printGrid: URL_PREFIX + '/services',
                    help: URL_PREFIX + '/services'
                } //goods catalog services
            }, //goods catalog
            auto: {
                all_requests_dict: URL_PREFIX + '/api/productrequest/all', //Все заявки (для создавалки)
                all_templates_dict: URL_PREFIX + '/server/goods_auto_all.json', //Все шаблоны (для создавалки)
                suppliers_dict: URL_PREFIX + '/api/productrequest/providertree', //Поставщики
                suppliers_contacts_dict: URL_PREFIX + '/api/employee/structure_v3', //Поставщики контактные лица
                getters_dict: URL_PREFIX + '/api/organization/grouptree',   //Грузополучатели
                getters_contacts_dict: URL_PREFIX + '/api/employee/structure_v3', //Грузополучатели контактные лица
                payers_dict: URL_PREFIX + '/api/organization/grouptree',    //Плательщики
                payers_contacts_dict: URL_PREFIX + '/api/employee/structure_v3', //Плательщики контактные лица
                transport_units_dict: URL_PREFIX + '/server/goods_transport_units.json', //Транспортные единицы
                products_dict: URL_PREFIX + '/api/contract/contractproduct', //Товары
                contracts_dict: URL_PREFIX + '/api/contract/contractsrequesttree', //Договоры
                warehouses_dict: URL_PREFIX + '/api/warehouse/structure', //Склады
                templates: { //Шаблоны
                    proxy: getProxy(URL_PREFIX + '/api/productrequest/new', 'goods'),
                    saveUrl: URL_PREFIX + '/server/goods.json',
                    saveMethod: 'POST',
                    deleteUrl: URL_PREFIX + '/server/goods/delete/{0}.json',
                    printItem: URL_PREFIX + '/server/goods/{0}',
                    printGrid: URL_PREFIX + '/server/goods',
                    help: URL_PREFIX + '/server/goods'
                }, //goods auto templates
                new_r: { //Новые
                    proxy: getProxy(URL_PREFIX + '/api/productrequest/list', 'goods'),
                    saveUrl: URL_PREFIX + '/api/productrequest/save',
                    saveIntermediaryUrl: URL_PREFIX + '/api/productrequest/save_intermediary',
                    setChainEndUrl: URL_PREFIX + '/api/productrequest/set_chain_end',
                    loadItemUrl: URL_PREFIX + '/api/contract/getcurrentorggetter',
                    loadIntermediaryItemUrl: URL_PREFIX + '/api/contract/getcurrentorggetter',
                    saveMethod: 'POST',
                    deleteUrl: URL_PREFIX + '/api/productrequest/delete/{0}',
                    printItem: URL_PREFIX + '/server/goods/{0}',
                    printGrid: URL_PREFIX + '/server/goods',
                    help: URL_PREFIX + '/server/goods'
                }, //goods auto new_r
                incoming: { //Входящие
                    proxy: getProxy(URL_PREFIX + '/api/productrequest/listincoming', 'goods'),
                    saveUrl: URL_PREFIX + '/server/goods.json',
                    saveMethod: 'POST',
                    deleteUrl: URL_PREFIX + '/server/goods/delete/{0}.json',
                    printItem: URL_PREFIX + '/server/goods/{0}',
                    printGrid: URL_PREFIX + '/server/goods',
                    help: URL_PREFIX + '/server/goods',
                    archiveItem: URL_PREFIX + '/api/productrequest/archive/{0}'
                }, //goods auto incoming
                outcoming: { //Исходящие
                    proxy: getProxy(URL_PREFIX + '/api/productrequest/listoutcoming', 'goods'),
                    saveUrl: URL_PREFIX + '/server/goods.json',
                    saveMethod: 'POST',
                    deleteUrl: URL_PREFIX + '/server/goods/delete/{0}.json',
                    printItem: URL_PREFIX + '/server/goods/{0}',
                    printGrid: URL_PREFIX + '/server/goods',
                    help: URL_PREFIX + '/server/goods',
                    archiveItem: URL_PREFIX + '/api/productrequest/archive/{0}'
                }, //goods auto outcoming
                archive: { //Архив
                    proxy: getProxy(URL_PREFIX + '/api/productrequest/listarchive', 'goods'),
                    saveUrl: URL_PREFIX + '/server/goods.json',
                    saveMethod: 'POST',
                    deleteUrl: URL_PREFIX + '/api/productrequest/deleteArchive/{0}',
                    printItem: URL_PREFIX + '/server/goods/{0}',
                    printGrid: URL_PREFIX + '/server/goods',
                    help: URL_PREFIX + '/server/goods',
                    unArchiveItem: URL_PREFIX + '/api/productrequest/unarchive/{0}'
                }, //goods auto archive
                documents: { //Связанные документы
                    proxy: getProxy(URL_PREFIX + '/server/connected_documents.json', 'documents'),
                    printItem: URL_PREFIX + '/server/documents/{0}',
                    printGrid: URL_PREFIX + '/server/documents',
                    help: URL_PREFIX + '/server/documents'
                } //goods auto documents
            }, //goods auto
            railway: {
                carriages_dict: URL_PREFIX + '/server/goods_carriages.json', //Вагоны
                getters_dict: URL_PREFIX + '/server/goods_warehouses.json', //Грузополучатели
                templates: { //Шаблоны
                    proxy: getProxy(URL_PREFIX + '/server/goods_auto.json', 'goods'),
                    saveUrl: URL_PREFIX + '/server/goods.json',
                    saveMethod: 'POST',
                    deleteUrl: URL_PREFIX + '/server/goods/delete/{0}.json',
                    printItem: URL_PREFIX + '/server/goods/{0}',
                    printGrid: URL_PREFIX + '/server/goods',
                    help: URL_PREFIX + '/server/goods'
                }, //goods railway templates
                new_r: { //Новые
                    proxy: getProxy(URL_PREFIX + '/server/goods_auto.json', 'goods'),
                    saveUrl: URL_PREFIX + '/server/goods.json',
                    saveMethod: 'POST',
                    deleteUrl: URL_PREFIX + '/server/goods/delete/{0}.json',
                    printItem: URL_PREFIX + '/server/goods/{0}',
                    printGrid: URL_PREFIX + '/server/goods',
                    help: URL_PREFIX + '/server/goods'
                }, //goods railway new_r
                incoming: { //Входящие
                    proxy: getProxy(URL_PREFIX + '/server/goods_auto.json', 'goods'),
                    saveUrl: URL_PREFIX + '/server/goods.json',
                    saveMethod: 'POST',
                    deleteUrl: URL_PREFIX + '/server/goods/delete/{0}.json',
                    printItem: URL_PREFIX + '/server/goods/{0}',
                    printGrid: URL_PREFIX + '/server/goods',
                    help: URL_PREFIX + '/server/goods',
                    archiveItem: URL_PREFIX + '/server/sign.json?id={0}'
                }, //goods railway incoming
                outcoming: { //Исходящие
                    proxy: getProxy(URL_PREFIX + '/server/goods_auto.json', 'goods'),
                    saveUrl: URL_PREFIX + '/server/goods.json',
                    saveMethod: 'POST',
                    deleteUrl: URL_PREFIX + '/server/goods/delete/{0}.json',
                    printItem: URL_PREFIX + '/server/goods/{0}',
                    printGrid: URL_PREFIX + '/server/goods',
                    help: URL_PREFIX + '/server/goods',
                    archiveItem: URL_PREFIX + '/server/sign.json?id={0}'
                }, //goods railway outcoming
                archive: { //Архив
                    proxy: getProxy(URL_PREFIX + '/server/goods_auto.json', 'goods'),
                    saveUrl: URL_PREFIX + '/server/goods.json',
                    saveMethod: 'POST',
                    deleteUrl: URL_PREFIX + '/server/goods/delete/{0}.json',
                    printItem: URL_PREFIX + '/server/goods/{0}',
                    printGrid: URL_PREFIX + '/server/goods',
                    help: URL_PREFIX + '/server/goods',
                    unArchiveItem: URL_PREFIX + '/server/sign.json?id={0}'
                }, //goods railway archive
                documents: { //Связанные документы
                    proxy: getProxy(URL_PREFIX + '/server/connected_documents.json', 'documents'),
                    printItem: URL_PREFIX + '/server/documents/{0}',
                    printGrid: URL_PREFIX + '/server/documents',
                    help: URL_PREFIX + '/server/documents'
                } //goods railway documents
            }, //goods railway
            services: {
                all_requests_dict: URL_PREFIX + '/api/servicerequest/list', //Все заявки (для создавалки)
                all_templates_dict: URL_PREFIX + '/api/servicerequest/list', //Все шаблоны (для создавалки)
                sellers_dict: URL_PREFIX + '/api/contract/clientcontractstree', //Продавцы
                sellers_contacts_dict: URL_PREFIX + '/api/employee/structure_v3', //Поставщики контактные лица
                services_dict: URL_PREFIX + '/api/pricelist/contractservices', //Услуги
                templates: { //Шаблоны
                    proxy: getProxy(URL_PREFIX + '/api/servicerequest/list', 'goods'),
                    saveUrl: URL_PREFIX + '/api/servicerequest/save',
                    saveMethod: 'POST',
                    deleteUrl: URL_PREFIX + '/server/goods/delete/{0}.json',
                    printItem: URL_PREFIX + '/server/goods/{0}',
                    printGrid: URL_PREFIX + '/server/goods',
                    help: URL_PREFIX + '/server/goods'
                }, //goods services templates
                new_r: { //Новые
                    proxy: getProxy(URL_PREFIX + '/api/servicerequest/list', 'goods'),
                    saveUrl: URL_PREFIX + '/api/servicerequest/save',
                    saveMethod: 'POST',
                    deleteUrl: URL_PREFIX + '/api/servicerequest/delete/{0}',
                    printItem: URL_PREFIX + '/server/goods/{0}',
                    printGrid: URL_PREFIX + '/server/goods',
                    help: URL_PREFIX + '/server/goods'
                }, //goods services new_r
                incoming: { //Входящие
                    proxy: getProxy(URL_PREFIX + '/api/servicerequest/list', 'goods'),
                    saveUrl: URL_PREFIX + '/server/goods.json',
                    saveMethod: 'POST',
                    deleteUrl: URL_PREFIX + '/server/goods/delete/{0}.json',
                    printItem: URL_PREFIX + '/server/goods/{0}',
                    printGrid: URL_PREFIX + '/server/goods',
                    help: URL_PREFIX + '/server/goods',
                    archiveItem: URL_PREFIX + '/server/sign.json?id={0}'
                }, //goods services incoming
                outcoming: { //Исходящие
                    proxy: getProxy(URL_PREFIX + '/api/servicerequest/list', 'goods'),
                    saveUrl: URL_PREFIX + '/server/goods.json',
                    saveMethod: 'POST',
                    deleteUrl: URL_PREFIX + '/server/goods/delete/{0}.json',
                    printItem: URL_PREFIX + '/server/goods/{0}',
                    printGrid: URL_PREFIX + '/server/goods',
                    help: URL_PREFIX + '/server/goods',
                    archiveItem: URL_PREFIX + '/server/sign.json?id={0}'
                }, //goods services outcoming
                archive: { //Архив
                    proxy: getProxy(URL_PREFIX + '/api/servicerequest/list', 'goods'),
                    saveUrl: URL_PREFIX + '/server/goods.json',
                    saveMethod: 'POST',
                    deleteUrl: URL_PREFIX + '/server/goods/delete/{0}.json',
                    printItem: URL_PREFIX + '/server/goods/{0}',
                    printGrid: URL_PREFIX + '/server/goods',
                    help: URL_PREFIX + '/server/goods',
                    unArchiveItem: URL_PREFIX + '/server/sign.json?id={0}'
                }, //goods services archive
                documents: { //Связанные документы
                    proxy: getProxy(URL_PREFIX + '/server/connected_documents.json', 'documents'),
                    printItem: URL_PREFIX + '/server/documents/{0}',
                    printGrid: URL_PREFIX + '/server/documents',
                    help: URL_PREFIX + '/server/documents'
                } //goods services documents
            }, //goods services
            plans: {
                documents_dict: URL_PREFIX + '/server/connected_documents_tree.json', //Связанные документы
                products_dict: URL_PREFIX + '/server/goods_products.json', //Товары
                saveUrl: URL_PREFIX + '/server/goods.json',
                saveMethod: 'POST',
                new_p: { //Новые
                    proxy: getProxy(URL_PREFIX + '/server/goods_plans.json', 'plans'),
                    deleteUrl: URL_PREFIX + '/server/goods/delete/{0}.json',
                    printItem: URL_PREFIX + '/server/goods/{0}',
                    printGrid: URL_PREFIX + '/server/goods',
                    help: URL_PREFIX + '/server/goods'
                }, //goods plans new_p
                incoming: { //Входящие
                    proxy: getProxy(URL_PREFIX + '/server/goods_plans.json', 'plans'),
                    deleteUrl: URL_PREFIX + '/server/goods/delete/{0}.json',
                    printItem: URL_PREFIX + '/server/goods/{0}',
                    printGrid: URL_PREFIX + '/server/goods',
                    help: URL_PREFIX + '/server/goods',
                    archiveItem: URL_PREFIX + '/server/sign.json?id={0}'
                }, //goods plans incoming
                outcoming: { //Исходящие
                    proxy: getProxy(URL_PREFIX + '/server/goods_plans.json', 'plans'),
                    deleteUrl: URL_PREFIX + '/server/goods/delete/{0}.json',
                    printItem: URL_PREFIX + '/server/goods/{0}',
                    printGrid: URL_PREFIX + '/server/goods',
                    help: URL_PREFIX + '/server/goods',
                    archiveItem: URL_PREFIX + '/server/sign.json?id={0}'
                }, //goods plans outcoming
                archive: { //Архив
                    proxy: getProxy(URL_PREFIX + '/server/goods_plans.json', 'plans'),
                    deleteUrl: URL_PREFIX + '/server/goods/delete/{0}.json',
                    printItem: URL_PREFIX + '/server/goods/{0}',
                    printGrid: URL_PREFIX + '/server/goods',
                    help: URL_PREFIX + '/server/goods',
                    unArchiveItem: URL_PREFIX + '/server/sign.json?id={0}'
                }, //goods plans archive
                documents: { //Связанные документы
                    proxy: getProxy(URL_PREFIX + '/server/connected_documents.json', 'documents'),
                    printItem: URL_PREFIX + '/server/documents/{0}',
                    printGrid: URL_PREFIX + '/server/documents',
                    help: URL_PREFIX + '/server/documents'
                } //goods plans documents
            }, //goods plans
            buyers: { //Покупатели
                proxy: getProxy(URL_PREFIX + '/server/goods_firms.json', 'firms'),
                saveUrl: URL_PREFIX + '/server/firms.json',
                saveMethod: 'POST',
                deleteUrl: URL_PREFIX + '/server/firms/delete/{0}.json',
                printItem: URL_PREFIX + '/server/firms/{0}',
                printGrid: URL_PREFIX + '/server/firms',
                help: URL_PREFIX + '/server/firms'
            }, //goods buyers
            sellers: { //Продавцы
                proxy: getProxy(URL_PREFIX + '/server/goods_firms.json', 'firms'),
                saveUrl: URL_PREFIX + '/server/firms.json',
                saveMethod: 'POST',
                deleteUrl: URL_PREFIX + '/server/firms/delete/{0}.json',
                printItem: URL_PREFIX + '/server/firms/{0}',
                printGrid: URL_PREFIX + '/server/firms',
                help: URL_PREFIX + '/server/firms'
            }, //goods sellers
            senders: { //Грузоотправители
                proxy: getProxy(URL_PREFIX + '/server/goods_firms.json', 'firms'),
                saveUrl: URL_PREFIX + '/server/firms.json',
                saveMethod: 'POST',
                deleteUrl: URL_PREFIX + '/server/firms/delete/{0}.json',
                printItem: URL_PREFIX + '/server/firms/{0}',
                printGrid: URL_PREFIX + '/server/firms',
                help: URL_PREFIX + '/server/firms'
            }, //goods senders
            getters: { //Грузополучатели
                proxy: getProxy(URL_PREFIX + '/api/productrequest/listgetters', 'firms'),
                //saveUrl: URL_PREFIX + '/server/firms.json',
                //saveMethod: 'POST',
                //deleteUrl: URL_PREFIX + '/server/firms/delete/{0}.json',
                printItem: URL_PREFIX + '/server/firms/{0}',
                printGrid: URL_PREFIX + '/server/firms',
                help: URL_PREFIX + '/server/firms'
            }, //goods getters
            payers: { //Плательщики
                proxy: getProxy(URL_PREFIX + '/api/productrequest/listpayers', 'firms'),
                //saveUrl: URL_PREFIX + '/server/firms.json',
                //saveMethod: 'POST',
                //deleteUrl: URL_PREFIX + '/server/firms/delete/{0}.json',
                printItem: URL_PREFIX + '/server/firms/{0}',
                printGrid: URL_PREFIX + '/server/firms',
                help: URL_PREFIX + '/server/firms'
            }, //goods payers
            request_products: { //Товары
                proxy: getProxy(URL_PREFIX + '/api/productrequest/listproducts', 'firms'),
                printItem: URL_PREFIX + '/server/firms/{0}',
                printGrid: URL_PREFIX + '/server/firms',
                help: URL_PREFIX + '/server/firms'
            }, //goods request_products
            request_services: { //Услуги
                proxy: getProxy(URL_PREFIX + '/api/productrequest/listservices', 'firms'),
                printItem: URL_PREFIX + '/server/firms/{0}',
                printGrid: URL_PREFIX + '/server/firms',
                help: URL_PREFIX + '/server/firms'
            }, //goods request_products
            suppliers: { //Поставщики
                proxy: getProxy(URL_PREFIX + '/api/productrequest/listproviders', 'firms'),
                //saveUrl: URL_PREFIX + '/server/firms.json',
                //saveMethod: 'POST',
                //deleteUrl: URL_PREFIX + '/server/firms/delete/{0}.json',
                printItem: URL_PREFIX + '/server/firms/{0}',
                printGrid: URL_PREFIX + '/server/firms',
                help: URL_PREFIX + '/server/firms'
            }, //goods suppliers
            transport_units: { // Транспортные единицы
                proxy: getProxy(URL_PREFIX + '/api/productrequest/listtrunits', 'firms'),
                printItem: URL_PREFIX + '/server/firms/{0}',
                printGrid: URL_PREFIX + '/server/firms',
                help: URL_PREFIX + '/server/firms'
            }, //goods transport_units
            warehouses: { // Склад грузополучателя
                proxy: getProxy(URL_PREFIX + '/api/productrequest/listwarehouses', 'firms'),
                printItem: URL_PREFIX + '/server/firms/{0}',
                printGrid: URL_PREFIX + '/server/firms',
                help: URL_PREFIX + '/server/firms'
            }, //goods warehouses
            routes: { //Маршруты
                proxy: getProxy(URL_PREFIX + '/api/productrequest/listroutes', 'routes'),
                //saveUrl: URL_PREFIX + '/price_routes.json',
                //saveMethod: 'POST',
                //deleteUrl: URL_PREFIX + '/price_routes/delete/{0}.json',
                printItem: URL_PREFIX + '/price_routes/{0}',
                printGrid: URL_PREFIX + '/price_routes',
                help: URL_PREFIX + '/price_routes'
            } //goods routes
        },//goods
        cargo: {//Грузооборот
            cities_dict: URL_PREFIX + '/server/cities.json',
            dispatchers_dict: URL_PREFIX + '/server/dispatchers.json',
            transport_units_dict: URL_PREFIX + '/server/goods_transport_units.json',
            requests_dict: URL_PREFIX + '/server/goods_auto_all.json', //заявки (для создавалки)
            contracts_dict: URL_PREFIX + '/server/cargo_contracts.json', //договора и услуги (для создавалки)
            prices_dict: URL_PREFIX + '/server/cargo_price.json', //прайс-листы и услуги (для создавалки)
            goods_lot: {
                detailUrl: URL_PREFIX + '/server/cargo_lot_detail.json?id={0}',
                new_l: { //Новые
                    proxy: getProxy(URL_PREFIX + '/server/goods_lot.json', 'lots'),
                    saveUrl: URL_PREFIX + '/server/goods.json',
                    saveMethod: 'POST',
                    deleteUrl: URL_PREFIX + '/server/goods/delete/{0}.json',
                    printItem: URL_PREFIX + '/server/goods/{0}',
                    printGrid: URL_PREFIX + '/server/goods',
                    help: URL_PREFIX + '/server/goods',
                    bidUrl: URL_PREFIX + '/server/sign.json'
                }, //cargo goods_lot new_r
                incoming: { //Входящие
                    proxy: getProxy(URL_PREFIX + '/server/goods_lot.json', 'lots'),
                    saveUrl: URL_PREFIX + '/server/goods.json',
                    saveMethod: 'POST',
                    deleteUrl: URL_PREFIX + '/server/goods/delete/{0}.json',
                    printItem: URL_PREFIX + '/server/goods/{0}',
                    printGrid: URL_PREFIX + '/server/goods',
                    help: URL_PREFIX + '/server/goods',
                    archiveItem: URL_PREFIX + '/server/sign.json?id={0}'
                }, //cargo goods_lot incoming
                outcoming: { //Исходящие
                    proxy: getProxy(URL_PREFIX + '/server/goods_lot.json', 'lots'),
                    saveUrl: URL_PREFIX + '/server/goods.json',
                    saveMethod: 'POST',
                    deleteUrl: URL_PREFIX + '/server/goods/delete/{0}.json',
                    printItem: URL_PREFIX + '/server/goods/{0}',
                    printGrid: URL_PREFIX + '/server/goods',
                    help: URL_PREFIX + '/server/goods',
                    archiveItem: URL_PREFIX + '/server/sign.json?id={0}'
                }, //cargo goods_lot outcoming
                archive: { //Архив
                    proxy: getProxy(URL_PREFIX + '/server/goods_lot.json', 'lots'),
                    saveUrl: URL_PREFIX + '/server/goods.json',
                    saveMethod: 'POST',
                    deleteUrl: URL_PREFIX + '/server/goods/delete/{0}.json',
                    printItem: URL_PREFIX + '/server/goods/{0}',
                    printGrid: URL_PREFIX + '/server/goods',
                    help: URL_PREFIX + '/server/goods',
                    unArchiveItem: URL_PREFIX + '/server/sign.json?id={0}'
                }, //cargo goods_lot archive
                requests: { //Заявки
                    proxy: getProxy(URL_PREFIX + '/server/goods_auto_cargo.json', 'goods'),
                    printItem: URL_PREFIX + '/server/goods/{0}',
                    printGrid: URL_PREFIX + '/server/goods',
                    help: URL_PREFIX + '/server/goods'
                }, //cargo goods_lot requests
                documents: { //Связанные документы
                    proxy: getProxy(URL_PREFIX + '/server/connected_documents.json', 'documents'),
                    printItem: URL_PREFIX + '/server/documents/{0}',
                    printGrid: URL_PREFIX + '/server/documents',
                    help: URL_PREFIX + '/server/documents'
                } //cargo goods_lot documents
            }, //cargo goods_lot
            transport_lot: {
                new_l: { //Новые
                    proxy: getProxy(URL_PREFIX + '/server/transport_lot.json', 'lots'),
                    saveUrl: URL_PREFIX + '/server/goods.json',
                    saveMethod: 'POST',
                    deleteUrl: URL_PREFIX + '/server/goods/delete/{0}.json',
                    printItem: URL_PREFIX + '/server/goods/{0}',
                    printGrid: URL_PREFIX + '/server/goods',
                    help: URL_PREFIX + '/server/goods',
                    bidUrl: URL_PREFIX + '/server/sign.json'
                }, //cargo transport_lot new_r
                incoming: { //Входящие
                    proxy: getProxy(URL_PREFIX + '/server/transport_lot.json', 'lots'),
                    saveUrl: URL_PREFIX + '/server/goods.json',
                    saveMethod: 'POST',
                    deleteUrl: URL_PREFIX + '/server/goods/delete/{0}.json',
                    printItem: URL_PREFIX + '/server/goods/{0}',
                    printGrid: URL_PREFIX + '/server/goods',
                    help: URL_PREFIX + '/server/goods',
                    archiveItem: URL_PREFIX + '/server/sign.json?id={0}'
                }, //cargo transport_lot incoming
                outcoming: { //Исходящие
                    proxy: getProxy(URL_PREFIX + '/server/transport_lot.json', 'lots'),
                    saveUrl: URL_PREFIX + '/server/goods.json',
                    saveMethod: 'POST',
                    deleteUrl: URL_PREFIX + '/server/goods/delete/{0}.json',
                    printItem: URL_PREFIX + '/server/goods/{0}',
                    printGrid: URL_PREFIX + '/server/goods',
                    help: URL_PREFIX + '/server/goods',
                    archiveItem: URL_PREFIX + '/server/sign.json?id={0}'
                }, //cargo transport_lot outcoming
                archive: { //Архив
                    proxy: getProxy(URL_PREFIX + '/server/transport_lot.json', 'lots'),
                    saveUrl: URL_PREFIX + '/server/goods.json',
                    saveMethod: 'POST',
                    deleteUrl: URL_PREFIX + '/server/goods/delete/{0}.json',
                    printItem: URL_PREFIX + '/server/goods/{0}',
                    printGrid: URL_PREFIX + '/server/goods',
                    help: URL_PREFIX + '/server/goods',
                    unArchiveItem: URL_PREFIX + '/server/sign.json?id={0}'
                }, //cargo transport_lot archive
                units: { //ТЕ
                    proxy: getProxy(URL_PREFIX + '/server/transport_units_cargo.json', 'transport_units'),
                    detailUrl: URL_PREFIX + '/server/transport_unit_detail.json?id={0}',
                    loadItem: URL_PREFIX + '/server/transport_units/{0}.json',
                    printItem: URL_PREFIX + '/server/transport_units/{0}',
                    printGrid: URL_PREFIX + '/server/transport_units',
                    help: URL_PREFIX + '/server/transport_units'
                }, //cargo transport_lot units
                documents: { //Связанные документы
                    proxy: getProxy(URL_PREFIX + '/server/connected_documents.json', 'documents'),
                    printItem: URL_PREFIX + '/server/documents/{0}',
                    printGrid: URL_PREFIX + '/server/documents',
                    help: URL_PREFIX + '/server/documents'
                } //cargo transport_lot documents
            }, //cargo transport_lot
            goods_exchange: { //Биржа грузов
                contract: { //Контрактная
                    proxy: getProxy(URL_PREFIX + '/server/goods_exchange.json', 'exchange'),
                    right_proxy: getProxy(URL_PREFIX + '/server/goods_right.json', 'items'),
                    linkUrl: URL_PREFIX + '/server/link_items.json?left={0}&right={1}',
                    saveUrl: URL_PREFIX + '/server/goods.json',
                    saveMethod: 'POST',
                    deleteUrl: URL_PREFIX + '/server/goods/delete/{0}.json',
                    printItem: URL_PREFIX + '/server/goods/{0}',
                    printGrid: URL_PREFIX + '/server/goods',
                    help: URL_PREFIX + '/server/goods',
                    moveToCorporate: URL_PREFIX + '/server/link_items.json?id={0}',
                    moveToCommon: URL_PREFIX + '/server/link_items.json?id={0}'
                }, //cargo goods_exchange contract
                corporate: { //Корпоративная
                    proxy: getProxy(URL_PREFIX + '/server/goods_exchange.json', 'exchange'),
                    right_proxy: getProxy(URL_PREFIX + '/server/goods_right.json', 'items'),
                    saveUrl: URL_PREFIX + '/server/goods.json',
                    saveMethod: 'POST',
                    deleteUrl: URL_PREFIX + '/server/goods/delete/{0}.json',
                    printItem: URL_PREFIX + '/server/goods/{0}',
                    printGrid: URL_PREFIX + '/server/goods',
                    help: URL_PREFIX + '/server/goods',
                    archiveItem: URL_PREFIX + '/server/sign.json?id={0}',
                    linkUrl: URL_PREFIX + '/server/link_items.json?left={0}&right={1}',
                    moveToContract: URL_PREFIX + '/server/link_items.json?id={0}'
                }, //cargo goods_exchange corporate
                common: { //Общая
                    proxy: getProxy(URL_PREFIX + '/server/goods_exchange.json', 'exchange'),
                    right_proxy: getProxy(URL_PREFIX + '/server/goods_right.json', 'items'),
                    saveUrl: URL_PREFIX + '/server/goods.json',
                    saveMethod: 'POST',
                    deleteUrl: URL_PREFIX + '/server/goods/delete/{0}.json',
                    printItem: URL_PREFIX + '/server/goods/{0}',
                    printGrid: URL_PREFIX + '/server/goods',
                    help: URL_PREFIX + '/server/goods',
                    linkUrl: URL_PREFIX + '/server/link_items.json?left={0}&right={1}',
                    moveToContract: URL_PREFIX + '/server/link_items.json?id={0}'
                } //cargo goods_exchange common
            },//cargo goods_exchange
            transport_exchange: { //Биржа грузов
                contract: { //Контрактная
                    proxy: getProxy(URL_PREFIX + '/server/transport_exchange.json', 'exchange'),
                    right_proxy: getProxy(URL_PREFIX + '/server/transport_right.json', 'items'),
                    saveUrl: URL_PREFIX + '/server/goods.json',
                    saveMethod: 'POST',
                    deleteUrl: URL_PREFIX + '/server/goods/delete/{0}.json',
                    printItem: URL_PREFIX + '/server/goods/{0}',
                    printGrid: URL_PREFIX + '/server/goods',
                    help: URL_PREFIX + '/server/goods',
                    linkUrl: URL_PREFIX + '/server/link_items.json?left={0}&right={1}',
                    moveToCorporate: URL_PREFIX + '/server/link_items.json?id={0}',
                    moveToCommon: URL_PREFIX + '/server/link_items.json?id={0}'
                }, //cargo transport_exchange contract
                corporate: { //Корпоративная
                    proxy: getProxy(URL_PREFIX + '/server/transport_exchange.json', 'exchange'),
                    right_proxy: getProxy(URL_PREFIX + '/server/transport_right.json', 'items'),
                    saveUrl: URL_PREFIX + '/server/goods.json',
                    saveMethod: 'POST',
                    deleteUrl: URL_PREFIX + '/server/goods/delete/{0}.json',
                    printItem: URL_PREFIX + '/server/goods/{0}',
                    printGrid: URL_PREFIX + '/server/goods',
                    help: URL_PREFIX + '/server/goods',
                    archiveItem: URL_PREFIX + '/server/sign.json?id={0}',
                    linkUrl: URL_PREFIX + '/server/link_items.json?left={0}&right={1}',
                    moveToContract: URL_PREFIX + '/server/link_items.json?id={0}'
                }, //cargo transport_exchange corporate
                common: { //Общая
                    proxy: getProxy(URL_PREFIX + '/server/transport_exchange.json', 'exchange'),
                    right_proxy: getProxy(URL_PREFIX + '/server/transport_right.json', 'items'),
                    saveUrl: URL_PREFIX + '/server/goods.json',
                    saveMethod: 'POST',
                    deleteUrl: URL_PREFIX + '/server/goods/delete/{0}.json',
                    printItem: URL_PREFIX + '/server/goods/{0}',
                    printGrid: URL_PREFIX + '/server/goods',
                    help: URL_PREFIX + '/server/goods',
                    linkUrl: URL_PREFIX + '/server/link_items.json?left={0}&right={1}',
                    moveToContract: URL_PREFIX + '/server/link_items.json?id={0}'
                } //cargo transport_exchange common
            },//cargo transport_exchange
            participants: {
                new_p: {
                    proxy: getProxy(URL_PREFIX + '/server/goods_firms.json', 'firms'),
                    saveUrl: URL_PREFIX + '/server/firms.json',
                    saveMethod: 'POST',
                    deleteUrl: URL_PREFIX + '/server/firms/delete/{0}.json',
                    printItem: URL_PREFIX + '/server/firms/{0}',
                    printGrid: URL_PREFIX + '/server/firms',
                    help: URL_PREFIX + '/server/firms'
                }, // cargo participants new_p
                current: {
                    proxy: getProxy(URL_PREFIX + '/server/goods_firms.json', 'firms'),
                    saveUrl: URL_PREFIX + '/server/firms.json',
                    saveMethod: 'POST',
                    deleteUrl: URL_PREFIX + '/server/firms/delete/{0}.json',
                    printItem: URL_PREFIX + '/server/firms/{0}',
                    printGrid: URL_PREFIX + '/server/firms',
                    help: URL_PREFIX + '/server/firms',
                    archiveUrl: URL_PREFIX + '/server/sign.json'
                }, // cargo participants current
                archive: {
                    proxy: getProxy(URL_PREFIX + '/server/goods_firms.json', 'firms'),
                    saveUrl: URL_PREFIX + '/server/firms.json',
                    saveMethod: 'POST',
                    deleteUrl: URL_PREFIX + '/server/firms/delete/{0}.json',
                    printItem: URL_PREFIX + '/server/firms/{0}',
                    printGrid: URL_PREFIX + '/server/firms',
                    help: URL_PREFIX + '/server/firms',
                    unArchiveUrl: URL_PREFIX + '/server/sign.json'
                }, // cargo participants archive
                documents: { //Связанные документы
                    proxy: getProxy(URL_PREFIX + '/server/connected_documents.json', 'documents'),
                    printItem: URL_PREFIX + '/server/documents/{0}',
                    printGrid: URL_PREFIX + '/server/documents',
                    help: URL_PREFIX + '/server/documents'
                } //cargo participants documents
            }, //cargo participants
            buyers: { //Покупатели
                proxy: getProxy(URL_PREFIX + '/server/goods_firms.json', 'firms'),
                saveUrl: URL_PREFIX + '/server/firms.json',
                saveMethod: 'POST',
                deleteUrl: URL_PREFIX + '/server/firms/delete/{0}.json',
                printItem: URL_PREFIX + '/server/firms/{0}',
                printGrid: URL_PREFIX + '/server/firms',
                help: URL_PREFIX + '/server/firms'
            }, //cargo buyers
            sellers: { //Продавцы
                proxy: getProxy(URL_PREFIX + '/server/goods_firms.json', 'firms'),
                saveUrl: URL_PREFIX + '/server/firms.json',
                saveMethod: 'POST',
                deleteUrl: URL_PREFIX + '/server/firms/delete/{0}.json',
                printItem: URL_PREFIX + '/server/firms/{0}',
                printGrid: URL_PREFIX + '/server/firms',
                help: URL_PREFIX + '/server/firms'
            }, //cargo sellers
            routes: { //Маршруты
                proxy: getProxy(URL_PREFIX + '/server/price_routes.json', 'routes'),
                saveUrl: URL_PREFIX + '/price_routes.json',
                saveMethod: 'POST',
                deleteUrl: URL_PREFIX + '/price_routes/delete/{0}.json',
                printItem: URL_PREFIX + '/price_routes/{0}',
                printGrid: URL_PREFIX + '/price_routes',
                help: URL_PREFIX + '/price_routes'
            } //cargo routes
        },//cargo
        office_work: {//Делопроизводство
            documents: {
                nomenklatures_dict: URL_PREFIX + '/api/nomenclature/tree',
                documents_dict: URL_PREFIX + '/api/document/tree',
                new_d: { //Новые
                    proxy: getProxy(URL_PREFIX + '/api/internaldoc/list', 'documents'),
                    saveUrl: URL_PREFIX + '/api/internaldoc/save',
                    saveMethod: 'POST',
                    deleteUrl: URL_PREFIX + '/api/internaldoc/delete/{0}',
                    printItem: URL_PREFIX + '/server/office_work/{0}',
                    printGrid: URL_PREFIX + '/server/office_work',
                    help: URL_PREFIX + '/server/office_work'
                }, //office_work documents new_r
                incoming: { //Входящие
                    proxy: getProxy(URL_PREFIX + '/api/internaldoc/listincoming', 'documents'),
                    deleteUrl: URL_PREFIX + '/server/office_work/delete/{0}.json',
                    printItem: URL_PREFIX + '/server/office_work/{0}',
                    printGrid: URL_PREFIX + '/server/office_work',
                    help: URL_PREFIX + '/server/office_work',
                    archiveItem: URL_PREFIX + '/api/internaldoc/archive/{0}'
                }, //office_work documents incoming
                outcoming: { //Исходящие
                    proxy: getProxy(URL_PREFIX + '/api/internaldoc/listoutcoming', 'documents'),
                    deleteUrl: URL_PREFIX + '/server/office_work/delete/{0}.json',
                    printItem: URL_PREFIX + '/server/office_work/{0}',
                    printGrid: URL_PREFIX + '/server/office_work',
                    help: URL_PREFIX + '/server/office_work',
                    archiveItem: URL_PREFIX + '/api/internaldoc/archive/{0}'
                }, //office_work documents outcoming
                archive: { //Архив
                    proxy: getProxy(URL_PREFIX + '/api/internaldoc/listarchive', 'documents'),
                    deleteUrl: URL_PREFIX + '/api/internaldoc/deleteArchive/{0}',
                    printItem: URL_PREFIX + '/server/office_work/{0}',
                    printGrid: URL_PREFIX + '/server/office_work',
                    help: URL_PREFIX + '/server/office_work',
                    unArchiveItem: URL_PREFIX + '/api/internaldoc/unarchive/{0}'
                }, //office_work documents archive
                documents: { //Связанные документы
                    proxy: getProxy(URL_PREFIX + '/api/docrelation/list', 'documents'),
                    printItem: URL_PREFIX + '/server/documents/{0}',
                    printGrid: URL_PREFIX + '/server/documents',
                    help: URL_PREFIX + '/server/documents'
                }, //office_work documents documents
                routes: { //Маршруты
                    proxy: getProxy(URL_PREFIX + '/api/internaldoc/listroutes', 'routes'),
                    saveUrl: URL_PREFIX + '/price_routes.json',
                    saveMethod: 'POST',
                    deleteUrl: URL_PREFIX + '/price_routes/delete/{0}.json',
                    printItem: URL_PREFIX + '/price_routes/{0}',
                    printGrid: URL_PREFIX + '/price_routes',
                    help: URL_PREFIX + '/price_routes'
                } //office_work documents routes
            }, //office_work documents
            correspondention: { //Корреспонденция
                new_c: { //Новые
                    proxy: getProxy(URL_PREFIX + '/api/correspondence/list', 'documents'),
                    saveUrl: URL_PREFIX + '/api/correspondence/save',
                    saveMethod: 'POST',
                    deleteUrl: URL_PREFIX + '/api/correspondence/delete/{0}',
                    printItem: URL_PREFIX + '/server/office_work/{0}',
                    printGrid: URL_PREFIX + '/server/office_work',
                    help: URL_PREFIX + '/server/office_work'
                }, //office_work correspondention new_c
                incoming: { //Входящие
                    proxy: getProxy(URL_PREFIX + '/api/correspondence/listincoming', 'documents'),
                    deleteUrl: URL_PREFIX + '/server/office_work/delete/{0}.json',
                    printItem: URL_PREFIX + '/server/office_work/{0}',
                    printGrid: URL_PREFIX + '/server/office_work',
                    help: URL_PREFIX + '/server/office_work',
                    archiveItem: URL_PREFIX + '/api/correspondence/archive/{0}'
                }, //office_work correspondention incoming
                outcoming: { //Исходящие
                    proxy: getProxy(URL_PREFIX + '/api/correspondence/listoutcoming', 'documents'),
                    deleteUrl: URL_PREFIX + '/server/office_work/delete/{0}.json',
                    printItem: URL_PREFIX + '/server/office_work/{0}',
                    printGrid: URL_PREFIX + '/server/office_work',
                    help: URL_PREFIX + '/server/office_work',
                    archiveItem: URL_PREFIX + '/api/correspondence/archive/{0}'
                }, //office_work correspondention outcoming
                archive: { //Архив
                    proxy: getProxy(URL_PREFIX + '/api/correspondence/listarchive', 'documents'),
                    deleteUrl: URL_PREFIX + '/api/correspondence/deleteArchive/{0}',
                    printItem: URL_PREFIX + '/server/office_work/{0}',
                    printGrid: URL_PREFIX + '/server/office_work',
                    help: URL_PREFIX + '/server/office_work',
                    unArchiveItem: URL_PREFIX + '/api/correspondence/unarchive/{0}'
                }, //office_work correspondention archive
                documents: { //Связанные документы
                    proxy: getProxy(URL_PREFIX + '/api/docrelation/list', 'documents'),
                    printItem: URL_PREFIX + '/server/documents/{0}',
                    printGrid: URL_PREFIX + '/server/documents',
                    help: URL_PREFIX + '/server/documents'
                }, //office_work correspondention documents
                routes: { //Маршруты
                    proxy: getProxy(URL_PREFIX + '/api/correspondence/listroutes', 'routes'),
                    saveUrl: URL_PREFIX + '/price_routes.json',
                    saveMethod: 'POST',
                    deleteUrl: URL_PREFIX + '/price_routes/delete/{0}.json',
                    printItem: URL_PREFIX + '/price_routes/{0}',
                    printGrid: URL_PREFIX + '/price_routes',
                    help: URL_PREFIX + '/price_routes'
                } //office_work documents routes
            }, //office_work correspondention
            disposal: {
                nomenklatures_dict: URL_PREFIX + '/api/nomenclature/tree',
                documents_dict: URL_PREFIX + '/api/document/tree',
                new_d: { //Новые
                    proxy: getProxy(URL_PREFIX + '/api/disposal/list', 'documents'),
                    saveUrl: URL_PREFIX + '/api/disposal/save',
                    saveMethod: 'POST',
                    deleteUrl: URL_PREFIX + '/api/disposal/delete/{0}',
                    printItem: URL_PREFIX + '/server/office_work/{0}',
                    printGrid: URL_PREFIX + '/server/office_work',
                    help: URL_PREFIX + '/server/office_work'
                }, //office_work disposal new_r
                incoming: { //Входящие
                    proxy: getProxy(URL_PREFIX + '/api/disposal/listincoming', 'documents'),
                    printItem: URL_PREFIX + '/server/office_work/{0}',
                    printGrid: URL_PREFIX + '/server/office_work',
                    help: URL_PREFIX + '/server/office_work',
                    archiveItem: URL_PREFIX + '/api/disposal/archive/{0}'
                }, //office_work disposal incoming
                outcoming: { //Исходящие
                    proxy: getProxy(URL_PREFIX + '/api/disposal/listoutcoming', 'documents'),
                    printItem: URL_PREFIX + '/server/office_work/{0}',
                    printGrid: URL_PREFIX + '/server/office_work',
                    help: URL_PREFIX + '/server/office_work',
                    archiveItem: URL_PREFIX + '/api/disposal/archive/{0}'
                }, //office_work disposal outcoming
                archive: { //Архив
                    proxy: getProxy(URL_PREFIX + '/api/disposal/listarchive', 'documents'),
                    deleteUrl: URL_PREFIX + '/api/internaldoc/deleteArchive/{0}',
                    printItem: URL_PREFIX + '/server/office_work/{0}',
                    printGrid: URL_PREFIX + '/server/office_work',
                    help: URL_PREFIX + '/server/office_work',
                    unArchiveItem: URL_PREFIX + '/api/disposal/unarchive/{0}'
                }, //office_work disposal archive
                documents: { //Связанные документы
                    proxy: getProxy(URL_PREFIX + '/api/docrelation/list', 'documents'),
                    printItem: URL_PREFIX + '/server/documents/{0}',
                    printGrid: URL_PREFIX + '/server/documents',
                    help: URL_PREFIX + '/server/documents'
                }, //office_work disposal documents
                routes: { //Маршруты
                    proxy: getProxy(URL_PREFIX + '/api/disposal/listroutes', 'routes'),
                    printItem: URL_PREFIX + '/price_routes/{0}',
                    printGrid: URL_PREFIX + '/price_routes',
                    help: URL_PREFIX + '/price_routes'
                } //office_work disposal routes
            }, //office_work disposal
            routes: { //Маршруты
                proxy: getProxy(URL_PREFIX + '/api/internaldoc/listroutes', 'routes'),
                saveUrl: URL_PREFIX + '/price_routes.json',
                saveMethod: 'POST',
                deleteUrl: URL_PREFIX + '/price_routes/delete/{0}.json',
                printItem: URL_PREFIX + '/price_routes/{0}',
                printGrid: URL_PREFIX + '/price_routes',
                help: URL_PREFIX + '/price_routes'
            } //office_work routes
        },//office_work

        // Склад
        warehouse: {
            // внутренние заявки
            internal: {
                stockroom_products_proxy: getProxy(URL_PREFIX + '/api/stockroomproduct/list'), //Товары Склада
                products_dict: URL_PREFIX + '/api/pricelist/warehouse_product', //Товары
                pricelist_dict: URL_PREFIX + '/api/pricelist/warehouse_pricelist', //Прайслисты
                causes_dict: URL_PREFIX + '/api/document/warehouse_causes', //Основания
                warehouses_dict: URL_PREFIX + '/api/warehouse/structure', //Склады
                warehouse_contacts_dict: URL_PREFIX + '/api/employee/warehouse_staff', //контактные лица
                new_i: buildConfig('stockroomrequest', { get: true }, {}),
                incoming: buildConfig('stockroomrequest', { listAction: 'listincoming' }),
                outcoming: buildConfig('stockroomrequest', { listAction: 'listoutcoming' }),
                archive: buildConfig('stockroomrequest', { listAction: 'listarchive' }),
                products: { proxy: getProxy(URL_PREFIX + '/api/stockroomrequest/products', '') },
                services: { proxy: getProxy(URL_PREFIX + '/api/stockroomrequest/services', '') },
                warehouses: { proxy: getProxy(URL_PREFIX + '/api/stockroomrequest/warehouses', '') },
                routes: { proxy: getProxy(URL_PREFIX + '/api/stockroomrequest/listroutes', '') },
                documents: { proxy: getProxy(URL_PREFIX + '/api/warehouse/documents', '') }
            },

            inventory: {
                products_dict: URL_PREFIX + '/api/product/warehouse_product', //Товары
                warehouses_dict: URL_PREFIX + '/api/warehouse/structure', //Склады
                employee_dict: URL_PREFIX + '/api/employee/structure_v2',
                new_i: buildConfig('inventory', { get: true }, {
                    stockRoomProductsProxy: getProxy(URL_PREFIX + '/api/stockroomproduct/inventory_list', '')
                }),
                incoming: {
                    proxy: getProxy(URL_PREFIX + '/api/inventory/listincoming', ''),
                    deleteUrl: URL_PREFIX + '/api/inventory/delete/{0}',
                    printItem: URL_PREFIX + '/inventory/print_item',
                    printGrid: URL_PREFIX + '/inventory/print_grid',
                    help: URL_PREFIX + '/inventory/help',
                    archiveItem: URL_PREFIX + '/api/inventory/archive/{0}'
                },
                outcoming: {
                    proxy: getProxy(URL_PREFIX + '/api/inventory/listoutcoming', ''),
                    deleteUrl: URL_PREFIX + '/api/inventory/delete/{0}',
                    printItem: URL_PREFIX + '/inventory/print_item',
                    printGrid: URL_PREFIX + '/inventory/print_grid',
                    help: URL_PREFIX + '/inventory/help',
                    archiveItem: URL_PREFIX + '/api/inventory/archive/{0}'
                },
                archive: {
                    proxy: getProxy(URL_PREFIX + '/api/inventory/listarchive', ''),
                    deleteUrl: URL_PREFIX + '/api/inventory/delete/{0}',
                    printItem: URL_PREFIX + '/inventory/print_item',
                    printGrid: URL_PREFIX + '/inventory/print_grid',
                    help: URL_PREFIX + '/inventory/help',
                    unArchiveItem: URL_PREFIX + '/api/inventory/unarchive/{0}'
                },
                products: {
                    proxy: getProxy(URL_PREFIX + '/api/inventory/products', '')
                },
                services: {
                    proxy: getProxy(URL_PREFIX + '/api/inventory/services', '')
                },
                warehouses: {
                    proxy: getProxy(URL_PREFIX + '/api/inventory/warehouses', '')
                },
                routes: {
                    proxy: getProxy(URL_PREFIX + '/api/inventory/listroutes', '')
                },
                documents: {
                    proxy: getProxy(URL_PREFIX + '/api/warehouse/documents', '')
                }
            },

            movement: {
                new_i: buildConfig('stockroomproducthistory'),
                incoming: buildConfig(''),
                outcoming: buildConfig(''),
                archive: buildConfig(''),
                movements: { proxy: getProxy(URL_PREFIX + '/api/stockroomproducthistory/detaillist', '') },
                documents: { proxy: getProxy(URL_PREFIX + '/api/warehouse/documents', '') }
            },

            receipt: {
                new_i: buildConfig('stockreceiptorder', { get: true }, {
                    signedBuyDataProxy: getProxy(URL_PREFIX + '/api/stockreceiptorder/listsignedbuydata', ''),
                    signedMovementDataProxy: getProxy(URL_PREFIX + '/api/stockreceiptorder/listsignedmovementdata', ''),
                    signedPostingDataProxy: getProxy(URL_PREFIX + '/api/stockreceiptorder/listsignedpostingdata', '')
                }),
                incoming: buildConfig('stockreceiptorder', { listAction: 'listincoming' }),
                outcoming: buildConfig('stockreceiptorder', { listAction: 'listoutcoming' }),
                archive: buildConfig('stockreceiptorder', { listAction: 'listarchive' }),
                products: { proxy: getProxy(URL_PREFIX + '/api/stockreceiptorder/products', '') },
                services: { proxy: getProxy(URL_PREFIX + '/api/stockreceiptorder/services', '') },
                warehouses: { proxy: getProxy(URL_PREFIX + '/api/stockreceiptorder/warehouses', '') },
                routes: { proxy: getProxy(URL_PREFIX + '/api/stockreceiptorder/listroutes', '') },
                providers: { proxy: getProxy(URL_PREFIX + '/api/stockreceiptorder/providers', '') },
                documents: { proxy: getProxy(URL_PREFIX + '/api/stockreceiptorder/documents', '') }
            },

            expense: {
                new_i: buildConfig('stockexpenseorder', { get: true }, {
                    signedWriteoffRequestProxy: getProxy(URL_PREFIX + '/api/stockroomrequest/listsignedwriteoffrequest', ''),
                    signedMovementRequestProxy: getProxy(URL_PREFIX + '/api/stockroomrequest/listsignedmovementrequest', ''),
                    signedSaleRequestProxy: getProxy(URL_PREFIX + '/api/stockroomrequest/listsignedsalerequest', '')
                }),
                incoming: buildConfig('stockexpenseorder', { listAction: 'listincoming' }),
                outcoming: buildConfig('stockexpenseorder', { listAction: 'listoutcoming' }),
                archive: buildConfig('stockexpenseorder', { listAction: 'listarchive' }),
                products: { proxy: getProxy(URL_PREFIX + '/api/stockexpenseorder/products', '') },
                services: { proxy: getProxy(URL_PREFIX + '/api/stockexpenseorder/services', '') },
                warehouses: { proxy: getProxy(URL_PREFIX + '/api/stockexpenseorder/warehouses', '') },
                routes: { proxy: getProxy(URL_PREFIX + '/api/stockexpenseorder/listroutes', '') },
                providers: { proxy: getProxy(URL_PREFIX + '/api/warehouse/providers', '') },
                documents: { proxy: getProxy(URL_PREFIX + '/api/warehouse/documents', '')}
            }
        }
    },
    // Стандартный размер страницы грида
    defaultPageSize: 15,

    // Стандартные кнопки детального просмотра
    modelViewButtons: [
        {
            xtype: 'button',
            text: 'Печать',
            iconCls: 'icon-print',
            action: 'print',
            disabled: 'true'
        },
        {
            xtype: 'button',
            text: 'Обратная связь',
            iconCls: 'icon-feedback',
            action: 'feedback'
        }
    ],

    // кнопки детального просмотра без обратной связи
    modelViewButtonsNoFeedback: [
        {
            xtype: 'button',
            text: 'Печать',
            iconCls: 'icon-print',
            action: 'print',
            disabled: 'true'
        }
    ],

    // Кнопки формы создания
    modelCreateButtonsOnly: [
        {
            text: 'Сохранить',
            action: 'form_save',
            iconCls: 'icon-save-item',
            disabled: true
        },
        {
            text: 'Отмена',
            action: 'form_cancel',
            iconCls: 'icon-cancel'
        }
    ],

    office_work: {
        repeat_kinds: {
            EVERY_DAY: 10,
            EVERY_WEEK: 20,
            EVERY_MONTH: 30,
            EVERY_YEAR: 40
        },
        repeat_end_kinds: {
            AFTER_N_REPEATS: 10,
            DATE: 20,
            NO_END: 30
        },
        repeat_day_values: {
            EVERY_N_DAY: 10,
            EVERY_WORK_DAY: 20
        },
        repeat_week_values: {
            EVERY_WEEK: 1
        },
        repeat_month_values: {
            EVERY_N_DAY: 1,
            EVERY_N_M_DAY: 2
        },
        repeat_year_values: {
            EVERY_DAY_MONTH: 1,
            EVERY_DAY_DAY_MONTH: 2
        },
        writ: {
            security: {
                normal: 10,
                high: 20,
                paranoidal: 30
            },
            importance: {
                normal: 20,
                high: 30
            }
        }
    },

    contract: {
        payment_rules_kinds: {
            DAYS_COUNT: 10,
            DAY_NEXT_MONTH: 20,
            DAY_NUMBER: 30,
            ACCREDITIVE: 40,
            PREPAY: 50
        },
        repeat_days_kinds: {
            WORK: 10,
            BANK: 20
        },
        pay_day_number_kinds: {
            DAY: 10,
            FROM_TO: 20
        }
    },

    products: {
        package_kinds: {
            SIMPLE: 1,
            GROUP: 2,
            TRANSPORT: 3
        }
    },

    goods: {
        load_kinds: {
            self: 1,
            self_contract: 2,
            centr: 3
        }
    },

    cargo: {
        sessions: {
            corporate: 1,
            contract: 2,
            common: 3
        }
    },

    accounting: {
        document_kinds: {
            payment: 1,
            bill: 2,
            act: 3,
            proxy: 4,
            waybill: 5,
            invoice: 6,
            goods_check: 7,
            universal_document: 8
        },
        other_kinds: {
            movementbill: 10,
            writeoffact: 20,
            postingact: 30
        },
        receiptexpense_kinds: {
            salebill: 10,
            completionact: 20,
            invoice: 30
        }
    },

    permission: {
        denied: 10,
        read: 20,
        readwrite: 30
    },

    transport: {
        timesheet_kinds: {
            available: 10,
            buzy: 20,
            in_repair: 30,
            missing: 40
        }
    },

    warehouse: {
        warehouse_kinds: {
            auto: 1,
            railway: 2
        },
        request_kinds: {
            writeoff: 10,
            posting: 20,
            movement: 30,
            making_posting: 40
        },
        receipt_kinds: {
            buy: 10,
            posting: 20,
            movement: 30
        },
        expense_kinds: {
            sale: 10,
            writeoff: 20,
            movement: 30
        }
    }
};

function getProxy(url, root) {
    return {
        type: 'ajax',
        url: url,
        format: 'json',
        reader: {
            type: 'json',
            root: 'data',
            totalProperty: 'total'
        },
        actionMethods: { create: 'POST', read: 'POST', update: 'POST', destroy: 'POST' },
        nodeParam: 'p_parent',
        startParam: 'p_start',
        limitParam: 'p_limit',
        filterParam: 'p_filter',
        pageParam: 'p_page',
        sortParam: 'p_sort'
    };
}

function getSyncProxy(prefix, create, read, update, destroy) {
    return {
        type: 'ajax',
        format: 'json',
        reader: {
            type: 'json',
            root: 'data',
            totalProperty: 'total'
        },
        writer: {
            type: 'json',
            allowSingle: false
        },
        actionMethods: { create: 'POST', read: 'POST', update: 'POST', destroy: 'POST' },
        api: {
            create: prefix + '/' + (create || 'save'),
            read: prefix + '/' + (read || 'list'),
            update: prefix + '/' + (update || 'save'),
            destroy: prefix + '/' + (destroy || 'save')
        },
        timeout: 99999999,
        nodeParam: 'p_parent',
        startParam: 'p_start',
        limitParam: 'p_limit',
        filterParam: 'p_filter',
        pageParam: 'p_page',
        sortParam: 'p_sort'
    };
}

function buildConfig(controllerName, cfg, extraActions) {
    var controllerFullPath = URL_PREFIX + '/api/' + controllerName + '/';

    var config = {
        proxy: getProxy(controllerFullPath + ((cfg && cfg.listAction) || 'list'), ''),
        loadItem: (cfg && cfg.get) ? (controllerFullPath + 'get/{0}') : null,
        saveUrl: controllerFullPath + ((cfg && cfg.saveAction) || 'save'),
        saveMethod: 'POST',
        deleteUrl: controllerFullPath + ((cfg && cfg.deleteAction) || 'delete') + '/{0}',
        printItem: controllerFullPath + ((cfg && cfg.printItemAction) || 'print') + '/{0}',
        printGrid: controllerFullPath + ((cfg && cfg.printGridAction) || 'printGrid'),
        help: controllerFullPath + ((cfg && cfg.helpAction) || 'help'),
        newNumberUrl: controllerFullPath + 'getnum',

        archiveItem: controllerFullPath + 'archive/{0}',
        unArchiveItem: controllerFullPath + 'unarchive/{0}'
    };

    for (var extraAction in extraActions) {
        config[extraAction] = extraActions[extraAction];
    }

    return config;
}