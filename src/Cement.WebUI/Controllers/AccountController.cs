﻿using System;
using System.Web.Mvc;
using Cement.Core.Auth;
using Cement.Core.Exceptions;
using Cement.WebUI.Framework;
using Cement.WebUI.Models;

namespace Cement.WebUI.Controllers
{
    public class AccountController : CementController
    {
        [HttpGet, Route("login")]
        public ActionResult Login(string returnUrl = null)
        {
            if (User.Identity.IsAuthenticated)
            {
                return Redirect("/");
            }

            return View(new LoginModel());
        }


        [HttpPost, Route("login")]
        [ValidateAntiForgeryToken]
        public ActionResult Login(LoginModel loginModel)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    var authenticator = Container.Resolve<IAuthenticator>(new { principal = User });

                    if (authenticator.SignIn(loginModel.Login, loginModel.Password))
                    {
                        return Redirect("/");
                    }

                    ModelState.AddModelError("", "Логин или пароль неверны");
                }
                catch (FailedThresholdExceededException e)
                {
                    ModelState.AddModelError("", String.Format("Превышено количество ошибочных авторизаций. Попробуйте авторизоваться через {0} минут.", (int)e.TryAfter.TotalMinutes));
                }
                catch (UserIsBlockedException)
                {
                    ModelState.AddModelError("", String.Format("Пользователь с логином '{0}' заблокирован, обратитесь к службу поддержки.", loginModel.Login));
                }
            }
            else
            {
                ModelState.AddModelError("", "Проверьте корректность заполнения полей.");
            }

            return loginModel != null ? View(loginModel) : Login();
        }

        [HttpGet, Route("logout")]
        public ActionResult Logout(string returnUrl)
        {
            var authenticator = Container.Resolve<IAuthenticator>(new { principal = User });
            
            authenticator.SignOut();
            return Redirect(returnUrl ?? "~/");
        }
    }
}