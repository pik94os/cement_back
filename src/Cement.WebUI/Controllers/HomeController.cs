﻿using System.Web.Mvc;
using Cement.WebUI.Framework;

namespace Cement.WebUI.Controllers
{
    [Authorize]
    public class HomeController : CementController
    {
        // GET: Home
        public ActionResult Index()
        {
            return View();
        }
    }
}