﻿using System.Web.Mvc;
using Cement.Core.Services;
using Cement.WebUI.Framework;

namespace Cement.WebUI.Controllers.Api
{
    [Authorize]
    [RoutePrefix("/file")]
    public class FileController : CementController
    {
        [HttpGet, Route("download/{id}")]
        public ActionResult Download(long id)
        {
            var entityFileService = Container.Resolve<IEntityFileService>();

            try
            {
                return entityFileService.LoadFile(id);
            }
            finally 
            {
                Container.Release(entityFileService);
            }
        }
    }

}