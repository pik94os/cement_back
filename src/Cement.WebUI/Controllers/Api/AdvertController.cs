﻿using System;
using System.Linq;
using System.Web.Http;
using Cement.Core.Entities;
using Cement.Core.Enums;
using Cement.Core.Extensions;
using Cement.Core.Services;
using Cement.Core.Services.EntityService;
using Cement.Core.Services.StoreParams;
using Cement.Protocol;
using Cement.Protocol.JsonConverters;
using Cement.WebUI.Extensions;

namespace Cement.WebUI.Controllers.Api
{
    [RoutePrefix("api/announcement")]
    public class AdvertController : MovingDocumentDataController<Advert, AdvertProxy, AdvertMovement>
    {
        #region Injections

        public IEntityService<AdvertRecipient> AdvertRecipientEntityService { get; set; }

        //public IEntityService<AdvertMovement> AdvertMovementEntityService { get; set; }

        //public IEntityService<IncomingAdvert> IncomingAdvertEntityService { get; set; }

        //public IEntityService<OutcomingAdvert> OutcomingAdvertEntityService { get; set; }

        public IEntityService<AdvertRoute> AdvertRouteEntityService { get; set; }

        public IAdvertService AdvertService { get; set; }

        #endregion Injections

        protected override IDataResult ListInternal(StoreParams storeParams)
        {
            var query = EntityService.GetAll()
                .Where(x => x.DocumentState == DocumentState.Draft)
                .Where(x => x.Creator == UserPrincipal.Employee)
                .Select(x => new
                {
                    id = x.Id,
                    p_date = x.Date,
                    p_author = (long?)x.Author.Id,
                    p_author_display = x.Author.Name,
                    p_theme = x.Subject,
                    p_importance = x.Importance,
                    p_security = x.Secrecy,
                    p_content = x.Content,
                    p_author_name = x.Author.Name,
                    p_author_department = x.Author.Department.Name,
                    p_author_position = x.Author.Position.Name,
                    p_task_display = x.DocumentState
                })
                .Filter(storeParams);

            var count = query.Count();

            var queryData = query
                .Order(storeParams)
                .Paging(storeParams)
                .ToList();

            var messageIds = queryData.Select(x => x.id).ToList();

            var recipientsDict = AdvertRecipientEntityService.GetAll()
                .Where(x => messageIds.Contains(x.Advert.Id))
                .Select(x => new
                {
                    x.Recipient.Name,
                    x.Recipient.Id,
                    AvdertId = x.Advert.Id
                })
                .AsEnumerable()
                .GroupBy(x => x.AvdertId)
                .ToDictionary(x => x.Key, x =>
                {
                    var recipients = new
                    {
                        Ids = x.Select(y => y.Id).ToList(),
                        Display = string.Join(", ", x.Select(y => y.Name))
                    };

                    return recipients;
                });

            var data = queryData
                .Extend((row, x) =>
                {
                    row["p_content"] = x.p_content.GetString();
                    row["p_task_display"] = x.p_task_display.GetDisplayValue();

                    var recipients = recipientsDict.Get(x.id);

                    if (recipients != null)
                    {
                        row["p_employees"] = recipients.Ids;
                        row["p_employees_display"] = recipients.Display;
                    }
                })
                .ToList();

            return new ObjectListResult
            {
                Data = data,
                Count = count
            };
        }

        protected override IQueryable<AdvertMovement> GetIncomingQuery()
        {
            return base.GetIncomingQuery().Where(x => x.Employee == UserPrincipal.Employee);
        }

        protected override IQueryable<AdvertMovement> GetOutcomingQuery()
        {
            return base.GetOutcomingQuery().Where(x => x.Employee == UserPrincipal.Employee);
        }

        protected override IQueryable<AdvertMovement> GetArchiveQuery()
        {
            return base.GetArchiveQuery().Where(x => x.Employee == UserPrincipal.Employee);
        }
        
        protected override IDataResult DocumentList(StoreParams storeParams, IQueryable<AdvertMovement> advertMovementQuery)
        {
            var moveTypeDict = typeof(DocumentMovementType).AsDictionary();

            var query = advertMovementQuery
                .Select(x => new
                {
                    p_base_doc = x.Document.Id,
                    id = x.Id,
                    processingResult = (DocumentProcessingResult?)x.Route.ProcessingResult,
                    p_date = x.Document.Date,
                    p_author = (long?)x.Document.Author.Id,
                    p_author_display = x.Document.Author.Name,
                    p_theme = x.Document.Subject,
                    p_importance = x.Document.Importance,
                    p_security = x.Document.Secrecy,
                    p_content = x.Document.Content,
                    p_author_name = x.Document.Author.Name,
                    p_author_department = x.Document.Author.Department.Name,
                    p_author_position = x.Document.Author.Position.Name,
                    x.MovementType
                })
                .Filter(storeParams);

            var count = query.Count();

            var data = query
                .Order(storeParams)
                .Paging(storeParams)
                .Extend((row, x) =>
                {
                    row["p_hide_sign"] = !(x.processingResult == DocumentProcessingResult.None); // R# врет, условие не менять!
                    row["p_content"] = x.p_content.GetString();
                    row["p_kind"] = moveTypeDict.Get((int)x.MovementType);
                })
                .ToList();

            return new ObjectListResult
            {
                Data = data,
                Count = count
            };
        }

        //[HttpPost, Route("agreementForNew")]
        //public JsResult AgreementForNew()
        //{
        //    var agreement = Request.ToType<AgreementProxy>();

        //    if (agreement == null || agreement.ItemId == 0)
        //    {
        //        return new JsResult { Success = false, Msg = "Not allowed" };
        //    }

        //    var employeeCommentTuple = agreement.GetEmployeeCommentTuple();

        //    AdvertService.Process(agreement.ItemId, agreement.Send, employeeCommentTuple.Item1, employeeCommentTuple.Item2);

        //    return JsResult.Successful();
        //}

        //[HttpPost, Route("agreementForIncoming")]
        //public JsResult AgreementForIncoming()
        //{
        //    var agreement = Request.ToType<AgreementProxy>();

        //    if (agreement == null || agreement.ItemId == 0)
        //    {
        //        return new JsResult { Success = false, Msg = "Not allowed" };
        //    }

        //    var employeeCommentTuple = agreement.GetEmployeeCommentTuple();

        //    AdvertService.ProcessFromMovement(agreement.ItemId, agreement.Send, employeeCommentTuple.Item1, employeeCommentTuple.Item2);

        //    return JsResult.Successful();
        //}

        [HttpPost, Route("listroutes")]
        public IDataResult ListRoutes()
        {
            var processResultDict = typeof(DocumentProcessingResult).AsDictionary();

            var storeParams = Request.GetStoreParams();

            var docIdStr = storeParams.FilterParams
                .Where(x => x.PropertyName == "p_base_doc")
                .Select(x => x.Value)
                .FirstOrDefault();

            var docId = docIdStr.SafeToString().ToLong();

            if (docId == 0)
            {
                return new ObjectListResult { Count = 0 };
            }

            var routes = AdvertRouteEntityService.GetAll()
                .Where(x => x.Document.Id == docId)
                .Select(x => new
                {
                    id = x.Id,
                    p_name = x.Employee.Name,
                    p_position = x.Employee.Position.Name,
                    p_department = x.Employee.Department.Name,
                    p_date_in = x.DateIn,
                    p_date_out = x.DateOut,
                    p_comment = x.Comment,
                    x.ProcessingResult,
                })
                //.Order(storeParams) Введение в заблуждение сбиванием порядка в маршруте
                .Extend((row, x) =>
                {
                    row["p_comment"] = x.p_comment.GetString();
                    row["p_status"] = processResultDict.Get((int)x.ProcessingResult);
                    row["p_date_in"] = new TimeDate(x.p_date_in);
                    row["p_date_out"] = new TimeDate(x.p_date_out);
                })
                .ToList();

            return new ObjectListResult
            {
                Count = routes.Count,
                Data = routes
            };
        }
    }
}