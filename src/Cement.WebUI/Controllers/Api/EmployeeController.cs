﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using Cement.Core.Auth;
using Cement.Core.Entities;
using Cement.Core.Enums;
using Cement.Core.Extensions;
using Cement.Core.Services.EntityService;
using Cement.Core.Services.StoreParams;
using Cement.Protocol;
using Cement.WebUI.Extensions;
using DataAccess.Repository;
using NHibernate.Linq;

namespace Cement.WebUI.Controllers.Api
{
    [RoutePrefix("api/employee")]
    public class EmployeeController : DataController<Employee, EmployeeProxy>
    {
        public IRepository<EmployeeDrivingCategory> EmployeeDrivingCategoryRepository { get; set; }

        public IEntityService<Department> DepartmentEntityService { get; set; }

        public IEntityService<PersonalClient> PersonalClientEntityService { get; set; }

        public IEntityService<Organization> OrganizationEntityService { get; set; }

        public IEntityService<WarehouseEmployee> WarehouseEmployeeEntityService { get; set; }

        public IPasswordHashProvider PasswordHashProvider { get; set; }

        public IPasswordGenerator PasswordGenerator { get; set; }

        public IEntityService<User> UserEntityService { get; set; }

        public override IDataResult Get(long id)
        {
            var employee = EntityService.GetAll()
                .Where(x => x.Id == id)
                .OrganizationFilter(UserPrincipal)
                .Select(x => new
                {
                    id = x.Id,
                    p_fio = x.Name,

                    p_surname = x.Surname,
                    p_name = x.FirstName,
                    p_middlename = x.Patronymic,
                    p_position = (long?)x.Position.Id,
                    p_position_display = x.Position.Name,
                    p_department = (long?)x.Department.Id,
                    p_department_display = x.Department.Name,
                    p_inn = x.Inn,

                    p_passport_serie = x.PassportSerie,
                    p_passport_number = x.PassportNo,
                    p_passport_get = x.PassportGivenPlace,
                    p_passport_date = x.PassportDate,
                    p_passport_birth = x.BirthDate,
                    p_passport_place = x.BirthPlace,

                    p_drive_serie = x.DriveLicSerie,
                    p_drive_number = x.DriveLicNo,
                    p_drive_get = x.DriveLicGivenPlace,
                    p_drive_date = x.DriveLicDate,
                    p_drive_date_end = x.DriveLicValidDate,
                    p_drive_age = (long?)x.DriveExperience.Id,
                    p_drive_age_display = x.DriveExperience.Name,

                    p_tractor_serie = x.TractorDriveLicSerie,
                    p_tractor_number = x.TractorDriveLicNo,
                    p_tractor_code = x.TractorDriveLicCode,
                    p_tractor_date = x.TractorDriveLicDate,
                    p_tractor_date_end = x.TractorDriveLicValidDate,

                    p_index = x.Index,
                    p_country = x.Country,
                    p_region = x.Region,
                    p_district = x.Area,
                    p_city = x.Locality,
                    p_street = x.Street,
                    p_house = x.House,
                    p_flat = x.Apartment,

                    p_country_display = x.Country,
                    p_region_display = x.Region,
                    p_district_display = x.Area,
                    p_city_display = x.Locality,
                    p_street_display = x.Street,

                    p_connect_phone1 = x.Phone1,
                    p_connect_phone2 = x.Phone2,
                    p_connect_phone3 = x.Phone3,
                    p_connect_fax = x.Fax,
                    p_connect_mobile1 = x.MobilePhone1,
                    p_connect_mobile2 = x.MobilePhone2,
                    p_connect_mobile3 = x.MobilePhone3,
                    p_connect_email1 = x.Email1,
                    p_connect_email2 = x.Email2,
                    p_connect_email3 = x.Email3
                })
                .FirstOrDefault();

            if (employee == null)
            {
                return new ObjectResult();
            }

            var driveCats = EmployeeDrivingCategoryRepository.GetAll()
                .Where(x => x.Employee.Id == id)
                .Select(x => new
                {
                    x.Category.Name,
                    x.Category.Type,
                    CategoryId = x.Category.Id
                })
                .ToList();

            var result = employee.Extend((row, x) =>
            {
                row["p_drive_category"] = driveCats.Where(y => y.Type == DriverCategoryType.AutoMoto).Select(y => y.CategoryId).ToList();
                row["p_tractor_category"] = driveCats.Where(y => y.Type == DriverCategoryType.Tractor).Select(y => y.CategoryId).ToList();
                row["p_drive_category_display"] = string.Join(", ", driveCats.Where(y => y.Type == DriverCategoryType.AutoMoto).Select(y => y.Name));
                row["p_tractor_category_display"] = string.Join(", ", driveCats.Where(y => y.Type == DriverCategoryType.Tractor).Select(y => y.Name));
            });

            return new ObjectResult(result);
        }

        protected override IDataResult ListInternal(StoreParams storeParams)
        {
            var organizationId = storeParams.GetFilterPropertyByName("p_client_current").ToLong();

            var query = EntityService.GetAll()
                .OrganizationExclusiveFilter(UserPrincipal, organizationId)
                .Select(x => new 
                {
                    id = x.Id,
                    p_fio = x.Name,

                    p_surname = x.Surname,
                    p_name = x.FirstName,
                    p_middlename = x.Patronymic,
                    p_position = (long?)x.Position.Id,
                    p_position_display = x.Position.Name,
                    p_department = (long?)x.Department.Id,
                    p_department_display = x.Department.Name,
                    p_inn = x.Inn,

                    p_passport_serie = x.PassportSerie,
                    p_passport_number = x.PassportNo,
                    p_passport_get = x.PassportGivenPlace,
                    p_passport_date = x.PassportDate,
                    p_passport_birth = x.BirthDate,
                    p_passport_place = x.BirthPlace,

                    p_drive_serie = x.DriveLicSerie,
                    p_drive_number = x.DriveLicNo,
                    p_drive_get = x.DriveLicGivenPlace,
                    p_drive_date = x.DriveLicDate,
                    p_drive_date_end = x.DriveLicValidDate,
                    p_drive_age = (long?)x.DriveExperience.Id,
                    p_drive_age_display = x.DriveExperience.Name,

                    p_tractor_serie = x.TractorDriveLicSerie,
                    p_tractor_number = x.TractorDriveLicNo,
                    p_tractor_code = x.TractorDriveLicCode,
                    p_tractor_date = x.TractorDriveLicDate,
                    p_tractor_date_end = x.TractorDriveLicValidDate,

                    p_index = x.Index,
                    p_country = x.Country,
                    p_region = x.Region,
                    p_district = x.Area,
                    p_city = x.Locality,
                    p_street = x.Street,
                    p_house = x.House,
                    p_flat = x.Apartment,

                    p_country_display = x.Country,
                    p_region_display = x.Region,
                    p_district_display = x.Area,
                    p_city_display = x.Locality,
                    p_street_display = x.Street,
                    
                    p_connect_phone1 = x.Phone1,
                    p_connect_phone2 = x.Phone2,
                    p_connect_phone3 = x.Phone3,
                    p_connect_fax = x.Fax,
                    p_connect_mobile1 = x.MobilePhone1,
                    p_connect_mobile2 = x.MobilePhone2,
                    p_connect_mobile3 = x.MobilePhone3,
                    p_connect_email1 = x.Email1,
                    p_connect_email2 = x.Email2,
                    p_connect_email3 = x.Email3
                })
                .Filter(storeParams);

            var count = query.Count();

            var data = query
                .Order(storeParams)
                .Paging(storeParams)
                .ToList();
            
            var employeeIds = data.Select(x => x.id).ToList();

            var driveCatsDict = EmployeeDrivingCategoryRepository.GetAll()
                .Where(x => employeeIds.Contains(x.Employee.Id))
                .Select(x => new
                {
                    x.Employee.Id,
                    x.Category.Name,
                    x.Category.Type,
                    CategoryId = x.Category.Id
                })
                .AsEnumerable()
                .GroupBy(x => x.Id)
                .ToDictionary(x => x.Key, x => x.ToList());

            var result = data.Extend((row, x) =>
                {
                    var driveCats = driveCatsDict.Get(x.id);

                    if (driveCats != null)
                    {
                        row["p_drive_category"] = driveCats.Where(y => y.Type == DriverCategoryType.AutoMoto).Select(y => y.CategoryId).ToList();
                        row["p_tractor_category"] = driveCats.Where(y => y.Type == DriverCategoryType.Tractor).Select(y => y.CategoryId).ToList();
                        row["p_drive_category_display"] = string.Join(", ", driveCats.Where(y => y.Type == DriverCategoryType.AutoMoto).Select(y => y.Name));
                        row["p_tractor_category_display"] = string.Join(", ", driveCats.Where(y => y.Type == DriverCategoryType.Tractor).Select(y => y.Name));
                    }
                })
                .Highlight(storeParams)
                .ToList();
            
            return new ObjectListResult
            {
                Data = result,
                Count = count
            };
        }

        [HttpPost, Route("listdrivers")]
        public List<TreeElemendDriver> ListDrivers()
        {
            var driverList = EntityService.GetAll()
                .OrganizationFilter(UserPrincipal)
                .Where(x => x.Position.Type == PositionType.Driver)
                .Select(x => new TreeElemendDriver
                {
                    Id = x.Id,
                    Name = x.Name,
                    Leaf = true,
                    DepartmentName = x.Department.Name,
                    PositionName = x.Position.Name
                })
                .OrderBy(x => x.Name)
                .ToList();

            return driverList;
        }

        [HttpPost, Route("structure")]
        public List<EmployeeTreeElement> Structure(TreeRequest request)
        {
            return Structure<EmployeeTreeElement>(request == null ? 0 : request.Node);
        }

        [HttpPost, Route("structure_v2")]
        public List<EmployeeNamedTreeElement> StructureV2(TreeRequest request)
        {
            return Structure<EmployeeNamedTreeElement>(request == null ? 0 : request.Node);
        }

        [HttpPost, Route("structure_v3")]
        public List<EmployeeNamedTreeElement> StructureV3(TreeRequestWithFirmId request)
        {
            if (request.OrganizationId == 0)
            {
                return new List<EmployeeNamedTreeElement>();
            }

            if (request.Node == 0)
            {
                var departments = DepartmentEntityService.GetAll()
                    .Where(x => x.Organization.Id == request.OrganizationId)
                    .Select(x => new EmployeeNamedTreeElement
                    {
                        Id = x.Id,
                        Name = x.Name
                    })
                    .ToList();

                return departments;
            }

            var departmentEmployees = EntityService.GetAll()
                .Where(x => x.Organization.Id == request.OrganizationId)
                .Where(x => x.Department.Id == request.Node)
                .Select(x => new EmployeeNamedTreeElement
                {
                    Id = x.Id,
                    Name = x.Name,
                    Leaf = true,
                    PositionName = x.Position.Name,
                    DepartmentName = x.Department.Name
                })
                .ToList();

            return departmentEmployees;
        }

        /// <summary>
        /// Сотрудники других организаций
        /// </summary>
        [HttpPost, Route("external_structure")]
        public List<ExternalEmployeeTreeElement> ExternalStructure(TreeRequest request)
        {
            // Гибридная загрузка дерева сотрудников
            // Первый запрос вытягивает дерево: 
            // Группа - Подгруппа - Организация - Отдел
            // Сотрудники каждого отдела вытягиваются отдельным запросом при выборе отдела
            

            // Получение списка сотрудников выбранного отдела
            if (request.Node > 0)
            {
                var departmentEmployees = EntityService.GetAll()
                    .Where(x => x.Department.Id == request.Node)
                    .Select(x => new ExternalEmployeeTreeElement
                    {
                        Id = x.Id,
                        Name = x.Name,
                        Leaf = true,
                        PositionName = x.Position.Name,
                        DepartmentName = x.Department.Name
                    })
                    .ToList();

                return departmentEmployees;
            }


            // Построение дерева: Группа - Подгруппа - Организация - Отдел

            var clientsQuery = PersonalClientEntityService.GetAll()
                .OrganizationFilter(UserPrincipal)
                .Where(x => x.IsCorporate || x.Employee == UserPrincipal.Employee)
                .Where(x => x.ArchivedAt == null);

            var clientIdsQuery = clientsQuery.Select(x => x.Client.Id);

            var departmentsDict = DepartmentEntityService.GetAll()
                .Where(x => clientIdsQuery.Contains(x.Organization.Id))
                .Select(x => new { x.Id, x.Name, orgId = x.Organization.Id })
                .AsEnumerable()
                .GroupBy(x => x.orgId)
                .ToDictionary(
                    x => x.Key,
                    x => x.Select(y => new ExternalEmployeeTreeElement
                    {
                        Id = y.Id,
                        Name = y.Name,
                        Leaf = false
                    }).ToList());

            var organizationsDict = OrganizationEntityService.GetAll()
                .Where(x => clientIdsQuery.Contains(x.Id))
                .Select(x => new { x.Id, x.Name })
                .ToDictionary(x => x.Id, x => x.Name);

            var clients = clientsQuery
                .Select(x => new
                {
                    clientId = x.Client.Id,
                    clientName = x.Client.Name,
                    groupId = x.Group.Id,
                    groupName = x.Group.Name,
                    subGroupId = x.Subgroup.Id,
                    subGroupName = x.Subgroup.Name
                })
                .AsEnumerable()
                .GroupBy(x => new { x.groupId, x.groupName })
                .Select(x => new ExternalEmployeeTreeElement
                {
                    Id = x.Key.groupId,
                    Name = x.Key.groupName,
                    Leaf = false,
                    Children = x.GroupBy(y => new { y.subGroupId, y.subGroupName })
                        .Select(y => new ExternalEmployeeTreeElement
                        {
                            Id = y.Key.subGroupId,
                            Name = y.Key.subGroupName,
                            Leaf = false,
                            Children = y.GroupBy(z => new { z.clientId, z.clientName })
                                .Select(z => new
                                {
                                    Id = z.Key.clientId,
                                    Name = organizationsDict.Get(z.Key.clientId, z.Key.clientName),
                                    Departments = departmentsDict.Get(z.Key.clientId, null)
                                })
                                .Select(z => new ExternalEmployeeTreeElement
                                {
                                    Id = z.Id,
                                    Name = z.Name,
                                    Leaf = false,
                                    Children = z.Departments
                                })
                                .ToList()
                        })
                        .ToList()
                })
                .ToList();

            return clients;
        }


        [HttpPost, Route("warehouse_staff")]
        public List<EmployeeNamedTreeElement> WarehouseStaff(TreeRequestWithWarehouseId request)
        {
            var warehouseId = request.WarehouseId;

            return WarehouseEmployeeEntityService.GetAll()
                .Where(x => x.Warehouse.Id == warehouseId)
                .Select(x => new EmployeeNamedTreeElement
                {
                    Id = x.Employee.Id,
                    Name = x.Employee.Name,
                    Leaf = true,
                    PositionName = x.Employee.Position.Name,
                    DepartmentName = x.Employee.Department.Name
                })
                .ToList();
        }

        [HttpPost, Route("generate_new_pass")]
        public void GenerateNewPassword()
        {
            var employeeId = Request.GetStoreParams().GetDataPropertyByName("id").ToLong();
            if (employeeId > 0)
            {
                var user = UserEntityService.GetAll().Fetch(x => x.Organization).First(x => x.Employee.Id == employeeId);
                user.SetNewPassword(PasswordHashProvider, PasswordGenerator.Generate());
                UserEntityService.Save(user);
            }
            else
            {
                throw new Exception("Не удалось получить пользователя");
            }
        }

        public List<T> Structure<T>(long node) where T : BaseEmployeeTreeElement, new()
        {
            if (node == 0)
            {
                var departments = DepartmentEntityService.GetAll()
                    .OrganizationFilter(UserPrincipal)
                    .Select(x => new T
                    {
                        Id = x.Id,
                        Name = x.Name
                    })
                    .ToList();

                return departments;
            }

            var departmentEmployees = EntityService.GetAll().OrganizationFilter(UserPrincipal)
                .Where(x => x.Department.Id == node)
                .Select(x => new T
                {
                    Id = x.Id,
                    Name = x.Name,
                    Leaf = true,
                    PositionName = x.Position.Name,
                    DepartmentName = x.Department.Name
                })
                .ToList();

            return departmentEmployees;
        }
    }
}