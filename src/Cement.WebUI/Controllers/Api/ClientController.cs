﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using Cement.Core;
using Cement.Core.Entities;
using Cement.Core.Extensions;
using Cement.Core.Services.EntityService;
using Cement.Core.Services.StoreParams;
using Cement.Protocol;
using Cement.WebUI.Extensions;
using Cement.WebUI.Framework;

namespace Cement.WebUI.Controllers.Api
{
    [Authorize]
    [RoutePrefix("api/client")]
    public class ClientController : CementApiController
    {
        public IEntityService<Client> ClientEntityService { get; set; }

        public IEntityService<Organization> OrganizationEntityService { get; set; }

        public IEntityService<ClientGroup> ClientGroupEntityService { get; set; }

        public IEntityService<PersonalClient> PersonalClientEntityService { get; set; }

        [HttpPost, Route("renters_structure")]
        public List<ClientTreeElement> RentersStructure()
        {
            var data = ClientEntityService.GetAll()
               .WhereIf(UserPrincipal.Organization != null, x => x != UserPrincipal.Organization.Client)
               .Select(x => new ClientTreeElement
               {
                   Id = x.Id,
                   Text = x.Name,
                   Address = x.Address
               })
               .OrderBy(x => x.Text)
               .ToList();

            return data;
        }

        [HttpPost, Route("clients_structure")]
        public List<ClientExtendedTreeElement> ClientsStructure(TreeRequest request)
        {
            const int active = -10;
            const int potential = -20;


            if (request == null || request.Node == 0)
            {
                return new List<ClientExtendedTreeElement>
                {
                    new ClientExtendedTreeElement
                    {
                        Id = active,
                        Name = "Действующие",
                        Children = new List<TreeElement>{ new TreeElement()}
                    },
                    new ClientExtendedTreeElement
                    {
                        Id = potential,
                        Name = "Потенциальные",
                        Children = new List<TreeElement>{ new TreeElement()}
                    }
                };
            }

            if (request.Node == active)
            {
                var data = ClientEntityService.GetAll()
                    .WhereIf(UserPrincipal.Organization != null, x => x != UserPrincipal.Organization.Client)
                    .Select(x => new ClientExtendedTreeElement
                    {
                        Id = x.Id,
                        Name = x.Name,
                        Address = x.Address,
                        Inn = x.Inn,
                        Kpp = x.Kpp
                    })
                    .OrderBy(x => x.Name)
                    .ToList();

                return data;
            }

            return new List<ClientExtendedTreeElement>();
        }

        protected List<OrganizationClientExtendedTreeElement> ClientsStructureBase(TreeRequest request, bool isCorporate)
        {
            switch (request.Level)
            {
                case TreeLevel.Level0: 
                    return ClientGroupEntityService.GetAll()
                     .OrganizationFilter(UserPrincipal)
                     .Where(x => x.Parent == null)
                     .Select(x => new OrganizationClientExtendedTreeElement
                     {
                         Id = x.Id,
                         Name = x.Name,
                         Level = TreeLevel.Level1,
                         Children = new List<TreeElement> { new TreeElement() }
                     })
                     .OrderBy(x => x.Name)
                     .ToList();

                case TreeLevel.Level1:
                    return ClientGroupEntityService.GetAll()
                     .OrganizationFilter(UserPrincipal)
                     .Where(x => x.Parent.Id == request.Node)
                     .Select(x => new OrganizationClientExtendedTreeElement
                     {
                         Id = x.Id,
                         Name = x.Name,
                         Level = TreeLevel.Level2,
                         Children = new List<TreeElement> { new TreeElement() }
                     })
                     .OrderBy(x => x.Name)
                     .ToList();

                case TreeLevel.Level2:
                    return PersonalClientEntityService.GetAll()
                     .OrganizationFilter(UserPrincipal)
                     .WhereIf(isCorporate, x => x.IsCorporate)
                     .WhereIf(!isCorporate, x => x.Employee == UserPrincipal.Employee)
                     .Where(x => x.Subgroup.Id == request.Node)
                     .Select(x => new OrganizationClientExtendedTreeElement
                     {
                         Id = x.Id,
                         Name = x.Client.Name,
                         Level = TreeLevel.Level3,
                         Address = x.Client.Address,
                         Inn = x.Client.Inn,
                         Kpp = x.Client.Kpp,
                         ClientId = x.Client.Id
                     })
                     .OrderBy(x => x.Name)
                     .ToList();
            }
            
            return new List<OrganizationClientExtendedTreeElement>();
        }

        [HttpPost, Route("orgclients_structure")]
        public List<OrganizationClientExtendedTreeElement> OrganizationClientsStructure(TreeRequest request)
        {
            return ClientsStructureBase(request, true);
        }

        [HttpPost, Route("personalclients_structure")]
        public List<OrganizationClientExtendedTreeElement> PersonalClientsStructure(TreeRequest request)
        {
            return ClientsStructureBase(request, false);
        }

        [HttpPost, Route("list")]
        public IDataResult List()
        {
            var storeParams = Request.GetStoreParams();

            var query = ClientEntityService.GetAll()
                .Select(x => new
                {
                    id = x.Id,
                    p_client = x.Id,
                    p_firm_brand = x.Brand,
                    p_firm_form = x.Ownership.ShortName,
                    p_firm_full_name = x.FullName,
                    p_firm_img = "",
                    p_firm_name = x.Name,
                    p_cont_email = x.Email,
                    p_cont_fax = x.Fax,
                    p_cont_phone = x.Phone,
                    p_cont_web = x.Web,
                    p_fact_building = x.FactAddressBuilding,
                    p_fact_city = x.FactAddressLocality,
                    p_fact_country = x.FactAddressCountry,
                    p_fact_district = x.FactAddressArea,
                    p_fact_house = x.FactAddressHouse,
                    p_fact_house_part = x.FactAddressHousing,
                    p_fact_index = x.FactAddressIndex,
                    p_fact_office = x.FactAddressOffice,
                    p_fact_region = x.FactAddressRegion,
                    p_fact_street = x.FactAddressStreet,
                    p_reg_inn = x.Inn,
                    p_reg_inn_img = "",
                    p_reg_kpp = x.Kpp,
                    p_reg_ogrn = x.Ogrn,
                    p_reg_ogrn_img = "",
                    p_reg_okato = x.Okato,
                    p_reg_okonh = x.Okonh,
                    p_reg_okpo = x.Okpo,
                    p_reg_okved = x.Okved,
                    p_address_display = x.Address,
                    p_ur_building = x.JurAddressBuilding,
                    p_ur_city = x.JurAddressLocality,
                    p_ur_country = x.JurAddressCountry,
                    p_ur_district = x.JurAddressArea,
                    p_ur_house = x.JurAddressHouse,
                    p_ur_house_part = x.JurAddressHousing,
                    p_ur_index = x.JurAddressIndex,
                    p_ur_office = x.JurAddressOffice,
                    p_ur_region = x.JurAddressRegion,
                    p_ur_street = x.JurAddressStreet
                })
                .Filter(storeParams);

            var count = query.Count();

            var data = query
                .Order(storeParams)
                .Paging(storeParams)
                .Highlight(storeParams)
                .ToList();

            return new ObjectListResult
            {
                Data = data,
                Count = count
            };
        }

        [HttpPost, Route("free_clients_list")]
        public IDataResult FreeClientsList()
        {
            var storeParams = Request.GetStoreParams();

            var organizationIdsQuery = OrganizationEntityService.GetAll().Select(x => x.Id);

            var query = ClientEntityService.GetAll()
                .Where(x => !organizationIdsQuery.Contains(x.Id))
                .Select(x => new
                {
                    id = x.Id,
                    p_name = x.Name,
                    p_inn = x.Inn
                })
                .Filter(storeParams);

            var count = query.Count();

            var data = query
                .Order(storeParams)
                .Paging(storeParams)
                .Highlight(storeParams)
                .ToList();

            return new ObjectListResult
            {
                Data = data,
                Count = count
            };
        }
    }
}