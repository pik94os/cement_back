﻿using System;
using System.Linq;
using System.Linq.Expressions;
using System.Net.Http;
using System.Web.Http;
using Cement.Core.Entities;
using Cement.Core.Entities.Stockroom;
using Cement.Core.Enums;
using Cement.Core.Exceptions;
using Cement.Core.Extensions;
using Cement.Core.Services;
using Cement.Core.Services.EntityService;
using Cement.Core.Services.StoreParams;
using Cement.Protocol;
using Cement.WebUI.Extensions;
using NHibernate.Linq;

namespace Cement.WebUI.Controllers.Api
{
    [RoutePrefix("api/stockexpenseorder")]
    public class StockroomExpenseOrderController : RoutedEntityDataController<StockroomExpenseOrderBase, StockroomExpenseOrderProxy, StockroomExpenseOrderRoute>
    {
        public IEntityService<StockroomExpenseOrderMovement> StockroomExpenseOrderMovementEntityService { get; set; }
        public IEntityService<StockroomExpenseOrderSale> StockroomExpenseOrderSaleEntityService { get; set; }
        public IEntityService<StockroomExpenseOrderWriteoff> StockroomExpenseOrderWriteoffEntityService { get; set; }
        public IWarehouseService WarehouseService { get; set; }
        
        protected object GetRequest(StockroomExpenseOrderBase expenseOrder)
        {
            if (expenseOrder is StockroomExpenseOrderWriteoff)
            {
                return Container.Resolve<IEntityService<StockroomExpenseOrderWriteoff>>().GetAll()
                    .Where(x => x.Id == expenseOrder.Id)
                    .Select(x => x.StockroomWriteoffRequest)
                    .Select(x => new
                    {
                        id = x.Id,
                        p_kind = x.StockroomRequestType,
                        p_number = x.No,
                        p_date = x.Date,
                        p_product_display = x.StockroomProduct.Product.Name,
                        p_measure_unit_display = x.StockroomProduct.MeasureUnit.ShortName,
                        p_count = x.Amount,
                        p_description = x.ExtraData,
                        p_warehouse_display = x.Warehouse.Name,
                        p_warehouse_address = x.Warehouse.Address
                    })
                    .Extend((row, x) =>
                    {
                        row["p_kind"] = x.p_kind.GetDisplayValue();
                    })
                    .FirstOrDefault();
            }

            if (expenseOrder is StockroomExpenseOrderMovement)
            {
                return Container.Resolve<IEntityService<StockroomExpenseOrderMovement>>().GetAll()
                    .Where(x => x.Id == expenseOrder.Id)
                    .Select(x => x.StockroomMovementRequest)
                    .Select(x => new
                    {
                        id = x.Id,
                        p_kind = x.StockroomRequestType,
                        p_warehouse_display = x.Warehouse.Name,
                        p_warehouse_address = x.Warehouse.Address,
                        p_warehouse_to_display = x.WarehouseTo.Name,
                        p_warehouse_to_address = x.WarehouseTo.Address,
                        p_number = x.No,
                        p_date = x.Date,
                        p_product_display = x.StockroomProduct.Product.Name,
                        p_measure_unit_display = x.StockroomProduct.MeasureUnit.ShortName,
                        p_count = x.Amount,
                        p_description = x.ExtraData
                    })
                    .Extend((row, x) =>
                    {
                        row["p_kind"] = x.p_kind.GetDisplayValue();
                    })
                    .FirstOrDefault();
            }

            if (expenseOrder is StockroomExpenseOrderSale)
            {
                return Container.Resolve<IEntityService<StockroomExpenseOrderSale>>().GetAll()
                    .Where(x => x.Id == expenseOrder.Id)
                    .Select(x => x.ProductRequest)
                    .Select(x => new
                    {
                        id = x.Id,
                        p_kind = x.RequestType,
                        p_number = x.Number,
                        p_date = x.Date,
                        p_product_display = x.Product.Name,
                        p_measure_unit_display = x.ProductMeasureUnit.ShortName,
                        p_count = x.ProductCount,
                        p_description = x.Content,
                        p_product_price = x.ProductPrice,
                        p_product_tax = x.ProductTax,
                        p_consignee_name = x.Consignee.Name,
                        p_consignee_address = x.Consignee.Client.Address
                    })
                    .Extend((row, x) =>
                    {
                        row["p_kind"] = x.p_kind.GetDisplayValue();
                        row["p_description"] = x.p_description.GetString();
                        row["p_product_sum"] = x.p_count * x.p_product_price;
                    })
                    .FirstOrDefault();
            }

            return null;
        }

        public override IDataResult Get(long id)
        {
            var data = EntityService.GetAll().Fetch(x => x.Warehouse).First(x => x.Id == id);
                
            var res = new
            {
                id = data.Id,
                p_number = data.No,
                p_date = data.Date,
                p_count_fact = data.Amount,
                p_type = data.StockroomExpenseOrderType,
                p_request = GetRequest(data),
                p_warehouse = new { data.Warehouse.Id, data.Warehouse.Name },
                p_warehouse_display = data.Warehouse.Name,
                p_warehouse_address = data.Warehouse.Address,
            };

            var result = res.Extend((row, x) =>
            {
                //row["p_tax"] = x.p_tax * 100;
            });

            return new ObjectResult { Data = result };
        }

        protected override Expression<Func<StockroomExpenseOrderRoute, bool>> DocIdFilterExpression(long id)
        {
            return x => x.Document.Id == id;
        }

        protected override IQueryable<StockroomExpenseOrderRoute> PersonalizeQuery(IQueryable<StockroomExpenseOrderRoute> query)
        {
            return query.Where(x => x.Employee == UserPrincipal.Employee);
        }

        protected override IDataResult DocumentList(StoreParams storeParams, IQueryable<StockroomExpenseOrderRoute> routeQuery)
        {
            var kindDict = typeof(StockroomExpenseOrderType).AsDictionary();
            var typeDict = typeof(DocumentMovementType).AsDictionary();
            var stateDict = typeof (DocumentState).AsDictionary();

            var query = routeQuery
                .Select(x => new
                {
                    p_base_doc = x.Document.Id,
                    id = x.Id,
                    p_kind = x.Document.StockroomExpenseOrderType,
                    p_number = x.Document.No,
                    p_date = x.Document.Date,
                    p_warehouse_display = x.Document.Warehouse.Name,
                    p_warehouse_address = x.Document.Warehouse.Address,
                    p_product_display = (x.Document.StockroomExpenseOrderType == StockroomExpenseOrderType.Sale ?
                        (x.Document as StockroomExpenseOrderSale).ProductRequest.Product.Name
                        : (x.Document as StockroomExpenseOrderMovement).StockroomMovementRequest.StockroomProduct.Product.Name
                    ),
                    p_measure_unit_display = (x.Document.StockroomExpenseOrderType == StockroomExpenseOrderType.Sale ?
                        (x.Document as StockroomExpenseOrderSale).ProductRequest.ProductMeasureUnit.Name
                        : (x.Document as StockroomExpenseOrderMovement).StockroomMovementRequest.StockroomProduct.MeasureUnit.Name
                    ),
                    p_count = (x.Document.StockroomExpenseOrderType == StockroomExpenseOrderType.Sale ?
                        (x.Document as StockroomExpenseOrderSale).ProductRequest.ProductCount
                        : (x.Document as StockroomExpenseOrderMovement).StockroomMovementRequest.Amount
                    ),
                    p_description_bytes = (x.Document as StockroomExpenseOrderSale).ProductRequest.Content,
                    p_description = (x.Document as StockroomExpenseOrderMovement).StockroomMovementRequest.ExtraData,
                    p_count_fact = x.Document.Amount,
                    p_type = x.MovementType,
                    x.Document.DocumentState
                })
                .Filter(storeParams);

            var data = query
                .Order(storeParams)
                .Paging(storeParams)
                .Extend((row, x) =>
                {
                    row["p_kind_display"] = kindDict.Get((int)x.p_kind);
                    row["p_type"] = typeDict.Get((int)x.p_type);
                    row["p_form_kind"] = (int)x.p_kind;
                    row["p_state"] = stateDict.Get((int)x.DocumentState);

                    if (x.p_description_bytes != null)
                    {
                        row["p_description"] = x.p_description_bytes.GetString();
                    }
                })
                .Highlight(storeParams)
                .ToList();

            return new ObjectListResult
            {
                Data = data,
                Count = query.Count()
            };
        }

        protected override StockroomExpenseOrderBase MapProxyToEntity(StockroomExpenseOrderProxy incomeProxy)
        {
            switch (incomeProxy.StockroomExpenseOrderType)
            {
                case StockroomExpenseOrderType.Writeoff: return AutoMapper.Mapper.DynamicMap<StockroomExpenseOrderWriteoff>(incomeProxy);
                case StockroomExpenseOrderType.Movement: return AutoMapper.Mapper.DynamicMap<StockroomExpenseOrderMovement>(incomeProxy);
                case StockroomExpenseOrderType.Sale: return AutoMapper.Mapper.DynamicMap<StockroomExpenseOrderSale>(incomeProxy);
                default:
                    throw new ValidationException(string.Empty);
            }
        }

        #region bottom lists

        [HttpPost, Route("warehouses")]
        public IDataResult StockroomExpenseOrderWarehousesBottomList()
        {
            var storeParams = Request.GetStoreParams();

            var docIdStr = storeParams.FilterParams
                .Where(x => x.PropertyName == "p_base_doc")
                .Select(x => x.Value)
                .FirstOrDefault();

            var docId = docIdStr.SafeToString().ToLong();

            if (docId == 0)
            {
                return new ObjectListResult { Count = 0 };
            }

            var warehouseQuery = EntityService.GetAll()
                .Where(x => x.Id == docId)
                .Select(x => x.Warehouse);

            var listData = WarehouseService.GetBottomList(warehouseQuery, storeParams, false);

            return new ObjectListResult
            {
                Data = listData.Item1,
                Count = listData.Item2
            };
        }
       
        [HttpPost, Route("products")]
        public IDataResult StockroomExpenseOrderProductsBottomList()
        {
            return GetProductsBottomList(Request, ProductType.Product);
        }

        [HttpPost, Route("services")]
        public IDataResult StockroomExpenseOrderServicesBottomList()
        {
            return GetProductsBottomList(Request, ProductType.Service);
        }

        private IDataResult GetProductsBottomList(HttpRequestMessage request, ProductType productType)
        {
            var storeParams = request.GetStoreParams();

            var docIdStr = storeParams.FilterParams
               .Where(x => x.PropertyName == "p_base_doc")
               .Select(x => x.Value)
               .FirstOrDefault();

            var docId = docIdStr.SafeToString().ToLong();

            if (docId == 0)
            {
                return new ObjectListResult { Count = 0 };
            }

            StockroomProductDataProxy stockroomProductsProxy = null;

            var stockroomExpenseOrderBaseType = EntityService.Get(docId).StockroomExpenseOrderType;

            switch (stockroomExpenseOrderBaseType)
            {
                case StockroomExpenseOrderType.Movement:
                    stockroomProductsProxy = StockroomExpenseOrderMovementEntityService.GetAll()
                        .Where(x => x.Id == docId)
                        .Where(x => x.StockroomMovementRequest.StockroomProduct.Product.Type == productType)
                        .Select(x => x.StockroomMovementRequest.StockroomProduct)
                        .Select(x =>new StockroomProductDataProxy
                            {
                                id = x.Product.Id,
                                p_name =  x.Product.Name,
                                p_group =  x.Product.Group.Name,
                                p_subgroup =  x.Product.SubGroup.Name,
                                p_trade_mark =  x.Product.Brand,
                                p_manufacturer =  x.Product.Manufacturer.Name,
                                p_unit_display = x.MeasureUnit.Name,
                                p_tax = null, // когда будет брать из товара склада
                                p_price = null //когда будет брать из товара склада
                            })
                            .FirstOrDefault();
                break;
                case StockroomExpenseOrderType.Sale:
                    stockroomProductsProxy = StockroomExpenseOrderSaleEntityService.GetAll()
                        .Where(x => x.Id == docId)
                        .Where(x => x.ProductRequest.Product.Type == productType)
                        .Select(x => x.ProductRequest)
                        .Select(x =>new StockroomProductDataProxy
                            {
                                id = x.Product.Id,
                                p_name =  x.Product.Name,
                                p_group =  x.Product.Group.Name,
                                p_subgroup =  x.Product.SubGroup.Name,
                                p_trade_mark =  x.Product.Brand,
                                p_manufacturer =  x.Product.Manufacturer.Name,
                                p_unit_display = x.ProductMeasureUnit.Name,
                                p_tax = x.ProductTax,
                                p_price = x.ProductPrice
                            })
                            .FirstOrDefault();
                break;
                case StockroomExpenseOrderType.Writeoff:
                stockroomProductsProxy = StockroomExpenseOrderWriteoffEntityService.GetAll()
                        .Where(x => x.Id == docId)
                        .Where(x => x.StockroomWriteoffRequest.StockroomProduct.Product.Type == productType)
                        .Select(x => x.StockroomWriteoffRequest.StockroomProduct)
                        .Select(x => new StockroomProductDataProxy
                        {
                            id = x.Product.Id,
                            p_name = x.Product.Name,
                            p_group = x.Product.Group.Name,
                            p_subgroup = x.Product.SubGroup.Name,
                            p_trade_mark = x.Product.Brand,
                            p_manufacturer = x.Product.Manufacturer.Name,
                            p_unit_display = x.MeasureUnit.Name,
                            p_tax = null, // когда будет брать из товара склада
                            p_price = null //когда будет брать из товара склада
                        })
                        .FirstOrDefault();
                break;
            }


            return new ObjectListResult
            {
                Data = stockroomProductsProxy,
                Count = stockroomProductsProxy !=  null ? 1 : 0
            };
        }

        #endregion bottom lists
    }
}