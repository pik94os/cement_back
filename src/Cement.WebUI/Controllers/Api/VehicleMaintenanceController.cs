﻿using System;
using System.Linq;
using System.Web.Http;
using Cement.Core.Entities;
using Cement.Core.Extensions;
using Cement.Core.Services.StoreParams;
using Cement.Protocol;
using Cement.Protocol.JsonConverters;

namespace Cement.WebUI.Controllers.Api
{
    [RoutePrefix("api/vehiclemaintenance")]
    public class VehicleMaintenanceController : DataController<VehicleMaintenance, VehicleMaintenanceProxy>
    {
        protected override IDataResult ListInternal(StoreParams storeParams)
        {
            var query = EntityService.GetAll()
                .OrganizationFilter(x => x.Vehicle.Organization, UserPrincipal)
                .Select(x => new
                {
                    id = x.Id,
                    p_transport_unit = x.Vehicle.Id,
                    p_transport_unit_display = x.Vehicle.Name,
                    p_date = x.Date,
                    p_name = x.Name,
                    p_odometer = x.Mileage,
                    p_autoservice = (long?)x.CarService.Id,
                    p_autoservice_display = x.CarService.Name,
                    p_address = x.CarService.Address,
                    p_master = x.Master,
                    p_made_date = x.WorkDate,
                    p_cost = x.Cost,
                    p_comment = x.Notes,
                    p_driver = (long?)x.Driver.Id,
                    p_driver_display = x.Driver.Name,
                    p_document = x.DocName,
                    p_document_file = new Downloadable(x.DocFile.Id)
                })
                .Filter(storeParams);

            var count = query.Count();

            var data = query
                .Order(storeParams)
                .Paging(storeParams)
                .Highlight(storeParams)
                .ToList();

            return new ObjectListResult
            {
                Data = data,
                Count = count
            };
        }
    }

}