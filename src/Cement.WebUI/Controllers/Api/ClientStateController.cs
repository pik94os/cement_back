﻿using System.Linq;
using System.Web.Http;
using Cement.Core.Entities;
using Cement.Core.Extensions;
using Cement.Core.Services.StoreParams;
using Cement.Protocol;

namespace Cement.WebUI.Controllers.Api
{
    [RoutePrefix("api/clientstate")]
    public class ClientStateController : DataController<ClientState, ClientStateProxy>
    {
        protected override IDataResult ListInternal(StoreParams storeParams)
        {
            var query = EntityService.GetAll()
                .OrganizationFilter(UserPrincipal)
                .Filter(storeParams);

            var count = query.Count();

            var data = query
                .Select(x => new
                {
                    id = x.Id,
                    p_name = x.Name,
                    p_description = x.Description
                })
                .Order(storeParams)
                .Paging(storeParams)
                .Highlight(storeParams)
                .ToList();

            return new ObjectListResult
            {
                Data = data,
                Count = count
            };
        }

        [HttpPost, Route("listall")]
        public IDataResult ListAll()
        {
            var data = EntityService.GetAll()
                .OrganizationFilter(UserPrincipal)
                .Select(x => new
                {
                    id = x.Id,
                    p_name = x.Name
                })
                .OrderBy(x => x.p_name)
                .ToList();

            return new ObjectListResult
            {
                Data = data,
                Count = data.Count
            };
        }
    }
}