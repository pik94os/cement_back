﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Linq.Expressions;
using System.Net.Http;
using System.Text;
using System.Web.Http;
using Cement.Core.Entities;
using Cement.Core.Entities.Stockroom;
using Cement.Core.Enums;
using Cement.Core.Extensions;
using Cement.Core.Services;
using Cement.Core.Services.EntityService;
using Cement.Core.Services.Impl;
using Cement.Core.Services.StoreParams;
using Cement.Protocol;
using Cement.Protocol.JsonConverters;
using Cement.WebUI.Extensions;
using DataAccess.Repository;
using Newtonsoft.Json;

namespace Cement.WebUI.Controllers.Api
{
    [RoutePrefix("api/inventory")]
    public class InventoryController : RoutedEntityDataController<Inventory, InventoryProxy, InventoryRoute>
    {
        public IEntityService<StockroomMovementRequest> StockroomMovementRequestEntityService { get; set; }
        public IEntityService<InventoryStockroomProduct> InventoryStockroomProductEntityService { get; set; }
        public IEntityService<InventoryMember> InventoryMemberEntityService { get; set; }
        public IEntityService<StockroomProduct> StockroomProductEntityService { get; set; }
        public IEntityService<Warehouse> WarehouseEntityService { get; set; }
        public IWarehouseService WarehouseService { get; set; }
        public IStockroomProductService StockroomProductService { get; set; }

        protected override Expression<Func<InventoryRoute, bool>> DocIdFilterExpression(long id)
        {
            return x => x.Document.Id == id;
        }

        public override IDataResult Get(long id)
        {
            var members = InventoryMemberEntityService.GetAll()
                .Where(x => x.Inventory.Id == id)
                .Select(x => new {x.Memeber.Id, x.Memeber.Name})
                .ToList();

            var stockroomProducts = InventoryStockroomProductEntityService.GetAll()
                .Where(x => x.Inventory.Id == id)
                .Select(x => new
                {
                    id = x.StockroomProduct.Id,
                    p_warehouse = x.StockroomProduct.Warehouse.Id,
                    p_group_display = x.StockroomProduct.Product.Group.Name,
                    p_subgroup_display = x.StockroomProduct.Product.SubGroup.Name,
                    p_trade_mark = x.StockroomProduct.Product.Brand,
                    p_manufacturer = x.StockroomProduct.Product.Manufacturer.Name,
                    p_count = x.StockroomProduct.Amount,
                    p_name = x.StockroomProduct.Product.Name,
                    p_unit = x.StockroomProduct.MeasureUnit.Name,
                    p_count_fact = x.Count
                })
                .ToList();

            var existingStockRoomProductsIdsQuery = InventoryStockroomProductEntityService.GetAll()
                .Where(x => x.Inventory.Id == id)
                .Select(x => x.StockroomProduct.Id);

            var data = EntityService.GetAll()
                .Where(x => x.Id == id)
                .Select(x => new
                {
                    id = x.Id,
                    p_number = x.No,
                    p_date = x.Date,
                    p_kind = x.Kind,
                    p_warehouse = (long?)x.Warehouse.Id,
                    p_warehouse_display = x.Warehouse.Name,
                    p_chairman = (long?)x.Chairman.Id,
                    p_chairman_display = x.Chairman.Name,
                    p_director = (long?)x.Director.Id,
                    p_director_display = x.Director.Name
                })
                .First();

            var warehouseProducts = StockroomProductEntityService.GetAll()
                .Where(x => x.Warehouse.Id == data.p_warehouse)
                .Where(x => !existingStockRoomProductsIdsQuery.Contains(x.Id))
                .Select(x => new
                {
                    id = x.Id,
                    p_warehouse = x.Warehouse.Id,
                    p_group_display = x.Product.Group.Name,
                    p_subgroup_display = x.Product.SubGroup.Name,
                    p_trade_mark = x.Product.Brand,
                    p_manufacturer = x.Product.Manufacturer.Name,
                    p_count = x.Amount,
                    p_name = x.Product.Name,
                    p_unit = x.MeasureUnit.Name
                })
                .ToList();

            var result = data.Extend((row, x) =>
            {
                row["p_members"] = members.Count > 0 ? members.Select(m => m.Id) : new List<long>();
                row["p_members_display"] = members.Count > 0 ? members.Select(m => m.Name).Aggregate((z, y) => y != null ? z + ", " + y : z) : string.Empty;
                //row["p_existing_stockroomproducts"] = JsonConvert.SerializeObject(stockroomProducts);
                //row["p_stockroomproducts"] = JsonConvert.SerializeObject(warehouseProducts);
                row["p_existing_stockroomproducts"] = stockroomProducts;
                row["p_stockroomproducts"] = warehouseProducts;
            });

            return new ObjectResult { Data = result };
        }

        protected override IQueryable<InventoryRoute> PersonalizeQuery(IQueryable<InventoryRoute> query)
        {
            return query.Where(x => x.Employee == UserPrincipal.Employee);
        }

        protected override IDataResult DocumentList(StoreParams storeParams, IQueryable<InventoryRoute> routeQuery)
        {
            var kindDict = typeof(InventoryKind).AsDictionary();
            var typeDict = typeof(DocumentMovementType).AsDictionary();
            var stateDict = typeof(DocumentState).AsDictionary();
            
            var query = routeQuery
                .Select(x => new
                {
                    p_base_doc = x.Document.Id,
                    id = x.Id,
                    p_kind = x.Document.Kind,
                    p_number = x.Document.No,
                    p_date = x.Document.Date,
                    p_name = x.Document.Warehouse.Name,
                    p_warehouse_display = x.Document.Warehouse.Name,
                    p_warehouse_address = x.Document.Warehouse.Address,
                    p_type = x.MovementType,
                    x.Document.DocumentState
                })
                .Filter(storeParams);

            var data = query
                .Order(storeParams)
                .Paging(storeParams)
                .Extend((row, x) =>
                {
                    row["p_kind_display"] = kindDict.Get((int)x.p_kind);
                    row["p_type"] = typeDict.Get((int)x.p_type);
                    row["p_state"] = stateDict.Get((int)x.DocumentState);
                })
                .Highlight(storeParams)
                .ToList();

            return new ObjectListResult
            {
                Data = data,
                Count = query.Count()
            };
        }

        #region bottom lists

        [HttpPost, Route("warehouses")]
        public IDataResult InventoryWarehousesBottomList()
        {
            var storeParams = Request.GetStoreParams();

            var docIdStr = storeParams.FilterParams
                .Where(x => x.PropertyName == "p_base_doc")
                .Select(x => x.Value)
                .FirstOrDefault();

            var docId = docIdStr.SafeToString().ToLong();

            if (docId == 0)
            {
                return new ObjectListResult { Count = 0 };
            }

            var warehouseQuery = EntityService.GetAll()
                .Where(x => x.Id == docId)
                .Select(x => x.Warehouse);

            var listData = WarehouseService.GetBottomList(warehouseQuery, storeParams, false);

            return new ObjectListResult
            {
                Data = listData.Item1,
                Count = listData.Item2
            };
        }

        [HttpPost, Route("products")]
        public IDataResult InventoryProductsBottomList()
        {
            return GetProductsBottomList(Request, ProductType.Product);
        }

        [HttpPost, Route("services")]
        public IDataResult InventoryServicesBottomList()
        {
            return GetProductsBottomList(Request, ProductType.Service);
        }

        private IDataResult GetProductsBottomList(HttpRequestMessage request, ProductType productType)
        {
            var storeParams = request.GetStoreParams();

            var docIdStr = storeParams.FilterParams
               .Where(x => x.PropertyName == "p_base_doc")
               .Select(x => x.Value)
               .FirstOrDefault();

            var docId = docIdStr.SafeToString().ToLong();

            if (docId == 0)
            {
                return new ObjectListResult { Count = 0 };
            }

            var productsQuery = InventoryStockroomProductEntityService.GetAll()
                .Where(x => x.Inventory.Id == docId)
                .Where(x => x.StockroomProduct.Product.Type == productType)
                .Select(x => x.StockroomProduct);

            var listData = StockroomProductService.GetBottomList(productsQuery, storeParams, false);

            return new ObjectListResult
            {
                Data = listData.Item1,
                Count = listData.Item2
            };
        }

        #endregion bottom lists
    }
}