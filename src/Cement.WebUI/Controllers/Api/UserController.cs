﻿using System;
using System.Linq;
using System.Web.Http;
using Cement.Core.Auth;
using Cement.Core.Auth.Impl;
using Cement.Core.Entities;
using Cement.Core.Extensions;
using Cement.Core.Services.StoreParams;
using Cement.Protocol;
using Cement.WebUI.Extensions;

namespace Cement.WebUI.Controllers.Api
{
    [RoutePrefix("api/user")]
    public class UserController : DataController<User, WarehouseProxy>
    {
        public IPasswordHashProvider PasswordHashProvider { get; set; }
        public IPasswordGenerator PasswordGenerator { get; set; }

        protected override IDataResult ListInternal(StoreParams storeParams)
        {
            var query = EntityService.GetAll()
                .Where(x => x.Role == UserRole.Root)
                .Select(x => new
                {
                    id = x.Id,
                    p_login = x.Login,
                    p_state = x.IsBlocked
                })
                .Filter(storeParams);

            var count = query.Count();

             var data = query
                .Order(storeParams)
                .Paging(storeParams)
                .Extend((row, x) =>
                {
                    row["p_state"] = x.p_state ? "Заблокирован" : "Активен";

                    var isRoot = x.p_login == RootUserManager.RootUserLogin;

                    row["p_is_deletable"] = isRoot;
                    row["p_is_blockable"] = isRoot || x.p_state;
                    row["p_is_unblockable"] = isRoot || !x.p_state;
                })
                .Highlight(storeParams)
                .ToList();

            return new ObjectListResult
            {
                Count = count,
                Data = data
            };
        }

        public override JsResult Save()
        {
            var requestNvc = Request.ToNameValueCollection();

            var login = requestNvc.Get("login");

            if (string.IsNullOrWhiteSpace(login))
            {
                return new JsResult();
            }

            var user = new User(UserRole.Root)
            {
                Email = login,
                Login = login,
                Name = login
            };

            user.SetNewPassword(PasswordHashProvider, PasswordGenerator.Generate());

            EntityService.Save(user);

            return new JsResult(user.Id);
        }

        [HttpPost, Route("manage/{block}/{id}")]
        public JsResult ManageUserState(bool block, long id)
        {
            try
            {
                var user = EntityService.Get(id);

                if (user != null)
                {
                    user.IsBlocked = block;
                    EntityService.Update(user);
                }

                return JsResult.Successful();
            }
            catch (Exception exc)
            {
                return new JsResult
                {
                    Msg = exc.Message,
                    Id = id,
                    Success = false
                };
            }
        }
    }
}