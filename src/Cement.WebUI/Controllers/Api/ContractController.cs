﻿using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using Cement.Core;
using Cement.Core.Auth;
using Cement.Core.Entities;
using Cement.Core.Enums;
using Cement.Core.Extensions;
using Cement.Core.Services;
using Cement.Core.Services.Contract;
using Cement.Core.Services.EntityService;
using Cement.Core.Services.StoreParams;
using Cement.Protocol;
using Cement.WebUI.Extensions;

namespace Cement.WebUI.Controllers.Api
{
    [System.Web.Http.RoutePrefix("api/contract")]
    public class ContractController : MovingDocumentDataController<Contract, DictionaryObject, ContractMovement>
    {
        public IEntityService<ContractApplication> ContractApplicationEntityService { get; set; }
        public IEntityService<ContractAnnexPriceList> ContractAnnexPriceListEntityService { get; set; }
        public IEntityService<ContractProduct> ContractProductEntityService { get; set; }
        public IEntityService<ContractAdditional> ContractAdditionalEntityService { get; set; }
        public IEntityService<ContractSpecification> ContractSpecificationEntityService { get; set; }
        public IEntityService<ClientGroup> ClientGroupEntityService { get; set; }
        public IEntityService<PersonalClient> PersonalClientEntityService { get; set; }
        public IEntityService<Organization> OrganizationEntityService { get; set; }
        public IEntityService<ContractAnnexPriceList> ContractAnnexPriceListService { get; set; }
        public IProductRequestService<ProductRequest> ProductRequestService { get; set; }

        private IQueryable<Contract> GetContractsQuery(IUserPrincipal userPrincipal)
        {
            return EntityService.GetAll()
                .Where(x => x.Organization == UserPrincipal.Organization || (UserPrincipal.Organization != null && x.Client == UserPrincipal.Organization.Client));
        }

        [System.Web.Http.HttpPost, System.Web.Http.Route("rentcontractstree")]
        public List<ContractServiceTreeElement> RentContractsTree(TreeRequest request)
        {
            if (request == null || request.Node == 0)
            {
                var contracts = EntityService.GetAll()
                    .Where(x => x.Organization == UserPrincipal.Organization)
                    .Select(x => new
                    {
                        x.Id,
                        x.Name
                    })
                    .AsEnumerable()
                    .Select(x => new ContractServiceTreeElement
                    {
                        Id = x.Id,
                        Name = x.Name,
                        Leaf = false
                    })
                    .ToList();

                return contracts;
            }

            var services = ContractProductEntityService.GetAll()
                .Where(x => x.Contract.Id == request.Node)
                .Where(x => x.Product.Type == ProductType.Service)
                .Select(x => new ContractServiceTreeElement
                {
                    Id = x.Id,
                    Name = x.Product.Name,
                    GroupName = x.Product.Group.Name,
                    SubGroupName = x.Product.SubGroup.Name,
                    ManufacturerName = x.Product.Manufacturer.Name,
                    Tax = ((decimal?)x.Tax.Rate) != null ? x.Tax.Rate * 100 + " %" : string.Empty,
                    UnitName = x.MeasureUnit.ShortName,
                    Price = x.Price,
                    Leaf = true
                })
                .AsEnumerable()
                .Distinct()
                .ToList();

            return services;
        }

        [System.Web.Http.HttpPost, System.Web.Http.Route("contractstree")]
        public List<NamedFirmContractTreeElement> ContractsTree(TreeRequestStringNodeWithFirmId request)
        {
            if (request == null || request.Node == "0")
            {
                return ClientGroupEntityService.GetAll()
                    .OrganizationFilter(UserPrincipal)
                    .Where(x => x.Parent == null)
                    .Select(x => new NamedFirmContractTreeElement
                    {
                        Id = string.Format("{0}{1}0", x.Id, Constants.DocBranchIdDelimeter),
                        Name = x.Name,
                        Leaf = false
                    })
                    .ToList();
            }

            var namedTreeElementList = new List<NamedFirmContractTreeElement>();

            var keyParts = request.Node.Split(Constants.DocBranchIdDelimeter).ToList();

            if (keyParts.Count != 2)
            {
                return namedTreeElementList;
            }

            var parentId = keyParts.First().ToLong();
            var treeLevel = keyParts.Last().ToLong();

            var organizationClientIdsQuery = OrganizationEntityService.GetAll()
                .Where(x => x.Id == request.p_firm_id)
                .Select(x => x.Client.Id);

            switch (treeLevel)
            {
                case 0:
                    return ClientGroupEntityService.GetAll()
                    .OrganizationFilter(UserPrincipal)
                    .Where(x => x.Parent.Id == parentId)
                    .Select(x => new NamedFirmContractTreeElement
                    {
                        Id = string.Format("{0}{1}1", x.Id, Constants.DocBranchIdDelimeter),
                        Name = x.Name,
                        Leaf = false
                    })
                    .ToList();
                case 1:
                    return PersonalClientEntityService.GetAll()
                    .OrganizationFilter(UserPrincipal)
                    .Where(x => organizationClientIdsQuery.Contains(x.Client.Id))
                    .Where(x => x.Subgroup.Id == parentId)
                    .Select(x => new NamedFirmContractTreeElement
                    {
                        Id = string.Format("{0}{1}2", x.Client.Id, Constants.DocBranchIdDelimeter),
                        Name = x.Client.Name,
                        Leaf = false
                    })
                    .ToList();

                case 2:
                    return EntityService.GetAll()
                    .OrganizationFilter(x => x.Organization, UserPrincipal)
                    .Where(x => x.Client.Id == parentId)
                    .Join(OrganizationEntityService.GetAll(),
                        x => x.Client.Id,
                        y => y.Client.Id,
                        (x, y) => new NamedFirmContractTreeElement
                            {
                                Name = x.Name,
                                Id = x.Id,
                                Leaf = true
                            })
                        .ToList();
            }

            return namedTreeElementList;
        }

        [System.Web.Http.HttpPost, System.Web.Http.Route("contractsrequesttree")]
        public List<NamedFirmContractTreeElement> ContractsProductRequestTree(TreeRequestStringNodeWithFirmId request)
        {
            long productId = 0;
            if (request.request_id > 0)
            {
                productId = ProductRequestService.GetInitialProductId(request.request_id);
            }

#warning Тут надо разобраться с бизнес-логикой договоров, чтобы определиться с фильтрами

            return ContractProductEntityService.GetAll()
                //.OrganizationFilter(x => x.Contract.Organization, UserPrincipal)
                //.Where(x => x.Contract.Client.Id == request.p_firm_id)
                .Where(x => (x.Contract.Client.Id == request.p_firm_id && x.Contract.Organization.Id == UserPrincipal.Organization.Id)
                         || (x.Contract.Client.Id == UserPrincipal.Organization.Id && x.Contract.Organization.Id == request.p_firm_id))
                .WhereIf(productId > 0, x => x.Product.Id == productId)
                .Select(x => new NamedFirmContractTreeElement
                {
                    Name = x.Contract.Name,
                    Id = x.Contract.Id,
                    Leaf = true
                })
                .ToList();
        }

        [System.Web.Http.HttpPost, System.Web.Http.Route("contractproduct")]
        public List<PriceListProductProxy> ContractProduct()
        {
            var storeParams = Request.GetStoreParams();
            var requestIdStr = storeParams.GetDataPropertyByName("request_id");
            long productId = 0;
            if (requestIdStr != null)
            {
                var requestId = requestIdStr.ToLong();
                if (requestId > 0)
                {
                    productId = ProductRequestService.GetInitialProductId(requestId);
                }
            }

            var products = ContractProductEntityService.GetAll()
                .WhereIf(productId > 0, x => x.Product.Id == productId)
                .Select(x => new PriceListProductProxy
                {
                    Id = x.Product.Id,
                    p_contract_id = x.Contract.Id,
                    Name = x.Product.Name,
                    MeasureUnit = x.MeasureUnit.Name,
                    MeasureUnitId = (long?)x.MeasureUnit.Id,
                    Price = x.Price,
                    Tax = x.Tax.Rate * 100
                })
                .Filter(storeParams)
                .ToList();

            return products;
        }

        protected override IQueryable<ContractMovement> PersonalizeQuery(IQueryable<ContractMovement> query)
        {
            return query
                .OrganizationFilter(x => x.Organization, UserPrincipal)
                .Where(x => x.Employee == null || x.Employee == UserPrincipal.Employee);
        }

        protected override IDataResult DocumentList(StoreParams storeParams, IQueryable<ContractMovement> contractMovementQuery)
        {
            var query = contractMovementQuery
                .Select(x => new
                {
                    p_base_doc = x.Document.Id,
                    id = x.Id,
                    p_number = x.Document.No,
                    p_date = x.Document.Date,
                    p_name = x.Document.Name,
                    p_type_display = x.MovementType,
                    p_document_kind_display = "Договор поставки Ж/Д"
                })
                .Filter(storeParams);

            var count = query.Count();

            var data = query
                .Order(storeParams)
                .Paging(storeParams)
                .Extend((row, x) =>
                {
                    row["p_type_display"] = x.p_type_display.GetDisplayValue();
                })
                .Highlight(storeParams)
                .ToList();

            return new ObjectListResult
            {
                Data = data,
                Count = count
            };
        }

        protected override IDataResult ListInternal(StoreParams storeParams)
        {
            var query = GetContractsQuery(UserPrincipal)
                .Select(x => new
                {
                    id = x.Id,
                    p_number = x.No,
                    p_date = x.Date,
                    p_name = x.Name,
                    p_document_kind_display = "Договор поставки Ж/Д",
                    p_contract_kind = 1
                })
                .Filter(storeParams);

            var count = query.Count();

            var data = query
                .Order(storeParams)
                .Paging(storeParams)
                .Highlight(storeParams)
                .ToList();

            return new ObjectListResult
            {
                Data = data,
                Count = count
            };
        }

        [System.Web.Http.HttpPost, System.Web.Http.Route("save/{formKind}")]
        public virtual JsResult Save(int formKind)
        {
            var contractService = Container.ResolveAll<IContractService>().SingleOrDefault(x => x.FormKind == formKind);

            if (contractService == null)
            {
                return new JsResult { Msg = "Not implemented", Success = false };
            }

            var result = contractService.Save(Request, UserPrincipal);

            return new JsResult
            {
                Id = result.Id,
                Msg = result.Message,
                Success = result.Success
            };
        }

        [System.Web.Http.HttpGet, System.Web.Http.Route("get/{formKind}/{id}")]
        public virtual IContract Get(int formKind, long id)
        {
            var contractService = Container.ResolveAll<IContractService>().SingleOrDefault(x => x.FormKind == formKind);

            if (contractService == null)
            {
                return null;
            }

            var result = contractService.Get(id);
            return result;
        }

        [System.Web.Http.HttpPost, System.Web.Http.Route("getcurrentorggetter")]
        public IDataResult GetCurrentOrganizationAsGetter()
        {
            var currentOrganization = UserPrincipal.Organization;
            var currentEmployee = UserPrincipal.Employee;

            return new RequestClientProxy
            {
                Id = currentOrganization.Id,
                Name = currentOrganization.Name,
                Address = currentOrganization.Client.Address,
                EmployeeId = currentEmployee.Id,
                EmployeeName = currentEmployee.Name
            };
        }
    }

    public class ContractRequest
    {
        public long request_id { get; set; }
    }
}