﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using Cement.Core.Entities;
using Cement.Core.Enums;
using Cement.Core.Extensions;
using Cement.Core.Services.EntityService;
using Cement.Core.Services.StoreParams;
using Cement.Protocol;
using Cement.WebUI.Extensions;
using Cement.WebUI.Framework;
using Cement.WebUI.Navigation;
using Newtonsoft.Json;

namespace Cement.WebUI.Controllers.Api
{
    [RoutePrefix("api/permission")]
    public class PermissionController : CementApiController
    {
        public IEntityService<Employee> EmployeEntityService { get; set; }

        public IEntityService<User> UserEntityService { get; set; }

        public IEntityService<UserPermission> UserPermissionEntityService { get; set; }

        public INavigationProvider NavigationProvider { get; set; }

        [HttpPost, Route("list")]
        public virtual IDataResult List()
        {
            var storeParams = Request.GetStoreParams();

            return ListInternal(storeParams);
        }

        protected IDataResult ListInternal(StoreParams storeParams)
        {
            var query = UserEntityService.GetAll()
                .OrganizationFilter(x => x.Organization, UserPrincipal)
                .Where(x => x.Employee != null)
                .Where(x => x.Role == UserRole.Employee)
                .Select(x => new
                {
                    id = x.Id,
                    p_employee_id = x.Employee.Id,
                    p_name = x.Employee.Name,
                    p_position_name = x.Employee.Position.Name,
                    p_department_name = x.Employee.Department.Name
                });
           
            var queryData = query
                .Order(storeParams)
                .Paging(storeParams)
                .ToList();

            var userIds = queryData.Select(x => x.id).ToList();

            var permissionsDict = UserPermissionEntityService.GetAll()
                .Where(x => userIds.Contains(x.User.Id))
                .Select(x => new
                {
                    x.Permission,
                    x.AccessType,
                    x.User.Id
                })
                .AsEnumerable()
                .GroupBy(x => x.Id)
                .ToDictionary(
                    x => x.Key, 
                    x => x.ToDictionary(y => y.Permission, y => y.AccessType));

            var permissionNames = NavigationProvider.GetPermissionNameList();

            var data = queryData
                .Extend((row, x) =>
                {
                    if (!permissionsDict.ContainsKey(x.id))
                    {
                        return;
                    }

                    var userPermissions = permissionsDict[x.id];

                    permissionNames.ForEach(permissionName =>
                    {
                        row[permissionName] = userPermissions.Get(permissionName, AccessType.Denied);
                    });
                })
                .ToList();

            var count = query.Count();
            
            return new ObjectListResult
            {
                Data = data,
                Count = count
            };
        }

        [HttpGet, Route("tree")]
        public List<PermissionTreeElement> GetTree([FromUri]IdRequest request)
        {
            if (request == null || request.Id <= 0)
            {
                return new List<PermissionTreeElement>();
            }

            var userId = request.Id;

            var navigationItems = NavigationProvider.GetFullNavigation();

            var permissionsDict = UserPermissionEntityService.GetAll()
                .Where(x => x.User.Id == userId)
                .Select(x => new
                {
                    x.Permission,
                    x.AccessType,
                    x.Description
                })
                .AsEnumerable()
                .ToDictionary(x => x.Permission);

            var tree = navigationItems
                .Select(x => new PermissionTreeElement
                {
                    Text = x.Name,
                    Id = x.Permission,
                    Leaf = false,
                    Expanded = true,
                    Children = x.Items != null
                        ? x.Items
                            .Select(y =>
                            {
                                var result = new PermissionTreeElement
                                {
                                    Id = y.Permission,
                                    Text = y.Name,
                                    AccessType = AccessType.Denied,
                                    UserId = userId,
                                    Leaf = true
                                };

                                if (permissionsDict.ContainsKey(y.Permission))
                                {
                                    var value = permissionsDict[y.Permission];
                                    result.AccessType = value.AccessType;
                                    result.Description = value.Description;
                                }
                                return result;
                            })
                            .ToList()
                        : null

                })
                .ToList();

            return tree;
        }

        [HttpPost, Route("tree")]
        public void UpdateTree(List<PermissionTreeElement> userPermissionList)
        {
            if (userPermissionList == null || userPermissionList.Count == 0)
            {
                return;
            }

            var userId = userPermissionList[0].UserId;

            if (userId <= 0)
            {
                return;
            }

            var existingUserPermissionDict = UserPermissionEntityService.GetAll()
                .Where(x => x.User.Id == userId)
                .AsEnumerable()
                .ToDictionary(x => x.Permission);

            var user = UserEntityService.Load(userId);

            foreach (var userPermission in userPermissionList)
            {
                if (existingUserPermissionDict.ContainsKey(userPermission.Id))
                {
                    var existing = existingUserPermissionDict[userPermission.Id];

                    if (existing.AccessType != userPermission.AccessType
                        || existing.Description != userPermission.Description)
                    {
                        existing.AccessType = userPermission.AccessType;
                        existing.Description = userPermission.Description;

                        UserPermissionEntityService.Update(existing);
                    }
                }
                else
                {
                    UserPermissionEntityService.Save(new UserPermission
                    {
                        User = user,
                        AccessType = userPermission.AccessType,
                        Description = userPermission.Description,
                        Permission = userPermission.Id
                    });
                }
            }
        }
        
        [HttpPost, Route("save")]
        public virtual JsResult Save()
        {
            return new JsResult();
        }
    }

    public class IdRequest
    {
        public long Id { get; set; }
    }

    public class PermissionTreeElement
    {
        [JsonProperty(PropertyName = "leaf")]
        public bool Leaf { get; set; }

        [JsonProperty(PropertyName = "expanded")]
        public bool Expanded { get; set; }

        [JsonProperty(PropertyName = "text")]
        public string Text { get; set; }
        
        [JsonProperty(PropertyName = "permission")]
        public AccessType AccessType { get; set; }

        [JsonProperty(PropertyName = "id")]
        public string Id { get; set; }

        [JsonProperty(PropertyName = "user_id")]
        public long UserId { get; set; }

        [JsonProperty(PropertyName = "data")]
        public List<PermissionTreeElement> Children { get; set; }

        [JsonProperty(PropertyName = "description")]
        public string Description { get; set; }
    }

}