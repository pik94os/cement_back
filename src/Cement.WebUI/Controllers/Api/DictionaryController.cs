﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using Cement.Core.Entities;
using Cement.Core.Entities.Dict;
using Cement.Core.Entities.Stockroom;
using Cement.Core.Enums;
using Cement.Core.Extensions;
using Cement.Core.Services.Dictionary;
using Cement.Protocol;
using Cement.Protocol.JsonConverters;
using Cement.WebUI.Extensions;
using Cement.WebUI.Framework;
using DataAccess.Repository;

namespace Cement.WebUI.Controllers.Api
{
    [Authorize]
    [RoutePrefix("api/dict")]
    public class DictionaryController : CementApiController
    {
        public IDictService DictService { get; set; }

        [HttpPost, Route("populate")]
        public void PopulateDictionaries()
        {
            Container.Resolve<IDictService>().Populate(HttpRuntime.AppDomainAppPath);
        }


        [HttpPost, Route("get")]
        public CommonDictionaries Dictionaries()
        {
            var measures = Container.Resolve<IRepository<MeasureUnit>>().GetAll().ToList();
            var clientRepository = Container.Resolve<IRepository<Client>>();

            var commonDict = new CommonDictionaries();

            var drivingCategories = Container.Resolve<IRepository<DriverCategory>>().GetAll().ToList();

            commonDict.TransportDrivingCategories = drivingCategories
                .Select(x => new DictionaryObject
                {
                    Id = x.Id,
                    Name = x.Type == DriverCategoryType.Tractor ? string.Format("{0} - Трактор", x.Name) : x.Name
                })
                .OrderBy(x => x.Id)
                .ToList();

            commonDict.DrivingCategories = drivingCategories
                .Where(x => x.Type == DriverCategoryType.AutoMoto)
                .Select(x => new DictionaryObject { Id = x.Id, Name = x.Name }).OrderBy(x => x.Id).ToList();

            commonDict.TractorDrivingCategories = drivingCategories
                .Where(x => x.Type == DriverCategoryType.Tractor)
                .Select(x => new DictionaryObject { Id = x.Id, Name = x.Name }).OrderBy(x => x.Id).ToList();

            commonDict.DrivingExperience = Container.Resolve<IRepository<Experience>>().GetAll()
                .Where(x => x.Type == ExperienceType.Driving)
                .Select(x => new DictionaryObject { Id = x.Id, Name = x.Name }).OrderBy(x => x.Id).ToList();

            commonDict.EmployeePositions = Container.Resolve<IRepository<Position>>().GetAll()
                .Select(x => new DictionaryObject { Id = x.Id, Name = x.Name }).OrderBy(x => x.Name).ToList();

            commonDict.TransportRegionCodes = Container.Resolve<IRepository<RegionCode>>().GetAll()
                .Select(x => new DictionaryObject { Id = x.Id, Name = x.Name }).ToList();

            commonDict.TransportCountryCodes = Container.Resolve<IRepository<CountryCode>>().GetAll()
                .Select(x => new DictionaryObject { Id = x.Id, Name = x.Name }).ToList();

            commonDict.TransportCharClasses = Container.Resolve<IRepository<VehicleClass>>().GetAll()
                .Select(x => new DictionaryObject { Id = x.Id, Name = x.Name }).OrderBy(x => x.Id).ToList();

            commonDict.TransportCharTypes = Container.Resolve<IRepository<VehicleBodyType>>().GetAll()
                .Select(x => new DictionaryObject { Id = x.Id, Name = x.Name }).ToList();

            commonDict.TransportCarOptions = Container.Resolve<IRepository<VehicleAdditionalOptions>>().GetAll()
                .Select(x => new DictionaryObject { Id = x.Id, Name = x.Name }).ToList();

            commonDict.TransportCarOptions = Container.Resolve<IRepository<VehicleAdditionalOptions>>().GetAll()
                .Select(x => new DictionaryObject { Id = x.Id, Name = x.Name }).ToList();

            commonDict.WheelFormulas = Container.Resolve<IRepository<VehicleWheelBase>>().GetAll()
                .Select(x => new DictionaryObject { Id = x.Id, Name = x.Name }).ToList();

            commonDict.TransportCategories = GetTransportCategories();

            commonDict.ProductAllUnits = measures
                .Where(x => !x.IsNational)
                .Where(x => !x.IsNotInEskk)
                .Select(x => new DictionaryObject { Id = x.Id, Name = x.Name })
                .OrderBy(x => x.Name)
                .ToList();

            commonDict.ProductCountries = Container.Resolve<IRepository<Country>>().GetAll()
                .Select(x => new DictionaryObject { Id = x.Id, Name = x.Name })
                .OrderBy(x => x.Name)
                .ToList();
            
            commonDict.ProductDangerClasses = Container.Resolve<IRepository<DangerClass>>().GetAll()
                .OrderBy(x => x.Num)
                .Select(x => new DictionaryObject { Id = x.Id, Name = x.Num + " " + x.Name }).ToList();

            commonDict.ProductImporters = clientRepository.GetAll().Select(x => new DictionaryObject { Id = x.Id, Name = x.Name }).ToList();

            commonDict.ProductManufacturers = clientRepository.GetAll().Select(x => new DictionaryObject { Id = x.Id, Name = x.Name }).ToList();

            commonDict.ProductPackageKinds = Container.Resolve<IRepository<PackingType>>().GetAll()
                .Select(x => new DictionaryObject { Id = x.Id, Name = x.Name }).ToList();

            commonDict.ProductPackageMaterials = Container.Resolve<IRepository<PackingMaterial>>().GetAll()
                .Select(x => new DictionaryObject { Id = x.Id, Name = x.Name }).ToList();

            commonDict.ProductPackers = clientRepository.GetAll().Select(x => new DictionaryObject { Id = x.Id, Name = x.Name }).ToList();

            commonDict.ProductSizeUnits = measures.Where(x => x.MeasureUnitGroup == MeasureUnitGroup.Length)
                .Select(x => new DictionaryObject { Id = x.Id, Name = x.ShortName }).ToList();

            commonDict.ProductTaxes = Container.Resolve<IRepository<Tax>>().GetAll()
                .OrderBy(x => x.Rate)
                .AsEnumerable()
                .Select(x => new DictionaryObject { Id = x.Id, Name = x.StringValue }).ToList();

            commonDict.ProductUnCodes = Container.Resolve<IRepository<DangerUnCode>>().GetAll()
                .OrderBy(x => x.Name)
                .Select(x => new DictionaryObject { Id = x.Id, Name = x.Name }).ToList();

            commonDict.ProductVolumeUnits = measures.Where(x => x.MeasureUnitGroup == MeasureUnitGroup.Volume)
                .Select(x => new DictionaryObject { Id = x.Id, Name = x.ShortName }).ToList();

            commonDict.ProductWeightUnits = measures.Where(x => x.MeasureUnitGroup == MeasureUnitGroup.Weight)
                .Select(x => new DictionaryObject { Id = x.Id, Name = x.ShortName }).ToList();

            commonDict.ServiceManufacturers = clientRepository.GetAll().Select(x => new DictionaryObject { Id = x.Id, Name = x.Name }).ToList();

            commonDict.ServiceUnits = measures.Select(x => new DictionaryObject { Id = x.Id, Name = x.ShortName }).ToList();

            commonDict.InnerDocumentKinds = typeof(InternalDocumentType).AsDictionary()
                .Where(x => x.Key != (int)InternalDocumentType.Disposal)
                .Select(x => new DictionaryObject { Id = x.Key, Name = x.Value }).ToList();

            commonDict.InnerDocumentImportances = typeof(ImportanceType).AsDictionary()
                .Select(x => new DictionaryObject { Id = x.Key, Name = x.Value }).ToList();

            commonDict.InnerDocumentSecrecies = typeof(SecrecyType).AsDictionary()
                .Select(x => new DictionaryObject { Id = x.Key, Name = x.Value }).ToList();

            commonDict.GoodsImportances = commonDict.InnerDocumentImportances;
            commonDict.GoodsSecrecies = commonDict.InnerDocumentSecrecies;

            commonDict.GoodsImportancesColored = typeof(ImportanceType).AsDictionary()
                .Select(x => new DictionaryFlagObject { Id = x.Key, Name = x.Value, Flag = Enum.GetName(typeof(ImportanceType), x.Key) }).ToList();

            commonDict.GoodsSecreciesColored = typeof(SecrecyType).AsDictionary()
                .Select(x => new DictionaryFlagObject { Id = x.Key, Name = x.Value, Flag = Enum.GetName(typeof(SecrecyType), x.Key) }).ToList();

            commonDict.MessageKinds = typeof(MessageType).AsDictionary()
                .Select(x => new DictionaryObject { Id = x.Key, Name = x.Value }).ToList();

            commonDict.ClientGroups = GetClientGroups();

            commonDict.ClientEventReminderTypes = typeof(PersonalClientEventReminderType).AsDictionary()
                .Select(x => new DictionaryObject { Id = x.Key, Name = x.Value }).ToList();

            commonDict.InventoryKinds = typeof(InventoryKind).AsDictionary()
                .Select(x => new DictionaryObject { Id = x.Key, Name = x.Value }).ToList();

            commonDict.LoadKinds = new List<DictionaryObject>();
            commonDict.LoadKinds.Add(new DictionaryObject { Id = 1, Name = "Самовывоз" });
            //commonDict.LoadKinds.Add(new DictionaryObject { Id = 2, Name = "Самовывоз (по договору)" });
            commonDict.LoadKinds.Add(new DictionaryObject { Id = 3, Name = "Централизация" });

            commonDict.TransportUnitType = new List<DictionaryObject>();
            commonDict.TransportUnitType.Add(new DictionaryObject { Id = 1, Name = "Одиночка" });
            commonDict.TransportUnitType.Add(new DictionaryObject { Id = 2, Name = "Полуприцеп" });
            commonDict.TransportUnitType.Add(new DictionaryObject { Id = 3, Name = "Сцепка" });

            commonDict.AccountingOtherMovementBillKinds = typeof(AccountingOtherMovementBillKind).AsDictionary()
                .Select(x => new DictionaryObject { Id = x.Key, Name = x.Value }).ToList();

            commonDict.AccountingOtherWriteoffActKinds = typeof(AccountingOtherWriteoffActKind).AsDictionary()
                .Select(x => new DictionaryObject { Id = x.Key, Name = x.Value }).ToList();

            commonDict.AccountingInOutDocWayBillSaleKinds = typeof(AccountingInOutDocWayBillSaleKind).AsDictionary()
                .Select(x => new DictionaryObject { Id = x.Key, Name = x.Value }).ToList();

            commonDict.AccountingInOutDocWayBillSaleShippingKinds = typeof(AccountingInOutDocWayBillSaleShippingKind).AsDictionary()
                .Select(x => new DictionaryObject { Id = x.Key, Name = x.Value }).ToList();

            SetProductGroups(commonDict);

            return commonDict;
        }
        
        [HttpPost, Route("packtypelist")]
        public IDataResult PackTypeList()
        {
            var storeParams = Request.GetStoreParams();

            var query = Container.Resolve<IRepository<PackingType>>().GetAll()
               .Select(x => new
               {
                   id = x.Id,
                   p_name = x.Name,
                   p_text = x.Description,
                   p_img = new Downloadable(x.Photo.Id)
               })
               .Filter(storeParams);

            var count = query.Count();

            var data = query
                .Order(storeParams)
                .Paging(storeParams)
                .Highlight(storeParams)
                .ToList();

            return new ObjectListResult
            {
                Data = data,
                Count = count
            };
        }

        private List<TransportCategory> GetTransportCategories()
        {
            var vehicleSubcategoryDict = Container.Resolve<IRepository<VehicleSubCategory>>().GetAll()
                .Select(x => new
                {
                    CatId = x.Category.Id,
                    x.Id,
                    x.Name, 
                    x.Category.Type
                })
                .AsEnumerable()
                .GroupBy(x => x.CatId)
                .ToDictionary(x => x.Key, x => x.Select(y => new TreeElement { Id = y.Type == VehicleCategoryType.Trailer ? -y.Id: y.Id, Text = y.Name }).ToList());

            return Container.Resolve<IRepository<VehicleCategory>>().GetAll()
                .Select(x => new { x.Id, x.Name, x.HasСharacteristics })
                .AsEnumerable()
                .Select(x => new TransportCategory
                {
                    Id = x.Id,
                    Text = x.Name,
                    HasСharacteristics = x.HasСharacteristics,
                    Children = vehicleSubcategoryDict.Get(x.Id)
                })
                .ToList();
        }

        private List<TreeElement> GetClientGroups()
        {
            var clientGroups = Container.Resolve<IRepository<ClientGroup>>().GetAll()
                .OrganizationFilter(UserPrincipal)
                .Select(x => new
                {
                    parentId = (long?)x.Parent.Id,
                    x.Id,
                    x.Name
                })
                .ToList();

            var clientGroupDict = clientGroups
                .Where(x => x.parentId.HasValue)
                .GroupBy(x => x.parentId)
                .ToDictionary(x => x.Key, x => x.Select(y => new TreeElement { Id = y.Id, Text = y.Name }).ToList());

            return clientGroups
                .Where(x => x.parentId == null)
                .Select(x => new TreeElement
                {
                    Id = x.Id,
                    Text = x.Name,
                    Children = clientGroupDict.Get(x.Id)
                })
                .ToList();
        }

        private void SetProductGroups(CommonDictionaries commonDict)
        {
            var groups = Container.Resolve<IRepository<ProductGroup>>().GetAll()
                .Select(x => new
                {
                    x.Id,
                    x.Name,
                    x.ProductType
                })
                .OrderBy(y => y.Name)
                .ToList();

            var subgroups = Container.Resolve<IRepository<ProductSubGroup>>().GetAll()
                .Where(x => x.ProductGroup != null)
                .Select(x => new
                {
                    x.Id, 
                    x.Name,
                    groupId = x.ProductGroup.Id
                })
                .AsEnumerable()
                .GroupBy(x => x.groupId)
                .ToDictionary(
                    x => x.Key, 
                    x => x.Select(y => new TreeElement { Id = y.Id, Text = y.Name })
                          .OrderBy(y => y.Text)
                          .ToList());

            commonDict.ProductGroups = groups
                .Where(x => x.ProductType == ProductType.Product)
                .Select(x => new TreeElement
                {
                    Id = x.Id,
                    Text = x.Name,
                    Children = subgroups.Get(x.Id)
                })
                .ToList();

            commonDict.ServiceGroups = groups
                .Where(x => x.ProductType == ProductType.Service)
                .Select(x => new TreeElement
                {
                    Id = x.Id,
                    Text = x.Name,
                    Children = subgroups.Get(x.Id)
                })
                .ToList();
        }

        [HttpPost, Route("list")]
        public virtual IDataResult List()
        {
            var storeParams = Request.GetStoreParams();

            var result = DictService.ListByType(storeParams);

            return new ObjectListResult
            {
                Data = result.Item1,
                Count = result.Item2
            };
        }

        [HttpPost, Route("list_entities")]
        public virtual IDataResult ListEntities()
        {
            return new ObjectListResult
            {
                Data = DictService.ListEntities()
            };
        }

        [HttpPost, Route("save")]
        public virtual JsResult Save()
        {
            var incomeProxy = Request.ToType<DictionaryEntity>();

            var result = DictService.Save(incomeProxy);

            return new JsResult(result);
        }

        [HttpPost, Route("delete/{entityType}/{id}")]
        public virtual JsResult Delete(string entityType, long id)
        {
            try
            {
                DictService.Delete(entityType, id);
                return JsResult.Successful();
            }
            catch (Exception exc)
            {
                return new JsResult
                {
                    Msg = exc.Message,
                    Id = id,
                    Success = false
                };
            }
        }
    }
}
