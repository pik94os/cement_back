﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Http;
using Cement.Core.Entities;
using Cement.Core.Enums;
using Cement.Core.Extensions;
using Cement.Core.Services.EntityService;
using Cement.Core.Services.StoreParams;
using Cement.Protocol;
using Cement.Protocol.JsonConverters;
using Cement.WebUI.Extensions;
using DataAccess.Repository;
using Newtonsoft.Json;

namespace Cement.WebUI.Controllers.Api
{
    [RoutePrefix("api/catalogue")]
    public class CatalogueController : DataController<Catalogue, CatalogueProxy>
    {
        public IEntityService<PriceListProduct> PriceListProductService { get; set; }

        private IDataResult ListGoods(ProductType productType)
        {

            var storeParams = Request.GetStoreParams();

            var query = EntityService.GetAll()
                //.OrganizationFilter(x => x.Organization, UserPrincipal)
                .Join(PriceListProductService.GetAll(),
                x => x.PriceList.Id,
                y => y.PriceList.Id,
                (a, b) => new { Catalogue = a, PriceListProduct = b})
                .Where(x => x.PriceListProduct.Product.Type == productType)
                .Select(x => new
                {
                    p_group = x.PriceListProduct.Product.Group.Name,
                    p_manufacturer = x.Catalogue.Organization.Name,
                    // p_manufacturer = x.PriceListProduct.Product.Manufacturer.Name,
                    p_name = x.PriceListProduct.Product.Name,
                    p_subgroup = x.PriceListProduct.Product.SubGroup.Name,
                    p_trade_mark = x.PriceListProduct.Product.Brand,
                    p_tax = x.PriceListProduct.Tax,
                    p_price = x.PriceListProduct.Price,
                    p_price_display = x.PriceListProduct.PriceList.Name,
                    p_found_country_display = x.PriceListProduct.Product.Country.Name,
                    p_found_country = (long?)x.PriceListProduct.Product.Country.Id,
                    p_found_manufacturer_display = x.PriceListProduct.Product.Manufacturer.Name,
                    p_found_manufacturer = (long?)x.PriceListProduct.Product.Manufacturer.Id,
                    p_found_packer_display = x.PriceListProduct.Product.Packer.Name,
                    p_found_packer = (long?)x.PriceListProduct.Product.Packer.Id,
                    p_found_importer_display = x.PriceListProduct.Product.Importer.Name,
                    p_found_importer = (long?)x.PriceListProduct.Product.Importer.Id,
                    p_danger_class_display = x.PriceListProduct.Product.DangerClass.Name,
                    p_danger_class = (long?)x.PriceListProduct.Product.DangerClass.Id,
                    p_danger_sub_class_display = x.PriceListProduct.Product.DangerSubClass.Name,
                    p_danger_sub_class = (long?)x.PriceListProduct.Product.DangerSubClass.Id,
                    p_danger_oon_display = x.PriceListProduct.Product.DangerUnCode.Name,
                    p_danger_oon = (long?)x.PriceListProduct.Product.DangerUnCode.Id,
                    p_tu_gost = x.PriceListProduct.Product.Gost,
                    p_tu_gost_r = x.PriceListProduct.Product.GostR,
                    p_tu_ost = x.PriceListProduct.Product.Ost,
                    p_tu_stp = x.PriceListProduct.Product.Stp,
                    p_tu_tu = x.PriceListProduct.Product.Tu,
                    p_code_okp = x.PriceListProduct.Product.Okp,
                    p_code_th_ved = x.PriceListProduct.Product.TnVed,
                    p_code_zd = x.PriceListProduct.Product.CargoTrainCode,
                    p_code_bar = x.PriceListProduct.Product.Barcode,
                    p_sert_soot_number = x.PriceListProduct.Product.ConformityCertificateNumber,
                    p_sert_soot_img1 = new Downloadable(x.PriceListProduct.Product.ConformityCertificateScan.Id),
                    p_sert_soot_img2 = new Downloadable(x.PriceListProduct.Product.ConformityCertificateScan2.Id),
                    p_sert_deklar_number = x.PriceListProduct.Product.ConformityDeclarationNumber,
                    p_sert_deklar_img = new Downloadable(x.PriceListProduct.Product.ConformityDeclarationScan.Id),
                    p_sert_san_number = x.PriceListProduct.Product.HealthCertificateNumber,
                    p_sert_san_img = new Downloadable(x.PriceListProduct.Product.HealthCertificateScan.Id),
                    p_sert_fire_number = x.PriceListProduct.Product.FireSafetyCertificateNumber,
                    p_sert_fire_img = new Downloadable(x.PriceListProduct.Product.FireSafetyCertificateScan.Id),
                    p_sert_fire_deklar = x.PriceListProduct.Product.FireSafetyDeclarationNumber,
                    p_sert_fire_deklar_img = new Downloadable(x.PriceListProduct.Product.FireSafetyDeclarationScan.Id),
                    p_package_item_kind_display = x.PriceListProduct.Product.TypeSingle.Name,
                    p_package_item_material_display = x.PriceListProduct.Product.MaterialSingle.Name,
                    p_package_item_volume_display = x.PriceListProduct.Product.VolumeSingle + " " + x.PriceListProduct.Product.VolumeUnitSingle.Name,
                    p_package_item_weight_display = x.PriceListProduct.Product.WeightSingle + " " + x.PriceListProduct.Product.WeightUnitSingle.Name,
                    p_package_item_size_display = (x.PriceListProduct.Product.WidthSingle.HasValue ? x.PriceListProduct.Product.WidthSingle.Value + "x" : string.Empty) +
                                                  (x.PriceListProduct.Product.HeightSingle.HasValue ? x.PriceListProduct.Product.HeightSingle.Value + "x" : string.Empty) +
                                                  (x.PriceListProduct.Product.LengthSingle.HasValue ? x.PriceListProduct.Product.LengthSingle.Value + " " : string.Empty) +
                                                  x.PriceListProduct.Product.SizeUnitSingle.Name,
                    p_package_group_kind_display = x.PriceListProduct.Product.TypeGroup.Name,
                    p_package_group_material_display = x.PriceListProduct.Product.MaterialGroup.Name,
                    p_package_group_volume_display = x.PriceListProduct.Product.VolumeGroup + " " + x.PriceListProduct.Product.VolumeUnitGroup.Name,
                    p_package_group_weight_display = x.PriceListProduct.Product.WeightGroup + " " + x.PriceListProduct.Product.WeightUnitGroup.Name,
                    p_package_group_size_display = (x.PriceListProduct.Product.WidthGroup.HasValue ? x.PriceListProduct.Product.WidthGroup.Value + "x" : string.Empty) +
                                                   (x.PriceListProduct.Product.HeightGroup.HasValue ? x.PriceListProduct.Product.HeightGroup.Value + "x" : string.Empty) +
                                                   (x.PriceListProduct.Product.LengthGroup.HasValue ? x.PriceListProduct.Product.LengthGroup.Value + " " : string.Empty) +
                                                   x.PriceListProduct.Product.SizeUnitGroup.Name,
                    p_package_vehicle_kind_display = x.PriceListProduct.Product.TypeTransport.Name,
                    p_package_vehicle_material = x.PriceListProduct.Product.MaterialTransport.Name,
                    p_package_vehicle_volume_display = x.PriceListProduct.Product.VolumeTransport + " " + x.PriceListProduct.Product.VolumeUnitTransport.Name,
                    p_package_vehicle_weight_display = x.PriceListProduct.Product.WeightTransport + " " + x.PriceListProduct.Product.WeightUnitTransport.Name,
                    p_package_vehicle_size_display = (x.PriceListProduct.Product.WidthTransport.HasValue ? x.PriceListProduct.Product.WidthTransport.Value + "x" : string.Empty) +
                                                   (x.PriceListProduct.Product.HeightTransport.HasValue ? x.PriceListProduct.Product.HeightTransport.Value + "x" : string.Empty) +
                                                   (x.PriceListProduct.Product.LengthTransport.HasValue ? x.PriceListProduct.Product.LengthTransport.Value + " " : string.Empty) +
                                                   x.PriceListProduct.Product.SizeUnitTransport.Name,
                    p_package_vehicle_count = x.PriceListProduct.Product.UnitsCountTransport,
                    p_package_group_count = x.PriceListProduct.Product.UnitsCountGroup,
                    p_package_display = x.PriceListProduct.Product.PackageDisplayString,
                    p_complect_display = x.PriceListProduct.Product.ComplectString,
                    p_package_type = x.PriceListProduct.Product.Kind,
                    p_code_okun = x.PriceListProduct.Product.Okun,
                    p_code_partnumber = x.PriceListProduct.Product.Article,
                    p_unit = (long?)x.PriceListProduct.Product.MeasureUnit.Id,
                    p_unit_display = x.PriceListProduct.Product.MeasureUnit.Name
                })
                .Filter(storeParams);

            var count = query.Count();

            var data = query
                .Order(storeParams)
                .Paging(storeParams)
                .Highlight(storeParams)
                .ToList();

            return new ObjectListResult
            {
                Data = data,
                Count = count
            };
        }

        [HttpPost, Route("productslist")]
        public IDataResult ProductsList()
        {
            return ListGoods(ProductType.Product);
        }

        [HttpPost, Route("serviceslist")]
        public IDataResult ServicesList()
        {
            return ListGoods(ProductType.Service);
        }
       
        public override JsResult Save()
        {
            var incomeProxy = Request.ToType<CatalogueProxy>();

            var entity = AutoMapper.Mapper.DynamicMap<Catalogue>(incomeProxy);

            EntityService.Save(entity);

            return new JsResult(entity.Id);
        }

    }
}