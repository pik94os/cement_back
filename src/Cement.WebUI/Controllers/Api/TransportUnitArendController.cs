﻿using System;
using System.Linq;
using System.Web.Http;
using Cement.Core.Entities;
using Cement.Core.Enums;
using Cement.Core.Extensions;
using Cement.Core.Services;
using Cement.Protocol;
using Cement.Protocol.DetailsServices;
using Cement.WebUI.Extensions;

namespace Cement.WebUI.Controllers.Api
{
    [RoutePrefix("api/transportunitarend")]
    public class TransportUnitArendController : DataController<TransportUnitArend, TransportUnitArendProxy>
    {
        public ITransportUnitService TransportUnitService { get; set; }

        public IDetailsService<Employee> EmployeeDetailsService { get; set; }
        public IDetailsService<Client> ClientDetailsService { get; set; }

        [HttpPost, Route("listarended")]
        public IDataResult ListArended()
        {
            var storeParams = Request.GetStoreParams();

            var query = TransportUnitService.GetArendedTransportUnitQuery(UserPrincipal.Organization)
                .Select(x => new
                {
                    id = x.Id,
                    p_name = x.TransportUnit.Name,
                    p_auto_mark_model = x.TransportUnit.Vehicle.ModelFullName,
                    p_auto_number = x.TransportUnit.Vehicle.Number,
                    p_auto_trailer = x.TransportUnit.Trailer.ModelFullName,
                    p_auto_trailer_number = x.TransportUnit.Trailer.Number,
                    p_driver_name = x.TransportUnit.Driver.FirstName,
                    p_driver_lastname = x.TransportUnit.Driver.Patronymic,
                    p_driver_surname = x.TransportUnit.Driver.Surname,
                    p_class_display = x.TransportUnit.Vehicle.Class.Name,
                    p_char_ops = (VehicleType?)x.TransportUnit.Vehicle.Type,
                    p_char_type_display = x.TransportUnit.Vehicle.BodyType.Name,
                    p_load_back = x.TransportUnit.Vehicle.IsRearLoading ? 1 : 0,
                    p_load_top = x.TransportUnit.Vehicle.IsOverLoading ? 1 : 0,
                    p_load_left = x.TransportUnit.Vehicle.IsSideLoading ? 1 : 0,
                    
                    p_control_got_name = x.TransportUnit.Organization.Client.Name,
                    p_control_got_address = x.TransportUnit.Organization.Client.Address,
	                p_control_date_start = x.DateStart,
                    p_control_date_end = x.DateEnd
                })
                .Filter(storeParams);

            var count = query.Count();

            var data = query
                .Order(storeParams)
                .Paging(storeParams)
                .Highlight(storeParams)
                .ToList();

            return new ObjectListResult
            {
                Data = data,
                Count = count
            };
        }

        [HttpPost, Route("listgiventoarend")]
        public IDataResult ListGivenToArend()
        {
            var storeParams = Request.GetStoreParams();

            var query = TransportUnitService.GetGivenToArendTransportUnitQuery(UserPrincipal.Organization)
                .Select(x => new
                {
                    id = x.Id,
                    p_name = x.TransportUnit.Name,
                    p_auto_mark_model = x.TransportUnit.Vehicle.ModelFullName,
                    p_auto_number = x.TransportUnit.Vehicle.Number,
                    p_auto_trailer = x.TransportUnit.Trailer.ModelFullName,
                    p_auto_trailer_number = x.TransportUnit.Trailer.Number,
                    p_driver_name = x.TransportUnit.Driver.FirstName,
                    p_driver_lastname = x.TransportUnit.Driver.Patronymic,
                    p_driver_surname = x.TransportUnit.Driver.Surname,
                    p_class_display = x.TransportUnit.Vehicle.Class.Name,
                    p_char_ops = (VehicleType?)x.TransportUnit.Vehicle.Type,
                    p_char_type_display = x.TransportUnit.Vehicle.BodyType.Name,
                    p_load_back = x.TransportUnit.Vehicle.IsRearLoading ? 1 : 0,
                    p_load_top = x.TransportUnit.Vehicle.IsOverLoading ? 1 : 0,
                    p_load_left = x.TransportUnit.Vehicle.IsSideLoading ? 1 : 0,

                    p_control_sent_name = x.Arendator.Name,
                    p_control_sent_address = x.Arendator.Address,
                    p_control_date_start = x.DateStart,
                    p_control_date_end = x.DateEnd
                })
                .Filter(storeParams);

            var count = query.Count();

            var data = query
                .Order(storeParams)
                .Paging(storeParams)
                .Highlight(storeParams)
                .ToList();

            return new ObjectListResult
            {
                Data = data,
                Count = count
            };
        }

        [HttpPost, Route("endarend/{id}")]
        public JsResult EndArend(long id)
        {
            try
            {
                var arend = EntityService.Get(id);

                if (arend != null)
                {
                    arend.IsActive = false;
                    EntityService.Update(arend);
                }

                return JsResult.Successful();
            }
            catch (Exception exc)
            {
                return new JsResult
                {
                    Msg = exc.Message,
                    Id = id,
                    Success = false
                };
            }
        }

        public override object Details(long id)
        {
            var data = EntityService.GetAll()
                .Where(x => x.Id == id)
                .Select(x => new 
                {
                    vehicleId = (long?)x.TransportUnit.Vehicle.Id,
                    trailerId = (long?)x.TransportUnit.Trailer.Id,
                    driverId = (long?) x.TransportUnit.Driver.Id,
                    firmId = (long?)x.Arendator.Id
                })
                .FirstOrDefault();

            if (data == null)
            {
                return new TransportUnitArendDetailProxy();
            }

            var result = new TransportUnitArendDetailProxy
            {
                auto = new { id = data.vehicleId },
                trailer = new { id = data.trailerId },
                firm = ClientDetailsService.GetDetails(data.firmId),
                driver = EmployeeDetailsService.GetDetails(data.driverId)
            };

            return result;
        }

        [HttpGet, Route("detailsarended/{id}")]
        public object DetailsArended(long id)
        {
            var data = EntityService.GetAll()
                .Where(x => x.Id == id)
                .Select(x => new
                {
                    vehicleId = (long?)x.TransportUnit.Vehicle.Id,
                    trailerId = (long?)x.TransportUnit.Trailer.Id,
                    driverId = (long?)x.TransportUnit.Driver.Id,
                    firmId = (long?)x.TransportUnit.Organization.Client.Id
                })
                .FirstOrDefault();

            if (data == null)
            {
                return new TransportUnitArendDetailProxy();
            }

            var result = new TransportUnitArendDetailProxy
            {
                auto = new { id = data.vehicleId },
                trailer = new { id = data.trailerId },
                firm = ClientDetailsService.GetDetails(data.firmId),
                driver = EmployeeDetailsService.GetDetails(data.driverId)
            };

            return result;
        }
    }
}