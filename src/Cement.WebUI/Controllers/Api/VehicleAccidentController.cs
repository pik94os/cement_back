﻿using System.Linq;
using System.Web.Http;
using Cement.Core.Entities;
using Cement.Core.Extensions;
using Cement.Core.Services.StoreParams;
using Cement.Protocol;

namespace Cement.WebUI.Controllers.Api
{
    [RoutePrefix("api/vehicleaccident")]
    public class VehicleAccidentController : DataController<VehicleAccident, VehicleAccidentProxy>
    {
        protected override IDataResult ListInternal(StoreParams storeParams)
        {
            var query = EntityService.GetAll()
                .OrganizationFilter(x => x.Vehicle.Organization, UserPrincipal)
                .Select(x => new
                {
                    id = x.Id,
                    p_transport_unit = x.Vehicle.Id,
                    p_transport_unit_display = x.Vehicle.Name,
                    p_date = x.Date,
                    p_count = x.Count,
                    p_damage = x.Damage,
                    p_comment = x.Description,
                    p_sum = x.Cost,
                    p_driver = (long?)x.Driver.Id,
                    p_driver_display = x.Driver.Name
                })
                .Filter(storeParams);

            var count = query.Count();

            var data = query
                .Order(storeParams)
                .Paging(storeParams)
                .Highlight(storeParams)
                .ToList();

            return new ObjectListResult
            {
                Data = data,
                Count = count
            };
        }
    }
}