﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Http;
using Cement.Core.Entities;
using Cement.Core.Enums;
using Cement.Core.Extensions;
using Cement.Core.Services;
using Cement.Core.Services.EntityService;
using Cement.Core.Services.Impl;
using Cement.Core.Services.StoreParams;
using Cement.Protocol;
using Cement.Protocol.JsonConverters;
using Cement.WebUI.Extensions;
using DataAccess.Repository;
using Newtonsoft.Json;

namespace Cement.WebUI.Controllers.Api
{
    [RoutePrefix("api/pricelist")]
    public class PriceListController : DataController<PriceList, PriceListProxy>
    {
        public IEntityService<Product> ProductService { get; set; }
        public IEntityService<ProductPersonal> ProductPersonalService { get; set; }
        public IEntityService<ContractAnnexPriceList> ContractAnnexPriceListService { get; set; }
        public IEntityService<PriceListProduct> PriceListProductService { get; set; }
        public IEntityService<InternalDocument> InternalDocumentService { get; set; }
        public IDocumentService DocumentService { get; set; }
        
        protected override IDataResult ListInternal(StoreParams storeParams)
        {
            var query = EntityService.GetAll()
                .OrganizationFilter(UserPrincipal)
                .Select(x => new
                {
                    id = x.Id,
                    p_name = x.Name,
                    p_date_start = x.DateStart,
                    p_date_end = x.DateEnd,
                    p_replace_name = x.ReplacementPriceList.Name,
                    p_replace = (long?)x.ReplacementPriceList.Id,
                    p_replace_date_end = x.ReplacementPriceList.DateEnd,
                    p_replace_date_start = x.ReplacementPriceList.DateStart,
                    x.DescriptionBytes
                })
                .Filter(storeParams);

            var count = query.Count();

            var data = query
                .Order(storeParams)
                .Paging(storeParams)
                .Extend((row, x) =>
                {
                    row["p_comment"] = x.DescriptionBytes.GetString();
                })
                .Highlight(storeParams)
                .ToList();

            return new ObjectListResult
            {
                Data = data,
                Count = count
            };
        }

        protected IDataResult DocumentList(StoreParams storeParams, PriceListStatus priceListStatus)
        {
            var priceListQuery = EntityService.GetAll()
                .Where(x => x.Status == priceListStatus);
            
            var priceListIdsQuery = priceListQuery
                .OrganizationFilter(UserPrincipal)
                .Select(x => x.Id);

            var priceListProductDict = PriceListProductService.GetAll()
                .Where(x => priceListIdsQuery.Contains(x.PriceList.Id))
                .Select(x => new
                {
                    productId = x.Product.Id,
                    priceListId = x.PriceList.Id,
                    productName = x.Product.Name
                })
                .AsEnumerable()
                .GroupBy(x => x.priceListId)
                .ToDictionary(x => x.Key,
                    x => new
                    {
                        productIdList = JsonConvert.SerializeObject(x.Select(y => y.productId).ToList()),
                        productsString = x.Select(y => y.productName).Aggregate((a, b) => string.Format("{0}, {1}", a, b))
                    });


            //var priceListMessageDocumentDictionary =
            //    priceListQuery
            //    .OrganizationFilter(UserPrincipal)
            //    .Where(x => x.MessageDocument != null)
            //    .Select(x => new
            //    {
            //        priceListId = x.Id,
            //        messageDocumentId = x.MessageDocument.Id,
            //        AuthorId = (long?)x.MessageDocument.Author.Id,
            //        AuthorName = x.MessageDocument.Author.Name,
            //        SignerId = (long?)x.MessageDocument.Signer.Id,
            //        SignerName = x.MessageDocument.Signer.Name,
            //        x.MessageDocument.Secrecy,
            //        x.MessageDocument.Importance,
            //        x.MessageDocument.ReviewBefore,
            //        File = new Downloadable(x.MessageDocument.File.Id)
            //    })
            //    .AsEnumerable()
            //    .ToDictionary(x => x.priceListId, x => x);

            
            var query = priceListQuery
                .OrganizationFilter(UserPrincipal)
                .Select(x => new
                {
                    id = x.Id,
                    p_name = x.Name,
                    p_date_start = x.DateStart,
                    p_date_end = x.DateEnd,
                    p_replace_name = x.ReplacementPriceList.Name,
                    p_replace_display = x.ReplacementPriceList.Name,
                    p_replace = (long?)x.ReplacementPriceList.Id,
                    p_replace_date_end = x.ReplacementPriceList.DateEnd,
                    p_replace_date_start = x.ReplacementPriceList.DateStart,
                    x.DescriptionBytes,
                    messageDocumentId = (long?)x.MessageDocument.Id,
                    p_author = (long?)x.MessageDocument.Author.Id,
                    p_author_display = x.MessageDocument.Author.Name,
                    p_sign_for = (long?)x.MessageDocument.Signer.Id,
                    p_sign_for_display = x.MessageDocument.Signer.Name,
                    p_security = (SecrecyType?)x.MessageDocument.Secrecy,
                    p_importance = (ImportanceType?)x.MessageDocument.Importance,
                    p_deadline = x.MessageDocument.ReviewBefore,
                    p_import = new Downloadable(x.MessageDocument.File.Id),
                    DocumentState = (DocumentState?)x.MessageDocument.DocumentState
                })
                .Filter(storeParams);

            var count = query.Count();

            var queryData = query
                .Order(storeParams)
                .Paging(storeParams)
                .ToList();

            var documentIds = queryData.Select(x => x.id).ToList();
            var relatedDocuments = DocumentService.GetRelatedDocumentsDict(documentIds);

            var data = queryData
                .Extend((row, x) =>
                {
                    row["p_comment"] = x.DescriptionBytes.GetString();
                    if (priceListProductDict.ContainsKey(x.id))
                    {
                        var currentPriceListProducts = priceListProductDict[x.id];
                        row["p_products"] = currentPriceListProducts.productIdList;
                        row["p_products_display"] = currentPriceListProducts.productsString;
                    }

                    var relatedDocs = relatedDocuments.Get(x.id);

                    if (relatedDocs != null)
                    {
                        row["p_documents"] = relatedDocs.Ids;
                        row["p_documents_display"] = relatedDocs.Display;
                    }

                    if (priceListStatus == PriceListStatus.Working && x.DocumentState.HasValue)
                    {
                        row["p_status"] = x.DocumentState.Value.GetDisplayValue();
                    }
                     
                })
                .Highlight(storeParams)
                .ToList();

            return new ObjectListResult
            {
                Data = data,
                Count = count
            };
        }

        [HttpPost, Route("newlist")]
        public IDataResult NewList()
        {
            var storeParams = Request.GetStoreParams();

            return DocumentList(storeParams, PriceListStatus.New);
        }

        [HttpPost, Route("inworklist")]
        public IDataResult InWorkList()
        {
            var storeParams = Request.GetStoreParams();

            return DocumentList(storeParams, PriceListStatus.Working);
        }

        [HttpPost, Route("archivelist")]
        public IDataResult ArchiveList()
        {
            var storeParams = Request.GetStoreParams();

            return DocumentList(storeParams, PriceListStatus.Archive);
        }

        [HttpPost, Route("archive/{id}")]
        public JsResult Archive(long id)
        {
            return ManageArchive(id, true);
        }

        [HttpPost, Route("unarchive/{id}")]
        public JsResult Unarchive(long id)
        {
            return ManageArchive(id, false);
        }

        /// <summary>
        /// Управление архивированием
        /// </summary>
        protected JsResult ManageArchive(long id, bool archivate, string successText = "Успешно")
        {
            try
            {
                var priceList = EntityService.Get(id);

                if (priceList != null)
                {
                    if (archivate)
                    {
                        priceList.Status = PriceListStatus.Archive;
                    }
                    else
                    {
                        priceList.Status = PriceListStatus.Working;
                    }

                    EntityService.Update(priceList);
                }

                return JsResult.Successful(successText);
            }
            catch (Exception exc)
            {
                return new JsResult
                {
                    Msg = exc.Message,
                    Id = id,
                    Success = false
                };
            }
        }

        public override JsResult Save()
        {
            var incomeProxy = Request.ToType<PriceListProxy>();

            var entity = AutoMapper.Mapper.DynamicMap<PriceList>(incomeProxy);

            EntityService.Save(entity);

            return new JsResult(entity.Id);
        }

        [HttpPost, Route("products")]
        public IDataResult Products()
        {
            var query = ProductPersonalService.GetAll()
                .OrganizationFilter(UserPrincipal)
                .Where(x => x.ArchiveDate == null)
                .Select(x => new
                {
                    id = x.Product.Id,
                    p_group = x.Product.Group.Name,
                    p_manufacturer = x.Product.Manufacturer.Name,
                    p_name = x.Product.Name,
                    p_subgroup = x.Product.SubGroup.Name,
                    p_text = string.Empty,
                    p_trade_mark = x.Product.Brand
                });

            var count = query.Count();

            var data = query
                .ToList();

            return new ObjectListResult
            {
                Data = data,
                Count = count
            };
        }

        [HttpPost, Route("contractproducts")]
        public IDataResult ContractPriceListProducts(ContractRequestProxy requestProxy)
        {
            if (requestProxy.p_contract_id < 0)
            {
                return new ObjectListResult();
            }

            return GetContractProducts(requestProxy.p_contract_id, ProductType.Product);
        }

        [HttpPost, Route("contractservices")]
        public IDataResult ContractPriceListServices(ContractRequestProxy requestProxy)
        {
            if (requestProxy.p_contract_id < 0)
            {
                return new ObjectListResult();
            }

            return GetContractProducts(requestProxy.p_contract_id, ProductType.Service);
        }

        private IDataResult GetContractProducts(long contractId, ProductType type)
        {
            var priceListIdsQuery = ContractAnnexPriceListService.GetAll()
                .Where(x => x.Contract.Id == contractId)
                .Where(x => x.PriceList.Status != PriceListStatus.Archive)
                .Select(x => x.PriceList.Id);

            var query = PriceListProductService.GetAll()
                .OrganizationFilter(x => x.PriceList.Organization, UserPrincipal)
                .WhereIf(contractId > 0, x => priceListIdsQuery.Contains(x.PriceList.Id))
                .Where(x => x.Product.Type == type)
                .Select(x => new
                {
                    id = x.Id,
                    p_group = x.Product.Group.Name,
                    p_manufacturer = x.Product.Manufacturer.Name,
                    p_name = x.Product.Name,
                    p_subgroup = x.Product.SubGroup.Name,
                    p_text = string.Empty,
                    p_trade_mark = x.Product.Brand,
                    p_price = x.Price,
                    p_product_unit_display = x.MeasureUnit.Name,
                    p_tax = x.Tax
                });

            var count = query.Count();

            var data = query
                .ToList();

            return new ObjectListResult
            {
                Data = data,
                Count = count
            };
        }

        [HttpPost, Route("warehouse_pricelist")]
        public virtual List<PriceListProductTreeElement> WarehousePricelist()
        {
            return EntityService.GetAll()
                .OrganizationFilter(UserPrincipal)
                .Select(x => new PriceListProductTreeElement
                {
                    Id = x.Id,
                    Name = x.Name,
                    Leaf = true
                })
                .ToList();
        }

        [HttpPost, Route("warehouse_product")]
        public virtual List<PriceListProductProxy> WarehouseProduct()
        {
            var storeParams = Request.GetStoreParams();
            var products = PriceListProductService.GetAll()
                .Select(x => new PriceListProductProxy
                {
                    Id = x.Product.Id,
                    Name = x.Product.Name,
                    p_pricelist_id = x.PriceList.Id,
                    PriceListProduct = x.Id,
                    MeasureUnit = x.MeasureUnit.Name,
                    Price = x.Price,
                    Tax = x.Tax.Rate * 100,
                    MeasureUnitId = x.MeasureUnit.Id
                })
                .Filter(storeParams)
                .ToList();

            return products;
        }
    }
}