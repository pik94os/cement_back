﻿using System.Linq;
using System.Web.Http;
using Cement.Core.Auth;
using Cement.Core.Entities;
using Cement.Core.Extensions;
using Cement.Core.Services.EntityService;
using Cement.Core.Services.StoreParams;
using Cement.Protocol;

namespace Cement.WebUI.Controllers.Api
{
    [RoutePrefix("api/contractannex")]
    public class ContractAnnexController : MovingDocumentDataController<ContractAnnexBase, DictionaryObject, ContractAnnexMovement>
    {
        private IQueryable<ContractAnnexBase> GetContractAnnexQuery()
        {
            return EntityService.GetAll()
                .Where(x => x.Contract.Organization == UserPrincipal.Organization || 
                    (UserPrincipal.Organization != null && x.Contract.Client == UserPrincipal.Organization.Client));
        }

        protected override IQueryable<ContractAnnexMovement> PersonalizeQuery(IQueryable<ContractAnnexMovement> query)
        {
            return query
                .OrganizationFilter(x => x.Organization, UserPrincipal)
                .Where(x => x.Employee == null || x.Employee == UserPrincipal.Employee);
        }

        protected override IDataResult DocumentList(StoreParams storeParams, IQueryable<ContractAnnexMovement> contractAnnexMovementQuery)
        {
            var query = contractAnnexMovementQuery
                .Select(x => new
                {
                    p_base_doc = x.Document.Id,
                    id = x.Id,
                    p_number = x.Document.No,
                    p_date = x.Document.Date,
                    p_name = x.Document.Name,
                    p_type_display = x.MovementType,
                    p_document_kind_display = "Договор поставки Ж/Д",
                    p_document_to_display = x.Document.Contract.Name
                })
                .Filter(storeParams);

            var count = query.Count();

            var data = query
                .Order(storeParams)
                .Paging(storeParams)
                .Extend((row, x) =>
                {
                    row["p_type_display"] = x.p_type_display.GetDisplayValue();
                })
                .Highlight(storeParams)
                .ToList();

            return new ObjectListResult
            {
                Data = data,
                Count = count
            };
        }

        protected override IDataResult ListInternal(StoreParams storeParams)
        {
            var query = GetContractAnnexQuery()
                .Select(x => new
                {
                    id = x.Id,
                    p_number = x.No,
                    p_date = x.Date,
                    p_name = x.Name,
                    p_document_kind_display = "Договор поставки Ж/Д",
                    p_document_to_display = x.Contract.Name,
                    p_form_kind = x.Type,
                    p_contract_kind = 1
                })
                .Filter(storeParams);

            var count = query.Count();

            var data = query
                .Order(storeParams)
                .Paging(storeParams)
                .Highlight(storeParams)
                .ToList();

            return new ObjectListResult
            {
                Data = data,
                Count = count
            };
        }

        [HttpPost, Route("save/{formKind}")]
        public virtual JsResult Save(int formKind)
        {
            return new JsResult();
        }

        public override JsResult Save()
        {
           return new JsResult();
        }
    }
}