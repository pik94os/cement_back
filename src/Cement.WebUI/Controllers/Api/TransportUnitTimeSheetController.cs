﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using Cement.Core.Entities;
using Cement.Core.Enums;
using Cement.Core.Extensions;
using Cement.Core.Services;
using Cement.Core.Services.EntityService;
using Cement.Core.Services.StoreParams;
using Cement.Protocol;
using Cement.WebUI.Extensions;
using Cement.WebUI.Framework;
using DataAccess.SessionProvider;
using Newtonsoft.Json;

namespace Cement.WebUI.Controllers.Api
{
    [RoutePrefix("api/tunittimesheet")]
    public class TransportUnitTimeSheetController : CementApiController
    {
        public IEntityService<TransportUnitAccounting> EntityService { get; set; }

        public ITransportUnitService TransportUnitService { get; set; }

        public enum TimeSheetDisplayType
        {
            Hour = 10,
            Day = 20,
            Week = 30,
            Month = 40
        }

        [HttpPost, Route("list")]
        public virtual IDataResult List()
        {
            var storeParams = Request.GetStoreParams();

            return ListInternal(storeParams);
        }

        protected virtual IDataResult ListInternal(StoreParams storeParams)
        {
            var date = storeParams.GetFilterPropertyByName("p_date_start").ToDateTimeFromCementString("dd.MM.yyyy HH") ?? DateTime.Now;
            var strKind = storeParams.GetFilterPropertyByName("p_kind") ?? "week";
            var kind = TimeSheetDisplayType.Week;

            switch (strKind)
            {
                case "hour" : kind = TimeSheetDisplayType.Hour; break;
                case "day": kind = TimeSheetDisplayType.Day; break;
                case "week": kind = TimeSheetDisplayType.Week; break;
                case "month": kind = TimeSheetDisplayType.Month; break;
            }

            var query = TransportUnitService.GetOwnTransportUnitQuery(UserPrincipal.Organization, date)
                .OrderBy(x => x.Vehicle.ModelFullName)
                .ThenBy(x => x.Vehicle.NumberText)
                .ThenBy(x => x.Trailer.ModelFullName)
                .ThenBy(x => x.Trailer.NumberText)
                .Select(x => new
                {
                    p_name = x.Vehicle.ModelFullName,
                    p_number = x.Vehicle.NumberText,
                    id = x.Id
                })
                .Filter(storeParams);
            
            var queryData = query
                .Paging(storeParams)
                .ToList();

            var transportUnitIds = queryData.Select(x => x.id).ToList();

            var dateStart = date.Date;
            var dateEnd = date.Date;

            switch (kind)
            {
                case TimeSheetDisplayType.Month:
                {
                    var dt = new DateTime(date.Year, date.Month, 1);
                    dateStart = dt.AddDays(-4);
                    dateEnd = dt.AddMonths(1).AddDays(3);
                    break;
                }
                case TimeSheetDisplayType.Week:
                {
                    dateStart = date.Date.AddDays(-(int)date.DayOfWeek);
                    dateEnd = date.Date.AddDays(8 - (int)date.DayOfWeek);
                    break;
                }
                case TimeSheetDisplayType.Day:
                case TimeSheetDisplayType.Hour:
                {
                    dateStart = date.Date.AddDays(-1);
                    dateEnd = date.Date.AddDays(1);
                    break;
                }
            }

            var accountingDataDict = EntityService.GetAll()
                .OrganizationFilter(UserPrincipal)
                .Where(x => transportUnitIds.Contains(x.TransportUnit.Id))
                .Where(x => x.Date >= dateStart)
                .Where(x => x.Date <= dateEnd)
                .Select(x => new
                {
                    x.Id,
                    x.Date,
                    trUnitId = x.TransportUnit.Id,
                    x.AccountingType
                })
                .AsEnumerable()
                .GroupBy(x => x.trUnitId)
                .ToDictionary(x => x.Key, x => x.OrderBy(y => y.Date).ToList());

            var data = queryData
                .Extend((row, x) =>
                {
                    var accountingDataList = accountingDataDict.Get(x.id);

                    if (accountingDataList != null)
                    {
                        row["p_indicators"] = accountingDataList.Select(y => new
                        {
                            id = y.Id,
                            p_accounting = y.AccountingType,
                            p_date_start = y.Date.ToString("dd.MM.yyyy 00:00"),
                            p_date_end = y.Date.AddDays(1).ToString("dd.MM.yyyy 00:00"),
                        }).ToList();
                    }
                    else
                    {
                        row["p_indicators"] = new List<int>();
                    }
                })
                .ToList();

            return new ObjectListResult
            {
                Data = data,
                Count = data.Count
            };
        }

        [HttpPost, Route("unitlist")]
        public IDataResult ListTransportUnits()
        {
            var storeParams = Request.GetStoreParams();

            var date = storeParams.GetFilterPropertyByName("p_date").ToDateTimeFromCementString("dd.MM.yyyy");

            if (date == null)
            {
                return new ObjectListResult();
            }

            var accountingDataDict = EntityService.GetAll()
                .OrganizationFilter(UserPrincipal)
                .Where(x => x.Date == date)
                .Select(x => new
                {
                    x.Id,
                    x.Date,
                    trUnitId = x.TransportUnit.Id,
                    x.AccountingType
                })
                .AsEnumerable()
                .GroupBy(x => x.trUnitId)
                .ToDictionary(x => x.Key, x => x.First());
            
            var dateStr = date.Value.ToString("yyyy-MM-dd");

            var data = TransportUnitService.GetOwnTransportUnitQuery(UserPrincipal.Organization, date.Value)
                .Select(x => new
                {
                    p_unit_id = x.Id,
                    p_name = x.Name,
                    p_auto_mark_model = x.Vehicle.ModelFullName,
                    p_auto_number = x.Vehicle.NumberText,
                    p_auto_trailer = x.Trailer.ModelFullName,
                    p_auto_trailer_number = x.Trailer.NumberText,
                    p_driver_name = x.Driver.Name,
                    p_class_display = x.Vehicle.Class.Name,
                    p_go_display = x.Vehicle.GoText,
                    p_size_display = x.Vehicle.SizeText,
                    p_char_ops = (VehicleType?)x.Vehicle.Type,
                    p_char_type_display = x.Vehicle.BodyType.Name,
                    p_load_back = x.Vehicle.IsRearLoading,
                    p_load_top = x.Vehicle.IsOverLoading,
                    p_load_left = x.Vehicle.IsSideLoading,
                    p_additional_chars_display = x.Vehicle.ExtraCharacteristic,
                    p_additional_options_display = x.Vehicle.Option.Name,
                    p_accounting = (long?)null,
                    p_date = dateStr
                })
                .OrderBy(x => x.p_auto_mark_model)
                .ThenBy(x => x.p_auto_number)
                .ThenBy(x => x.p_auto_trailer)
                .ThenBy(x => x.p_auto_trailer_number)
                .Extend((row, x) =>
                {
                    var accountingData = accountingDataDict.Get(x.p_unit_id);

                    if (accountingData != null)
                    {
                        row["id"] = accountingData.Id;
                        row["p_accounting"] = accountingData.AccountingType;
                    }
                })
                .ToList();

            return new ObjectListResult
            {
                Data = data,
                Count = data.Count
            };

        }

        [HttpPost, Route("save")]
        public void Save(List<TimeSheetElement> timeSheetElementList)
        {
            if (timeSheetElementList == null || timeSheetElementList.Count == 0)
            {
                return;
            }

            var date = timeSheetElementList.First().Date;

            var data = EntityService.GetAll()
                .OrganizationFilter(UserPrincipal)
                .Where(x => x.Date == date)
                .AsEnumerable()
                .GroupBy(x => x.TransportUnit.Id)
                .ToDictionary(x => x.Key, x => x.First());

            var listForUpdate = new List<TransportUnitAccounting>();
            var listForCreate = new List<TransportUnitAccounting>();

            foreach (var timeSheetElement in timeSheetElementList)
            {
                if (data.ContainsKey(timeSheetElement.UnitId))
                {
                    var existing = data[timeSheetElement.UnitId];

                    if (existing.AccountingType != timeSheetElement.AccountingType)
                    {
                        existing.AccountingType = timeSheetElement.AccountingType;
                        listForUpdate.Add(existing);
                    }
                }
                else
                {
                    listForCreate.Add(new TransportUnitAccounting
                    {
                        Date = date,
                        AccountingType = timeSheetElement.AccountingType,
                        Organization = UserPrincipal.Organization,
                        TransportUnit = new TransportUnit { Id = timeSheetElement.UnitId }
                    });
                }
            }

            var sessionProvider = Container.Resolve<ISessionProvider>();

            using (var statelessSession = sessionProvider.OpenStatelessSession())
            {
                using (var transaction = statelessSession.BeginTransaction())
                {
                    try
                    {
                        listForCreate.ForEach(x => statelessSession.Insert(x));
                        listForUpdate.ForEach(statelessSession.Update);
                        transaction.Commit();
                    }
                    catch
                    {
                        transaction.Rollback();
                        throw;
                    }
                }
            }
        }
    }

    public class TimeSheetElement
    {
        [JsonProperty(PropertyName = "id")]
        public long Id { get; set; }

        [JsonProperty(PropertyName = "p_unit_id")]
        public long UnitId { get; set; }

        [JsonProperty(PropertyName = "p_date")]
        public DateTime Date { get; set; }

        [JsonProperty(PropertyName = "p_accounting")]
        public TransportUnitAccountingType AccountingType { get; set; }
    }
}