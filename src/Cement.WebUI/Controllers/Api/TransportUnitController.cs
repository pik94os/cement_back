﻿using System.Linq;
using System.Web.Http;
using Cement.Core.Entities;
using Cement.Core.Enums;
using Cement.Core.Extensions;
using Cement.Core.Services;
using Cement.Core.Services.StoreParams;
using Cement.Protocol;
using Cement.Protocol.DetailsServices;

namespace Cement.WebUI.Controllers.Api
{
    [RoutePrefix("api/transportunit")]
    public class TransportUnitController : DataController<TransportUnit, TransportUnitProxy>
    {
        public ITransportUnitService TransportUnitService { get; set; }

        public IDetailsService<Employee> EmployeeDetailsService { get; set; }

        [HttpPost, Route("listall")]
        public object Dropdownlist()
        {
            var data = TransportUnitService.GetTransportUnitQuery(UserPrincipal.Organization)
                .Select(x => new
                {
                    leaf = true,
                    id = x.Id,
                    p_name = x.Name,

                    p_additional_chars_display = x.Vehicle.ExtraCharacteristic,
                    p_additional_options_display = x.Vehicle.Option.Name,
                    p_auto_mark_model = x.Vehicle.ModelFullName,
                    p_auto_number = x.Vehicle.NumberText,
                    p_auto_trailer = x.Trailer.ModelFullName,
                    p_auto_trailer_number = x.Trailer.NumberText,
                    p_char_ops = (VehicleType?)x.Vehicle.Type,
                    p_char_type_display = x.Vehicle.BodyType.Name,
                    p_class_display = x.Vehicle.Class.Name,
                    p_driver_lastname = x.Driver.Patronymic,
                    p_driver_name = x.Driver.FirstName,
                    p_driver_surname = x.Driver.Surname,
                    p_go_display = x.Vehicle.GoText,
                    p_size_display = x.Vehicle.SizeText,
                    p_load_back = x.Vehicle.IsRearLoading,
                    p_load_left = x.Vehicle.IsSideLoading,
                    p_load_top = x.Vehicle.IsOverLoading
                    
                })
                .OrderBy(x => x.p_name)
                .ToList();

            return data;
        }

        protected override IDataResult ListInternal(StoreParams storeParams)
        {
            var query = TransportUnitService.GetOwnTransportUnitQuery(UserPrincipal.Organization)
                .Select(x => new
                {
                    id = x.Id,
                    p_name = x.Name,
                    p_auto_mark_model = x.Vehicle.ModelFullName,
                    p_auto_number = x.Vehicle.NumberText,
                    p_auto_trailer = x.Trailer.ModelFullName,
                    p_auto_trailer_number = x.Trailer.NumberText,
                    p_driver_name = x.Driver.FirstName,
                    p_driver_lastname = x.Driver.Patronymic,
                    p_driver_surname = x.Driver.Surname,
                    p_class_display = x.Vehicle.Class.Name,
                    p_go_display = x.Vehicle.GoText,
                    p_size_display = x.Vehicle.SizeText,
                    p_char_ops = (VehicleType?)x.Vehicle.Type,
                    p_char_type_display = x.Vehicle.BodyType.Name,
                    p_load_back = x.Vehicle.IsRearLoading,
                    p_load_top= x.Vehicle.IsOverLoading,
                    p_load_left= x.Vehicle.IsSideLoading,
                    p_additional_chars_display = x.Vehicle.ExtraCharacteristic,
                    p_additional_options_display = x.Vehicle.Option.Name,
                    p_date_start = x.DateStart
                })
                .Filter(storeParams);

            var count = query.Count();

            var data = query
                .Order(storeParams)
                .Paging(storeParams)
                .Highlight(storeParams)
                .ToList();

            return new ObjectListResult
            {
                Data = data,
                Count = count
            };
        }

        public override object Details(long id)
        {
            var result = EntityService.GetAll()
                .Where(x => x.Id == id)
                .Select(x => new
                {
                    driverId = (long?)x.Driver.Id,
                    vehicleId = (long?)x.Vehicle.Id,
                    trailerId = (long?)x.Trailer.Id
                })
                .FirstOrDefault();

            if (result == null)
            {
                return new TransportUnitOutProxy();
            }

            return new TransportUnitOutProxy
            {
                Driver = EmployeeDetailsService.GetDetails(result.driverId),
                Vehicle = new {id = result.vehicleId},
                Trailer = new {id = result.trailerId}
            };
        }
    }
}