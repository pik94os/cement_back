﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using Cement.Core.Entities;
using Cement.Core.Extensions;
using Cement.Protocol;
using Cement.WebUI.Framework;
using DataAccess.Repository;

namespace Cement.WebUI.Controllers.Api
{
    [Authorize]
    [RoutePrefix("api/addressdict")]
    public class AddressDictController : CementApiController
    {
        public IRepository<AddressDict> AddressDictRepository { get; set; }

        [HttpPost, Route("list")]
        public IDataResult List(AddressDictRequest request)
        {
            var result = new ListResult<AddressDictProxy>
            {
                Data = new List<AddressDictProxy>(),
                Count = 0
            };

            if (string.IsNullOrWhiteSpace(request.Query))
            {
                return result;
            }

            if (request.Query.Length < 4)
            {
                return result;
            }

            var data = AddressDictRepository.GetAll()
                .Where(x => x.Index.StartsWith(request.Query))
                //.WhereIf(request.Query.Length < 6, x => x.Index.StartsWith(request.Query))
                .Select(x => new 
                {
                    Id = x.Id,
                    Area = x.Area,
                    Country = x.Country,
                    Index = x.Index,
                    Locality = x.Locality,
                    Region = x.Region,
                    Street = x.Street
                })
                .OrderBy(x => x.Region)
                .ThenBy(x => x.Area)
                .ThenBy(x => x.Locality)
                .ThenBy(x => x.Street)
                .Extend((row, x) =>
                {
                    row["p_country_display"] = x.Country;
                    row["p_region_display"] = x.Region;
                    row["p_district_display"] = x.Area;
                    row["p_city_display"] = x.Locality;
                    row["p_street_display"] = x.Street;

                    row["p_country"] = x.Country;
                    row["p_region"] = x.Region;
                    row["p_district"] = x.Area;
                    row["p_city"] = x.Locality;
                    row["p_street"] = x.Street;
                    row["id"] = x.Id;
                    row["p_index"] = x.Index;

                })
                //.Take(10)
                .ToList();

            return new ObjectListResult
            {
                Data = data,
                Count = data.Count
            };
        }
    }
}
