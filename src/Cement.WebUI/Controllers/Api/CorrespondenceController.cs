﻿using System.Linq;
using System.Web.Http;
using Cement.Core.Entities;
using Cement.Core.Enums;
using Cement.Core.Extensions;
using Cement.Core.Services;
using Cement.Core.Services.EntityService;
using Cement.Core.Services.StoreParams;
using Cement.Protocol;
using Cement.Protocol.JsonConverters;
using Cement.WebUI.Extensions;

namespace Cement.WebUI.Controllers.Api
{
    [RoutePrefix("api/correspondence")]
    public class CorrespondenceController : MovingDocumentDataController<Correspondence, CorrespondenceProxy, CorrespondenceMovement>
    {
        #region injections
        
        public IEntityService<CorrespondenceRoute> CorrespondenceRouteEntityService { get; set; }

        public IDocumentService DocumentService { get; set; }

        public ICorrespondenceService CorrespondenceService { get; set; }

        #endregion injections
        
        protected override IDataResult ListInternal(StoreParams storeParams)
        {
            var kindDict = typeof(CorrespondenceType).AsDictionary();

            var outcomingQuery = PersonalizeQuery(base.GetOutcomingQuery());

            var query = EntityService.GetAll()
                .Where(x => x.SignedAt == null)
                .Where(x => outcomingQuery.Any(y => y.Document == x))
                .Select(x => new
                {
                    p_base_doc = x.Id,
                    id = x.Id,
                    p_kind = x.CorrespondenceType,
                    p_number = x.No,
                    p_date = x.Date,
                    p_task_display = x.DocumentState,
                    p_up_date = x.ReviewBefore,
                    p_deadline = x.ReviewBefore,
                    p_author = (long?)x.Author.Id,
                    p_author_display = x.Author.Name,
                    p_for_sign = (long?)x.Signer.Id,
                    p_for_sign_display = x.Signer.Name,
                    p_theme = x.Subject,
                    p_importance = x.Importance,
                    p_security = x.Secrecy,
                    p_content = x.Content,
                    p_nomenklature = (long?)x.Nomenclature.Id,
                    p_nomenklature_display = x.Nomenclature.CaseTitle,
                    p_nom_index = x.Nomenclature.CaseIndex,
                    p_nom_title = x.Nomenclature.CaseTitle,
                    p_nom_count = x.Nomenclature.StorageUnitsCount,
                    p_nom_deadline = x.Nomenclature.StorageLifeAndArticleNumbers,
                    p_nom_comment = x.Nomenclature.Description,
                    p_nom_year = x.Nomenclature.Year,
                    p_author_name = x.Author.Name,
                    p_author_department = x.Author.Department.Name,
                    p_author_position = x.Author.Position.Name,
                    p_sign_name = x.Signer.Name,
                    p_sign_department = x.Signer.Department.Name,
                    p_sign_position = x.Signer.Position.Name,

                    p_adressee_display = x.Addressee.Name,
                    p_adressee = (long?)x.Addressee.Id,
                    p_adressee_title = x.Addressee.Name,
                    p_adressee_name = x.Addressee.Name,
                    p_adressee_department = x.Addressee.Department.Name,
                    p_adressee_position = x.Addressee.Position.Name,
                })
                .Filter(storeParams);

            var count = query.Count();

            var queryData = query
                .Order(storeParams)
                .Paging(storeParams)
                .ToList();

            var documentIds = queryData.Select(x => x.id).ToList();

            var relatedDocuments = DocumentService.GetRelatedDocumentsDict(documentIds);

            var data = queryData
                .Extend((row, x) =>
                {
                    row["p_kind_display"] = kindDict.Get((int)x.p_kind);
                    row["p_content"] = x.p_content.GetString();
                    row["p_sign_for"] = x.p_for_sign;
                    row["p_sign_for_display"] = x.p_for_sign_display;
                    row["p_task_display"] = x.p_task_display.GetDisplayValue();

                    var relatedDocs = relatedDocuments.Get(x.id);

                    if (relatedDocs != null)
                    {
                        row["p_documents"] = relatedDocs.Ids;
                        row["p_documents_display"] = relatedDocs.Display;
                    }
                })
                .Highlight(storeParams)
                .ToList();

            return new ObjectListResult
            {
                Data = data,
                Count = count
            };
        }

        protected override IDataResult DocumentList(StoreParams storeParams, IQueryable<CorrespondenceMovement> correspondenceQuery)
        {
            var kindDict = typeof(CorrespondenceType).AsDictionary();
            var moveTypeDict = typeof(DocumentMovementType).AsDictionary();

            var query = correspondenceQuery
                .Select(x => new
                {
                    p_base_doc = x.Document.Id,
                    id = x.Id,
                    p_kind = x.Document.CorrespondenceType,
                    p_number = x.Document.No,
                    p_date = x.Document.Date,
                    p_up_date = x.Document.ReviewBefore,
                    p_deadline = x.Document.ReviewBefore,
                    p_author = (long?)x.Document.Author.Id,
                    p_author_display = x.Document.Author.Name,
                    p_for_sign = (long?)x.Document.Signer.Id,
                    p_for_sign_display = x.Document.Signer.Name,
                    p_theme = x.Document.Subject,
                    p_importance = x.Document.Importance,
                    p_security = x.Document.Secrecy,
                    p_content = x.Document.Content,
                    p_nomenklature = (long?)x.Document.Nomenclature.Id,
                    p_nomenklature_display = x.Document.Nomenclature.CaseTitle,
                    p_nom_index = x.Document.Nomenclature.CaseIndex,
                    p_nom_title = x.Document.Nomenclature.CaseTitle,
                    p_nom_count = x.Document.Nomenclature.StorageUnitsCount,
                    p_nom_deadline = x.Document.Nomenclature.StorageLifeAndArticleNumbers,
                    p_nom_comment = x.Document.Nomenclature.Description,
                    p_nom_year = x.Document.Nomenclature.Year,
                    p_author_name = x.Document.Author.Name,
                    p_author_department = x.Document.Author.Department.Name,
                    p_author_position = x.Document.Author.Position.Name,
                    p_sign_name = x.Document.Signer.Name,
                    p_sign_department = x.Document.Signer.Department.Name,
                    p_sign_position = x.Document.Signer.Position.Name,
                    p_task_display = x.Document.DocumentState,
                    
                    p_adressee_display = x.MovementType == DocumentMovementType.Incoming ? x.Employee.Name : x.Meeting.Name,
                    p_correspondent_display = x.MovementType == DocumentMovementType.Incoming ? x.Meeting.Name : x.Employee.Name,

                    p_adressee_title = x.Employee.Organization.Name,
                    p_adressee_name = x.Employee.Name,
                    p_adressee_department = x.Employee.Department.Name,
                    p_adressee_position = x.Employee.Position.Name,
                    
                    p_correspondent_title = x.Meeting.Organization.Name,
                    p_correspondent_name = x.Meeting.Name,
                    p_correspondent_department = x.Meeting.Department.Name,
                    p_correspondent_position = x.Meeting.Position.Name,

                    x.MovementType
                })
                .Filter(storeParams);

            var count = query.Count();

            var data = query
                .Order(storeParams)
                .Paging(storeParams)
                .Extend((row, x) =>
                {
                    row["p_kind_display"] = kindDict.Get((int)x.p_kind);
                    row["p_content"] = x.p_content.GetString();
                    row["p_sign_for"] = x.p_for_sign;
                    row["p_sign_for_display"] = x.p_for_sign_display;
                    row["p_type_display"] = moveTypeDict.Get((int)x.MovementType);
                    row["p_task_display"] = x.p_task_display.GetDisplayValue();

                    if (x.MovementType != DocumentMovementType.Incoming)
                    {
                        row["p_correspondent_title"] = x.p_adressee_title;
                        row["p_correspondent_name"] = x.p_adressee_name;
                        row["p_correspondent_department"] = x.p_adressee_department;
                        row["p_correspondent_position"] = x.p_adressee_position;

                        row["p_adressee_title"] = x.p_correspondent_title;
                        row["p_adressee_name"] = x.p_correspondent_name;
                        row["p_adressee_department"] = x.p_correspondent_department;
                        row["p_adressee_position"] = x.p_correspondent_position;
                    }
                })
                .Highlight(storeParams)
                .ToList();

            return new ObjectListResult
            {
                Data = data,
                Count = count
            };
        }

        protected override IQueryable<CorrespondenceMovement> PersonalizeQuery(IQueryable<CorrespondenceMovement> query)
        {
            return query.Where(x => x.Employee == UserPrincipal.Employee);
        }

        protected override IQueryable<CorrespondenceMovement> GetOutcomingQuery()
        {
            return base.GetOutcomingQuery().Where(x => x.Document.SignedAt != null);
        }

        [HttpPost, Route("listroutes")]
        public IDataResult ListRoutes()
        {
            var processResultDict = typeof(DocumentProcessingResult).AsDictionary();

            var storeParams = Request.GetStoreParams();

            var docIdStr = storeParams.FilterParams
                .Where(x => x.PropertyName == "p_base_doc")
                .Select(x => x.Value)
                .FirstOrDefault();

            var docId = docIdStr.SafeToString().ToLong();

            if (docId == 0)
            {
                return new ObjectListResult { Count = 0 };
            }

            var res = CorrespondenceRouteEntityService.GetAll()
                .Where(x => x.Document.Id == docId)
                .Select(x => new
                {
                    id = x.Id,
                    p_org_name = x.Employee.Organization.Name,
                    p_name = x.Employee.Name,
                    p_position = x.Employee.Position.Name,
                    p_department = x.Employee.Department.Name,
                    p_date_in = x.DateIn,
                    p_date_out = x.DateOut,
                    p_comment = x.Comment,
                    p_status = x.ProcessingResult,
                })
                .OrderBy(x => x.id)
                //.Order(storeParams) Введение в заблуждение сбиванием порядка в маршруте
                .Extend((row, x) =>
                {
                    row["p_comment"] = x.p_comment.GetString();
                    row["p_status"] = processResultDict.Get((int)x.p_status);
                    row["p_date_in"] = new TimeDate(x.p_date_in);
                    row["p_date_out"] = new TimeDate(x.p_date_out);
                    row["p_time_spent"] = CementExtension.TimeSpent(x.p_date_out, x.p_date_in);
                })
                .ToList();

            return new ObjectListResult
            {
                Count = res.Count,
                Data = res
            };
        }
    }
}