﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using Cement.Core.Entities.Stockroom;
using Cement.Core.Extensions;
using Cement.Core.Services.EntityService;
using Cement.Core.Services.StoreParams;
using Cement.Protocol;
using Cement.WebUI.Extensions;
using Cement.WebUI.Framework;

namespace Cement.WebUI.Controllers.Api
{
    [RoutePrefix("api/stockroomproducthistory")]
    public class StockroomProductHistoryController : CementApiController
    {
        private const string InternalDateFormat = "dd.MM.yyyy";

        public IEntityService<StockroomProductHistory> StockroomProductHistoryEntityService { get; set; }

        [HttpPost, Route("list")]
        public virtual IDataResult List()
        {
            var storeParams = Request.GetStoreParams();

            return ListInternal(storeParams);
        }

        protected virtual IDataResult ListInternal(StoreParams storeParams)
        {
            var dateFrom = storeParams.GetFilterPropertyByName("dateFrom").ToDateTime().Date;
            var dateTo = storeParams.GetFilterPropertyByName("dateTo").ToDateTime().Date.AddDays(1);

            var data = StockroomProductHistoryEntityService.GetAll()
                .Where(x => x.Date >= dateFrom)
                .Where(x => x.Date < dateTo)
                .WarehouseFilter(x => x.StockroomProduct.Warehouse, UserPrincipal)
                .Select(x => new
                {
                    x.StockroomProduct.Id,
                    p_warehouse_display = x.StockroomProduct.Warehouse.Name,
                    p_group_display = x.StockroomProduct.Product.Group.Name,
                    p_subgroup_display = x.StockroomProduct.Product.SubGroup.Name,
                    p_trade_mark = x.StockroomProduct.Product.Brand,
                    p_manufacturer = x.StockroomProduct.Product.Manufacturer.Name,
                    p_name = x.StockroomProduct.Product.Name,
                    p_measure_unit_display = x.StockroomProduct.MeasureUnit.Name,
                    x.InitialAmount,
                    x.InitialPrice,
                    x.InitialTax,
                    x.ChangeAmount,
                    x.ChangePrice,
                    x.ChangeTax,
                    x.Date,
                    x.IsExpense
                })
                .AsEnumerable()
                .GroupBy(x => new
                {
                    x.Id,
                    x.p_warehouse_display,
                    x.p_group_display,
                    x.p_subgroup_display,
                    x.p_trade_mark,
                    x.p_manufacturer,
                    x.p_name,
                    x.p_measure_unit_display
                })
                .Select(x =>
                {
                    var initialData = x.OrderBy(y => y.Date).First();
                    var incoming = x.Sum(y => y.IsExpense ? 0 : y.ChangeAmount);
                    var outcoming = x.Sum(y => y.IsExpense ? y.ChangeAmount : 0);

                    return new
                    {
                        stockroomProductHistoryFilterData = string.Format("{0}###{1:" + InternalDateFormat + "}###{2:" + InternalDateFormat + "}", x.Key.Id, dateFrom, dateTo),
                        x.Key.p_warehouse_display,
                        x.Key.p_group_display,
                        x.Key.p_subgroup_display,
                        x.Key.p_trade_mark,
                        x.Key.p_manufacturer,
                        x.Key.p_name,
                        x.Key.p_measure_unit_display,
                        p_initial_balance = initialData.InitialAmount,
                        p_incoming = incoming,
                        p_rate = outcoming,
                        p_final_balance = initialData.InitialAmount + incoming - outcoming,
                    };
                })
                .AsQueryable()
                .Order(storeParams)
                .ToList();
            
            return new ObjectListResult
            {
                Count = data.Count(),
                Data = data
            };
        }

        [HttpPost, Route("detaillist")]
        public virtual IDataResult DetailList()
        {
            var storeParams = Request.GetStoreParams();

            var stockroomProductHistoryFilterData = storeParams.GetFilterPropertyByName("stockroomProductHistoryFilterData");

            if (string.IsNullOrWhiteSpace(stockroomProductHistoryFilterData))
            {
                return new ObjectListResult();
            }

            var filterParts = stockroomProductHistoryFilterData.Split(new[]{"###"}, StringSplitOptions.None);
            if (filterParts.Length != 3)
            {
                return new ObjectListResult();
            }

            var stockroomProductId = filterParts[0].ToLong();
            var dateFrom = filterParts[1].ToDateTimeFromCementString(InternalDateFormat);
            var dateTo = filterParts[2].ToDateTimeFromCementString(InternalDateFormat);

            if (stockroomProductId == 0)
            {
                return new ObjectListResult();
            }

            var data = StockroomProductHistoryEntityService.GetAll()
                .Where(x => x.Date >= dateFrom)
                .Where(x => x.Date < dateTo)
                .Where(x => x.StockroomProduct.Id == stockroomProductId)
                .OrderBy(x => x.Date)
                .ThenBy(x => x.Id)
                .Select(x => new
                {
                    p_warehouse_display = x.StockroomProduct.Warehouse.Name,
                    p_group_display = x.StockroomProduct.Product.Group.Name,
                    p_subgroup_display = x.StockroomProduct.Product.SubGroup.Name,
                    p_trade_mark = x.StockroomProduct.Product.Brand,
                    p_manufacturer = x.StockroomProduct.Product.Manufacturer.Name,
                    p_name = x.StockroomProduct.Product.Name,
                    p_measure_unit_display = x.StockroomProduct.MeasureUnit.Name,
                    x.InitialAmount,
                    x.ChangeAmount,
                    x.IsExpense
                })
                .Extend((row, x) =>
                {
                    row["p_initial_balance"] = x.InitialAmount;
                    row[x.IsExpense ? "p_rate" : "p_incoming"] = x.ChangeAmount;
                    row["p_final_balance"] = x.InitialAmount + (x.IsExpense ? -1 : 1) * x.ChangeAmount;
                })
                .ToList();

            return new ObjectListResult
            {
                Count = data.Count(),
                Data = data
            };
        }
    }
}