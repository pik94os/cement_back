﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using Cement.Core.Entities;
using Cement.Core.Enums;
using Cement.Core.Extensions;
using Cement.Core.Services.StoreParams;
using Cement.Protocol;
using Cement.WebUI.Extensions;
using DataAccess.Repository;
using Newtonsoft.Json;

namespace Cement.WebUI.Controllers.Api
{
    [RoutePrefix("api/railwaystation")]
    public class RailwayStationController : DataController<PaymentAccount, PaymentAccountProxy>
    {
        [HttpPost, Route("dict")]
        public IDataResult Dropdownlist(BankDictRequest request)
        {
            if (string.IsNullOrWhiteSpace(request.Query))
            {
                return null;
            }

            if (request.Query.Length < 4)
            {
                return null;
            }
            
            var bankList =  Container.Resolve<IRepository<Bank>>().GetAll()
                .Where(x => x.Bik.StartsWith(request.Query))
                .Select(x => new 
                {
                    id = x.Id, 
                    p_name = x.FullName,
                    p_bik = x.Bik,
                    p_corr = x.Ks,
                })
                .OrderBy(x => x.p_bik)
                .ThenBy(x => x.p_name)
                .ToList();

            return new ObjectListResult
            {
                Data = bankList,
                Count = bankList.Count
            };
        }
    }
}