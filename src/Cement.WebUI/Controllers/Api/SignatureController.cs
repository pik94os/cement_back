﻿using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Web.Http;
using Cement.Core.Entities;
using Cement.Core.Extensions;
using Cement.Core.Services.EntityService;
using Cement.Core.Services.StoreParams;
using Cement.Protocol;

namespace Cement.WebUI.Controllers.Api
{
    [RoutePrefix("api/signature")]
    public class SignatureController : DataController<Signature, SignatureProxy>
    {
        public IEntityService<EmployeeDrivingCategory> EmployeeDrivingCategoryEntityService { get; set; }

        protected override IDataResult ListInternal(StoreParams storeParams)
        {
            var organizationId = storeParams.GetFilterPropertyByName("p_client_current").ToLong();

            var query = EntityService.GetAll()
                .OrganizationExclusiveFilter(UserPrincipal, organizationId)
                .Select(x => new
                {
                    id = x.Id,
                    p_employee = (long?)x.Recipient.Id,
                    p_employee_display = x.Recipient.Name,
                    p_name = x.Recipient.Name,
                    p_fio = x.Recipient.Name,
                    p_position = x.Recipient.Position.Name,
                    p_sign_reason = (long?) x.Reason.Id,
                    p_sign_reason_display = x.Reason.Name,

                    p_sign_for_who = (long?)x.Truster.Id,
                    p_sign_for_who_display = x.Truster.Name,
                    p_sign_for_who_position = x.Truster.Position.Name,
                    p_sign_date_from = x.DateStart,
                    p_sign_date_end = x.DateEnd,

                    p_surname = x.Recipient.Surname,
                    p_middlename = x.Recipient.Patronymic,
                    p_position_display = x.Recipient.Position.Name,
                    p_department = x.Recipient.Department.Name,
                    p_inn = x.Recipient.Inn,

                    p_passport_serie = x.Recipient.PassportSerie,
                    p_passport_number = x.Recipient.PassportNo,
                    p_passport_get = x.Recipient.PassportGivenPlace,
                    p_passport_date = x.Recipient.PassportDate,
                    p_passport_birth = x.Recipient.BirthDate,
                    p_passport_place = x.Recipient.BirthPlace,

                    p_drive_serie = x.Recipient.DriveLicSerie,
                    p_drive_number = x.Recipient.DriveLicNo,
                    p_drive_get = x.Recipient.DriveLicGivenPlace,
                    p_drive_date = x.Recipient.DriveLicDate,
                    p_drive_date_end = x.Recipient.DriveLicValidDate,
                    p_drive_age = x.Recipient.DriveExperience.Name,
                    
                    p_address_index = x.Recipient.Index,
                    p_address_country = x.Recipient.Country,
                    p_address_region = x.Recipient.Region,
                    p_address_district = x.Recipient.Area,
                    p_address_city = x.Recipient.Locality,
                    p_address_street = x.Recipient.Street,
                    p_address_house = x.Recipient.House,
                    p_address_office = x.Recipient.Apartment,
                    p_flat = x.Recipient.Apartment,

                    p_connect_phone1 = x.Recipient.Phone1,
                    p_connect_phone2 = x.Recipient.Phone2,
                    p_connect_phone3 = x.Recipient.Phone3,
                    p_connect_fax = x.Recipient.Fax,
                    p_connect_mobile1 = x.Recipient.MobilePhone1,
                    p_connect_mobile2 = x.Recipient.MobilePhone2,
                    p_connect_mobile3 = x.Recipient.MobilePhone3,
                    p_connect_email1 = x.Recipient.Email1,
                    p_connect_email2 = x.Recipient.Email2,
                    p_connect_email3 = x.Recipient.Email3
                })
                .Filter(storeParams);

            var count = query.Count();

            var data = query
                .Order(storeParams)
                .Paging(storeParams)
                .ToList();

            var employeeIds = data.Select(x => x.p_employee).Distinct().ToList();

            var driveCatsDict = new Dictionary<long, string>();

            if (employeeIds.Any())
            {
                driveCatsDict = EmployeeDrivingCategoryEntityService.GetAll()
                    .Where(x => employeeIds.Contains(x.Employee.Id))
                    .Select(x => new
                    {
                        x.Employee.Id,
                        x.Category.Name
                    })
                    .AsEnumerable()
                    .GroupBy(x => x.Id)
                    .ToDictionary(x => x.Key, x => string.Join(", ", x.Select(y => y.Name)));
            }

            var list = data.Extend((row, x) =>
                {
                    row["p_drive_category"] = x.p_employee.HasValue ? driveCatsDict.Get(x.p_employee.Value) : null;
                })
                .Highlight(storeParams)
                .ToList();

            return new ObjectListResult
            {
                Data = list,
                Count = count
            };
        }
    }
}