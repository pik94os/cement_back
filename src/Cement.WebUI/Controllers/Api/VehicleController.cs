﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using Cement.Core.Entities;
using Cement.Core.Enums;
using Cement.Core.Extensions;
using Cement.Core.Services;
using Cement.Core.Services.StoreParams;
using Cement.Protocol;

namespace Cement.WebUI.Controllers.Api
{
    [RoutePrefix("api/vehicle")]
    public class VehicleController : DataController<Vehicle, VehicleProxy>
    {
        public IVehicleService VehicleService { get; set; }

        [HttpPost, Route("dictionarylist")]
        public object Dropdownlist()
        {
            var query = VehicleService.GetVehicleQuery(UserPrincipal.Organization);

            return DataList(query);
        }

        [HttpPost, Route("listall")]
        public object ListAll()
        {
            var query = VehicleService.GetVehicleQuery(UserPrincipal.Organization);

            return DataList(query);
        }

        [HttpPost, Route("listwithouttrailers")]
        public object ListWithoutTrailers()
        {
            var query = VehicleService.GetVehicleQuery(UserPrincipal.Organization)
                .Where(x => x.Category.Type != VehicleCategoryType.Trailer);

            return DataList(query);
        }

        [HttpPost, Route("listtrailers")]
        public object ListTrailers()
        {
            var query = VehicleService.GetVehicleQuery(UserPrincipal.Organization)
                .Where(x => x.Category.Type == VehicleCategoryType.Trailer);
            
            return DataList(query);
        }

        protected object DataList(IQueryable<Vehicle> vehicleQuery)
        {
            var data = vehicleQuery
                .Select(x => new
                {
                    leaf = true,
                    id = x.Id,
                    p_name = x.Name,
                    p_category_display = x.Category.Name,
                    p_sub_category_display = x.SubCategory.Name,
                    p_mark_model_spec_display = x.ModelFullName,
                    p_number_display = x.NumberText,
                    p_class_display = x.Class.Name,
                    p_size_display = x.SizeText,
                    p_go_display = x.GoText,
                    p_char_ops = (VehicleType?)x.Type,
                    p_char_type_display = x.BodyType.Name,
                    p_load_back = x.IsRearLoading,
                    p_load_top = x.IsOverLoading,
                    p_load_left = x.IsSideLoading,
                    p_additional_options_display = x.Option.Name,
                    p_additional_chars_display = x.ExtraCharacteristic,
                    p_department_display = x.Division.Name,
                    p_column_display = x.Column.Name,
                    p_garage_number = x.GarageNumber,
                    show_char = (bool?)x.Category.HasСharacteristics ?? false
                })
                .OrderBy(x => x.p_name)
                .ToList();

            return data;
        }

        protected override IDataResult ListInternal(StoreParams storeParams)
        {
            var ownVehicleQuery = VehicleService.GetOwnVehicleQuery(UserPrincipal.Organization);

            var query = ownVehicleQuery
                .Select(x => new
                {
                    id = x.Id,
                    p_name = x.Name,
                    p_category = (long?) x.Category.Id,
                    p_category_display = x.Category.Name,
                    p_sub_category = (long?) x.SubCategory.Id,
                    p_sub_category_display = x.SubCategory.Name,
                    p_mark_model_spec = (long?) x.Modification.Id,
                    p_mark_model_spec_display = x.ModelFullName,
                    p_number_display = x.NumberText,
                    p_class_display = x.Class.Name,
                    p_size_display = x.SizeText,
                    p_go_display = x.GoText,
                    p_char_ops = (VehicleType?) x.Type,
                    p_char_type_display = x.BodyType.Name,
                    p_load_back = x.IsRearLoading,
                    p_load_top = x.IsOverLoading,
                    p_load_left = x.IsSideLoading,
                    p_additional_options_display = x.Option.Name,
                    p_additional_chars_display = x.ExtraCharacteristic,
                    p_department_display = x.Division.Name,
                    p_column_display = x.Column.Name,
                    p_garage_number = x.GarageNumber
                })
                .Filter(storeParams);
            
            var count = query.Count();

            var data = query
                .Order(storeParams)
                .Paging(storeParams)
                .Highlight(storeParams)
                .ToList();

            return new ObjectListResult
            {
                Data = data,
                Count = count
            };
        }

        public override object Details(long id)
        {
            var data1 = EntityService.GetAll().Where(x => x.Id == id)
                .Select(x => new VehicleDetailProxy
                {
                    Id = x.Id,
                    CategoryId = x.Category.Id,
                    SubCategoryId = x.SubCategory.Id,
                    Name = x.ModelFullName, 
                    ModelId = x.Model.Id,
                    ModificationId = x.Modification.Id,
                    BrandName = x.Model.Brand.Name,
                    ModelName = x.Model.Name,
                    ModificationName = x.Modification.Name,
                    ReleaseYear = x.ReleaseYear,
                    NumberText = x.NumberText,
                    Number = x.Number,
                    NumCountryCodeId = x.NumCountryCode.Id,
                    NumRegionCodeId = x.NumRegionCode.Id,
                    PhotoLink = x.Photo.Id,
                    
                    Vin = x.Vin,
                    EngineSerialNum = x.EngineSerialNum,
                    СhassisSerialNum = x.СhassisSerialNum,
                    EcoClass = x.EcoClass,
                    Body = x.Body,
                    WheelBaseId = x.WheelBase.Id,
                    WheelFormulaName = x.WheelBase.Name,
                    DriverCategoryId = x.DriverCategory.Id,
                    DriverCategoryName = x.DriverCategory.Name,
                    Color = x.Color,
                    Power = x.Power,
                    EngineVolume = x.EngineVolume,
                    MaxMass = x.MaxMass,
                    VehicleMass = x.VehicleMass,
                    SizeText = x.SizeText,

                    PassportDate = x.PassportDate,
                    PassportManufacturer = x.PassportManufacturer,
                    PassportSerie = x.PassportSerie,
                    PassportNumber = x.PassportNumber,
                    PasspostScan1Id = x.PasspostScan1.Id,
                    PasspostScan2Id = x.PasspostScan2.Id,

                    DocumentSerie = x.DocumentSerie,
                    DocumentDate = x.DocumentDate,
                    DocumentNumber = x.DocumentNumber,
                    DocumentOwner = x.DocumentOwner,
                    DocumentScan1Id = x.DocumentScan1.Id,
                    DocumentScan2Id = x.DocumentScan2.Id,

                    Length = x.Length,
                    Width = x.Width,
                    Height = x.Height,
                    Capacity = x.Capacity,
                    Volume = x.Volume,
                    TypeNullable = x.Type,
                    BodyTypeId = x.BodyType.Id,
                    BodyTypeName = x.BodyType.Name, 
                    ClassId = x.Class.Id,
                    ClassName = x.Class.Name,
                    IsRearLoading = x.IsRearLoading,
                    IsOverLoading = x.IsOverLoading,
                    IsSideLoading = x.IsSideLoading,
                    OptionId = x.Option.Id,
                    OptionName = x.Option.Name,
                    ExtraCharacteristic = x.ExtraCharacteristic,

                    DepartmentName = x.Division.Name,
                    ColumnName = x.Column.Name,
                    DepartmentId = (long?)x.Division.Id,
                    ColumnId = (long?)x.Column.Id,
                    GarageNumber = x.GarageNumber,

                    HasСharacteristics = x.Category.HasСharacteristics
                })
                .FirstOrDefault();

            return data1;


            var data = EntityService.GetAll().Where(x => x.Id == id)
                .Select(x => new 
                {
                    id = x.Id,
                    p_name = x.Name,
                    //p_category = (long?)x.Category.Id,
                    //p_category_display = x.Category.Name,
                    //p_sub_category = (long?)x.SubCategory.Id,
                    //p_sub_category_display = x.SubCategory.Name,
                    //p_mark_model_spec = (long?)x.Modification.Id,
                    //p_mark_model_spec_display = x.ModelFullName,

                    //p_mark = 1,
                    p_mark_display = x.Model.Brand.Name,
                    //p_model = 2,
                    p_model_display = x.Model.Name,
                    //p_modification = string.Empty,
                    p_modification_display = x.Modification.Name,

                    p_number = x.Number,
                    p_number_country_code = x.NumCountryCode.Name,
                    p_number_region_code = x.NumRegionCode.Name,
                    p_number_display = x.NumberText,

                    p_class_display = x.Class.Name,
                    p_size_display = x.SizeText,
                    p_go_display = x.GoText,
                    p_char_ops = (VehicleType?)x.Type,
                    p_char_type_display = x.BodyType.Name,

                    p_load_back = x.IsRearLoading,
                    p_load_top = x.IsOverLoading,
                    p_load_left = x.IsSideLoading,

                    p_char_load_back = x.IsRearLoading,
                    //p_char_load_ekmt= string.Empty,
                    p_char_load_left = x.IsOverLoading,
                    //p_char_load_tir= string.Empty,
                    p_char_load_top = x.IsSideLoading,

                    p_additional_options_display = x.Option.Name,
                    p_additional_chars_display = x.ExtraCharacteristic,
                    p_department_display = x.Division,
                    p_column_display = x.Column,
                    p_garage_number = x.GarageNumber,

                    p_reg_color = x.Color,
                    p_reg_body = string.Empty,
                    p_reg_category = string.Empty,
                    p_reg_chasis = string.Empty,
                    p_reg_eco_class = string.Empty,
                    p_reg_engine = string.Empty,
                    p_reg_engine_volume = string.Empty,
                    p_reg_max_mass = string.Empty,
                    p_reg_power = string.Empty,
                    p_reg_truck_mass = string.Empty,
                    p_reg_vin = string.Empty,
                    p_reg_wheel_base = string.Empty,

                    p_year = x.ReleaseYear,


                    p_add_additional = x.Option.Name,
                    p_add_column = x.Column,
                    p_add_garage_number = x.GarageNumber,
                    p_add_subdivision = x.Division,
                    p_char_additional = x.ExtraCharacteristic,
                    p_char_capacity = x.Capacity,
                    p_char_height = x.Height,
                    p_char_length = x.Length,
                    p_char_type = "11",
                    p_char_volume = 12,
                    p_char_width = 13,
                    p_document_date = "2004.12.12",
                    p_document_number = string.Empty,
                    p_document_owner = string.Empty,
                    p_document_page1 = string.Empty,
                    p_document_page2 = string.Empty,
                    p_document_serie = string.Empty,
                    p_img = string.Empty,
    
                    p_pts_date = string.Empty,
                    p_pts_manufacturer = string.Empty,
                    p_pts_number = string.Empty,
                    p_pts_page1 = string.Empty,
                    p_pts_page2 = string.Empty,
                    p_pts_serie = string.Empty,
                    
                    p_truck_display = "Mercedes Модель 1-2",
                    show_char = true

                })
                .FirstOrDefault();

            return data;
        }

        [HttpPost, Route("details/{id}")]
        public virtual object LoadItem(long id)
        {
            return Details(id);
        }
    }
}