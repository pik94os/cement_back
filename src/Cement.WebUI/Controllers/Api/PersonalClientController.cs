﻿using System;
using System.Linq;
using System.Web.Http;
using Cement.Core.Entities;
using Cement.Core.Extensions;
using Cement.Core.Services.StoreParams;
using Cement.Protocol;
using Cement.WebUI.Extensions;

namespace Cement.WebUI.Controllers.Api
{
    [RoutePrefix("api/personalclient")]
    public class PersonalClientController : DataController<PersonalClient, PersonalClientProxy>
    {
        protected override IDataResult ListInternal(StoreParams storeParams)
        {
            var query = EntityService.GetAll()
                .Where(x => x.ArchivedAt == null)
                .Where(x => x.Employee == UserPrincipal.Employee);

            return ListContractData(storeParams, query);
        }
        
        [HttpPost, Route("listcorporate")]
        public IDataResult ListCorporate()
        {
            var storeParams = Request.GetStoreParams();

            var query = EntityService.GetAll()
                .Where(x => x.ArchivedAt == null)
                .Where(x => x.IsCorporate)
                .OrganizationFilter(UserPrincipal);

            return ListContractData(storeParams, query);
        }

        [HttpPost, Route("listarchived")]
        public IDataResult ListArchived()
        {
            var storeParams = Request.GetStoreParams();

            var query = EntityService.GetAll()
                .Where(x => x.ArchivedAt != null)
                .Where(x => x.Employee == UserPrincipal.Employee);

            return ListContractData(storeParams, query);
        }

        protected IDataResult ListContractData(StoreParams storeParams, IQueryable<PersonalClient> personalClientQuery)
        {
            var query = personalClientQuery.Select(x => new
                {
                    id = x.Id,
                    p_client = x.Client.Id,
                    
                    p_group = (long?)x.Group.Id,
                    p_group_display = x.Group.Name,
                    p_sub_group = (long?)x.Subgroup.Id,
                    p_sub_group_display = x.Subgroup.Name,

                    p_firm_brand = x.Client.Brand,
                    p_firm_form = x.Client.Ownership.ShortName,
                    p_firm_full_name = x.Client.FullName,
                    //p_firm_img = "",
                    p_firm_name = x.Client.Name,
                    p_cont_email = x.Client.Email,
                    p_cont_fax = x.Client.Fax,
                    p_cont_phone = x.Client.Phone,
                    p_cont_web = x.Client.Web,
                    p_fact_building = x.Client.FactAddressBuilding,
                    p_fact_city = x.Client.FactAddressLocality,
                    p_fact_country = x.Client.FactAddressCountry,
                    p_fact_district = x.Client.FactAddressArea,
                    p_fact_house = x.Client.FactAddressHouse,
                    p_fact_house_part = x.Client.FactAddressHousing,
                    p_fact_index = x.Client.FactAddressIndex,
                    p_fact_office = x.Client.FactAddressOffice,
                    p_fact_region = x.Client.FactAddressRegion,
                    p_fact_street = x.Client.FactAddressStreet,
                    p_reg_inn = x.Client.Inn,
                    //p_reg_inn_img = "",
                    p_reg_kpp = x.Client.Kpp,
                    p_reg_ogrn = x.Client.Ogrn,
                    //p_reg_ogrn_img = "",
                    p_reg_okato = x.Client.Okato,
                    p_reg_okonh = x.Client.Okonh,
                    p_reg_okpo = x.Client.Okpo,
                    p_reg_okved = x.Client.Okved,
                    p_address_display = x.Client.Address,
                    p_ur_building = x.Client.JurAddressBuilding,
                    p_ur_city = x.Client.JurAddressLocality,
                    p_ur_country = x.Client.JurAddressCountry,
                    p_ur_district = x.Client.JurAddressArea,
                    p_ur_house = x.Client.JurAddressHouse,
                    p_ur_house_part = x.Client.JurAddressHousing,
                    p_ur_index = x.Client.JurAddressIndex,
                    p_ur_office = x.Client.JurAddressOffice,
                    p_ur_region = x.Client.JurAddressRegion,
                    p_ur_street = x.Client.JurAddressStreet,

                    p_curator_display = x.Employee.Name,
                    p_curator = (long?)x.Employee.Id,
                    p_curator_name = x.Employee.FirstName,
                    p_curator_surname = x.Employee.Surname,
                    p_curator_lastname = x.Employee.Patronymic,
                    p_curator_position = x.Employee.Position.Name,
                    p_curator_for = string.Empty,

                    p_first_contact = x.DateFirstContract,
                    p_last_contact = x.DateLastContract,

                    p_status = (long?)x.State.Id,
                    p_status_display = x.State.Name,
                    p_from = x.From,
                    p_comment = x.Comment
                })
                .Filter(storeParams);

            var count = query.Count();

            var data = query
                .Order(storeParams)
                .Paging(storeParams)
                .Highlight(storeParams)
                .ToList();

            return new ObjectListResult
            {
                Data = data,
                Count = count
            };
        }

        [HttpPost, Route("archive/{id}")]
        public JsResult Archive(long id)
        {
            return ManageArchive(id, true);
        }

        [HttpPost, Route("unarchive/{id}")]
        public JsResult Unarchive(long id)
        {
            return ManageArchive(id, false);
        }

        /// <summary>
        /// Управление архивированием
        /// </summary>
        protected JsResult ManageArchive(long id, bool archivate, string successText = "Успешно")
        {
            try
            {
                var personalClient = EntityService.Get(id);

                if (personalClient != null)
                {
                    personalClient.ArchivedAt = archivate ? DateTime.Now : (DateTime?)null;
                    EntityService.Update(personalClient);
                }

                return JsResult.Successful(successText);
            }
            catch (Exception exc)
            {
                return new JsResult
                {
                    Msg = exc.Message,
                    Id = id,
                    Success = false
                };
            }
        }
    }
}