﻿using System;
using System.Linq;
using System.Web.Http;
using Cement.Core.Entities;
using Cement.Core.Enums;
using Cement.Core.Extensions;
using Cement.Core.Services;
using Cement.Core.Services.EntityService;
using Cement.Core.Services.StoreParams;
using Cement.Protocol;
using Cement.Protocol.JsonConverters;
using Cement.WebUI.Extensions;

namespace Cement.WebUI.Controllers.Api
{
    [RoutePrefix("api/message")]
    public class MessageController : MovingDocumentDataController<Message, MessageProxy, MessageMovement>
    {
        #region Injections
        
        public IEntityService<MessageRecipient> MessageRecipientEntityService { get; set; }

        //public IEntityService<MessageMovement> MessageMovementEntityService { get; set; }

        //public IEntityService<IncomingMessage> IncomingMessageEntityService { get; set; }

        //public IEntityService<OutcomingMessage> OutcomingMessageEntityService { get; set; }

        public IEntityService<MessageRoute> MessageRouteEntityService { get; set; }

        public IMessageService MessageService { get; set; }

        #endregion Injections

        protected override IDataResult ListInternal(StoreParams storeParams)
        {
            var kindDict = typeof(MessageType).AsDictionary();

            var query = EntityService.GetAll()
                .Where(x => x.DocumentState == DocumentState.Draft)
                .Where(x => x.Creator == UserPrincipal.Employee)
                .Select(x => new
                {
                    id = x.Id,
                    p_kind = x.MessageType,
                    p_date = x.Date,
                    p_author = (long?)x.Author.Id,
                    p_author_display = x.Author.Name,
                    p_theme = x.Subject,
                    p_importance = x.Importance,
                    p_security = x.Secrecy,
                    p_content = x.Content,
                    p_author_name = x.Author.Name,
                    p_author_department = x.Author.Department.Name,
                    p_author_position = x.Author.Position.Name,
                    //p_sign_name = x.Signer.Name,
                    //p_sign_department = x.Signer.Department.Name,
                    //p_sign_position = x.Signer.Position.Name,
                    p_task_display = x.DocumentState,
                    p_repeat_display = x.RepeatDisplay,
                    p_repeat = x.RepeatDetails
                })
                .Filter(storeParams);

            var count = query.Count();

            var queryData = query
                .Order(storeParams)
                .Paging(storeParams)
                .ToList();

            var messageIds = queryData.Select(x => x.id).ToList();

            var recipientsDict = MessageRecipientEntityService.GetAll()
                .Where(x => messageIds.Contains(x.Message.Id))
                .Select(x => new
                {
                    x.Recipient.Name,
                    x.Recipient.Id,
                    MessageId = x.Message.Id
                })
                .AsEnumerable()
                .GroupBy(x => x.MessageId)
                .ToDictionary(x => x.Key, x =>
                {
                    var recipients = new
                    {
                        Ids = x.Select(y => y.Id).ToList(),
                        Display = string.Join(", ", x.Select(y => y.Name))
                    };

                    return recipients;
                });

            var data = queryData
                .Extend((row, x) =>
                {
                    row["p_kind_display"] = kindDict.Get((int)x.p_kind);
                    row["p_content"] = x.p_content.GetString();
                    row["p_hide_sign"] = true; //
                    //row["p_sign_for"] = x.p_for_sign;
                    //row["p_sign_for_display"] = x.p_for_sign_display;
                    row["p_task_display"] = x.p_task_display.GetDisplayValue();

                    var recipients = recipientsDict.Get(x.id);

                    if (recipients != null)
                    {
                        row["p_employees"] = recipients.Ids;
                        row["p_employees_display"] = recipients.Display;
                    }
                })
                .ToList();

            return new ObjectListResult
            {
                Data = data,
                Count = count
            };
        }


        protected override IQueryable<MessageMovement> PersonalizeQuery(IQueryable<MessageMovement> query)
        {
            return query.Where(x => x.Employee == UserPrincipal.Employee);
        }
        
        protected override IDataResult DocumentList(StoreParams storeParams, IQueryable<MessageMovement> messageMovementQuery)
        {
            var kindDict = typeof(MessageType).AsDictionary();
            var moveTypeDict = typeof(DocumentMovementType).AsDictionary();

            var query = messageMovementQuery
                .Select(x => new
                {
                    p_base_doc = x.Document.Id,
                    id = x.Id,
                    p_kind = x.Document.MessageType,
                    processingResult = (DocumentProcessingResult?)x.Route.ProcessingResult,
                    //p_number = x.Document.No,
                    p_date = x.Document.Date,
                    p_author = (long?)x.Document.Author.Id,
                    p_author_display = x.Document.Author.Name,
                    p_theme = x.Document.Subject,
                    p_importance = x.Document.Importance,
                    p_security = x.Document.Secrecy,
                    p_content = x.Document.Content,
                    p_author_name = x.Document.Author.Name,
                    p_author_department = x.Document.Author.Department.Name,
                    p_author_position = x.Document.Author.Position.Name,
                    x.MovementType
                })
                .Filter(storeParams);

            var count = query.Count();

            var data = query
                .Order(storeParams)
                .Paging(storeParams)
                .Extend((row, x) =>
                {
                    row["p_hide_sign"] = true; //!(x.processingResult == DocumentProcessingResult.None); // R# врет, условие не менять!
                    row["p_kind_display"] = kindDict.Get((int)x.p_kind);
                    row["p_content"] = x.p_content.GetString();
                    row["p_type_display"] = moveTypeDict.Get((int)x.MovementType);
                })
                .Highlight(storeParams)
                .ToList();

            return new ObjectListResult
            {
                Data = data,
                Count = count
            };
        }

        //[HttpPost, Route("agreementForNew")]
        //public JsResult AgreementForNew()
        //{
        //    var agreement = Request.ToType<AgreementProxy>();

        //    if (agreement == null || agreement.ItemId == 0)
        //    {
        //        return new JsResult { Success = false, Msg = "Not allowed" };
        //    }

        //    var employeeCommentTuple = agreement.GetEmployeeCommentTuple();

        //    MessageService.Process(agreement.ItemId, agreement.Send, employeeCommentTuple.Item1, employeeCommentTuple.Item2);

        //    return JsResult.Successful();
        //}

        //[HttpPost, Route("agreementForIncoming")]
        //public JsResult AgreementForIncoming()
        //{
        //    var agreement = Request.ToType<AgreementProxy>();

        //    if (agreement == null || agreement.ItemId == 0)
        //    {
        //        return new JsResult { Success = false, Msg = "Not allowed" };
        //    }

        //    var employeeCommentTuple = agreement.GetEmployeeCommentTuple();

        //    MessageService.ProcessFromMovement(agreement.ItemId, agreement.Send, employeeCommentTuple.Item1, employeeCommentTuple.Item2);

        //    return JsResult.Successful();
        //}

        [HttpPost, Route("listroutes")]
        public IDataResult ListRoutes()
        {
            var processResultDict = typeof(DocumentProcessingResult).AsDictionary();

            var storeParams = Request.GetStoreParams();

            var docIdStr = storeParams.FilterParams
                .Where(x => x.PropertyName == "p_base_doc")
                .Select(x => x.Value)
                .FirstOrDefault();

            var docId = docIdStr.SafeToString().ToLong();

            if (docId == 0)
            {
                return new ObjectListResult { Count = 0 };
            }
            
            var routes = MessageRouteEntityService.GetAll()
                .Where(x => x.Document.Id == docId)
                .Select(x => new
                {
                    id = x.Id,
                    p_name = x.Employee.Name,
                    p_position = x.Employee.Position.Name,
                    p_department = x.Employee.Department.Name,
                    p_date_in = x.DateIn,
                    p_date_out = x.DateOut,
                    p_comment = x.Comment,
                    x.ProcessingResult,
                })
                //.Order(storeParams) Введение в заблуждение сбиванием порядка в маршруте
                .Extend((row, x) =>
                {
                    row["p_comment"] = x.p_comment.GetString();
                    row["p_status"] = processResultDict.Get((int)x.ProcessingResult);
                    row["p_date_in"] = new TimeDate(x.p_date_in);
                    row["p_date_out"] = new TimeDate(x.p_date_out);
                })
                .ToList();

            return new ObjectListResult
            {
                Count = routes.Count,
                Data = routes
            };
        }
    }
}