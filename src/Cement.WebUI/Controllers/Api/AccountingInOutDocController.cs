﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Net.Http;
using System.Web.Http;
using Cement.Core.Entities;
using Cement.Core.Entities.Stockroom;
using Cement.Core.Enums;
using Cement.Core.Exceptions;
using Cement.Core.Extensions;
using Cement.Core.Services;
using Cement.Core.Services.EntityService;
using Cement.Core.Services.StoreParams;
using Cement.Protocol;
using Cement.WebUI.Extensions;

namespace Cement.WebUI.Controllers.Api
{
    [RoutePrefix("api/accountinginoutdoc")]
    public class AccountingInOutDocController : RoutedEntityDataController<AccountingInOutDocBase, AccountingInOutDocProxy, AccountingInOutDocRoute>
    {
        const string Suppliers = "suppliers";
        const string Getters = "getters";
        const string Payers = "payers";
        const string Sellers = "sellers";
        const string Senders = "senders";

        public IEntityService<StockroomExpenseOrderRoute> StockroomExpenseOrderRouteEntityService { get; set; }
        public IEntityService<StockroomExpenseOrderSale> StockroomExpenseOrderSaleEntityService { get; set; }
        public IEntityService<StockroomExpenseOrderMovement> StockroomExpenseOrderMovementEntityService { get; set; }
        public IEntityService<AccountingInOutDocInvoice> AccountingInOutDocInvoiceEntityService { get; set; }
        public IOrganizationService OrganizationService { get; set; }

        protected override Expression<Func<AccountingInOutDocRoute, bool>> DocIdFilterExpression(long id)
        {
            return x => x.Document.Id == id;
        }

        protected override IQueryable<AccountingInOutDocRoute> PersonalizeQuery(IQueryable<AccountingInOutDocRoute> query)
        {
            return query.Where(x => x.Employee == UserPrincipal.Employee);
        }

        protected override void DeleteInternal(long routeId)
        {
            // Бухгалтерские документы в общем не удаляются, но можно удалить некорректно созданный, т.е. тот, который в новых
            
            var docRoute = DocumentRouteEntityService.Get(routeId);
            EntityService.Delete(docRoute.Document.Id);
        }


        #region Get
        protected void ProcessInvoiceDetails(AccountingInOutDocInvoice accountingInOutDocInvoice, Dictionary<string, object> dict)
        {
            var query = Container.Resolve<IEntityService<AccountingInOutDocInvoice>>().GetAll()
                .Where(x => x == accountingInOutDocInvoice)
                .Select(x => x.AccountingInOutDocWayBill as AccountingInOutDocWayBillSale);

            dict["p_dst"] = SignedAccountingInOutDocWayBillQueryExecutor(query);
            dict["p_src"] = SignedAccountingInOutDocWayBillList();
        }

        protected void ProcessWayBillSaleDetails(AccountingInOutDocWayBillSale accountingInOutDocWayBillSale, Dictionary<string, object> dict)
        {
            dict["p_kind"] = accountingInOutDocWayBillSale.AccountingInOutDocWayBillSaleKind;
            dict["p_shipping_kind"] = accountingInOutDocWayBillSale.AccountingInOutDocWayBillSaleShippingKind;

            var query = StockroomExpenseOrderSaleEntityService.GetAll()
                .Where(x => x.AccountingInOutDocWayBill == accountingInOutDocWayBillSale);
            
            dict["p_dst"] = StockroomExpenseOrderSaleQueryExecutor(query);
            dict["p_src"] = StockroomExpenseOrderSaleList(RequestType.Product);
        }

        protected void ProcessWayBillDetails(AccountingInOutDocWayBill accountingInOutDocWayBill, Dictionary<string, object> dict)
        {
            var query = StockroomExpenseOrderSaleEntityService.GetAll()
                .Where(x => x.AccountingInOutDocWayBill == accountingInOutDocWayBill);

            dict["p_dst"] = StockroomExpenseOrderSaleQueryExecutor(query);
            dict["p_src"] = StockroomExpenseOrderSaleList(RequestType.Service);
        }

        protected void ProcessDetails(AccountingInOutDocBase accountingDoc, Dictionary<string, object> dict)
        {
            if (accountingDoc is AccountingInOutDocWayBillSale)
            {
                ProcessWayBillSaleDetails(accountingDoc as AccountingInOutDocWayBillSale, dict);
            }
            else if (accountingDoc is AccountingInOutDocWayBill)
            {
                ProcessWayBillDetails(accountingDoc as AccountingInOutDocWayBill, dict);
            }
            else if (accountingDoc is AccountingInOutDocInvoice)
            {
                ProcessInvoiceDetails(accountingDoc as AccountingInOutDocInvoice, dict);
            }
        }

        public override IDataResult Get(long id)
        {
            var data = EntityService.GetAll().First(x => x.Id == id);

            var res = new
            {
                id = data.Id,
                p_number = data.No,
                p_date = data.Date,
                p_type = data.AccountingInOutDocType
            };

            var result = res.Extend((row, x) => ProcessDetails(data, row));

            return new ObjectResult { Data = result };
        }
        #endregion Get

        protected override IDataResult DocumentList(StoreParams storeParams, IQueryable<AccountingInOutDocRoute> routeQuery)
        {
            var query = routeQuery
                .Select(x => new
                {
                    p_base_doc = x.Document.Id,
                    id = x.Id,
                    p_kind = x.Document.AccountingInOutDocType,
                    p_number = x.Document.No,
                    p_date = x.Document.Date,
                    p_type = x.MovementType,
                    p_state = x.Document.DocumentState,
                    p_sum = x.Document.Sum,
                    p_organization_from = x.Document.OrganizationFrom.Name,
                    p_organization_to = x.Document.OrganizationTo.Name,
                    p_organization_from_id = x.Document.OrganizationFrom.Id,
                })
                .Filter(storeParams);

            var data = query
                .Order(storeParams)
                .Paging(storeParams)
                .Extend((row, x) =>
                {
                    row["p_kind_display"] = x.p_kind.GetDisplayValue();
                    row["p_type"] = x.p_type.GetDisplayValue();
                    row["p_form_kind"] = (int)x.p_kind;
                    row["p_state"] = x.p_state.GetDisplayValue();
                    row["p_contractor_display"] = x.p_organization_from_id == UserPrincipal.Organization.Return(y => y.Id) ? x.p_organization_to : x.p_organization_from;
                })
                .Highlight(storeParams)
                .ToList();

            return new ObjectListResult
            {
                Data = data,
                Count = query.Count()
            };
        }

        protected override AccountingInOutDocBase MapProxyToEntity(AccountingInOutDocProxy incomeProxy)
        {
            switch (incomeProxy.AccountingInOutDocType)
            {
                case AccountingInOutDocType.Invoice: return AutoMapper.Mapper.DynamicMap<AccountingInOutDocInvoice>(incomeProxy);
                case AccountingInOutDocType.WayBillSale: return AutoMapper.Mapper.DynamicMap<AccountingInOutDocWayBillSale>(incomeProxy);
                case AccountingInOutDocType.WorkCompleteAct: return AutoMapper.Mapper.DynamicMap<AccountingInOutDocWayBill>(incomeProxy);
                default:
                    throw new ValidationException(string.Empty);
            }
        }

        [HttpPost, Route("listsignedexpensesaleproduct")]
        public IDataResult ListSignedExpenseSaleProduct()
        {
            return StockroomExpenseOrderSaleList(RequestType.Product);
        }

        [HttpPost, Route("listsignedexpensesaleservice")]
        public IDataResult ListSignedExpenseSaleService()
        {
            return StockroomExpenseOrderSaleList(RequestType.Service);
        }

        [HttpPost, Route("listsignedaccoutingbill")]
        public IDataResult ListSignedAccountingInOutDocWayBill()
        {
            return SignedAccountingInOutDocWayBillList();
        }

        protected ObjectListResult StockroomExpenseOrderSaleList(RequestType requestType)
        {
            var query = StockroomExpenseOrderRouteEntityService.GetAll()
                .Where(x => x.ArchivedAt == null)
                .Where(x => x.DeletedAt == null)
                .Where(x => x.Employee == UserPrincipal.Employee)
                .Where(x => x.Document.StockroomExpenseOrderType == StockroomExpenseOrderType.Sale)
                .Select(x => x.Document as StockroomExpenseOrderSale)
                .Where(x => x.DocumentState == DocumentState.Signed)
                .Where(x => x.ProductRequest.RequestType == requestType)
                .Where(x => x.AccountingInOutDocWayBill == null);

            var data = StockroomExpenseOrderSaleQueryExecutor(query);

            return data;
        }

        protected ObjectListResult StockroomExpenseOrderSaleQueryExecutor(IQueryable<StockroomExpenseOrderSale> query)
        {
            var result = query.Select(x => new
            {
                id = x.Id,
                p_number = x.No,
                p_date = x.Date,
                p_kind = "Расходный складской ордер (продажа)",
                p_payer_display = x.ProductRequest.Payer.Name,
                p_getter_display = x.ProductRequest.Consignee.Name,
                p_getter_address = x.ProductRequest.Consignee.Client.Address,
                p_product_display = x.ProductRequest.Product.Name,
                p_measure_unit_display = x.ProductRequest.ProductMeasureUnit.ShortName,
                p_count = x.Amount,
                p_product_price = x.ProductRequest.ProductPrice,
                p_product_tax = x.ProductRequest.ProductTax,
                p_description = x.ProductRequest.Content
            })
            .Extend((row, x) =>
            {
                row["p_description"] = x.p_description.GetString();
                row["p_product_sum"] = x.p_count * x.p_product_price;
            })
            .ToList();

            return new ObjectListResult
            {
                Data = result,
                Count = result.Count
            };
        }

        protected ObjectListResult SignedAccountingInOutDocWayBillList()
        {
            var query = DocumentRouteEntityService.GetAll()
                .Where(x => x.ArchivedAt == null)
                .Where(x => x.DeletedAt == null)
                .Where(x => x.Employee == UserPrincipal.Employee)
                .Where(x => x.Document.AccountingInOutDocType == AccountingInOutDocType.WayBillSale)
                .Select(x => x.Document as AccountingInOutDocWayBillSale)
                .Where(x => x.OrganizationFrom == UserPrincipal.Organization)
                .Where(x => x.UsedAt == null)
                .Where(x => x.DocumentState >= DocumentState.OnSecondSideAgreement);

            var data = SignedAccountingInOutDocWayBillQueryExecutor(query);

            return data;
        }

        protected ObjectListResult SignedAccountingInOutDocWayBillQueryExecutor(IQueryable<AccountingInOutDocWayBillSale> query)
        {
            var result = query.Select(x => new
            {
                id = x.Id,
                p_number = x.No,
                p_date = x.Date,
                p_kind = "Накладная (продажа)",
                p_sum = x.Sum,
                p_contractor_display = x.OrganizationTo.Name
            })
            .ToList();

            return new ObjectListResult
            {
                Data = result,
                Count = result.Count
            };
        }

        #region bottom lists
        
        [HttpPost, Route("products")]
        public IDataResult StockroomExpenseOrderProductsBottomList()
        {
            return GetProductsBottomList(Request, ProductType.Product);
        }

        [HttpPost, Route("services")]
        public IDataResult StockroomExpenseOrderServicesBottomList()
        {
            return GetProductsBottomList(Request, ProductType.Service);
        }

        private IDataResult GetProductsBottomList(HttpRequestMessage request, ProductType productType)
        {
            var storeParams = request.GetStoreParams();

            var docIdStr = storeParams.FilterParams
               .Where(x => x.PropertyName == "p_base_doc")
               .Select(x => x.Value)
               .FirstOrDefault();

            var docId = docIdStr.SafeToString().ToLong();

            if (docId == 0)
            {
                return new ObjectListResult { Count = 0 };
            }

            
            var data = EntityService.GetAll()
                .Where(x => x.Id == docId)
                .Select(x => new {
                    wayBillId = (long?)(x as AccountingInOutDocInvoice).AccountingInOutDocWayBill.Id,
                    x.AccountingInOutDocType
                })
                .First();

            if (data.AccountingInOutDocType == AccountingInOutDocType.Invoice)
            {
                docId = data.wayBillId ?? 0;

                if (docId == 0)
                {
                    return new ObjectListResult { Count = 0 };
                }
            }

            var stockroomProductsProxy = GetStockroomProductDataProxyList(docId, productType);

            return new ObjectListResult
            {
                Data = stockroomProductsProxy,
                Count = stockroomProductsProxy != null ? 1 : 0
            };
        }

        private List<StockroomProductDataProxy> GetStockroomProductDataProxyList(long docId, ProductType productType)
        {
            return StockroomExpenseOrderSaleEntityService.GetAll()
                .Where(x => x.AccountingInOutDocWayBill.Id == docId)
                .Where(x => x.ProductRequest.Product.Type == productType)
                .Select(x => x.ProductRequest)
                .Select(x => new StockroomProductDataProxy
                {
                    id = x.Product.Id,
                    p_name = x.Product.Name,
                    p_group = x.Product.Group.Name,
                    p_subgroup = x.Product.SubGroup.Name,
                    p_trade_mark = x.Product.Brand,
                    p_manufacturer = x.Product.Manufacturer.Name,
                    p_unit_display = x.ProductMeasureUnit.Name,
                    p_tax = x.ProductTax,
                    p_price = x.ProductPrice
                })
                .ToList();
        }

        [HttpPost, Route(Suppliers)]
        public IDataResult StockroomExpenseOrderSuppliersBottomList()
        {
            return GetRequestOrgList(Request, Suppliers);
        }

        [HttpPost, Route(Getters)]
        public IDataResult StockroomExpenseOrderGettersBottomList()
        {
            return GetRequestOrgList(Request, Getters);
        }

        [HttpPost, Route(Payers)]
        public IDataResult StockroomExpenseOrderPayersBottomList()
        {
            return GetRequestOrgList(Request, Payers);
        }

        [HttpPost, Route(Sellers)]
        public IDataResult StockroomExpenseOrderSellersBottomList()
        {
            return GetRequestOrgList(Request, Sellers);
        }

        [HttpPost, Route(Senders)]
        public IDataResult StockroomExpenseOrderSendersBottomList()
        {
            return GetRequestOrgList(Request, Senders);
        }

        private IDataResult GetRequestOrgList(HttpRequestMessage request, string orgType)
        {
            var storeParams = request.GetStoreParams();

            var docIdStr = storeParams.FilterParams
               .Where(x => x.PropertyName == "p_base_doc")
               .Select(x => x.Value)
               .FirstOrDefault();

            var docId = docIdStr.SafeToString().ToLong();

            if (docId == 0)
            {
                return new ObjectListResult { Count = 0 };
            }


            var data = EntityService.GetAll()
                .Where(x => x.Id == docId)
                .Select(x => new
                {
                    wayBillId = (long?)(x as AccountingInOutDocInvoice).AccountingInOutDocWayBill.Id,
                    x.AccountingInOutDocType
                })
                .First();

            if (data.AccountingInOutDocType == AccountingInOutDocType.Invoice)
            {
                docId = data.wayBillId ?? 0;

                if (docId == 0)
                {
                    return new ObjectListResult { Count = 0 };
                }
            }

            var productRequestQuery = StockroomExpenseOrderSaleEntityService.GetAll()
                .Where(x => x.AccountingInOutDocWayBill.Id == docId)
                .Select(x => x.ProductRequest);

            IQueryable<Organization> organizationQuery = null;

            switch (orgType)
            {
                case Suppliers:
                    organizationQuery = productRequestQuery.Select(x => x.Provider);
                    break;
                case Getters:
                    organizationQuery = productRequestQuery.Select(x => x.Consignee);
                    break;
                case Payers:
                    organizationQuery = productRequestQuery.Select(x => x.Payer);
                    break;
                case Sellers:
                    organizationQuery = productRequestQuery.Select(x => x.Provider);
                    break;
                case Senders:
                    organizationQuery = productRequestQuery.Select(x => x.Provider);
                    break;
            }

            var organizationsProxy = OrganizationService.GetBottomList(organizationQuery, storeParams, false);

            return new ObjectListResult
            {
                Data = organizationsProxy.Item1,
                Count = organizationsProxy.Item2
            };
        }

        #endregion bottom lists
    }
}