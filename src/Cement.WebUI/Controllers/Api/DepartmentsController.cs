﻿using System.Linq;
using System.Web.Http;
using Cement.Core.Entities;
using Cement.Core.Extensions;
using Cement.Core.Services.StoreParams;
using Cement.Protocol;

namespace Cement.WebUI.Controllers.Api
{
    [RoutePrefix("api/department")]
    public class DepartmentsController : DataController<Department, DictionaryObject>
    {
        protected override IDataResult ListInternal(StoreParams storeParams)
        {
            var query = EntityService.GetAll()
                .OrganizationFilter(UserPrincipal)
                .Select(x => new DictionaryObject
                {
                    Id = x.Id,
                    Name = x.Name
                });

            var count = query.Count();

            var data = query.OrderBy(x => x.Name).ToList();

            return new ListResult<DictionaryObject>
            {
                Data = data,
                Count = count
            };
        }
    }
}