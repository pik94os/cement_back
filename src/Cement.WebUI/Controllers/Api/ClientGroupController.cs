﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using Cement.Core.Entities;
using Cement.Core.Extensions;
using Cement.Core.Services.StoreParams;
using Cement.Protocol;
using Cement.WebUI.Extensions;

namespace Cement.WebUI.Controllers.Api
{
    [RoutePrefix("api/clientgroup")]
    public class ClientGroupController : DataController<ClientGroup, ClientGroupProxy>
    {
        [HttpPost, Route("tree")]
        public List<TreeElement> Tree()
        {
            var data = EntityService.GetAll()
                .OrganizationFilter(UserPrincipal)
                .Select(x => new
                {
                    x.Id,
                    x.Name,
                    parentId = (long?)x.Parent.Id
                })
                .ToList();

            var childrenDict = data.Where(x => x.parentId != null)
                .GroupBy(x => x.parentId.Value)
                .ToDictionary(
                    x => x.Key,
                    x => x.Select(y => new TreeElement
                    {
                        Id = y.Id,
                        Text = y.Name
                    })
                    .ToList());

            var result = data.Where(x => x.parentId == null)
                .Select(x => new TreeElement
                {
                    Id = x.Id,
                    Text = x.Name,
                    Children = childrenDict.Get(x.Id)
                })
                .ToList();

            return result;
        }

        protected override IDataResult ListInternal(StoreParams storeParams)
        {
            var query = EntityService.GetAll()
                .Where(x => x.Parent == null)
                .OrganizationFilter(UserPrincipal)
                .Filter(storeParams);

            var count = query.Count();

            var data = query
                .Select(x => new
                {
                    id = x.Id,
                    p_name = x.Name,
                    p_description = x.Description,
                    //p_group_display = x.Parent.Name,
                    //p_group = (long?)x.Parent.Id,
                })
                .Order(storeParams)
                .Paging(storeParams)
                .Highlight(storeParams)
                .ToList();

            return new ObjectListResult
            {
                Data = data,
                Count = count
            };
        }

        [HttpPost, Route("subgroupslist")]
        public IDataResult SubGroupsList()
        {
            var storeParams = Request.GetStoreParams();
            
            var query = EntityService.GetAll()
                 .Where(x => x.Parent != null)
                 .OrganizationFilter(UserPrincipal)
                 .Filter(storeParams);

            var count = query.Count();

            var data = query
                .Select(x => new
                {
                    id = x.Id,
                    p_name = x.Name,
                    p_description = x.Description,
                    p_group_display = x.Parent.Name,
                    p_group = (long?)x.Parent.Id,
                })
                .Order(storeParams)
                .Paging(storeParams)
                .Highlight(storeParams)
                .ToList();

            return new ObjectListResult
            {
                Data = data,
                Count = count
            };
        }
    }
}