﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using Cement.Core.Entities;
using Cement.Core.Entities.Stockroom;
using Cement.Core.Enums;
using Cement.Core.Extensions;
using Cement.Core.Services.EntityService;
using Cement.Core.Services.StoreParams;
using Cement.Protocol;
using Cement.Protocol.JsonConverters;
using Cement.WebUI.Extensions;
using DataAccess.Repository;
using Newtonsoft.Json;

namespace Cement.WebUI.Controllers.Api
{
    [RoutePrefix("api/product")]
    public class ProductController : DataController<Product, ProductProxy>
    {
        public IEntityService<ProductPersonal> ProductPesonalService { get; set; }
        public IEntityService<ProductComplect> ProductComplectService { get; set; }

        public override IDataResult Get(long id)
        {
            var res  = DocumentList(null, EntityService.GetAll().Where(x => x.Id == id)) as ObjectListResult;

            if (res == null)
            {
                return null;
            }

            var list = res.Data as List<Dictionary<string, object>>;

            if (list == null || list.Count == 0)
            {
                return null;
            }

            var data = list.First();

            return new ObjectResult
            {
                Data = data
            };

            return res;
        }
        
        [HttpPost, Route("dictionarylist")]
        public List<DictionaryObject> Dropdownlist()
        {
            var data = EntityService.GetAll()
                //.OrganizationFilter(UserPrincipal)
                .Where(x => x.Type == ProductType.Product)
                .Select(x => new DictionaryObject
                {
                    Id = x.Id,
                    Name = x.Name
                })
                .OrderBy(x => x.Name)
                .ToList();

            return data;
        }

        [HttpPost, Route("danger_subclasses")]
        public List<DictionaryObject> GetDangerSubClasses()
        {
            var dangerClassIdStr = Request.GetStoreParams().GetFilterPropertyByName("p_danger_class_id");

            return Container.Resolve<IRepository<DangerSubClass>>().GetAll()
                .WhereIf(!string.IsNullOrEmpty(dangerClassIdStr), x => x.Class.Id == dangerClassIdStr.To<int>())
                .OrderBy(x => x.Num)
                .Select(x => new DictionaryObject { Id = x.Id, Name = x.Num + " " + x.Name }).ToList();
        }

        [HttpPost, Route("complect")]
        public List<ProductTreeElement> GetComplectList(TreeRequest request)
        {
            const int product = -10;
            const int service = -20;

            if (request == null || request.Node == 0)
            {
                return new List<ProductTreeElement>
                {
                    new ProductTreeElement
                    {
                        Id = product,
                        Name = "Товары",
                        Leaf = false
                    },
                    new ProductTreeElement
                    {
                        Id = service,
                        Name = "Услуги",
                        Leaf = false
                    }
                };
            }

            var res = EntityService.GetAll()
                //.OrganizationFilter(UserPrincipal)
                .WhereIf(request.Node == product, x => x.Type == ProductType.Product)
                .WhereIf(request.Node == service, x => x.Type == ProductType.Service)
                .Select(x => new ProductTreeElement
                {
                    Id = x.Id,
                    Name = x.Name,
                    Manufacturer = x.Manufacturer.Name,
                    Brand = x.Brand,
                    Unit = x.MeasureUnit.ShortName,
                    Complect = true,
                    Leaf = true
                })
                .ToList();

            return res;
        }

        [HttpPost, Route("moderation")]
        public IDataResult Moderation()
        {
            var storeParams = Request.GetStoreParams();

            var query = EntityService.GetAll()
                .Where(x => !x.IsModerated);

            return DocumentList(storeParams, query);
        }

        [HttpPost, Route("general")]
        public IDataResult General()
        {
            var storeParams = Request.GetStoreParams();

            var query = EntityService.GetAll()
                .Where(x => x.IsModerated);

            return DocumentList(storeParams, query);
        }

        [HttpPost, Route("personal")]
        public IDataResult Personal()
        {
            var storeParams = Request.GetStoreParams();

            var organizationId = storeParams.GetFilterPropertyByName("p_client_current").ToLong();

            var query = ProductPesonalService.GetAll()
                .OrganizationExclusiveFilter(UserPrincipal, organizationId)
                .Where(x => !x.ArchiveDate.HasValue)
                .Select(x => x.Product);

            return DocumentList(storeParams, query);
        }

        [HttpPost, Route("archive")]
        public IDataResult Archive()
        {
            var storeParams = Request.GetStoreParams();

            var query = ProductPesonalService.GetAll()
                .OrganizationFilter(UserPrincipal)
                .Where(x => x.ArchiveDate.HasValue)
                .Select(x => x.Product);

            return DocumentList(storeParams, query);
        }

        protected IDataResult DocumentList(StoreParams storeParams, IQueryable<Product> queryList)
        {
            //todo при переделке на отдачу только необходимых для грида полей доработать Get

            var productQuery = queryList.Where(x => x.Type == ProductType.Product);

            var query = productQuery
                .Select(x => new
                {
                    id = x.Id,
                    p_img = new Downloadable(x.Photo.Id),
                    p_group = (long?)x.Group.Id,
                    p_group_display = x.Group.Name,
                    p_manufacturer = x.Manufacturer.Name,
                    p_name = x.Name,
                    p_subgroup = (long?)x.SubGroup.Id,
                    p_subgroup_display = x.SubGroup.Name,
                    p_text = string.Empty,
                    p_trade_mark = x.Brand,
                    p_found_country_display = x.Country.Name,
                    p_found_country = (long?)x.Country.Id,
                    p_found_manufacturer_display = x.Manufacturer.Name,
                    p_found_manufacturer = (long?)x.Manufacturer.Id,
                    p_found_packer_display = x.Packer.Name,
                    p_found_packer = (long?)x.Packer.Id,
                    p_found_importer_display = x.Importer.Name,
                    p_found_importer = (long?)x.Importer.Id,
                    p_danger_class_display = x.DangerClass.Name,
                    p_danger_class = (long?)x.DangerClass.Id,
                    p_danger_sub_class_display = x.DangerSubClass.Name,
                    p_danger_sub_class = (long?)x.DangerSubClass.Id,
                    p_danger_oon_display = x.DangerUnCode.Name,
                    p_danger_oon = (long?)x.DangerUnCode.Id,
                    p_tu_gost = x.Gost,
                    p_tu_gost_r = x.GostR,
                    p_tu_ost = x.Ost,
                    p_tu_stp = x.Stp,
                    p_tu_tu = x.Tu,
                    p_code_okp = x.Okp,
                    p_code_th_ved = x.TnVed,
                    p_code_zd = x.CargoTrainCode,
                    p_code_bar = x.Barcode,
                    p_sert_soot_number = x.ConformityCertificateNumber,
                    p_sert_soot_img1 = new Downloadable(x.ConformityCertificateScan.Id),
                    p_sert_soot_img2 = new Downloadable(x.ConformityCertificateScan2.Id),
                    p_sert_soot_start = x.ConformityCertificateStartDate,
                    p_sert_soot_end = x.ConformityCertificateEndDate,
                    p_sert_deklar_number = x.ConformityDeclarationNumber,
                    p_sert_deklar_img = new Downloadable(x.ConformityDeclarationScan.Id),
                    p_sert_deklar_start = x.ConformityDeclarationStartDate,
                    p_sert_deklar_end = x.ConformityDeclarationEndDate,
                    p_sert_san_number = x.HealthCertificateNumber,
                    p_sert_san_img = new Downloadable(x.HealthCertificateScan.Id),
                    p_sert_san_start = x.HealthCertificateStartDate,
                    p_sert_san_end = x.HealthCertificateEndDate,
                    p_sert_fire_number = x.FireSafetyCertificateNumber,
                    p_sert_fire_img = new Downloadable(x.FireSafetyCertificateScan.Id),
                    p_sert_fire_start = x.FireSafetyCertificateStartDate,
                    p_sert_fire_end = x.FireSafetyCertificateEndDate,
                    p_sert_fire_deklar = x.FireSafetyDeclarationNumber,
                    p_sert_fire_deklar_img = new Downloadable(x.FireSafetyDeclarationScan.Id),
                    p_sert_fire_deklar_start = x.FireSafetyDeclarationStartDate,
                    p_sert_fire_deklar_end = x.FireSafetyDeclarationEndDate,
                    p_package_item_kind_display = x.TypeSingle.Name,
                    p_package_item_material_display = x.MaterialSingle.Name,
                    p_package_item_volume_display = x.VolumeSingle + " " + x.VolumeUnitSingle.Name,
                    p_package_item_weight_display = x.WeightSingle + " " + x.WeightUnitSingle.Name,
                    p_package_item_size_display = (x.WidthSingle.HasValue ? x.WidthSingle.Value + "x" : string.Empty) +
                                                  (x.HeightSingle.HasValue ? x.HeightSingle.Value + "x" : string.Empty) +
                                                  (x.LengthSingle.HasValue ? x.LengthSingle.Value + " " : string.Empty) +
                                                  x.SizeUnitSingle.Name,
                    p_package_group_kind_display = x.TypeGroup.Name,
                    p_package_group_material_display = x.MaterialGroup.Name,
                    p_package_group_volume_display = x.VolumeGroup + " " + x.VolumeUnitGroup.Name,
                    p_package_group_weight_display = x.WeightGroup + " " + x.WeightUnitGroup.Name,
                    p_package_group_size_display = (x.WidthGroup.HasValue ? x.WidthGroup.Value + "x" : string.Empty) +
                                                   (x.HeightGroup.HasValue ? x.HeightGroup.Value + "x" : string.Empty) +
                                                   (x.LengthGroup.HasValue ? x.LengthGroup.Value + " " : string.Empty) +
                                                   x.SizeUnitGroup.Name,
                    p_package_vehicle_kind_display = x.TypeTransport.Name,
                    p_package_vehicle_material_display = x.MaterialTransport.Name,
                    p_package_vehicle_volume_display = x.VolumeTransport + " " + x.VolumeUnitTransport.Name,
                    p_package_vehicle_weight_display = x.WeightTransport + " " + x.WeightUnitTransport.Name,
                    p_package_vehicle_size_display = (x.WidthTransport.HasValue ? x.WidthTransport.Value + "x" : string.Empty) +
                                                   (x.HeightTransport.HasValue ? x.HeightTransport.Value + "x" : string.Empty) +
                                                   (x.LengthTransport.HasValue ? x.LengthTransport.Value + " " : string.Empty) +
                                                   x.SizeUnitTransport.Name,
                    p_package_vehicle_count = x.UnitsCountTransport,
                    p_package_group_count = x.UnitsCountGroup,
                    p_package_display = x.PackageDisplayString,
                    p_complect_display = x.ComplectString,
                    p_package_kind = x.Kind,
                    p_package_type = x.Kind,
                    x.PackageJsonBytes
                })
                .Filter(storeParams);

            var count = query.Count();

            var productComplectDict = Container.Resolve<IRepository<ProductComplect>>().GetAll()
                .Where(x => productQuery.Select(y => y.Id).Contains(x.Product.Id))
                .Select(x => new
                {
                    ProductId = x.Product.Id,
                    ComplectProductId = x.ComplectProduct.Id
                })
                .AsEnumerable()
                .GroupBy(x => x.ProductId)
                .ToDictionary(x => x.Key, x => x.Select(y => y.ComplectProductId).ToArray());

            var data = query
                .Order(storeParams)
                .Paging(storeParams)
                .Extend((row, x) =>
                {
                    row["p_package"] = x.PackageJsonBytes.GetString();
                    row["p_complect"] = productComplectDict.ContainsKey(x.id) ? productComplectDict[x.id] : new long[0];
                    row["p_have_complect"] = productComplectDict.ContainsKey(x.id) ? 1 : 0;
                })
                .Highlight(storeParams)
                .ToList();

            return new ObjectListResult
            {
                Data = data,
                Count = count
            };
        }

        [HttpPost, Route("topersonal/{id}")]
        public virtual JsResult ToPersonalCatalog(long id)
        {
            try
            {
                ProductPesonalService.Save(new ProductPersonal{ Organization = UserPrincipal.Organization, Product = new Product { Id = id } });

                return JsResult.Successful();
            }
            catch (Exception exc)
            {
                return new JsResult
                {
                    Msg = exc.Message,
                    Id = id,
                    Success = false
                };
            }
        }

        [HttpPost, Route("archive/{id}")]
        public virtual JsResult ArchiveItem(long id)
        {
            try
            {
                var productPersonal = ProductPesonalService.GetAll()
                    .Where(x => x.Product.Id == id)
                    .Where(x => x.Organization == UserPrincipal.Organization)
                    .First();

                productPersonal.ArchiveDate = DateTime.Now;
                ProductPesonalService.Save(productPersonal);
                return JsResult.Successful();
            }
            catch (Exception exc)
            {
                return new JsResult
                {
                    Msg = exc.Message,
                    Id = id,
                    Success = false
                };
            }
        }

        [HttpPost, Route("unarchive/{id}")]
        public virtual JsResult UnarchiveItem(long id)
        {
            try
            {
                var productPersonal = ProductPesonalService.GetAll()
                    .Where(x => x.Product.Id == id)
                    .Where(x => x.Organization == UserPrincipal.Organization)
                    .First();

                productPersonal.ArchiveDate = null;
                ProductPesonalService.Save(productPersonal);
                return JsResult.Successful();
            }
            catch (Exception exc)
            {
                return new JsResult
                {
                    Msg = exc.Message,
                    Id = id,
                    Success = false
                };
            }
        }

        [HttpPost, Route("deletePersonal/{id}")]
        public virtual JsResult DeleteProductPersonal(long id)
        {
            try
            {
                var productPersonal = ProductPesonalService.GetAll()
                    .Where(x => x.Product.Id == id)
                    .Where(x => x.Organization == UserPrincipal.Organization)
                    .First();

                ProductPesonalService.Delete(productPersonal.Id);
                return JsResult.Successful();
            }
            catch (Exception exc)
            {
                return new JsResult
                {
                    Msg = exc.Message,
                    Id = id,
                    Success = false
                };
            }
        }

        [HttpPost, Route("complectlist")]
        public virtual IDataResult ComplectList()
        {
            var storeParams = Request.GetStoreParams();
            var productId = storeParams.GetFilterPropertyByName("p_product").ToLong();

            var query = ProductComplectService.GetAll()
                .Where(x => x.Product.Id == productId)
                .Select(x => x.ComplectProduct);

            return DocumentList(storeParams, query);
        }

        public override JsResult Save()
        {
            var incomeProxy = Request.ToType<ProductProxy>();

            var entity = AutoMapper.Mapper.DynamicMap<Product>(incomeProxy);

            entity.Type = ProductType.Product;
            
            EntityService.Save(entity);

            return new JsResult(entity.Id);
        }
    }
}