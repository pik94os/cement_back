﻿using System;
using System.Linq;
using System.Web.Http;
using Cement.Core.Entities;
using Cement.Core.Enums;
using Cement.Core.Extensions;
using Cement.Core.Services.EntityService;
using Cement.Core.Services.StoreParams;
using Cement.Protocol;
using Cement.WebUI.Extensions;

namespace Cement.WebUI.Controllers.Api
{
    [RoutePrefix("api/personalclientevent")]
    public class PersonalClientEventController : DataController<PersonalClientEvent, PersonalClientEventProxy>
    {
        #region injections
        public IEntityService<PersonalClientEventComment> PersonalClientEventCommentService { get; set; }
        #endregion injections

        protected override IDataResult ListInternal(StoreParams storeParams)
        {
            var query = EntityService.GetAll()
                .OrganizationFilter(x => x.PersonalClient.Organization, UserPrincipal)
                .Where(x => x.ArchivedAt == null);

            return ListContractData(storeParams, query);
        }
        
        [HttpPost, Route("listarchived")]
        public IDataResult ListArchived()
        {
            var storeParams = Request.GetStoreParams();

            var query = EntityService.GetAll()
                .OrganizationFilter(x => x.PersonalClient.Organization, UserPrincipal)
                .Where(x => x.ArchivedAt != null);

            return ListContractData(storeParams, query);
        }

        protected IDataResult ListContractData(StoreParams storeParams, IQueryable<PersonalClientEvent> personalClientEventQuery)
        {
            var reminderTypeDict = typeof (PersonalClientEventReminderType).AsDictionary();

            var query = personalClientEventQuery.Select(x => new
                {
                    id = x.Id,

                    p_theme = x.Theme,
                    p_description = x.Description,
                    p_reminder = x.ReminderType,
                    p_date = x.EventDateTime,

                    p_personal_client = x.PersonalClient.Id,
                    p_client_display = x.PersonalClient.Client.Name,

                    p_client = x.PersonalClient.Client.Id,

                    p_group = (long?)x.PersonalClient.Group.Id,
                    p_group_display = x.PersonalClient.Group.Name,
                    p_sub_group = (long?)x.PersonalClient.Subgroup.Id,
                    p_sub_group_display = x.PersonalClient.Subgroup.Name,

                    p_firm_brand = x.PersonalClient.Client.Brand,
                    p_firm_form = x.PersonalClient.Client.Ownership.ShortName,
                    p_firm_full_name = x.PersonalClient.Client.FullName,
                    //p_firm_img = "",
                    p_firm_name = x.PersonalClient.Client.Name,
                    p_cont_email = x.PersonalClient.Client.Email,
                    p_cont_fax = x.PersonalClient.Client.Fax,
                    p_cont_phone = x.PersonalClient.Client.Phone,
                    p_cont_web = x.PersonalClient.Client.Web,
                    p_fact_building = x.PersonalClient.Client.FactAddressBuilding,
                    p_fact_city = x.PersonalClient.Client.FactAddressLocality,
                    p_fact_country = x.PersonalClient.Client.FactAddressCountry,
                    p_fact_district = x.PersonalClient.Client.FactAddressArea,
                    p_fact_house = x.PersonalClient.Client.FactAddressHouse,
                    p_fact_house_part = x.PersonalClient.Client.FactAddressHousing,
                    p_fact_index = x.PersonalClient.Client.FactAddressIndex,
                    p_fact_office = x.PersonalClient.Client.FactAddressOffice,
                    p_fact_region = x.PersonalClient.Client.FactAddressRegion,
                    p_fact_street = x.PersonalClient.Client.FactAddressStreet,
                    p_reg_inn = x.PersonalClient.Client.Inn,
                    //p_reg_inn_img = "",
                    p_reg_kpp = x.PersonalClient.Client.Kpp,
                    p_reg_ogrn = x.PersonalClient.Client.Ogrn,
                    //p_reg_ogrn_img = "",
                    p_reg_okato = x.PersonalClient.Client.Okato,
                    p_reg_okonh = x.PersonalClient.Client.Okonh,
                    p_reg_okpo = x.PersonalClient.Client.Okpo,
                    p_reg_okved = x.PersonalClient.Client.Okved,
                    p_address_display = x.PersonalClient.Client.Address,
                    p_ur_building = x.PersonalClient.Client.JurAddressBuilding,
                    p_ur_city = x.PersonalClient.Client.JurAddressLocality,
                    p_ur_country = x.PersonalClient.Client.JurAddressCountry,
                    p_ur_district = x.PersonalClient.Client.JurAddressArea,
                    p_ur_house = x.PersonalClient.Client.JurAddressHouse,
                    p_ur_house_part = x.PersonalClient.Client.JurAddressHousing,
                    p_ur_index = x.PersonalClient.Client.JurAddressIndex,
                    p_ur_office = x.PersonalClient.Client.JurAddressOffice,
                    p_ur_region = x.PersonalClient.Client.JurAddressRegion,
                    p_ur_street = x.PersonalClient.Client.JurAddressStreet,

                    p_curator_display = x.PersonalClient.Employee.Name,
                    p_curator = (long?)x.PersonalClient.Employee.Id,
                    p_curator_name = x.PersonalClient.Employee.FirstName,
                    p_curator_surname = x.PersonalClient.Employee.Surname,
                    p_curator_lastname = x.PersonalClient.Employee.Patronymic,
                    p_curator_position = x.PersonalClient.Employee.Position.Name,
                    p_curator_for = string.Empty,

                    p_first_contact = x.PersonalClient.DateFirstContract,
                    p_last_contact = x.PersonalClient.DateLastContract,

                    p_status = (long?)x.PersonalClient.State.Id,
                    p_status_display = x.PersonalClient.State.Name,
                    p_from = x.PersonalClient.From,
                    p_comment = x.PersonalClient.Comment
                })
                .Filter(storeParams);

            var count = query.Count();

            var queryData = query.Order(storeParams).Paging(storeParams);

            var eventIds = queryData.Select(x => x.id).ToList();

            var commentsDict = PersonalClientEventCommentService.GetAll()
                .Where(x => eventIds.Contains(x.PersonalClientEvent.Id))
                .Select(x => new
                {
                    x.Text,
                    x.PersonalClientEvent.Id,
                    x.DateTime
                })
                .AsEnumerable()
                .GroupBy(x => x.Id)
                .ToDictionary(
                    x => x.Key, 
                    x => x.OrderBy(y => y.DateTime)
                          .Select(y => new
                          {
                              text = y.Text,
                              date = y.DateTime.HasValue ? y.DateTime.Value.ToString("dd.MM.yyyy HH:mm:ss"): string.Empty
                          })
                          .ToList());

            var data = query
                .Order(storeParams)
                .Paging(storeParams)
                .Extend((row, x) =>
                {
                    row["p_time"] = x.p_date.HasValue ? x.p_date.Value.ToString("HH:mm") : "00:00";
                    row["p_reminder"] = reminderTypeDict.Get((int)x.p_reminder);
                    row["p_comments"] = commentsDict.Get(x.id);
                    row["p_personal_client_display"] = x.p_client_display;
                })
                .Highlight(storeParams)
                .ToList();

            return new ObjectListResult
            {
                Data = data,
                Count = count
            };
        }

        [HttpPost, Route("archive/{id}")]
        public JsResult Archive(long id)
        {
            return ManageArchive(id, true);
        }

        [HttpPost, Route("unarchive/{id}")]
        public JsResult Unarchive(long id)
        {
            return ManageArchive(id, false);
        }

        /// <summary>
        /// Управление архивированием
        /// </summary>
        protected JsResult ManageArchive(long id, bool archivate, string successText = "Успешно")
        {
            try
            {
                var personalClientEvent = EntityService.Get(id);

                if (personalClientEvent != null)
                {
                    personalClientEvent.ArchivedAt = archivate ? DateTime.Now : (DateTime?)null;
                    EntityService.Update(personalClientEvent);
                }

                return JsResult.Successful(successText);
            }
            catch (Exception exc)
            {
                return new JsResult
                {
                    Msg = exc.Message,
                    Id = id,
                    Success = false
                };
            }
        }

        [HttpPost, Route("comment")]
        public JsResult Comment()
        {
            var requestNvc = Request.ToNameValueCollection();

            var id = requestNvc.Get("id").ToLong();
            var comment = requestNvc.Get("comment");

            if (id == 0)
            {
                return new JsResult();
            }

            PersonalClientEventCommentService.Save(new PersonalClientEventComment
            {
                DateTime = DateTime.Now,
                Employee = UserPrincipal.Employee,
                PersonalClientEvent = new PersonalClientEvent { Id = id },
                Text = comment
            });

            var result = PersonalClientEventCommentService.GetAll()
                .Where(x => x.PersonalClientEvent.Id == id)
                .Select(x => new
                {
                    x.Text,
                    x.DateTime
                })
                .AsEnumerable()
                .OrderBy(y => y.DateTime)
                .Select(y => new
                {
                    text = y.Text,
                    date = y.DateTime.HasValue ? y.DateTime.Value.ToString("dd.MM.yyyy HH:mm:ss") : string.Empty
                })
                .ToList();

            return new JsResult
            {
                Data = result,
                Success = true
            };
        }
    }
}