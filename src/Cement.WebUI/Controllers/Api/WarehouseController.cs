﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using Cement.Core.Entities;
using Cement.Core.Entities.Stockroom;
using Cement.Core.Extensions;
using Cement.Core.Services;
using Cement.Core.Services.EntityService;
using Cement.Core.Services.StoreParams;
using Cement.Protocol;
using Cement.WebUI.Extensions;
using DataAccess.Repository;

namespace Cement.WebUI.Controllers.Api
{
    [RoutePrefix("api/warehouse")]
    public class WarehouseController : DataController<Warehouse, WarehouseProxy>
    {
        public IEntityService<WarehouseEmployee> WarehouseEmployeeEntityService { get; set; }
        public IEntityService<Inventory> InventoryEntityService { get; set; }
        
        [HttpPost, Route("stationdict")]
        public IDataResult Dropdownlist(RailwayStationtRequest request)
        {
            if (string.IsNullOrWhiteSpace(request.Query))
            {
                return null;
            }

            if (request.Query.Length < 4)
            {
                return null;
            }

            var stations = Container.Resolve<IRepository<RailwayStation>>().GetAll()
                .Where(x => x.Name.ToUpper().StartsWith(request.Query.ToUpper()))
                .Select(x => new
                {
                    id = x.Id,
                    p_road = x.RailwayName,
                    p_station = x.Name,
                    p_station_code = x.Code,
                })
                .OrderBy(x => x.p_station)
                .ThenBy(x => x.p_road)
                .ToList();

            return new ObjectListResult
            {
                Data = stations,
                Count = stations.Count
            };
        }

        public override IDataResult Get(long id)
        {
            var res = List(EntityService.GetAll().Where(x => x.Id == id), null, false);

            var list = res.Item1 as List<Dictionary<string, object>>;

            if (list ==null || list.Count == 0)
            {
                return null;
            }

            var data = list.First();

            data["p_warehouse_employee"] = WarehouseEmployeeEntityService.GetAll()
                .Where(x => x.Warehouse.Id == id)
                .Select(x => new
                {
                    x.Employee.Id, 
                    x.Employee.Name,
                    PositionName = x.Employee.Position.Name,
                    DepartmentName = x.Employee.Department.Name
                })
                .ToList();

            return new ObjectResult
            {
                Data = data
            };
        }

        public static Tuple<object, int> List(IQueryable<Warehouse> warehouseQuery, StoreParams storeParams, bool separateCountQuery = true )
        {
            var query = warehouseQuery
               .Select(x => new
               {
                   id = x.Id,
                   p_kind = x.Type,
                   p_kind_display = x.TypeStr,
                   p_railway_is_own = x.IsOwnDriveways,
                   clientInn = x.DrivewaysOwner.Inn,
                   clientKpp = x.DrivewaysOwner.Kpp,
                   clientOkpo = x.DrivewaysOwner.Okpo,
                   ownInn = x.Organization.Client.Inn,
                   ownKpp = x.Organization.Client.Kpp,
                   ownOkpo = x.Organization.Client.Okpo,
                   p_railway_owner = (long?)x.DrivewaysOwner.Id,
                   p_railway_owner_display = x.DrivewaysOwner.Name,
                   p_name = x.Name,
                   p_index = x.AddressIndex,
                   p_country = x.AddressCountry,
                   p_region = x.AddressRegion,
                   p_district = x.AddressArea,
                   p_city = x.AddressLocality,
                   p_street = x.AddressStreet,
                   p_house = x.AddressHouse,
                   p_housepart = x.AddressHousing,
                   p_building = x.AddressBuilding,
                   p_office = x.AddressOffice,
                   p_firm_code = x.CompanyCode,
                   p_station = (long?)x.RailwayStation.Id,
                   p_station_display = x.RailwayStation.Name,
                   p_station_code_display = x.RailwayStation.Code,
                   p_road_display = x.RailwayStation.RailwayName,
                   p_address = x.Address,
                   p_address_comment = x.AddressComment,
                   p_latitude = x.Latitude,
                   p_longitude = x.Longitude
               })
               .Filter(storeParams);

            var data = query
                .Order(storeParams)
                .Paging(storeParams)
                .Extend((row, x) =>
                {
                    row["p_country_display"] = x.p_country;
                    row["p_region_display"] = x.p_region;
                    row["p_district_display"] = x.p_district;
                    row["p_city_display"] = x.p_city;
                    row["p_street_display"] = x.p_street;
                    row["p_form_kind"] = (int)x.p_kind;

                    if (x.p_railway_is_own)
                    {
                        row["p_inn"] = x.ownInn;
                        row["p_kpp"] = x.ownKpp;
                        row["p_okpo"] = x.ownOkpo;
                        row["p_railway_owner_inn"] = x.ownInn;
                        row["p_railway_owner_kpp"] = x.ownKpp;
                        row["p_railway_owner_okpo"] = x.ownOkpo;
                    }
                    else
                    {
                        row["p_inn"] = x.clientInn;
                        row["p_kpp"] = x.clientKpp;
                        row["p_okpo"] = x.clientOkpo;
                        row["p_railway_owner_inn"] = x.clientInn;
                        row["p_railway_owner_kpp"] = x.clientKpp;
                        row["p_railway_owner_okpo"] = x.clientOkpo;
                    }
                })
                .Highlight(storeParams)
                .ToList();

            var count = separateCountQuery ? query.Count() : data.Count;

            return new Tuple<object, int>(data, count); 
        }

        protected override IDataResult ListInternal(StoreParams storeParams)
        {
            var organizationId = storeParams.GetFilterPropertyByName("p_client_current").ToLong();

            var query = EntityService.GetAll()
                .OrganizationExclusiveFilter(UserPrincipal, organizationId);

            var res = List(query, storeParams);

            return new ObjectListResult
            {
                Count = res.Item2,
                Data = res.Item1
            };
        }

        [HttpPost, Route("structure")]
        public List<WarehouseTreeElement> Structure(TreeRequestWithFirmId request)
        {
            return EntityService.GetAll()
                .WhereIf(request.OrganizationId != 0, x => x.Organization.Id == request.OrganizationId)
                .WhereIf(request.OrganizationId == 0, x => x.Organization == UserPrincipal.Organization)
                .Select(x => new WarehouseTreeElement
                {
                    Id = x.Id,
                    Name = x.Name,
                    Leaf = true,
                    Address = x.Address
                })
                .ToList();
        }

        [HttpPost, Route("organization_owned_without_personal")]
        public IDataResult OrganizationOwnedWithoutPersonal()
        {
            var storeParams = Request.GetStoreParams();

            var query = EntityService.GetAll()
                .OrganizationFilter(UserPrincipal)
                .WarehouseFilter(x => x, UserPrincipal, true)
                .Select(x => new { x.Id, x.Name, x.Address })
                .Filter(storeParams);

            return new ObjectListResult
            {
                Count = query.Count(),
                Data = query.Order(storeParams).Paging(storeParams).ToList()
            };
        }

        [HttpPost, Route("organization_owned")]
        public IDataResult OrganizationOwned()
        {
            var storeParams = Request.GetStoreParams();

            var except = storeParams.GetDataPropertyByName("except").ToLong();

            var query = EntityService.GetAll()
                .OrganizationFilter(UserPrincipal)
                .WhereIf(except > 0, x => x.Id != except)
                .Select(x => new { x.Id, x.Name, x.Address })
                .Filter(storeParams);

            return new ObjectListResult
            {
                Count = query.Count(),
                Data = query.Order(storeParams).Paging(storeParams).ToList()
            };
        }

        [HttpPost, Route("personal")]
        public IDataResult PersonalList()
        {
            var storeParams = Request.GetStoreParams();

            var query = EntityService.GetAll()
                .WarehouseFilter(x => x, UserPrincipal)
                .Select(x => new { x.Id, x.Name, x.Address })
                .Filter(storeParams);

            return new ObjectListResult
            {
                Count = query.Count(),
                Data = query.Order(storeParams).Paging(storeParams).ToList()
            };
        }

        [HttpPost, Route("listemployee")]
        public IDataResult EmployeeList()
        {
            var storeParams = Request.GetStoreParams();

            var warehouseId = storeParams.GetDataPropertyByName("warehouseId").ToLong();

            var excludeQuery = WarehouseEmployeeEntityService.GetAll()
                .Where(x => x.Warehouse.Id == warehouseId)
                .Select(x => x.Employee.Id);

            var data = Container.Resolve<IEntityService<Employee>>().GetAll()
                .OrganizationFilter(UserPrincipal)
                .WhereIf(warehouseId > 0, x => !excludeQuery.Contains(x.Id))
                .Select(x => new
                {
                    x.Id, 
                    x.Name, 
                    PositionName = x.Position.Name, 
                    DepartmentName = x.Department.Name
                })
                .ToList();

            return new ObjectListResult
            {
                Count = data.Count(),
                Data = data
            };
        }
    }
}