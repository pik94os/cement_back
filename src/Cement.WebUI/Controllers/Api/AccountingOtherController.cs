﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Net.Http;
using System.Web.Http;
using Cement.Core.Entities;
using Cement.Core.Entities.Stockroom;
using Cement.Core.Enums;
using Cement.Core.Exceptions;
using Cement.Core.Extensions;
using Cement.Core.Services;
using Cement.Core.Services.EntityService;
using Cement.Core.Services.StoreParams;
using Cement.Protocol;
using Cement.WebUI.Extensions;

namespace Cement.WebUI.Controllers.Api
{
    [RoutePrefix("api/accountingother")]
    public class AccountingOtherController : RoutedEntityDataController<AccountingOtherBase, AccountingOtherProxy, AccountingOtherRoute>
    {
        public IEntityService<StockroomExpenseOrderRoute> StockroomExpenseOrderRouteEntityService { get; set; }
        public IEntityService<StockroomExpenseOrderMovement> StockroomExpenseOrderMovementEntityService { get; set; }
        public IEntityService<StockroomExpenseOrderSale> StockroomExpenseOrderSaleEntityService { get; set; }
        public IEntityService<StockroomExpenseOrderWriteoff> StockroomExpenseOrderWriteoffEntityService { get; set; }
        public IEntityService<StockroomBaseRequest> StockroomBaseRequestEntityService { get; set; }
        public IEntityService<StockroomPostingRequest> StockroomPostingRequestEntityService { get; set; }
        public IEntityService<StockroomMakingPostingRequest> StockroomMakingPostingRequestEntityService { get; set; }
        public IWarehouseService WarehouseService { get; set; }

        protected override Expression<Func<AccountingOtherRoute, bool>> DocIdFilterExpression(long id)
        {
            return x => x.Document.Id == id;
        }

        protected override IQueryable<AccountingOtherRoute> PersonalizeQuery(IQueryable<AccountingOtherRoute> query)
        {
            return query.Where(x => x.Employee == UserPrincipal.Employee);
        }

        protected override void DeleteInternal(long routeId)
        {
            // Бухгалтерские документы в общем не удаляются, но можно удалить некорректно созданный, т.е. тот, который в новых
            
            var docRoute = DocumentRouteEntityService.Get(routeId);
            EntityService.Delete(docRoute.Document.Id);
        }

        #region Get
        protected void ProcessMovementBillDetails(AccountingOtherMovementBill accountingOtherMovementBill, Dictionary<string, object> dict)
        {
            dict["p_movementbill_kind"] = accountingOtherMovementBill.AccountingOtherMovementBillKind;

            var query = StockroomExpenseOrderMovementEntityService.GetAll()
                .Where(x => x.AccountingOtherMovementBill == accountingOtherMovementBill);

            var expenseOrders = StockroomExpenseOrderMovementQueryExecutor(query);

            dict["p_dst"] = expenseOrders;
            dict["p_src"] = StockroomExpenseOrderMovementList();
        }

        protected void ProcessWriteoffActDetails(AccountingOtherWriteoffAct accountingOtherWriteoffAct, Dictionary<string, object> dict)
        {
            dict["p_writeoffact_kind"] = accountingOtherWriteoffAct.AccountingOtherWriteoffActKind;

            var query = Container.Resolve<IEntityService<StockroomExpenseOrderWriteoff>>().GetAll()
                .Where(x => x.AccountingOtherWriteoffAct == accountingOtherWriteoffAct);

            var expenseOrders = StockroomExpenseOrderWriteoffQueryExecutor(query);

            dict["p_dst"] = expenseOrders;
            dict["p_src"] = StockroomExpenseOrderWriteoffList();
        }

        protected void ProcessPostingActDetails(AccountingOtherPostingAct accountingOtherPostingAct, Dictionary<string, object> dict)
        {
            //dict["p_cause"] = "Test";
            //dict["p_cause_display"] = "Test";

            var query = Container.Resolve<IEntityService<StockroomBaseRequest>>().GetAll()
                .Where(x => x.StockroomRequestType == StockroomRequestType.Posting || x.StockroomRequestType == StockroomRequestType.MakingPosting)
                .Where(x => (x as StockroomPostingRequest).AccountingOtherPostingAct == accountingOtherPostingAct);

            var expenseOrders = StockroomPostingRequestQueryExecutor(query);

            dict["p_dst"] = expenseOrders;
            dict["p_src"] = StockroomPostingRequestList();
        }

        protected void ProcessDetails(AccountingOtherBase accountingOther, Dictionary<string, object> dict)
        {
            if (accountingOther is AccountingOtherMovementBill)
            {
                ProcessMovementBillDetails(accountingOther as AccountingOtherMovementBill, dict);
            } 
            else if (accountingOther is AccountingOtherPostingAct)
            {
                ProcessPostingActDetails(accountingOther as AccountingOtherPostingAct, dict);
            } 
            else if (accountingOther is AccountingOtherWriteoffAct)
            {
                ProcessWriteoffActDetails(accountingOther as AccountingOtherWriteoffAct, dict);
            }
        }

        public override IDataResult Get(long id)
        {
            var data = EntityService.GetAll().First(x => x.Id == id);

            var res = new
            {
                id = data.Id,
                p_number = data.No,
                p_date = data.Date,
                p_type = data.AccountingOtherDocumentType
            };

            var result = res.Extend((row, x) => ProcessDetails(data, row));

            return new ObjectResult { Data = result };
        }
        #endregion Get

        protected override IDataResult DocumentList(StoreParams storeParams, IQueryable<AccountingOtherRoute> routeQuery)
        {
            var query = routeQuery
                .Select(x => new
                {
                    p_base_doc = x.Document.Id,
                    id = x.Id,
                    p_kind = x.Document.AccountingOtherDocumentType,
                    p_number = x.Document.No,
                    p_date = x.Document.Date,
                    p_type = x.MovementType,
                    p_state = x.Document.DocumentState,
                    p_sum = x.Document.Sum
                })
                .Filter(storeParams);

            var data = query
                .Order(storeParams)
                .Paging(storeParams)
                .Extend((row, x) =>
                {
                    row["p_kind_display"] = x.p_kind.GetDisplayValue();
                    row["p_type"] = x.p_type.GetDisplayValue();
                    row["p_form_kind"] = (int)x.p_kind;
                    row["p_state"] = x.p_state.GetDisplayValue();
                })
                .Highlight(storeParams)
                .ToList();

            return new ObjectListResult
            {
                Data = data,
                Count = query.Count()
            };
        }

        protected override AccountingOtherBase MapProxyToEntity(AccountingOtherProxy incomeProxy)
        {
            switch (incomeProxy.AccountingOtherDocumentType)
            {
                case AccountingOtherDocumentType.MovementBill: return AutoMapper.Mapper.DynamicMap<AccountingOtherMovementBill>(incomeProxy);
                case AccountingOtherDocumentType.PostingAct: return AutoMapper.Mapper.DynamicMap<AccountingOtherPostingAct>(incomeProxy);
                case AccountingOtherDocumentType.WriteoffAct: return AutoMapper.Mapper.DynamicMap<AccountingOtherWriteoffAct>(incomeProxy);
                default:
                    throw new ValidationException(string.Empty);
            }
        }

        protected ObjectListResult StockroomExpenseOrderMovementList()
        {
            var query = StockroomExpenseOrderRouteEntityService.GetAll()
                .Where(x => x.ArchivedAt == null)
                .Where(x => x.DeletedAt == null)
                .Where(x => x.Employee == UserPrincipal.Employee)
                .Where(x => x.Document is StockroomExpenseOrderMovement)
                .Select(x => x.Document as StockroomExpenseOrderMovement)
                .Where(x => x.DocumentState == DocumentState.Signed)
                .Where(x => x.AccountingOtherMovementBill == null);

            var data = StockroomExpenseOrderMovementQueryExecutor(query);

            return data;
        }

        protected ObjectListResult StockroomExpenseOrderWriteoffList()
        {
            var query = StockroomExpenseOrderRouteEntityService.GetAll()
                .Where(x => x.ArchivedAt == null)
                .Where(x => x.DeletedAt == null)
                .Where(x => x.Employee == UserPrincipal.Employee)
                .Where(x => x.Document is StockroomExpenseOrderWriteoff)
                .Select(x => x.Document as StockroomExpenseOrderWriteoff)
                .Where(x => x.DocumentState == DocumentState.Signed)
                .Where(x => x.AccountingOtherWriteoffAct == null);

            var data = StockroomExpenseOrderWriteoffQueryExecutor(query);

            return data;
        }

        protected ObjectListResult StockroomPostingRequestList()
        {
            var query = Container.Resolve<IEntityService<StockroomRequestRoute>>().GetAll()
                .Where(x => x.ArchivedAt == null)
                .Where(x => x.DeletedAt == null)
                .Where(x => x.Employee == UserPrincipal.Employee)
                .Where(x => x.Document.DocumentState == DocumentState.Signed)
                .Where(x => x.Document.StockroomRequestType == StockroomRequestType.Posting || x.Document.StockroomRequestType == StockroomRequestType.MakingPosting)
                .Where(x => (x.Document as StockroomPostingRequest).AccountingOtherPostingAct == null)
                .Select(x => x.Document);

            var data = StockroomPostingRequestQueryExecutor(query);

            return data;
        }

        protected ObjectListResult StockroomExpenseOrderMovementQueryExecutor(IQueryable<StockroomExpenseOrderMovement> query)
        {
            var result = query.Select(x => new
            {
                id = x.Id,
                p_kind = "Расходный складской ордер (перемещение)",
                p_warehouse_name = x.StockroomMovementRequest.Warehouse.Name,
                p_warehouse_address = x.StockroomMovementRequest.Warehouse.Address,
                p_number = x.No,
                p_date = x.Date,
                p_product_display = x.StockroomMovementRequest.StockroomProduct.Product.Name,
                p_measure_unit_display = x.StockroomMovementRequest.StockroomProduct.MeasureUnit.ShortName,
                p_count = x.Amount,
                p_description = x.StockroomMovementRequest.ExtraData
            })
            .ToList();

            return new ObjectListResult
            {
                Data = result,
                Count = result.Count
            };
        }

        protected ObjectListResult StockroomExpenseOrderWriteoffQueryExecutor(IQueryable<StockroomExpenseOrderWriteoff> query)
        {
            var result = query.Select(x => new
                {
                    id = x.Id,
                    p_kind = "Расходный складской ордер (списание)",
                    p_number = x.No,
                    p_date = x.Date,
                    p_product_display = x.StockroomWriteoffRequest.StockroomProduct.Product.Name,
                    p_measure_unit_display = x.StockroomWriteoffRequest.StockroomProduct.MeasureUnit.ShortName,
                    p_count = x.Amount
                })
                .ToList();

            return new ObjectListResult
            {
                Data = result,
                Count = result.Count
            };
        }

        protected ObjectListResult StockroomPostingRequestQueryExecutor(IQueryable<StockroomBaseRequest> query)
        {
            var result = query.Select(x => new
                {
                    id = x.Id,
                    p_kind = x.StockroomRequestType,
                    p_number = x.No,
                    p_date = x.Date,
                    p_product_display = x.StockroomProduct.Product.Name,
                    p_measure_unit_display = x.StockroomProduct.MeasureUnit.ShortName,
                    p_count = x.Amount
                })
                .Extend((row, x) =>
                {
                    row["p_kind"] = x.p_kind.GetDisplayValue();
                })
                .ToList();

            return new ObjectListResult
            {
                Data = result,
                Count = result.Count
            };
        }

        [HttpPost, Route("listsignedexpensemovement")]
        public IDataResult ListSignedExpenseMovement()
        {
            return StockroomExpenseOrderMovementList();
        }

        [HttpPost, Route("listsignedexpensewriteoff")]
        public IDataResult ListSignedExpenseWriteoff()
        {
            return StockroomExpenseOrderWriteoffList();
        }

        [HttpPost, Route("listsignedpostingrequest")]
        public IDataResult ListSignedPostingRequest()
        {
            return StockroomPostingRequestList();
        }

        #region bottom lists

        [HttpPost, Route("warehouses")]
        public IDataResult StockroomExpenseOrderWarehousesBottomList()
        {
            var storeParams = Request.GetStoreParams();

            var docIdStr = storeParams.FilterParams
                .Where(x => x.PropertyName == "p_base_doc")
                .Select(x => x.Value)
                .FirstOrDefault();

            var docId = docIdStr.SafeToString().ToLong();

            if (docId == 0)
            {
                return new ObjectListResult { Count = 0 };
            }

            IQueryable<Warehouse> warehouseQuery = null;

            var accountingOtherDocumentType = EntityService.Load(docId).AccountingOtherDocumentType;

            switch (accountingOtherDocumentType)
            {
                case AccountingOtherDocumentType.MovementBill:
                    warehouseQuery = StockroomExpenseOrderMovementEntityService.GetAll()
                        .Where(x => x.AccountingOtherMovementBill.Id == docId)
                        .Select(x => x.Warehouse);
                    break;
                case AccountingOtherDocumentType.PostingAct:
                    warehouseQuery = StockroomBaseRequestEntityService.GetAll()
                        .Where(x => x.StockroomRequestType == StockroomRequestType.MakingPosting || x.StockroomRequestType == StockroomRequestType.Posting)
                        .Where(x => (x as StockroomPostingRequest).AccountingOtherPostingAct.Id == docId)
                        .Select(x => x.Warehouse);
                    break;
                case AccountingOtherDocumentType.WriteoffAct:
                    warehouseQuery = StockroomExpenseOrderWriteoffEntityService.GetAll()
                        .Where(x => x.AccountingOtherWriteoffAct.Id == docId)
                        .Select(x => x.Warehouse);
                    break;
            }

            var listData = WarehouseService.GetBottomList(warehouseQuery, storeParams, false);

            return new ObjectListResult
            {
                Data = listData.Item1,
                Count = listData.Item2
            };
        }

        [HttpPost, Route("products")]
        public IDataResult StockroomExpenseOrderProductsBottomList()
        {
            return GetProductsBottomList(Request, ProductType.Product);
        }

        [HttpPost, Route("services")]
        public IDataResult StockroomExpenseOrderServicesBottomList()
        {
            return GetProductsBottomList(Request, ProductType.Service);
        }

        private IDataResult GetProductsBottomList(HttpRequestMessage request, ProductType productType)
        {
            var storeParams = request.GetStoreParams();

            var docIdStr = storeParams.FilterParams
               .Where(x => x.PropertyName == "p_base_doc")
               .Select(x => x.Value)
               .FirstOrDefault();

            var docId = docIdStr.SafeToString().ToLong();

            if (docId == 0)
            {
                return new ObjectListResult { Count = 0 };
            }

            List<StockroomProductDataProxy> stockroomProductsProxy = null;

            var accountingOtherDocumentType = EntityService.Load(docId).AccountingOtherDocumentType;

            switch (accountingOtherDocumentType)
            {
                case AccountingOtherDocumentType.MovementBill:
                    stockroomProductsProxy = StockroomExpenseOrderMovementEntityService.GetAll()
                        .Where(x => x.AccountingOtherMovementBill.Id == docId)
                        .Where(x => x.StockroomMovementRequest.StockroomProduct.Product.Type == productType)
                        .Select(x => x.StockroomMovementRequest.StockroomProduct)
                        .Select(x => new StockroomProductDataProxy
                        {
                            id = x.Product.Id,
                            p_name = x.Product.Name,
                            p_group = x.Product.Group.Name,
                            p_subgroup = x.Product.SubGroup.Name,
                            p_trade_mark = x.Product.Brand,
                            p_manufacturer = x.Product.Manufacturer.Name,
                            p_unit_display = x.MeasureUnit.Name,
                            p_tax = null, // когда будет брать из товара склада
                            p_price = null //когда будет брать из товара склада
                        })
                        .ToList();
                    break;
                case AccountingOtherDocumentType.PostingAct:
                    stockroomProductsProxy = StockroomBaseRequestEntityService.GetAll()
                        .Where(x => x.StockroomRequestType == StockroomRequestType.MakingPosting || x.StockroomRequestType == StockroomRequestType.Posting)
                        .Where(x => x.StockroomProduct.Product.Type == productType)
                        .Where(x => (x as StockroomPostingRequest).AccountingOtherPostingAct.Id == docId)
                        .Select(x => x.StockroomProduct)
                        .Select(x => new StockroomProductDataProxy
                        {
                            id = x.Product.Id,
                            p_name = x.Product.Name,
                            p_group = x.Product.Group.Name,
                            p_subgroup = x.Product.SubGroup.Name,
                            p_trade_mark = x.Product.Brand,
                            p_manufacturer = x.Product.Manufacturer.Name,
                            p_unit_display = x.MeasureUnit.Name,
                            p_tax = null, // когда будет брать из товара склада
                            p_price = null //когда будет брать из товара склада
                        })
                        .ToList();
                    break;
                case AccountingOtherDocumentType.WriteoffAct:
                    stockroomProductsProxy = StockroomExpenseOrderWriteoffEntityService.GetAll()
                        .Where(x => x.AccountingOtherWriteoffAct.Id == docId)
                        .Where(x => x.StockroomWriteoffRequest.StockroomProduct.Product.Type == productType)
                        .Select(x => x.StockroomWriteoffRequest.StockroomProduct)
                        .Select(x => new StockroomProductDataProxy
                        {
                            id = x.Product.Id,
                            p_name = x.Product.Name,
                            p_group = x.Product.Group.Name,
                            p_subgroup = x.Product.SubGroup.Name,
                            p_trade_mark = x.Product.Brand,
                            p_manufacturer = x.Product.Manufacturer.Name,
                            p_unit_display = x.MeasureUnit.Name,
                            p_tax = null, // когда будет брать из товара склада
                            p_price = null //когда будет брать из товара склада
                        })
                        .ToList();
                    break;
            }


            return new ObjectListResult
            {
                Data = stockroomProductsProxy,
                Count = stockroomProductsProxy != null ? 1 : 0
            };
        }

        #endregion bottom lists
    }
}