﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using Cement.Core;
using Cement.Core.Entities;
using Cement.Core.Enums;
using Cement.Core.Extensions;
using Cement.Core.Services;
using Cement.Core.Services.EntityService;
using Cement.Protocol;
using Cement.WebUI.Framework;

namespace Cement.WebUI.Controllers.Api
{
    [Authorize]
    [RoutePrefix("api/document")]
    public class DocumentController : CementApiController
    {
        public IEntityService<InternalDocument> InternalDocumentEntityService { get; set; }
        public IEntityService<Contract> ContractEntityService { get; set; }
        public IEntityService<Correspondence> СorrespondenceEntityService { get; set; }
        public IDocumentService DocumentService { get; set; }


        [HttpPost, Route("tree")]
        public List<NamedTreeElement> DocumentTree(TreeRequestStringNode request)
        {
            if (request == null || request.Node == "0")
            {
                return typeof(DocumentType).AsDictionary()
                    .Select(x => new NamedTreeElement
                    {
                        Id = string.Format("{0}{1}0", x.Key, Constants.DocBranchIdDelimeter),
                        Name = x.Value,
                        Leaf = false
                    })
                    .ToList();
            }

            var namedTreeElementList = new List<NamedTreeElement>();

            var keyParts = request.Node.Split(Constants.DocBranchIdDelimeter).ToList();

            if (keyParts.Count != 2)
            {
                return namedTreeElementList;
            }
            
            var docType = (DocumentType)keyParts.First().ToLong();
            var docKind = keyParts.Last().ToLong();

            switch (docType)
            {
                case DocumentType.InternalDocument: return InternalDocumentTree(docKind);

                case DocumentType.Correspondence: return CorrespondenceTree(docKind);

                case DocumentType.Contract: return ContractTree(docKind);
            }
            
            return namedTreeElementList;
        }

        protected List<NamedTreeElement> InternalDocumentTree(long kind)
        {
            if (kind == 0)
            {
                return typeof(InternalDocumentType).AsDictionary()
                    .Select(x => new NamedTreeElement
                    {
                        Id = DocumentService.GetBranchIdString(DocumentType.InternalDocument, x.Key),
                        Name = x.Value,
                        Leaf = false
                    })
                    .ToList();
            }

            var docKind = (InternalDocumentType) kind;

            var namedTreeElementList = InternalDocumentEntityService.GetAll()
                        .OrganizationFilter(UserPrincipal)
                        .Where(x => x.InternalDocumentType == docKind)
                        .Select(x => new NamedTreeElement
                        {
                            Id = DocumentService.GetDocIdString(DocumentType.InternalDocument, x.Id),
                            Name = x.Name
                        })
                        .OrderBy(x => x.Name)
                        .ToList();

            return namedTreeElementList;
        }

        protected List<NamedTreeElement> CorrespondenceTree(long kind)
        {
            if (kind == 0)
            {
                return typeof(CorrespondenceType).AsDictionary()
                    .Select(x => new NamedTreeElement
                    {
                        Id = DocumentService.GetBranchIdString(DocumentType.Correspondence, x.Key),
                        Name = x.Value,
                        Leaf = false
                    })
                    .ToList();
            }

            var docKind = (CorrespondenceType)kind;

            var namedTreeElementList = СorrespondenceEntityService.GetAll()
                        .OrganizationFilter(UserPrincipal)
                        .Where(x => x.CorrespondenceType == docKind)
                        .Select(x => new NamedTreeElement
                        {
                            Id = DocumentService.GetDocIdString(DocumentType.Correspondence, x.Id),
                            Name = x.Name
                        })
                        .OrderBy(x => x.Name)
                        .ToList();

            return namedTreeElementList;
        }

        protected List<NamedTreeElement> ContractTree(long kind)
        {
            var namedTreeElementList = ContractEntityService.GetAll()
                .Where(x => x.Organization == UserPrincipal.Organization)
                .Select(x => new NamedTreeElement
                {
                    Id = DocumentService.GetDocIdString(DocumentType.Contract, x.Id),
                    Name = x.Name
                })
                .OrderBy(x => x.Name)
                .ToList();
            
            return namedTreeElementList;
        }

        [HttpPost, Route("internaldoctree")]
        public List<TreeElement> InternalDocumentTree(TreeRequest request)
        {
            if (request == null || request.Node == 0)
            {
                return typeof(InternalDocumentType).AsDictionary()
                    .Select(x => new TreeElement
                    {
                        Id = x.Key,
                        Text = x.Value,
                        Children = new List<TreeElement>{ new TreeElement()}
                    })
                    .ToList();
            }

            var docKind = (InternalDocumentType)request.Node;

            var namedTreeElementList = InternalDocumentEntityService.GetAll()
                .OrganizationFilter(UserPrincipal)
                .Where(x => x.DocumentState == DocumentState.Signed)
                .Where(x => x.InternalDocumentType == docKind)
                .Select(x => new TreeElement
                {
                    Id = x.Id,
                    Text = x.Name
                })
                .OrderBy(x => x.Text)
                .ToList();

            return namedTreeElementList;
        }

        [HttpPost, Route("warehouse_causes")]
        public List<NamedTreeElement> WarehouseCauses()
        {
            // вот тут надо тянуть внутренние документы и иные бух. доки
            var namedTreeElementList = ContractEntityService.GetAll()
                .Where(x => x.Organization == UserPrincipal.Organization)
                .Select(x => new NamedTreeElement
                {
                    //Id = DocumentService.GetDocIdString(DocumentType.Contract, x.Id),
                    Id = x.Id,
                    Name = x.Name
                })
                .OrderBy(x => x.Name)
                .ToList();

            return namedTreeElementList;
        }
    }
}