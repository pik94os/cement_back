﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using Cement.Core.Auth;
using Cement.Core.Entities;
using Cement.Core.Entities.Dict;
using Cement.Core.Enums;
using Cement.Core.Extensions;
using Cement.Core.Services.EntityService;
using Cement.Protocol;
using Cement.Protocol.JsonConverters;
using Cement.WebUI.Extensions;
using Cement.WebUI.Framework;
using Cement.WebUI.Navigation;
using DataAccess.Repository;
using DataAccess.SessionProvider;
namespace Cement.WebUI.Controllers.Api
{
    [Authorize]
    [RoutePrefix("server")]
    public class ServerController : CementApiController
    {
        [HttpGet, Route("firmdetails")]
        public object Documents()
        {
            var organization = UserPrincipal.Organization;
            var client = organization != null ? organization.Client : null;
            var clientId = client != null ? client.Id : (long?)null;

            var data = Container.Resolve<IEntityService<Client>>().GetAll()
                .Where(x => x.Id == clientId)
                .Select(x => new
                {
                    //id = x.Id,
                    p_brand = x.Brand,
                    p_ownership = x.Ownership.ShortName,
                    p_full_name = x.FullName,
                    //p_firm_img = "",
                    p_name = x.Name,

                    p_email = x.Email,
                    p_fax = x.Fax,
                    p_phone = x.Phone,
                    p_web = x.Web,

                    p_fact_building = x.FactAddressBuilding,
                    p_fact_city = x.FactAddressLocality,
                    p_fact_country = x.FactAddressCountry,
                    p_fact_district = x.FactAddressArea,
                    p_fact_house = x.FactAddressHouse,
                    p_fact_housepart = x.FactAddressHousing,
                    p_fact_index = x.FactAddressIndex,
                    p_fact_office = x.FactAddressOffice,
                    p_fact_region = x.FactAddressRegion,
                    p_fact_street = x.FactAddressStreet,

                    p_inn = x.Inn,
                    p_inn_file = "",
                    p_kpp = x.Kpp,
                    p_ogrn = x.Ogrn,
                    p_ogrn_file = "",
                    p_okato = x.Okato,
                    p_okonh = x.Okonh,
                    p_okpo = x.Okpo,
                    p_okved = x.Okved,
                    p_address_display = x.Address,

                    p_ur_building = x.JurAddressBuilding,
                    p_law_city = x.JurAddressLocality,
                    p_law_country = x.JurAddressCountry,
                    p_law_district = x.JurAddressArea,
                    p_law_house = x.JurAddressHouse,
                    p_law_housepart = x.JurAddressHousing,
                    p_law_building = x.JurAddressBuilding,
                    p_law_index = x.JurAddressIndex,
                    p_law_office = x.JurAddressOffice,
                    p_law_region = x.JurAddressRegion,
                    p_law_street = x.JurAddressStreet,

                    p_charter = " - ",
                    p_firm_code = " - ",
                    p_station = " - ",
                    p_station_code = " - ",
                    p_road = " - "
                })
                .FirstOrDefault();

            return data;
        }

        [HttpPost, Route("contracts_structure")]
        public object Contracts()
        {
            return new List<object>
            {
                new {
		            id= "555",
		            text= "Действующие",
		            leaf= false,
		            expanded= false,
		            children= new List<object> {
			            new {
				            id= "1111",
				            text= "Группа 1",
				            leaf= false,
				            expanded= false,
				            children= new List<object> {
					            new {
						            id= 2,
						            text= "ООО Рога и копыта",
						            leaf= true
					            },
					            new {
						            id= 3,
						            text= "ООО Рога и копыта 2",
						            leaf= true
					            },
					            new {
						            id= 4,
						            text= "ООО Рога и копыта 3",
						            leaf= true
					            }
				            }
			            }
		            }
	            },
	            new {
		            id= "666",
		            text= "Потенциальные",
		            leaf= false,
		            expanded= false,
		            children= new List<object> {
			            new {
				            id= "2222",
				            text= "Группа 1",
				            leaf= false,
				            expanded= false,
				            children= new List<object> {
					            new {
						            id= 23,
						            text= "ООО Рога и копыта",
						            leaf= true
					            },
					            new {
						            id= 33,
						            text= "ООО Рога и копыта 2",
						            leaf= true
					            },
					            new {
						            id= 43,
						            text= "ООО Рога и копыта 3",
						            leaf= true
					            }
				            }
			            }
		            }
	            }
            };
        }

        [HttpPost, Route("dashboard")]
        public object DashBoard()
        {
            var info = Container.Resolve<IEntityService<Employee>>().GetAll()
                .Where(x => x == UserPrincipal.Employee)
                .Select(x => new
                {
                    p_photo = new Downloadable(x.Photo.Id),
                    p_logo = new Downloadable(x.Organization.Logo.Id),
                    p_emp_name = x.Name,
                    p_emp_department = x.Department.Name,
                    p_emp_position = x.Position.Name,
                    p_emp_phone = x.Phone1,
                    p_emp_mobile = x.MobilePhone1,
                    p_emp_email = x.Email1,
                    p_firm_name = x.Organization.Name,
                    p_firm_ur_address = x.Organization.Client.Address,
                    p_firm_fact_address = x.Organization.Client.Kpp,
                    p_firm_web = x.Organization.Client.Okpo,
                    p_firm_inn = x.Organization.Client.Inn
                })
                .FirstOrDefault() ?? new
                {
                    p_photo = new Downloadable(null),
                    p_logo = new Downloadable(null),
                    p_emp_name = "",
                    p_emp_department = "",
                    p_emp_position = "",
                    p_emp_phone = "",
                    p_emp_mobile = "",
                    p_emp_email = "",
                    p_firm_name = "",
                    p_firm_ur_address = "",
                    p_firm_fact_address = "",
                    p_firm_web = "",
                    p_firm_inn = ""
                };

            var news = Enumerable.Range(1, 10)
                .Select(x => new
                    {
                        id = x,
                        p_name = "Новость " + x,
                        p_date = DateTime.Now
                    })
                .ToList();
            
            var docs = Container.Resolve<IEntityService<InternalDocumentMovement>>()
                .GetAll()
                .Where(x => !x.IsArchived)
                .Where(x => x.Employee == UserPrincipal.Employee)
                .Select(x => new
                {
                    x.Document.Name,
                    x.Document.DocumentState,
                    x.MovementType
                })
                .ToList();

            var officeDocRoute = "Cement.view.office_work.documents.Total";

            var events = new List<object>
            {
                new
                {
                    p_name = "Внутренние документы",
                    expanded = true,
                    leaf = false,
                    id = "1",
                    //p_new_total = 10,
                    //p_new = 9,
                    //p_work_total = 15,
                    //p_work = 9,
                    p_sign_total = docs.Count(x => x.DocumentState == DocumentState.Signed),
                    //p_sign = 19,
                    p_agree_total = docs.Count(x => x.DocumentState == DocumentState.OnAgreement),
                    //p_agree = 9,
                    //p_control_total = 12,
                    //p_control = 11,
                    p_route_1 = officeDocRoute,
                    p_route_2 = officeDocRoute,
                    p_route_3 = officeDocRoute,
                    p_route_4 = officeDocRoute,
                    p_route_5 = officeDocRoute,
                    children = new List<object>
                    {
                        new
                        {
                            id = "2",
                            p_name = "Входящие",
                            expanded = true,
                            leaf = false,
                            //p_new_total = 10,
                            //p_new = 9,
                            //p_work_total = 15,
                            //p_work = 9,
                            //p_sign_total = 20,
                            //p_sign = 19,
                            //p_agree_total = 10,
                            //p_agree = 9,
                            //p_control_total = 12,
                            //p_control = 11,
                            p_sign_total = docs.Where(x => x.MovementType == DocumentMovementType.Incoming).Count(x => x.DocumentState == DocumentState.Signed),
                            p_agree_total = docs.Where(x => x.MovementType == DocumentMovementType.Incoming).Count(x => x.DocumentState == DocumentState.OnAgreement),
                            p_route_1 = officeDocRoute + "#tab-1",
                            p_route_2 = officeDocRoute + "#tab-1",
                            p_route_3 = officeDocRoute + "#tab-1",
                            p_route_4 = officeDocRoute + "#tab-1",
                            p_route_5 = officeDocRoute + "#tab-1",
                            children = docs.Where(x => x.MovementType == DocumentMovementType.Incoming)
                            .Select(x => new { p_name = x.Name, leaf =  true})
                            .ToList()
                            /*.Enumerable.Range(10, 10).Select(x => new
                                {
                                    id = x,
                                    p_name = "Тест " + x,
                                    leaf = true,
                                    p_new_total = 10,
                                    p_new = 9,
                                    p_work_total = 15,
                                    p_work = 9,
                                    p_sign_total = 20,
                                    p_sign = 19,
                                    p_agree_total = 10,
                                    p_agree = 9,
                                    p_control_total = 12,
                                    p_control = 11,
                                    p_route_1 = "Cement.view.goods.auto.Total#tab-1",
                                    p_route_2 = "Cement.view.goods.auto.Total#tab-2",
                                    p_route_3 = "Cement.view.goods.auto.Total#tab-3",
                                    p_route_4 = "Cement.view.goods.auto.Total#tab-0",
                                    p_route_5 = "Cement.view.goods.auto.Total#tab-1"
                                }
                            ).ToList()*/
                        },
                        new
                        {
                            id = "4",
                            p_name = "Исходящие",
                            expanded = true,
                            leaf = false,
                            //p_new_total = 10,
                            //p_new = 9,
                            //p_work_total = 15,
                            //p_work = 9,
                            //p_sign_total = 20,
                            //p_sign = 19,
                            //p_agree_total = 10,
                            //p_agree = 9,
                            //p_control_total = 12,
                            //p_control = 11,
                            p_sign_total = docs.Where(x => x.MovementType == DocumentMovementType.Outcoming).Count(x => x.DocumentState == DocumentState.Signed),
                            p_agree_total = docs.Where(x => x.MovementType == DocumentMovementType.Outcoming).Count(x => x.DocumentState == DocumentState.OnAgreement),
                            p_route_1 = officeDocRoute + "#tab-2",
                            p_route_2 = officeDocRoute + "#tab-2",
                            p_route_3 = officeDocRoute + "#tab-2",
                            p_route_4 = officeDocRoute + "#tab-2",
                            p_route_5 = officeDocRoute + "#tab-2",
                            children = docs.Where(x => x.MovementType == DocumentMovementType.Outcoming)
                            .Select(x => new { p_name = x.Name, leaf =  true})
                            .ToList()/*Enumerable.Range(20, 10).Select(x => new
                                {
                                    id = x,
                                    p_name = "Тест " + x,
                                    leaf = true,
                                    p_new_total = 10,
                                    p_new = 9,
                                    p_work_total = 15,
                                    p_work = 9,
                                    p_sign_total = 20,
                                    p_sign = 19,
                                    p_agree_total = 10,
                                    p_agree = 9,
                                    p_control_total = 12,
                                    p_control = 11,
                                    p_route_1 = "Cement.view.products.products.List",
                                    p_route_2 = "Cement.view.products.products.List",
                                    p_route_3 = "Cement.view.products.products.List",
                                    p_route_5 = "Cement.view.products.products.List",
                                    p_route_4 = "Cement.view.products.products.List"
                                }).ToList()*/
                        }
                    }
                }
            };

            return new {
                info= info,
                events= events,
                news= news
            };
        }

        [HttpPost, Route("navigation")]
        public object Navigation()
        {
            return Container.Resolve<INavigationProvider>().GetNavigation(UserPrincipal);

            #region Персональные данные
            var personalData = new
            {
                groupName = "Персональные данные",
                items = new List<object>
                {
                    new
                    {
                        text = "Внутренние документы",
                        master = "Cement.view.personal.documents.Total",
                        detail = ""
                    },
                    new
                    {
                        text = "Корреспонденция",
                        master = "Cement.view.personal.correspondention.Total",
                        detail = ""
                    },
                    new
                    {
                        text = "Календарь",
                        master = "Cement.view.personal.calendar.Total",
                        detail = "Cement.view.personal.calendar.view.DatePicker"
                    },
                    new
                    {
                        text = "Сообщения",
                        master = "Cement.view.personal.messages.Total",
                        detail = "Cement.view.personal.messages.view.View"
                    },
                    new
                    {
                        text = "Объявления",
                        master = "Cement.view.personal.adverts.Total",
                        detail = "Cement.view.personal.adverts.view.View"
                    }
                }
            };
            #endregion Персональные данные

            #region Корпоративные данные

            var corporateData = new
            {
                groupName = "Корпоративные данные",
                items = new List<object>
                {
                    new
                    {
                        text = "Реквизиты",
                        master = "Cement.view.corporate.firm.ClientView",
                        detail = "Cement.view.corporate.firm.View"
                    },
                    new
                    {
                        text = "Реквизиты счетов",
                        master = "Cement.view.corporate.bank_account.List",
                        detail = "Cement.view.corporate.bank_account.View"
                    },
                    new
                    {
                        text = "Номенклатура дел",
                        master = "Cement.view.corporate.nomenklature.List",
                        detail = "Cement.view.corporate.nomenklature.View"
                    },
                    new
                    {
                        text = "Склады",
                        master = "Cement.view.corporate.warehouse.List",
                        detail = "Cement.view.corporate.warehouse.View"
                    },
                    new
                    {
                        text = "Сотрудники",
                        master = "Cement.view.corporate.employee.List",
                        detail = "Cement.view.corporate.employee.View"
                    },
                    new
                    {
                        text = "Подписи",
                        master = "Cement.view.corporate.signature.List",
                        detail = "Cement.view.corporate.signature.View"
                    }
                }
            };
            #endregion Корпоративные данные

            #region Транспорт

            var transport = new
            {
                groupName = "Транспорт",
                items = new List<object>
                {
                    new
                    {
                        text = "Автомобильный транспорт",
                        master = "Cement.view.transport.auto.Complex",
                        detail = "Cement.view.transport.auto.View"
                    },
                    new
                    {
                        text = "Управление АТС",
                        master = "Cement.view.transport.auto_control.Complex",
                        detail = "Cement.view.transport.auto_control.view.Maintenance"
                    },
                    new
                    {
                        text = "Транспортные единицы",
                        master = "Cement.view.transport.units.Complex",
                        detail = "Cement.view.transport.units.View"
                    },
                    //new
                    //{
                    //    text = "ЖД Транспорт",
                    //    master = "Cement.view.transport.railway.Complex",
                    //    detail = "Cement.view.transport.railway.View"
                    //}
                }
            };
            #endregion Транспорт

            #region Товары (услуги)

            var products = new
            {
                groupName = "Товары (услуги)",
                items = new List<object>
                {
                    new
                    {
                        text = "Товары",
                        master = "Cement.view.products.products.Total",
                        detail = "Cement.view.products.products.View"
                    },
                    new
                    {
                        text = "Услуги",
                        master = "Cement.view.products.services.Total",
                        detail = "Cement.view.products.services.View"
                    },
                    new
                    {
                        text = "Прайс-лист",
                        master = "Cement.view.products.price.Total",
                        detail = "Cement.view.products.price.View"
                    },
                    new
                    {
                        text = "Тип упаковки",
                        master = "Cement.view.products.packtype.List",
                        detail = "Cement.view.products.packtype.View"
                    }
                }
            };
            #endregion Товары (услуги)

            #region Клиенты

            var clients = new
            {
                groupName = "Клиенты",
                items = new List<object>
                {
                    new
                    {
                        text = "Клиенты",
                        master = "Cement.view.clients.clients.Total",
                        detail = "Cement.view.clients.View"
                    },
                    new
                    {
                        text = "Управление клиентами",
                        master = "Cement.view.clients.management.Total",
                        detail = "Cement.view.clients.management.View"
                    },
                    new
                    {
                        text = "Группы/подгруппы",
                        //master = "Cement.view.clients.groups.list.Groups",
                        master = "Cement.view.clients.groups.Total",
                        detail = "Cement.view.clients.groups.view.Group"
                    },
                    new
                    {
                        text = "Статус",
                        master = "Cement.view.clients.statuses.list.Statuses",
                        detail = "Cement.view.clients.statuses.View"
                    }
                }
            };
            #endregion Клиенты

            #region Договора

            var contracts = new
            {
                groupName = "Договора",
                items = new List<object>
                {
                    new
                    {
                        text = "Договор",
                        master = "Cement.view.contracts.contracts.Total",
                        detail = "Cement.view.contracts.contracts.view.Contract"
                    },
                    new
                    {
                        text = "Приложения",
                        master = "Cement.view.contracts.addons.Total",
                        detail = "Cement.view.contracts.contracts.view.Contract"
                    }
                }
            };
            #endregion Договора

            #region Товарооборот

            var goods = new
            {
                groupName = "Товарооборот",
                items = new List<object>
                {
                    //new
                    //{
                    //    text = "Планы",
                    //    master = "Cement.view.goods.plans.Total",
                    //    detail = "Cement.view.goods.plans.View"
                    //},
                    new
                    {
                        text = "Каталог",
                        master = "Cement.view.goods.catalog.Complex",
                        detail = "Cement.view.goods.catalog.ProductView"
                    },
                    new
                    {
                        //text = "Заявки на товар (АВТО)",
                        text = "Заявки на товар",
                        master = "Cement.view.goods.auto.Total",
                        detail = "Cement.view.goods.auto.view.TemplateView"
                    },
                    //new
                    //{
                    //    text = "Заявки на товар (ЖД)",
                    //    master = "Cement.view.goods.railway.Total",
                    //    detail = "Cement.view.goods.railway.view.TemplateView"
                    //},
                    new
                    {
                        text = "Заявки на услуги",
                        master = "Cement.view.goods.services.Total",
                        detail = "Cement.view.goods.services.view.TemplateView"
                    }
                }
            };
            #endregion Товарооборот

            #region Грузооборот

            var cargo = new
            {
                groupName = "Грузооборот",
                items = new List<object>
                {
                    new
                    {
                        text = "Лот на груз",
                        master = "Cement.view.cargo.goods_lot.Total",
                        detail = "Cement.view.cargo.goods_lot.view.View"
                    },
                    new
                    {
                        text = "Лот на транспорт",
                        master = "Cement.view.cargo.transport_lot.Total",
                        detail = "Cement.view.cargo.goods_lot.view.View"
                    },
                    new
                    {
                        text = "Биржа грузов",
                        master = "Cement.view.cargo.goods_exchange.Total",
                        detail = ""
                    },
                    new
                    {
                        text = "Биржа транспорта",
                        master = "Cement.view.cargo.transport_exchange.Total",
                        detail = ""
                    },
                    new
                    {
                        text = "Участники",
                        master = "Cement.view.cargo.participants.Total",
                        detail = ""
                    }
                }
            };
            #endregion Грузооборот

            #region Бухгалтерия

            var accounting = new
            {
                groupName = "Бухгалтерия",
                items = new List<object>
                {
                    new
                    {
                        text = "Документы",
                        master = "Cement.view.accounting.documents.Total",
                        detail = "Cement.view.accounting.documents.view.View"
                    },
                    new
                    {
                        text = "Индикаторы",
                        master = "Cement.view.accounting.indicators.Total",
                        detail = "Cement.view.accounting.indicators.view.View"
                    }
                }
            };
            #endregion Бухгалтерия

            #region Делопроизводство

            var office_work = new
            {
                groupName = "Делопроизводство",
                items = new List<object>
                {
                    new
                    {
                        text = "Внутренние документы",
                        master = "Cement.view.office_work.documents.Total",
                        detail = "Cement.view.office_work.documents.View"
                    },
                    new
                    {
                        text = "Корреспонденция",
                        master = "Cement.view.office_work.correspondention.Total",
                        detail = "Cement.view.office_work.correspondention.View"
                    },
                    new
                    {
                        text = "Приказы",
                        master = "Cement.view.office_work.disposal.Total",
                        detail = "Cement.view.office_work.disposal.View"
                    }
                }
            };
            #endregion Делопроизводство

            #region Навигация

            var navigation = new
            {
                groupName = "Навигация",
                items = new List<object>
                {
                    new
                    {
                        text = "Карта",
                        master = "Cement.view.navigation.map.Total",
                        detail = "Cement.view.navigation.map.View"
                    },
                    new
                    {
                        text = "Терминалы",
                        master = "Cement.view.navigation.terminals.Total",
                        detail = "Cement.view.navigation.terminals.view.View"
                    },
                    new
                    {
                        text = "Индикаторы",
                        master = "Cement.view.navigation.indicators.Total",
                        detail = "Cement.view.navigation.indicators.View"
                    },
                    new
                    {
                        text = "Геозоны",
                        master = "Cement.view.navigation.geozones.Total",
                        detail = "Cement.view.navigation.geozones.View"
                    }
                }
            };
            #endregion Навигация

            return new List<object> {
                personalData,
                corporateData,
                transport,
                products,
                clients,
                contracts,
                goods,
                cargo,
                accounting,
                office_work,
                //navigation
            };
        }
    }
}
