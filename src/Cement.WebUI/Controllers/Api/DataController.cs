﻿using System;
using System.Web.Http;
using Cement.Core.Services;
using Cement.Core.Services.EntityService;
using Cement.Core.Services.StoreParams;
using Cement.Protocol;
using Cement.WebUI.Extensions;
using Cement.WebUI.Framework;
using DataAccess;

namespace Cement.WebUI.Controllers.Api
{
    [Authorize]
    public class DataController<T, TP> : CementApiController 
        where T : IEntity
        where TP : class, IDataResult, new()
    {
        public IEntityService<T> EntityService { get; set; }

        [HttpGet, Route("getnum")]
        public virtual string GetNextNumber()
        {
            return Container.Resolve<IDocumentNumberProvider>().GetNewNumber(typeof (T));
        }

        [HttpGet, Route("get/{id}")]
        public virtual IDataResult Get(long id)
        {
            var entity = EntityService.Get(id);
            var entityProxy = AutoMapper.Mapper.DynamicMap<TP>(entity);
            return entityProxy;
        }

        [HttpGet, Route("details/null")] 
        public virtual object NullDetails()
        {
            return new {};
        }

        [HttpGet, Route("details/{id}")]
        public virtual object Details(long id)
        {
            var entity = EntityService.Get(id);
            var entityProxy = AutoMapper.Mapper.DynamicMap<TP>(entity);
            return entityProxy;
        }

        [HttpPost, Route("list")]
        public virtual IDataResult List()
        {
            var storeParams = Request.GetStoreParams();

            return ListInternal(storeParams);
        }
        
        [HttpPost, Route("delete/{id}")]
        public virtual JsResult Delete(long id)
        {
            try
            {
                DeleteInternal(id);
                return JsResult.Successful();
            }
            catch (Exception exc)
            {
                return new JsResult
                {
                    Msg = exc.Message,
                    Id = id,
                    Success = false
                };

                //throw;
            }
        }

        protected virtual void DeleteInternal(long id)
        {
            EntityService.Delete(id);
        }

        [HttpPost, Route("save")]
        public virtual JsResult Save()
        {
            var incomeProxy = Request.ToType<TP>();

            BeforeProxyMap(incomeProxy);

            var entity = MapProxyToEntity(incomeProxy);

            BeforeEntitySave(entity);

            EntityService.Save(entity);

            return new JsResult(entity.Id);
        }

        protected virtual T MapProxyToEntity(TP incomeProxy)
        {
            return AutoMapper.Mapper.DynamicMap<T>(incomeProxy);
        }

        protected virtual void BeforeProxyMap(TP proxy)
        {

        }

        protected virtual void BeforeEntitySave(T entity)
        {

        }

        protected virtual IDataResult ListInternal(StoreParams storeParams)
        {
            return new ListResult<T>();
        }
    }
}