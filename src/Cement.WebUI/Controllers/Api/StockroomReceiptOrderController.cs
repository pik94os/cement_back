﻿using System;
using System.Linq;
using System.Linq.Expressions;
using System.Web.Http;
using Cement.Core.Entities;
using Cement.Core.Entities.Stockroom;
using Cement.Core.Enums;
using Cement.Core.Exceptions;
using Cement.Core.Extensions;
using Cement.Core.Services;
using Cement.Core.Services.EntityService;
using Cement.Core.Services.StoreParams;
using Cement.Protocol;
using Cement.WebUI.Extensions;
using NHibernate.Linq;

namespace Cement.WebUI.Controllers.Api
{
    [RoutePrefix("api/stockreceiptorder")]
    public class StockroomReceiptOrderController : RoutedEntityDataController<StockroomReceiptOrderBase, StockroomReceiptOrderProxy, StockroomReceiptOrderRoute>
    {
        public IEntityService<AccountingInOutDocRoute> AccountingInOutDocRouteEntityService { get; set; }
        public IEntityService<AccountingOtherRoute> AccountingOtherRouteEntityService { get; set; }
        public IEntityService<StockroomReceiptOrderItemOuter> StockroomReceiptOrderItemOuterEntityService { get; set; }
        public IEntityService<StockroomReceiptOrderItemInnerMove> StockroomReceiptOrderItemInnerMoveEntityService { get; set; }
        public IEntityService<StockroomReceiptOrderItemInnerPosting> StockroomReceiptOrderItemInnerPostingEntityService { get; set; }
        public IWarehouseService WarehouseService { get; set; }

        protected override Expression<Func<StockroomReceiptOrderRoute, bool>> DocIdFilterExpression(long id)
        {
            return x => x.Document.Id == id;
        }

        protected override IQueryable<StockroomReceiptOrderRoute> PersonalizeQuery(IQueryable<StockroomReceiptOrderRoute> query)
        {
            return query.Where(x => x.Employee == UserPrincipal.Employee);
        }

        protected override IDataResult DocumentList(StoreParams storeParams, IQueryable<StockroomReceiptOrderRoute> routeQuery)
        {
            var kindDict = typeof(StockroomReceiptOrderType).AsDictionary();
            var typeDict = typeof(DocumentMovementType).AsDictionary();
            var stateDict = typeof (DocumentState).AsDictionary();

            var query = routeQuery
                .Select(x => new
                {
                    p_base_doc = x.Document.Id,
                    id = x.Id,
                    p_kind = x.Document.StockroomReceiptOrderType,
                    p_number = x.Document.No,
                    p_date = x.Document.Date,
                    p_product_display = (x.Document as StockroomReceiptOrderMovement).StockroomReceiptOrderItemMove.Product.Name,
                    p_measure_unit_display = (x.Document as StockroomReceiptOrderMovement).StockroomReceiptOrderItemMove.MeasureUnit.Name,
                    p_warehouse_display = (x.Document as StockroomReceiptOrderMovement).StockroomReceiptOrderItemMove.WarehouseFrom.Name,
                    //p_count_fact = x.Document.Amount,
                    p_count = (x.Document as StockroomReceiptOrderMovement).StockroomReceiptOrderItemMove.Amount,
                    p_count_fact = (x.Document as StockroomReceiptOrderMovement).StockroomReceiptOrderItemMove.Amount,
                    p_type = x.MovementType,
                    x.Document.DocumentState
                })
                .Filter(storeParams);

            var data = query
                .Order(storeParams)
                .Paging(storeParams)
                .Extend((row, x) =>
                {
                    row["p_kind_display"] = kindDict.Get((int)x.p_kind);
                    row["p_type"] = typeDict.Get((int)x.p_type);
                    row["p_form_kind"] = (int)x.p_kind;
                    row["p_state"] = stateDict.Get((int)x.DocumentState);
                })
                .Highlight(storeParams)
                .ToList();

            return new ObjectListResult
            {
                Data = data,
                Count = query.Count()
            };
        }

        protected override StockroomReceiptOrderBase MapProxyToEntity(StockroomReceiptOrderProxy incomeProxy)
        {
            switch (incomeProxy.StockroomReceiptOrderType)
            {
                case StockroomReceiptOrderType.Buy: return AutoMapper.Mapper.DynamicMap<StockroomReceiptOrderBuy>(incomeProxy);
                case StockroomReceiptOrderType.Movement: return AutoMapper.Mapper.DynamicMap<StockroomReceiptOrderMovement>(incomeProxy);
                case StockroomReceiptOrderType.Posting: return AutoMapper.Mapper.DynamicMap<StockroomReceiptOrderPosting>(incomeProxy);
                default:
                    throw new ValidationException(string.Empty);
            }
        }

        [HttpPost, Route("listsignedbuydata")]
        public IDataResult SignedBuyDataList()
        {
            var storeParams = Request.GetStoreParams();

            var data = AccountingInOutDocRouteEntityService.GetAll()
                .Where(x => x.ArchivedAt == null)
                .Where(x => x.DeletedAt == null)
                .Where(x => x.Employee == UserPrincipal.Employee)
                .Where(x => x.MovementType == DocumentMovementType.Incoming)
                .Where(x => x.Document.DocumentState == DocumentState.Signed)
                .Join(StockroomReceiptOrderItemOuterEntityService.GetAll(), 
                    x => x.Document.Id,
                    y => y.AccountingInOutDoc.Id, 
                    (a, b) => new
                    {
                        id = b.Id,
                        p_kind = a.Document.AccountingInOutDocType,
                        b.UsedAt,
                        p_number = a.Document.No,
                        p_date = a.Document.Date,
                        p_product_display = b.Product.Name,
                        p_measure_unit_display = b.MeasureUnit.ShortName,
                        p_count = b.Amount,
                        p_product_price = b.Price,
                        p_product_tax = b.Tax,
                        p_warehouse_display = b.Warehouse.Name,
                        p_warehouse_address = b.Warehouse.Address
                    })
                .Where(x => x.UsedAt == null)
                .Order(storeParams)
                .Extend((row, x) =>
                {
                    row["p_kind"] = x.p_kind.GetDisplayValue();
                    row["p_product_sum"] = x.p_count * x.p_product_price;
                })
                .Highlight(storeParams)
                .Distinct()
                .ToList();

            return new ObjectListResult
            {
                Data = data,
                Count = data.Count()
            };
        }

        [HttpPost, Route("listsignedmovementdata")]
        public IDataResult SignedMovementDataList()
        {
            return new ObjectListResult();
        }

        [HttpPost, Route("listsignedpostingdata")]
        public IDataResult SignedPostingDataList()
        {
            return new ObjectListResult();
        }
    }
}