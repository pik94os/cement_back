﻿using System;
using System.Linq;
using System.Web.Http;
using Cement.Core.Entities;
using Cement.Core.Enums;
using Cement.Core.Services.EntityService;
using Cement.Core.Services.StoreParams;
using Cement.Protocol;
using Cement.WebUI.Extensions;
using DataAccess;

namespace Cement.WebUI.Controllers.Api
{
    public class MovingDocumentDataController<T, TP, TM> : DataController<T, TP>
        where T : IEntity
        where TP : class, IDataResult, new()
        where TM : class, IEntity, IDocumentMovement
    {
        public IEntityService<TM> DocumentMovementEntityService { get; set; }

        [HttpPost, Route("archive/{id}")]
        public JsResult Archive(long id)
        {
            return ManageArchive(id, true);
        }

        [HttpPost, Route("unarchive/{id}")]
        public JsResult Unarchive(long id)
        {
            return ManageArchive(id, false);
        }

        [HttpPost, Route("deleteArchive/{id}")]
        public JsResult DeleteArchive(long id)
        {
            try
            {
                DocumentMovementEntityService.Delete(id);

                return JsResult.Successful("Успешно");
            }
            catch (Exception exc)
            {
                return new JsResult
                {
                    Msg = exc.Message,
                    Id = id,
                    Success = false
                };
            }
        }

        /// <summary>
        /// Управление архивированием
        /// </summary>
        protected virtual JsResult ManageArchive(long id, bool archivate, string successText = "Успешно")
        {
            try
            {
                var docMovement = DocumentMovementEntityService.Get(id);

                if (docMovement != null)
                {
                    docMovement.IsArchived = archivate;
                    DocumentMovementEntityService.Update(docMovement);
                }

                return JsResult.Successful(successText);
            }
            catch (Exception exc)
            {
                return new JsResult
                {
                    Msg = exc.Message,
                    Id = id,
                    Success = false
                };
            }
        }

        [HttpPost, Route("listincoming")]
        public IDataResult ListIncoming()
        {
            return DocumentList(Request.GetStoreParams(), PersonalizeQuery(GetIncomingQuery()));
        }

        [HttpPost, Route("listoutcoming")]
        public IDataResult ListOutcoming()
        {
            return DocumentList(Request.GetStoreParams(), PersonalizeQuery(GetOutcomingQuery()));
        }

        [HttpPost, Route("listarchive")]
        public IDataResult ListArchive()
        {
            return DocumentList(Request.GetStoreParams(), PersonalizeQuery(GetArchiveQuery()));
        }

        protected virtual IQueryable<TM> GetIncomingQuery()
        {
            return DocumentMovementEntityService.GetAll()
                .Where(x => !x.IsArchived)
                .Where(x => x.MovementType == DocumentMovementType.Incoming);
        }

        protected virtual IQueryable<TM> GetOutcomingQuery()
        {
            return DocumentMovementEntityService.GetAll()
                .Where(x => !x.IsArchived)
                .Where(x => x.MovementType == DocumentMovementType.Outcoming);
        }

        protected virtual IQueryable<TM> GetArchiveQuery()
        {
            return DocumentMovementEntityService.GetAll()
                .Where(x => x.IsArchived);
        }

        protected virtual IDataResult DocumentList(StoreParams storeParams, IQueryable<TM> messageMovementQuery)
        {
            return new ObjectListResult { Data = null, Count = 0 };
        }

        protected virtual IQueryable<TM> PersonalizeQuery(IQueryable<TM> query)
        {
            return query;
        }
    }
}