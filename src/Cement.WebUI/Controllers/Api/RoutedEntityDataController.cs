﻿using System;
using System.Linq;
using System.Linq.Expressions;
using System.Web.Http;
using Castle.Core.Internal;
using Cement.Core.Entities;
using Cement.Core.Enums;
using Cement.Core.Extensions;
using Cement.Core.Services.EntityService;
using Cement.Core.Services.StoreParams;
using Cement.Protocol;
using Cement.Protocol.JsonConverters;
using Cement.WebUI.Extensions;
using DataAccess;
using DataAccess.SessionProvider;
using NHibernate.Mapping.ByCode;

namespace Cement.WebUI.Controllers.Api
{
    public class RoutedEntityDataController<T, TP, TM> : DataController<T, TP>
        where T : BaseEntity, IDocument
        where TP : class, IDataResult, new()
        where TM : DocumentRoute<T>, IEntity, IDocumentRoute
    {
        public IEntityService<TM> DocumentRouteEntityService { get; set; }

        [HttpPost, Route("archive/{id}")]
        public JsResult Archive(long id)
        {
            return ManageArchive(id, true);
        }

        [HttpPost, Route("unarchive/{id}")]
        public JsResult Unarchive(long id)
        {
            return ManageArchive(id, false);
        }

        protected override void DeleteInternal(long id)
        {
            var docRoute = DocumentRouteEntityService.Get(id);

            if (docRoute != null)
            {
                docRoute.DeletedAt = DateTime.Now;
                DocumentRouteEntityService.Update(docRoute);
            }
        }

        /// <summary>
        /// Управление архивированием
        /// </summary>
        protected virtual JsResult ManageArchive(long id, bool archivate, string successText = "Успешно")
        {
            try
            {
                var docMovement = DocumentRouteEntityService.Get(id);

                if (docMovement != null)
                {
                    docMovement.ArchivedAt = archivate ? DateTime.Now : (DateTime?)null;
                    DocumentRouteEntityService.Update(docMovement);
                }

                return JsResult.Successful(successText);
            }
            catch (Exception exc)
            {
                return new JsResult
                {
                    Msg = exc.Message,
                    Id = id,
                    Success = false
                };
            }
        }

        [HttpPost, Route("listincoming")]
        public IDataResult ListIncoming()
        {
            return DocumentList(Request.GetStoreParams(), PersonalizeQuery(GetIncomingQuery()));
        }

        [HttpPost, Route("listoutcoming")]
        public IDataResult ListOutcoming()
        {
            return DocumentList(Request.GetStoreParams(), PersonalizeQuery(GetOutcomingQuery()));
        }

        [HttpPost, Route("listarchive")]
        public IDataResult ListArchive()
        {
            return DocumentList(Request.GetStoreParams(), PersonalizeQuery(GetArchiveQuery()));
        }

        [HttpPost, Route("listroutes")]
        public IDataResult ListRoute()
        {
            var processResultDict = typeof(DocumentProcessingResult).AsDictionary();

            var storeParams = Request.GetStoreParams();

            var docIdStr = storeParams.FilterParams
                .Where(x => x.PropertyName == "p_base_doc")
                .Select(x => x.Value)
                .FirstOrDefault();

            var docId = docIdStr.SafeToString().ToLong();

            if (docId == 0)
            {
                return new ObjectListResult { Count = 0 };
            }

            var res = DocumentRouteEntityService.GetAll()
                .Where(DocIdFilterExpression(docId))
                .Select(x => new
                {
                    id = x.Id,
                    p_org_name = x.Employee.Organization.Name,
                    p_name = x.Employee.Name,
                    p_position = x.Employee.Position.Name,
                    p_department = x.Employee.Department.Name,
                    p_date_in = x.DateIn,
                    p_date_out = x.DateOut,
                    p_comment = x.Comment,
                    p_status = x.ProcessingResult,
                })
                .OrderBy(x => x.id)
                //.Order(storeParams) Введение в заблуждение сбиванием порядка в маршруте
                .Extend((row, x) =>
                {
                    row["p_comment"] = x.p_comment;
                    row["p_status"] = processResultDict.Get((int)x.p_status);
                    row["p_date_in"] = new TimeDate(x.p_date_in);
                    row["p_date_out"] = new TimeDate(x.p_date_out);
                    row["p_time_spent"] = CementExtension.TimeSpent(x.p_date_out, x.p_date_in);
                })
                .ToList();

            return new ObjectListResult
            {
                Count = res.Count,
                Data = res
            };
        }

        protected virtual Expression<Func<TM, bool>> DocIdFilterExpression(long id)
        {
            return x => false;
        }

        protected override IDataResult ListInternal(StoreParams storeParams)
        {
            return DocumentList(storeParams, PersonalizeQuery(GetNewListQuery()));
        }

        protected virtual IQueryable<TM> GetNewListQuery()
        {
            return DocumentRouteEntityService.GetAll()
                .Where(x => x.ArchivedAt == null)
                .Where(x => x.DeletedAt == null)
                .Where(x => x.Stage == 0)
                .Where(x => x.ProcessingResult == DocumentProcessingResult.None)
                .Where(x => x.MovementType == DocumentMovementType.Outcoming);
        }

        protected virtual IQueryable<TM> GetIncomingQuery()
        {
            return DocumentRouteEntityService.GetAll()
                .Where(x => x.ArchivedAt == null)
                .Where(x => x.DeletedAt == null)
                .Where(x => x.MovementType == DocumentMovementType.Incoming);
        }

        protected virtual IQueryable<TM> GetOutcomingQuery()
        {
            return DocumentRouteEntityService.GetAll()
                .Where(x => x.ArchivedAt == null)
                .Where(x => x.DeletedAt == null)
                .Where(x => x.Stage > 0 || x.ProcessingResult != DocumentProcessingResult.None)
                .Where(x => x.MovementType == DocumentMovementType.Outcoming);
        }

        protected virtual IQueryable<TM> GetArchiveQuery()
        {
            return DocumentRouteEntityService.GetAll()
                .Where(x => x.ArchivedAt != null)
                .Where(x => x.DeletedAt == null);
        }

        protected virtual IDataResult DocumentList(StoreParams storeParams, IQueryable<TM> messageMovementQuery)
        {
            return new ObjectListResult { Data = null, Count = 0 };
        }

        protected virtual IQueryable<TM> PersonalizeQuery(IQueryable<TM> query)
        {
            return query;
        }
    }
}