﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using Cement.Core;
using Cement.Core.Auth;
using Cement.Core.Auth.Impl;
using Cement.Core.Entities;
using Cement.Core.Enums;
using Cement.Core.Extensions;
using Cement.Core.Services;
using Cement.Core.Services.EntityService;
using Cement.Protocol;
using Cement.WebUI.Extensions;
using Cement.WebUI.Framework;
using NHibernate;

namespace Cement.WebUI.Controllers.Api
{
    [Authorize]
    [RoutePrefix("api/organization")]
    public class OrganizationController : CementApiController
    {
        public IEntityService<ClientGroup> ClientGroupEntityService { get; set; }

        public IPasswordHashProvider PasswordHashProvider { get; set; }

        public IEntityService<Organization> OrganizationEntityService { get; set; }

        public IEntityService<User> UserEntityService { get; set; }

        public IEntityService<PersonalClient> PersonalClientEntityService { get; set; }

        [HttpPost, Route("grouptree")]
        public List<NamedFirmTreeElement> OrganizationTree(TreeRequestStringNode request)
        {
            if (request == null || request.Node == "0")
            {
                return ClientGroupEntityService.GetAll()
                    .OrganizationFilter(UserPrincipal)
                    .Where(x => x.Parent == null)
                    .Select(x => new NamedFirmTreeElement
                    {
                        Id = string.Format("{0}{1}0", x.Id, Constants.DocBranchIdDelimeter),
                        Name = x.Name,
                        Leaf = false
                    })
                    .ToList();
            }

            var namedTreeElementList = new List<NamedFirmTreeElement>();

            var keyParts = request.Node.Split(Constants.DocBranchIdDelimeter).ToList();

            if (keyParts.Count != 2)
            {
                return namedTreeElementList;
            }
            
            var parentId = keyParts.First().ToLong();
            var treeLevel = keyParts.Last().ToLong();

            if (treeLevel > 0)
            {
                return PersonalClientEntityService.GetAll()
                    .OrganizationFilter(UserPrincipal)
                    .Where(x => x.Subgroup.Id == parentId)
                    .Join(OrganizationEntityService.GetAll(),
                        x => x.Client.Id,
                        y => y.Client.Id,
                        (x, y) => new NamedFirmTreeElement
                        {
                            Name = y.Name,
                            Id = y.Id,
                            Leaf = true,
                            Address = y.Client.Address
                        })
                    .ToList();

            }

            return ClientGroupEntityService.GetAll()
                .OrganizationFilter(UserPrincipal)
                .Where(x => x.Parent.Id == parentId)
                .Select(x => new NamedFirmTreeElement
                {
                    Id = string.Format("{0}{1}1", x.Id, Constants.DocBranchIdDelimeter),
                    Name = x.Name,
                    Leaf = false
                })
                .ToList();
        }

        [HttpPost, Route("list")]
        public IDataResult OrganizationList()
        {
            var storeParams = Request.GetStoreParams();

            var query = OrganizationEntityService.GetAll()
                .Select(x => new
                {
                    id = x.Id,
                    p_name = x.Name,
                    p_inn = x.Client.Inn,
                })
                .Filter(storeParams);

            var count = query.Count();

            var data = query
                .Order(storeParams)
                .Paging(storeParams)
                .Highlight(storeParams)
                .ToList();

            return new ObjectListResult
            {
                Data = data,
                Count = count
            };
        }

        [HttpGet, Route("get/{id}")]
        public IDataResult Get(long id)
        {
            var org = UserEntityService.GetAll()
                .Where(x => x.Organization.Id == id)
                .Where(x => x.Role == UserRole.Admin)
                .Select(x => new
                {
                    id = x.Organization.Id,
                    p_client = x.Organization.Client.Id,
                    p_client_display = x.Organization.Name,
                    p_email = x.Email,
                    p_name = x.Organization.Client.Name,
                    p_full_name = x.Organization.Client.FullName,
                    p_ownership = x.Organization.Client.Ownership.Name,
                    p_brand = x.Organization.Client.Brand,
                    p_ogrn = x.Organization.Client.Ogrn,
                    p_inn = x.Organization.Client.Inn,
                    p_kpp = x.Organization.Client.Kpp,
                    p_okpo = x.Organization.Client.Okpo,
                    p_okved = x.Organization.Client.Okved,
                    p_okato = x.Organization.Client.Okato,
                    p_okonh = x.Organization.Client.Okonh,
                    p_phone = x.Organization.Client.Phone,
                    p_fax = x.Organization.Client.Fax,
                    p_web = x.Organization.Client.Web,
                    p_law_index = x.Organization.Client.JurAddressIndex,
                    p_law_country = x.Organization.Client.JurAddressCountry,
                    p_law_region = x.Organization.Client.JurAddressRegion,
                    p_law_district = x.Organization.Client.JurAddressArea,
                    p_law_city = x.Organization.Client.JurAddressLocality,
                    p_law_street = x.Organization.Client.JurAddressStreet,
                    p_law_house = x.Organization.Client.JurAddressHouse,
                    p_law_housepart = x.Organization.Client.JurAddressHousing,
                    p_law_building = x.Organization.Client.JurAddressBuilding,
                    p_law_office = x.Organization.Client.JurAddressOffice,
                    p_fact_index = x.Organization.Client.FactAddressIndex,
                    p_fact_country = x.Organization.Client.FactAddressCountry,
                    p_fact_region = x.Organization.Client.FactAddressRegion,
                    p_fact_district = x.Organization.Client.FactAddressArea,
                    p_fact_city = x.Organization.Client.FactAddressLocality,
                    p_fact_street = x.Organization.Client.FactAddressStreet,
                    p_fact_house = x.Organization.Client.FactAddressHouse,
                    p_fact_housepart = x.Organization.Client.FactAddressHousing,
                    p_fact_building = x.Organization.Client.FactAddressBuilding,
                    p_fact_office = x.Organization.Client.FactAddressOffice,
                    /*
                    p_ogrn_file //'ОГРН'
                    p_inn_file//'ИНН'
                    p_charter//'Устав'
                    p_firm_code//'Код предприятия'
                    p_station//'Станция'
                    p_station_code//'Код станции'
                    p_road//'Дорога*/
            
                })
                .FirstOrDefault();

            if (org == null)
            {
                return new ObjectResult();
            }

            var result = org.Extend((row, x) => { });

            return new ObjectResult(result);
        }

        [HttpPost, Route("save")]
        public JsResult Save()
        {
            var incomeProxy = Request.ToType<OrganizationProxy>();
            var entity = AutoMapper.Mapper.DynamicMap<Organization>(incomeProxy);

            var user = UserProcess(entity, incomeProxy.Email, incomeProxy.Pass);

            using (var transaction = Container.Resolve<ITransaction>())
            {
                try
                {
                    OrganizationEntityService.Save(entity);
                    UserEntityService.Save(user);

                    transaction.Commit();
                }
                catch (Exception exception)
                {
                    transaction.Rollback();
                    throw;
                }
            }

            return new JsResult(entity.Id);
        }

        private User UserProcess(Organization organization, string email, string password)
        {
            var user = UserEntityService.GetAll()
                .Where(x => x.Organization.Id == organization.Client.Id)
                .FirstOrDefault(x => x.Role == UserRole.Admin)
                ?? new User(UserRole.Admin);

            user.Email = email;
            user.Login = email;
            user.Name = email;
            user.Organization = organization;

            if (user.Id > 0 && string.IsNullOrEmpty(password))
            {
                return user;
            }

            user.SetNewPassword(PasswordHashProvider, password);

            return user;
        }
    }
}