﻿using System.Linq;
using System.Web.Http;
using Cement.Core.Entities;
using Cement.Core.Extensions;
using Cement.Core.Services.StoreParams;
using Cement.Protocol;

namespace Cement.WebUI.Controllers.Api
{
    [RoutePrefix("api/vehiclecol")]
    public class VehicleColumnController : DataController<VehicleColumn, DictionaryObject>
    {
        protected override IDataResult ListInternal(StoreParams storeParams)
        {
            // тут подгрузка в combobox, storeParams не используем

            var data = EntityService.GetAll()
                .OrganizationFilter(UserPrincipal)
                .Select(x => new
                {
                    id = x.Id,
                    p_name = x.Name
                })
                .OrderBy(x => x.p_name)
                .ToList();

            var count = data.Count();

            return new ObjectListResult
            {
                Data = data,
                Count = count
            };
        }
    }
}