﻿using System.Linq;
using System.Web.Http;
using Cement.Core.Entities;
using Cement.Core.Extensions;
using Cement.Core.Services.StoreParams;
using Cement.Protocol;
using Cement.Protocol.JsonConverters;

namespace Cement.WebUI.Controllers.Api
{
    [RoutePrefix("api/vehicleinsurance")]
    public class VehicleInsuranceController : DataController<VehicleInsurance, VehicleInsuranceProxy>
    {
        protected override IDataResult ListInternal(StoreParams storeParams)
        {
            var query = EntityService.GetAll()
                .OrganizationFilter(x => x.Vehicle.Organization, UserPrincipal)
                .Select(x => new
                {
                    id = x.Id,
                    p_name = x.Name,
                    p_address = x.InsuranceCompany.Address,
                    p_contact = x.ContactPerson,
                    p_company = (long?) x.InsuranceCompany.Id,
                    p_company_display = x.InsuranceCompany.Name,
                    p_transport_unit = (long?) x.Vehicle.Id,
                    p_transport_unit_display = x.Vehicle.Name,
                    p_date_start = x.DateStart,
                    p_date_end = x.DateEnd,
                    p_number = x.No,
                    p_phone = x.Phone,
                    p_sum = x.InsuranceSum,
                    p_document_file = new Downloadable(x.Document.Id)
                })
                .Filter(storeParams);
            
            var count = query.Count();

            var data = query
                .Order(storeParams)
                .Paging(storeParams)
                .Highlight(storeParams)
                .ToList();

            return new ObjectListResult
            {
                Data = data,
                Count = count
            };
        }
    }
}