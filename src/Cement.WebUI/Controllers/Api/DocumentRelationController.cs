﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using Cement.Core.Entities;
using Cement.Core.Enums;
using Cement.Core.Extensions;
using Cement.Core.Services.EntityService;
using Cement.Protocol;
using Cement.WebUI.Extensions;
using Cement.WebUI.Framework;

namespace Cement.WebUI.Controllers.Api
{
    [Authorize]
    [RoutePrefix("api/docrelation")]
    public class DocumentRelationController : CementApiController
    {
        private static readonly Dictionary<int, string> DocumentTypeDict;

        static DocumentRelationController()
        {
            DocumentTypeDict = typeof (DocumentType).AsDictionary();
        }

        public IEntityService<DocumentRelation> DocumentRelationEntityService { get; set; }
        public IEntityService<IncomingDocument> IncomingDocumentEntityService { get; set; }
        public IEntityService<OutcomingDocument> OutcomingDocumentEntityService { get; set; }

        [HttpPost, Route("internalincoming")]
        public IDataResult ListIncomingFromInternalDocument()
        {
            return ListInternal(DocumentType.InternalDocument, DocumentMovementType.Incoming);
        }

        [HttpPost, Route("internaloutcoming")]
        public IDataResult ListOutcomingFromInternalDocument()
        {
            return ListInternal(DocumentType.InternalDocument, DocumentMovementType.Outcoming);
        }

        [HttpPost, Route("correspincoming")]
        public IDataResult ListIncomingFromCorrespondence()
        {
            return ListInternal(DocumentType.Correspondence, DocumentMovementType.Incoming);
        }

        [HttpPost, Route("correspoutcoming")]
        public IDataResult ListOutcomingFromCorrespondence()
        {
            return ListInternal(DocumentType.Correspondence, DocumentMovementType.Outcoming);
        }

        [HttpPost, Route("messageincoming")]
        public IDataResult ListIncomingFromMessage()
        {
            return ListInternal(DocumentType.Message, DocumentMovementType.Incoming);
        }

        [HttpPost, Route("messageoutcoming")]
        public IDataResult ListOutcomingFromMessage()
        {
            return ListInternal(DocumentType.Message, DocumentMovementType.Outcoming);
        }

        [HttpPost, Route("list")]
        public IDataResult List()
        {
            var storeParams = Request.GetStoreParams();

            var docIdStr = storeParams.FilterParams
                .Where(x => x.PropertyName == "p_base_doc")
                .Select(x => x.Value)
                .FirstOrDefault();

            var docId = docIdStr.SafeToString().ToLong();

            if (docId == 0)
            {
                return new ObjectListResult { Count = 0 };
            }

            var data = DocumentRelationEntityService.GetAll()
                .Where(x => x.BaseDocumentId == docId)
                .Select(x => new
                {
                    id = x.JoinedDocId,
                    documentType = x.JoinedDocumentType,
                    p_number = x.No,
                    p_date = x.Date,
                    p_status = x.State,
                    p_name = x.Name
                })
                .AsEnumerable()
                .Extend((row, x) =>
                {
                    row["p_kind"] = DocumentTypeDict.Get((int)x.documentType);
                })
                .ToList();

            return new ObjectListResult
            {
                Data = data,
                Count = data.Count
            };
        }

        protected IDataResult ListInternal(DocumentType documentType, DocumentMovementType movementType)
        {
            var storeParams = Request.GetStoreParams();

            var docIdStr = storeParams.FilterParams
                .Where(x => x.PropertyName == "p_base_doc")
                .Select(x => x.Value)
                .FirstOrDefault();

            var docId = docIdStr.SafeToString().ToLong();

            if (docId == 0)
            {
                return new ObjectListResult { Count = 0 };
            }

            return movementType == DocumentMovementType.Incoming 
                ? ListInternal(documentType, IncomingDocumentEntityService.GetAll(), docId) 
                : ListInternal(documentType, OutcomingDocumentEntityService.GetAll(), docId);
        }

        protected IDataResult ListInternal<T>(DocumentType documentType, IQueryable<T> query, long docId) where T : DocumentMovement
        {
            var employeeId = UserPrincipal.Employee != null ? UserPrincipal.Employee.Id : (long?)null;
            var organizationId = UserPrincipal.Organization != null ? UserPrincipal.Organization.Id : (long?)null;

            var data = DocumentRelationEntityService.GetAll()
                .Join(query,
                    x => x.JoinedDocId,
                    y => y.DocumentId,
                    (a, b) => new
                    {
                        a.BaseDocumentId,
                        id = a.JoinedDocId,
                        a.BaseDocumentType,
                        documentType = a.JoinedDocumentType,
                        a.Date,
                        a.Name,
                        a.No,
                        a.State,
                        b.IsArchived,
                        b.EmployeeId,
                        b.OrganizationId
                    })
                .Where(x => x.IsArchived == false)
                .Where(x => x.EmployeeId == null || x.EmployeeId == employeeId)
                .Where(x => x.OrganizationId == null || x.OrganizationId == organizationId)
                .Where(x => x.BaseDocumentId == docId)
                .Where(x => x.BaseDocumentType == documentType) // ???
                .Select(x => new
                {
                    x.id,
                    x.documentType,
                    p_number = x.No,
                    p_date = x.Date,
                    p_status = x.State,
                    p_name = x.Name
                })
                .AsEnumerable()
                .Extend((row, x) =>
                {
                    row["p_kind"] = DocumentTypeDict.Get((int)x.documentType);
                })
                .ToList();

            return new ObjectListResult
            {
                Data = data,
                Count = data.Count
            };
        }
    }
}