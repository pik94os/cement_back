﻿using System.Linq;
using System.Web.Http;
using Cement.Core.Entities;
using Cement.Core.Extensions;
using Cement.Core.Services.StoreParams;
using Cement.Protocol;
using Cement.Protocol.JsonConverters;

namespace Cement.WebUI.Controllers.Api
{
    [RoutePrefix("api/vehiclefuel")]
    public class VehicleFuelController : DataController<VehicleFuel, VehicleFuelProxy>
    {
        protected override IDataResult ListInternal(StoreParams storeParams)
        {
            var query = EntityService.GetAll()
                .OrganizationFilter(x => x.Vehicle.Organization, UserPrincipal)
                .Select(x => new
                {			
                   	id = x.Id,
			        p_transport_unit = x.Vehicle.Id,
			        p_transport_unit_display = x.Vehicle.Name,
			        p_date = x.Date,
			        p_name = x.Name,
			        //p_odometer = Хз
			        p_station = (long?)x.FuelStation.Id,
                    p_station_display = x.FuelStation.Name,
                    p_address = x.FuelStation.Address,
			        p_cost = x.Cost,
			        p_unit = (long?)x.MeasureUnit.Id,
                    p_unit_display = x.MeasureUnit.Name,
			        p_count = x.Amount,
			        p_driver = (long?) x.Driver.Id,
			        p_driver_display = x.Driver.Name,
			        p_document = x.DocName,
                    p_document_file = new Downloadable(x.DocFile.Id)
                })
                .Filter(storeParams);

            var count = query.Count();

            var data = query
                .Order(storeParams)
                .Paging(storeParams)
                .Highlight(storeParams)
                .ToList();

            return new ObjectListResult
            {
                Data = data,
                Count = count
            };
        }
    }
}