﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using Cement.Core.Entities.Stockroom;
using Cement.Core.Extensions;
using Cement.Core.Services.EntityService;
using Cement.Core.Services.StoreParams;
using Cement.Protocol;
using Cement.WebUI.Extensions;
using Cement.WebUI.Framework;

namespace Cement.WebUI.Controllers.Api
{
    [RoutePrefix("api/stockroomproduct")]
    public class StockroomProductController : CementApiController
    {
        public IEntityService<StockroomProduct> StockroomProductEntityService { get; set; }

        [HttpPost, Route("list")]
        public virtual IDataResult List()
        {
            var storeParams = Request.GetStoreParams();

            return ListInternal(storeParams);
        }

        protected virtual IDataResult ListInternal(StoreParams storeParams)
        {
            var mainQuery = StockroomProductEntityService.GetAll();

            if (storeParams.Data.ContainsKey("warehouseId"))
            {
                var warehouseId = storeParams.Data.Get("warehouseId").ToLong();
                mainQuery = mainQuery.Where(x => x.Warehouse.Id == warehouseId);
            }
            else
            {
                mainQuery = mainQuery.WarehouseFilter(x => x.Warehouse, UserPrincipal);
            }

            var query = mainQuery
                .Select(x => new
                {
                    x.Id,
                    x.Product.Name,
                    UnitName = x.MeasureUnit.Name
                })
                .Filter(storeParams);

            var data = query
                .Order(storeParams)
                .Paging(storeParams)
                .ToList();
            
            return new ObjectListResult
            {
                Count = query.Count(),
                Data = data
            };
        }

        [HttpPost, Route("inventory_list")]
        public virtual IDataResult InventoryList()
        {
            var storeParams = Request.GetStoreParams();

            var warehouseId = storeParams.GetDataPropertyByName("p_warehouse").ToLong();

            var data = StockroomProductEntityService.GetAll()
                .Where(x => x.Warehouse.Id == warehouseId)
                .Select(x => new
                {
                    id = x.Id,
                    p_warehouse = x.Warehouse.Id,
                    p_group_display = x.Product.Group.Name,
                    p_subgroup_display = x.Product.SubGroup.Name,
                    p_trade_mark = x.Product.Brand,
                    p_manufacturer = x.Product.Manufacturer.Name,
                    p_count = x.Amount,
                    p_name = x.Product.Name,
                    p_unit = x.MeasureUnit.Name,
                    p_count_fact = x.Amount
                })
                .ToList();

            return new ObjectListResult
            {
                Count = data.Count(),
                Data = data
            };
        }
    }
}