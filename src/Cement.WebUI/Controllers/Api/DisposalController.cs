﻿using System.Linq;
using System.Web.Http;
using Cement.Core.Entities;
using Cement.Core.Enums;
using Cement.Core.Extensions;
using Cement.Core.Services;
using Cement.Core.Services.EntityService;
using Cement.Core.Services.StoreParams;
using Cement.Protocol;
using Cement.Protocol.JsonConverters;
using Cement.WebUI.Extensions;

namespace Cement.WebUI.Controllers.Api
{
    [RoutePrefix("api/disposal")]
    public class DisposalController : MovingDocumentDataController<InternalDocument, InternalDocumentProxy, InternalDocumentMovement>
    {
        public IEntityService<DisposalRecipient> DisposalRecipientEntityService { get; set; }

        public IDocumentService DocumentService { get; set; }

        public IEntityService<InternalDocumentRoute> InternalDocumentRouteEntityService { get; set; }

        protected override IDataResult DocumentList(StoreParams storeParams, IQueryable<InternalDocumentMovement> internalDocumentQuery)
        {
            var kindDict = typeof(InternalDocumentType).AsDictionary();
            var moveTypeDict = typeof(DocumentMovementType).AsDictionary();

            var query = internalDocumentQuery
                .Select(x => new
                {
                    p_base_doc = x.Document.Id,
                    id = x.Id,
                    p_kind = x.Document.InternalDocumentType,
                    p_number = x.Document.No,
                    p_date = x.Document.Date,
                    p_up_date = x.Document.ReviewBefore,
                    p_deadline = x.Document.ReviewBefore,
                    p_author = (long?)x.Document.Author.Id,
                    p_author_display = x.Document.Author.Name,
                    p_for_sign = (long?)x.Document.Signer.Id,
                    p_for_sign_display = x.Document.Signer.Name,
                    p_theme = x.Document.Subject,
                    p_importance = x.Document.Importance,
                    p_security = x.Document.Secrecy,
                    p_content = x.Document.Content,
                    p_nomenklature = (long?)x.Document.Nomenclature.Id,
                    p_nomenklature_display = x.Document.Nomenclature.CaseTitle,
                    p_nom_index = x.Document.Nomenclature.CaseIndex,
                    p_nom_title = x.Document.Nomenclature.CaseTitle,
                    p_nom_count = x.Document.Nomenclature.StorageUnitsCount,
                    p_nom_deadline = x.Document.Nomenclature.StorageLifeAndArticleNumbers,
                    p_nom_comment = x.Document.Nomenclature.Description,
                    p_nom_year = x.Document.Nomenclature.Year,
                    p_author_name = x.Document.Author.Name,
                    p_author_department = x.Document.Author.Department.Name,
                    p_author_position = x.Document.Author.Position.Name,
                    p_sign_name = x.Document.Signer.Name,
                    p_sign_department = x.Document.Signer.Department.Name,
                    p_sign_position = x.Document.Signer.Position.Name,
                    p_task_display = x.Document.DocumentState,
                    x.MovementType
                })
                .Filter(storeParams);

            var count = query.Count();

            var data = query
                .Order(storeParams)
                .Paging(storeParams)
                .Extend((row, x) =>
                {
                    row["p_kind_display"] = kindDict.Get((int)x.p_kind);
                    row["p_content"] = x.p_content.GetString();
                    row["p_sign_for"] = x.p_for_sign;
                    row["p_sign_for_display"] = x.p_for_sign_display;
                    row["p_type_display"] = moveTypeDict.Get((int)x.MovementType);
                    row["p_task_display"] = x.p_task_display.GetDisplayValue();
                })
                .Highlight(storeParams)
                .ToList();

            return new ObjectListResult
            {
                Data = data,
                Count = count
            };
        }

        protected override IQueryable<InternalDocumentMovement> GetIncomingQuery()
        {
            return base.GetIncomingQuery()
                .Where(x => x.Document.InternalDocumentType == InternalDocumentType.Disposal)
                .Where(x => x.Employee == UserPrincipal.Employee);
        }

        protected override IQueryable<InternalDocumentMovement> GetOutcomingQuery()
        {
            return base.GetOutcomingQuery()
                .Where(x => x.Document.InternalDocumentType == InternalDocumentType.Disposal)
                .Where(x => x.Employee == UserPrincipal.Employee);
        }

        protected override IQueryable<InternalDocumentMovement> GetArchiveQuery()
        {
            return base.GetArchiveQuery()
                .Where(x => x.Document.InternalDocumentType == InternalDocumentType.Disposal)
                .Where(x => x.Employee == UserPrincipal.Employee);
        }
        
        protected override IDataResult ListInternal(StoreParams storeParams)
        {
            var kindDict = typeof (InternalDocumentType).AsDictionary();

            var query = EntityService.GetAll()
                .Where(x => x.DocumentState == DocumentState.Draft)
                .Where(x => x.InternalDocumentType == InternalDocumentType.Disposal)
                .Where(x => x.Creator == UserPrincipal.Employee)
                .Select(x => new
                {
                    p_base_doc = x.Id,
                    id = x.Id,
                    p_kind = x.InternalDocumentType,
                    p_number = x.No,
                    p_date = x.Date,
                    p_up_date = x.ReviewBefore,
                    p_deadline = x.ReviewBefore,
                    p_author = (long?)x.Author.Id,
                    p_author_display = x.Author.Name,
                    p_for_sign = (long?) x.Signer.Id,
                    p_for_sign_display = x.Signer.Name,
                    p_theme = x.Subject,
                    p_importance = x.Importance,
                    p_security = x.Secrecy,
                    p_content = x.Content,
                    p_nomenklature = (long?)x.Nomenclature.Id,
                    p_nomenklature_display = x.Nomenclature.CaseTitle,
                    p_nom_index = x.Nomenclature.CaseIndex,
                    p_nom_title = x.Nomenclature.CaseTitle,
                    p_nom_count = x.Nomenclature.StorageUnitsCount,
                    p_nom_deadline = x.Nomenclature.StorageLifeAndArticleNumbers,
                    p_nom_comment = x.Nomenclature.Description,
                    p_nom_year = x.Nomenclature.Year,
                    p_author_name = x.Author.Name,
                    p_author_department = x.Author.Department.Name,
                    p_author_position = x.Author.Position.Name,
                    p_sign_name = x.Signer.Name,
                    p_sign_department = x.Signer.Department.Name,
                    p_sign_position = x.Signer.Position.Name,
                    p_task_display = x.DocumentState
                })
                .Filter(storeParams);
            
            var count = query.Count();

            var queryData = query
                .Order(storeParams)
                .Paging(storeParams)
                .ToList();

            var documentIds = queryData.Select(x => x.id).ToList();

            var relatedDocuments = DocumentService.GetRelatedDocumentsDict(documentIds);
            
            var recipientsDict = DisposalRecipientEntityService.GetAll()
               .Where(x => documentIds.Contains(x.Disposal.Id))
               .Select(x => new
               {
                   x.Recipient.Name,
                   x.Recipient.Id,
                   DisposalId = x.Disposal.Id
               })
               .AsEnumerable()
               .GroupBy(x => x.DisposalId)
               .ToDictionary(x => x.Key, x =>
               {
                   var recipients = new
                   {
                       Ids = x.Select(y => y.Id).ToList(),
                       Display = string.Join(", ", x.Select(y => y.Name))
                   };

                   return recipients;
               });

            var data = queryData
                .Extend((row, x) =>
                {
                    row["p_kind_display"] = kindDict.Get((int)x.p_kind);
                    row["p_content"] = x.p_content.GetString();
                    row["p_sign_for"] = x.p_for_sign;
                    row["p_sign_for_display"] = x.p_for_sign_display;
                    row["p_task_display"] = x.p_task_display.GetDisplayValue();

                    var relatedDocs = relatedDocuments.Get(x.id);

                    if (relatedDocs != null)
                    {
                        row["p_documents"] = relatedDocs.Ids;
                        row["p_documents_display"] = relatedDocs.Display;
                    }

                    var recipients = recipientsDict.Get(x.id);

                    if (recipients != null)
                    {
                        row["p_employees"] = recipients.Ids;
                        row["p_employees_display"] = recipients.Display;
                    }
                })
                .Highlight(storeParams)
                .ToList();

            return new ObjectListResult
            {
                Data = data,
                Count = count
            };
        }

        protected override void BeforeEntitySave(InternalDocument entity)
        {
            base.BeforeEntitySave(entity);

            entity.InternalDocumentType = InternalDocumentType.Disposal;
        }

        [HttpPost, Route("listroutes")]
        public IDataResult ListRoutes()
        {
            var processResultDict = typeof(DocumentProcessingResult).AsDictionary();

            var storeParams = Request.GetStoreParams();

            var docIdStr = storeParams.FilterParams
                .Where(x => x.PropertyName == "p_base_doc")
                .Select(x => x.Value)
                .FirstOrDefault();

            var docId = docIdStr.SafeToString().ToLong();

            if (docId == 0)
            {
                return new ObjectListResult { Count = 0 };
            }

            var res = InternalDocumentRouteEntityService.GetAll()
                .Where(x => x.Document.Id == docId)
                .Select(x => new
                {
                    id = x.Id,
                    p_name = x.Employee.Name,
                    p_position = x.Employee.Position.Name,
                    p_department = x.Employee.Department.Name,
                    p_date_in = x.DateIn,
                    p_date_out = x.DateOut,
                    p_comment = x.Comment,
                    p_status = x.ProcessingResult,
                })
                .OrderBy(x => x.id)
                //.Order(storeParams) Введение в заблуждение сбиванием порядка в маршруте
                .Extend((row, x) =>
                {
                    row["p_comment"] = x.p_comment.GetString();
                    row["p_status"] = processResultDict.Get((int)x.p_status);
                    row["p_date_in"] = new TimeDate(x.p_date_in);
                    row["p_date_out"] = new TimeDate(x.p_date_out);
                    row["p_time_spent"] = CementExtension.TimeSpent(x.p_date_out, x.p_date_in);
                })
                .ToList();

            return new ObjectListResult
            {
                Count = res.Count,
                Data = res
            };

        }
    }
}