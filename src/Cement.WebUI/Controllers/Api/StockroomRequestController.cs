﻿using System;
using System.Linq;
using System.Linq.Expressions;
using System.Net.Http;
using System.Web.Http;
using Cement.Core.Entities;
using Cement.Core.Entities.Stockroom;
using Cement.Core.Enums;
using Cement.Core.Exceptions;
using Cement.Core.Extensions;
using Cement.Core.Services;
using Cement.Core.Services.EntityService;
using Cement.Core.Services.StoreParams;
using Cement.Protocol;
using Cement.WebUI.Extensions;

namespace Cement.WebUI.Controllers.Api
{
    [RoutePrefix("api/stockroomrequest")]
    public class StockroomRequestController : RoutedEntityDataController<StockroomBaseRequest, StockroomRequestProxy, StockroomRequestRoute>
    {
        public IEntityService<StockroomMovementRequest> StockroomMovementRequestEntityService { get; set; }
        public IWarehouseService WarehouseService { get; set; }
        public IStockroomProductService StockroomProductService { get; set; }

        public override IDataResult Get(long id)
        {
            var data = EntityService.GetAll()
                .Where(x => x.Id == id)
                .Select(x => new
                {
                    id = x.Id,
                    p_number = x.No,
                    p_date = x.Date,
                    p_description = x.ExtraData,
                    p_warehouse = new { x.Warehouse.Id, x.Warehouse.Name },
                    p_warehouse_to = (x is StockroomMovementRequest) ? new
                        {
                            Id = (long?)(x as StockroomMovementRequest).WarehouseTo.Id,
                            (x as StockroomMovementRequest).WarehouseTo.Name,
                            (x as StockroomMovementRequest).WarehouseTo.Address,
                        } : null,
                    p_product = (long?)x.StockroomProduct.Product.Id,
                    p_product_display = x.StockroomProduct.Product.Name,
                    p_stockroom_product = new
                    {
                        Id = (long?)x.StockroomProduct.Id, 
                        x.StockroomProduct.Product.Name
                    }, 
                    p_measure_unit_display = x.StockroomProduct.MeasureUnit.Name,
                    p_measure_unit = (long?)x.StockroomProduct.MeasureUnit.Id,
                    p_count = x.Amount,
                    p_reason = (x is StockroomWriteoffRequest) ? (x as StockroomWriteoffRequest).Reason : null,
                    p_type = x.StockroomRequestType,
                    p_price = (x is StockroomMakingPostingRequest) ? (x as StockroomMakingPostingRequest).PriceListProduct.Price : (decimal?)null,
                    p_tax = (x is StockroomMakingPostingRequest) ? (x as StockroomMakingPostingRequest).PriceListProduct.Tax.Rate : (decimal?)null,
                    p_warehouse_contact = (x is StockroomMovementRequest) ? (long?)(x as StockroomMovementRequest).WarehouseContactPerson.Id : null,
                    p_warehouse_contact_display = (x is StockroomMovementRequest) ? (x as StockroomMovementRequest).WarehouseContactPerson.Name : null
                })
                .First();

            var result = data.Extend((row, x) =>
            {
                row["p_tax"] = x.p_tax * 100;
            });

            return new ObjectResult { Data = result };
        }

        protected override Expression<Func<StockroomRequestRoute, bool>> DocIdFilterExpression(long id)
        {
            return x => x.Document.Id == id;
        }
        
        protected override IQueryable<StockroomRequestRoute> PersonalizeQuery(IQueryable<StockroomRequestRoute> query)
        {
            return query.Where(x => x.Employee == UserPrincipal.Employee);
        }

        protected override IDataResult DocumentList(StoreParams storeParams, IQueryable<StockroomRequestRoute> routeQuery)
        {
            var kindDict = typeof(StockroomRequestType).AsDictionary();
            var typeDict = typeof(DocumentMovementType).AsDictionary();
            var stateDict = typeof(DocumentState).AsDictionary();

            var query = routeQuery
                .Select(x => new
                {
                    p_base_doc = x.Document.Id,
                    id = x.Id,
                    p_kind = x.Document.StockroomRequestType,
                    p_number = x.Document.No,
                    p_date = x.Document.Date,
                    p_warehouse_display = x.Document.Warehouse.Name,
                    p_product_display = x.Document.StockroomProduct.Product.Name,
                    p_measure_unit_display = x.Document.StockroomProduct.MeasureUnit.Name,
                    p_count = x.Document.Amount,
                    p_description = x.Document.ExtraData,
                    p_type = x.MovementType,
                    x.Document.DocumentState
                })
                .Filter(storeParams);
            
            var data = query
                .Order(storeParams)
                .Paging(storeParams)
                .Extend((row, x) =>
                {
                    row["p_kind_display"] = kindDict.Get((int)x.p_kind);
                    row["p_form_kind"] = (int)x.p_kind;
                    row["p_type"] = typeDict.Get((int)x.p_type);
                    row["p_state_display"] = stateDict.Get((int)x.DocumentState);
                })
                .Highlight(storeParams)
                .ToList();

            return new ObjectListResult
            {
                Data = data,
                Count = query.Count()
            };
        }

        protected override StockroomBaseRequest MapProxyToEntity(StockroomRequestProxy incomeProxy)
        {
            switch (incomeProxy.StockroomRequestType)
            {
                case StockroomRequestType.Writeoff: return AutoMapper.Mapper.DynamicMap<StockroomWriteoffRequest>(incomeProxy);
                case StockroomRequestType.Posting: return AutoMapper.Mapper.DynamicMap<StockroomPostingRequest>(incomeProxy);
                case StockroomRequestType.Movement: return AutoMapper.Mapper.DynamicMap<StockroomMovementRequest>(incomeProxy);
                case StockroomRequestType.MakingPosting: return AutoMapper.Mapper.DynamicMap<StockroomMakingPostingRequest>(incomeProxy);
                default:
                    throw new ValidationException(string.Empty);
            }
        }

        [HttpPost, Route("listsignedwriteoffrequest")]
        public IDataResult SignedWriteoffRequestList()
        {
            var storeParams = Request.GetStoreParams();

            var kindDict = typeof(StockroomRequestType).AsDictionary();

            var data = DocumentRouteEntityService.GetAll()
                .Where(x => x.ArchivedAt == null)
                .Where(x => x.DeletedAt == null)
                .Where(x => x.Employee == UserPrincipal.Employee)
                .WarehouseFilter(x => x.Document.Warehouse, UserPrincipal)
                .Select(x => x.Document)
                .Where(x => x.DocumentState == DocumentState.Signed)
                .Where(x => x.StockroomRequestType == StockroomRequestType.Writeoff)
                .Where(x => x.UsedAt == null)
                .Select(x => new
                {
                    id = x.Id,
                    p_kind = x.StockroomRequestType,
                    p_number = x.No,
                    p_date = x.Date,
                    p_product_display = x.StockroomProduct.Product.Name,
                    p_measure_unit_display = x.StockroomProduct.MeasureUnit.ShortName,
                    p_count = x.Amount, 
                    p_description = x.ExtraData,
                    p_warehouse_display = x.Warehouse.Name,
                    p_warehouse_address = x.Warehouse.Address
                })
                .Order(storeParams)
                .Extend((row, x) =>
                {
                    row["p_kind"] = kindDict.Get((int)x.p_kind);
                })
                .Highlight(storeParams)
                .Distinct()
                .ToList();

            return new ObjectListResult
            {
                Data = data,
                Count = data.Count()
            };
        }

        [HttpPost, Route("listsignedmovementrequest")]
        public IDataResult SignedMovementRequestList()
        {
            var storeParams = Request.GetStoreParams();

            var kindDict = typeof(StockroomRequestType).AsDictionary();

            var data = DocumentRouteEntityService.GetAll()
                .Where(x => x.ArchivedAt == null)
                .Where(x => x.DeletedAt == null)
                .Where(x => x.Employee == UserPrincipal.Employee)
                .Where(x => x.Document is StockroomMovementRequest)
                .WarehouseFilter(x => (x.Document as StockroomMovementRequest).WarehouseTo, UserPrincipal)
                .Select(x => x.Document as StockroomMovementRequest)
                .Where(x => x.DocumentState == DocumentState.Signed)
                .Where(x => x.UsedAt == null)
                .Select(x => new
                {
                    id = x.Id,
                    p_kind = x.StockroomRequestType,
                    p_warehouse_display = x.Warehouse.Name,
                    p_warehouse_address = x.Warehouse.Address,
                    p_warehouse_to_display = x.WarehouseTo.Name,
                    p_warehouse_to_address = x.WarehouseTo.Address,
                    p_number = x.No,
                    p_date = x.Date,
                    p_product_display = x.StockroomProduct.Product.Name,
                    p_measure_unit_display = x.StockroomProduct.MeasureUnit.ShortName,
                    p_count = x.Amount,
                    p_description = x.ExtraData
                })
                .Order(storeParams)
                .Extend((row, x) =>
                {
                    row["p_kind"] = kindDict.Get((int)x.p_kind);
                })
                .Highlight(storeParams)
                .Distinct()
                .ToList();

            return new ObjectListResult
            {
                Data = data,
                Count = data.Count()
            };
        }

        [HttpPost, Route("listsignedsalerequest")]
        public IDataResult SignedSaleRequestList()
        {
            var storeParams = Request.GetStoreParams();

            var kindDict = typeof(RequestType).AsDictionary();

            var data = Container.Resolve<IEntityService<RequestMovement>>().GetAll()
                .Where(x => x.IsArchived == false)
                .Where(x => x.MovementType == DocumentMovementType.Incoming)
                .Where(x => x.Employee == UserPrincipal.Employee)
                .Select(x => x.Document)
                .Where(x => x.DocumentState == DocumentState.Signed)
                .Where(x => x.UsedAt == null)
                .Select(x => new
                {
                    id = x.Id,
                    p_kind = x.RequestType,
                    p_number = x.Number,
                    p_date = x.Date,
                    p_product_display = x.Product.Name,
                    p_measure_unit_display = x.ProductMeasureUnit.ShortName,
                    p_count = x.ProductCount,
                    p_description = x.Content,
                    p_product_price = x.ProductPrice,
                    p_product_tax = x.ProductTax,
                    p_consignee_name = x.Consignee.Name,
                    p_consignee_address = x.Consignee.Client.Address
                })
                .Order(storeParams)
                .Extend((row, x) =>
                {
                    row["p_kind"] = kindDict.Get((int)x.p_kind);
                    row["p_description"] = x.p_description.GetString();
                    row["p_product_sum"] = x.p_count * x.p_product_price;
                })
                .Highlight(storeParams)
                .Distinct()
                .ToList();

            return new ObjectListResult
            {
                Data = data,
                Count = data.Count()
            };
        }

        #region bottom lists

        [HttpPost, Route("warehouses")]
        public IDataResult StockroomRequestWarehousesBottomList()
        {
            var storeParams = Request.GetStoreParams();

            var docIdStr = storeParams.FilterParams
                .Where(x => x.PropertyName == "p_base_doc")
                .Select(x => x.Value)
                .FirstOrDefault();

            var docId = docIdStr.SafeToString().ToLong();

            if (docId == 0)
            {
                return new ObjectListResult { Count = 0 };
            }

            var warehouseQuery = EntityService.GetAll()
                .Where(x => x.Id == docId)
                .Select(x => x.Warehouse);

            var listData = WarehouseService.GetBottomList(warehouseQuery, storeParams, false);

            return new ObjectListResult
            {
                Data = listData.Item1,
                Count = listData.Item2
            };
        }

        [HttpPost, Route("products")]
        public IDataResult StockroomRequestProductsBottomList()
        {
            return GetProductsBottomList(Request, ProductType.Product);
        }

        [HttpPost, Route("services")]
        public IDataResult StockroomRequestServicesBottomList()
        {
            return GetProductsBottomList(Request, ProductType.Service);
        }

        private IDataResult GetProductsBottomList(HttpRequestMessage request, ProductType productType)
        {
            var storeParams = request.GetStoreParams();

            var docIdStr = storeParams.FilterParams
               .Where(x => x.PropertyName == "p_base_doc")
               .Select(x => x.Value)
               .FirstOrDefault();

            var docId = docIdStr.SafeToString().ToLong();

            if (docId == 0)
            {
                return new ObjectListResult { Count = 0 };
            }

            var productsQuery = EntityService.GetAll()
                .Where(x => x.Id == docId)
                .Where(x => x.StockroomProduct.Product.Type == productType)
                .Select(x => x.StockroomProduct);

            var listData = StockroomProductService.GetBottomList(productsQuery, storeParams, false);

            return new ObjectListResult
            {
                Data = listData.Item1,
                Count = listData.Item2
            };
        }

        #endregion bottom lists
    }
}