﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using Cement.Core.Entities;
using Cement.Core.Enums;
using Cement.Core.Extensions;
using Cement.Core.Services.StoreParams;
using Cement.Protocol;
using Cement.Protocol.JsonConverters;
using Cement.WebUI.Extensions;
using DataAccess.Repository;
using Newtonsoft.Json;

namespace Cement.WebUI.Controllers.Api
{
    [RoutePrefix("api/servicerequest")]
    public class ServiceRequestController : DataController<ServiceRequest, ProductRequestProxy>
    {
        protected override IDataResult ListInternal(StoreParams storeParams)
        {
            var productQuery = EntityService.GetAll()
                .OrganizationFilter(UserPrincipal);

            var query = productQuery
                .Select(x => new
                {
                    id = x.Id,
                    p_number = x.Number,
                    p_date = x.Date,
                    x.Content,
                    p_product_display = x.Product.Name,
                    p_product_quantity = x.ProductCount,
                    p_product_unit_display = x.ProductMeasureUnit.Name,
                    p_product_unit = x.ProductMeasureUnit.Id,
                    p_product_price = x.ProductPrice,
                    //p_product_tax = x.PriceListProduct.Tax
                })
                .Filter(storeParams);

            var count = query.Count();
            var data = query
                .Order(storeParams)
                .Paging(storeParams)
                .Extend((row, x) =>
                {
                    row["p_comment"] = x.Content.GetString();
                })
                .ToList();

            return new ObjectListResult
            {
                Data = data,
                Count = count
            };
        }

        public override JsResult Save()
        {
            var incomeProxy = Request.ToType<ProductRequestProxy>();

            var entity = AutoMapper.Mapper.DynamicMap<ServiceRequest>(incomeProxy);
           
            EntityService.Save(entity);

            return new JsResult(entity.Id);
        }
    }
}