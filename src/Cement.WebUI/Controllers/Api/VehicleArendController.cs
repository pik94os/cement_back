﻿using System;
using System.Linq;
using System.Web.Http;
using Cement.Core.Entities;
using Cement.Core.Enums;
using Cement.Core.Extensions;
using Cement.Core.Services;
using Cement.Protocol;
using Cement.Protocol.DetailsServices;
using Cement.WebUI.Extensions;

namespace Cement.WebUI.Controllers.Api
{
    [RoutePrefix("api/vehiclearend")]
    public class VehicleArendController : DataController<VehicleArend, VehicleArendProxy>
    {
        public IVehicleService VehicleService { get; set; }
        public IDetailsService<Client> ClientDetailsService { get; set; }

        [HttpPost, Route("listarended")]
        public IDataResult ListArended()
        {
            var storeParams = Request.GetStoreParams();

            var query = VehicleService.GetArendedVehicleQuery(UserPrincipal.Organization)
                .Select(x => new
                {
                    id = x.Id,
                    p_name = x.Vehicle.Name,
                    p_category_display = x.Vehicle.Category.Name,
                    p_sub_category_display = x.Vehicle.SubCategory.Name,
                    p_mark_model_spec_display = x.Vehicle.ModelFullName,
                    p_number_display = x.Vehicle.NumberText,
                    p_class_display = x.Vehicle.Class.Name,
                    p_size_display = x.Vehicle.SizeText,
                    p_go_display = x.Vehicle.GoText,
                    p_char_ops = (VehicleType?)x.Vehicle.Type,
                    p_char_type_display = x.Vehicle.BodyType.Name,
                    p_load_back = x.Vehicle.IsRearLoading,
                    p_load_top = x.Vehicle.IsOverLoading,
                    p_load_left = x.Vehicle.IsSideLoading,
                    p_additional_options_display = x.Vehicle.Option.Name,
                    p_additional_chars_display = x.Vehicle.ExtraCharacteristic,
                    p_department_display = x.Vehicle.Division,
                    p_column_display = x.Vehicle.Column,
                    p_garage_number = x.Vehicle.GarageNumber,

                    p_rent_name = x.Vehicle.Organization.Name,
                    p_rent_address = x.Vehicle.Organization.Client.Inn,
                    p_rent_from = x.DateStart,
                    p_rent_to = x.DateEnd,
                    p_rent_service_name = "",
                    p_rent_service_unit_display = "",
                    p_rent_service_count = "",
                    p_rent_service_fact_count = "",
                    p_rent_service_price = "",
                    p_rent_service_tax = "",
                    p_rent_service_sum = ""
                })
                .Filter(storeParams);

            var count = query.Count();

            var data = query
                .Order(storeParams)
                .Paging(storeParams)
                .Highlight(storeParams)
                .ToList();

            return new ObjectListResult
            {
                Data = data,
                Count = count
            };
        }

        [HttpPost, Route("listgiventoarend")]
        public IDataResult ListGivenToArend()
        {
            var storeParams = Request.GetStoreParams();

            var query = VehicleService.GetGivenToArendVehicleQuery(UserPrincipal.Organization)
                .Select(x => new
                {
                    id = x.Id,
                    p_name = x.Vehicle.Name,
                    p_category_display = x.Vehicle.Category.Name,
                    p_sub_category_display = x.Vehicle.SubCategory.Name,
                    p_mark_model_spec_display = x.Vehicle.ModelFullName,
                    p_number_display = x.Vehicle.NumberText,
                    p_class_display = x.Vehicle.Class.Name,
                    p_size_display = x.Vehicle.SizeText,
                    p_go_display = x.Vehicle.GoText,
                    p_char_ops = (VehicleType?)x.Vehicle.Type,
                    p_char_type_display = x.Vehicle.BodyType.Name,
                    p_load_back = x.Vehicle.IsRearLoading,
                    p_load_top = x.Vehicle.IsOverLoading,
                    p_load_left = x.Vehicle.IsSideLoading,
                    p_additional_options_display = x.Vehicle.Option.Name,
                    p_additional_chars_display = x.Vehicle.ExtraCharacteristic,
                    p_department_display = x.Vehicle.Division,
                    p_column_display = x.Vehicle.Column,
                    p_garage_number = x.Vehicle.GarageNumber,

                    p_rent_name = x.Arendator.Name,
                    p_rent_address = x.Arendator.Inn,
                    p_rent_from = x.DateStart,
                    p_rent_to = x.DateEnd,
                    p_rent_service_name = "",
                    p_rent_service_unit_display = "",
                    p_rent_service_count = "",
                    p_rent_service_fact_count = "",
                    p_rent_service_price = "",
                    p_rent_service_tax = "",
                    p_rent_service_sum = ""
                })
                .Filter(storeParams);

            var count = query.Count();

            var data = query
                .Order(storeParams)
                .Paging(storeParams)
                .Highlight(storeParams)
                .ToList();

            return new ObjectListResult
            {
                Data = data,
                Count = count
            };
        }

        [HttpPost, Route("endarend/{id}")]
        public JsResult EndArend(long id)
        {
            try
            {
                var arend = EntityService.Get(id);

                if (arend != null)
                {
                    arend.IsActive = false;
                    EntityService.Update(arend);
                }

                return JsResult.Successful();
            }
            catch (Exception exc)
            {
                return new JsResult
                {
                    Msg = exc.Message,
                    Id = id,
                    Success = false
                };
            }
        }

        public override object Details(long id)
        {
            var data = EntityService.GetAll()
                .Where(x => x.Id == id)
                .Select(x => new
                {
                    vehicleId = (long?) x.Vehicle.Id,
                    clientId = (long?) x.Arendator.Id
                })
                .FirstOrDefault();

            if (data == null)
            {
                return new VehicleArendDetailProxy();
            }

            var result = new VehicleArendDetailProxy
            {
                auto = new {id = data.vehicleId },
                firm = ClientDetailsService.GetDetails(data.clientId),
                service = new {id = (long?)null }
            };

            return result;
        }

        [HttpGet, Route("detailsarended/{id}")]
        public object DetailsArended(long id)
        {
            var data = EntityService.GetAll()
                .Where(x => x.Id == id)
                .Select(x => new
                {
                    vehicleId = (long?)x.Vehicle.Id,
                    clientId = (long?)x.Vehicle.Organization.Client.Id
                })
                .FirstOrDefault();

            if (data == null)
            {
                return new VehicleArendDetailProxy();
            }

            var result = new VehicleArendDetailProxy
            {
                auto = new { id = data.vehicleId },
                firm = ClientDetailsService.GetDetails(data.clientId),
                service = new { id = (long?)null }
            };

            return result;
        }
    }
}