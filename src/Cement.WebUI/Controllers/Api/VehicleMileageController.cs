﻿using System.Linq;
using System.Web.Http;
using Cement.Core.Entities;
using Cement.Core.Extensions;
using Cement.Core.Services.StoreParams;
using Cement.Protocol;

namespace Cement.WebUI.Controllers.Api
{
    [RoutePrefix("api/vehiclemileage")]
    public class VehicleMileageController : DataController<VehicleMileage, VehicleMileageProxy>
    {
        protected override IDataResult ListInternal(StoreParams storeParams)
        {
            var query = EntityService.GetAll()
                .OrganizationFilter(x => x.Vehicle.Organization, UserPrincipal)
                .Select(x => new 
                {
                    id = x.Id,
                    p_date = x.Date,
                    p_value = x.Mileage,
                    p_transport_unit = (long?)x.Vehicle.Id,
                    p_transport_unit_display = x.Vehicle.Name
                })
                .Filter(storeParams);

            var count = query.Count();

            var data = query
                .Order(storeParams)
                .Paging(storeParams)
                .Highlight(storeParams)
                .ToList();

            return new ObjectListResult
            {
                Data = data,
                Count = count
            };
        }
    }
}