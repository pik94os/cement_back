﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using Cement.Core.Entities;
using Cement.Core.Extensions;
using Cement.Core.Services.EntityService;
using Cement.Protocol;
using Cement.WebUI.Extensions;
using Cement.WebUI.Framework;

namespace Cement.WebUI.Controllers
{
    [Authorize]
    [RoutePrefix("api/vehiclemodification")]
    public class VehicleModificationController : CementApiController
    {
        public IEntityService<VehicleBrand> VehicleBrandService { get; set; }
        public IEntityService<VehicleModel> VehicleModelService { get; set; }
        public IEntityService<VehicleModification> VehicleModificationService { get; set; }

        [HttpPost, Route("brandmodeltree")]
        public List<TreeElement> BrandModelTree(VehicleModelTreeRequest request)
        {
            if (request == null || request.Node == 0)
            {
                var result1 = VehicleModelService.GetAll()
                     .WhereIf(request != null && request.CategoryId > 0, x => x.Category.Id == request.CategoryId)
                     .WhereIf(request != null && request.SubCategoryId > 0, x => x.SubCategory.Id == request.SubCategoryId)
                     .GroupBy(x => new { x.Brand.Id, x.Brand.Name })
                     .Select(x => x.Key)
                     .AsEnumerable()
                     .Select(x => new TreeElement
                     {
                         Children = new List<TreeElement> { new TreeElement() },
                         Id = x.Id,
                         Text = x.Name
                     })
                     .OrderBy(x => x.Text)
                     .ToList();

                return result1;
            }

            var result = VehicleModelService.GetAll()
                .Where(x => x.Brand.Id == request.Node)
                .WhereIf(request.CategoryId > 0, x => x.Category.Id == request.CategoryId)
                .WhereIf(request.SubCategoryId > 0, x => x.SubCategory.Id == request.SubCategoryId)
                .Select(x => new { x.Id, x.Name})
                .AsEnumerable()
                .Select(x => new TreeElement
                    {
                        //Children = new List<TreeElement> { new TreeElement()},
                        Id = x.Id,
                        Text = x.Name
                    })
                .OrderBy(x => x.Text)
                .ToList();

            return result;
        }

        [HttpPost, Route("listmodifications")]
        public List<TreeElement> ListModifications()
        {
            var storeParams = Request.GetStoreParams();

            var modelId = storeParams.FilterParams.Any() ? storeParams.FilterParams.Last().Value.ToLong() : (long?) null;

            var model = VehicleModelService.GetAll().Where(x => x.Id == modelId).Select(x => (long?)x.Id).FirstOrDefault();

            if (model == null)
            {
                return new List<TreeElement>();
            }

            var result = VehicleModificationService.GetAll()
                .Where(x => x.Model.Id == modelId)
                .Select(x => new
                {
                    x.Id,
                    x.Name,
                    ModelName = x.Model.Name,
                    BrandName = x.Model.Brand.Name
                })
                .AsEnumerable()
                .Select(x => new TreeElement
                {
                    Id = x.Id,
                    Text = string.Format("{0} {1} {2}", x.BrandName, x.ModelName, x.Name.SafeToString().Replace(x.ModelName, string.Empty)).Trim()
                })
                .OrderBy(x => x.Text)
                .ToList();

            if (result.Count == 0)
            {
                var name = VehicleModelService.GetAll()
                    .Where(x => x.Id == modelId)
                    .Select(x => new
                    {
                        ModelName = x.Name,
                        BrandName = x.Brand.Name
                    })
                    .AsEnumerable()
                    .Select(x => string.Format("{0} {1}", x.BrandName, x.ModelName).Trim())
                    .FirstOrDefault();
                
                result = new List<TreeElement>{new TreeElement
                {
                    Id = 0,
                    Text = name //"НЕТ МОДИФИКАЦИЙ"
                }};
            }

            return result;
        }
    }
}