﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using Cement.Core.Entities;
using Cement.Core.Enums;
using Cement.Core.Extensions;
using Cement.Core.Services.EntityService;
using Cement.Core.Services.StoreParams;
using Cement.Protocol;
using Cement.Protocol.JsonConverters;
using Cement.WebUI.Extensions;
using DataAccess.Repository;
using Newtonsoft.Json;

namespace Cement.WebUI.Controllers.Api
{
    [RoutePrefix("api/service")]
    public class ServiceController : DataController<Product, ProductProxy>
    {
        public IEntityService<ProductPersonal> ProductPesonalService { get; set; }
        public IEntityService<ProductComplect> ProductComplectService { get; set; }
        
        [HttpPost, Route("dictionarylist")]
        public List<DictionaryObject> Dropdownlist()
        {
            var data = EntityService.GetAll()
                //.OrganizationFilter(UserPrincipal)
                .Where(x => x.Type == ProductType.Service)
                .Select(x => new DictionaryObject
                {
                    Id = x.Id,
                    Name = x.Name
                })
                .OrderBy(x => x.Name)
                .ToList();

            return data;
        }

        [HttpPost, Route("danger_subclasses")]
        public List<DictionaryObject> GetDangerSubClasses()
        {
            return Container.Resolve<IRepository<DangerSubClass>>().GetAll()
                .OrderBy(x => x.Num)
                .Select(x => new DictionaryObject { Id = x.Id, Name = x.Num + " " + x.Name }).ToList();
        }

        [HttpPost, Route("moderation")]
        public IDataResult Moderation()
        {
            var storeParams = Request.GetStoreParams();

            var query = EntityService.GetAll()
                .Where(x => !x.IsModerated);

            return DocumentList(storeParams, query);
        }

        [HttpPost, Route("general")]
        public IDataResult General()
        {
            var storeParams = Request.GetStoreParams();

            var query = EntityService.GetAll()
                .Where(x => x.IsModerated);

            return DocumentList(storeParams, query);
        }

        [HttpPost, Route("personal")]
        public IDataResult Personal()
        {
            var storeParams = Request.GetStoreParams();

            var organizationId = storeParams.GetFilterPropertyByName("p_client_current").ToLong();

            var query = ProductPesonalService.GetAll()
                .OrganizationExclusiveFilter(UserPrincipal, organizationId)
                .Where(x => !x.ArchiveDate.HasValue)
                .Select(x => x.Product);

            return DocumentList(storeParams, query);
        }

        [HttpPost, Route("archive")]
        public IDataResult Archive()
        {
            var storeParams = Request.GetStoreParams();

            var query = ProductPesonalService.GetAll()
                .OrganizationFilter(UserPrincipal)
                .Where(x => x.ArchiveDate.HasValue)
                .Select(x => x.Product);

            return DocumentList(storeParams, query);
        }

        protected IDataResult DocumentList(StoreParams storeParams, IQueryable<Product> queryList)
        {
            var serviceQuery = queryList.Where(x => x.Type == ProductType.Service);

            var query = serviceQuery
                .Select(x => new
                {
                    id = x.Id,
                    p_img = new Downloadable(x.Photo.Id),
                    p_group = (long?)x.Group.Id,
                    p_group_display = x.Group.Name,
                    p_manufacturer = x.Manufacturer.Name,
                    p_name = x.Name,
                    p_subgroup = (long?)x.SubGroup.Id,
                    p_subgroup_display = x.SubGroup.Name,
                    p_text = string.Empty,
                    p_trade_mark = x.Brand,
                    p_found_country_display = x.Country.Name,
                    p_found_country = (long?)x.Country.Id,
                    p_found_manufacturer_display = x.Manufacturer.Name,
                    p_found_manufacturer = (long?)x.Manufacturer.Id,
                    p_code_okun = x.Okun,
                    p_code_partnumber = x.Article,
                    p_tu_gost = x.Gost,
                    p_tu_gost_r = x.GostR,
                    p_tu_ost = x.Ost,
                    p_tu_stp = x.Stp,
                    p_tu_tu = x.Tu,
                    p_sert_soot_number = x.ConformityCertificateNumber,
                    p_sert_soot_img1 = new Downloadable(x.ConformityCertificateScan.Id),
                    p_sert_soot_img2 = new Downloadable(x.ConformityCertificateScan2.Id),
                    p_sert_deklar_number = x.ConformityDeclarationNumber,
                    p_sert_deklar_img = new Downloadable(x.ConformityDeclarationScan.Id),
                    p_sert_san_number = x.HealthCertificateNumber,
                    p_sert_san_img = new Downloadable(x.HealthCertificateScan.Id),
                    p_sert_fire_number = x.FireSafetyCertificateNumber,
                    p_sert_fire_img = new Downloadable(x.FireSafetyCertificateScan.Id),
                    p_sert_fire_deklar = x.FireSafetyDeclarationNumber,
                    p_sert_fire_deklar_img = new Downloadable(x.FireSafetyDeclarationScan.Id),
                    p_unit = (long?)x.MeasureUnit.Id,
                    p_unit_display = x.MeasureUnit.Name,
                    p_complect_display = x.ComplectString
                })
                .Filter(storeParams);

            var count = query.Count();

            var serviceComplectDict = Container.Resolve<IRepository<ProductComplect>>().GetAll()
                .Where(x => serviceQuery.Select(y => y.Id).Contains(x.Product.Id))
                .Select(x => new
                {
                    ProductId = x.Product.Id,
                    ComplectProductId = x.ComplectProduct.Id
                })
                .AsEnumerable()
                .GroupBy(x => x.ProductId)
                .ToDictionary(x => x.Key, x => x.Select(y => y.ComplectProductId).ToArray());

            var data = query
                .Order(storeParams)
                .Paging(storeParams)
                .Extend((row, x) =>
                {
                    var package = new ProductPackageProxy
                    {
                        PackingKind = PackingKind.Single,
                        HeightSingle = 100
                    };

                    row["p_package"] = JsonConvert.SerializeObject(package);
                    row["p_complect"] = serviceComplectDict.ContainsKey(x.id) ? serviceComplectDict[x.id] : new long[0];
                    row["p_have_complect"] = serviceComplectDict.ContainsKey(x.id) ? 1 : 0;
                })
                .Highlight(storeParams)
                .ToList();

            return new ObjectListResult
            {
                Data = data,
                Count = count
            };
        }

        [HttpPost, Route("topersonal/{id}")]
        public virtual JsResult ToPersonalCatalog(long id)
        {
            try
            {
                ProductPesonalService.Save(new ProductPersonal { Organization = UserPrincipal.Organization, Product = new Product { Id = id } });

                return JsResult.Successful();
            }
            catch (Exception exc)
            {
                return new JsResult
                {
                    Msg = exc.Message,
                    Id = id,
                    Success = false
                };
            }
        }

        [HttpPost, Route("archive/{id}")]
        public virtual JsResult ArchiveItem(long id)
        {
            try
            {
                var productPersonal = ProductPesonalService.GetAll()
                    .Where(x => x.Product.Id == id)
                    .Where(x => x.Organization == UserPrincipal.Organization)
                    .First();

                productPersonal.ArchiveDate = DateTime.Now;
                ProductPesonalService.Save(productPersonal);
                return JsResult.Successful();
            }
            catch (Exception exc)
            {
                return new JsResult
                {
                    Msg = exc.Message,
                    Id = id,
                    Success = false
                };
            }
        }

        [HttpPost, Route("unarchive/{id}")]
        public virtual JsResult UnarchiveItem(long id)
        {
            try
            {
                var productPersonal = ProductPesonalService.GetAll()
                    .Where(x => x.Product.Id == id)
                    .Where(x => x.Organization == UserPrincipal.Organization)
                    .First();

                productPersonal.ArchiveDate = null;
                ProductPesonalService.Save(productPersonal);
                return JsResult.Successful();
            }
            catch (Exception exc)
            {
                return new JsResult
                {
                    Msg = exc.Message,
                    Id = id,
                    Success = false
                };
            }
        }

        [HttpPost, Route("deletePersonal/{id}")]
        public virtual JsResult DeleteProductPersonal(long id)
        {
            try
            {
                var productPersonal = ProductPesonalService.GetAll()
                    .Where(x => x.Product.Id == id)
                    .Where(x => x.Organization == UserPrincipal.Organization)
                    .First();

                ProductPesonalService.Delete(productPersonal.Id);
                return JsResult.Successful();
            }
            catch (Exception exc)
            {
                return new JsResult
                {
                    Msg = exc.Message,
                    Id = id,
                    Success = false
                };
            }
        }

        [HttpPost, Route("complectlist")]
        public virtual IDataResult ComplectList()
        {
            var storeParams = Request.GetStoreParams();
            var productId = storeParams.GetFilterPropertyByName("p_product").ToLong();

            var query = ProductComplectService.GetAll()
                .Where(x => x.Product.Id == productId)
                .Select(x => x.ComplectProduct);
                
            return DocumentList(storeParams, query);
        }

        public override JsResult Save()
        {
            var incomeProxy = Request.ToType<ProductProxy>();

            var entity = AutoMapper.Mapper.DynamicMap<Product>(incomeProxy);
            entity.Type = ProductType.Service;

            EntityService.Save(entity);

            return new JsResult(entity.Id);
        }
    }
}