﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using Cement.Core;
using Cement.Core.Entities;
using Cement.Core.Enums;
using Cement.Core.Extensions;
using Cement.Core.Services;
using Cement.Core.Services.EntityService;
using Cement.Core.Services.StoreParams;
using Cement.Protocol;
using Cement.Protocol.JsonConverters;
using Cement.WebUI.Extensions;
using DataAccess.Repository;
using Newtonsoft.Json;

namespace Cement.WebUI.Controllers.Api
{
    [RoutePrefix("api/productrequest")]
    public class ProductRequestController : MovingDocumentDataController<ProductRequest, ProductRequestProxy, RequestMovement>
    {
        #region Injections

        public IProductRequestService<ProductRequest> ProductRequestService { get; set; }

        public IEntityService<RequestRoute> RequestRouteEntityService { get; set; }

        public IEntityService<Client> ClientEntityService { get; set; }

        public IEntityService<ClientGroup> ClientGroupEntityService { get; set; }

        public IEntityService<Organization> OrganizationEntityService { get; set; }

        public IEntityService<PersonalClient> PersonalClientEntityService { get; set; }

        public IEntityService<ContractProduct> ContractProductEntityService { get; set; }
        
        #endregion Injections

        protected override IQueryable<RequestMovement> PersonalizeQuery(IQueryable<RequestMovement> query)
        {
            return query.Where(x => x.Employee == UserPrincipal.Employee);
        }

        protected override IDataResult ListInternal(StoreParams storeParams)
        {
            var query = EntityService.GetAll()
                .Where(x => x.SignedAt == null)
                .OrganizationFilter(UserPrincipal)
                .Select(x => new
                {
                    id = x.Id,
                    p_number = x.Number,
                    p_date = x.Date,
                    x.UnshipmentDateTime,
                    x.ShipmentDateTime,
                    p_load_kind = x.ShipmentType,
                    p_supplier = (long?)x.Provider.Id,
                    p_supplier_display = x.Provider.Name,
                    p_supplier_address = x.ProviderContract.Client.Address,
                    p_supplier_contact = (long?)x.ProviderContactPerson.Id,
                    p_supplier_contact_display = x.ProviderContactPerson.Name,
                    p_contract = (long?)x.ProviderContract.Id,
                    p_contract_display = x.ProviderContract.Name,
                    p_product = (long?)x.Product.Id,
                    p_product_display = x.Product.Name,
                    p_product_count = x.ProductCount,
                    p_product_fact_count = x.ProductFactCount,
                    p_product_unit_display = x.ProductMeasureUnit.Name,
                    p_product_unit = (long?)x.ProductMeasureUnit.Id,
                    p_product_price = x.ProductPrice,
                    p_product_price_display = x.ProductPrice,
                    p_product_tax = x.ProductTax,
                    p_getter = (long?)x.Consignee.Id,
                    p_getter_display = x.Consignee.Name,
                    p_getter_address = x.Consignee.Client.Address,
                    p_getter_contact = (long?)x.ConsigneeContactPerson.Id,
                    p_getter_contact_display = x.ConsigneeContactPerson.Name,
                    p_warehouse = (long?)x.Warehouse.Id,
                    p_warehouse_display = x.Warehouse.Name,
                    p_warehouse_display_address = x.Warehouse.Address,
                    x.Content,
                    p_payer = (long?)x.Payer.Id,
                    p_payer_display = x.Payer.Name,
                    p_payer_address = x.Payer.Client.Address,
                    p_payer_contact_display = x.PayerContactPerson.Name,
                    p_payer_contact = (long?)x.PayerContactPerson.Id,
                    p_char_ops = x.VehicleType,
                    p_char_type = (long?)x.VehicleBodyType.Id,
                    p_char_type_display = x.VehicleBodyType.Name,
                    p_char_load_top = x.VehicleIsRearLoading,
                    p_char_load_back = x.VehicleIsOverLoading,
                    p_char_load_left = x.VehicleIsSideLoading,
                    p_char_additional = (long?)x.VehicleOption.Id,
                    p_char_additional_display = x.VehicleOption.Name,
                    p_add_additional = x.VehicleExtraCharacteristic,
                    p_importance = x.Importance,
                    p_security = x.Secrecy,
                    x.ShipmentType,
                    p_is_initial = x.StageNumber == 0,
                    p_unit_name = x.TransportUnit.Name,
                    p_unit_number = x.TransportUnit.Vehicle.Number,
                    p_pts_name = x.TransportUnit.Trailer.Name,
                    p_pts_number = x.TransportUnit.Trailer.Number,
                    p_driver = x.TransportUnit.Driver.Name,
                    x.TransportUnit.Vehicle.Width,
                    x.TransportUnit.Vehicle.Length,
                    x.TransportUnit.Vehicle.Height
                })
                .Filter(storeParams);

            var count = query.Count();
            var data = query
                .Order(storeParams)
                .Paging(storeParams)
                .Extend((row, x) =>
                {
                    row["p_product_sum"] = x.p_product_count * (x.p_product_price.HasValue ? x.p_product_price.Value : 0M);
                    row["p_load_kind_display"] = x.p_load_kind.GetDisplayValue();
                    row["p_importance_display"] = Enum.GetName(typeof (ImportanceType), x.p_importance);
                    row["p_security_display"] = Enum.GetName(typeof(SecrecyType), x.p_security);
                    row["p_importance_name"] = x.p_importance.GetDisplayValue();
                    row["p_security_name"] = x.p_security.GetDisplayValue();
                    row["p_is_mediation"] = !x.p_is_initial;

                    row["p_char_dimensions"] =
                        (x.Length.HasValue ? x.Length.Value.ToString() + "x" : string.Empty) +
                        (x.Width.HasValue ? x.Width.Value.ToString() : string.Empty) +
                        (x.Height.HasValue ? "x" + x.Height.Value.ToString() : string.Empty);
                    
                    if (x.UnshipmentDateTime.HasValue)
                    {
                        row["p_unload_date"] = (DateTime?)x.UnshipmentDateTime.Value.Date;
                        row["p_unload_time"] = x.UnshipmentDateTime.Value.ToString("HH:mm");
                    }

                    if (x.ShipmentDateTime.HasValue)
                    {
                        row["p_load_date"] = (DateTime?)x.ShipmentDateTime.Value.Date;
                        row["p_load_time"] = x.ShipmentDateTime.Value.ToString("HH:mm");
                    }
                    row["p_comment"] = x.Content.GetString();
                    switch (x.ShipmentType)
                    {
                        case ShipmentType.Pickup:
                            row["p_s"] = 1;
                            break;
                        case ShipmentType.PickupByContract:
                            row["p_sd"] = 1;
                            break;
                        case ShipmentType.Сentralization:
                            row["p_c"] = 1;
                            break;
                    }
                })
                .Highlight(storeParams)
                .ToList();

            return new ObjectListResult
            {
                Data = data,
                Count = count
            };
        }

        protected override IQueryable<RequestMovement> GetOutcomingQuery()
        {
            return base.GetOutcomingQuery()
                .Where(x => x.Document.SignedAt != null);
        }

        protected override IDataResult DocumentList(StoreParams storeParams, IQueryable<RequestMovement> messageMovementQuery)
        {
            var query = messageMovementQuery
                .Where(x => x.Document is ProductRequest)
                .Select(x => new
                {
                    x.Id,
                    Document = x.Document as ProductRequest,
                    x.MovementType
                })
                .Select(x => new
                {
                    p_base_doc = x.Document.Id,
                    x.MovementType,
                    id = x.Id,
                    p_number = x.Document.Number,
                    p_date = x.Document.Date,
                    x.Document.UnshipmentDateTime,
                    x.Document.ShipmentDateTime,
                    p_load_kind = x.Document.ShipmentType,
                    p_supplier = (long?)x.Document.Provider.Id,
                    p_supplier_display = x.Document.Provider.Name,
                    p_supplier_address = x.Document.ProviderContract.Client.Address,
                    p_supplier_contact = (long?)x.Document.ProviderContactPerson.Id,
                    p_supplier_contact_display = x.Document.ProviderContactPerson.Name,
                    p_contract = (long?)x.Document.ProviderContract.Id,
                    p_contract_display = x.Document.ProviderContract.Name,
                    p_product = (long?)x.Document.Product.Id,
                    p_product_display = x.Document.Product.Name,
                    p_product_count = x.Document.ProductCount,
                    p_product_fact_count = x.Document.ProductFactCount,
                    p_product_unit_display = x.Document.ProductMeasureUnit.Name,
                    p_product_unit = (long?)x.Document.ProductMeasureUnit.Id,
                    p_product_price = x.Document.ProductPrice,
                    p_product_price_display = x.Document.ProductPrice,
                    p_product_tax = x.Document.ProductTax,
                    p_getter = (long?)x.Document.Consignee.Id,
                    p_getter_display = x.Document.Consignee.Name,
                    p_getter_address = x.Document.Consignee.Client.Address,
                    p_getter_contact = (long?)x.Document.ConsigneeContactPerson.Id,
                    p_getter_contact_display = x.Document.ConsigneeContactPerson.Name,
                    p_warehouse = (long?)x.Document.Warehouse.Id,
                    p_warehouse_display = x.Document.Warehouse.Name,
                    p_warehouse_display_address = x.Document.Warehouse.Address,
                    x.Document.Content,
                    p_payer = (long?)x.Document.Payer.Id,
                    p_payer_display = x.Document.Payer.Name,
                    p_payer_address = x.Document.Payer.Client.Address,
                    p_payer_contact_display = x.Document.PayerContactPerson.Name,
                    p_payer_contact = (long?)x.Document.PayerContactPerson.Id,
                    p_char_ops = x.Document.VehicleType,
                    p_char_type = (long?)x.Document.VehicleBodyType.Id,
                    p_char_type_display = x.Document.VehicleBodyType.Name,
                    p_char_load_top = x.Document.VehicleIsRearLoading,
                    p_char_load_back = x.Document.VehicleIsOverLoading,
                    p_char_load_left = x.Document.VehicleIsSideLoading,
                    p_char_additional = (long?)x.Document.VehicleOption.Id,
                    p_char_additional_display = x.Document.VehicleOption.Name,
                    p_add_additional = x.Document.VehicleExtraCharacteristic,
                    p_importance = x.Document.Importance,
                    p_security = x.Document.Secrecy,
                    x.Document.ShipmentType,
                    p_is_initial = x.Document.StageNumber == 0,
                    p_unit_name = x.Document.TransportUnit.Name,
                    p_unit_number = x.Document.TransportUnit.Vehicle.Number,
                    p_pts_name = x.Document.TransportUnit.Trailer.Name,
                    p_pts_number = x.Document.TransportUnit.Trailer.Number,
                    p_driver = x.Document.TransportUnit.Driver.Name,
                    x.Document.TransportUnit.Vehicle.Width,
                    x.Document.TransportUnit.Vehicle.Length,
                    x.Document.TransportUnit.Vehicle.Height
                })
                .Filter(storeParams);

            var count = query.Count();
            var data = query
                .Order(storeParams)
                .Paging(storeParams)
                .Extend((row, x) =>
                {
                    row["p_product_sum"] = x.p_product_count * (x.p_product_price.HasValue ? x.p_product_price.Value : 0M);
                    row["p_load_kind_display"] = x.p_load_kind.GetDisplayValue();
                    row["p_kind_display"] = x.MovementType.GetDisplayValue();
                    row["p_importance_display"] = Enum.GetName(typeof(ImportanceType), x.p_importance);
                    row["p_security_display"] = Enum.GetName(typeof(SecrecyType), x.p_security);
                    row["p_importance_name"] = x.p_importance.GetDisplayValue();
                    row["p_security_name"] = x.p_security.GetDisplayValue();
                    row["p_is_mediation"] = !x.p_is_initial || x.MovementType == DocumentMovementType.Incoming;

                    row["p_char_dimensions"] =
                        (x.Length.HasValue ? x.Length.Value.ToString() + "x" : string.Empty) +
                        (x.Width.HasValue ? x.Width.Value.ToString() : string.Empty) +
                        (x.Height.HasValue ? "x" + x.Height.Value.ToString() : string.Empty);

                    if (x.UnshipmentDateTime.HasValue)
                    {
                        row["p_unload_date"] = (DateTime?)x.UnshipmentDateTime.Value.Date;
                        row["p_unload_time"] = x.UnshipmentDateTime.Value.ToString("HH:mm");
                    }

                    if (x.ShipmentDateTime.HasValue)
                    {
                        row["p_load_date"] = (DateTime?)x.ShipmentDateTime.Value.Date;
                        row["p_load_time"] = x.ShipmentDateTime.Value.ToString("HH:mm");
                    }
                    row["p_comment"] = x.Content.GetString();
                    switch (x.ShipmentType)
                    {
                        case ShipmentType.Pickup:
                            row["p_s"] = 1;
                            break;
                        case ShipmentType.PickupByContract:
                            row["p_sd"] = 1;
                            break;
                        case ShipmentType.Сentralization:
                            row["p_c"] = 1;
                            break;
                    }
                })
                .Highlight(storeParams)
                .ToList();

            return new ObjectListResult
            {
                Data = data,
                Count = count
            };
        }
        
        public override JsResult Save()
        {
            var incomeProxy = Request.ToType<ProductRequestProxy>();

            var entity = AutoMapper.Mapper.DynamicMap<ProductRequest>(incomeProxy);

            ProductRequestService.SaveChainInitialRequest(entity, UserPrincipal.Employee);

            return new JsResult(entity.Id);
        }

        [HttpPost, Route("save_intermediary")]
        public JsResult SaveIntermediary()
        {
            var incomeProxy = Request.ToType<ProductRequestProxy>();

            var entity = AutoMapper.Mapper.DynamicMap<ProductRequest>(incomeProxy);

            ProductRequestService.SaveChainNonInitialRequest(entity, UserPrincipal.Employee);

            return new JsResult(entity.Id);
        }

        [HttpPost, Route("set_chain_end")]
        public JsResult SetChainEnd(ProductRequestEndProxy request)
        {
            var shipmentDateTime = DateTime.MinValue;
            if (request.ShipmentDate > DateTime.MinValue && request.ShipmentTime > DateTime.MinValue)
            {
                shipmentDateTime = new DateTime(
                    request.ShipmentDate.Year,
                    request.ShipmentDate.Month,
                    request.ShipmentDate.Day,
                    request.ShipmentTime.Hour,
                    request.ShipmentTime.Minute,
                    request.ShipmentTime.Second);
            }

            ProductRequestService.SetChainEnd(request.Id, shipmentDateTime);

            return JsResult.Successful();
        }

        [HttpPost, Route("listgetters")]
        public IDataResult ListGetters()
        {
            Func<long, StoreParams, Tuple<object, int>> dataGainer = (docId, storeParams) =>
            {
                var list = EntityService.GetAll()
                    .Where(x => x.Id == docId)
                    .Select(x => x.Consignee.Client)
                    .Select(x => new
                    {
                        id = x.Id,
                        p_name = x.Name,
                        p_address = x.Address,
                        p_inn = x.Inn,
                        p_kpp = x.Kpp,
                        p_ogrn = x.Ogrn
                    })
                    .Order(storeParams)
                    .ToList();

                return new Tuple<object, int>(list, list.Count);
            };

            return ListWrapper(dataGainer);
        }

        [HttpPost, Route("listpayers")]
        public IDataResult ListPayers()
        {
            Func<long, StoreParams, Tuple<object, int>> dataGainer = (docId, storeParams) =>
            {
                var list = EntityService.GetAll()
                    .Where(x => x.Id == docId)
                    .Where(x => x.Payer != null)
                    .Select(x => new
                    {
                        id = x.Payer.Client.Id,
                        p_name = x.Payer.Client.Name,
                        p_address = x.Payer.Client.Address,
                        p_inn = x.Payer.Client.Inn,
                        p_kpp = x.Payer.Client.Kpp,
                        p_ogrn = x.Payer.Client.Ogrn
                    })
                    .Order(storeParams)
                    .ToList();

                return new Tuple<object, int>(list, list.Count);
            };

            return ListWrapper(dataGainer);
        }

        [HttpPost, Route("listproviders")]
        public IDataResult ListProviders()
        {
            Func<long, StoreParams, Tuple<object, int>> dataGainer = (docId, storeParams) =>
            {
                var list = EntityService.GetAll()
                    .Where(x => x.Id == docId)
                    .Where(x => x.Provider != null)
                    .Select(x => new
                    {
                        id = x.Provider.Client.Id,
                        p_name = x.Provider.Client.Name,
                        p_address = x.Provider.Client.Address,
                        p_inn = x.Provider.Client.Inn,
                        p_kpp = x.Provider.Client.Kpp,
                        p_ogrn = x.Provider.Client.Ogrn
                    })
                    .Order(storeParams)
                    .ToList();

                return new Tuple<object, int>(list, list.Count);
            };

            return ListWrapper(dataGainer);
        }

        [HttpPost, Route("listproducts")]
        public IDataResult ListProducts()
        {
            Func<long, StoreParams, Tuple<object, int>> dataGainer = (docId, storeParams) =>
            {
                var list = EntityService.GetAll()
                    .Where(x => x.Id == docId)
                    .Where(x => x.Product != null)
                    .Select(x => new
                    {
                        id = x.Product.Id,
                        p_name = x.Product.Name,
                        p_group = x.Product.Group.Name,
                        p_subgroup = x.Product.SubGroup.Name,
                        p_trade_mark = x.Product.Brand,
                        p_manufacturer = x.Product.Manufacturer.Name,
                        p_unit_display = x.ProductMeasureUnit.Name,
                        p_price = x.ProductPrice,
                        p_tax = x.ProductTax
                    })
                    .Order(storeParams)
                    .ToList();

                return new Tuple<object, int>(list, list.Count);
            };

            return ListWrapper(dataGainer);
        }

        [HttpPost, Route("listservices")]
        public IDataResult ListServices()
        {
            return new ObjectListResult { Count = 0 };
        }

        [HttpPost, Route("listroutes")]
        public IDataResult ListRoutes()
        {
            Func<long, StoreParams, Tuple<object, int>> dataGainer = (docId, storeParams) =>
            {
                var processResultDict = typeof(DocumentProcessingResult).AsDictionary();

                var list = RequestRouteEntityService.GetAll()
                    .Where(x => x.Document.Id == docId)
                    .Select(x => new
                    {
                        id = x.Id,
                        p_org_name = x.Employee.Organization.Name,
                        p_name = x.Employee.Name,
                        p_position = x.Employee.Position.Name,
                        p_department = x.Employee.Department.Name,
                        p_date_in = x.DateIn,
                        p_date_out = x.DateOut,
                        p_comment = x.Comment,
                        x.ProcessingResult,
                    })
                    .OrderBy(x => x.id)
                    .Extend((row, x) =>
                    {
                        row["p_comment"] = x.p_comment.GetString();
                        row["p_status"] = processResultDict.Get((int)x.ProcessingResult);
                        row["p_date_in"] = new TimeDate(x.p_date_in);
                        row["p_date_out"] = new TimeDate(x.p_date_out);
                        row["p_time_spent"] = CementExtension.TimeSpent(x.p_date_out, x.p_date_in);
                    })
                    .ToList();

                return new Tuple<object, int>(list, list.Count);
            };

            return ListWrapper(dataGainer);
        }

        [HttpPost, Route("listtrunits")]
        public IDataResult ListTransportUnits()
        {
            Func<long, StoreParams, Tuple<object, int>> dataGainer = (docId, storeParams) =>
            {
                var list = EntityService.GetAll()
                    .Where(x => x.Id == docId)
                    .Where(x => x.TransportUnit != null)
                    .Select(x => x.TransportUnit)
                    .Select(x => new
                    {
                        id = x.Id,
                        p_name = x.Name,
                        p_auto_mark_model = x.Vehicle.ModelFullName,
                        p_auto_number = x.Vehicle.NumberText,
                        p_auto_trailer = x.Trailer.ModelFullName,
                        p_auto_trailer_number = x.Trailer.NumberText,
                        p_driver_name = x.Driver.FirstName,
                        p_driver_lastname = x.Driver.Patronymic,
                        p_driver_surname = x.Driver.Surname,
                        p_class_display = x.Vehicle.Class.Name,
                        p_go_display = x.Vehicle.GoText,
                        p_size_display = x.Vehicle.SizeText,
                        p_char_ops = (VehicleType?)x.Vehicle.Type,
                        p_char_type_display = x.Vehicle.BodyType.Name,
                        p_load_back = x.Vehicle.IsRearLoading,
                        p_load_top = x.Vehicle.IsOverLoading,
                        p_load_left = x.Vehicle.IsSideLoading,
                        p_additional_chars_display = x.Vehicle.ExtraCharacteristic,
                        p_additional_options_display = x.Vehicle.Option.Name,
                        p_date_start = x.DateStart
                    })
                    .Order(storeParams)
                    .ToList();

                return new Tuple<object, int>(list, list.Count);
            };

            return ListWrapper(dataGainer);
        }

        [HttpPost, Route("listwarehouses")]
        public IDataResult ListWarehouses()
        {
            Func<long, StoreParams, Tuple<object, int>> dataGainer = (docId, storeParams) =>
            {
                var query = EntityService.GetAll()
                    .Where(x => x.Id == docId)
                    .Where(x => x.Warehouse != null)
                    .Select(x => x.Warehouse);
                
                return WarehouseController.List(query, storeParams, false);
            };

            return ListWrapper(dataGainer);
        }

        protected IDataResult ListWrapper(Func<long, StoreParams, Tuple<object, int>> dataGainer)
        {
            var storeParams = Request.GetStoreParams();

            var docIdStr = storeParams.FilterParams
                .Where(x => x.PropertyName == "p_base_doc")
                .Select(x => x.Value)
                .FirstOrDefault();

            var docId = docIdStr.SafeToString().ToLong();

            if (docId == 0)
            {
                return new ObjectListResult { Count = 0 };
            }

            var res = dataGainer(docId, storeParams);
            
            return new ObjectListResult
            {
                Count = res.Item2,
                Data = res.Item1
            };
        }

        [HttpPost, Route("providertree")]
        public List<ProviderTreeElement> ProviderTree(ProviderRequest request)
        {
            long productId = 0;
            if (request.request_id > 0)
            {
                productId = ProductRequestService.GetInitialProductId(request.request_id);
            }

            var clientsIds = ContractProductEntityService.GetAll()
                .Where(x => x.Product.Id == productId)
                .Select(x => x.Contract.Client.Id);

            var result = new List<ProviderTreeElement>();

            result =  PersonalClientEntityService.GetAll()
                .OrganizationFilter(UserPrincipal)
                .WhereIf(productId > 0, x => clientsIds.Contains(x.Client.Id))
                .Select(x => new
                {
                    x.Client.Id,
                    x.Client.Name,
                    x.Client.Address,
                    Group = x.Group.Name,
                    Subgroup = x.Subgroup.Name
                })
                .AsEnumerable()
                .GroupBy(x => x.Group)
                .Select(x => new ProviderTreeElement
                {
                    Name = x.Key,
                    Leaf = false,
                    Children = x.Select(y => y)
                        .GroupBy(y => y.Subgroup)
                        .Select(y => new ProviderTreeElement
                        {
                            Name = y.Key,
                            Leaf = false,
                            Children = y.Select(z => new ProviderTreeElement
                            {
                                Name = z.Name,
                                Address = z.Address,
                                Leaf = true,
                                Id = z.Id.ToString()
                            })
                            .ToList()
                        })
                        .ToList()
                })
                .ToList();

            return result;
        }

        public class ProviderRequest : TreeRequest
        {
            public long request_id { get; set; }
        }

        public class ProviderTreeElement
        {
            [JsonProperty(PropertyName = "leaf")]
            public bool Leaf { get; set; }

            [JsonProperty(PropertyName = "id")]
            public string Id { get; set; }

            [JsonProperty(PropertyName = "data")]
            public List<ProviderTreeElement> Children { get; set; }

            [JsonProperty(PropertyName = "p_address")]
            public string Address { get; set; }

            [JsonProperty(PropertyName = "p_name")]
            public string Name { get; set; }
        }
    }
}