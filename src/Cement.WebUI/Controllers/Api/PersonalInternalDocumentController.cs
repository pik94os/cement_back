﻿using System.Linq;
using System.Web.Http;
using Cement.Core.Entities;
using Cement.Core.Entities.Views;
using Cement.Core.Enums;
using Cement.Core.Extensions;
using Cement.Core.Services;
using Cement.Core.Services.Impl;
using Cement.Protocol;
using Cement.WebUI.Extensions;
using Cement.WebUI.Framework;
using DataAccess.Repository;

namespace Cement.WebUI.Controllers.Api
{
    [Authorize]
    [RoutePrefix("api/personalinternaldoc")]
    public class PersonalInternalDocumentController : CementApiController
    {
        #region injections
        public ISignAuthorityService SignAuthorityService { get; set; }

        public IDocumentService DocumentService { get; set; }

        public IInternalDocumentService InternalDocumentService { get; set; }

        public IMessageService MessageService { get; set; }

        public IAdvertService AdvertService { get; set; }

        #endregion injections
        
        [HttpPost, Route("inlist")]
        public IDataResult ListIncoming()
        {
            var storeParams = Request.GetStoreParams();

            var employeeId = UserPrincipal.Employee != null ? UserPrincipal.Employee.Id : 0;

            var query = Container.Resolve<IViewRepository<InternalIncoming>>().GetAll()
                .Where(x => x.EmployeeId == employeeId)
                .Select(x => new
                {
                    id = x.Id,
                    p_task = x.State,
                    p_number = x.No,
                    p_date = x.Date,
                    p_packet = x.Id,
                    p_name = x.Name,
                    p_author_name = x.AuthorName,
                    x.DocumentType,
                    x.DocumentKind,
                    signerId = x.SigneeId
                })
                .Filter(storeParams);

            var count = query.Count();

            var data = query
                .Order(storeParams)
                .Paging(storeParams)
                .Extend((row, x) =>
                {
                    var showSign = false;
                    var showAgree = false;
                    var showCancel = true;

                    switch (x.DocumentType)
                    {
                        case DocumentType.InternalDocument:
                            showSign = SignAuthorityService.CanSignFor(x.signerId, UserPrincipal.Employee);
                            showAgree = x.signerId != employeeId;
                            row["p_task"] = x.p_task.GetDisplayValue();
                            break;

                        case DocumentType.Advert:
                        case DocumentType.Message:
                            row["p_task"] = string.Empty;
                            showCancel = false;
                            break;
                    }

                    row["p_kind"] = DocumentService.GetDocTypeString(x.DocumentType, x.DocumentKind);
                    row["p_hide_reg"] = true;

                    //row["p_hide_cancel"] = !showCancel;
                    //row["p_hide_sign"] = !showSign;
                    //row["p_hide_agree"] = !showAgree;

                    row["p_hide_cancel"] = false;
                    row["p_hide_sign"] = false;
                    row["p_hide_agree"] = false;

                })
                .ToList();

            return new ObjectListResult
            {
                Data = data,
                Count = count
            };
        }

        [HttpPost, Route("outlist")]
        public IDataResult ListOutcoming()
        {
            var storeParams = Request.GetStoreParams();
            var employeeId = UserPrincipal.Employee != null ? UserPrincipal.Employee.Id : 0;

            var query = Container.Resolve<IViewRepository<InternalOutcoming>>().GetAll()
                .Where(x => x.EmployeeId == employeeId)
                .Select(x => new
                {
                    id = x.Id,
                    p_task = x.State,
                    p_number = x.No,
                    p_date = x.Date,
                    p_packet = x.Id,
                    p_name = x.Name,
                    p_author_name = x.AuthorName,
                    x.DocumentType,
                    x.DocumentKind,
                    signerId = x.SigneeId
                })
                .Filter(storeParams);
            
            var count = query.Count();
            
            var data = query
                .Order(storeParams)
                .Paging(storeParams)
                .Extend((row, x) =>
                {
                    //row["id"] =  DocumentService.GetDocIdString(x.DocumentType, x.id);
                    
                    var showSign = false;
                    var showAgree = false;
                    var showCancel = true;

                    switch (x.DocumentType)
                    {
                        case DocumentType.InternalDocument:
                            showSign = SignAuthorityService.CanSignFor(x.signerId, UserPrincipal.Employee);
                            showAgree = x.signerId != employeeId;
                            break;

                        case DocumentType.Advert:
                        case DocumentType.Message:
                            showSign = true;
                            showAgree = false;
                            break;
                    }

                    row["p_kind"] = DocumentService.GetDocTypeString(x.DocumentType, x.DocumentKind);
                    row["p_hide_reg"] = true;
                    row["p_task"] = x.p_task.GetDisplayValue();

                    //row["p_hide_cancel"] = !showCancel;
                    //row["p_hide_sign"] = !showSign;
                    //row["p_hide_agree"] = !showAgree;

                    row["p_hide_cancel"] = false;
                    row["p_hide_sign"] = false;
                    row["p_hide_agree"] = false;
                })
                .ToList();

            return new ObjectListResult
            {
                Data = data,
                Count = count
            };
        }

        protected IRoutedDocumentProcessingService GetService(string serviceName)
        {
            if (Container.Kernel.HasComponent(serviceName))
            {
                return Container.Resolve<IRoutedDocumentProcessingService>(serviceName);
            }

            return null;
        }

        [HttpPost, Route("signIncoming")]
        public JsResult SignIncoming(AgreeRequest request)
        {
            if (request.Docs.Count > 1)
            {
                return new JsResult { Success = false, Msg = "Операции над несколькими документами не реализованы." };
            }

            var doc = request.Docs.First();

            switch (doc.DocumentType)
            {
                case DocumentType.InternalDocument:
                    InternalDocumentService.Sign(doc.Id, request.Comment);
                    break;
                default:
                    var service = GetService(doc.DocumentType.GetRegistrationName());

                    if (service != null)
                    {
                        service.Sign(doc.Id, request.Comment);
                    }
                    else
                    {
                        return new JsResult { Success = false, Msg = "Недопустимо." };
                    }
                    break;
            }

            return JsResult.Successful();
        }

        [HttpPost, Route("signOutcoming")]
        public JsResult SignOutcoming(AgreeRequest request)
        {
            if (request.Docs.Count > 1)
            {
                return new JsResult { Success = false, Msg = "Операции над несколькими документами не реализованы." };
            }

            var doc = request.Docs.First();

            switch (doc.DocumentType)
            {
                case DocumentType.InternalDocument:
                    InternalDocumentService.SignOutcoming(doc.Id, request.Comment);
                    break;
                case DocumentType.Advert:
                    AdvertService.Sign(doc.Id, request.Comment);
                    break;
                case DocumentType.Message:
                    MessageService.Sign(doc.Id, request.Comment);
                    break;

                default:
                    var service = GetService(doc.DocumentType.GetRegistrationName());

                    if (service != null)
                    {
                        service.Sign(doc.Id, request.Comment);
                    }
                    else
                    {
                        return new JsResult { Success = false, Msg = "Недопустимо." };
                    }
                    break;
            }

            return JsResult.Successful();
        }

        [HttpPost, Route("agreeIncoming")]
        public JsResult AgreeIncoming(AgreeRequest request)
        {
            if (request.Docs.Count > 1)
            {
                return new JsResult { Success = false, Msg = "Операции над несколькими документами не реализованы." };
            }

            if (request.EmployeeId == 0)
            {
                return new JsResult { Success = false, Msg = "Недопустимо." };
            }

            var doc = request.Docs.First();

            var employee = new Employee {Id = request.EmployeeId};

            switch (doc.DocumentType)
            {
                case DocumentType.InternalDocument:
                    InternalDocumentService.ProcessDocument(doc.Id, DocumentProcessingResult.Agreed, request.Comment, employee);
                    break;
                default:
                    var service = GetService(doc.DocumentType.GetRegistrationName());

                    if (service != null)
                    {
                        service.ProcessDocument(doc.Id, DocumentProcessingResult.Agreed, request.Comment, employee);
                    }
                    else
                    {
                        return new JsResult { Success = false, Msg = "Недопустимо." };
                    }
                    break;
            }

            return JsResult.Successful();
        }

        [HttpPost, Route("agreeOutcoming")]
        public JsResult AgreeOutcoming(AgreeRequest request)
        {
            if (request.Docs.Count > 1)
            {
                return new JsResult { Success = false, Msg = "Операции над несколькими документами не реализованы." };
            }

            if (request.EmployeeId == 0)
            {
                return new JsResult { Success = false, Msg = "Недопустимо." };
            }

            var doc = request.Docs.First();

            var employee = new Employee { Id = request.EmployeeId };

            switch (doc.DocumentType)
            {
                case DocumentType.InternalDocument:
                    InternalDocumentService.SendTo(doc.Id, UserPrincipal.Employee, employee, request.Comment);
                    break;
                                default:
                    var service = GetService(doc.DocumentType.GetRegistrationName());

                    if (service != null)
                    {
                        service.ProcessDocument(doc.Id, DocumentProcessingResult.Agreed, request.Comment, employee);
                    }
                    else
                    {
                        return new JsResult { Success = false, Msg = "Недопустимо." };
                    }
                    break;
            }

            return JsResult.Successful();
        }

        [HttpPost, Route("cancelIncoming")]
        public JsResult CancelIncoming(AgreeRequest request)
        {
            if (request.Docs.Count > 1)
            {
                return new JsResult { Success = false, Msg = "Операции над несколькими документами не реализованы." };
            }

            var doc = request.Docs.First();

            switch (doc.DocumentType)
            {
                case DocumentType.InternalDocument:
                    InternalDocumentService.Reject(doc.Id, request.Comment);
                    break;

                default:
                    var service = GetService(doc.DocumentType.GetRegistrationName());

                    if (service != null)
                    {
                        service.Reject(doc.Id, request.Comment);
                    }
                    else
                    {
                        return new JsResult { Success = false, Msg = "Недопустимо." };
                    }
                    break;
            }

            return JsResult.Successful();
        }

        [HttpPost, Route("cancelOutcoming")]
        public JsResult CancelOutcoming(AgreeRequest request)
        {
            if (request.Docs.Count > 1)
            {
                return new JsResult { Success = false, Msg = "Операции над несколькими документами не реализованы." };
            }

            var doc = request.Docs.First();
           
            var service = GetService(doc.DocumentType.GetRegistrationName());

            if (service != null)
            {
                service.Reject(doc.Id, request.Comment);
            }
            else
            {
                return new JsResult { Success = false, Msg = "Недопустимо." };
            }

            return JsResult.Successful();
        }

        [HttpGet, Route("printIncoming/{documentType}/{id}")]
        public JsResult PrintIncoming(DocumentType documentType, long id)
        {
            switch (documentType)
            {
                case DocumentType.Message:
                    MessageService.MarkAsRead(id);
                    break;

                case DocumentType.Advert:
                    AdvertService.MarkAsRead(id);
                    break;

                case DocumentType.InternalDocument:
                    InternalDocumentService.MarkAsRead(id);
                    break;
            }

            return JsResult.Successful();
        }
    }
}