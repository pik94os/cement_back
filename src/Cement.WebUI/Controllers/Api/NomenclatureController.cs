﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using Cement.Core.Entities;
using Cement.Core.Enums;
using Cement.Core.Extensions;
using Cement.Core.Services.StoreParams;
using Cement.Protocol;
using Cement.WebUI.Extensions;
using DataAccess.Repository;
using Newtonsoft.Json;

namespace Cement.WebUI.Controllers.Api
{
    [RoutePrefix("api/nomenclature")]
    public class NomenclatureController : DataController<Nomenclature, NomenclatureProxy>
    {
        protected override IDataResult ListInternal(StoreParams storeParams)
        {
            var query = EntityService.GetAll()
                .OrganizationFilter(UserPrincipal)
                .Select(x => new
                {
                    id = x.Id,
                    p_index = x.CaseIndex,
                    p_title = x.CaseTitle,
                    p_count = x.StorageUnitsCount,
                    p_deadline = x.StorageLifeAndArticleNumbers,
                    p_comment = x.Description,
                    p_year = x.Year
                })
                .Filter(storeParams);

            var count = query.Count();

            var data = query
                .Order(storeParams)
                .Paging(storeParams)
                .Highlight(storeParams)
                .ToList();

            return new ObjectListResult
            {
                Data = data,
                Count = count
            };
        }

        [HttpPost, Route("tree")]
        public List<NamedTreeElement> NomenclatureTree()
        {
            var nomenclatureTree = EntityService.GetAll()
                .OrganizationFilter(UserPrincipal)
                .Select(x => new
                {
                    x.Id,
                    x.CaseIndex,
                    x.CaseTitle,
                    x.Year
                })
                .AsEnumerable()
                .Select(x => new NamedTreeElement
                {
                    Id = x.Id,
                    Name = string.Format("{0} {1}", x.CaseIndex, x.CaseTitle)
                })
                .OrderBy(x => x.Name)
                .ToList();

            return nomenclatureTree;
        }
    }
}