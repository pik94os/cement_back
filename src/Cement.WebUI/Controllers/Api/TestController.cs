﻿using System;
using System.Linq;
using System.Web.Http;
using Cement.Core.Auth;
using Cement.Core.Entities;
using Cement.Core.Enums;
using Cement.Core.Services;
using Cement.Core.Services.EntityService;
using Cement.WebUI.Framework;
using DataAccess.Repository;
using NHibernate.Linq;

namespace Cement.WebUI.Controllers.Api
{
    [RoutePrefix("test")]
    public class TestController : CementApiController
    {
        public IEntityService<DocumentRelation> DocumentRelationEntityService { get; set; }
        public IEntityService<IncomingDocument> IncomingDocumentEntityService { get; set; }
        public IEntityService<OutcomingDocument> OutcomingDocumentEntityService { get; set; }

        [HttpPost, Route("test")]
        public object Test()
        {
            var organizationEntityService = Container.Resolve<IEntityService<Organization>>();
            var userEntityService = Container.Resolve<IEntityService<User>>();

            var org = organizationEntityService.GetAll().FirstOrDefault();

            if (org == null)
            {
                org = new Organization
                {
                    Name = "Тестовая организация"
                };

                organizationEntityService.Save(org);
            }

            var user = userEntityService.GetAll().FirstOrDefault();

            if (user == null)
            {
                user = new User
                {
                    Name = "Тестовый пользователь",
                    Email = "test@test.org",
                    IsBlocked = false
                };
            }

            user.Organization = org;
            user.Login = "test";

            var passwordHashProvider = Container.Resolve<IPasswordHashProvider>();

            user.SetNewPassword(passwordHashProvider, "8gjB7Xfy");

            userEntityService.Save(user);

            return null;
        }

        [HttpPost, Route("createUserForEmployee")]
        public void CreateUserForEmployee()
        {
            var userEntityService = Container.Resolve<IEntityService<User>>();

            var usersQuery = Container.Resolve<IRepository<User>>().GetAll();

            var employesWithoutUser = Container.Resolve<IRepository<Employee>>().GetAll()
                .Where(x => !usersQuery.Any(y => y.Employee == x))
                .Fetch(x => x.Organization)
                .ToList();
            
            employesWithoutUser.ForEach(x =>
            {
                var user = new User
                {
                    Name = x.Name,
                    Email = x.Name,
                    Login = x.Name,
                    Employee = x,
                    Organization = x.Organization
                };

                userEntityService.Save(user);
            });
        }

        [HttpPost, Route("createAdmin")]
        public void CreateAdmin()
        {
            var userEntityService = Container.Resolve<IEntityService<User>>();
            var admin = userEntityService.GetAll().FirstOrDefault(x => x.Name.ToLower() == "admin");

            if (admin == null)
            {
                admin = new User
                {
                    Name = "admin",
                    Email = "admin@test.org",
                    IsBlocked = false
                };
            }

            admin.Login = "admin";

            var passwordHashProvider = Container.Resolve<IPasswordHashProvider>();

            admin.SetNewPassword(passwordHashProvider, "8gjB7Xf1");

            userEntityService.Save(admin);
        }

        [HttpPost, Route("contract")]
        public void Contract()
        {
            var contractEntityService = Container.Resolve<IEntityService<Contract>>();
            var contractAdditionalEntityService = Container.Resolve<IEntityService<ContractAdditional>>();
            var contractApplicationEntityService = Container.Resolve<IEntityService<ContractApplication>>();
            var contractSpecificationEntityService = Container.Resolve<IEntityService<ContractSpecification>>();

            var contract = new Contract
            {
                Name = "123 от 12.12.12",
                No = "123",
                Date = DateTime.Now
            };

            var contractAdditional = new ContractAdditional
            {
                Contract = contract,
                Date = DateTime.Now.AddDays(1),
                Name = "Допник",
                No = "123/1"
            };

            var contractApplication = new ContractApplication
            {
                Contract = contract,
                Date = DateTime.Now.AddDays(2),
                Name = "Приложение",
                No = "123/11"
            };

            var contractSpecification = new ContractSpecification
            {
                Contract = contract,
                Date = DateTime.Now.AddDays(3),
                Name = "Спец",
                No = "123/111"
            };

            contractEntityService.Save(contract);
            contractAdditionalEntityService.Save(contractAdditional);
            contractApplicationEntityService.Save(contractApplication);
            contractSpecificationEntityService.Save(contractSpecification);
        }

        [HttpPost, Route("joindoc")]
        public void JoinDoc()
        {
            var internalDocumentEntityService = Container.Resolve<IEntityService<InternalDocument>>();
            var incomingInternalDocumentEntityService = Container.Resolve<IEntityService<IncomingInternalDocument>>();
            var outcomingInternalDocumentEntityService = Container.Resolve<IEntityService<OutcomingInternalDocument>>();
            var documentRelationEntityService = Container.Resolve<IEntityService<DocumentRelation>>();

            var intDoc1 = internalDocumentEntityService.GetAll().First();
            var intDoc2 = internalDocumentEntityService.GetAll().First(x => x != intDoc1);

            var documentService = Container.Resolve<IDocumentService>();

            documentService.BindDocuments(intDoc1, intDoc2);

            incomingInternalDocumentEntityService.Save(new IncomingInternalDocument
            {
                Document = intDoc2,
                Employee = UserPrincipal.Employee,
                //Properties = IncomingDocumentProperty.None
            });

            incomingInternalDocumentEntityService.Save(new IncomingInternalDocument
            {
                Document = intDoc1,
                Employee = UserPrincipal.Employee,
                //Properties = IncomingDocumentProperty.None
            });

            outcomingInternalDocumentEntityService.Save(new OutcomingInternalDocument
            {
                Document = intDoc2,
                Employee = UserPrincipal.Employee
            });

            outcomingInternalDocumentEntityService.Save(new OutcomingInternalDocument
            {
                Document = intDoc1,
                Employee = UserPrincipal.Employee
            });

            //var a = outcomingInternalDocumentEntityService.GetAll().First();

            //outcomingInternalDocumentEntityService.Delete(a.Id);

        }

        [HttpPost, Route("joindoc2")]
        public void JoinDoc2()
        {
            var correspondenceEntityService = Container.Resolve<IEntityService<Correspondence>>();
            var incomingCorrespondenceEntityService = Container.Resolve<IEntityService<IncomingCorrespondence>>();
            var outcomingCorrespondenceEntityService = Container.Resolve<IEntityService<OutcomingCorrespondence>>();

            var doc1 = correspondenceEntityService.GetAll().First();
            var doc2 = correspondenceEntityService.GetAll().First(x => x != doc1);

            var documentService = Container.Resolve<IDocumentService>();

            documentService.BindDocuments(doc1, doc2);

            incomingCorrespondenceEntityService.Save(new IncomingCorrespondence
            {
                Document = doc2,
                Employee = UserPrincipal.Employee,
                //Properties = IncomingDocumentProperty.None
            });

            incomingCorrespondenceEntityService.Save(new IncomingCorrespondence
            {
                Document = doc1,
                Employee = UserPrincipal.Employee,
                //Properties = IncomingDocumentProperty.None
            });

            outcomingCorrespondenceEntityService.Save(new OutcomingCorrespondence
            {
                Document = doc2,
                Employee = UserPrincipal.Employee
            });

            outcomingCorrespondenceEntityService.Save(new OutcomingCorrespondence
            {
                Document = doc1,
                Employee = UserPrincipal.Employee
            });

            //var a = outcomingInternalDocumentEntityService.GetAll().First();

            //outcomingInternalDocumentEntityService.Delete(a.Id);

        }
    }
}