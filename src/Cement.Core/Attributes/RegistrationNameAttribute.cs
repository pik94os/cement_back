﻿using System;

namespace Cement.Core.Attributes
{
    [AttributeUsage(AttributeTargets.Property | AttributeTargets.Field | AttributeTargets.Parameter, AllowMultiple = false)]
    public sealed class RegistrationNameAttribute : Attribute
    {
        public string Name { get; private set; }

        public RegistrationNameAttribute(string name)
        {
            Name = name;
        }
    }
}