﻿namespace Cement.Core
{
    public static class Constants
    {
        public static char DocIdDelimeter { get { return '|'; }}

        public static char DocBranchIdDelimeter { get { return '#'; } }

        public static string HighLight { get { return "_highlight"; } }
    }
}