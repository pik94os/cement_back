﻿namespace Cement.Core.Entities
{
    /// <summary>
    /// Адрес - реальная запись адреса для связанного объекта
    /// </summary>
    public class Address : BaseEntity
    {
        /// <summary>
        /// Индекс
        /// </summary>
        public virtual string Index { get; set; }

        /// <summary>
        /// Страна
        /// </summary>
        public virtual string Country { get; set; }

        /// <summary>
        /// Регион
        /// </summary>
        public virtual string Region { get; set; }

        /// <summary>
        /// Район
        /// </summary>
        public virtual string Area { get; set; }

        /// <summary>
        /// Населенный пункт
        /// </summary>
        public virtual string Locality { get; set; }

        /// <summary>
        /// Улица
        /// </summary>
        public virtual string Street { get; set; }

        /// <summary>
        /// Дом
        /// </summary>
        public virtual string House { get; set; }

        /// <summary>
        /// Офис
        /// </summary>
        public virtual string Office { get; set; }

        /// <summary>
        /// Квартира
        /// </summary>
        public virtual string Apartment { get; set; }

        /// <summary>
        /// Комментарий
        /// </summary>
        public virtual string Comment { get; set; }
    }
}