﻿using Cement.Core.Enums;

namespace Cement.Core.Entities
{
    /// <summary>
    /// Движение сообщения
    /// </summary>
    public abstract class MessageMovement : BaseEntity, IDocumentMovement
    {
        /// <summary>
        /// Сообщение
        /// </summary>
        public virtual Message Document { get; set; }

        /// <summary>
        /// Направление движения
        /// </summary>
        public virtual DocumentMovementType MovementType { get; private set; }

        /// <summary>
        /// Сотрудник
        /// </summary>
        public virtual Employee Employee { get; set; }

        /// <summary>
        /// Движение в архиве
        /// </summary>
        public virtual bool IsArchived { get; set; }

        /// <summary>
        /// Этап маршрута
        /// </summary>
        public virtual MessageRoute Route { get; set; }

        public long DocumentId { get { return Document.Id; } }

        public long? EmployeeId { get { return Employee != null ? Employee.Id : (long?)null; } }

        public long? OrganizationId { get { return null; } }
    }

    /// <summary>
    /// Входящee сообщение
    /// </summary>
    public class IncomingMessage : MessageMovement
    {
        public override DocumentMovementType MovementType { get { return DocumentMovementType.Incoming; } }
    }

    /// <summary>
    /// Исходящee сообщение
    /// </summary>
    public class OutcomingMessage : MessageMovement
    {
        public override DocumentMovementType MovementType { get { return DocumentMovementType.Outcoming; } }
    }
}