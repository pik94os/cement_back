﻿using System;
using Cement.Core.Enums;
using DataAccess;

namespace Cement.Core.Entities.Views
{
    public class InternalOutcoming : IViewEntity
    {
        public long Id { get; set; }

        public DocumentState State { get; set; }

        public string No { get; set; }

        public DateTime? Date { get; set; }

        public string Name { get; set; }

        public string AuthorName { get; set; }
        
        public DocumentType DocumentType { get; set; }

        public int DocumentKind { get; set; }

        public long? SigneeId { get; set; }

        public long EmployeeId { get; set; }
    }
}