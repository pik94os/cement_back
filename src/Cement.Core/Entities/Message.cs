﻿using System;
using System.Collections.Generic;
using Cement.Core.Enums;
using Newtonsoft.Json;

namespace Cement.Core.Entities
{
    public class Message : BaseOrganizationBasedEntity, IDocument
    {
        /// <summary>
        /// Создатель
        /// </summary>
        public virtual Employee Creator { get; set; }

        /// <summary>
        /// Автор
        /// </summary>
        public virtual Employee Author { get; set; }

        /// <summary>
        /// Наименование - составное поле
        /// </summary>
        public virtual string Name { get; set; }

        /// <summary>
        /// Дата
        /// </summary>
        public virtual DateTime? Date { get; set; }

        /// <summary>
        /// Вид
        /// </summary>
        public virtual MessageType MessageType { get; set; }

        /// <summary>
        /// Тема
        /// </summary>
        public virtual string Subject { get; set; }

        /// <summary>
        /// Краткое содержание
        /// </summary>
        public virtual byte[] Content { get; set; }

        /// <summary>
        /// Важность
        /// </summary>
        public virtual ImportanceType Importance { get; set; }

        /// <summary>
        /// Секретность
        /// </summary>
        public virtual SecrecyType Secrecy { get; set; }

        /// <summary>
        /// Файл
        /// </summary>
        public virtual EntityFile File { get; set; }

        /// <summary>
        /// Статус 
        /// </summary>
        public virtual DocumentState DocumentState { get; set; }

        public DocumentType Type { get { return DocumentType.Message; } }

        public string State { get { return string.Empty; } }

        public string No { get { return string.Empty; } }

        public List<string> RelatedDocumentIds { get { return null; } }
        
        #region Повторение

        /// <summary>
        /// Повтороение в текстовом выражении
        /// </summary>
        public virtual string RepeatDisplay { get; set; }

        /// <summary>
        /// Начало
        /// </summary>
        public virtual DateTime? RepeatStartAt { get; set; }

        /// <summary>
        /// Окончание - Время
        /// </summary>
        public virtual DateTime? RepeatStopAt { get; set; }

        /// <summary>
        /// Количество повторений
        /// </summary>
        public virtual int? RepeatCount { get; set; }

        /// <summary>
        /// Схема повторения (хранится json)
        /// </summary>
        public virtual string RepeatDetails { get; set; }

        #endregion Повторение

        #region Поля хэлперы

        /// <summary>
        /// Не хранимое! Краткое содержимое строкой
        /// </summary>
        [JsonProperty(PropertyName = "p_content")]
        public string ContentString { get; set; }

        /// <summary>
        /// Не хранимое! Список идентификаторов сотрудников
        /// </summary>
        [JsonProperty(PropertyName = "p_employees")]
        public List<long> RecipientIds { get; set; }

        #endregion Поля хэлперы
    }

    public enum RepeatKind
    {
        EveryDay = 10,
        EveryWeek = 20,
        EveryMonth = 30,
        EveryYear = 40
    }

    public enum RepeatDayKind
    {
        EveryNDay = 10,
        EveryWorkDay = 20
    }

    public enum RepeatEndKind
    {
        AfterNRepeats = 10,
        Date = 20,
        NoEnd = 30
    }
    
    public class MessageRepeatProxy
    {
        [JsonProperty(PropertyName = "p_repeat_kind")]
        public RepeatKind Kind { get; set; }

        [JsonProperty(PropertyName = "p_repeat_begin_date")]
        public string BeginDate { get; set; }

        [JsonProperty(PropertyName = "p_repeat_begin_time")]
        public string BeginTime { get; set; }

        [JsonProperty(PropertyName = "p_repeat_end_kind")]
        public RepeatEndKind EndKind { get; set; }

        [JsonProperty(PropertyName = "p_repeat_end_date")]
        public string EndDate { get; set; }

        [JsonProperty(PropertyName = "p_repeat_end_count")]
        public int? EndCount { get; set; }

        #region Day

        [JsonProperty(PropertyName = "p_repeat_day_days")]
        public int? DayDays { get; set; }

        [JsonProperty(PropertyName = "p_repeat_day_value")]
        public RepeatDayKind DayKind { get; set; }

        #endregion Day

        #region Week

        [JsonProperty(PropertyName = "p_repeat_week_week")]
        public int? Week { get; set; }

        [JsonProperty(PropertyName = "p_repeat_week_monday")]
        public string WeekMonday { get; set; }

        [JsonProperty(PropertyName = "p_repeat_week_tuesday")]
        public string WeekTuesday { get; set; }

        [JsonProperty(PropertyName = "p_repeat_week_wednesday")]
        public string WeekWednesday { get; set; }

        [JsonProperty(PropertyName = "p_repeat_week_thursday")]
        public string WeekThursday { get; set; }

        [JsonProperty(PropertyName = "p_repeat_week_friday")]
        public string WeekFriday { get; set; }

        [JsonProperty(PropertyName = "p_repeat_week_saturday")]
        public string WeekSaturday { get; set; }

        [JsonProperty(PropertyName = "p_repeat_week_sunday")]
        public string WeekSunday { get; set; }

        #endregion Week

        #region Month

        [JsonProperty(PropertyName = "p_repeat_month_days")]
        public string MonthDays { get; set; }

        [JsonProperty(PropertyName = "p_repeat_month_month")]
        public string MonthMonth { get; set; }

        [JsonProperty(PropertyName = "p_repeat_month_days2")]
        public string MonthDays2 { get; set; }

        [JsonProperty(PropertyName = "p_repeat_month_week_days")]
        public string MonthWeekDays { get; set; }

        [JsonProperty(PropertyName = "p_repeat_month_month2")]
        public string MonthMonth2 { get; set; }

        #endregion Month

        #region Year

        [JsonProperty(PropertyName = "p_repeat_year_days")]
        public string YearDays { get; set; }

        [JsonProperty(PropertyName = "p_repeat_year_month")]
        public string YearMonth { get; set; }

        [JsonProperty(PropertyName = "p_repeat_year_days2")]
        public string YearDays2 { get; set; }

        [JsonProperty(PropertyName = "p_repeat_year_week_days")]
        public string YearWeekDays { get; set; }

        [JsonProperty(PropertyName = "p_repeat_year_month2")]
        public string YearMonth2 { get; set; }

        #endregion Year
    }
}