﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Cement.Core.Enums;
using Newtonsoft.Json;

namespace Cement.Core.Entities.Stockroom
{
    /// <summary>
    /// Базовая складская заявка
    /// </summary>
    public class StockroomBaseRequest : BaseEntity, IDocument
    {
        /// <summary>
        /// Склад
        /// </summary>
        public virtual Warehouse Warehouse { get; set; }

        /// <summary>
        /// Номер
        /// </summary>
        public virtual string No { get; set; }

        /// <summary>
        /// Дата
        /// </summary>
        public virtual DateTime? Date { get; set; }

        /// <summary>
        /// Дополнительные данные
        /// </summary>
        public virtual string ExtraData { get; set; }

        /// <summary>
        /// Тип складской заявки
        /// </summary>
        public virtual StockroomRequestType StockroomRequestType { get; set; }

        /// <summary>
        /// Количество
        /// </summary>
        public virtual decimal Amount { get; set; }

        /// <summary>
        /// Основание
        /// </summary>
        public virtual string Foundation { get; set; }

        /// <summary>
        /// Товар
        /// </summary>
        public virtual StockroomProduct StockroomProduct { get; set; }

        /// <summary>
        /// Использовано
        /// </summary>
        public virtual DateTime? UsedAt { get; set; }

        /// <summary>
        /// Статус
        /// </summary>
        public virtual DocumentState DocumentState { get; set; }

        public string Name { get { return string.Empty; } }
        
        public DocumentType Type { get { return DocumentType.StockroomRequest; } }

        public string State { get { return string.Empty; } }

        public List<string> RelatedDocumentIds { get { return null; } }

        /// <summary>
        /// Не хранимое
        /// </summary>
        [JsonProperty(PropertyName = "p_measure_unit")]
        public MeasureUnit MeasureUnit { get; set; }

        [JsonProperty(PropertyName = "p_product")]
        public long? ProductId { get; set; }
    }

    /// <summary>
    /// Этап маршрута складской заявки
    /// </summary>
    public class StockroomRequestRoute : DocumentRoute<StockroomBaseRequest>
    {
    }

    /// <summary>
    /// Заявка на списание
    /// </summary>
    public class StockroomWriteoffRequest : StockroomBaseRequest
    {
        /// <summary>
        /// Причина
        /// </summary>
        public string Reason { get; set; }
    }

    /// <summary>
    /// Заявка на оприходование (инвентаризация)
    /// </summary>
    public class StockroomPostingRequest : StockroomBaseRequest
    {
        /// <summary>
        /// Акт на оприходование
        /// </summary>
        public virtual AccountingOtherPostingAct AccountingOtherPostingAct { get; set; }
    }

    /// <summary>
    /// Заявка на оприходование (производство)
    /// </summary>
    public class StockroomMakingPostingRequest : StockroomBaseRequest
    {
        /// <summary>
        /// Акт на оприходование
        /// </summary>
        public virtual AccountingOtherPostingAct AccountingOtherPostingAct { get; set; }

        /// <summary>
        /// Товар прайс-листа
        /// </summary>
        public virtual PriceListProduct PriceListProduct { get; set; }
    }

    /// <summary>
    /// Заявка на перемещение
    /// </summary>
    public class StockroomMovementRequest : StockroomBaseRequest
    {
        /// <summary>
        /// Склад (куда)
        /// </summary>
        public virtual Warehouse WarehouseTo { get; set; }

        /// <summary>
        /// Контактное лицо склада (куда)
        /// </summary>
        public virtual Employee WarehouseContactPerson { get; set; }
    }

    /// <summary>
    /// Тип складской заявки
    /// </summary>
    public enum StockroomRequestType
    {
        [Display(Name = "Заявка на списание")]
        Writeoff  = 10,

        [Display(Name = "Заявка на оприходование (инвентаризация)")]
        Posting = 20,

        [Display(Name = "Заявка на перемещение")]
        Movement = 30,

        [Display(Name = "Заявка на оприходование (производство)")]
        MakingPosting = 40
    }
}