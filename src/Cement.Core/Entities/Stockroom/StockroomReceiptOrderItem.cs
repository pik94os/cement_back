﻿using System;

namespace Cement.Core.Entities.Stockroom
{
    /// <summary>
    /// Базовый элемент, связывающий приходный складской ордер и бух.док
    /// </summary>
    public class StockroomReceiptOrderItemBase : BaseEntity
    {
        /// <summary>
        /// Тип
        /// </summary>
        public virtual StockroomReceiptOrderItemType StockroomReceiptOrderItemType { get; set; }
        
        /// <summary>
        /// Склад, на который должен прийти товар
        /// </summary>
        public virtual Warehouse Warehouse { get; set; }

        /// <summary>
        /// Товар
        /// </summary>
        public virtual Product Product { get; set; }

        /// <summary>
        /// Единица измерения товара
        /// </summary>
        public virtual MeasureUnit MeasureUnit { get; set; }

        /// <summary>
        /// Кол-во факт
        /// </summary>
        public virtual decimal Amount { get; set; }

        /// <summary>
        /// Цена товара
        /// </summary>
        public virtual decimal? Price { get; set; }

        /// <summary>
        /// Налог
        /// </summary>
        public virtual decimal? Tax { get; set; }

        /// <summary>
        /// Флаг использования в приходном складском ордере
        /// </summary>
        public virtual DateTime? UsedAt{ get; set; }
    }

    /// <summary>
    /// Внутриорганизационное движение товара - перемещение
    /// </summary>
    public class StockroomReceiptOrderItemInnerMove : StockroomReceiptOrderItemBase
    {
        /// <summary>
        /// Иной бух.док
        /// </summary>
        public virtual AccountingOtherBase AccountingOtherDoc { get; set; }
        
        /// <summary>
        /// Склад, c которого идет товар
        /// </summary>
        public virtual Warehouse WarehouseFrom { get; set; }
    }

    /// <summary>
    /// Внутриорганизационное движение товара - оприходование
    /// </summary>
    public class StockroomReceiptOrderItemInnerPosting : StockroomReceiptOrderItemBase
    {
        /// <summary>
        /// Иной бух.док
        /// </summary>
        public virtual AccountingOtherBase AccountingOtherDoc { get; set; }

        ///// <summary>
        ///// Основание
        ///// </summary>
        //public virtual Document Foundation { get; set; }
    }

    /// <summary>
    /// Межорганизационное движение товара
    /// </summary>
    public class StockroomReceiptOrderItemOuter : StockroomReceiptOrderItemBase
    {
        /// <summary>
        /// Бух.док (приход/расход)
        /// </summary>
        public virtual AccountingInOutDocBase AccountingInOutDoc { get; set; }
    }
    
    /// <summary>
    /// Тип элемента, связывающего приходный складской ордер и бух.док
    /// </summary>
    public enum StockroomReceiptOrderItemType
    {
        InnerMove = 10,

        InnerPosting = 20,

        Outer = 30,
    }
}