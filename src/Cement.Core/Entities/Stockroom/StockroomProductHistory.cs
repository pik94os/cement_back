﻿using System;

namespace Cement.Core.Entities.Stockroom
{
    /// <summary>
    /// История движения товара склада
    /// </summary>
    public class StockroomProductHistory : BaseEntity
    {
        /// <summary>
        /// Товар склада
        /// </summary>
        public virtual StockroomProduct StockroomProduct { get; set; }
        
        /// <summary>
        /// Дата изменения
        /// </summary>
        public virtual DateTime Date { get; set; }

        /// <summary>
        /// Является расходом
        /// </summary>
        public virtual bool IsExpense{ get; set; }

        /// <summary>
        /// Начальный остаток
        /// </summary>
        public virtual decimal InitialAmount { get; set; }

        /// <summary>
        /// Начальный налог
        /// </summary>
        public virtual decimal InitialTax { get; set; }

        /// <summary>
        /// Начальная цена
        /// </summary>
        public virtual decimal InitialPrice { get; set; }
        
        /// <summary>
        /// Изменение - Количество 
        /// </summary>
        public virtual decimal ChangeAmount { get; set; }

        /// <summary>
        /// Изменение - Налог 
        /// </summary>
        public virtual decimal ChangeTax { get; set; }

        /// <summary>
        /// Изменение - Цена
        /// </summary>
        public virtual decimal ChangePrice { get; set; }
    }
}