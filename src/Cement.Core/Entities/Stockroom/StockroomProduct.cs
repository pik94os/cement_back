﻿namespace Cement.Core.Entities.Stockroom
{
    /// <summary>
    /// Товар склада
    /// </summary>
    public class StockroomProduct : BaseEntity
    {
        /// <summary>
        /// Склад
        /// </summary>
        public virtual Warehouse Warehouse { get; set; }
        
        /// <summary>
        /// Товар
        /// </summary>
        public virtual Product Product { get; set; }

        /// <summary>
        /// Единица измерения
        /// </summary>
        public virtual MeasureUnit MeasureUnit { get; set; }

        /// <summary>
        /// Количество
        /// </summary>
        public virtual decimal Amount { get; set; }
    }
}