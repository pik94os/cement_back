﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Cement.Core.Enums;

namespace Cement.Core.Entities.Stockroom
{
    /// <summary>
    /// Приходный складской ордер
    /// </summary>
    public class StockroomReceiptOrderBase : BaseEntity, IDocument
    {
        /// <summary>
        /// Номер
        /// </summary>
        public virtual string No { get; set; }

        /// <summary>
        /// Дата
        /// </summary>
        public virtual DateTime? Date { get; set; }

        public string Name { get { return string.Empty; } }

        public DocumentType Type { get { return DocumentType.StockroomReceiptOrder; } }

        public string State { get { return string.Empty; } }

        public List<string> RelatedDocumentIds { get { return null; } }

        /// <summary>
        /// Тип расходного складского ордера
        /// </summary>
        public virtual StockroomReceiptOrderType StockroomReceiptOrderType { get; set; }

        /// <summary>
        /// Кол-во факт
        /// </summary>
        public virtual decimal Amount { get; set; }

        /// <summary>
        /// Склад
        /// </summary>
        public virtual Warehouse Warehouse { get; set; }

        /// <summary>
        /// Статус
        /// </summary>
        public virtual DocumentState DocumentState { get; set; }
    }

    /// <summary>
    /// Этап маршрута приходного складского ордера
    /// </summary>
    public class StockroomReceiptOrderRoute : DocumentRoute<StockroomReceiptOrderBase> { }

    /// <summary>
    /// Приходный складской ордер (Покупка)
    /// Основаниями могут служить:
    /// </summary>
    public class StockroomReceiptOrderBuy : StockroomReceiptOrderBase
    {
        /// <summary>
        /// Детализация
        /// </summary>
        public virtual StockroomReceiptOrderItemOuter StockroomReceiptOrderItemOuter { get; set; }
    }

    /// <summary>
    /// Приходный складской ордер (Перемещение)
    /// Основаниями могут служить:
    /// </summary>
    public class StockroomReceiptOrderMovement : StockroomReceiptOrderBase
    {
        /// <summary>
        /// Детализация
        /// </summary>
        public virtual StockroomReceiptOrderItemInnerMove StockroomReceiptOrderItemMove { get; set; }
    }

    /// <summary>
    /// Приходный складской ордер (Оприходование)
    /// Основаниями могут служить:
    /// </summary>
    public class StockroomReceiptOrderPosting : StockroomReceiptOrderBase
    {
        /// <summary>
        /// Детализация
        /// </summary>
        public virtual StockroomReceiptOrderItemInnerPosting StockroomReceiptOrderItemPosting { get; set; }
    }

    /// <summary>
    /// Тип приходного складского ордера
    /// </summary>
    public enum StockroomReceiptOrderType
    {
        [Display(Name = "Покупка")]
        Buy = 10,

        [Display(Name = "Оприходование")]
        Posting = 20,

        [Display(Name = "Перемещение")]
        Movement = 30
    }
}