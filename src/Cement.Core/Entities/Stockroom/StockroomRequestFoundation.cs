﻿namespace Cement.Core.Entities.Stockroom
{
    /// <summary>
    /// Основание заявки
    /// </summary>
    public class StockroomRequestFoundation : BaseEntity
    {
        /// <summary>
        /// Базовая складская заявка
        /// </summary>
        public virtual StockroomBaseRequest StockroomRequest { get; set; }

        /// <summary>
        /// Базовый документ
        /// </summary>
        public virtual Document Document { get; set; }
    }
}