﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Cement.Core.Enums;

namespace Cement.Core.Entities.Stockroom
{
    /// <summary>
    /// Расходный складской ордер
    /// </summary>
    public class StockroomExpenseOrderBase : BaseEntity, IDocument
    {
        /// <summary>
        /// Номер
        /// </summary>
        public virtual string No { get; set; }

        /// <summary>
        /// Дата
        /// </summary>
        public virtual DateTime? Date { get; set; }
        
        public string Name { get { return string.Empty; } }

        public DocumentType Type { get { return DocumentType.StockroomExpenseOrder; } }

        public string State { get { return string.Empty; } }

        public List<string> RelatedDocumentIds { get { return null; } }

        /// <summary>
        /// Тип расходного складского ордера
        /// </summary>
        public virtual StockroomExpenseOrderType StockroomExpenseOrderType { get; set; }

        /// <summary>
        /// Кол-во факт
        /// </summary>
        public virtual decimal Amount { get; set; }

        /// <summary>
        /// Склад
        /// </summary>
        public virtual Warehouse Warehouse { get; set; }

        /// <summary>
        /// Статус
        /// </summary>
        public virtual DocumentState DocumentState { get; set; }
    }

    /// <summary>
    /// Этап маршрута расходного складского ордера
    /// </summary>
    public class StockroomExpenseOrderRoute : DocumentRoute<StockroomExpenseOrderBase> {}

    /// <summary>
    /// Расходный складской ордер (Продажа)
    /// Основаниями могут служить:
    /// - входящая заявка на товар (авто) 
    /// - входящая заявка на товар (ЖД)
    /// - входящая заявка на услуги
    /// - входящая заявка на грузоперевозку
    /// </summary>
    public class StockroomExpenseOrderSale : StockroomExpenseOrderBase
    {
        /// <summary>
        /// Заявка на товар
        /// </summary>
        public virtual ProductRequest ProductRequest { get; set; }

        /// <summary>
        /// Документ (приход/расход)
        /// </summary>
        public virtual AccountingInOutDocWayBill AccountingInOutDocWayBill { get; set; }
    }

    /// <summary>
    /// Расходный складской ордер (Перемещение)
    /// Основанием служит складская заявка на перемещение
    /// </summary>
    public class StockroomExpenseOrderMovement : StockroomExpenseOrderBase
    {
        /// <summary>
        /// Складская заявка на перемещение
        /// </summary>
        public virtual StockroomMovementRequest StockroomMovementRequest { get; set; }

        /// <summary>
        /// Накладная (перемещение)
        /// </summary>
        public virtual AccountingOtherMovementBill AccountingOtherMovementBill { get; set; }
    }

    /// <summary>
    /// Расходный складской ордер (Списание)
    /// Основанием служит складская заявка на списание
    /// </summary>
    public class StockroomExpenseOrderWriteoff : StockroomExpenseOrderBase
    {
        /// <summary>
        /// Складская заявка на списание
        /// </summary>
        public virtual StockroomWriteoffRequest StockroomWriteoffRequest { get; set; }

        /// <summary>
        /// Акт на списание
        /// </summary>
        public virtual AccountingOtherWriteoffAct AccountingOtherWriteoffAct { get; set; }
    }

    /// <summary>
    /// Тип расходного складского ордера
    /// </summary>
    public enum StockroomExpenseOrderType
    {
        [Display(Name = "Продажа")]
        Sale = 10,

        [Display(Name = "Списание")]
        Writeoff = 20,

        [Display(Name = "Перемещение")]
        Movement = 30
    }
}