﻿namespace Cement.Core.Entities.Stockroom
{
    /// <summary>
    /// Член комиссии инвентаризации
    /// </summary>
    public class InventoryMember : BaseEntity
    {
        /// <summary>
        /// Инвентаризация
        /// </summary>
        public virtual Inventory Inventory { get; set; }

        /// <summary>
        /// Член комиссии
        /// </summary>
        public virtual Employee Memeber { get; set; }
    }
}