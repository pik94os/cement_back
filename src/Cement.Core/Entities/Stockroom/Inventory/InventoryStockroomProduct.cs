﻿namespace Cement.Core.Entities.Stockroom
{
    /// <summary>
    /// Товар склада инвентаризации
    /// </summary>
    public class InventoryStockroomProduct : BaseEntity
    {
        /// <summary>
        /// Инвентаризация
        /// </summary>
        public virtual Inventory Inventory { get; set; }

        /// <summary>
        /// Товар склада
        /// </summary>
        public virtual StockroomProduct StockroomProduct { get; set; }

        /// <summary>
        /// Количество
        /// </summary>
        public virtual int Count { get; set; }
    }
}