﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Cement.Core.Enums;
using Newtonsoft.Json;

namespace Cement.Core.Entities.Stockroom
{
    /// <summary>
    /// Инвентаризация
    /// </summary>
    public class Inventory : BaseEntity, IDocument
    {
        /// <summary>
        /// Склад
        /// </summary>
        public virtual Warehouse Warehouse { get; set; }

        /// <summary>
        /// Номер
        /// </summary>
        public virtual string No { get; set; }

        /// <summary>
        /// Дата
        /// </summary>
        public virtual DateTime? Date { get; set; }

        /// <summary>
        /// Председатель комиссии
        /// </summary>
        public virtual Employee Chairman { get; set; }

        /// <summary>
        /// Директор
        /// </summary>
        public virtual Employee Director { get; set; }

        /// <summary>
        /// Вид
        /// </summary>
        public virtual InventoryKind Kind { get; set; }
        
        /// <summary>
        /// Статус
        /// </summary>
        public virtual DocumentState DocumentState { get; set; }

        public string Name { get { return string.Empty; } }

        public DocumentType Type { get { return DocumentType.Inventory; } }

        public string State { get { return string.Empty; } }

        public List<string> RelatedDocumentIds { get { return null; } }

        #region Поля хэлперы

        /// <summary>
        /// Не хранимое! Товары склада строкой json
        /// </summary>
        [JsonProperty(PropertyName = "p_products")]
        public string StockRoomProducts { get; set; }

        /// <summary>
        /// Не хранимое! Список идентификаторов сотрудников (членов комиссии)
        /// </summary>
        [JsonProperty(PropertyName = "p_members")]
        public List<long> MemberIds { get; set; }

        #endregion Поля хэлперы
    }


    /// <summary>
    /// Этап маршрута документа инвентризации
    /// </summary>
    public class InventoryRoute : DocumentRoute<Inventory> { }

    /// <summary>
    /// Вид инвентаризации
    /// </summary>
    public enum InventoryKind
    {
        [Display(Name = "Инвентаризация")]
        Inventory  = 10,

        [Display(Name = "ИНВ-3")]
        Inv3 = 20
    }
}