﻿using System;
using Cement.Core.Enums;

namespace Cement.Core.Entities
{
    /// <summary>
    /// Транспортное средство
    /// </summary>
    public class Vehicle : BaseOrganizationBasedEntity
    {
        #region Общие сведения

        /// <summary>
        /// Наименование - сборное поле: полное наименование модели + полный номер
        /// </summary>
        public virtual string Name { get; set; }

        /// <summary>
        /// Модель
        /// </summary>
        public virtual VehicleModel Model { get; set; }

        /// <summary>
        /// Модификация
        /// </summary>
        public virtual VehicleModification Modification { get; set; }

        /// <summary>
        /// Категория ТС
        /// </summary>
        public virtual VehicleCategory Category { get; set; }

        /// <summary>
        /// Подкатегория ТС
        /// </summary>
        public virtual VehicleSubCategory SubCategory { get; set; }

        /// <summary>
        /// Категория ТС
        /// </summary>
        public virtual int? ReleaseYear { get; set; }

        /// <summary>
        /// Гос номер (основное поле)
        /// </summary>
        public virtual string Number { get; set; }

        /// <summary>
        /// Гос номер (ID кода страны)
        /// </summary>
        public virtual CountryCode NumCountryCode { get; set; }

        /// <summary>
        /// Гос номер (ID кода региона)
        /// </summary>
        public virtual RegionCode NumRegionCode { get; set; }

        /// <summary>
        /// Фото
        /// </summary>
        public virtual EntityFile Photo { get; set; }
        #endregion Общие сведения

        #region Регистрационные данные
        /// <summary>
        /// VIN
        /// </summary>
        public virtual string Vin { get; set; }

        /// <summary>
        /// Двигатель №
        /// </summary>
        public virtual string EngineSerialNum { get; set; }

        /// <summary>
        /// Шасси №
        /// </summary>
        public virtual string СhassisSerialNum { get; set; }

        /// <summary>
        /// Кузов
        /// </summary>
        public virtual string Body { get; set; }

        /// <summary>
        /// Колесная база
        /// </summary>
        public virtual VehicleWheelBase WheelBase { get; set; }
        
        /// <summary>
        /// Водительская категория
        /// </summary>
        public virtual DriverCategory DriverCategory { get; set; }

        /// <summary>
        /// Эко класс
        /// </summary>
        public virtual decimal? EcoClass { get; set; }

        /// <summary>
        /// Цвет
        /// </summary>
        public virtual string Color { get; set; }

        /// <summary>
        /// Мощность
        /// </summary>
        public virtual decimal? Power { get; set; }

        /// <summary>
        /// Мощность
        /// </summary>
        public virtual decimal? EngineVolume { get; set; }

        /// <summary>
        /// Максимальная масса
        /// </summary>
        public virtual decimal? MaxMass { get; set; }

        /// <summary>
        /// Масса без нагрузки
        /// </summary>
        public virtual decimal? VehicleMass { get; set; }
        #endregion Регистрационные данные

        #region ПТС
        /// <summary>
        /// Серия
        /// </summary>
        public virtual string PassportSerie { get; set; }

        /// <summary>
        /// Номер
        /// </summary>
        public virtual string PassportNumber { get; set; }

        /// <summary>
        /// Производитель
        /// </summary>
        public virtual string PassportManufacturer { get; set; }

        /// <summary>
        /// Дата
        /// </summary>
        public virtual DateTime? PassportDate { get; set; }

        /// <summary>
        /// ПТС скан стр 1
        /// </summary>
        public virtual EntityFile PasspostScan1 { get; set; }

        /// <summary>
        /// ПТС скан стр 2
        /// </summary>
        public virtual EntityFile PasspostScan2 { get; set; }
        #endregion ПТС

        #region Свидетельство
        /// <summary>
        /// Серия
        /// </summary>
        public virtual string DocumentSerie { get; set; }

        /// <summary>
        /// Номер
        /// </summary>
        public virtual string DocumentNumber { get; set; }

        /// <summary>
        /// Собственник
        /// </summary>
        public virtual string DocumentOwner { get; set; }

        /// <summary>
        /// Дата
        /// </summary>
        public virtual DateTime? DocumentDate { get; set; }

        /// <summary>
        /// скан стр 1
        /// </summary>
        public virtual EntityFile DocumentScan1 { get; set; }

        /// <summary>
        /// скан стр 2
        /// </summary>
        public virtual EntityFile DocumentScan2 { get; set; }
        #endregion Свидетельство

        #region Характеристика

        /// <summary>
        /// Класс
        /// </summary>
        public virtual VehicleClass Class { get; set; }

        /// <summary>
        /// Тип ТС
        /// </summary>
        public virtual VehicleType Type { get; set; }

        /// <summary>
        /// Тип кузова
        /// </summary>
        public virtual VehicleBodyType BodyType { get; set; }

        /// <summary>
        /// Длина
        /// </summary>
        public virtual decimal? Length { get; set; }

        /// <summary>
        /// Ширина
        /// </summary>
        public virtual decimal? Width { get; set; }

        /// <summary>
        /// Высота
        /// </summary>
        public virtual decimal? Height { get; set; }

        /// <summary>
        /// Задний способ загрузки
        /// </summary>
        public virtual bool IsRearLoading { get; set; }

        /// <summary>
        /// Боковой способ загрузки
        /// </summary>
        public virtual bool IsSideLoading { get; set; }

        /// <summary>
        /// Верхний способ загрузки
        /// </summary>
        public virtual bool IsOverLoading { get; set; }

        /// <summary>
        /// Грузоподъемность
        /// </summary>
        public virtual decimal? Capacity { get; set; }

        /// <summary>
        /// Объем
        /// </summary>
        public virtual decimal? Volume { get; set; }

        /// <summary>
        /// Доп опция
        /// </summary>
        public virtual VehicleAdditionalOptions Option { get; set; }

        /// <summary>
        /// Доп.характеристики
        /// </summary>
        public virtual string ExtraCharacteristic { get; set; }

        #endregion Характеристика


        #region Составные, дополнительно хранимые в БД поля

        /// <summary>
        /// Номер ТС целиком
        /// </summary>
        public virtual string NumberText { get; set; }

        /// <summary>
        /// Габариты в формате ДxШxВ
        /// </summary>
        public virtual string SizeText { get; set; }

        /// <summary>
        /// Текст: Грузоподъемность/Объем
        /// </summary>
        public virtual string GoText { get; set; }

        /// <summary>
        /// Полное наименование модели: Бренд + Модель + Модификация
        /// </summary>
        public virtual string ModelFullName { get; set; }

        #endregion Составные, дополнительно хранимые в БД поля

        #region Дополнительно
        /// <summary>
        /// Подразделение
        /// </summary>
        public virtual VehicleDepartment Division { get; set; }

        /// <summary>
        /// Колонна
        /// </summary>
        public virtual VehicleColumn Column { get; set; }

        /// <summary>
        /// Гаражный номер
        /// </summary>
        public virtual string GarageNumber { get; set; }
        #endregion Дополнительно

    }
}