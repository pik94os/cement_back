﻿using System;

namespace Cement.Core.Entities
{
    /// <summary>
    /// Передача права подписи
    /// </summary>
    public class Signature : BaseOrganizationBasedEntity
    {
        /// <summary>
        /// Сотрудник, получающей право подписи
        /// </summary>
        public virtual Employee Recipient { get; set; }

        /// <summary>
        /// Сотрудник, передающий право подписи
        /// </summary>
        public virtual Employee Truster { get; set; }

        /// <summary>
        /// Основание 
        /// </summary>
        public virtual InternalDocument Reason { get; set; }

        /// <summary>
        /// Действует от
        /// </summary>
        public virtual DateTime? DateStart { get; set; }

        /// <summary>
        /// Действует до
        /// </summary>
        public virtual DateTime? DateEnd { get; set; }
    }
}