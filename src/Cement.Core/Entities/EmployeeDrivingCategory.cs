﻿namespace Cement.Core.Entities
{
    /// <summary>
    /// Водительская категория сотрудника
    /// </summary>
    public class EmployeeDrivingCategory : BaseEntity
    {
        /// <summary>
        /// Сотрудник
        /// </summary>
        public virtual Employee Employee { get; set; }

        /// <summary>
        /// Водительская категория
        /// </summary>
        public virtual DriverCategory Category { get; set; }
    }
}