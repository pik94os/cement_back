﻿using DataAccess;
using Newtonsoft.Json;

namespace Cement.Core.Entities
{
    /// <summary>
    /// Базовая сущность
    /// </summary>
    public abstract class BaseEntity : IEntity
    {
        /// <summary>
        /// Идентификатор
        /// </summary>
        [JsonProperty(PropertyName = "id")]
        public virtual long Id { get; set; }
    }
}