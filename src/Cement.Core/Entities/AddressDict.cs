﻿namespace Cement.Core.Entities
{
    /// <summary>
    /// Адрес - исходная запись для копирования данных в сущность реального адреса
    /// Планируется, что справочник может обновляться целиком
    /// Нельзя ссылаться на эту таблицу по внешнему ключу
    /// </summary>
    public class AddressDict : BaseEntity
    {
        /// <summary>
        /// Индекс
        /// </summary>
        public virtual string Index { get; set; }

        /// <summary>
        /// Страна
        /// </summary>
        public virtual string Country { get; set; }

        /// <summary>
        /// Регион
        /// </summary>
        public virtual string Region { get; set; }
        
        /// <summary>
        /// Район
        /// </summary>
        public virtual string Area { get; set; }

        /// <summary>
        /// Населенный пункт
        /// </summary>
        public virtual string Locality { get; set; }

        /// <summary>
        /// Улица
        /// </summary>
        public virtual string Street { get; set; }
    }
}