﻿namespace Cement.Core.Entities
{
    /// <summary>
    /// Комплект товара (услуги)
    /// </summary>
    public class ProductComplect : BaseEntity
    {
        /// <summary>
        /// Продукт (услуга)
        /// </summary>
        public virtual Product Product { get; set; }

        /// <summary>
        /// Продукт (услуга) комплекта
        /// </summary>
        public virtual Product ComplectProduct { get; set; }
    }
}
