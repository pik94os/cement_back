﻿namespace Cement.Core.Entities
{
    /// <summary>
    /// Транспортная колонна
    /// </summary>
    public class VehicleColumn : BaseOrganizationBasedEntity
    {
        public virtual string Name { get; set; } 
    }
}