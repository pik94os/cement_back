﻿using System;
using Cement.Core.Enums;

namespace Cement.Core.Entities
{
    /// <summary>
    /// Учет табеля транспортной единицы
    /// </summary>
    public class TransportUnitAccounting : BaseOrganizationBasedEntity
    {
        /// <summary>
        /// Дата
        /// </summary>
        public virtual DateTime Date { get; set; }

        /// <summary>
        /// Транспортная единица
        /// </summary>
        public virtual TransportUnit TransportUnit { get; set; }
        
        /// <summary>
        /// Учёт
        /// </summary>
        public virtual TransportUnitAccountingType AccountingType { get; set; }
    }
}