﻿using Cement.Core.Enums;

namespace Cement.Core.Entities
{
    /// <summary>
    /// Движение внутреннего документа
    /// </summary>
    public abstract class InternalDocumentMovement : BaseEntity, IDocumentMovement
    {
        /// <summary>
        /// Документ
        /// </summary>
        public virtual InternalDocument Document { get; set; }

        /// <summary>
        /// Направление движения
        /// </summary>
        public virtual DocumentMovementType MovementType { get; private set; }

        /// <summary>
        /// Сотрудник
        /// </summary>
        public virtual Employee Employee { get; set; }

        /// <summary>
        /// Движение в архиве
        /// </summary>
        public virtual bool IsArchived { get; set; }

        /// <summary>
        /// Сотрудник - встречная сторона
        /// </summary>
        public virtual Employee Meeting { get; set; }
        
        /// <summary>
        /// Этап маршрута
        /// </summary>
        public virtual InternalDocumentRoute Route { get; set; }
        
        public long DocumentId { get { return Document.Id; }}

        public long? EmployeeId { get { return Employee != null ? Employee.Id : (long?)null; } }

        public long? OrganizationId { get { return null; }}
    }

    /// <summary>
    /// Входящий внутренний документ
    /// </summary>
    public class IncomingInternalDocument : InternalDocumentMovement
    {
        /// <summary>
        /// Свойства входящего документа
        /// </summary>
        //public virtual DocumentProperty Properties { get; set; }

        public override DocumentMovementType MovementType { get { return DocumentMovementType.Incoming; }}
    }

    /// <summary>
    /// Исходящий внутренний документ
    /// </summary>
    public class OutcomingInternalDocument : InternalDocumentMovement
    {
        public override DocumentMovementType MovementType { get { return DocumentMovementType.Outcoming; } }
    }
}