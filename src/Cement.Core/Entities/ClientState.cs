﻿namespace Cement.Core.Entities
{
    /// <summary>
    /// Статус клиента
    /// </summary>
    public class ClientState : BaseOrganizationBasedEntity
    {
        /// <summary>
        /// Наименование
        /// </summary>
        public virtual string Name { get; set; }

        /// <summary>
        /// Описание
        /// </summary>
        public virtual string Description { get; set; }
    }
}