﻿using System;

namespace Cement.Core.Entities
{
    /// <summary>
    /// Страховка ТС
    /// </summary>
    public class VehicleInsurance : BaseEntity
    {
        /// <summary>
        /// ТС
        /// </summary>
        public virtual Vehicle Vehicle { get; set; }

        /// <summary>
        /// Наименование
        /// </summary>
        public virtual string Name { get; set; }

        /// <summary>
        /// Номер Страховки
        /// </summary>
        public virtual string No { get; set; }

        /// <summary>
        /// Страховая компания
        /// </summary>
        public virtual Client InsuranceCompany { get; set; }

        /// <summary>
        /// Контактное лицо
        /// </summary>
        public virtual string ContactPerson { get; set; }
        
        /// <summary>
        /// Телефон
        /// </summary>
        public virtual string Phone { get; set; }

        /// <summary>
        /// Дата начала
        /// </summary>
        public virtual DateTime? DateStart { get; set; }

        /// <summary>
        /// Дата окончания
        /// </summary>
        public virtual DateTime? DateEnd { get; set; }

        /// <summary>
        /// Сумма страховки
        /// </summary>
        public virtual decimal? InsuranceSum { get; set; }

        /// <summary>
        /// Документ (файл)
        /// </summary>
        public virtual EntityFile Document { get; set; }
    }
}