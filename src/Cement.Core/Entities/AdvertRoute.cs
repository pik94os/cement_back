﻿using System;
using Cement.Core.Enums;

namespace Cement.Core.Entities
{
    /// <summary>
    /// Этап маршрута объявления
    /// </summary>
    public class AdvertRoute : BaseEntity
    {
        /// <summary>
        /// Документ
        /// </summary>
        public virtual Advert Document { get; set; }

        /// <summary>
        /// Сотрудник
        /// </summary>
        public virtual Employee Employee { get; set; }

        /// <summary>
        /// Дата поступления
        /// </summary>
        public virtual DateTime? DateIn { get; set; }

        /// <summary>
        /// Дата отправки
        /// </summary>
        public virtual DateTime? DateOut { get; set; }

        /// <summary>
        /// Комментарий
        /// </summary>
        public virtual byte[] Comment { get; set; }
        
        /// <summary>
        /// Результат обработки сообщения
        /// </summary>
        public virtual DocumentProcessingResult ProcessingResult { get; set; }
    }
}