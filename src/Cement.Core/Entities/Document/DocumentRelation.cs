﻿using System;
using System.Collections.Generic;
using Cement.Core.Enums;

namespace Cement.Core.Entities
{
    public interface IDocument
    {
        DocumentType Type { get; }

        long Id { get; }

        string Name { get; }

        string No { get; }

        DateTime? Date { get; }

        string State { get; }

        List<string> RelatedDocumentIds { get; }

        DocumentState DocumentState { get; set; }
    }

    /// <summary>
    /// Связь документов
    /// </summary>
    public class DocumentRelation : BaseEntity
    {
        /// <summary>
        /// Тип документа 1
        /// </summary>
        public virtual DocumentType BaseDocumentType { get; set; }

        /// <summary>
        /// Тип документа 2
        /// </summary>
        public virtual DocumentType JoinedDocumentType { get; set; }

        /// <summary>
        /// Идентификатор документа 1
        /// </summary>
        public virtual long BaseDocumentId { get; set; }

        /// <summary>
        /// Идентификатор документа 2
        /// </summary>
        public virtual long JoinedDocId { get; set; }

        /// <summary>
        /// Наименование документа 2
        /// </summary>
        public virtual string Name { get; set; }

        /// <summary>
        /// Номер документа 2
        /// </summary>
        public virtual string No { get; set; }
        
        /// <summary>
        /// Дата документа 2
        /// </summary>
        public virtual DateTime? Date { get; set; }
        
        /// <summary>
        /// Статус документа 2
        /// </summary>
        public virtual string State { get; set; }
    }
}