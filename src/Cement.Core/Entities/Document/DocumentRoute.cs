﻿using System;
using Cement.Core.Enums;

namespace Cement.Core.Entities
{
    public class DocumentRoute<T> : BaseEntity, IDocumentRoute where T : BaseEntity, IDocument
    {
        /// <summary>
        /// Документ
        /// </summary>
        public virtual T Document { get; set; }
        
        /// <summary>
        /// Сотрудник
        /// </summary>
        public virtual Employee Employee { get; set; }

        /// <summary>
        /// Дата поступления
        /// </summary>
        public virtual DateTime? DateIn { get; set; }

        /// <summary>
        /// Дата отправки
        /// </summary>
        public virtual DateTime? DateOut { get; set; }

        /// <summary>
        /// Комментарий
        /// </summary>
        public virtual string Comment { get; set; }
        
        /// <summary>
        /// Результат обработки документа
        /// </summary>
        public virtual DocumentProcessingResult ProcessingResult { get; set; }
        
        /// <summary>
        /// Тип движения документа для сотрудника
        /// </summary>
        public virtual DocumentMovementType MovementType { get; set; }

        /// <summary>
        /// Дата отправки в сотрудником в архив
        /// </summary>
        public virtual DateTime? ArchivedAt { get; set; }

        /// <summary>
        /// Дата удаления сотрудником
        /// </summary>
        public virtual DateTime? DeletedAt { get; set; }

        /// <summary>
        /// Порядковый номер этапа
        /// </summary>
        public virtual int Stage { get; set; }
    }
}