﻿using System;
using Cement.Core.Enums;

namespace Cement.Core.Entities
{
    public interface IDocumentRoute
    {
        /// <summary>
        /// Сотрудник
        /// </summary>
        Employee Employee { get; set; }

        /// <summary>
        /// Дата поступления
        /// </summary>
        DateTime? DateIn { get; set; }

        /// <summary>
        /// Дата отправки
        /// </summary>
        DateTime? DateOut { get; set; }

        /// <summary>
        /// Комментарий
        /// </summary>
        string Comment { get; set; }

        /// <summary>
        /// Результат обработки документа
        /// </summary>
        DocumentProcessingResult ProcessingResult { get; set; }

        /// <summary>
        /// Тип движения документа для сотрудника
        /// </summary>
        DocumentMovementType MovementType { get; set; }

        /// <summary>
        /// Дата отправки в сотрудником в архив
        /// </summary>
        DateTime? ArchivedAt { get; set; }

        /// <summary>
        /// Дата удаления сотрудником
        /// </summary>
        DateTime? DeletedAt { get; set; }

        /// <summary>
        /// Порядковый номер этапа
        /// </summary>
        int Stage { get; set; }
    }
}