﻿using System;
using Cement.Core.Enums;

namespace Cement.Core.Entities
{
    /// <summary>
    /// Движение документа
    /// </summary>
    public class Document : BaseEntity
    {
        /// <summary>
        /// Номер
        /// </summary>
        public virtual string Number { get; set; }

        /// <summary>
        /// Дата
        /// </summary>
        public virtual DateTime? Date { get; set; }

        /// <summary>
        /// Наименование
        /// </summary>
        public virtual string Name { get; set; }

        /// <summary>
        /// Тип
        /// </summary>
        public virtual DocumentType Type { get; set; }
        
        /// <summary>
        /// Статус
        /// </summary>
        public virtual DocumentState DocumentState { get; set; }
    }
}