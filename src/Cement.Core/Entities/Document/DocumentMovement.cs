﻿using System;
using Cement.Core.Enums;

namespace Cement.Core.Entities
{
    public interface IDocumentMovement
    {
        long DocumentId { get;}

        long? EmployeeId { get; }

        long? OrganizationId { get; }

        DocumentMovementType MovementType { get; }

        bool IsArchived { get; set; }
    }

    /// <summary>
    /// Движение документа
    /// </summary>
    [Obsolete]
    public abstract class DocumentMovement : BaseEntity
    {
        /// <summary>
        /// Документ
        /// </summary>
        public virtual long DocumentId { get; set; }

        /// <summary>
        /// Сотрудник
        /// </summary>
        public virtual long? EmployeeId { get; set; }

        /// <summary>
        /// Организация
        /// </summary>
        public virtual long? OrganizationId { get; set; }

        /// <summary>
        /// Архивирован
        /// </summary>
        public virtual bool IsArchived { get; set; }
    }

    /// <summary>
    /// Входящий документ
    /// </summary>
    [Obsolete]
    public class IncomingDocument : DocumentMovement
    {
    }

    /// <summary>
    /// Исходящий документ
    /// </summary>
    [Obsolete]
    public class OutcomingDocument : DocumentMovement
    {
    }
}