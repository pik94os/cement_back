﻿using System;

namespace Cement.Core.Entities
{
    /// <summary>
    /// Транспортная единица
    /// </summary>
    public class TransportUnit : BaseOrganizationBasedEntity
    {
        /// <summary>
        /// Наименование
        /// </summary>
        public virtual string Name { get; set; }
        
        /// <summary>
        /// Дата начала
        /// </summary>
        public virtual DateTime? DateStart { get; set; }
        
        /// <summary>
        /// ТС
        /// </summary>
        public virtual Vehicle Vehicle { get; set; }

        /// <summary>
        /// Прицеп
        /// </summary>
        public virtual Vehicle Trailer { get; set; }

        /// <summary>
        /// Водитель
        /// </summary>
        public virtual Employee Driver { get; set; }
    }
}