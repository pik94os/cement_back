﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cement.Core.Entities
{
    /// <summary>
    /// Номенклатура дел
    /// </summary>
    public class Nomenclature : BaseOrganizationBasedEntity
    {
        /// <summary>
        /// Индекс дела
        /// </summary>
        public virtual string CaseIndex { get; set; }

        /// <summary>
        /// Заголовок дела
        /// </summary>
        public virtual string CaseTitle { get; set; }

        /// <summary>
        /// Количество ед.хр.
        /// </summary>
        public virtual int? StorageUnitsCount { get; set; }

        /// <summary>
        /// Срок хранения дела и № статей поперечню
        /// </summary>
        public virtual string StorageLifeAndArticleNumbers { get; set; }

        /// <summary>
        /// Примечания
        /// </summary>
        public virtual string Description { get; set; }

        /// <summary>
        /// Год
        /// </summary>
        public virtual int? Year { get; set; }
    }
}
