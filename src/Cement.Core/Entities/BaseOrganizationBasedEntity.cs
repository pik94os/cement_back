﻿namespace Cement.Core.Entities
{
    /// <summary>
    /// Сущность, связанная с организацией
    /// </summary>
    public abstract class BaseOrganizationBasedEntity : BaseEntity
    {
        /// <summary>
        /// Организация
        /// </summary>
        public virtual Organization Organization { get; set; }
    }
}