﻿namespace Cement.Core.Entities
{
    /// <summary>
    /// Получатель сообщения
    /// </summary>
    public class MessageRecipient : BaseEntity
    {
        /// <summary>
        /// Сообщение
        /// </summary>
        public virtual Message Message { get; set; }

        /// <summary>
        /// Получатель
        /// </summary>
        public virtual Employee Recipient { get; set; }
    }
}