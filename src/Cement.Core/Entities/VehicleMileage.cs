﻿using System;

namespace Cement.Core.Entities
{
    /// <summary>
    /// Пробег ТС
    /// </summary>
    public class VehicleMileage : BaseEntity
    {
        /// <summary>
        /// ТС
        /// </summary>
        public virtual Vehicle Vehicle { get; set; }

        /// <summary>
        /// Дата фиксации
        /// </summary>
        public virtual DateTime? Date { get; set; }

        /// <summary>
        /// Пробег
        /// </summary>
        public virtual decimal? Mileage { get; set; }
    }
}