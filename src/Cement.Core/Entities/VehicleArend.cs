﻿using System;

namespace Cement.Core.Entities
{
    /// <summary>
    /// Аренда автомобильного ТС
    /// </summary>
    public class VehicleArend : BaseEntity
    {
        /// <summary>
        /// Арендатор (тот, кто взял в аренду)
        /// </summary>
        public virtual Client Arendator { get; set; }

        /// <summary>
        /// ТС
        /// </summary>
        public virtual Vehicle Vehicle { get; set; }

        /// <summary>
        /// Дата начала периода аренды
        /// </summary>
        public virtual DateTime? DateStart { get; set; }
        
        /// <summary>
        /// Дата окончания периода аренды
        /// </summary>
        public virtual DateTime? DateEnd { get; set; }

        /// <summary>
        /// Аренда действительна
        /// </summary>
        public virtual bool IsActive { get; set; }
    }
}