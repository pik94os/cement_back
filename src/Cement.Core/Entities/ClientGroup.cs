﻿namespace Cement.Core.Entities
{
    /// <summary>
    /// Клиентская группа/подгруппа
    /// </summary>
    public class ClientGroup : BaseOrganizationBasedEntity
    {
        /// <summary>
        /// Наименование
        /// </summary>
        public virtual string Name { get; set; }

        /// <summary>
        /// Описание
        /// </summary>
        public virtual string Description { get; set; }

        /// <summary>
        /// Родительская группа
        /// </summary>
        public virtual ClientGroup Parent { get; set; }
    }
}