﻿using System;
using System.Collections.Generic;
using Cement.Core.Enums;
using Newtonsoft.Json;

namespace Cement.Core.Entities
{
    /// <summary>
    /// Внутренний документ
    /// </summary>
    public class InternalDocument : BaseOrganizationBasedEntity, IDocument
    {
        /// <summary>
        /// Наименование - составное поле
        /// </summary>
        public virtual string Name { get; set; }

        /// <summary>
        /// Номер
        /// </summary>
        public virtual string No { get; set; }

        /// <summary>
        /// Дата
        /// </summary>
        public virtual DateTime? Date { get; set; }

        /// <summary>
        /// Вид
        /// </summary>
        public virtual InternalDocumentType InternalDocumentType { get; set; }

        /// <summary>
        /// Номенклатура дел
        /// </summary>
        public virtual Nomenclature Nomenclature { get; set; }

        /// <summary>
        /// Тема
        /// </summary>
        public virtual string Subject { get; set; }

        /// <summary>
        /// Краткое содержание
        /// </summary>
        public virtual byte[] Content { get; set; }

        /// <summary>
        /// Автор
        /// </summary>
        public virtual Employee Author { get; set; }

        /// <summary>
        /// На подпись
        /// </summary>
        public virtual Employee Signer { get; set; }

        /// <summary>
        /// Срок рассмотрения
        /// </summary>
        public virtual DateTime? ReviewBefore { get; set; }

        /// <summary>
        /// Важность
        /// </summary>
        public virtual ImportanceType Importance { get; set; }

        /// <summary>
        /// Секретность
        /// </summary>
        public virtual SecrecyType Secrecy { get; set; }

        /// <summary>
        /// Файл
        /// </summary>
        public virtual EntityFile File { get; set; }

        /// <summary>
        /// Создатель документа
        /// </summary>
        public virtual Employee Creator { get; set; }

        /// <summary>
        /// Специфичность
        /// </summary>
        public virtual Specificity Specificity { get; set; }

        /// <summary>
        /// Статус документа
        /// </summary>
        public virtual DocumentState DocumentState { get; set; }

        public DocumentType Type { get { return DocumentType.InternalDocument;} }

        public string State { get { return string.Empty; }}


        #region Поля хэлперы
        /// <summary>
        /// Не хранимое! Краткое содержимое строкой
        /// </summary>
        public virtual string ContentString { get; set; }

        /// <summary>
        /// Не хранимое! Список идентификаторов связанных документов
        /// </summary>
        public virtual List<string> RelatedDocumentIds { get; set; }

        /// <summary>
        /// Не хранимое! Список идентификаторов сотрудников
        /// </summary>
        [JsonProperty(PropertyName = "p_employees")]
        public List<long> RecipientIds { get; set; }

        #endregion Поля хэлперы
    }
}