﻿using System;

namespace Cement.Core.Entities
{
    /// <summary>
    /// Персональный клиент сотрудника
    /// </summary>
    public class PersonalClient : BaseOrganizationBasedEntity
    {
        /// <summary>
        /// Клиент
        /// </summary>
        public virtual Client Client { get; set; }

        /// <summary>
        /// Сотрудник
        /// </summary>
        public virtual Employee Employee { get; set; }

        /// <summary>
        /// Клиент является корпоративным
        /// </summary>
        public virtual bool IsCorporate { get; set; }

        /// <summary>
        /// Дата первого контракта
        /// </summary>
        public virtual DateTime? DateFirstContract { get; set; }

        /// <summary>
        /// Дата последнего контракта
        /// </summary>
        public virtual DateTime? DateLastContract { get; set; }

        /// <summary>
        /// Дата отправки в архив
        /// </summary>
        public virtual DateTime? ArchivedAt { get; set; }

        /// <summary>
        /// Статус
        /// </summary>
        public virtual ClientState State { get; set; }

        /// <summary>
        /// Группа
        /// </summary>
        public virtual ClientGroup Group { get; set; }

        /// <summary>
        /// Подгруппа
        /// </summary>
        public virtual ClientGroup Subgroup { get; set; }

        /// <summary>
        /// Откуда
        /// </summary>
        public virtual string From { get; set; }

        /// <summary>
        /// Комментарий
        /// </summary>
        public virtual string Comment { get; set; }
    }
}