﻿namespace Cement.Core.Entities
{
    /// <summary>
    /// Отдел
    /// </summary>
    public class Department : BaseOrganizationBasedEntity
    {
        public virtual string Name { get; set; }
    }
}