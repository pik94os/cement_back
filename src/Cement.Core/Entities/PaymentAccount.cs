﻿namespace Cement.Core.Entities
{
    /// <summary>
    /// Расчетный счет
    /// </summary>
    public class PaymentAccount : BaseOrganizationBasedEntity
    {
        /// <summary>
        /// Номер р/с
        /// </summary>
        public virtual string Number { get; set; }

        /// <summary>
        /// Банк
        /// </summary>
        public virtual Bank Bank { get; set; }

        /// <summary>
        /// БМК (не хранимое)
        /// </summary>
        public virtual string Bik { get; set; }
        
    }
}
