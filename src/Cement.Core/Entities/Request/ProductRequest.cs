﻿using System;
using System.Collections.Generic;
using System.Linq;
using Cement.Core.Enums;

namespace Cement.Core.Entities
{
    /// <summary>
    /// Заявка на товар
    /// </summary>
    public class ProductRequest : BaseRequest
    {
        /// <summary>
        /// Способ отгрузки
        /// </summary>
        public virtual ShipmentType ShipmentType { get; set; }

        #region Характеристики транспортной единицы

        /// <summary>
        /// Тип ТС
        /// </summary>
        public virtual VehicleType VehicleType { get; set; }

        /// <summary>
        /// Тип кузова
        /// </summary>
        public virtual VehicleBodyType VehicleBodyType { get; set; }

        /// <summary>
        /// Задний способ загрузки
        /// </summary>
        public virtual bool VehicleIsRearLoading { get; set; }

        /// <summary>
        /// Боковой способ загрузки
        /// </summary>
        public virtual bool VehicleIsSideLoading { get; set; }

        /// <summary>
        /// Верхний способ загрузки
        /// </summary>
        public virtual bool VehicleIsOverLoading { get; set; }

        /// <summary>
        /// Доп опция
        /// </summary>
        public virtual VehicleAdditionalOptions VehicleOption { get; set; }

        /// <summary>
        /// Доп.характеристики
        /// </summary>
        public virtual string VehicleExtraCharacteristic { get; set; }

        #endregion Характеристики транспортной единицы

        /// <summary>
        /// Транспортная единица
        /// </summary>
        public virtual TransportUnit TransportUnit { get; set; }
    }
}
