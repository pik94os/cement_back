﻿using System;
using Cement.Core.Enums;

namespace Cement.Core.Entities
{
    /// <summary>
    /// Этап маршрута заявки
    /// </summary>
    public class RequestRoute : BaseEntity
    {
        /// <summary>
        /// Документ
        /// </summary>
        public virtual BaseRequest Document { get; set; }

        /// <summary>
        /// Сотрудник
        /// </summary>
        public virtual Employee Employee { get; set; }

        /// <summary>
        /// Дата поступления
        /// </summary>
        public virtual DateTime? DateIn { get; set; }

        /// <summary>
        /// Дата отправки
        /// </summary>
        public virtual DateTime? DateOut { get; set; }

        /// <summary>
        /// Комментарий
        /// </summary>
        public virtual byte[] Comment { get; set; }

        /// <summary>
        /// Исходные свойства
        /// </summary>
        public virtual DocumentProperty InitialProperty { get; set; }

        /// <summary>
        /// Результат обработки документа
        /// </summary>
        public virtual DocumentProcessingResult ProcessingResult { get; set; }
    }
}