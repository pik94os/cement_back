﻿using System;
using System.Collections.Generic;
using Cement.Core.Enums;

namespace Cement.Core.Entities
{
    /// <summary>
    /// Заявка (товарооборот)
    /// </summary>
    public class BaseRequest : BaseOrganizationBasedEntity, IDocument
    {
        /// <summary>
        /// Начальная заявка
        /// </summary>
        public virtual BaseRequest InitialRequest { get; set; }

        /// <summary>
        /// Порядок в цепочке заявок
        /// </summary>
        public virtual int StageNumber { get; set; }

        /// <summary>
        /// Guid цепочки заявок
        /// </summary>
        public virtual string ChainGuid { get; set; }

        /// <summary>
        /// Время подписания
        /// </summary>
        public virtual DateTime? SignedAt { get; set; }

        /// <summary>
        /// Тип
        /// </summary>
        public virtual RequestType RequestType { get; set; }
        
        /// <summary>
        /// Номер
        /// </summary>
        public virtual string Number { get; set; }

        /// <summary>
        /// Дата
        /// </summary>
        public virtual DateTime? Date { get; set; }

        #region Общие данные

        /// <summary>
        /// Дата и время загрузки
        /// </summary>
        public virtual DateTime? ShipmentDateTime { get; set; }

        /// <summary>
        /// Дата и время выгрузки
        /// </summary>
        public virtual DateTime? UnshipmentDateTime { get; set; }

        #endregion Общие данные

        #region Поставщик

        /// <summary>
        /// Поставщик
        /// </summary>
        public virtual Organization Provider { get; set; }

        /// <summary>
        /// Договор с продавцом/поставщиком
        /// </summary>
        public virtual Contract ProviderContract { get; set; }

        /// <summary>
        /// Контактное лицо продавца/поставщика
        /// </summary>
        public virtual Employee ProviderContactPerson { get; set; }

        #endregion Поставщик

        #region Плательщик

        /// <summary>
        /// Плательщик
        /// </summary>
        public virtual Organization Payer { get; set; }

        /// <summary>
        /// Контактное лицо плательщика
        /// </summary>
        public virtual Employee PayerContactPerson { get; set; }

        #endregion Плательщик

        #region Грузополучатель

        /// <summary>
        /// Грузополучатель
        /// </summary>
        public virtual Organization Consignee { get; set; }

        /// <summary>
        /// Контактное лицо грузополучателя
        /// </summary>
        public virtual Employee ConsigneeContactPerson { get; set; }

        /// <summary>
        /// Склад
        /// </summary>
        public virtual Warehouse Warehouse { get; set; }

        #endregion Грузополучатель

        #region Товар/Услуга

        /// <summary>
        /// Товар
        /// </summary>
        public virtual Product Product { get; set; }

        /// <summary>
        /// Цена товара
        /// </summary>
        public virtual decimal? ProductPrice { get; set; }

        /// <summary>
        /// Единица измерения товара
        /// </summary>
        public virtual MeasureUnit ProductMeasureUnit { get; set; }

        /// <summary>
        /// Количество товара
        /// </summary>
        public virtual int ProductCount { get; set; }

        /// <summary>
        /// Налог
        /// </summary>
        public virtual decimal? ProductTax { get; set; }

        /// <summary>
        /// Фактическое количество
        /// </summary>
        public virtual int? ProductFactCount { get; set; }

        #endregion Товар/Услуга

        #region Дополнительные данные

        /// <summary>
        /// Примечание
        /// </summary>
        public virtual byte[] Content { get; set; }

        /// <summary>
        /// Важность
        /// </summary>
        public virtual ImportanceType Importance { get; set; }

        /// <summary>
        /// Секретность
        /// </summary>
        public virtual SecrecyType Secrecy { get; set; }

        #endregion Дополнительные данные

        #region Поля хэлперы

        /// <summary>
        /// Примечание
        /// </summary>
        public virtual string ContentString { get; set; }

        /// <summary>
        /// Время загрузки
        /// </summary>
        public virtual DateTime? ShipmentTime { get; set; }

        /// <summary>
        /// Время выгрузки
        /// </summary>
        public virtual DateTime? UnshipmentTime { get; set; }

        /// <summary>
        /// Статус документа
        /// </summary>
        public virtual DocumentState DocumentState { get; set; }

        /// <summary>
        /// Использовано
        /// </summary>
        public virtual DateTime? UsedAt { get; set; }

        #endregion Поля хэлперы

        /// <summary>
        /// Последняя заявка в цепочке
        /// </summary>
        public virtual bool IsLastInChain { get; set; }

        public DocumentType Type { get { return DocumentType.Request; } }

        public string State { get { return string.Empty; } }

        public string No { get { return string.Empty; } }

        public List<string> RelatedDocumentIds { get { return null; } }

        public virtual string Name { get; set; }
    }
}
