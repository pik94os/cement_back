﻿using Cement.Core.Enums;

namespace Cement.Core.Entities
{
    /// <summary>
    /// Движение заявки
    /// </summary>
    public abstract class RequestMovement : BaseEntity, IDocumentMovement
    {
        /// <summary>
        /// Документ
        /// </summary>
        public virtual BaseRequest Document { get; set; }

        /// <summary>
        /// Направление движения
        /// </summary>
        public virtual DocumentMovementType MovementType { get; private set; }

        /// <summary>
        /// Ответственное лицо
        /// </summary>
        public virtual Employee Employee { get; set; }

        /// <summary>
        /// Движение в архиве
        /// </summary>
        public virtual bool IsArchived { get; set; }

        /// <summary>
        /// Ответственное лицо поставщика
        /// </summary>
        public virtual Employee Meeting { get; set; }

        /// <summary>
        /// Этап маршрута
        /// </summary>
        public virtual RequestRoute Route { get; set; }

        public long DocumentId { get { return Document.Id; } }

        public long? EmployeeId { get { return Employee != null ? Employee.Id : (long?)null; } }

        public long? OrganizationId { get { return null; } }
    }

    /// <summary>
    /// Входящяя заявка
    /// </summary>
    public class IncomingRequest : RequestMovement
    {
        public override DocumentMovementType MovementType { get { return DocumentMovementType.Incoming; } }
    }

    /// <summary>
    /// Исходящяя заявка
    /// </summary>
    public class OutcomingRequest : RequestMovement
    {
        public override DocumentMovementType MovementType { get { return DocumentMovementType.Outcoming; } }
    }
}