﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Cement.Core.Enums;
using Newtonsoft.Json;

namespace Cement.Core.Entities
{
    /// <summary>
    /// Товар (Услуга) в персональном каталоге
    /// </summary>
    public class ProductPersonal : BaseOrganizationBasedEntity
    {
        /// <summary>
        /// Товар
        /// </summary>
        public virtual Product Product { get; set; }

        /// <summary>
        /// Дата архивирования
        /// </summary>
        public virtual DateTime? ArchiveDate { get; set; }
    }
}
