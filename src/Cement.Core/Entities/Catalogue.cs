﻿namespace Cement.Core.Entities
{
    /// <summary>
    /// Каталог
    /// </summary>
    public class Catalogue : BaseOrganizationBasedEntity
    {
        /// <summary>
        /// Прайс-лист
        /// </summary>
        public virtual PriceList PriceList { get; set; }

        /// <summary>
        /// Примечание (не хранимое)
        /// </summary>
        public virtual string Description { get; set; }

        /// <summary>
        /// Примечание
        /// </summary>
        public virtual byte[] DescriptionBytes { get; set; }
    }
}
