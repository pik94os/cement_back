﻿namespace Cement.Core.Entities
{
    /// <summary>
    /// Клиент
    /// </summary>
    public class Client : BaseEntity
    {
        /// <summary>
        /// Наименование
        /// </summary>
        public virtual string Name { get; set; }

        /// <summary>
        /// Полное наименование
        /// </summary>
        public virtual string FullName { get; set; }

        /// <summary>
        /// Форма собственности
        /// </summary>
        public virtual Ownership Ownership { get; set; }

        /// <summary>
        /// Бренд
        /// </summary>
        public virtual string Brand { get; set; }

        #region Регистрационные данные

        /// <summary>
        /// ОГРН
        /// </summary>
        public virtual string Ogrn { get; set; }

        /// <summary>
        /// Инн
        /// </summary>
        public virtual string Inn { get; set; }

        /// <summary>
        /// Кпп
        /// </summary>
        public virtual string Kpp { get; set; }

        /// <summary>
        /// ОКПО
        /// </summary>
        public virtual string Okpo { get; set; }

        /// <summary>
        /// ОКВЭД
        /// </summary>
        public virtual string Okved { get; set; }

        /// <summary>
        /// ОКАТО
        /// </summary>
        public virtual string Okato { get; set; }

        /// <summary>
        /// ОКОНХ
        /// </summary>
        public virtual string Okonh { get; set; }

        #endregion Регистрационные данные

        #region Юридический адрес

        /// <summary>
        /// Юр.Адрес сторокой целиком
        /// </summary>
        public virtual string Address { get; set; }

        /// <summary>
        /// Индекс
        /// </summary>
        public virtual string JurAddressIndex { get; set; }

        /// <summary>
        /// Страна
        /// </summary>
        public virtual string JurAddressCountry { get; set; }

        /// <summary>
        /// Регион
        /// </summary>
        public virtual string JurAddressRegion { get; set; }

        /// <summary>
        /// Район
        /// </summary>
        public virtual string JurAddressArea { get; set; }

        /// <summary>
        /// Населенный пункт
        /// </summary>
        public virtual string JurAddressLocality { get; set; }

        /// <summary>
        /// Улица
        /// </summary>
        public virtual string JurAddressStreet { get; set; }

        /// <summary>
        /// Дом
        /// </summary>
        public virtual string JurAddressHouse { get; set; }

        /// <summary>
        /// Корпус
        /// </summary>
        public virtual string JurAddressHousing { get; set; }

        /// <summary>
        /// Строение
        /// </summary>
        public virtual string JurAddressBuilding { get; set; }

        /// <summary>
        /// Офис
        /// </summary>
        public virtual string JurAddressOffice { get; set; }
        
        #endregion Юридический адрес

        #region Фактический адрес

        /// <summary>
        /// Факт.Адрес сторокой целиком
        /// </summary>
        public virtual string FactAddress { get; set; }

        /// <summary>
        /// Индекс
        /// </summary>
        public virtual string FactAddressIndex { get; set; }

        /// <summary>
        /// Страна
        /// </summary>
        public virtual string FactAddressCountry { get; set; }

        /// <summary>
        /// Регион
        /// </summary>
        public virtual string FactAddressRegion { get; set; }

        /// <summary>
        /// Район
        /// </summary>
        public virtual string FactAddressArea { get; set; }

        /// <summary>
        /// Населенный пункт
        /// </summary>
        public virtual string FactAddressLocality { get; set; }

        /// <summary>
        /// Улица
        /// </summary>
        public virtual string FactAddressStreet { get; set; }

        /// <summary>
        /// Дом
        /// </summary>
        public virtual string FactAddressHouse { get; set; }

        /// <summary>
        /// Корпус
        /// </summary>
        public virtual string FactAddressHousing { get; set; }

        /// <summary>
        /// Строение
        /// </summary>
        public virtual string FactAddressBuilding { get; set; }

        /// <summary>
        /// Офис
        /// </summary>
        public virtual string FactAddressOffice { get; set; }

        #endregion Юридический адрес

        #region Связь

        /// <summary>
        /// Телефон
        /// </summary>
        public virtual string Phone { get; set; }

        /// <summary>
        /// Факс
        /// </summary>
        public virtual string Fax { get; set; }

        /// <summary>
        /// E-mail
        /// </summary>
        public virtual string Email { get; set; }

        /// <summary>
        /// WEB
        /// </summary>
        public virtual string Web { get; set; }

        #endregion Связь
    }
}
