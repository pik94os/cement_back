﻿using Cement.Core.Enums;

namespace Cement.Core.Entities
{
    /// <summary>
    /// Категория транспортного средства
    /// </summary>
    public class VehicleCategory : BaseEntity
    {
        public virtual string Name { get; set; }

        /// <summary>
        /// Имеет характеристики
        /// </summary>
        public virtual bool HasСharacteristics { get; set; }

        /// <summary>
        /// Тип
        /// </summary>
        public virtual VehicleCategoryType Type { get; set; }
    }
}