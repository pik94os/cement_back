﻿namespace Cement.Core.Entities
{
    /// <summary>
    /// Импортер
    /// </summary>
    public class Importer : BaseEntity
    {
        public virtual string Name { get; set; }
    }
}