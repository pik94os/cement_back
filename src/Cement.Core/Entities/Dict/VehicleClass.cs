﻿namespace Cement.Core.Entities
{
    /// <summary>
    /// Класс автомобиля
    /// </summary>
    public class VehicleClass : BaseEntity
    {
        public virtual string Name { get; set; }
    }
}