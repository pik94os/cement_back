﻿using Cement.Core.Enums;

namespace Cement.Core.Entities
{
    /// <summary>
    /// Водительская категория
    /// </summary>
    public class DriverCategory : BaseEntity
    {
        public virtual string Name { get; set; }

        /// <summary>
        /// Cоответствующее значение на клиенте
        /// </summary>
        public virtual int ClientValue { get; set; }

        /// <summary>
        /// Тип водительской категории
        /// </summary>
        public virtual DriverCategoryType Type { get; set; }
    }
}