﻿namespace Cement.Core.Entities
{
    /// <summary>
    /// Модификация автомобиля
    /// </summary>
    public class VehicleModification : BaseEntity
    {
        public virtual string Name { get; set; }

        public virtual VehicleModel Model { get; set; }

        /// <summary>
        /// Todo Все поля ниже - паразитные (только для отображения в универсальном справочнике). 
        /// При наступлении лучших времен создать атрибут для класса, где и прописывать доп поля
        /// </summary>
        private bool ModelBrand { get; set; }

        private bool ModelCategory { get; set; }

        private bool ModelSubCategory { get; set; }
    }
}