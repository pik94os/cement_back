﻿using Cement.Core.Enums;

namespace Cement.Core.Entities.Dict
{
    /// <summary>
    /// Должность
    /// </summary>
    public class Position : BaseEntity
    {
        public virtual string Name { get; set; }

        public PositionType Type { get; set; }
    }
}