﻿namespace Cement.Core.Entities
{
    /// <summary>
    /// Код ООН (Класс опасности)
    /// </summary>
    public class DangerUnCode : BaseEntity
    {
        public virtual string Name { get; set; }
    }
}