﻿namespace Cement.Core.Entities
{
    /// <summary>
    /// Единица измерения
    /// </summary>
    public class MeasureUnit : BaseEntity
    {
        /// <summary>
        /// Наименование
        /// </summary>
        public virtual string Name { get; set; }

        /// <summary>
        /// Краткое наименование
        /// </summary>
        public virtual string ShortName { get; set; }
        
        /// <summary>
        /// Группа
        /// </summary>
        public virtual MeasureUnitGroup MeasureUnitGroup { get; set; }

        /// <summary>
        /// Национальная
        /// </summary>
        public virtual bool IsNational { get; set; }

        /// <summary>
        /// Не включен в ЕСКК
        /// </summary>
        public virtual bool IsNotInEskk { get; set; }
    }

    /// <summary>
    /// Группа единицы измерения
    /// </summary>
    public enum MeasureUnitGroup
    {
        /// <summary>
        /// Единицы длины
        /// </summary>
        Length = 10,

        /// <summary>
        /// Единицы площади
        /// </summary>
        Square = 20,

        /// <summary>
        /// Единицы объема
        /// </summary>
        Volume = 30,

        /// <summary>
        /// Единицы массы
        /// </summary>
        Weight = 40,

        /// <summary>
        /// Технические единицы
        /// </summary>
        Technical = 50,

        /// <summary>
        /// Единицы времени
        /// </summary>
        Time = 60,

        /// <summary>
        /// Экономические единицы
        /// </summary>
        Economic = 70
    }
}