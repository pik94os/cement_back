﻿namespace Cement.Core.Entities
{
    /// <summary>
    /// Тележка ж/д транспорта
    /// </summary>
    public class Bogie : BaseEntity
    {
        public virtual string Name { get; set; }

        public virtual string Model { get; set; }
    }
}