﻿namespace Cement.Core.Entities
{
    /// <summary>
    /// Габарит ж/д транспорта
    /// </summary>
    public class Size : BaseEntity
    {
        public virtual string Name { get; set; }
    }
}