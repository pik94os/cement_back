﻿namespace Cement.Core.Entities
{
    /// <summary>
    /// Изготовитель ж/д транспорта
    /// </summary>
    public class Fabricator : BaseEntity
    {
        public virtual string Name { get; set; }
    }
}