﻿namespace Cement.Core.Entities
{
    /// <summary>
    /// Особенности моедли ж/д транспорта
    /// </summary>
    public class FeatureModel : BaseEntity
    {
        public virtual string Name { get; set; }
    }
}