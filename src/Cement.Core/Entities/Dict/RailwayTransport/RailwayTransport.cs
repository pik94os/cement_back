﻿namespace Cement.Core.Entities
{
    /// <summary>
    /// Железнодорожный транспорт
    /// </summary>
    public class RailwayTransport : BaseEntity
    {
        /// <summary>
        /// Модель
        /// </summary>
        public virtual string Model { get; set; }

        /// <summary>
        /// Изготовитель
        /// </summary>
        public virtual Fabricator Fabricator { get; set; }

        /// <summary>
        /// Особенности модели
        /// </summary>
        public virtual FeatureModel FeatureModel { get; set; }

        /// <summary>
        /// Тележка
        /// </summary>
        public virtual Bogie Bogie { get; set; }

        /// <summary>
        /// Габариты
        /// </summary>
        public virtual Size Size { get; set; }
        
        /// <summary>
        /// Род
        /// </summary>
        public virtual string Kind { get; set; }

        /// <summary>
        /// Год начала выпуска
        /// </summary>
        public virtual int ReleaseYear { get; set; }

        /// <summary>
        /// Год окончания выпуска
        /// </summary>
        public virtual int EndEditionYear { get; set; }

        /// <summary>
        /// Грузоподъемность, т.
        /// </summary>
        public virtual decimal Capacity { get; set; }

        /// <summary>
        /// Масса тары min, т.
        /// </summary>
        public virtual decimal TareWeightMin { get; set; }

        /// <summary>
        /// Масса тары max, т.
        /// </summary>
        public virtual decimal TareWeightMax { get; set; }

        /// <summary>
        /// Длина по осям автосцепки, мм
        /// </summary>
        public virtual decimal AxesLength { get; set; }

        /// <summary>
        /// Количество осей
        /// </summary>
        public virtual int AxesCount { get; set; }

        /// <summary>
        /// Осевая нагрузка, т.
        /// </summary>
        public virtual decimal AxialLoad { get; set; }

        /// <summary>
        /// Переходная площадка
        /// </summary>
        public virtual bool HasTransitionPlatform { get; set; }

        /// <summary>
        /// Объем кузова, куб.м.
        /// </summary>
        public virtual decimal BodyVolume { get; set; }

        /// <summary>
        /// Калибровка котла
        /// </summary>
        public virtual string BoilerCalibration { get; set; }
    }
}