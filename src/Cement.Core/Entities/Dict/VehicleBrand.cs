﻿namespace Cement.Core.Entities
{
    /// <summary>
    /// Марка автомобиля
    /// </summary>
    public class VehicleBrand : BaseEntity
    {
        public virtual string Name { get; set; }
    }
}