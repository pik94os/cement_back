﻿namespace Cement.Core.Entities
{
    /// <summary>
    /// Упаковщик
    /// </summary>
    public class Packer : BaseEntity
    {
        public virtual string Name { get; set; }
    }
}