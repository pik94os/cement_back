﻿namespace Cement.Core.Entities
{
    /// <summary>
    /// Подкатегория транспортного средства
    /// </summary>
    public class VehicleSubCategory : BaseEntity
    {
        public virtual string Name { get; set; }

        public virtual VehicleCategory Category { get; set; }
    }
}