﻿namespace Cement.Core.Entities
{
    /// <summary>
    /// Страна
    /// </summary>
    public class Country : BaseEntity
    {
        public virtual string Name { get; set; }
    }
}