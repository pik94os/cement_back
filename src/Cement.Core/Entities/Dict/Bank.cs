﻿namespace Cement.Core.Entities
{
    /// <summary>
    /// Банк
    /// </summary>
    public class Bank : BaseEntity
    {
        /// <summary>
        /// Наименование
        /// </summary>
        public virtual string Name { get; set; }

        /// <summary>
        /// Полное наименование
        /// </summary>
        public virtual string FullName { get; set; }

        /// <summary>
        /// Бик
        /// </summary>
        public virtual string Bik { get; set; }

        /// <summary>
        /// Кор.счет
        /// </summary>
        public virtual string Ks { get; set; }
    }
}