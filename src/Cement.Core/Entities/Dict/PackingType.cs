﻿namespace Cement.Core.Entities
{
    /// <summary>
    /// Тип упаковки
    /// </summary>
    public class PackingType : BaseEntity
    {
        public virtual string Name { get; set; }

        public virtual string Description { get; set; }

        /// <summary>
        /// Фото
        /// </summary>
        public virtual EntityFile Photo { get; set; }
    }
}