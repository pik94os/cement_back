﻿namespace Cement.Core.Entities
{
    /// <summary>
    /// Код региона
    /// </summary>
    public class RegionCode : BaseEntity
    {
        public virtual string Name { get; set; }
    }
}