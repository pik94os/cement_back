﻿namespace Cement.Core.Entities
{
    /// <summary>
    /// Подгруппа товара
    /// </summary>
    public class ProductSubGroup : BaseEntity
    {
        public virtual string Name { get; set; }

        public virtual ProductGroup ProductGroup { get; set; }

        /// <summary>
        /// Todo Паразитное поле (только для отображения в универсальном справочнике). 
        /// При наступлении лучших времен создать атрибут для класса, где и прописывать доп поля
        /// </summary>
        private bool ProductGroupType { get; set; }
    }
}