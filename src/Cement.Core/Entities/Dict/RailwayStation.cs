﻿namespace Cement.Core.Entities
{
    /// <summary>
    /// Железнодорожная станция
    /// </summary>
    public class RailwayStation : BaseEntity
    {
        public virtual string Code { get; set; }
        
        public virtual string Name { get; set; }

        public virtual string ShortName { get; set; }

        public virtual string RegionName { get; set; }

        public virtual string RailwayName { get; set; }

        public virtual string RailwayShortName { get; set; }
    }
}