﻿namespace Cement.Core.Entities
{
    /// <summary>
    /// Код страны
    /// </summary>
    public class CountryCode : BaseEntity
    {
        public virtual string Name { get; set; }
    }
}