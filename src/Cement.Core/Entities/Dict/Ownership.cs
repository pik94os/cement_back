﻿namespace Cement.Core.Entities
{
    /// <summary>
    /// Форма собственности
    /// </summary>
    public class Ownership : BaseEntity
    {
        public virtual string Name { get; set; }

        public virtual string ShortName { get; set; }
    }
}