﻿namespace Cement.Core.Entities
{
    /// <summary>
    /// Поставщик услуг
    /// </summary>
    public class ServiceSupplier : BaseEntity
    {
        public virtual string Name { get; set; }
    }
}