﻿namespace Cement.Core.Entities
{
    /// <summary>
    /// Класс опасности
    /// </summary>
    public class DangerClass : BaseEntity
    {
        public virtual string Num { get; set; }
        
        public virtual string Name { get; set; }
    }
}