﻿namespace Cement.Core.Entities
{
    /// <summary>
    /// Модель автомобиля
    /// </summary>
    public class VehicleModel : BaseEntity
    {
        public virtual string Name { get; set; }

        public virtual VehicleBrand Brand { get; set; }

        public virtual VehicleCategory Category { get; set; }

        public virtual VehicleSubCategory SubCategory { get; set; }
    }
}