﻿namespace Cement.Core.Entities
{
    /// <summary>
    /// Подкласс опасности
    /// </summary>
    public class DangerSubClass : BaseEntity
    {
        public virtual string Num { get; set; }

        public virtual string Name { get; set; }

        public virtual DangerClass Class { get; set; }
    }
}