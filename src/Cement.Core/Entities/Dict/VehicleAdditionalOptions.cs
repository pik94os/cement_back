﻿namespace Cement.Core.Entities
{
    /// <summary>
    /// Дополнительные опции автомобиля
    /// </summary>
    public class VehicleAdditionalOptions : BaseEntity
    {
        public virtual string Name { get; set; }
    }
}