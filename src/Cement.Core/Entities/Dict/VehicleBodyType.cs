﻿namespace Cement.Core.Entities
{
    /// <summary>
    /// Тип кузова автомобиля
    /// </summary>
    public class VehicleBodyType : BaseEntity
    {
        public virtual string Name { get; set; }
    }
}