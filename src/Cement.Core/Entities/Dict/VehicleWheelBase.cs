﻿namespace Cement.Core.Entities
{
    /// <summary>
    /// Колесная база ТС
    /// </summary>
    public class VehicleWheelBase : BaseEntity
    {
        public virtual string Name { get; set; }
    }
}