﻿using Cement.Core.Enums;

namespace Cement.Core.Entities.Dict
{
    /// <summary>
    /// Стаж
    /// </summary>
    public class Experience : BaseEntity
    {
        /// <summary>
        /// Наименование
        /// </summary>
        public virtual string Name { get; set; }

        /// <summary>
        /// Тип стажа
        /// </summary>
        public virtual ExperienceType Type { get; set; }

        public Experience()
        {
            Type = ExperienceType.Driving;
        }
    }
}