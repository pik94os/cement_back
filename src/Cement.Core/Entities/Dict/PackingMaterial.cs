﻿namespace Cement.Core.Entities
{
    /// <summary>
    /// Материал упаковки
    /// </summary>
    public class PackingMaterial : BaseEntity
    {
        public virtual string Name { get; set; }
    }
}