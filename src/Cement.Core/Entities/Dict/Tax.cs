﻿namespace Cement.Core.Entities
{
    /// <summary>
    /// Налог (ставка в %)
    /// </summary>
    public class Tax : BaseEntity
    {
        /// <summary>
        /// Ставка (от 0 до 1)
        /// </summary>
        public virtual decimal Rate { get; set; }

        /// <summary>
        /// Ставка строкой
        /// </summary>
        public virtual string StringValue { get { return this.Rate*100 + " %"; } }
    }
}