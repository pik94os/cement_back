﻿using Cement.Core.Enums;

namespace Cement.Core.Entities
{
    /// <summary>
    /// Группа товара
    /// </summary>
    public class ProductGroup : BaseEntity
    {
        public virtual string Name { get; set; }

        public virtual ProductType ProductType { get; set; }
    }
}