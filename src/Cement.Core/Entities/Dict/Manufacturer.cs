﻿namespace Cement.Core.Entities
{
    /// <summary>
    /// Производитель
    /// </summary>
    public class Manufacturer : BaseEntity
    {
        public virtual string Name { get; set; }
    }
}