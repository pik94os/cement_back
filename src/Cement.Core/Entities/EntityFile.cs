﻿namespace Cement.Core.Entities
{
    /// <summary>
    /// Поле-файл сущности
    /// </summary>
    public class EntityFile : BaseEntity
    {
        /// <summary>
        /// Наименование файла (без расширения)
        /// </summary>
        public virtual string Name { get; set; }

        /// <summary>
        /// Расширение
        /// </summary>
        public virtual string Extension { get; set; }

        /// <summary>
        /// Контрольная сумма
        /// </summary>
        public virtual string CheckSum { get; set; }

        /// <summary>
        /// Размер (в байтах)
        /// </summary>
        public virtual long Size { get; set; }

        /// <summary>
        /// Полное имя файла - наименование + '.' + расширение
        /// </summary>
        public string FullName
        {
            get { return Name + "." + Extension; }
        }
    }

    public class EntityFileWithData : EntityFile
    {
        public byte[] Data { get; set; }
    }
}