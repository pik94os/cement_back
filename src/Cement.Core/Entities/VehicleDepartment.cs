﻿namespace Cement.Core.Entities
{
    /// <summary>
    /// Транспортное подразделение
    /// </summary>
    public class VehicleDepartment : BaseOrganizationBasedEntity
    {
        public virtual string Name { get; set; } 
    }
}