﻿using System;

namespace Cement.Core.Entities
{
    /// <summary>
    /// Техобслуживание ТС
    /// </summary>
    public class VehicleMaintenance : BaseEntity
    {
        /// <summary>
        /// ТС
        /// </summary>
        public virtual Vehicle Vehicle { get; set; }

        /// <summary>
        /// Дата фиксации
        /// </summary>
        public virtual DateTime? Date { get; set; }

        /// <summary>
        /// Пробег
        /// </summary>
        public virtual decimal? Mileage { get; set; }

        /// <summary>
        /// Наименование
        /// </summary>
        public virtual string Name { get; set; }

        /// <summary>
        /// Автосервис
        /// </summary>
        public virtual Client CarService { get; set; }

        /// <summary>
        /// Мастер
        /// </summary>
        public virtual string Master { get; set; }

        /// <summary>
        /// Дата выполнения
        /// </summary>
        public virtual DateTime? WorkDate { get; set; }

        /// <summary>
        /// Стоимость
        /// </summary>
        public virtual decimal? Cost { get; set; }
        
        /// <summary>
        /// Заметки
        /// </summary>
        public virtual string Notes { get; set; }

        /// <summary>
        /// Водитель
        /// </summary>
        public virtual Employee Driver { get; set; }

        /// <summary>
        /// Документ (наименование)
        /// </summary>
        public virtual string DocName { get; set; }

        /// <summary>
        /// Документ (файл)
        /// </summary>
        public virtual EntityFile DocFile { get; set; }
    }
}