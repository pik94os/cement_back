﻿namespace Cement.Core.Entities
{
    /// <summary>
    /// Документ (приход/расход) Счет-фактура
    /// </summary>
    public class AccountingInOutDocInvoice : AccountingInOutDocBase
    {
        public AccountingInOutDocWayBillSale AccountingInOutDocWayBill { get; set; }
    }
}