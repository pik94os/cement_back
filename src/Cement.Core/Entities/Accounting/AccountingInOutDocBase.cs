﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Cement.Core.Enums;
using Newtonsoft.Json;

namespace Cement.Core.Entities
{
    /// <summary>
    /// Документ (приход/расход)
    /// </summary>
    public class AccountingInOutDocBase : BaseEntity, IDocument
    {
        public virtual AccountingInOutDocType AccountingInOutDocType { get; set; }

        public virtual decimal Sum { get; set; }

        /// <summary>
        /// Номер
        /// </summary>
        public virtual string No { get; set; }

        /// <summary>
        /// Дата
        /// </summary>
        public virtual DateTime? Date { get; set; }

        /// <summary>
        /// Статус
        /// </summary>
        public virtual DocumentState DocumentState { get; set; }

        public string Name { get { return string.Empty; } }

        public DocumentType Type { get { return DocumentType.AccountingInOutDocument; } }

        public string State { get { return string.Empty; } }

        public List<string> RelatedDocumentIds { get { return null; } }

        public virtual Organization OrganizationFrom { get; set; }

        public virtual Organization OrganizationTo { get; set; }

        #region Поля хэлперы

        /// <summary>
        /// Не хранимое! Список идентификаторов связанных документов
        /// </summary>
        [JsonProperty(PropertyName = "p_dst")]
        public List<long> DstItems { get; set; }

        #endregion Поля хэлперы
    }

    /// <summary>
    /// Этап маршрута бухгалтерского документа (приход/расход)
    /// </summary>
    public class AccountingInOutDocRoute : DocumentRoute<AccountingInOutDocBase> { }

    public enum AccountingInOutDocType
    {
        [Display(Name = "Накладная (продажа)")]
        WayBillSale = 10,

        [Display(Name = "Акт выполненных работ")]
        WorkCompleteAct = 20,

        [Display(Name = "Счет-фактура")]
        Invoice = 30
    }
}