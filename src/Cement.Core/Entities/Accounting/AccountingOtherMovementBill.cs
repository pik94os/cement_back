﻿using System.ComponentModel.DataAnnotations;

namespace Cement.Core.Entities
{
    /// <summary>
    /// Иной бухгалтерский документ: Накладная (перемещение)
    /// </summary>
    public class AccountingOtherMovementBill : AccountingOtherBase
    {
        public virtual AccountingOtherMovementBillKind AccountingOtherMovementBillKind { get; set; }
    }

    public enum AccountingOtherMovementBillKind
    {
        [Display(Name = "Товарная накладная ТОРГ-13")]
        PackingListTorg13 = 110,

        [Display(Name = "Накладная на перемещение")]
        BillToMove = 120,
    }
}