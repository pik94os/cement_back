﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Cement.Core.Entities
{
    /// <summary>
    /// Документ (приход/расход) Накладная (продажа)
    /// </summary>
    public class AccountingInOutDocWayBillSale : AccountingInOutDocWayBill
    {
        public AccountingInOutDocWayBillSaleKind AccountingInOutDocWayBillSaleKind { get; set; }

        public AccountingInOutDocWayBillSaleShippingKind AccountingInOutDocWayBillSaleShippingKind { get; set; }

        /// <summary>
        /// Использовано
        /// </summary>
        public virtual DateTime? UsedAt { get; set; }
    }

    public enum AccountingInOutDocWayBillSaleKind
    {
        [Display(Name = "Товарная накладная ТОРГ-12")]
        PackingListTorg12 = 110,

        [Display(Name = "Расходная накладная")]
        SalesInvoice = 120,

        [Display(Name = "Универсальный передаточный документ")]
        UniversalTransferDoc = 130
    }

    public enum AccountingInOutDocWayBillSaleShippingKind
    {
        [Display(Name = "Товарно-транспортная накладная")]
        ProductTransportWaybill = 10,

        [Display(Name = "Транспортная накладная")]
        TransportWaybill = 20
    }
}