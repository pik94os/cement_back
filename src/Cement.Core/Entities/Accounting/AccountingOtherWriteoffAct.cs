﻿using System.ComponentModel.DataAnnotations;

namespace Cement.Core.Entities
{
    /// <summary>
    /// Иной бухгалтерский документ: Акт на списание
    /// </summary>
    public class AccountingOtherWriteoffAct : AccountingOtherBase
    {
        public virtual AccountingOtherWriteoffActKind AccountingOtherWriteoffActKind { get; set; }
    }

    public enum AccountingOtherWriteoffActKind
    {
        [Display(Name = "Акт на списание ТОРГ-15")]
        WriteoffActTorg15 = 210,

        [Display(Name = "Акт на списание ТОРГ-16")]
        WriteoffActTorg16 = 220,
    }
}