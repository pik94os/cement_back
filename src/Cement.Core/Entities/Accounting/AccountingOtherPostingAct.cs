﻿namespace Cement.Core.Entities
{
    /// <summary>
    /// Иной бухгалтерский документ: Акт на оприходование
    /// </summary>
    public class AccountingOtherPostingAct : AccountingOtherBase
    {
        ///// <summary>
        ///// Основание
        ///// </summary>
        //public virtual Document Foundation { get; set; }
    }
}