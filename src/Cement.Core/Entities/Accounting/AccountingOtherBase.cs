﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Cement.Core.Enums;
using Newtonsoft.Json;

namespace Cement.Core.Entities
{
    /// <summary>
    /// Базовый иной бухгалтерский документ
    /// </summary>
    public class AccountingOtherBase : BaseEntity, IDocument
    {
        public virtual AccountingOtherDocumentType AccountingOtherDocumentType { get; set; }

        public virtual decimal Sum { get; set; }

        /// <summary>
        /// Номер
        /// </summary>
        public virtual string No { get; set; }

        /// <summary>
        /// Дата
        /// </summary>
        public virtual DateTime? Date { get; set; }

        /// <summary>
        /// Статус
        /// </summary>
        public virtual DocumentState DocumentState { get; set; }

        public string Name { get { return string.Empty; } }

        public DocumentType Type { get { return DocumentType.OtherAccountingDocument; } }

        public string State { get { return string.Empty; } }

        public List<string> RelatedDocumentIds { get { return null; } }
        
        #region Поля хэлперы
        
        /// <summary>
        /// Не хранимое! Список идентификаторов связанных документов
        /// </summary>
        [JsonProperty(PropertyName = "p_dst")]
        public List<long> DstItems { get; set; }

        #endregion Поля хэлперы
    }

    /// <summary>
    /// Этап маршрута иного бухгалтерского документа
    /// </summary>
    public class AccountingOtherRoute : DocumentRoute<AccountingOtherBase> { }

    public enum AccountingOtherDocumentType
    {
        [Display(Name = "Накладная (перемещение)")]
        MovementBill = 10,

        [Display(Name = "Акт на списание")]
        WriteoffAct = 20,

        [Display(Name = "Акт на оприходование")]
        PostingAct = 30
    }
}