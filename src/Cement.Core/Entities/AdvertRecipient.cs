﻿namespace Cement.Core.Entities
{
    /// <summary>
    /// Получатель объявления
    /// </summary>
    public class AdvertRecipient : BaseEntity
    {
        /// <summary>
        /// Объявление
        /// </summary>
        public virtual Advert Advert { get; set; }

        /// <summary>
        /// Получатель
        /// </summary>
        public virtual Employee Recipient { get; set; }
    }
}