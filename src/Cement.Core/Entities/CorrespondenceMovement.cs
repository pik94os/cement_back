﻿using Cement.Core.Enums;

namespace Cement.Core.Entities
{
    /// <summary>
    /// Движение корреспонденции
    /// </summary>
    public abstract class CorrespondenceMovement : BaseEntity, IDocumentMovement
    {
        /// <summary>
        /// Документ
        /// </summary>
        public virtual Correspondence Document { get; set; }

        /// <summary>
        /// Направление движения
        /// </summary>
        public virtual DocumentMovementType MovementType { get; private set; }

        /// <summary>
        /// Сотрудник
        /// </summary>
        public virtual Employee Employee { get; set; }

        /// <summary>
        /// Движение в архиве
        /// </summary>
        public virtual bool IsArchived { get; set; }

        /// <summary>
        /// Сотрудник - встречная сторона
        /// </summary>
        public virtual Employee Meeting { get; set; }

        /// <summary>
        /// Этап маршрута
        /// </summary>
        public virtual CorrespondenceRoute Route { get; set; }

        public long DocumentId { get { return Document.Id; } }

        public long? EmployeeId { get { return Employee != null ? Employee.Id : (long?)null; } }

        public long? OrganizationId { get { return null; } }
    }

    /// <summary>
    /// Входящяя корреспонденция
    /// </summary>
    public class IncomingCorrespondence : CorrespondenceMovement
    {
        /// <summary>
        /// Свойства корреспонденции
        /// </summary>
        public virtual DocumentProperty Properties { get; set; }

        public override DocumentMovementType MovementType { get { return DocumentMovementType.Incoming; } }
    }

    /// <summary>
    /// Исходящяя корреспонденция
    /// </summary>
    public class OutcomingCorrespondence : CorrespondenceMovement
    {
        public override DocumentMovementType MovementType { get { return DocumentMovementType.Outcoming; } }
    }
}