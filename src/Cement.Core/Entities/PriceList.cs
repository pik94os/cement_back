﻿using System;
using System.Collections.Generic;
using Cement.Core.Enums;

namespace Cement.Core.Entities
{
    /// <summary>
    /// Прайс-лист
    /// </summary>
    public class PriceList : BaseOrganizationBasedEntity, IDocument
    {
        /// <summary>
        /// Наименование
        /// </summary>
        public virtual string Name { get; set; }

        /// <summary>
        /// Замена прайс-листа
        /// </summary>
        public virtual PriceList ReplacementPriceList { get; set; }

        /// <summary>
        /// Действует с
        /// </summary>
        public virtual DateTime? DateStart { get; set; }

        /// <summary>
        /// Действует по
        /// </summary>
        public virtual DateTime? DateEnd { get; set; }

        /// <summary>
        /// Примечание (не хранимое)
        /// </summary>
        public virtual string Description { get; set; }

        /// <summary>
        /// Примечание
        /// </summary>
        public virtual byte[] DescriptionBytes { get; set; }

        /// <summary>
        /// Статус
        /// </summary>
        public virtual PriceListStatus Status { get; set; }

        /// <summary>
        /// json-строка товаров
        /// </summary>
        public virtual string ProductsJsonString { get; set; }

        public DocumentType Type { get { return DocumentType.Message; } }

        public InternalDocument MessageDocument { get; set; }

        public DateTime? Date { get; private set; }

        public string State { get { return string.Empty; } }

        public DocumentState DocumentState { get; set; }

        public string No { get { return string.Empty; } }

        #region не хранимые поля (для создания письма)

        /// <summary>
        /// Автор
        /// </summary>
        public virtual Employee Author { get; set; }

        /// <summary>
        /// На подпись
        /// </summary>
        public virtual Employee Signer { get; set; }

        /// <summary>
        /// Срок рассмотрения
        /// </summary>
        public virtual DateTime? ReviewBefore { get; set; }

        /// <summary>
        /// Важность
        /// </summary>
        public virtual ImportanceType Importance { get; set; }

        /// <summary>
        /// Секретность
        /// </summary>
        public virtual SecrecyType Secrecy { get; set; }

        /// <summary>
        /// Список идентификаторов связанных документов
        /// </summary>
        public virtual List<string> RelatedDocumentIds { get; set; }

        /// <summary>
        /// Файл
        /// </summary>
        public virtual EntityFile File { get; set; }

        #endregion
    }
}
