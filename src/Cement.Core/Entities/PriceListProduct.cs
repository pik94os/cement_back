﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cement.Core.Entities
{
    /// <summary>
    /// Товар/услуга прайс-листа
    /// </summary>
    public class PriceListProduct : BaseEntity
    {
        /// <summary>
        /// Товар/услуга
        /// </summary>
        public virtual Product Product { get; set; }

        /// <summary>
        /// Прайс-лист
        /// </summary>
        public virtual PriceList PriceList { get; set; }

        /// <summary>
        /// Налог
        /// </summary>
        public virtual Tax Tax { get; set; }

        /// <summary>
        /// Единица измерения
        /// </summary>
        public virtual MeasureUnit MeasureUnit { get; set; }

        /// <summary>
        /// Цена
        /// </summary>
        public virtual decimal Price { get; set; }
    }
}
