﻿using System;

namespace Cement.Core.Entities
{
    /// <summary>
    /// Комментарий события персонального клиента
    /// </summary>
    public class PersonalClientEventComment : BaseEntity
    {
        /// <summary>
        /// Событие персонального клиента
        /// </summary>
        public virtual PersonalClientEvent PersonalClientEvent { get; set; }

        /// <summary>
        /// Автор комментария
        /// </summary>
        public virtual Employee Employee { get; set; }

        /// <summary>
        /// Дата/Время
        /// </summary>
        public virtual DateTime? DateTime { get; set; }

        /// <summary>
        /// Текст комментария
        /// </summary>
        public virtual string Text { get; set; }
    }
}