﻿using System;
using System.Collections.Generic;
using Cement.Core.Enums;

namespace Cement.Core.Entities
{
    /// <summary>
    /// Событие персонального клиента
    /// </summary>
    public class PersonalClientEvent : BaseEntity
    {
        /// <summary>
        /// Персональный Клиент
        /// </summary>
        public virtual PersonalClient PersonalClient { get; set; }

        /// <summary>
        /// Дата/Время
        /// </summary>
        public virtual DateTime? EventDateTime { get; set; }

        /// <summary>
        /// Дата отправки в архив
        /// </summary>
        public virtual DateTime? ArchivedAt { get; set; }

        /// <summary>
        /// Тема
        /// </summary>
        public virtual string Theme { get; set; }

        /// <summary>
        /// Описание
        /// </summary>
        public virtual string Description { get; set; }

        /// <summary>
        /// Тип напоминания
        /// </summary>
        public virtual PersonalClientEventReminderType ReminderType { get; set; }


        #region Поля хэлперы
        /// <summary>
        /// Не хранимое! Список идентификаторов связанных документов
        /// </summary>
        public virtual List<string> RelatedDocumentIds { get; set; }

        public virtual string TimeString { get; set; }
        #endregion Поля хэлперы
    }
}