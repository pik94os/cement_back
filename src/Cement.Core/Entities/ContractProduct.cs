﻿namespace Cement.Core.Entities
{
    /// <summary>
    /// Товар/услуга договора
    /// </summary>
    public class ContractProduct : BaseEntity
    {
        /// <summary>
        /// Товар/услуга
        /// </summary>
        public virtual Product Product { get; set; }

        /// <summary>
        /// Договор
        /// </summary>
        public virtual Contract Contract { get; set; }

        /// <summary>
        /// Налог
        /// </summary>
        public virtual Tax Tax { get; set; }

        /// <summary>
        /// Единица измерения
        /// </summary>
        public virtual MeasureUnit MeasureUnit { get; set; }

        /// <summary>
        /// Цена
        /// </summary>
        public virtual decimal Price { get; set; }
    }
}
