﻿using System;
using System.Collections.Generic;
using Cement.Core.Enums;
using Newtonsoft.Json;

namespace Cement.Core.Entities
{
    /// <summary>
    /// Объявление
    /// </summary>
    public class Advert : BaseOrganizationBasedEntity, IDocument
    {
        /// <summary>
        /// Создатель
        /// </summary>
        public virtual Employee Creator { get; set; }

        /// <summary>
        /// Автор
        /// </summary>
        public virtual Employee Author { get; set; }

        /// <summary>
        /// Наименование - составное поле
        /// </summary>
        public virtual string Name { get; set; }

        /// <summary>
        /// Дата показа
        /// </summary>
        public virtual DateTime? Date { get; set; }

        /// <summary>
        /// Тема
        /// </summary>
        public virtual string Subject { get; set; }

        /// <summary>
        /// Краткое содержание
        /// </summary>
        public virtual byte[] Content { get; set; }

        /// <summary>
        /// Важность
        /// </summary>
        public virtual ImportanceType Importance { get; set; }

        /// <summary>
        /// Секретность
        /// </summary>
        public virtual SecrecyType Secrecy { get; set; }

        /// <summary>
        /// Файл
        /// </summary>
        public virtual EntityFile File { get; set; }

        /// <summary>
        /// Статус 
        /// </summary>
        public virtual DocumentState DocumentState { get; set; }

        public DocumentType Type { get { return DocumentType.Advert; } }

        public string State { get { return string.Empty; } }

        public string No { get { return string.Empty; } }

        public List<string> RelatedDocumentIds { get { return null; } }

        #region Поля хэлперы

        /// <summary>
        /// Не хранимое! Краткое содержимое строкой
        /// </summary>
        [JsonProperty(PropertyName = "p_content")]
        public string ContentString { get; set; }

        /// <summary>
        /// Не хранимое! Список идентификаторов сотрудников
        /// </summary>
        [JsonProperty(PropertyName = "p_employees")]
        public List<long> RecipientIds { get; set; }

        #endregion Поля хэлперы
    }
}