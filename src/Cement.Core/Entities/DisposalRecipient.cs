﻿namespace Cement.Core.Entities
{
    /// <summary>
    /// Получатель приказа
    /// </summary>
    public class DisposalRecipient : BaseEntity
    {
        /// <summary>
        /// Приказ
        /// </summary>
        public virtual InternalDocument Disposal { get; set; }

        /// <summary>
        /// Получатель
        /// </summary>
        public virtual Employee Recipient { get; set; }
    }
}