﻿using System;

namespace Cement.Core.Entities
{
    /// <summary>
    /// Запчасти/услуги
    /// </summary>
    public class VehicleSparePartService : BaseEntity
    {
        /// <summary>
        /// ТС
        /// </summary>
        public virtual Vehicle Vehicle { get; set; }

        /// <summary>
        /// Дата
        /// </summary>
        public virtual DateTime? Date { get; set; }
        
        /// <summary>
        /// Пробег
        /// </summary>
        public virtual decimal? Mileage { get; set; }

        /// <summary>
        /// Наименование
        /// </summary>
        public virtual string Name { get; set; }

        /// <summary>
        /// Автосервис
        /// </summary>
        public virtual Client CarService { get; set; }
        
        /// <summary>
        /// Мастер
        /// </summary>
        public virtual string Master { get; set; }

        /// <summary>
        /// Единица измерения
        /// </summary>
        public virtual MeasureUnit MeasureUnit { get; set; }

        /// <summary>
        /// Количество
        /// </summary>
        public virtual decimal? Amount { get; set; }

        /// <summary>
        /// Дата начала
        /// </summary>
        public virtual DateTime? DateStart { get; set; }

        /// <summary>
        /// Дата окончания
        /// </summary>
        public virtual DateTime? DateEnd { get; set; }

        /// <summary>
        /// Стоимость
        /// </summary>
        public virtual decimal? Cost { get; set; }

        /// <summary>
        /// Водитель
        /// </summary>
        public virtual Employee Driver { get; set; }

        /// <summary>
        /// Документ (наименование)
        /// </summary>
        public virtual string DocName { get; set; }

        /// <summary>
        /// Документ (файл)
        /// </summary>
        public virtual EntityFile DocFile { get; set; }
    }
}