﻿using System;

namespace Cement.Core.Entities
{
    /// <summary>
    /// ДТП
    /// </summary>
    public class VehicleAccident : BaseEntity
    {
        /// <summary>
        /// ТС
        /// </summary>
        public virtual Vehicle Vehicle { get; set; }

        /// <summary>
        /// Дата
        /// </summary>
        public virtual DateTime? Date { get; set; }

        /// <summary>
        /// Кол-во ТС
        /// </summary>
        public virtual int? Count { get; set; }
        
        /// <summary>
        /// Ущерб
        /// </summary>
        public virtual decimal? Damage { get; set; }

        /// <summary>
        /// Описание
        /// </summary>
        public virtual string Description { get; set; }

        /// <summary>
        /// Сумма
        /// </summary>
        public virtual decimal? Cost { get; set; }

        /// <summary>
        /// Водитель
        /// </summary>
        public virtual Employee Driver { get; set; }
    }
}