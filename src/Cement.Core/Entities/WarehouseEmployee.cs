﻿namespace Cement.Core.Entities
{
    /// <summary>
    /// Сотрудник склада
    /// </summary>
    public class WarehouseEmployee : BaseEntity
    {
        /// <summary>
        /// Склад
        /// </summary>
        public virtual Warehouse Warehouse { get; set; }
        
        /// <summary>
        /// Сотрудник
        /// </summary>
        public virtual Employee Employee { get; set; }
    }
}