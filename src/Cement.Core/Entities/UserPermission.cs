﻿using Cement.Core.Enums;

namespace Cement.Core.Entities
{
    public class UserPermission : BaseEntity
    {
        public virtual User User { get; set; }

        public virtual AccessType AccessType { get; set; }

        public virtual string Permission { get; set; }

        public virtual string Description { get; set; }
    }
}