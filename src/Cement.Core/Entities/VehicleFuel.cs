﻿using System;

namespace Cement.Core.Entities
{
    /// <summary>
    /// Топливо ТС
    /// </summary>
    public class VehicleFuel : BaseEntity
    {
        /// <summary>
        /// ТС
        /// </summary>
        public virtual Vehicle Vehicle { get; set; }

        /// <summary>
        /// Дата
        /// </summary>
        public virtual DateTime? Date { get; set; }

        /// <summary>
        /// Наименование
        /// </summary>
        public virtual string Name { get; set; }

        /// <summary>
        /// АЗС
        /// </summary>
        public virtual Client FuelStation { get; set; }

        /// <summary>
        /// Единица измерения
        /// </summary>
        public virtual MeasureUnit MeasureUnit { get; set; }

        /// <summary>
        /// Количество
        /// </summary>
        public virtual decimal? Amount { get; set; }

        /// <summary>
        /// Стоимость
        /// </summary>
        public virtual decimal? Cost { get; set; }
        
        /// <summary>
        /// Водитель
        /// </summary>
        public virtual Employee Driver { get; set; }

        /// <summary>
        /// Документ (наименование)
        /// </summary>
        public virtual string DocName { get; set; }

        /// <summary>
        /// Документ (файл)
        /// </summary>
        public virtual EntityFile DocFile { get; set; }
    }
}