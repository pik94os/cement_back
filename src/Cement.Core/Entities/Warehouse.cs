﻿using System.Collections.Generic;
using Cement.Core.Enums;
using Newtonsoft.Json;

namespace Cement.Core.Entities
{
    /// <summary>
    /// Склад
    /// </summary>
    public class Warehouse : BaseOrganizationBasedEntity
    {
        /// <summary>
        /// Тип
        /// </summary>
        public virtual WarehouseType Type { get; set; }

        /// <summary>
        /// Тип (строкой)
        /// </summary>
        public virtual string TypeStr { get; set; }

        /// <summary>
        /// Наименование
        /// </summary>
        public virtual string Name { get; set; }

        /// <summary>
        /// Собственные подъездные пути
        /// </summary>
        public virtual bool IsOwnDriveways { get; set; }

        /// <summary>
        /// Владелец подъездных путей
        /// </summary>
        public virtual Client DrivewaysOwner { get; set; }

        #region Адрес

        /// <summary>
        /// Индекс
        /// </summary>
        public virtual string AddressIndex { get; set; }

        /// <summary>
        /// Страна
        /// </summary>
        public virtual string AddressCountry { get; set; }

        /// <summary>
        /// Регион
        /// </summary>
        public virtual string AddressRegion { get; set; }

        /// <summary>
        /// Район
        /// </summary>
        public virtual string AddressArea { get; set; }

        /// <summary>
        /// Населенный пункт
        /// </summary>
        public virtual string AddressLocality { get; set; }

        /// <summary>
        /// Улица
        /// </summary>
        public virtual string AddressStreet { get; set; }

        /// <summary>
        /// Дом
        /// </summary>
        public virtual string AddressHouse { get; set; }

        /// <summary>
        /// Корпус
        /// </summary>
        public virtual string AddressHousing { get; set; }

        /// <summary>
        /// Строение
        /// </summary>
        public virtual string AddressBuilding { get; set; }

        /// <summary>
        /// Офис
        /// </summary>
        public virtual string AddressOffice { get; set; }

        /// <summary>
        /// Коментарий к адресу
        /// </summary>
        public virtual string AddressComment { get; set; }

        /// <summary>
        /// Адрес строкой
        /// </summary>
        public virtual string Address { get; set; }

        /// <summary>
        /// Широта
        /// </summary>
        public virtual decimal Latitude { get; set; }

        /// <summary>
        /// Долгота
        /// </summary>
        public virtual decimal Longitude { get; set; }
        
        #endregion Адрес

        #region Отгрузочные реквизиты

        /// <summary>
        /// Код предприятия
        /// </summary>
        public virtual string CompanyCode { get; set; }

        /// <summary>
        /// Станция
        /// </summary>
        public virtual RailwayStation RailwayStation { get; set; }

        #endregion Отгрузочные реквизиты

        #region Поля хэлперы

        /// <summary>
        /// Не хранимое! Список идентификаторов сотрудников
        /// </summary>
        [JsonProperty(PropertyName = "p_employes")]
        public List<long> Employes { get; set; }

        #endregion Поля хэлперы
    }
}