﻿namespace Cement.Core.Entities
{
    /// <summary>
    /// Приложение к договору
    /// </summary>
    public class ContractApplication : ContractAnnexBase
    {
        public static string DocumentName = "Приложение";
    }
}