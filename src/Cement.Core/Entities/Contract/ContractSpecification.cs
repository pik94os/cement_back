﻿namespace Cement.Core.Entities
{
    /// <summary>
    /// Спецификация к договору
    /// </summary>
    public class ContractSpecification : ContractAnnexBase
    {
        public static string DocumentName = "Спецификация";
    }
}