﻿using Cement.Core.Enums;

namespace Cement.Core.Entities
{
    /// <summary>
    /// Движение договора
    /// </summary>
    public abstract class ContractMovement : BaseEntity, IDocumentMovement
    {
        /// <summary>
        /// Документ
        /// </summary>
        public virtual Contract Document { get; set; }

        /// <summary>
        /// Направление движения
        /// </summary>
        public virtual DocumentMovementType MovementType { get; private set; }

        /// <summary>
        /// Организация
        /// </summary>
        public virtual Organization Organization { get; set; }

        /// <summary>
        /// Сотрудник
        /// </summary>
        public virtual Employee Employee { get; set; }

        /// <summary>
        /// Движение в архиве
        /// </summary>
        public virtual bool IsArchived { get; set; }

        public long DocumentId { get { return Document.Id; } }

        public long? EmployeeId { get { return Employee != null ? Employee.Id : (long?)null; } }

        public long? OrganizationId { get { return Organization != null ? Organization.Id : (long?)null; } }
    }

    /// <summary>
    /// Входящий договор
    /// </summary>
    public class IncomingContract : ContractMovement
    {
        public override DocumentMovementType MovementType { get { return DocumentMovementType.Incoming; } }
    }

    /// <summary>
    /// Исходящий договор
    /// </summary>
    public class OutcomingContract : ContractMovement
    {
        public override DocumentMovementType MovementType { get { return DocumentMovementType.Outcoming; } }
    }
}