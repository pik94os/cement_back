﻿using Cement.Core.Enums;

namespace Cement.Core.Entities
{
    /// <summary>
    /// Движение приложения договора
    /// </summary>
    public abstract class ContractAnnexMovement : BaseEntity, IDocumentMovement
    {
        /// <summary>
        /// Документ
        /// </summary>
        public virtual ContractAnnexBase Document { get; set; }

        /// <summary>
        /// Направление движения
        /// </summary>
        public virtual DocumentMovementType MovementType { get; private set; }

        /// <summary>
        /// Организация
        /// </summary>
        public virtual Organization Organization { get; set; }

        /// <summary>
        /// Сотрудник
        /// </summary>
        public virtual Employee Employee { get; set; }

        /// <summary>
        /// Движение в архиве
        /// </summary>
        public virtual bool IsArchived { get; set; }

        public long DocumentId { get { return Document.Id; } }

        public long? EmployeeId { get { return Employee != null ? Employee.Id : (long?)null; } }

        public long? OrganizationId { get { return Organization != null ? Organization.Id : (long?)null; } }
    }

    /// <summary>
    /// Входящее приложение договора
    /// </summary>
    public class IncomingContractAnnex : ContractAnnexMovement
    {
        public override DocumentMovementType MovementType { get { return DocumentMovementType.Incoming; } }
    }

    /// <summary>
    /// Исходящее приложение договора
    /// </summary>
    public class OutcomingContractAnnex : ContractAnnexMovement
    {
        public override DocumentMovementType MovementType { get { return DocumentMovementType.Outcoming; } }
    }
}