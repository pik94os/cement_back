﻿using System;
using System.Collections.Generic;
using Cement.Core.Enums;

namespace Cement.Core.Entities
{
    /// <summary>
    /// Договор
    /// </summary>
    public class Contract : BaseEntity, IDocument
    {
        /// <summary>
        /// Наименование - составное поле
        /// </summary>
        public virtual string Name { get; set; }

        /// <summary>
        /// Номер
        /// </summary>
        public virtual string No { get; set; }

        /// <summary>
        /// Адрес
        /// </summary>
        public virtual DateTime? Date { get; set; }

        /// <summary>
        /// Первая сторона договора - организация-пользователь системы
        /// </summary>
        public virtual Organization Organization { get; set; }

        /// <summary>
        /// Вторая сторона договора - любой клиент
        /// </summary>
        public virtual Client Client { get; set; }

        public DocumentType Type { get { return DocumentType.Contract; } }

        public string State { get { return string.Empty; } }

        public DocumentState DocumentState { get; set; }

        public List<string> RelatedDocumentIds { get { return null; } }
    }
}