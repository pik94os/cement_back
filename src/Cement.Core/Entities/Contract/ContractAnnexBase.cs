﻿using System;
using Cement.Core.Enums;
using Cement.Core.Enums.TermsOfPayment;

namespace Cement.Core.Entities
{
    /// <summary>
    /// Базовая сущность для дочерних документов договора
    /// </summary>
    public abstract class ContractAnnexBase : BaseEntity
    {
        /// <summary>
        /// Наименование - составное поле
        /// </summary>
        public virtual string Name { get; set; }

        /// <summary>
        /// Номер
        /// </summary>
        public virtual string No { get; set; }

        /// <summary>
        /// Адрес
        /// </summary>
        public virtual DateTime? Date { get; set; }

        /// <summary>
        /// Родительский договор
        /// </summary>
        public virtual Contract Contract { get; set; }

        /// <summary>
        /// Тип
        /// </summary>
        public virtual ContractAnnexType Type { get; private set; }

        /// <summary>
        /// Подтип (определяется в рамках конкретного типа договора)
        /// </summary>
        public virtual int SubType { get; set; }

        #region Условия платежа

        public virtual string TermsOfPaymentText { get; set; }

        public virtual PayKind TermsOfPaymentPayKindVal { get; set; }

        public virtual DaysKind? TermsOfPaymentDaysKindVal { get; set; }

        public virtual int? TermsOfPaymentWorkDays { get; set; }

        public virtual int? TermsOfPaymentBankDays { get; set; }

        public virtual int? TermsOfPaymentNextMonthDay { get; set; }

        public virtual DayNumberKind? TermsOfPaymentDayNumberKindVal { get; set; }

        public virtual int? TermsOfPaymentDayNumberDay { get; set; }

        public virtual DateTime? TermsOfPaymentDayNumberFrom { get; set; }

        public virtual DateTime? TermsOfPaymentDayNumberTo { get; set; }

        #endregion Условия платежа
    }
}