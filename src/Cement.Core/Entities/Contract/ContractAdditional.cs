﻿namespace Cement.Core.Entities
{
    /// <summary>
    /// Доп. соглашение к договору
    /// </summary>
    public class ContractAdditional : ContractAnnexBase
    {
        public static string DocumentName = "Дополнительное соглашение";
    }
}