﻿namespace Cement.Core.Entities
{
    public class ContractAnnexPriceList : BaseEntity
    {
        /// <summary>
        /// Дочерний документ договора
        /// </summary>
        public virtual ContractAnnexBase ContractAnnex { get; set; }

        /// <summary>
        /// Договор
        /// </summary>
        public virtual Contract Contract { get; set; }

        /// <summary>
        /// Прайс-лист
        /// </summary>
        public virtual PriceList PriceList { get; set; }

        /// <summary>
        /// Тип (определяется дополнительно к договору или его дочернему документу)
        /// </summary>
        public virtual int Type { get; set; }
    }
}