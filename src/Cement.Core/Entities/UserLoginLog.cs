﻿using System;

namespace Cement.Core.Entities
{
    public class UserLoginLog : BaseEntity
    {
        public virtual DateTime LogedAt { get; set; }

        public virtual bool Success { get; set; }

        public virtual User User { get; set; }
    }
}