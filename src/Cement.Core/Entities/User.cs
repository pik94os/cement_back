﻿using System;
using System.Collections.Generic;
using System.Linq;
using Cement.Core.Auth;

namespace Cement.Core.Entities
{
    public class User : BaseEntity
    {
        public User()
        {
            CreatedAt = DateTime.Now;
            Role = UserRole.Employee;
        }

        public User(UserRole role) : this()
        {
            Role = role;
        }

        public virtual string Login { get; set; }

        private string _email;
        public virtual string Email
        {
            get { return _email ?? Login; }
            set
            {
                if (_email != value)
                {
                    _email = value;
                    this.IsEmailConfirmed = false;
                }
            }
        }
        
        public virtual string Name { get; set; }
        
        public virtual bool IsBlocked { get; set; }

        public virtual DateTime CreatedAt { get; protected set; }
        
        public virtual bool IsEmailConfirmed { get; protected set; }

        public virtual byte[] SecretToken { get; protected set; }
        
        protected internal virtual byte[] PasswordHash { get; set; }

        protected internal virtual byte[] Salt { get; set; }
        
        public virtual Organization Organization { get; set; }

        public virtual Employee Employee { get; set; }

        public virtual UserRole Role { get; private set; }

        public string Password { get; protected set; }

        public bool Authenticate(IPasswordHashProvider hashProvider, string password)
        {
            if (String.IsNullOrWhiteSpace(password) || Salt == null || PasswordHash == null)
            {
                return false;
            }

            password = password.Trim();

            return hashProvider.IsValid(password, Salt, PasswordHash);
        }

        public void SetNewPassword(IPasswordHashProvider hashProvider, string password)
        {
            if (String.IsNullOrWhiteSpace(password))
            {
                throw new Exception("Can not set null password to user.");
            }

            password = password.Trim();

            var solt = hashProvider.CreateSolt();

            var hash = hashProvider.CalcPasswordHash(password, solt);

            Salt = solt;
            PasswordHash = hash;
            Password = password;
        }

        public bool IsInRole(UserRole roleName)
        {
            return Role == roleName;
        }

        private List<long> _warehouseIds;
        public List<long> GetWarehouses()
        {
            return _warehouseIds != null ? _warehouseIds.ToList(): null;
        }
    }

    public enum UserRole
    {
        /// <summary>
        /// Суперадминистратор
        /// </summary>
        Root = 1,

        /// <summary>
        /// Администратор организации
        /// </summary>
        Admin = 128,

        /// <summary>
        /// Рядовой пользователь системы
        /// </summary>
        Employee = 65536
    }
}