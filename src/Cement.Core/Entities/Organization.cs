﻿namespace Cement.Core.Entities
{
    /// <summary>
    /// Организация (пользователь системы)
    /// </summary>
    public class Organization : BaseEntity
    {
        /// <summary>
        /// Клиент
        /// </summary>
        public virtual Client Client { get; set; }

        /// <summary>
        /// Наименование
        /// </summary>
        public virtual string Name { get; set; }

        /// <summary>
        /// Логотип
        /// </summary>
        public virtual EntityFile Logo { get; set; }
    }
}