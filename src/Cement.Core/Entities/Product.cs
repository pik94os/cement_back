﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Cement.Core.Enums;
using Newtonsoft.Json;

namespace Cement.Core.Entities
{
    /// <summary>
    /// Товар (Услуга)
    /// </summary>
    public class Product : BaseEntity
    {
        /// <summary>
        /// Тип
        /// </summary>
        public virtual ProductType Type { get; set; }
        
        /// <summary>
        /// Наименование
        /// </summary>
        public virtual string Name { get; set; }

        /// <summary>
        /// Группа товара
        /// </summary>
        public virtual ProductGroup Group { get; set; }

        /// <summary>
        /// Подгруппа товара
        /// </summary>
        public virtual ProductSubGroup SubGroup { get; set; }

        /// <summary>
        /// Торговая марка
        /// </summary>
        public virtual string Brand { get; set; }

        /// <summary>
        /// Единица измерения (Услуга)
        /// </summary>
        public virtual MeasureUnit MeasureUnit { get; set; }

        /// <summary>
        /// Фото
        /// </summary>
        public virtual EntityFile Photo { get; set; }

        #region Происхождение

        /// <summary>
        /// Страна
        /// </summary>
        public virtual Country Country { get; set; }

        /// <summary>
        /// Производитель
        /// </summary>
        public virtual Client Manufacturer { get; set; }

        /// <summary>
        /// Упаковщик
        /// </summary>
        public virtual Client Packer { get; set; }

        /// <summary>
        /// Импортер
        /// </summary>
        public virtual Client Importer { get; set; }

        #endregion

        #region Коды

        /// <summary>
        /// ОК 005 (ОКП)
        /// </summary>
        public virtual string Okp { get; set; }

        /// <summary>
        /// ТН ВЭД России
        /// </summary>
        public virtual string TnVed { get; set; }

        /// <summary>
        /// Код груза Ж/Д
        /// </summary>
        public virtual string CargoTrainCode { get; set; }

        /// <summary>
        /// Штрих код
        /// </summary>
        public virtual string Barcode { get; set; }

        /// <summary>
        /// ОКУН (Услуга)
        /// </summary>
        public virtual string Okun { get; set; }

        /// <summary>
        /// Артикул (Услуга)
        /// </summary>
        public virtual string Article { get; set; }

        #endregion

        #region Технические условия

        /// <summary>
        /// ГОСТ
        /// </summary>
        public virtual string Gost { get; set; }

        /// <summary>
        /// ГОСТ Р
        /// </summary>
        public virtual string GostR { get; set; }

        /// <summary>
        /// ОСТ
        /// </summary>
        public virtual string Ost { get; set; }

        /// <summary>
        /// СТП
        /// </summary>
        public virtual string Stp { get; set; }

        /// <summary>
        /// ТУ
        /// </summary>
        public virtual string Tu { get; set; }

        #endregion

        #region Сертификаты

        /// <summary>
        /// Номер сертификата сообтветствия
        /// </summary>
        public virtual string ConformityCertificateNumber { get; set; }

        /// <summary>
        /// Скан сертификата сообтветствия
        /// </summary>
        public virtual EntityFile ConformityCertificateScan { get; set; }

        /// <summary>
        /// Скан2 сертификата сообтветствия
        /// </summary>
        public virtual EntityFile ConformityCertificateScan2 { get; set; }

        /// <summary>
        /// Дата начала действия сертификата сообтветствия
        /// </summary>
        public virtual DateTime? ConformityCertificateStartDate { get; set; }

        /// <summary>
        /// Дата окончания действия сертификата сообтветствия
        /// </summary>
        public virtual DateTime? ConformityCertificateEndDate { get; set; }

        /// <summary>
        /// Номер декларации сообтветствия
        /// </summary>
        public virtual string ConformityDeclarationNumber { get; set; }

        /// <summary>
        /// Скан декларации сообтветствия
        /// </summary>
        public virtual EntityFile ConformityDeclarationScan { get; set; }

        /// <summary>
        /// Дата начала действия декларации сообтветствия
        /// </summary>
        public virtual DateTime? ConformityDeclarationStartDate { get; set; }

        /// <summary>
        /// Дата окончания действия декларации сообтветствия
        /// </summary>
        public virtual DateTime? ConformityDeclarationEndDate { get; set; }

        /// <summary>
        /// Номер санитарно-эпидемиологического заключения
        /// </summary>
        public virtual string HealthCertificateNumber { get; set; }

        /// <summary>
        /// Скан санитарно-эпидемиологического заключения
        /// </summary>
        public virtual EntityFile HealthCertificateScan { get; set; }

        /// <summary>
        /// Дата начала действия санитарно-эпидемиологического заключения
        /// </summary>
        public virtual DateTime? HealthCertificateStartDate { get; set; }

        /// <summary>
        /// Дата окончания действия санитарно-эпидемиологического заключения
        /// </summary>
        public virtual DateTime? HealthCertificateEndDate { get; set; }

        /// <summary>
        /// Номер сертификата пожарной безопасности
        /// </summary>
        public virtual string FireSafetyCertificateNumber { get; set; }

        /// <summary>
        /// Скан сертификата пожарной безопасности
        /// </summary>
        public virtual EntityFile FireSafetyCertificateScan { get; set; }

        /// <summary>
        /// Дата начала действия сертификата пожарной безопасности
        /// </summary>
        public virtual DateTime? FireSafetyCertificateStartDate { get; set; }

        /// <summary>
        /// Дата окончания действия сертификата пожарной безопасности
        /// </summary>
        public virtual DateTime? FireSafetyCertificateEndDate { get; set; }

        /// <summary>
        /// Номер декларации о соответствии требованиям пожарной безопасности
        /// </summary>
        public virtual string FireSafetyDeclarationNumber { get; set; }

        /// <summary>
        /// Скан декларации о соответствии требованиям пожарной безопасности
        /// </summary>
        public virtual EntityFile FireSafetyDeclarationScan { get; set; }

        /// <summary>
        /// Дата начала действия декларации о соответствии требованиям пожарной безопасности
        /// </summary>
        public virtual DateTime? FireSafetyDeclarationStartDate { get; set; }

        /// <summary>
        /// Дата окончания действия декларации о соответствии требованиям пожарной безопасности
        /// </summary>
        public virtual DateTime? FireSafetyDeclarationEndDate { get; set; }

        #endregion

        #region Класс опасности (ADR)

        /// <summary>
        /// Класс опасности
        /// </summary>
        public virtual DangerClass DangerClass { get; set; }

        /// <summary>
        /// Подкласс опасности
        /// </summary>
        public virtual DangerSubClass DangerSubClass { get; set; }

        /// <summary>
        /// Код ООН
        /// </summary>
        public virtual DangerUnCode DangerUnCode { get; set; }

        #endregion

        #region Тара/упаковка

        /// <summary>
        /// Тип
        /// </summary>
        public virtual PackingKind Kind { get; set; }

        /// <summary>
        /// Вид (Единичная)
        /// </summary>
        public virtual PackingType TypeSingle { get; set; }

        /// <summary>
        /// Материал (Единичная)
        /// </summary>
        public virtual PackingMaterial MaterialSingle { get; set; }

        /// <summary>
        /// Объем (Единичная)
        /// </summary>
        public virtual decimal? VolumeSingle { get; set; }

        /// <summary>
        /// Единица измерения объема (Единичная)
        /// </summary>
        public virtual MeasureUnit VolumeUnitSingle { get; set; }

        /// <summary>
        /// Вес (Единичная)
        /// </summary>
        public virtual decimal? WeightSingle { get; set; }

        /// <summary>
        /// Единица измерения веса (Единичная)
        /// </summary>
        public virtual MeasureUnit WeightUnitSingle { get; set; }

        /// <summary>
        /// Длинна (Единичная)
        /// </summary>
        public virtual decimal? LengthSingle { get; set; }

        /// <summary>
        /// Ширина (Единичная)
        /// </summary>
        public virtual decimal? WidthSingle { get; set; }

        /// <summary>
        /// Высота (Единичная)
        /// </summary>
        public virtual decimal? HeightSingle { get; set; }

        /// <summary>
        /// Единица измерения габаритов (Единичная)
        /// </summary>
        public virtual MeasureUnit SizeUnitSingle { get; set; }

        /// <summary>
        /// Количество единиц в упаковке (Групповая)
        /// </summary>
        public virtual decimal? UnitsCountGroup { get; set; }

        /// <summary>
        /// Вид (Групповая)
        /// </summary>
        public virtual PackingType TypeGroup { get; set; }

        /// <summary>
        /// Материал (Групповая)
        /// </summary>
        public virtual PackingMaterial MaterialGroup { get; set; }

        /// <summary>
        /// Объем (Групповая)
        /// </summary>
        public virtual decimal? VolumeGroup { get; set; }

        /// <summary>
        /// Единица измерения объема (Групповая)
        /// </summary>
        public virtual MeasureUnit VolumeUnitGroup { get; set; }

        /// <summary>
        /// Вес (Групповая)
        /// </summary>
        public virtual decimal? WeightGroup { get; set; }

        /// <summary>
        /// Единица измерения веса (Групповая)
        /// </summary>
        public virtual MeasureUnit WeightUnitGroup { get; set; }

        /// <summary>
        /// Длинна (Групповая)
        /// </summary>
        public virtual decimal? LengthGroup { get; set; }

        /// <summary>
        /// Ширина (Групповая)
        /// </summary>
        public virtual decimal? WidthGroup { get; set; }

        /// <summary>
        /// Высота (Групповая)
        /// </summary>
        public virtual decimal? HeightGroup { get; set; }

        /// <summary>
        /// Единица измерения габаритов (Групповая)
        /// </summary>
        public virtual MeasureUnit SizeUnitGroup { get; set; }

        /// <summary>
        /// Количество единиц в упаковке (Транспортная)
        /// </summary>
        public virtual decimal? UnitsCountTransport { get; set; }

        /// <summary>
        /// Вид (Транспортная)
        /// </summary>
        public virtual PackingType TypeTransport { get; set; }

        /// <summary>
        /// Материал (Транспортная)
        /// </summary>
        public virtual PackingMaterial MaterialTransport { get; set; }

        /// <summary>
        /// Объем (Транспортная)
        /// </summary>
        public virtual decimal? VolumeTransport { get; set; }

        /// <summary>
        /// Единица измерения объема (Транспортная)
        /// </summary>
        public virtual MeasureUnit VolumeUnitTransport { get; set; }

        /// <summary>
        /// Вес (Транспортная)
        /// </summary>
        public virtual decimal? WeightTransport { get; set; }

        /// <summary>
        /// Единица измерения веса (Транспортная)
        /// </summary>
        public virtual MeasureUnit WeightUnitTransport { get; set; }

        /// <summary>
        /// Длинна (Транспортная)
        /// </summary>
        public virtual decimal? LengthTransport { get; set; }

        /// <summary>
        /// Ширина (Транспортная)
        /// </summary>
        public virtual decimal? WidthTransport { get; set; }

        /// <summary>
        /// Высота (Транспортная)
        /// </summary>
        public virtual decimal? HeightTransport { get; set; }

        /// <summary>
        /// Единица измерения габаритов (Транспортная)
        /// </summary>
        public virtual MeasureUnit SizeUnitTransport { get; set; }

        /// <summary>
        /// Строка отображаемая в поле при редактировании записи товара/услуги
        /// </summary>
        public virtual string PackageDisplayString { get; set; }

        /// <summary>
        /// Строка JSON упаковки (не хранимое)
        /// </summary>
        public virtual string PackageJsonString { get; set; }

        /// <summary>
        /// JSON упаковки
        /// </summary>
        public virtual byte[] PackageJsonBytes { get; set; }
        #endregion

        /// <summary>
        /// Строка отображаемая в поле комплекта
        /// </summary>
        public virtual string ComplectString { get; set; }

        /// <summary>
        /// Идентификаторы товаров в комплекте
        /// </summary>
        public virtual List<long> ComplectProductsIds { get; set; }

        /// <summary>
        /// Отмодерировано
        /// </summary>
        public virtual bool IsModerated { get; set; }
    }

    /// <summary>
    /// Тип упаковки
    /// </summary>
    public enum PackingKind
    {
        /// <summary>
        /// Единичная
        /// </summary>
        Single = 1,

        /// <summary>
        /// Групповая
        /// </summary>
        Group = 2,

        /// <summary>
        /// Транспортная
        /// </summary>
        Transport = 3
    }

    public class ProductPackageProxy
    {
        [JsonProperty(PropertyName = "p_package_kind")]
        public virtual PackingKind PackingKind { get; set; }

        [JsonProperty(PropertyName = "p_package_item_kind")]
        public virtual long? PackingTypeSingleId { get; set; }

        [JsonProperty(PropertyName = "p_package_item_material")]
        public virtual long? PackingMaterialSingleId { get; set; }

        [JsonProperty(PropertyName = "p_package_item_volume")]
        public virtual decimal? VolumeSingle { get; set; }

        [JsonProperty(PropertyName = "p_package_item_volume_unit")]
        public virtual long? VolumeUnitSingleId { get; set; }

        [JsonProperty(PropertyName = "p_package_item_weight")]
        public virtual decimal? WeightSingle { get; set; }

        [JsonProperty(PropertyName = "p_package_item_weight_unit")]
        public virtual long? WeightUnitSingleId { get; set; }

        [JsonProperty(PropertyName = "p_package_item_size_w")]
        public virtual decimal? LengthSingle { get; set; }

        [JsonProperty(PropertyName = "p_package_item_size_d")]
        public virtual decimal? WidthSingle { get; set; }

        [JsonProperty(PropertyName = "p_package_item_size_h")]
        public virtual decimal? HeightSingle { get; set; }

        [JsonProperty(PropertyName = "p_package_item_size_unit")]
        public virtual long? SizeUnitSingleId { get; set; }

        [JsonProperty(PropertyName = "p_package_group_count")]
        public virtual decimal? UnitsCountGroup { get; set; }

        [JsonProperty(PropertyName = "p_package_group_kind")]
        public virtual long? PackingTypeGroupId { get; set; }

        [JsonProperty(PropertyName = "p_package_group_material")]
        public virtual long? PackingMaterialGroupId { get; set; }

        [JsonProperty(PropertyName = "p_package_group_volume")]
        public virtual decimal? VolumeGroup { get; set; }

        [JsonProperty(PropertyName = "p_package_group_volume_unit")]
        public virtual long? VolumeUnitGroupId { get; set; }

        [JsonProperty(PropertyName = "p_package_group_weight")]
        public virtual decimal? WeightGroup { get; set; }

        [JsonProperty(PropertyName = "p_package_group_weight_unit")]
        public virtual long? WeightUnitGroupId { get; set; }

        [JsonProperty(PropertyName = "p_package_group_size_w")]
        public virtual decimal? LengthGroup { get; set; }

        [JsonProperty(PropertyName = "p_package_group_size_d")]
        public virtual decimal? WidthGroup { get; set; }

        [JsonProperty(PropertyName = "p_package_group_size_h")]
        public virtual decimal? HeightGroup { get; set; }

        [JsonProperty(PropertyName = "p_package_group_size_unit")]
        public virtual long? SizeUnitGroupId { get; set; }

        [JsonProperty(PropertyName = "p_package_vehicle_count")]
        public virtual decimal? UnitsCountTransport { get; set; }

        [JsonProperty(PropertyName = "p_package_vehicle_kind")]
        public virtual long? PackingTypeTransportId { get; set; }

        [JsonProperty(PropertyName = "p_package_vehicle_material")]
        public virtual long? PackingMaterialTransportId { get; set; }

        [JsonProperty(PropertyName = "p_package_vehicle_volume")]
        public virtual decimal? VolumeTransport { get; set; }

        [JsonProperty(PropertyName = "p_package_vehicle_volume_unit")]
        public virtual long? VolumeUnitTransportId { get; set; }

        [JsonProperty(PropertyName = "p_package_vehicle_weight")]
        public virtual decimal? WeightTransport { get; set; }

        [JsonProperty(PropertyName = "p_package_vehicle_weight_unit")]
        public virtual long? WeightUnitTransportId { get; set; }

        [JsonProperty(PropertyName = "p_package_vehicle_size_w")]
        public virtual decimal? LengthTransport { get; set; }

        [JsonProperty(PropertyName = "p_package_vehicle_size_d")]
        public virtual decimal? WidthTransport { get; set; }

        [JsonProperty(PropertyName = "p_package_vehicle_size_h")]
        public virtual decimal? HeightTransport { get; set; }

        [JsonProperty(PropertyName = "p_package_vehicle_size_unit")]
        public virtual long? SizeUnitTransportId { get; set; }
    }
}
