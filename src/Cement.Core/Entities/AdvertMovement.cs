﻿using Cement.Core.Enums;

namespace Cement.Core.Entities
{
    /// <summary>
    /// Движение объявления
    /// </summary>
    public abstract class AdvertMovement : BaseEntity, IDocumentMovement
    {
        /// <summary>
        /// Сообщение
        /// </summary>
        public virtual Advert Document { get; set; }

        /// <summary>
        /// Направление движения
        /// </summary>
        public virtual DocumentMovementType MovementType { get; private set; }

        /// <summary>
        /// Сотрудник
        /// </summary>
        public virtual Employee Employee { get; set; }

        /// <summary>
        /// Движение в архиве
        /// </summary>
        public virtual bool IsArchived { get; set; }

        /// <summary>
        /// Этап маршрута
        /// </summary>
        public virtual AdvertRoute Route { get; set; }

        public long DocumentId { get { return Document.Id; } }

        public long? EmployeeId { get { return Employee != null ? Employee.Id : (long?)null; } }

        public long? OrganizationId { get { return null; } }
    }

    /// <summary>
    /// Входящee объявление
    /// </summary>
    public class IncomingAdvert : AdvertMovement
    {
        public override DocumentMovementType MovementType { get { return DocumentMovementType.Incoming; } }
    }

    /// <summary>
    /// Исходящee объявление
    /// </summary>
    public class OutcomingAdvert : AdvertMovement
    {
        public override DocumentMovementType MovementType { get { return DocumentMovementType.Outcoming; } }
    }
}