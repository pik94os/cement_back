﻿using System;
using System.Collections.Generic;
using Cement.Core.Entities.Dict;

namespace Cement.Core.Entities
{
    /// <summary>
    /// Сотрудник
    /// </summary>
    public class Employee : BaseOrganizationBasedEntity
    {
        #region Общие сведения

        /// <summary>
        /// ФИО 
        /// </summary>
        public virtual string Name { get; set; }

        /// <summary>
        /// Фамилия
        /// </summary>
        public virtual string Surname { get; set; }

        /// <summary>
        /// Имя
        /// </summary>
        public virtual string FirstName { get; set; }

        /// <summary>
        /// Отчество
        /// </summary>
        public virtual string Patronymic { get; set; }

        /// <summary>
        /// Должность
        /// </summary>
        public virtual Position Position { get; set; }

        /// <summary>
        /// Отдел
        /// </summary>
        public virtual Department Department { get; set; }

        /// <summary>
        /// ИНН
        /// </summary>
        public virtual string Inn { get; set; }

        /// <summary>
        /// Фото
        /// </summary>
        public virtual EntityFile Photo { get; set; }
        
        #endregion Общие сведения

        #region Паспорт
        /// <summary>
        /// Паспорт: Серия
        /// </summary>
        public virtual string PassportSerie { get; set; }

        /// <summary>
        /// Паспорт: Номер
        /// </summary>
        public virtual string PassportNo { get; set; }

        /// <summary>
        /// Паспорт: Выдан
        /// </summary>
        public virtual string PassportGivenPlace { get; set; }

        /// <summary>
        /// Паспорт: Дата
        /// </summary>
        public virtual DateTime? PassportDate { get; set; }

        /// <summary>
        /// Дата рождения
        /// </summary>
        public virtual DateTime? BirthDate { get; set; }

        /// <summary>
        /// Место рождения
        /// </summary>
        public virtual string BirthPlace { get; set; }

        #endregion Паспорт

        #region Водительское удостоверение
        /// <summary>
        /// Водительское удостоверение: Серия
        /// </summary>
        public virtual string DriveLicSerie { get; set; }

        /// <summary>
        /// Водительское удостоверение: Номер
        /// </summary>
        public virtual string DriveLicNo { get; set; }

        /// <summary>
        /// Водительское удостоверение: Выдано
        /// </summary>
        public virtual string DriveLicGivenPlace { get; set; }

        /// <summary>
        /// Водительское удостоверение: Дата выдачи
        /// </summary>
        public virtual DateTime? DriveLicDate { get; set; }

        /// <summary>
        /// Водительское удостоверение: Действительно до
        /// </summary>
        public virtual DateTime? DriveLicValidDate { get; set; }

        /// <summary>
        /// Водительское удостоверение: Стаж
        /// </summary>
        public virtual Experience DriveExperience { get; set; }
        
        #endregion Водительское удостоверение

        #region Удостоверение тракториста
        /// <summary>
        /// Удостоверение тракториста: Серия
        /// </summary>
        public virtual string TractorDriveLicSerie { get; set; }

        /// <summary>
        /// Удостоверение тракториста: Номер
        /// </summary>
        public virtual string TractorDriveLicNo { get; set; }

        /// <summary>
        /// Удостоверение тракториста: Код
        /// </summary>
        public virtual string TractorDriveLicCode { get; set; }

        /// <summary>
        /// Удостоверение тракториста: Дата выдачи
        /// </summary>
        public virtual DateTime? TractorDriveLicDate { get; set; }

        /// <summary>
        /// Удостоверение тракториста: Действительно до
        /// </summary>
        public virtual DateTime? TractorDriveLicValidDate { get; set; }

        #endregion Удостоверение тракториста

        #region Прописка
        /// <summary>
        /// Индекс
        /// </summary>
        public virtual string Index { get; set; }

        /// <summary>
        /// Страна
        /// </summary>
        public virtual string Country { get; set; }

        /// <summary>
        /// Регион
        /// </summary>
        public virtual string Region { get; set; }

        /// <summary>
        /// Район
        /// </summary>
        public virtual string Area { get; set; }

        /// <summary>
        /// Населенный пункт
        /// </summary>
        public virtual string Locality { get; set; }

        /// <summary>
        /// Улица
        /// </summary>
        public virtual string Street { get; set; }

        /// <summary>
        /// Дом
        /// </summary>
        public virtual string House { get; set; }

        /// <summary>
        /// Квартира
        /// </summary>
        public virtual string Apartment { get; set; }

        #endregion Прописка

        #region Связь
        /// <summary>
        /// Телефон 1
        /// </summary>
        public virtual string Phone1 { get; set; }

        /// <summary>
        /// Телефон 2
        /// </summary>
        public virtual string Phone2 { get; set; }

        /// <summary>
        /// Телефон 3
        /// </summary>
        public virtual string Phone3 { get; set; }

        /// <summary>
        /// Факс
        /// </summary>
        public virtual string Fax { get; set; }

        /// <summary>
        /// Моб. телефон 1
        /// </summary>
        public virtual string MobilePhone1 { get; set; }

        /// <summary>
        /// Моб. телефон 2
        /// </summary>
        public virtual string MobilePhone2 { get; set; }

        /// <summary>
        /// Моб. телефон 3
        /// </summary>
        public virtual string MobilePhone3 { get; set; }

        /// <summary>
        /// E-mail 1
        /// </summary>
        public virtual string Email1 { get; set; }

        /// <summary>
        /// E-mail 2
        /// </summary>
        public virtual string Email2 { get; set; }

        /// <summary>
        /// E-mail 3
        /// </summary>
        public virtual string Email3 { get; set; }

        #endregion Связь


        #region Поля хэлперы
        /// <summary>
        /// Не хранимое! Список идентификаторов водительских категорий
        /// </summary>
        public virtual List<long> DriveCategories { get; set; }

        /// <summary>
        /// Не хранимое! Список идентификаторов категорий тракториста
        /// </summary>
        public virtual List<long> TractorCategories { get; set; }

        #endregion Поля хэлперы

    }
}