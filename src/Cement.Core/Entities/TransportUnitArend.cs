﻿using System;

namespace Cement.Core.Entities
{
    /// <summary>
    /// Аренда транспортной единицы
    /// </summary>
    public class TransportUnitArend : BaseEntity
    {
        /// <summary>
        /// Арендатор (тот, кто взял в аренду)
        /// </summary>
        public virtual Client Arendator { get; set; }

        /// <summary>
        /// Транспортная единица
        /// </summary>
        public virtual TransportUnit TransportUnit { get; set; }

        /// <summary>
        /// Дата начала периода аренды
        /// </summary>
        public virtual DateTime? DateStart { get; set; }

        /// <summary>
        /// Дата окончания периода аренды
        /// </summary>
        public virtual DateTime? DateEnd { get; set; }

        /// <summary>
        /// Аренда действительна
        /// </summary>
        public virtual bool IsActive { get; set; }
    }
}