﻿using System.Data;
using ECM7.Migrator.Framework;

namespace Cement.Core.Migrations
{
    [Migration(2015011400)]
    public class Migration_2015011400 : Migration
    {
        public override void Apply()
        {
            Database.AddColumn("PRODUCT", new Column("PACKAGE_JSON", DbType.Binary));
        }

        public override void Revert()
        {
            Database.RemoveColumn("PRODUCT", "PACKAGE_JSON");
        }
    }
}