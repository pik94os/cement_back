﻿using Cement.Core.Migrations.Extensions;
using ECM7.Migrator.Framework;

namespace Cement.Core.Migrations
{
    [Migration(2015021900)]
    public class Migration_2015021900 : Migration
    {
        public override void Apply()
        {
            Database.AddRefColumn("ORGANIZATION", new RefColumn("LOGO_FILE_ID", "ORGANIZATION_LOGO_FILE", "ENTITY_FILE", "ID"));
            Database.AddRefColumn("EMPLOYEE", new RefColumn("PHOTO_FILE_ID", "EMPLOYEE_PHOTO_FILE", "ENTITY_FILE", "ID"));
        }

        public override void Revert()
        {
            Database.RemoveRefColumn("ORGANIZATION", "LOGO_FILE_ID", "ORGANIZATION_LOGO_FILE");
            Database.RemoveRefColumn("EMPLOYEE", "PHOTO_FILE_ID", "EMPLOYEE_PHOTO_FILE");
        }
    }
}