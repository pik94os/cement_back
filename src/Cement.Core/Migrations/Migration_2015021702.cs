﻿using System.Data;
using Cement.Core.Migrations.Extensions;
using ECM7.Migrator.Framework;

namespace Cement.Core.Migrations
{
    [Migration(2015021702)]
    public class Migration_2015021702 : Migration
    {
        public override void Apply()
        {
            Database.AddColumn("PRODUCT_REQUEST", new Column("PRODUCT_TAX", DbType.Decimal));
            Database.AddColumn("PRODUCT_REQUEST", new Column("PRODUCT_FACT_COUNT", DbType.Int32));
        }

        public override void Revert()
        {
            Database.RemoveColumn("PRODUCT_REQUEST", "PRODUCT_TAX");
            Database.RemoveColumn("PRODUCT_REQUEST", "PRODUCT_FACT_COUNT");
        }
    }
}