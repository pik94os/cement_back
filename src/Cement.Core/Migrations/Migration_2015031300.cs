﻿using System.Data;
using Cement.Core.Enums;
using Cement.Core.Migrations.Extensions;
using ECM7.Migrator.Framework;

namespace Cement.Core.Migrations
{
    [Migration(2015031300)]
    public class Migration_2015031300 : Migration
    {
        public override void Apply()
        {
            Database.AddRefColumn("STOCKROOM_REQUEST", new RefColumn("PRICELIST_PRODUCT_ID", "STOCKREQ_PRICELISTPROD", "PRICE_LIST_PRODUCT", "ID"));
        }

        public override void Revert()
        {
            Database.RemoveRefColumn("STOCKROOM_REQUEST", "PRICELIST_PRODUCT_ID", "STOCKREQ_PRICELISTPROD");
        }
    }
}