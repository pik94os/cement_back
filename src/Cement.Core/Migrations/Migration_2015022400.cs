﻿using Cement.Core.Migrations.Extensions;
using ECM7.Migrator.Framework;

namespace Cement.Core.Migrations
{
    [Migration(2015022400)]
    public class Migration_2015022400 : Migration
    {
        public override void Apply()
        {
            Database.AddEntityTable("DISPOSAL_RECIPIENT",
                new RefColumn("DISPOSAL_ID", "DISP_RECIPIENT_DISPOSAL", "INTERNAL_DOCUMENT", "ID"),
                new RefColumn("RECIPIENT_ID", "DISP_RECIPIENT_EMPLOYEE", "EMPLOYEE", "ID"));
        }

        public override void Revert()
        {
            Database.RemoveEntityTable("DISPOSAL_RECIPIENT");
        }
    }
}