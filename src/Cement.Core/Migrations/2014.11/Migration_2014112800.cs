﻿using System.Data;
using Cement.Core.Migrations.Extensions;
using ECM7.Migrator.Framework;

namespace Cement.Core.Migrations
{
    [Migration(2014112800)]
    public class Migration_2014112800 : Migration
    {
        public override void Apply()
        {
            Database.AddEntityTable("DOC_RELATION",
                new Column("BASE_DOC_ID", DbType.Int64, ColumnProperty.NotNull),
                new Column("DOC_ID", DbType.Int64, ColumnProperty.NotNull),

                new Column("BASE_DOC_TYPE", DbType.Int16, ColumnProperty.NotNull),
                new Column("DOC_TYPE", DbType.Int16, ColumnProperty.NotNull),
                
                new Column("NAME", DbType.String),
                new Column("NO", DbType.String),
                new Column("DOC_DATE", DbType.DateTime),
                new Column("STATE", DbType.String));

            Database.AddIndex("DOC_RELATION_DOC1", false, "DOC_RELATION", "BASE_DOC_ID");
            Database.AddIndex("DOC_RELATION_DOC2", false, "DOC_RELATION", "DOC_ID");

            Database.AddEntityTable("DOC_INCOMING",
                new Column("DOCUMENT_ID", DbType.Int64, ColumnProperty.NotNull),
                new Column("EMPLOYEE_ID", DbType.Int64),
                new Column("ORGANIZATION_ID", DbType.Int64),
                new Column("IS_ARCHIVED", DbType.Boolean));

            Database.AddIndex("DOC_INCOMING_DOC", false, "DOC_INCOMING", "DOCUMENT_ID");
            Database.AddIndex("DOC_INCOMING_EMPL", false, "DOC_INCOMING", "EMPLOYEE_ID");
            Database.AddIndex("DOC_INCOMING_ORG", false, "DOC_INCOMING", "ORGANIZATION_ID");

            Database.AddEntityTable("DOC_OUTCOMING",
                new Column("DOCUMENT_ID", DbType.Int64, ColumnProperty.NotNull),
                new Column("EMPLOYEE_ID", DbType.Int64),
                new Column("ORGANIZATION_ID", DbType.Int64),
                new Column("IS_ARCHIVED", DbType.Boolean));

            Database.AddIndex("DOC_OUTCOMING_DOC", false, "DOC_OUTCOMING", "DOCUMENT_ID");
            Database.AddIndex("DOC_OUTCOMING_EMPL", false, "DOC_OUTCOMING", "EMPLOYEE_ID");
            Database.AddIndex("DOC_OUTCOMING_ORG", false, "DOC_OUTCOMING", "ORGANIZATION_ID");

            //Database.AddTable("HIBERNATE_UNIQUE_KEY", new Column("NEXT_HI", DbType.Int64));
            //Database.ExecuteNonQuery("INSERT INTO HIBERNATE_UNIQUE_KEY(NEXT_HI) VALUES (30000)");

            Database.RemoveColumn("INTERNAL_DOC_MOVEMENT", "PROPERTIES");
            Database.AddColumn("INTERNAL_DOC_MOVEMENT", new Column("PROPERTIES", DbType.Int16));
        }

        public override void Revert()
        {
            //Database.RemoveTable("HIBERNATE_UNIQUE_KEY");

            Database.RemoveEntityTable("DOC_INCOMING");
            Database.RemoveEntityTable("DOC_OUTCOMING");
            Database.RemoveEntityTable("DOC_RELATION");
        }
    }
}