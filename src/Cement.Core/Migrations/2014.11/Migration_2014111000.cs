﻿using System.Data;
using Cement.Core.Migrations.Extensions;
using ECM7.Migrator.Framework;

namespace Cement.Core.Migrations
{
    [Migration(2014111000)]
    public class Migration_2014111000 : Migration
    {
        public override void Apply()
        {
            Database.AddEntityTable("DICT_MEASURE_UNIT",
                new Column("NAME", DbType.String),
                new Column("SHORT_NAME", DbType.String));

            Database.AddEntityTable("CLIENT",
                new Column("NAME", DbType.String));

            Database.AddEntityTable("PRODUCT", 
                new Column("NAME", DbType.String),
                new RefColumn("GROUP_ID", ColumnProperty.Null, "PRODUCT_GROUP", "DICT_PRODUCT_GROUP", "ID"),
                new RefColumn("SUB_GROUP_ID", ColumnProperty.Null, "PRODUCT_SUBGROUP", "DICT_PRODUCT_SUBGROUP", "ID"),
                new Column("BRAND", DbType.String),
                new RefColumn("COUNTRY_ID", ColumnProperty.Null, "PRODUCT_COUNTRY", "DICT_COUNTRY", "ID"),
                new RefColumn("MANUFACTURER_ID", ColumnProperty.Null, "PRODUCT_MNFCTRR", "CLIENT", "ID"),
                new RefColumn("PACKER_ID", ColumnProperty.Null, "PRODUCT_PACKER", "CLIENT", "ID"),
                new RefColumn("IMPORTER_ID", ColumnProperty.Null, "PRODUCT_IMPTR", "CLIENT", "ID"),
                new Column("OKP", DbType.String),
                new Column("TNVED", DbType.String),
                new Column("CARGO_TRAIN_CODE", DbType.String),
                new Column("BARCODE", DbType.String),
                new Column("GOST", DbType.String),
                new Column("GOSTR", DbType.String),
                new Column("OST", DbType.String),
                new Column("STP", DbType.String),
                new Column("TU", DbType.String),
                new Column("CONFORM_CERT_NUM", DbType.String),
                new RefColumn("CONFORM_CERT_FILE_ID", ColumnProperty.Null, "PROD_CNFRM_CERT", "ENTITY_FILE", "ID"),
                new RefColumn("CONFORM_CERT_FILE2_ID", ColumnProperty.Null, "PROD_CNFRM_CERT2", "ENTITY_FILE", "ID"),
                new Column("CONFORM_DECL_NUM", DbType.String),
                new RefColumn("CONFORM_DECL_FILE_ID", ColumnProperty.Null, "PROD_CNFRM_DECL", "ENTITY_FILE", "ID"),
                new Column("HEALTH_CERT_NUM", DbType.String),
                new RefColumn("HEALTH_CERT_FILE_ID", ColumnProperty.Null, "PROD_HLTH_CERT", "ENTITY_FILE", "ID"),
                new Column("FIRE_CERT_NUM", DbType.String),
                new RefColumn("FIRE_CERT_FILE_ID", ColumnProperty.Null, "PROD_FIRE_CERT", "ENTITY_FILE", "ID"),
                new Column("FIRE_DECL_NUM", DbType.String),
                new RefColumn("FIRE_DECL_FILE_ID", ColumnProperty.Null, "PROD_FIRE_DECL", "ENTITY_FILE", "ID"),
                new RefColumn("DNGR_CLASS_ID", ColumnProperty.Null, "PROD_DNGR_CLS", "DICT_DANGER_CLASS", "ID"),
                new RefColumn("DNGR_SUBCLASS_ID", ColumnProperty.Null, "PROD_DNGR_SUBCLS", "DICT_DANGER_SUB_CLASS", "ID"),
                new RefColumn("DNGR_UN_CODE_ID", ColumnProperty.Null, "PROD_UN_CODE", "DICT_DANGER_UN_CODE", "ID"),
                new Column("KIND", DbType.String),
                new RefColumn("TYPE_SINGLE_ID", ColumnProperty.Null, "PROD_PKG_TYPE_SNGL", "DICT_PACKING_TYPE", "ID"),
                new RefColumn("MATERIAL_SINGLE_ID", ColumnProperty.Null, "PROD_PKG_MTRL_SNGL", "DICT_PACKING_MATERIAL", "ID"),
                new Column("VOLUME_SINGLE", DbType.Decimal),
                new RefColumn("VOLUME_UNIT_SINGLE_ID", ColumnProperty.Null, "PROD_VOl_UNIT_SNGL", "DICT_MEASURE_UNIT", "ID"),
                new Column("WEIGHT_SINGLE", DbType.Decimal),
                new RefColumn("WEIGHT_UNIT_SINGLE_ID", ColumnProperty.Null, "PROD_WGHT_UNIT_SNGL", "DICT_MEASURE_UNIT", "ID"),
                new Column("LENGTH_SINGLE", DbType.Decimal),
                new Column("WIDTH_SINGLE", DbType.Decimal),
                new Column("HEIGHT_SINGLE", DbType.Decimal),
                new RefColumn("SIZE_UNIT_SINGLE_ID", ColumnProperty.Null, "PROD_SIZE_UNIT_SNGL", "DICT_MEASURE_UNIT", "ID"),
                new Column("UNIT_COUNT_GROUP", DbType.Int32),
                new RefColumn("TYPE_GROUP_ID", ColumnProperty.Null, "PROD_PKG_TYPE_GRP", "DICT_PACKING_TYPE", "ID"),
                new RefColumn("MATERIAL_GROUP_ID", ColumnProperty.Null, "PROD_PKG_MTRL_GRP", "DICT_PACKING_MATERIAL", "ID"),
                new Column("VOLUME_GROUP", DbType.Decimal),
                new RefColumn("VOLUME_UNIT_GROUP_ID", ColumnProperty.Null, "PROD_VOl_UNIT_GRP", "DICT_MEASURE_UNIT", "ID"),
                new Column("WEIGHT_GROUP", DbType.Decimal),
                new RefColumn("WEIGHT_UNIT_GROUP_ID", ColumnProperty.Null, "PROD_WGHT_UNIT_GRP", "DICT_MEASURE_UNIT", "ID"),
                new Column("LENGTH_GROUP", DbType.Decimal),
                new Column("WIDTH_GROUP", DbType.Decimal),
                new Column("HEIGHT_GROUP", DbType.Decimal),
                new RefColumn("SIZE_UNIT_GROUP_ID", ColumnProperty.Null, "PROD_SIZE_UNIT_GRP", "DICT_MEASURE_UNIT", "ID"),
                new Column("UNIT_COUNT_TRN", DbType.Int32),
                new RefColumn("TYPE_TRN_ID", ColumnProperty.Null, "PROD_PKG_TYPE_TRN", "DICT_PACKING_TYPE", "ID"),
                new RefColumn("MATERIAL_TRN_ID", ColumnProperty.Null, "PROD_PKG_MTRL_TRN", "DICT_PACKING_MATERIAL", "ID"),
                new Column("VOLUME_TRN", DbType.Decimal),
                new RefColumn("VOLUME_UNIT_TRN_ID", ColumnProperty.Null, "PROD_VOl_UNIT_TRN", "DICT_MEASURE_UNIT", "ID"),
                new Column("WEIGHT_TRN", DbType.Decimal),
                new RefColumn("WEIGHT_UNIT_TRN_ID", ColumnProperty.Null, "PROD_WGHT_UNIT_TRN", "DICT_MEASURE_UNIT", "ID"),
                new Column("LENGTH_TRN", DbType.Decimal),
                new Column("WIDTH_TRN", DbType.Decimal),
                new Column("HEIGHT_TRN", DbType.Decimal),
                new RefColumn("SIZE_UNIT_TRN_ID", ColumnProperty.Null, "PROD_SIZE_UNIT_TRN", "DICT_MEASURE_UNIT", "ID"),
                new Column("PKG_DISPLAY_STRING", DbType.String.WithSize(2000))
                );
        }

        public override void Revert()
        {
            Database.RemoveEntityTable("PRODUCT");
            Database.RemoveEntityTable("CLIENT");
            Database.RemoveEntityTable("DICT_MEASURE_UNIT");
        }
    }
}