﻿using System.Data;
using Cement.Core.Migrations.Extensions;
using ECM7.Migrator.Framework;

namespace Cement.Core.Migrations
{
    [Migration(2014111801)]
    public class Migration_2014111801 : Migration
    {
        public override void Apply()
        {
            Database.AddEntityTable("CONTRACT",
                new Column("NO", DbType.String),
                new Column("DATE_CONTRACT", DbType.DateTime),
                new Column("NAME", DbType.String.WithSize(2000)),
                new RefColumn("ORGANIZATION_ID", "CONTRACT_ORGANIZATION", "ORGANIZATION", "ID"),
                new RefColumn("CLIENT_ID", "CONTRACT_CLIENT", "CLIENT", "ID"));

            Database.AddEntityTable("CONTRACT_ANNEX",
                new Column("TYPE", DbType.Int16, ColumnProperty.NotNull),
                new Column("NO", DbType.String),
                new Column("DATE_DOCUMENT", DbType.DateTime),
                new Column("NAME", DbType.String.WithSize(2000)),
                new RefColumn("CONTRACT_ID", "CONTRACT_ANNEX_CONTRACT", "CONTRACT", "ID"));

            Database.AddSubclassEntityTable("CONTRACT_ADDITIONAL", "CONTRACT_ANNEX", "CONTRACT_ADD_ANNEX");
            Database.AddSubclassEntityTable("CONTRACT_APPLICATION", "CONTRACT_ANNEX", "CONTRACT_APP_ANNEX");
            Database.AddSubclassEntityTable("CONTRACT_SPECIFICATION", "CONTRACT_ANNEX", "CONTRACT_SPEC_ANNEX");
        }

        public override void Revert()
        {
            Database.RemoveEntityTable("CONTRACT_SPECIFICATION");
            Database.RemoveEntityTable("CONTRACT_APPLICATION");
            Database.RemoveEntityTable("CONTRACT_ADDITIONAL");
            Database.RemoveEntityTable("CONTRACT_ANNEX");
            Database.RemoveEntityTable("CONTRACT");
        }
    }
}