﻿using System.Data;
using Cement.Core.Migrations.Extensions;
using ECM7.Migrator.Framework;

namespace Cement.Core.Migrations
{
    [Migration(2014111101)]
    public class Migration_2014111101 : Migration
    {
        public override void Apply()
        {
            Database.AddColumn("DICT_DRIVER_CATEGORY", new Column("TYPE", DbType.Int16, ColumnProperty.Null, 10));

            Database.AddEntityTable("DICT_EXPERIENCE",
                new Column("NAME", DbType.String),
                new Column("TYPE", DbType.Int16));

            Database.AddEntityTable("EMPLOYEE",
                new Column("NAME", DbType.String),
                new Column("SURNAME", DbType.String),
                new Column("FIRSTNAME", DbType.String),
                new Column("PATRONYMIC", DbType.String),
                new RefColumn("POSITION_ID", "EMPLOYEE_POSITION", "DICT_POSITION", "ID"),
                new RefColumn("DEPARTMENT_ID", "EMPLOYEE_DEPARTMENT", "DICT_DEPARTMENT", "ID"),
                new Column("INN", DbType.String),

                new Column("PASSPORT_SERIE", DbType.String),
                new Column("PASSPORT_NO", DbType.String),
                new Column("PASSPORT_GIVEN_PLACE", DbType.String),
                new Column("PASSPORT_DATE", DbType.DateTime),
                new Column("BIRTH_DATE", DbType.DateTime),
                new Column("BIRTH_PLACE", DbType.String),

                new Column("DRIVELIC_SERIE", DbType.String),
                new Column("DRIVELIC_NO", DbType.String),
                new Column("DRIVELIC_GIVEN_PLACE", DbType.String),
                new Column("DRIVELIC_DATE", DbType.DateTime),
                new Column("DRIVELIC_VALID_DATE", DbType.DateTime),
                new RefColumn("DRIVE_EXPERIENCE_ID", "EMPLOYEE_EXPERIENCE", "DICT_EXPERIENCE", "ID"),

                new Column("TRACTDRIVELIC_SERIE", DbType.String),
                new Column("TRACTDRIVELIC_NO", DbType.String),
                new Column("TRACTDRIVELIC_CODE", DbType.String),
                new Column("TRACTDRIVELIC_DATE", DbType.DateTime),
                new Column("TRACTDRIVELIC_VALID_DATE", DbType.DateTime),

                new Column("POSTAL_CODE", DbType.String),
                new Column("COUNTRY", DbType.String),
                new Column("REGION", DbType.String),
                new Column("AREA", DbType.String),
                new Column("LOCALITY", DbType.String),
                new Column("STREET", DbType.String),
                new Column("HOUSE", DbType.String),
                new Column("APARTMENT", DbType.String),

                new Column("PHONE1", DbType.String),
                new Column("PHONE2", DbType.String),
                new Column("PHONE3", DbType.String),
                new Column("FAX", DbType.String),
                new Column("MOBILE_PHONE1", DbType.String),
                new Column("MOBILE_PHONE2", DbType.String),
                new Column("MOBILE_PHONE3", DbType.String),
                new Column("EMAIL1", DbType.String),
                new Column("EMAIL2", DbType.String),
                new Column("EMAIL3", DbType.String));

            Database.AddEntityTable("EMPLOYEE_DRIVE_CAT",
                new RefColumn("EMPLOYEE_ID", ColumnProperty.NotNull, "EMP_DRIVECAT_EMP", "EMPLOYEE", "ID"),
                new RefColumn("DRIVE_CATEGORY_ID", ColumnProperty.NotNull, "EMP_DRIVECAT_DRIVECAT", "DICT_DRIVER_CATEGORY", "ID"));

            Database.AddEntityTable("VEHICLE_FUEL",
                new Column("NAME", DbType.String),
                new Column("DATE", DbType.DateTime),
                new RefColumn("FUEL_STATION_ID", "VEH_FUEL_ST_CLIENT", "CLIENT", "ID"),
                new Column("AMOUNT", DbType.Decimal),
                new Column("COST", DbType.Decimal),
                new RefColumn("MEASURE_UNIT_ID", "VEH_FUEL_MEASUNIT", "DICT_MEASURE_UNIT", "ID"),
                new RefColumn("DRIVER_ID", "VEH_FUEL_EMPLOYEE", "EMPLOYEE", "ID"),
                new RefColumn("VEHICLE_ID", "VEH_FUEL_VEH", "VEHICLE", "ID"),
                new RefColumn("DOCUMENT_ID", "VEH_FUEL_DOC", "ENTITY_FILE", "ID"));
        }

        public override void Revert()
        {
            Database.RemoveEntityTable("VEHICLE_FUEL");
            Database.RemoveEntityTable("EMPLOYEE_DRIVE_CAT");
            Database.RemoveEntityTable("EMPLOYEE");
            Database.RemoveEntityTable("DICT_EXPERIENCE");
            Database.RemoveColumn("DICT_DRIVER_CATEGORY", "TYPE");
        }
    }
}