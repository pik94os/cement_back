﻿using System.Data;
using ECM7.Migrator.Framework;

namespace Cement.Core.Migrations
{
    [Migration(2014112100)]
    public class Migration_2014112100 : Migration
    {
        public override void Apply()
        {
            Database.RemoveColumn("VEHICLE", "RELEASE_YEAR");
            Database.AddColumn("VEHICLE", new Column("RELEASE_YEAR", DbType.Int32)); 
        }

        public override void Revert()
        {
            Database.RemoveColumn("VEHICLE", "RELEASE_YEAR");
            Database.AddColumn("VEHICLE", new Column("RELEASE_YEAR", DbType.DateTime)); 
        }
    }
}