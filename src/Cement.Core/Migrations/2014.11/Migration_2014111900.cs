﻿using System.Data;
using Cement.Core.Migrations.Extensions;
using ECM7.Migrator.Framework;

namespace Cement.Core.Migrations
{
    [Migration(2014111900)]
    public class Migration_2014111900 : Migration
    {
        public override void Apply()
        {
            Database.AddEntityTable("CONTRACT_ANNEX_PRICELIST",
                new RefColumn("CONTRACT_ID", "CNTRANX_PRICE_CNTR", "CONTRACT", "ID"),
                new RefColumn("CONTRACT_ANNEX_ID", "CNTRANX_PRICE_CNTRANX", "CONTRACT_ANNEX", "ID"),
                new RefColumn("PRICELIST_ID", "CNTRANX_PRICE_PRCLST", "PRICE_LIST", "ID"));

            Database.AddEntityTable("SIGNATURE",
                new RefColumn("ORGANIZATION_ID", "SIGNATURE_ORG", "ORGANIZATION", "ID"),
                new Column("DATE_START", DbType.DateTime),
                new Column("DATE_END", DbType.DateTime),
                new RefColumn("TRUSTER_ID", "SIGN_TRUST_EMPLOYEE", "EMPLOYEE", "ID"),
                new RefColumn("RECIPIENT_ID", "SIGN_RECIP_EMPLOYEE", "EMPLOYEE", "ID"));
        }

        public override void Revert()
        {
            Database.RemoveEntityTable("SIGNATURE");
            Database.RemoveEntityTable("CONTRACT_ANNEX_PRICELIST");
        }
    }
}