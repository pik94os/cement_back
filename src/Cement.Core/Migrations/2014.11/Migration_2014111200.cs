﻿using System.Data;
using Cement.Core.Migrations.Extensions;
using ECM7.Migrator.Framework;

namespace Cement.Core.Migrations
{
    [Migration(2014111200)]
    public class Migration_2014111200 : Migration
    {
        public override void Apply()
        {
            Database.AddColumn("PRODUCT", new Column("OKUN", DbType.String));
            Database.AddColumn("PRODUCT", new Column("ARTICLE", DbType.String));
            Database.AddRefColumn("PRODUCT", new RefColumn("MEASURE_UNIT_ID", ColumnProperty.Null, "PROD_SERVICE_UNIT", "DICT_MEASURE_UNIT", "ID"));
        }

        public override void Revert()
        {
            Database.RemoveColumn("PRODUCT", "OKUN");
            Database.RemoveColumn("PRODUCT", "ARTICLE");
            Database.RemoveColumn("PRODUCT", "MEASURE_UNIT_ID");
        }
    }
}