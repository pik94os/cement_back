﻿using System.Data;
using ECM7.Migrator.Framework;

namespace Cement.Core.Migrations
{
    [Migration(2014112400)]
    public class Migration_2014112400 : Migration
    {
        public override void Apply()
        {
            Database.AddColumn("CONTRACT_ANNEX", new Column("PAY_TERM_TEXT", DbType.String));
            Database.AddColumn("CONTRACT_ANNEX", new Column("PAY_TERM_KIND", DbType.Int16));
            Database.AddColumn("CONTRACT_ANNEX", new Column("PAY_TERM_DAYS_KIND", DbType.Int16));
            Database.AddColumn("CONTRACT_ANNEX", new Column("PAY_TERM_WORK_DAYS", DbType.Int16));
            Database.AddColumn("CONTRACT_ANNEX", new Column("PAY_TERM_BANK_DAYS", DbType.Int16));
            Database.AddColumn("CONTRACT_ANNEX", new Column("PAY_TERM_NEXTMNTHDAY", DbType.Int16));
            Database.AddColumn("CONTRACT_ANNEX", new Column("PAY_TERM_DAYNUMKIND", DbType.Int16));
            Database.AddColumn("CONTRACT_ANNEX", new Column("PAY_TERM_DAYNUMDAY", DbType.Int16));
            Database.AddColumn("CONTRACT_ANNEX", new Column("PAY_TERM_DAYNUMFROM", DbType.DateTime));
            Database.AddColumn("CONTRACT_ANNEX", new Column("PAY_TERM_DAYNUMTO", DbType.DateTime));
        }

        public override void Revert()
        {
            Database.RemoveColumn("CONTRACT_ANNEX", "PAY_TERM_TEXT");
            Database.RemoveColumn("CONTRACT_ANNEX", "PAY_TERM_KIND");
            Database.RemoveColumn("CONTRACT_ANNEX", "PAY_TERM_DAYS_KIND");
            Database.RemoveColumn("CONTRACT_ANNEX", "PAY_TERM_WORK_DAYS");
            Database.RemoveColumn("CONTRACT_ANNEX", "PAY_TERM_BANK_DAYS");
            Database.RemoveColumn("CONTRACT_ANNEX", "PAY_TERM_NEXTMNTHDAY");
            Database.RemoveColumn("CONTRACT_ANNEX", "PAY_TERM_DAYNUMKIND");
            Database.RemoveColumn("CONTRACT_ANNEX", "PAY_TERM_DAYNUMDAY");
            Database.RemoveColumn("CONTRACT_ANNEX", "PAY_TERM_DAYNUMFROM");
            Database.RemoveColumn("CONTRACT_ANNEX", "PAY_TERM_DAYNUMTO");
        }
    }
}