﻿using System.Data;
using Cement.Core.Migrations.Extensions;
using ECM7.Migrator.Framework;

namespace Cement.Core.Migrations
{
    [Migration(2014110400)]
    public class Migration_2014110400 : Migration
    {
        public override void Apply()
        {
            Database.AddEntityTable("ENTITY_FILE", 
                new Column("NAME", DbType.String),
                new Column("EXTENSION", DbType.String),
                new Column("CHECKSUM", DbType.String),
                new Column("FILE_SIZE", DbType.Int64));

            Database.AddEntityTable("VEHICLE",
                new Column("VIN", DbType.String),
                new Column("CHASSIS_SERIAL_NUM", DbType.String),
                new Column("ENGINE_SERIAL_NUM", DbType.String),
                new RefColumn("PHOTO_FILE_ID", "VEHICLE_PHOTO", "ENTITY_FILE", "ID"),
                new RefColumn("PTS_SCAN1_FILE_ID", "VEHICLE_PTS_SCAN1", "ENTITY_FILE", "ID"),
                new RefColumn("PTS_SCAN2_FILE_ID", "VEHICLE_PTS_SCAN2", "ENTITY_FILE", "ID"));
        }

        public override void Revert()
        {
            Database.RemoveEntityTable("VEHICLE");
            Database.RemoveEntityTable("ENTITY_FILE");
        }
    }
}