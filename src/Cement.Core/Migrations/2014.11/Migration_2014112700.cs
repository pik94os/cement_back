﻿using System.Data;
using Cement.Core.Enums;
using Cement.Core.Migrations.Extensions;
using ECM7.Migrator.Framework;

namespace Cement.Core.Migrations
{
    [Migration(2014112700)]
    public class Migration_2014112700 : Migration
    {
        public override void Apply()
        {
            Database.AddRefColumn("USERS", new RefColumn("EMPLOYEE_ID", "USER_EMPLOYEE", "EMPLOYEE", "ID"));

            Database.AddRefColumn("INTERNAL_DOCUMENT", new RefColumn("CREATOR_ID", "INTDOC_CREATOR_EMPLOYEE", "EMPLOYEE", "ID"));
            Database.AddColumn("INTERNAL_DOCUMENT", new Column("STATE", DbType.Int16, ColumnProperty.NotNull, 10));
            
            Database.AddEntityTable("INTERNAL_DOC_MOVEMENT",
                new RefColumn("DOCUMENT_ID", ColumnProperty.NotNull, "INTDOCMVT_INTDOC", "INTERNAL_DOCUMENT", "ID"),
                new RefColumn("EMPLOYEE_ID", ColumnProperty.NotNull, "INTDOCMVT_EMPLOYEE", "EMPLOYEE", "ID"),
                new Column("TYPE", DbType.Int16, ColumnProperty.NotNull),
                new Column("PROPERTIES", DbType.Int16, ColumnProperty.NotNull),
                new Column("IS_ARCHIVED", DbType.Boolean, ColumnProperty.NotNull));

            Database.AddEntityTable("INTERNAL_DOC_ROUTE",
                new RefColumn("DOCUMENT_ID", ColumnProperty.NotNull, "INTDOCROUTE_INTDOC", "INTERNAL_DOCUMENT", "ID"),
                new RefColumn("EMPLOYEE_ID", ColumnProperty.NotNull, "INTDOCROUTE_EMPLOYEE", "EMPLOYEE", "ID"),
                new Column("DATE_IN", DbType.DateTime),
                new Column("DATE_OUT", DbType.DateTime));

        }

        public override void Revert()
        {
            Database.RemoveEntityTable("INTERNAL_DOC_ROUTE");

            Database.RemoveEntityTable("INTERNAL_DOC_MOVEMENT");
            
            Database.RemoveColumn("INTERNAL_DOCUMENT", "STATE");
            Database.RemoveRefColumn("INTERNAL_DOCUMENT", "CREATOR_ID", "INTDOC_CREATOR_EMPLOYEE");
            
            Database.RemoveRefColumn("USERS", "EMPLOYEE_ID", "USER_EMPLOYEE");
        }
    }
}