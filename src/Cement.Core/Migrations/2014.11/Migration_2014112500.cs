﻿using System.Data;
using Cement.Core.Migrations.Extensions;
using ECM7.Migrator.Framework;

namespace Cement.Core.Migrations
{
    [Migration(2014112500)]
    public class Migration_2014112500 : Migration
    {
        public override void Apply()
        {
            Database.AddEntityTable("INTERNAL_DOCUMENT",
                new RefColumn("ORGANIZATION_ID", "INTERNALDOC_ORG", "ORGANIZATION", "ID"),
                new Column("NAME", DbType.String),
                new Column("NO", DbType.String),
                new Column("DOCUMENT_DATE", DbType.DateTime),
                new Column("TYPE", DbType.Int16),
                new RefColumn("NOMENCLATURE_ID", "INTERNALDOC_NOMENCLATURE", "NOMENCLATURE", "ID"),
                new Column("SUBJECT", DbType.String),
                new Column("CONTENT", DbType.Binary),
                new RefColumn("AUTHOR_ID", "INTDOC_AUTHOR_EMPLOYEE", "EMPLOYEE", "ID"),
                new RefColumn("SIGNER_ID", "INTDOC_SIGNER_EMPLOYEE", "EMPLOYEE", "ID"),
                new Column("REVIEW_BEFORE", DbType.DateTime),
                new Column("IMPORTANCE", DbType.Int16),
                new Column("SECRECY", DbType.Int16),
                new RefColumn("FILE_ID", "INTDOC_ENTFILE", "ENTITY_FILE", "ID"));

            //Database.AddEntityTable("DOCUMENT_RELATION");
        }

        public override void Revert()
        {
            //Database.RemoveEntityTable("DOCUMENT_RELATION");
            Database.RemoveEntityTable("INTERNAL_DOCUMENT");
        }
    }
}