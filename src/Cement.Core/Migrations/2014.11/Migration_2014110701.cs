﻿using System.Data;
using Cement.Core.Migrations.Extensions;
using ECM7.Migrator.Framework;

namespace Cement.Core.Migrations
{
    [Migration(2014110701)]
    public class Migration_2014110701 : Migration
    {
        public override void Apply()
        {
            Database.AddEntityTable("DICT_VEHICLE_WHEELBASE", new Column("NAME", DbType.String));
            Database.AddEntityTable("DICT_VEHICLE_COLUMN", new Column("NAME", DbType.String));
            Database.AddEntityTable("DICT_VEHICLE_DEPARTMENT", new Column("NAME", DbType.String));

            Database.AddRefColumn("VEHICLE", new RefColumn("WHEEL_BASE_ID", "VEH_VEH_WHEELBASE", "DICT_VEHICLE_WHEELBASE", "ID"));
            Database.AddRefColumn("VEHICLE", new RefColumn("COLUMN_ID", "VEH_VEH_COLUMN", "DICT_VEHICLE_COLUMN", "ID"));
            Database.AddRefColumn("VEHICLE", new RefColumn("DIVISION_ID", "VEH_VEH_DEPARTMENT", "DICT_VEHICLE_DEPARTMENT", "ID"));
            Database.AddRefColumn("VEHICLE", new RefColumn("MODEL_ID", "VEH_VEH_MODEL", "DICT_VEHICLE_MODEL", "ID"));
            Database.AddRefColumn("VEHICLE", new RefColumn("MODIFICATION_ID", "VEH_VEH_MODIFICATION", "DICT_VEHICLE_MODIFICATION", "ID"));

            Database.AddRefColumn("DICT_VEHICLE_MODEL", new RefColumn("CATEGORY_ID", "VEHMODEL_CATEGORY", "DICT_VEHICLE_CATEGORY", "ID"));
            Database.AddRefColumn("DICT_VEHICLE_MODEL", new RefColumn("SUBCATEGORY_ID", "VEHMODEL_SUBCATEGORY", "DICT_VEHICLE_SUBCATEGORY", "ID"));
        }

        public override void Revert()
        {
            Database.RemoveRefColumn("VEHICLE", "WHEEL_BASE_ID", "VEH_VEH_WHEELBASE");
            Database.RemoveRefColumn("VEHICLE", "COLUMN_ID", "VEH_VEH_COLUMN");
            Database.RemoveRefColumn("VEHICLE", "DIVISION_ID", "VEH_VEH_DEPARTMENT");
            Database.RemoveRefColumn("VEHICLE", "MODEL_ID", "VEH_VEH_MODEL");
            Database.RemoveRefColumn("VEHICLE", "MODIFICATION_ID", "VEH_VEH_MODIFICATION");

            Database.RemoveRefColumn("DICT_VEHICLE_MODEL", "CATEGORY_ID", "VEHMODEL_CATEGORY");
            Database.RemoveRefColumn("DICT_VEHICLE_MODEL", "SUBCATEGORY_ID", "VEHMODEL_SUBCATEGORY");

            Database.RemoveEntityTable("DICT_VEHICLE_WHEELBASE");
            Database.RemoveEntityTable("DICT_VEHICLE_COLUMN");
            Database.RemoveEntityTable("DICT_VEHICLE_DEPARTMENT");
        }
    }
}