﻿using System.Data;
using Cement.Core.Migrations.Extensions;
using ECM7.Migrator.Framework;

namespace Cement.Core.Migrations
{
    [Migration(2014111301)]
    public class Migration_2014111301 : Migration
    {
        public override void Apply()
        {
            Database.AddRefColumn("PRODUCT", new RefColumn("ORGANIZATION_ID", ColumnProperty.Null, "PRODUCT_ORGANIZATION", "ORGANIZATION", "ID"));
            Database.AddRefColumn("PRICE_LIST", new RefColumn("ORGANIZATION_ID", ColumnProperty.Null, "PRICELIST_ORGANIZATION", "ORGANIZATION", "ID"));

            Database.AddEntityTable("DICT_RAILWAY_STATION", 
                new Column("CODE", DbType.String),
                new Column("NAME", DbType.String),
                new Column("SHORT_NAME", DbType.String),
                new Column("REGION_NAME", DbType.String),
                new Column("RAILWAY_NAME", DbType.String),
                new Column("RAILWAY_SHORT_NAME", DbType.String));
        }

        public override void Revert()
        {
            Database.RemoveRefColumn("PRODUCT", "ORGANIZATION_ID", "PRODUCT_ORGANIZATION");
            Database.RemoveRefColumn("PRICE_LIST", "ORGANIZATION_ID", "PRICELIST_ORGANIZATION");

            Database.RemoveEntityTable("DICT_RAILWAY_STATION");
        }
    }
}