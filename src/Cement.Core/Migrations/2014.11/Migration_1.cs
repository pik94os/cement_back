﻿using System.Data;
using Cement.Core.Migrations.Extensions;
using ECM7.Migrator.Framework;

namespace Cement.Core.Migrations
{
    [Migration(1)]
    public class Migration_1 : Migration
    {
        public override void Apply()
        {
            Database.AddTable("HIBERNATE_UNIQUE_KEY", new Column("NEXT_HI", DbType.Int64));
            Database.ExecuteNonQuery(@"INSERT INTO HIBERNATE_UNIQUE_KEY(NEXT_HI) VALUES (40000)");

            Database.ExecuteNonQuery(@"create sequence hibernate_sequence minvalue 1 start with 1 increment by 1");

            Database.AddEntityTable("PERSON", new Column("NAME", DbType.String));
        }

        public override void Revert()
        {
            Database.RemoveTable("PERSON");

            Database.RemoveTable("HIBERNATE_UNIQUE_KEY");

            Database.ExecuteNonQuery("drop sequence hibernate_sequence");
        }
    }
}