﻿using System.Data;
using Cement.Core.Migrations.Extensions;
using ECM7.Migrator.Framework;

namespace Cement.Core.Migrations
{
    [Migration(2014110500)]
    public class Migration_2014110500 : Migration
    {
        public override void Apply()
        {
            Database.AddEntityTable("DICT_PACKING_MATERIAL", 
                new Column("NAME", DbType.String));

            Database.AddEntityTable("DICT_PACKING_TYPE",
                new Column("NAME", DbType.String),
                new Column("DESCRIPTION", DbType.String));
        }

        public override void Revert()
        {
            Database.RemoveEntityTable("DICT_PACKING_MATERIAL");
            Database.RemoveEntityTable("DICT_PACKING_TYPE");
        }
    }
}