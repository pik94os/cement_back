﻿using System.Data;
using Cement.Core.Migrations.Extensions;
using ECM7.Migrator.Framework;

namespace Cement.Core.Migrations
{
    [Migration(2014110501)]
    public class Migration_2014110501 : Migration
    {
        public override void Apply()
        {
            Database.AddEntityTable("DICT_VEHICLE_CATEGORY", 
                new Column("NAME", DbType.String),
                new Column("HAS_CHARACTERISTICS", DbType.Boolean, ColumnProperty.NotNull, false));

            Database.AddEntityTable("DICT_VEHICLE_SUBCATEGORY",
                new Column("NAME", DbType.String),
                new RefColumn("CATEGORY_ID", "VEHICLE_SUBCAT_CAT", "DICT_VEHICLE_CATEGORY", "ID"));

            if (Database.TableExists("VEHICLE"))
            {
                Database.RemoveEntityTable("VEHICLE");
            }
            
            Database.AddEntityTable("VEHICLE",
                new RefColumn("CATEGORY_ID", "VEHICLE_CATEGORY", "DICT_VEHICLE_CATEGORY", "ID"),
                new RefColumn("SUBCATEGORY_ID", "VEHICLE_SUBCATEGORY", "DICT_VEHICLE_SUBCATEGORY", "ID"),
                new Column("RELEASE_YEAR", DbType.DateTime),
                new Column("NUM", DbType.String),
                new RefColumn("NUM_COUNTRY_CODE_ID", "VEH_NUM_CNTR_CODE", "DICT_COUNTRY_CODE", "ID"),
                new RefColumn("NUM_REGION_CODE_ID", "VEH_NUM_REG_CODE", "DICT_REGION_CODE", "ID"),
                new RefColumn("PHOTO_FILE_ID", "VEHICLE_PHOTO", "ENTITY_FILE", "ID"),

                new Column("VIN", DbType.String),
                new Column("CHASSIS_SERIAL_NUM", DbType.String),
                new Column("ENGINE_SERIAL_NUM", DbType.String),
                new Column("BODY", DbType.String),
                new Column("WHEEL_BASE", DbType.String),
                new RefColumn("DRIVER_CAT_ID", "VEH_DRIVER_CAT", "DICT_DRIVER_CATEGORY", "ID"),
                new Column("ECO_CLASS", DbType.Decimal),
                new Column("COLOR", DbType.String),
                new Column("POWER", DbType.Decimal),
                new Column("ENGINE_VOLUME", DbType.Decimal),
                new Column("MAX_MASS", DbType.Decimal),
                new Column("VEHICLE_MASS", DbType.Decimal),

                new Column("PTS_SERIE", DbType.String),
                new Column("PTS_NUMBER", DbType.String),
                new Column("PTS_MANUFACTURER", DbType.String),
                new Column("PTS_DATE", DbType.DateTime),
                new RefColumn("PTS_SCAN1_FILE_ID", "VEHICLE_PTS_SCAN1", "ENTITY_FILE", "ID"),
                new RefColumn("PTS_SCAN2_FILE_ID", "VEHICLE_PTS_SCAN2", "ENTITY_FILE", "ID"),

                new Column("DOC_SERIE", DbType.String),
                new Column("DOC_NUMBER", DbType.String),
                new Column("DOC_OWNER", DbType.String),
                new Column("DOC_DATE", DbType.DateTime),
                new RefColumn("DOC_SCAN1_FILE_ID", "VEHICLE_DOC_SCAN1", "ENTITY_FILE", "ID"),
                new RefColumn("DOC_SCAN2_FILE_ID", "VEHICLE_DOC_SCAN2", "ENTITY_FILE", "ID"),

                new RefColumn("CLASS_ID", "VEH_VEH_CLASS", "DICT_VEHICLE_CLASS", "ID"),
                new Column("TYPE", DbType.Int16),
                new RefColumn("BODYTYPE_ID", "VEH_VEH_BODYTYPE", "DICT_VEHICLE_BODY_TYPE", "ID"),
                new Column("LENGTH", DbType.Decimal),
                new Column("WIDTH", DbType.Decimal),
                new Column("HEIGHT", DbType.Decimal),
                new Column("IS_REAR_LOADING", DbType.Boolean, ColumnProperty.NotNull, false),
                new Column("IS_SIDE_LOADING", DbType.Boolean, ColumnProperty.NotNull, false),
                new Column("IS_OVER_LOADING", DbType.Boolean, ColumnProperty.NotNull, false),
                new Column("CAPACITY", DbType.Decimal),
                new Column("VOLUME", DbType.Decimal),
                new RefColumn("OPTION_ID", "VEH_VEH_OPTION", "DICT_VEHICLE_ADD_OPTIONS", "ID"),
                new Column("EXTRA_CHAR", DbType.String),

                new Column("DIVISION", DbType.String),
                new Column("COLONNA", DbType.String),
                new Column("GARAGE_NUMBER", DbType.String)
            );
        }

        public override void Revert()
        {
            Database.RemoveEntityTable("VEHICLE");
            Database.AddEntityTable("VEHICLE",
                new Column("VIN", DbType.String),
                new Column("CHASSIS_SERIAL_NUM", DbType.String),
                new Column("ENGINE_SERIAL_NUM", DbType.String),
                new RefColumn("PHOTO_FILE_ID", "VEHICLE_PHOTO", "ENTITY_FILE", "ID"),
                new RefColumn("PTS_SCAN1_FILE_ID", "VEHICLE_PTS_SCAN1", "ENTITY_FILE", "ID"),
                new RefColumn("PTS_SCAN2_FILE_ID", "VEHICLE_PTS_SCAN2", "ENTITY_FILE", "ID"));

            Database.RemoveEntityTable("DICT_VEHICLE_SUBCATEGORY");
            Database.RemoveEntityTable("DICT_VEHICLE_CATEGORY");
        }
    }
}