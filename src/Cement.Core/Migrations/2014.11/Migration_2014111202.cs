﻿using System.Data;
using Cement.Core.Migrations.Extensions;
using ECM7.Migrator.Framework;

namespace Cement.Core.Migrations
{
    [Migration(2014111202)]
    public class Migration_2014111202 : Migration
    {
        public override void Apply()
        {
            Database.AddEntityTable("DICT_TAX", new Column("RATE", DbType.Decimal));

            Database.AddEntityTable("PRICE_LIST",
                new Column("NAME", DbType.String),
                new Column("DATE_START", DbType.DateTime),
                new Column("DATE_END", DbType.DateTime),
                new Column("DESCRIPTION", DbType.Binary),
                new RefColumn("RPL_PRICELIST_ID", ColumnProperty.Null, "RPL_PRICELIST", "PRICE_LIST", "ID"));

            Database.AddEntityTable("PRICE_LIST_PRODUCT",
                new Column("PRICE", DbType.Decimal),
                new RefColumn("PRODUCT_ID", ColumnProperty.Null, "PRCLST_PROD_PROD", "PRODUCT", "ID"),
                new RefColumn("PRICELIST_ID", ColumnProperty.Null, "PRCLST_PROD_PRCLST", "PRICE_LIST", "ID"),
                new RefColumn("TAX_ID", ColumnProperty.Null, "PRCLST_PROD_TAX", "DICT_TAX", "ID"),
                new RefColumn("MEASURE_UNIT_ID", ColumnProperty.Null, "PRCLST_PROD_UNIT", "DICT_MEASURE_UNIT", "ID"));

            Database.AddEntityTable("PRODUCT_COMPLECT",
                new RefColumn("PRODUCT_ID", ColumnProperty.Null, "PROD_COMPLECT_PROD", "PRODUCT", "ID"),
                new RefColumn("CMPL_PRODUCT_ID", ColumnProperty.Null, "PROD_COMPLECT_CMPLPROD", "PRODUCT", "ID"));

            Database.AddColumn("PRODUCT", new Column("COMPLECT_STRING", DbType.String));
        }

        public override void Revert()
        {
            Database.RemoveTable("PRICE_LIST_PRODUCT");
            Database.RemoveTable("PRICE_LIST");
            Database.RemoveEntityTable("DICT_TAX");
            Database.RemoveTable("PRODUCT_COMPLECT");
            Database.RemoveColumn("PRODUCT", "COMPLECT_STRING");
        }
    }
}