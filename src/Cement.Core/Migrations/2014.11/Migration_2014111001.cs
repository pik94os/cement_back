﻿using System.Data;
using Cement.Core.Migrations.Extensions;
using ECM7.Migrator.Framework;

namespace Cement.Core.Migrations
{
    [Migration(2014111001)]
    public class Migration_2014111001 : Migration
    {
        public override void Apply()
        {
            Database.AddColumn("VEHICLE", new Column("NAME", DbType.String));

            Database.AddEntityTable("VEHICLE_MILEAGE",
                new Column("FIXATION_DATE", DbType.DateTime),
                new Column("MILEAGE", DbType.Decimal),
                new RefColumn("VEHICLE_ID", "VEH_MILEAGE_VEH", "VEHICLE", "ID"));

            Database.AddEntityTable("VEHICLE_INSURANCE",
                new Column("NAME", DbType.String),
                new Column("NO", DbType.String),
                new RefColumn("INSURANCE_COMPANY_ID", "VEH_INSURANCE_CLIENT", "CLIENT", "ID"),
                new Column("CONTACT_PERSON", DbType.String),
                new Column("PHONE", DbType.String),
                new Column("DATE_START", DbType.DateTime),
                new Column("DATE_END", DbType.DateTime),
                new Column("INSURANCE_SUM", DbType.Decimal),
                new RefColumn("VEHICLE_ID", "VEH_INSURANCE_VEH", "VEHICLE", "ID"),
                new RefColumn("DOCUMENT_ID", "VEH_INSURANCE_DOC", "ENTITY_FILE", "ID"));

            //Database.AddEntityTable("VEHICLE_FUEL",
            //    new Column("NAME", DbType.String),
            //    new Column("DATE", DbType.DateTime),
            //    new RefColumn("FUEL_STATION_ID", "VEH_FUEL_ST_CLIENT", "CLIENT", "ID"),
            //    new Column("AMOUNT", DbType.Decimal),
            //    new Column("COST", DbType.Decimal),
            //    new RefColumn("MEASURE_UNIT_ID", "VEH_FUEL_MEASUNIT", "DICT_MEASURE_UNIT", "ID"),
            //    new RefColumn("DRIVER_ID", "VEH_FUEL_STAFF", "STAFF", "ID"),
            //    new RefColumn("VEHICLE_ID", "VEH_FUEL_VEH", "VEHICLE", "ID"),
            //    new RefColumn("DOCUMENT_ID", "VEH_FUEL_DOC", "ENTITY_FILE", "ID"));
        }

        public override void Revert()
        {
            Database.RemoveColumn("VEHICLE", "NAME");
            Database.RemoveEntityTable("VEHICLE_MILEAGE");
            Database.RemoveEntityTable("VEHICLE_INSURANCE");
            //Database.RemoveEntityTable("VEHICLE_FUEL");
        }
    }
}