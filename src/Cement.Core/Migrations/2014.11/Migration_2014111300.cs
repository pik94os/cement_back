﻿using Cement.Core.Migrations.Extensions;
using ECM7.Migrator.Framework;

namespace Cement.Core.Migrations
{
    [Migration(2014111300)]
    public class Migration_2014111300 : Migration
    {
        public override void Apply()
        {
            Database.AddRefColumn("EMPLOYEE", new RefColumn("ORGANIZATION_ID", "EMPLOYEE_ORGANIZATION", "ORGANIZATION", "ID"));
            Database.AddRefColumn("VEHICLE", new RefColumn("ORGANIZATION_ID", "VEHICLE_ORGANIZATION", "ORGANIZATION", "ID"));
            Database.AddRefColumn("DICT_DEPARTMENT", new RefColumn("ORGANIZATION_ID", "DEPARTMENT_ORGANIZATION", "ORGANIZATION", "ID"));
            Database.AddRefColumn("DICT_VEHICLE_COLUMN", new RefColumn("ORGANIZATION_ID", "VEHCOLUMN_ORGANIZATION", "ORGANIZATION", "ID"));
            Database.AddRefColumn("DICT_VEHICLE_DEPARTMENT", new RefColumn("ORGANIZATION_ID", "VEHDEPT_ORGANIZATION", "ORGANIZATION", "ID"));
        }

        public override void Revert()
        {
            Database.RemoveRefColumn("EMPLOYEE", "ORGANIZATION_ID", "EMPLOYEE_ORGANIZATION");
            Database.RemoveRefColumn("VEHICLE", "ORGANIZATION_ID", "VEHICLE_ORGANIZATION");
            Database.RemoveRefColumn("DICT_DEPARTMENT", "ORGANIZATION_ID", "DEPARTMENT_ORGANIZATION");
            Database.RemoveRefColumn("DICT_VEHICLE_COLUMN", "ORGANIZATION_ID", "VEHCOLUMN_ORGANIZATION");
            Database.RemoveRefColumn("DICT_VEHICLE_DEPARTMENT", "ORGANIZATION_ID", "VEHDEPT_ORGANIZATION");
        }
    }
}