﻿using System.Data;
using Cement.Core.Migrations.Extensions;
using ECM7.Migrator.Framework;

namespace Cement.Core.Migrations
{
    [Migration(2014103100)]
    public class Migration_2014103100 : Migration
    {
        public override void Apply()
        {
            Database.AddEntityTable("DICT_COUNTRY", new Column("NAME", DbType.String));
            Database.AddEntityTable("DICT_MANUFACTURER", new Column("NAME", DbType.String));
            Database.AddEntityTable("DICT_PACKER", new Column("NAME", DbType.String));
            Database.AddEntityTable("DICT_IMPORTER", new Column("NAME", DbType.String));
            Database.AddEntityTable("DICT_DANGER_CLASS", new Column("NAME", DbType.String));
            Database.AddEntityTable("DICT_DANGER_SUB_CLASS", new Column("NAME", DbType.String));
            Database.AddEntityTable("DICT_DANGER_UN_CODE", new Column("NAME", DbType.String));
            Database.AddEntityTable("DICT_COUNTRY_CODE", new Column("NAME", DbType.String));
            Database.AddEntityTable("DICT_DRIVER_CATEGORY", new Column("NAME", DbType.String));
            Database.AddEntityTable("DICT_REGION_CODE", new Column("NAME", DbType.String));
            Database.AddEntityTable("DICT_SERVICE_SUPPLIER", new Column("NAME", DbType.String));
            Database.AddEntityTable("DICT_VEHICLE_ADD_OPTIONS", new Column("NAME", DbType.String));
            Database.AddEntityTable("DICT_VEHICLE_BODY_TYPE", new Column("NAME", DbType.String));
            Database.AddEntityTable("DICT_VEHICLE_CLASS", new Column("NAME", DbType.String));
        }

        public override void Revert()
        {
            Database.RemoveTable("DICT_COUNTRY");
            Database.RemoveTable("DICT_MANUFACTURER");
            Database.RemoveTable("DICT_PACKER");
            Database.RemoveTable("DICT_IMPORTER");
            Database.RemoveTable("DICT_DANGER_CLASS");
            Database.RemoveTable("DICT_DANGER_SUB_CLASS");
            Database.RemoveTable("DICT_DANGER_UN_CODE");
            Database.RemoveTable("DICT_COUNTRY_CODE");
            Database.RemoveTable("DICT_DRIVER_CATEGORY");
            Database.RemoveTable("DICT_REGION_CODE");
            Database.RemoveTable("DICT_SERVICE_SUPPLIER");
            Database.RemoveTable("DICT_VEHICLE_ADD_OPTIONS");
            Database.RemoveTable("DICT_VEHICLE_BODY_TYPE");
            Database.RemoveTable("DICT_VEHICLE_CLASS");
        }
    }
}