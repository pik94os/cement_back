﻿using System.Data;
using Cement.Core.Migrations.Extensions;
using ECM7.Migrator.Framework;

namespace Cement.Core.Migrations
{
    [Migration(2014111302)]
    public class Migration_2014111302 : Migration
    {
        public override void Apply()
        {
            Database.AddRefColumn("ORGANIZATION", new RefColumn("CLIENT_ID", "ORGANIZATION_CLIENT", "CLIENT", "ID"));

            Database.AddEntityTable("VEHICLE_AREND",
                new RefColumn("VEHICLE_ID", "VEH_AREND_VEH", "VEHICLE", "ID"),
                new RefColumn("CLIENT_ID", "VEH_AREND_CLIENT", "CLIENT", "ID"),
                new Column("DATE_START", DbType.DateTime),
                new Column("DATE_END", DbType.DateTime),
                new Column("IS_ACTIVE", DbType.Boolean));
        }

        public override void Revert()
        {
            Database.RemoveRefColumn("ORGANIZATION", "CLIENT_ID", "ORGANIZATION_CLIENT");

            Database.RemoveEntityTable("VEHICLE_AREND");
        }
    }
}