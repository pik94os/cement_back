﻿using System.Data;
using Cement.Core.Migrations.Extensions;
using ECM7.Migrator.Framework;

namespace Cement.Core.Migrations
{
    [Migration(2014111700)]
    public class Migration_2014111700 : Migration
    {
        public override void Apply()
        {
            Database.AddEntityTable("TRANSPORT_UNIT",
                new Column("NAME", DbType.String),
                new Column("DATE_START", DbType.DateTime),
                new RefColumn("ORGANIZATION_ID", "TPUNIT_ORGANIZATION", "ORGANIZATION", "ID"),
                new RefColumn("VEHICLE_ID", "TPUNIT_VEH_VEH", "VEHICLE", "ID"),
                new RefColumn("TRAILER_ID", "TPUNIT_TRAIL_VEH", "VEHICLE", "ID"),
                new RefColumn("DRIVER_ID", "TPUNIT_DRV_EMPL", "EMPLOYEE", "ID"));

            Database.AddEntityTable("TRANSPORT_UNIT_AREND",
                new RefColumn("TRANSPORT_UNIT_ID", "TPUNIT_AREND_TPUNIT", "TRANSPORT_UNIT", "ID"),
                new RefColumn("CLIENT_ID", "TPUNIT_AREND_CLIENT", "CLIENT", "ID"),
                new Column("DATE_START", DbType.DateTime),
                new Column("DATE_END", DbType.DateTime),
                new Column("IS_ACTIVE", DbType.Boolean));

            Database.AddColumn("DICT_VEHICLE_CATEGORY", new Column("TYPE", DbType.Int16, ColumnProperty.Null, 100));

            Database.AddColumn("CLIENT", new Column("ADDRESS", DbType.String.WithSize(2000)));

            Database.AddColumn("VEHICLE", new Column("NUMBER_TEXT", DbType.String));
            Database.AddColumn("VEHICLE", new Column("SIZE_TEXT", DbType.String));
            Database.AddColumn("VEHICLE", new Column("GO_TEXT", DbType.String));
            Database.AddColumn("VEHICLE", new Column("MODEL_TEXT", DbType.String));
        }

        public override void Revert()
        {
            Database.RemoveEntityTable("TRANSPORT_UNIT_AREND");
            Database.RemoveEntityTable("TRANSPORT_UNIT");

            Database.RemoveColumn("DICT_VEHICLE_CATEGORY", "TYPE");
            Database.RemoveColumn("CLIENT", "ADDRESS");

            Database.RemoveColumn("VEHICLE", "NUMBER_TEXT");
            Database.RemoveColumn("VEHICLE", "SIZE_TEXT");
            Database.RemoveColumn("VEHICLE", "GO_TEXT");
            Database.RemoveColumn("VEHICLE", "MODEL_TEXT");

        }
    }
}