﻿using System.Data;
using Cement.Core.Migrations.Extensions;
using ECM7.Migrator.Framework;

namespace Cement.Core.Migrations
{
    [Migration(2014111400)]
    public class Migration_2014111400 : Migration
    {
        public override void Apply()
        {
            Database.AddEntityTable("DICT_BOGIE",
                new Column("NAME", DbType.String),
                new Column("MODEL", DbType.String));

            Database.AddEntityTable("DICT_FABRICATOR",
                new Column("NAME", DbType.String));

            Database.AddEntityTable("DICT_FEATURE_MODEL",
                new Column("NAME", DbType.String));

            Database.AddEntityTable("DICT_SIZE",
                new Column("NAME", DbType.String));

            Database.AddEntityTable("DICT_RAILWAY_TRANSPORT",
                new Column("MODEL", DbType.String),
                new Column("KIND", DbType.String),
                new Column("RELEASE_YEAR", DbType.Int32),
                new Column("END_EDITION_YEAR", DbType.Int32),
                new Column("CAPACITY", DbType.Decimal),
                new Column("TARE_WEIGHT_MIN", DbType.Decimal),
                new Column("TARE_WEIGHT_MAX", DbType.Decimal),
                new Column("AXES_LENGTH", DbType.Decimal),
                new Column("AXIAL_LOAD", DbType.Decimal),
                new Column("AXES_COUNT", DbType.Int32),
                new Column("HAS_TRSN_PLATFORM", DbType.Boolean),
                new Column("BODY_VOLUME", DbType.Decimal),
                new Column("BOILER_CALIBRATION", DbType.String),
                new RefColumn("FABRICATOR_ID", "RW_TRN_FABRICATOR", "DICT_FABRICATOR", "ID"),
                new RefColumn("FEATURE_MODEL_ID", "RW_TRN_FEATUREMODEL", "DICT_FEATURE_MODEL", "ID"),
                new RefColumn("BOGIE_ID", "RW_TRN_BOGIE", "DICT_BOGIE", "ID"),
                new RefColumn("SIZE_ID", "RW_TRN_SIZE", "DICT_SIZE", "ID"));
        }

        public override void Revert()
        {
            Database.RemoveEntityTable("DICT_RAILWAY_TRANSPORT");
            Database.RemoveEntityTable("DICT_SIZE");
            Database.RemoveEntityTable("DICT_FEATURE_MODEL");
            Database.RemoveEntityTable("DICT_FABRICATOR");
            Database.RemoveEntityTable("DICT_BOGIE");
        }
    }
}