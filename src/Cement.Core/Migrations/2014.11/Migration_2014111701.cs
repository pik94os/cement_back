﻿using System.Data;
using Cement.Core.Migrations.Extensions;
using ECM7.Migrator.Framework;

namespace Cement.Core.Migrations
{
    [Migration(2014111701)]
    public class Migration_2014111701 : Migration
    {
        public override void Apply()
        {
            Database.AddEntityTable("DICT_BANK",
                new Column("NAME", DbType.String),
                new Column("FULL_NAME", DbType.String),
                new Column("BIK", DbType.String),
                new Column("KS", DbType.String));

            Database.AddEntityTable("PAYMENT_ACCOUNT",
                new RefColumn("ORGANIZATION_ID", "PAYMENT_ACC_ORGANIZATION", "ORGANIZATION", "ID"),
                new Column("ACCOUNT_NUMBER", DbType.String),
                new RefColumn("BANK_ID", ColumnProperty.Null, "PAYMENT_ACC_BANK", "DICT_BANK", "ID"));
        }

        public override void Revert()
        {
            Database.RemoveEntityTable("PAYMENT_ACCOUNT");
            Database.RemoveEntityTable("DICT_BANK");
        }
    }
}