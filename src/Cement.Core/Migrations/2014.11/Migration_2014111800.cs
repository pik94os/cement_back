﻿using System.Data;
using Cement.Core.Migrations.Extensions;
using ECM7.Migrator.Framework;

namespace Cement.Core.Migrations
{
    [Migration(2014111800)]
    public class Migration_2014111800 : Migration
    {
        public override void Apply()
        {
            Database.AddColumn("CLIENT", new Column("INN", DbType.String));
            Database.AddColumn("CLIENT", new Column("KPP", DbType.String));
            Database.AddColumn("CLIENT", new Column("OKPO", DbType.String));
            
            Database.AddEntityTable("WAREHOUSE",
                new RefColumn("ORGANIZATION_ID", "WHOUSE_ORG", "ORGANIZATION", "ID"),
                new RefColumn("WAYSOWNER_ID", "WHOUSE_WAYSOWNER", "CLIENT", "ID"),
                new RefColumn("STATION_ID", "WHOUSE_STATION", "DICT_RAILWAY_STATION", "ID"),
                new Column("ADDRESS_POSTAL_CODE", DbType.String),
                new Column("ADDRESS_COUNTRY", DbType.String),
                new Column("ADDRESS_REGION", DbType.String),
                new Column("ADDRESS_AREA", DbType.String),
                new Column("ADDRESS_LOCALITY", DbType.String),
                new Column("ADDRESS_STREET", DbType.String),
                new Column("ADDRESS_HOUSE", DbType.String),
                new Column("ADDRESS_HOUSING", DbType.String),
                new Column("ADDRESS_BUILDING", DbType.String),
                new Column("ADDRESS_OFFICE", DbType.String),
                new Column("ADDRESS_COMMENT", DbType.String),
                new Column("ADDRESS", DbType.String),
                new Column("LATITUDE", DbType.Decimal),
                new Column("LONGITUDE", DbType.Decimal),
                new Column("NAME", DbType.String),
                new Column("TYPE", DbType.Int16),
                new Column("TYPE_STR", DbType.String),
                new Column("IS_OWN_DRIVEWAYS", DbType.Boolean),
                new Column("COMPANY_CODE", DbType.String));
        }

        public override void Revert()
        {
            Database.RemoveEntityTable("WAREHOUSE");

            Database.RemoveColumn("CLIENT", "INN");
            Database.RemoveColumn("CLIENT", "KPP");
            Database.RemoveColumn("CLIENT", "OKPO");
        }
    }
}