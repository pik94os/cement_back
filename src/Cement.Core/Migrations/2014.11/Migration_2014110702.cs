﻿using System.Data;
using ECM7.Migrator.Framework;

namespace Cement.Core.Migrations
{
    [Migration(2014110702)]
    public class Migration_2014110702 : Migration
    {
        public override void Apply()
        {
            Database.AddColumn("DICT_DRIVER_CATEGORY", new Column("CLIENT_VALUE", DbType.Int32));
        }

        public override void Revert()
        {
            Database.RemoveColumn("DICT_DRIVER_CATEGORY", "CLIENT_VALUE");
        }
    }
}