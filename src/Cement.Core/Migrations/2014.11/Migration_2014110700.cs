﻿using System.Data;
using Cement.Core.Migrations.Extensions;
using ECM7.Migrator.Framework;

namespace Cement.Core.Migrations
{
    [Migration(2014110700)]
    public class Migration_2014110700 : Migration
    {
        public override void Apply()
        {
            Database.ChangeColumn("DICT_PACKING_TYPE", "DESCRIPTION", DbType.String.WithSize(2000), false);

            Database.AddColumn("DICT_DANGER_CLASS", new Column("NUM", DbType.String));
            Database.AddColumn("DICT_DANGER_SUB_CLASS", new Column("NUM", DbType.String));
            Database.AddRefColumn("DICT_DANGER_SUB_CLASS", new RefColumn("CLASS_ID", ColumnProperty.Null, "DICT_DNGR_CLS_SUBCLS", "DICT_DANGER_CLASS", "ID"));

            Database.AddEntityTable("DICT_OWNERSHIP", 
                new Column("NAME", DbType.String),
                new Column("SHORT_NAME", DbType.String));

            Database.AddEntityTable("DICT_PRODUCT_GROUP", new Column("NAME", DbType.String));

            Database.AddEntityTable("DICT_PRODUCT_SUBGROUP",
                new Column("NAME", DbType.String),
                new RefColumn("GROUP_ID", ColumnProperty.Null, "DICT_PROD_GRP_SUBGRP", "DICT_PRODUCT_GROUP", "ID"));

            Database.AddEntityTable("DICT_VEHICLE_BRAND", new Column("NAME", DbType.String));

            Database.AddEntityTable("DICT_VEHICLE_MODEL",
                new Column("NAME", DbType.String),
                new RefColumn("BRAND_ID", ColumnProperty.Null, "DICT_VHCL_MODEL", "DICT_VEHICLE_BRAND", "ID"));

            Database.AddEntityTable("DICT_VEHICLE_MODIFICATION",
                new Column("NAME", DbType.String),
                new RefColumn("MODEL_ID", ColumnProperty.Null, "DICT_VHCL_MODIF", "DICT_VEHICLE_MODEL", "ID"));
        }

        public override void Revert()
        {
            Database.RemoveConstraint("DICT_DANGER_SUB_CLASS", "FK_DICT_DNGR_CLS_SUBCLS");
            Database.RemoveIndex("IND_DICT_DNGR_CLS_SUBCLS", "DICT_DANGER_SUB_CLASS");
            Database.RemoveColumn("DICT_DANGER_SUB_CLASS", "CLASS_ID");
            Database.RemoveColumn("DICT_DANGER_SUB_CLASS", "NUM");
            Database.RemoveColumn("DICT_DANGER_CLASS", "NUM");

            Database.RemoveTable("DICT_PRODUCT_SUBGROUP");
            Database.RemoveTable("DICT_PRODUCT_GROUP");

            Database.RemoveTable("DICT_VEHICLE_MODIFICATION");
            Database.RemoveTable("DICT_VEHICLE_MODEL");
            Database.RemoveTable("DICT_VEHICLE_BRAND");

            Database.RemoveTable("DICT_OWNERSHIP");
        }
    }
}