﻿using System.Data;
using Cement.Core.Migrations.Extensions;
using ECM7.Migrator.Framework;

namespace Cement.Core.Migrations
{
    [Migration(2014111102)]
    public class Migration_2014111102 : Migration
    {
        public override void Apply()
        {
            Database.AddColumn("DICT_POSITION", new Column("TYPE", DbType.Int16, ColumnProperty.Null, 10));
            Database.AddColumn("PRODUCT", new Column("TYPE", DbType.Int16, ColumnProperty.Null, 10));
        }

        public override void Revert()
        {
            Database.RemoveColumn("DICT_POSITION", "TYPE");
            Database.RemoveColumn("PRODUCT", "TYPE");
        }
    }
}