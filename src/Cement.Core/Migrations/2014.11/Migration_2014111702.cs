﻿using System.Data;
using Cement.Core.Migrations.Extensions;
using ECM7.Migrator.Framework;

namespace Cement.Core.Migrations
{
    [Migration(2014111702)]
    public class Migration_2014111702 : Migration
    {
        public override void Apply()
        {
            Database.AddEntityTable("NOMENCLATURE",
                new RefColumn("ORGANIZATION_ID", "NMCLTR_ORGANIZATION", "ORGANIZATION", "ID"),
                new Column("CASE_INDEX", DbType.String),
                new Column("CASE_TITLE", DbType.String),
                new Column("STORAGE_UNITS_COUNT", DbType.Int32),
                new Column("STORAGE_LIFE", DbType.String),
                new Column("DESCRIPTION", DbType.String),
                new Column("NOMENCLATURE_YEAR", DbType.Int32));
        }

        public override void Revert()
        {
            Database.RemoveEntityTable("NOMENCLATURE");
        }
    }
}