﻿using System.Data;
using Cement.Core.Migrations.Extensions;
using ECM7.Migrator.Framework;

namespace Cement.Core.Migrations
{
    [Migration(2014111203)]
    public class Migration_2014111203 : Migration
    {
        public override void Apply()
        {
            Database.AddEntityTable("ORGANIZATION",
                new Column("NAME", DbType.String),
                new Column("FULL_NAME", DbType.String.WithSize(2000)),
                new RefColumn("OWNERSHIP_ID", "ORGANIZATION_OWNERSHIP", "DICT_OWNERSHIP", "ID"),

                new Column("JUR_ADDRESS_POSTAL_CODE", DbType.String),
                new Column("JUR_ADDRESS_COUNTRY", DbType.String),
                new Column("JUR_ADDRESS_REGION", DbType.String),
                new Column("JUR_ADDRESS_AREA", DbType.String),
                new Column("JUR_ADDRESS_LOCALITY", DbType.String),
                new Column("JUR_ADDRESS_STREET", DbType.String),
                new Column("JUR_ADDRESS_HOUSE", DbType.String),
                new Column("JUR_ADDRESS_HOUSING", DbType.String),
                new Column("JUR_ADDRESS_BUILDING", DbType.String),
                new Column("JUR_ADDRESS_OFFICE", DbType.String),

                new Column("PHONE", DbType.String),
                new Column("FAX", DbType.String),
                new Column("EMAIL", DbType.String),
                new Column("WEB", DbType.String));
            
            Database.AddEntityTable("USERS",
                new Column("LOGIN", DbType.String, ColumnProperty.NotNull),
                new Column("EMAIL", DbType.String, ColumnProperty.NotNull),
                new Column("NAME", DbType.String, ColumnProperty.NotNull),
                new Column("IS_BLOCKED", DbType.Boolean),
                new Column("CREATED_AT", DbType.DateTime),
                new Column("IS_EMAIL_CONFIRMED", DbType.Boolean),
                new Column("PASSWORD_HASH", DbType.Binary),
                new Column("SALT", DbType.Binary),
                new Column("SECRET_TOKEN", DbType.Binary),
                new RefColumn("ORGANIZATION_ID", "USER_ORGANIZATION", "ORGANIZATION", "ID"));

            Database.AddEntityTable("USER_LOGIN_LOG",
                new Column("SUCCESS", DbType.Boolean),
                new Column("LOGED_AT", DbType.DateTime),
                new Column("USER_ID", DbType.Int64, ColumnProperty.NotNull));
        }

        public override void Revert()
        {
            Database.RemoveEntityTable("USER_LOGIN_LOG");
            Database.RemoveEntityTable("USERS");
            Database.RemoveEntityTable("ORGANIZATION");
        }
    }
}