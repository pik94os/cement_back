﻿using System.Data;
using Cement.Core.Migrations.Extensions;
using ECM7.Migrator.Framework;

namespace Cement.Core.Migrations
{
    [Migration(2014111100)]
    public class Migration_2014111100 : Migration
    {
        public override void Apply()
        {
            Database.AddColumn("DICT_MEASURE_UNIT", new Column("UNIT_GROUP", DbType.Int32));
            Database.AddColumn("DICT_MEASURE_UNIT", new Column("IS_NATIONAL", DbType.Boolean));
            Database.AddColumn("DICT_MEASURE_UNIT", new Column("IS_NOT_IN_ESKK", DbType.Boolean));
        }

        public override void Revert()
        {
            Database.RemoveColumn("DICT_MEASURE_UNIT", "UNIT_GROUP");
            Database.RemoveColumn("DICT_MEASURE_UNIT", "IS_NATIONAL");
            Database.RemoveColumn("DICT_MEASURE_UNIT", "IS_NOT_IN_ESKK");
        }
    }
}