﻿using System.Data;
using Cement.Core.Migrations.Extensions;
using ECM7.Migrator.Framework;

namespace Cement.Core.Migrations
{
    [Migration(2014111201)]
    public class Migration_2014111201 : Migration
    {
        public override void Apply()
        {
            Database.AddColumn("VEHICLE_FUEL", new Column("DOCUMENT", DbType.String));
            Database.AddColumn("VEHICLE_FUEL", new Column("DATE_FIX", DbType.DateTime));
            Database.RemoveColumn("VEHICLE_FUEL", "DATE");

            Database.AddEntityTable("VEHICLE_MAINTENANCE",
                new Column("DATE_FIX", DbType.DateTime),
                new Column("MILEAGE", DbType.Decimal),
                new Column("NAME", DbType.String),
                new Column("REPAIR_MASTER", DbType.String),
                new Column("WORK_DATE", DbType.DateTime),
                new Column("COST", DbType.Decimal),
                new Column("NOTES", DbType.String),
                new Column("DOCUMENT", DbType.String),
                
                new RefColumn("DRIVER_ID", "VEH_MAINTAIN_EMPLOYEE", "EMPLOYEE", "ID"),
                new RefColumn("VEHICLE_ID", "VEH_MAINTAIN_VEH", "VEHICLE", "ID"),
                new RefColumn("CAR_SERVICE_ID", "VEH_MAINTAIN_CS_CLIENT", "CLIENT", "ID"),
                new RefColumn("DOCUMENT_ID", "VEH_MAINTAIN_DOC", "ENTITY_FILE", "ID"));

            Database.AddEntityTable("VEHICLE_ACCIDENT",
                new Column("DATE_FIX", DbType.DateTime),
                new Column("COUNT", DbType.Int32),
                new Column("DAMAGE", DbType.Decimal),
                new Column("DESCRIPTION", DbType.String),
                new Column("COST", DbType.Decimal),

                new RefColumn("DRIVER_ID", "VEH_ACCIDENT_EMPLOYEE", "EMPLOYEE", "ID"),
                new RefColumn("VEHICLE_ID", "VEH_ACCIDENT_VEH", "VEHICLE", "ID"));

            Database.AddEntityTable("VEHICLE_SPAREPART_SERVICE",
                new Column("DATE_FIX", DbType.DateTime),
                new Column("MILEAGE", DbType.Decimal),
                new Column("NAME", DbType.String),
                new Column("REPAIR_MASTER", DbType.String),
                new Column("AMOUNT", DbType.Decimal),
                new Column("COST", DbType.Decimal),
                new Column("DOCUMENT", DbType.String),
                new Column("DATE_START", DbType.DateTime),
                new Column("DATE_END", DbType.DateTime),

                new RefColumn("DRIVER_ID", "VEH_SPSRV_EMPLOYEE", "EMPLOYEE", "ID"),
                new RefColumn("VEHICLE_ID", "VEH_SPSRV_VEH", "VEHICLE", "ID"),
                new RefColumn("CAR_SERVICE_ID", "VEH_SPSRV_CLIENT", "CLIENT", "ID"),
                new RefColumn("MEASURE_UNIT_ID", "VEH_SPSRV_MEASUNIT", "DICT_MEASURE_UNIT", "ID"),
                new RefColumn("DOCUMENT_ID", "VEH_SPSRV_DOC", "ENTITY_FILE", "ID"));
        }

        public override void Revert()
        {
            Database.RemoveEntityTable("VEHICLE_SPAREPART_SERVICE");
            Database.RemoveEntityTable("VEHICLE_ACCIDENT");
            Database.RemoveEntityTable("VEHICLE_MAINTENANCE");

            Database.RemoveColumn("VEHICLE_FUEL", "DATE_FIX");
            Database.RemoveColumn("VEHICLE_FUEL", "DOCUMENT");
            Database.AddColumn("VEHICLE_FUEL", new Column("DATE", DbType.DateTime));
        }
    }
}