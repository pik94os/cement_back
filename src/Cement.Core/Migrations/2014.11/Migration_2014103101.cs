﻿using System.Data;
using Cement.Core.Migrations.Extensions;
using ECM7.Migrator.Framework;

namespace Cement.Core.Migrations
{
    [Migration(2014103101)]
    public class Migration_2014103101 : Migration
    {
        public override void Apply()
        {
            Database.AddEntityTable("DICT_POSITION", new Column("NAME", DbType.String));
            Database.AddEntityTable("DICT_DEPARTMENT", new Column("NAME", DbType.String));

            Database.AddEntityTable("ADDRESS_DICT", 
                new Column("POSTALCODE", DbType.String),
                new Column("COUNTRY", DbType.String),
                new Column("REGION", DbType.String),
                new Column("AREA", DbType.String),
                new Column("LOCALITY", DbType.String),
                new Column("STREET", DbType.String));

            Database.AddIndex("IND_ADDRESS_DICT_INDEX", false, "ADDRESS_DICT", "POSTALCODE");

            Database.AddEntityTable("ADDRESS",
                new Column("POSTALCODE", DbType.String),
                new Column("COUNTRY", DbType.String),
                new Column("REGION", DbType.String),
                new Column("AREA", DbType.String),
                new Column("LOCALITY", DbType.String),
                new Column("STREET", DbType.String),
                new Column("HOUSE", DbType.String),
                new Column("OFFICE", DbType.String),
                new Column("APARTMENT", DbType.String),
                new Column("COMMENT", DbType.String));
        }

        public override void Revert()
        {
            Database.RemoveTable("DICT_POSITION");
            Database.RemoveTable("DICT_DEPARTMENT");

            Database.RemoveIndex("IND_ADDRESS_DICT_INDEX", "ADDRESS_DICT");
            Database.RemoveTable("ADDRESS_DICT");
            Database.RemoveTable("ADDRESS");
        }
    }
}