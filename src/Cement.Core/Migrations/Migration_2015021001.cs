﻿using System.Data;
using Cement.Core.Migrations.Extensions;
using ECM7.Migrator.Framework;

namespace Cement.Core.Migrations
{
    [Migration(2015021001)]
    public class Migration_2015021001 : Migration
    {
        public override void Apply()
        {
            Database.AddEntityTable("TRANSPORT_UNIT_ACCOUNTING",
                new RefColumn("ORGANIZATION_ID", ColumnProperty.NotNull, "TRUNIT_ACC_ORG", "ORGANIZATION", "ID"),
                new RefColumn("TRANSPORT_UNIT_ID", ColumnProperty.NotNull, "TRUNIT_ACC_TRUNIT", "TRANSPORT_UNIT", "ID"),
                new Column("TYPE", DbType.Int16, ColumnProperty.NotNull),
                new Column("DATE_ACCOUNTING", DbType.DateTime, ColumnProperty.NotNull));
        }

        public override void Revert()
        {
            Database.RemoveEntityTable("TRANSPORT_UNIT_ACCOUNTING");
        }
    }
}