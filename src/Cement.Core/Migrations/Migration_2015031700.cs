﻿using System.Data;
using Cement.Core.Enums;
using Cement.Core.Migrations.Extensions;
using ECM7.Migrator.Framework;

namespace Cement.Core.Migrations
{
    [Migration(2015031700)]
    public class Migration_2015031700 : Migration
    {
        public override void Apply()
        {
            Database.AddRefColumn("INVENTORY", new RefColumn("CHAIRMAN_ID", "INVENTORY_CHAIRMAN", "EMPLOYEE", "ID"));
            Database.AddRefColumn("INVENTORY", new RefColumn("DIRECTOR_ID", "INVENTORY_DIRECTOR", "EMPLOYEE", "ID"));
            Database.AddColumn("INVENTORY_PRODUCT", new Column("FACT_COUNT", DbType.Int32, ColumnProperty.NotNull, 0));
        }

        public override void Revert()
        {
            Database.RemoveRefColumn("INVENTORY", "CHAIRMAN_ID", "INVENTORY_CHAIRMAN");
            Database.RemoveRefColumn("INVENTORY", "DIRECTOR_ID", "INVENTORY_DIRECTOR");
            Database.RemoveColumn("INVENTORY_PRODUCT", "FACT_COUNT");
        }
    }
}