﻿using System.Data;
using ECM7.Migrator.Framework;

namespace Cement.Core.Migrations
{
    [Migration(2015022500)]
    public class Migration_2015022500 : Migration
    {
        public override void Apply()
        {
            Database.AddColumn("CORRESPONDENCE", new Column("SIGNED_AT", DbType.DateTime));
        }

        public override void Revert()
        {
            Database.RemoveColumn("CORRESPONDENCE", "SIGNED_AT");
        }
    }
}