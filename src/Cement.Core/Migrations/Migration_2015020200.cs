﻿using Cement.Core.Migrations.Extensions;
using ECM7.Migrator.Framework;

namespace Cement.Core.Migrations
{
    [Migration(2015020200)]
    public class Migration_2015020200 : Migration
    {
        public override void Apply()
        {
            Database.AddRefColumn("PRODUCT", new RefColumn("PHOTO_FILE_ID", "PRODUCT_PHOTO_FILE", "ENTITY_FILE", "ID"));
        }

        public override void Revert()
        {
            Database.RemoveRefColumn("PRODUCT", "PHOTO_FILE_ID", "PRODUCT_PHOTO_FILE");
        }
    }
}