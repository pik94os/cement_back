﻿using System.Data;
using Cement.Core.Enums;
using Cement.Core.Migrations.Extensions;
using ECM7.Migrator.Framework;

namespace Cement.Core.Migrations
{
    [Migration(2015031601)]
    public class Migration_2015031601 : Migration
    {
        public override void Apply()
        {
            Database.AddEntityTable("STOCKROOM_EXPENSE_ORDER",
                new RefColumn("WAREHOUSE_ID", ColumnProperty.NotNull, "STR_EXPORD_WAREHOUSE", "WAREHOUSE", "ID"),
                new Column("TYPE", DbType.Int16, ColumnProperty.NotNull),
                new Column("NO", DbType.String),
                new Column("DOCUMENT_DATE", DbType.DateTime),
                new Column("STATE", DbType.Int16, ColumnProperty.NotNull, (int)DocumentState.Draft),
                new Column("AMOUNT", DbType.Decimal, ColumnProperty.NotNull, 0),
                new RefColumn("PRODUCT_REQUEST_ID", "STR_EXPORD_PRODREQUEST", "PRODUCT_REQUEST", "ID"),
                new RefColumn("STOCKROOM_REQUEST_ID", "STR_EXPORD_STR_REQUEST", "STOCKROOM_REQUEST", "ID"));

            Database.AddEntityTable("STOCKROOM_EXPENSE_ORDER_ROUTE",
                new RefColumn("STOCKROOM_EXPENSE_ORDER_ID", ColumnProperty.NotNull, "STR_EXPORDROUTE_STR_EXPORD", "STOCKROOM_EXPENSE_ORDER", "ID"),
                new RefColumn("EMPLOYEE_ID", ColumnProperty.NotNull, "STR_EXPORDROUTE_EMPLOYEE", "EMPLOYEE", "ID"),
                new Column("DATE_IN", DbType.DateTime, ColumnProperty.NotNull),
                new Column("DATE_OUT", DbType.DateTime),
                new Column("COMMENT", DbType.String.WithSize(2000)),
                new Column("PROCESSING_RESULT", DbType.Int16),
                new Column("MOVEMENT_TYPE", DbType.Int16, ColumnProperty.NotNull),
                new Column("ARCHIVED_AT", DbType.DateTime),
                new Column("DELETED_AT", DbType.DateTime),
                new Column("STAGE", DbType.Int16, ColumnProperty.NotNull, 0));
        }

        public override void Revert()
        {
            Database.RemoveEntityTable("STOCKROOM_EXPENSE_ORDER_ROUTE");
            Database.RemoveEntityTable("STOCKROOM_EXPENSE_ORDER");
        }
    }
}