﻿using System.Data;
using Cement.Core.Migrations.Extensions;
using ECM7.Migrator.Framework;

namespace Cement.Core.Migrations
{
    [Migration(2015031000)]
    public class Migration_2015031000 : Migration
    {
        public override void Apply()
        {
            Database.AddEntityTable("STOCKROOM_PRODUCT",
                new RefColumn("WAREHOUSE_ID", ColumnProperty.NotNull, "STOCKPROD_WAREHOUSE", "WAREHOUSE", "ID"),
                new RefColumn("PRODUCT_ID", ColumnProperty.NotNull, "STOCKPROD_PRODUCT", "PRODUCT", "ID"),
                new RefColumn("MEASURE_UNIT_ID", ColumnProperty.NotNull, "STOCKPROD_MEASUNIT", "DICT_MEASURE_UNIT", "ID"),
                new Column("AMOUNT", DbType.Decimal, ColumnProperty.NotNull, 0));

            Database.AddEntityTable("STOCKROOM_REQUEST",
                new RefColumn("WAREHOUSE_ID", ColumnProperty.NotNull, "STOCKREQ_WAREHOUSE", "WAREHOUSE", "ID"),
                new RefColumn("STOCKROOM_PRODUCT_ID", ColumnProperty.NotNull, "STOCKREQ_STOCKPROD", "STOCKROOM_PRODUCT", "ID"),
                new Column("TYPE", DbType.Int16, ColumnProperty.NotNull, 0),
                new Column("NO", DbType.String),
                new Column("REQUEST_DATE", DbType.DateTime),
                new Column("AMOUNT", DbType.Decimal, ColumnProperty.NotNull, 0),
                new Column("EXTRA_DATA", DbType.String.WithSize(2000)),
                new Column("FOUNDATION", DbType.String),

                new Column("REASON", DbType.String),
                new RefColumn("WAREHOUSE_TO_ID", "STOCKREQ_WAREHOUSE_TO", "WAREHOUSE", "ID"));
        }

        public override void Revert()
        {
            Database.RemoveEntityTable("STOCKROOM_REQUEST");
            Database.RemoveEntityTable("STOCKROOM_PRODUCT");
        }
    }
}