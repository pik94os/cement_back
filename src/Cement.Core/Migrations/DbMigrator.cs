﻿using System.Linq;
using ECM7.Migrator;
using ECM7.Migrator.Framework;

namespace Cement.Core.Migrations
{
    public static class DbMigrator
    {
        private static Migrator GetMigrator(string connectionString, string provider)
        {
            var migrator = new Migrator(
                provider,
                connectionString,
                typeof(DbMigrator).Assembly);


            return migrator;
        }

        private static Migrator GetMigrator(ITransformationProvider provider)
        {
            var migrator = new Migrator(provider, typeof(DbMigrator).Assembly);

            return migrator;
        }

        public static void MigrateToLast(ITransformationProvider provider)
        {
            var migrator = GetMigrator(provider);

            migrator.Migrate();
        }

        public static void MigrateToPrev(ITransformationProvider provider)
        {
            var migrator = GetMigrator(provider);

            var applied = migrator.GetAppliedMigrations();

            migrator.Migrate(applied.Count >= 2 ? applied[applied.Count - 2] : 0);
        }

        public static void MigrateTo(ITransformationProvider provider, long version)
        {
            var migrator = GetMigrator(provider);

            migrator.Migrate(version);
        }

        public static void UpdateViews(ITransformationProvider provider)
        {
            var migrator = GetMigrator(provider);

            DbViewCollection.Update(migrator.Provider);
        }

        //public static void MigrateToLast(string connectionString, string provider)
        //{
        //    var migrator = GetMigrator(connectionString, provider);

        //    migrator.Migrate();
        //}

        //public static void MigrateToPrev(string connectionString, string provider)
        //{
        //    var migrator = GetMigrator(connectionString, provider);

        //    var applied = migrator.GetAppliedMigrations();

        //    migrator.Migrate(applied.Count >= 2 ? applied[applied.Count - 2] : 0);
        //}

        //public static void MigrateTo(string connectionString, string provider, long version)
        //{
        //    var migrator = GetMigrator(connectionString, provider);

        //    migrator.Migrate(version);
        //}

        //public static void UpdateViews(string connectionString, string provider)
        //{
        //    var migrator = GetMigrator(connectionString, provider);

        //    DbViewCollection.Update(migrator.Provider);
        //}
    }
}