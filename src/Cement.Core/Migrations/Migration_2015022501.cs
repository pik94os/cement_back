﻿using System.Data;
using Cement.Core.Migrations.Extensions;
using ECM7.Migrator.Framework;

namespace Cement.Core.Migrations
{
    [Migration(2015022501)]
    public class Migration_2015022501 : Migration
    {
        public override void Apply()
        {
            Database.AddColumn("PRODUCT_REQUEST", new Column("IS_LAST", DbType.Boolean, ColumnProperty.NotNull, false));
            Database.RemoveRefColumn("PRODUCT_REQUEST", "REQUEST_ID", "PROD_RQST_RQST");
        }

        public override void Revert()
        {
            Database.RemoveColumn("PRODUCT_REQUEST", "IS_LAST");
            Database.AddRefColumn("PRODUCT_REQUEST", new RefColumn("REQUEST_ID", "PROD_RQST_RQST", "PRODUCT_REQUEST", "ID"));
        }
    }
}