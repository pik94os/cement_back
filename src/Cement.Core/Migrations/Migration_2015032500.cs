﻿using System.Data;
using Cement.Core.Enums;
using Cement.Core.Migrations.Extensions;
using ECM7.Migrator.Framework;

namespace Cement.Core.Migrations
{
    [Migration(2015032500)]
    public class Migration_2015032500 : Migration
    {
        public override void Apply()
        {
            Database.AddEntityTable("ACCOUNTING_OTHER",
                new Column("TOTAL_SUM", DbType.Decimal, ColumnProperty.NotNull),
                new Column("TYPE", DbType.Int16, ColumnProperty.NotNull),
                new Column("NO", DbType.String),
                new Column("DOCUMENT_DATE", DbType.DateTime),
                new Column("STATE", DbType.Int16, ColumnProperty.NotNull, (int)DocumentState.Draft),
                new Column("KIND", DbType.Int16));

            Database.AddEntityTable("ACCOUNTING_OTHER_ROUTE",
                new RefColumn("ACCOUNTING_OTHER_ID", ColumnProperty.NotNull, "ACCOTHERROUTE_ACCOTHER", "ACCOUNTING_OTHER", "ID"),
                new RefColumn("EMPLOYEE_ID", ColumnProperty.NotNull, "ACCOTHERROUTE_EMPLOYEE", "EMPLOYEE", "ID"),
                new Column("DATE_IN", DbType.DateTime, ColumnProperty.NotNull),
                new Column("DATE_OUT", DbType.DateTime),
                new Column("COMMENT", DbType.String.WithSize(2000)),
                new Column("PROCESSING_RESULT", DbType.Int16),
                new Column("MOVEMENT_TYPE", DbType.Int16, ColumnProperty.NotNull),
                new Column("ARCHIVED_AT", DbType.DateTime),
                new Column("DELETED_AT", DbType.DateTime),
                new Column("STAGE", DbType.Int16, ColumnProperty.NotNull, 0));

            Database.AddRefColumn("STOCKROOM_EXPENSE_ORDER", new RefColumn("ACCOUNTING_OTHER_ID", "STREXPORD_ACCOTHER", "ACCOUNTING_OTHER", "ID"));
            Database.AddRefColumn("STOCKROOM_REQUEST", new RefColumn("ACCOUNTING_OTHER_ID", "STRREQ_ACCOTHER", "ACCOUNTING_OTHER", "ID"));
        }

        public override void Revert()
        {
            Database.RemoveRefColumn("STOCKROOM_REQUEST", "ACCOUNTING_OTHER_ID", "STRREQ_ACCOTHER");
            Database.RemoveRefColumn("STOCKROOM_EXPENSE_ORDER", "ACCOUNTING_OTHER_ID", "STREXPORD_ACCOTHER");

            Database.RemoveEntityTable("ACCOUNTING_OTHER_ROUTE");
            Database.RemoveEntityTable("ACCOUNTING_OTHER");
        }
    }
}