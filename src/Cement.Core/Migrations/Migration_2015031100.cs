﻿using System.Data;
using Cement.Core.Enums;
using Cement.Core.Migrations.Extensions;
using ECM7.Migrator.Framework;

namespace Cement.Core.Migrations
{
    [Migration(2015031100)]
    public class Migration_2015031100 : Migration
    {
        public override void Apply()
        {
            Database.AddEntityTable("STOCKROOM_REQUEST_ROUTE",
                new RefColumn("STOCKROOM_REQUEST_ID", ColumnProperty.NotNull, "STOCKREQROUTE_STOCKREQ", "STOCKROOM_REQUEST", "ID"),
                new RefColumn("EMPLOYEE_ID", ColumnProperty.NotNull, "STOCKREQROUTE_EMPLOYEE", "EMPLOYEE", "ID"),
                new Column("DATE_IN", DbType.DateTime, ColumnProperty.NotNull),
                new Column("DATE_OUT", DbType.DateTime),
                new Column("COMMENT", DbType.String.WithSize(2000)),
                new Column("PROCESSING_RESULT", DbType.Int16),
                new Column("MOVEMENT_TYPE", DbType.Int16, ColumnProperty.NotNull),
                new Column("ARCHIVED_AT", DbType.DateTime),
                new Column("DELETED_AT", DbType.DateTime),
                new Column("STAGE", DbType.Int16, ColumnProperty.NotNull, 0));

            Database.AddColumn("STOCKROOM_REQUEST", new Column("STATE", DbType.Int16, ColumnProperty.NotNull, (int)DocumentState.Draft));
        }

        public override void Revert()
        {
            Database.RemoveColumn("STOCKROOM_REQUEST", "STATE");
            Database.RemoveEntityTable("STOCKROOM_REQUEST_ROUTE");
        }
    }
}