﻿using System.Data;
using Cement.Core.Enums;
using Cement.Core.Migrations.Extensions;
using ECM7.Migrator.Framework;

namespace Cement.Core.Migrations
{
    [Migration(2015032700)]
    public class Migration_2015032700 : Migration
    {
        public override void Apply()
        {
            Database.AddEntityTable("ACCOUNTING_INOUT_DOC",
                new Column("TOTAL_SUM", DbType.Decimal, ColumnProperty.NotNull),
                new Column("TYPE", DbType.Int16, ColumnProperty.NotNull),
                new Column("NO", DbType.String),
                new Column("DOCUMENT_DATE", DbType.DateTime),
                new Column("STATE", DbType.Int16, ColumnProperty.NotNull, (int)DocumentState.Draft),
                new Column("KIND", DbType.Int16),
                new Column("SHIPPING_KIND", DbType.Int16),
                new Column("USED_AT", DbType.DateTime),
                new RefColumn("ACCOUNTING_INOUT_DOC_ID", "ACCINOUTDOC_ACCINOUTDOC", "ACCOUNTING_INOUT_DOC", "ID"),
                new RefColumn("ORGANIZATION_FROM_ID", ColumnProperty.NotNull, "ACCINOUTDOC_ORGANIZATION_FROM", "ORGANIZATION", "ID"),
                new RefColumn("ORGANIZATION_TO_ID", ColumnProperty.NotNull, "ACCINOUTDOC_ORGANIZATION_TO", "ORGANIZATION", "ID"));

            Database.AddEntityTable("ACCOUNTING_INOUT_DOC_ROUTE",
                new RefColumn("ACCOUNTING_INOUT_DOC_ID", ColumnProperty.NotNull, "ACCINOUTDOCROUTE_ACCINOUTDOC", "ACCOUNTING_INOUT_DOC", "ID"),
                new RefColumn("EMPLOYEE_ID", ColumnProperty.NotNull, "ACCINOUTDOCROUTE_EMPLOYEE", "EMPLOYEE", "ID"),
                new Column("DATE_IN", DbType.DateTime, ColumnProperty.NotNull),
                new Column("DATE_OUT", DbType.DateTime),
                new Column("COMMENT", DbType.String.WithSize(2000)),
                new Column("PROCESSING_RESULT", DbType.Int16),
                new Column("MOVEMENT_TYPE", DbType.Int16, ColumnProperty.NotNull),
                new Column("ARCHIVED_AT", DbType.DateTime),
                new Column("DELETED_AT", DbType.DateTime),
                new Column("STAGE", DbType.Int16, ColumnProperty.NotNull, 0));

            Database.AddRefColumn("STOCKROOM_EXPENSE_ORDER", new RefColumn("ACCOUNTING_INOUT_DOC_ID", "STREXPORD_ACCINOUTDOC", "ACCOUNTING_INOUT_DOC", "ID"));
        }

        public override void Revert()
        {
            Database.RemoveRefColumn("STOCKROOM_EXPENSE_ORDER", "ACCOUNTING_INOUT_DOC_ID", "STREXPORD_ACCINOUTDOC");

            Database.RemoveEntityTable("ACCOUNTING_INOUT_DOC_ROUTE");
            Database.RemoveEntityTable("ACCOUNTING_INOUT_DOC");
        }
    }
}