﻿using System.Data;
using Cement.Core.Migrations.Extensions;
using ECM7.Migrator.Framework;

namespace Cement.Core.Migrations
{
    [Migration(2015020500)]
    public class Migration_2015020500 : Migration
    {
        public override void Apply()
        {
            Database.AddRefColumn("DICT_PACKING_TYPE", new RefColumn("PHOTO_FILE_ID", "PACKTYPE_PHOTO_FILE", "ENTITY_FILE", "ID"));

            Database.AddColumn("PRODUCT", new Column("CONFORM_CERT_START", DbType.DateTime, ColumnProperty.Null));
            Database.AddColumn("PRODUCT", new Column("CONFORM_CERT_END", DbType.DateTime, ColumnProperty.Null));
            Database.AddColumn("PRODUCT", new Column("CONFORM_DECL_START", DbType.DateTime, ColumnProperty.Null));
            Database.AddColumn("PRODUCT", new Column("CONFORM_DECL_END", DbType.DateTime, ColumnProperty.Null));
            Database.AddColumn("PRODUCT", new Column("HEALTH_CERT_START", DbType.DateTime, ColumnProperty.Null));
            Database.AddColumn("PRODUCT", new Column("HEALTH_CERT_END", DbType.DateTime, ColumnProperty.Null));
            Database.AddColumn("PRODUCT", new Column("FIRE_CERT_START", DbType.DateTime, ColumnProperty.Null));
            Database.AddColumn("PRODUCT", new Column("FIRE_CERT_END", DbType.DateTime, ColumnProperty.Null));
            Database.AddColumn("PRODUCT", new Column("FIRE_DECL_START", DbType.DateTime, ColumnProperty.Null));
            Database.AddColumn("PRODUCT", new Column("FIRE_DECL_END", DbType.DateTime, ColumnProperty.Null));
        }

        public override void Revert()
        {
            Database.RemoveRefColumn("DICT_PACKING_TYPE", "PHOTO_FILE_ID", "PACKTYPE_PHOTO_FILE");

            Database.RemoveColumn("PRODUCT", "CONFORM_CERT_START");
            Database.RemoveColumn("PRODUCT", "CONFORM_CERT_END");
            Database.RemoveColumn("PRODUCT", "CONFORM_DECL_START");
            Database.RemoveColumn("PRODUCT", "CONFORM_DECL_END");
            Database.RemoveColumn("PRODUCT", "HEALTH_CERT_START");
            Database.RemoveColumn("PRODUCT", "HEALTH_CERT_END");
            Database.RemoveColumn("PRODUCT", "FIRE_CERT_START");
            Database.RemoveColumn("PRODUCT", "FIRE_CERT_END");
            Database.RemoveColumn("PRODUCT", "FIRE_DECL_START");
            Database.RemoveColumn("PRODUCT", "FIRE_DECL_END");
        }
    }
}