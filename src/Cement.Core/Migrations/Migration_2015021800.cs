﻿using System.Data;
using Cement.Core.Migrations.Extensions;
using ECM7.Migrator.Framework;

namespace Cement.Core.Migrations
{
    [Migration(2015021800)]
    public class Migration_2015021800 : Migration
    {
        public override void Apply()
        {
            Database.RemoveColumn("PRODUCT_REQUEST", "IS_NO_REQUEST");
            Database.RemoveColumn("PRODUCT_REQUEST", "IS_NO_TEMPLATE");
            Database.RemoveColumn("PRODUCT_REQUEST", "IS_NO_PROVIDER");
            Database.RemoveColumn("PRODUCT_REQUEST", "IS_PAYER_CONSIGNEE");

            Database.AddRefColumn("PRODUCT_REQUEST", new RefColumn("PROVIDER_ID", "REQUEST_PROVIDER", "ORGANIZATION", "ID"));
        }

        public override void Revert()
        {
            Database.RemoveRefColumn("PRODUCT_REQUEST", "PROVIDER_ID", "REQUEST_PROVIDER");

            Database.AddColumn("PRODUCT_REQUEST", new Column("IS_NO_REQUEST", DbType.Boolean));
            Database.AddColumn("PRODUCT_REQUEST", new Column("IS_NO_TEMPLATE", DbType.Boolean));
            Database.AddColumn("PRODUCT_REQUEST", new Column("IS_NO_PROVIDER", DbType.Boolean));
            Database.AddColumn("PRODUCT_REQUEST", new Column("IS_PAYER_CONSIGNEE", DbType.Boolean));
        }
    }
}