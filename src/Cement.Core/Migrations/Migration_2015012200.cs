﻿using System.Data;
using ECM7.Migrator.Framework;

namespace Cement.Core.Migrations
{
    [Migration(2015012200)]
    public class Migration_2015012200 : Migration
    {
        public override void Apply()
        {
            Database.ChangeColumn("PERSONAL_CLIENT", "ORGANIZATION_ID", DbType.Int64, true);
            Database.ChangeColumn("PERSONAL_CLIENT", "CLIENT_ID", DbType.Int64, true);
            Database.ChangeColumn("PERSONAL_CLIENT", "STATE_ID", DbType.Int64, true);
            Database.ChangeColumn("PERSONAL_CLIENT", "GROUP_ID", DbType.Int64, true);
            Database.ChangeColumn("PERSONAL_CLIENT", "SUBGROUP_ID", DbType.Int64, true);
        }

        public override void Revert()
        {
            
        }
    }
}