﻿using System.Data;
using ECM7.Migrator.Framework;

namespace Cement.Core.Migrations
{
    [Migration(2015013000)]
    public class Migration_2015013000 : Migration
    {
        public override void Apply()
        {
            Database.AddColumn("CONTRACT_ANNEX", new Column("SUBTYPE", DbType.Int64, ColumnProperty.NotNull, 0));
            Database.AddColumn("CONTRACT_ANNEX_PRICELIST", new Column("TYPE", DbType.Int64, ColumnProperty.NotNull, 0));
        }

        public override void Revert()
        {
            Database.RemoveColumn("CONTRACT_ANNEX", "SUBTYPE");
            Database.RemoveColumn("CONTRACT_ANNEX_PRICELIST", "TYPE");
        }
    }
}