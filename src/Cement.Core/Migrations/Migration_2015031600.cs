﻿using System.Data;
using Cement.Core.Enums;
using Cement.Core.Migrations.Extensions;
using ECM7.Migrator.Framework;

namespace Cement.Core.Migrations
{
    [Migration(2015031600)]
    public class Migration_2015031600 : Migration
    {
        public override void Apply()
        {
            Database.AddEntityTable("INVENTORY",
                new RefColumn("WAREHOUSE_ID", ColumnProperty.NotNull, "INVENTORY_WAREHOUSE", "WAREHOUSE", "ID"),
                new Column("NO", DbType.String),
                new Column("INVENTORY_DATE", DbType.DateTime),
                new Column("KIND", DbType.Int16, ColumnProperty.NotNull, 0),
                new Column("STATE", DbType.Int16, ColumnProperty.NotNull, (int)DocumentState.Draft));

            Database.AddEntityTable("INVENTORY_MEMBER",
                new RefColumn("INVENTORY_ID", ColumnProperty.NotNull, "INV_MEMBER_INV", "INVENTORY", "ID"),
                new RefColumn("EMPLOYEE_ID", ColumnProperty.NotNull, "INV_MEMBER_EMP", "EMPLOYEE", "ID"));

            Database.AddEntityTable("INVENTORY_PRODUCT",
                new RefColumn("INVENTORY_ID", ColumnProperty.NotNull, "INV_PRODUCT_INV", "INVENTORY", "ID"),
                new RefColumn("SR_PRODUCT_ID", ColumnProperty.NotNull, "INV_PRODUCT_PROD", "STOCKROOM_PRODUCT", "ID"));

            Database.AddEntityTable("INVENTORY_ROUTE",
                new RefColumn("INVENTORY_ID", ColumnProperty.NotNull, "INVENTORYROUTE_INVENTORY", "INVENTORY", "ID"),
                new RefColumn("EMPLOYEE_ID", ColumnProperty.NotNull, "INVENTORYROUTE_EMPLOYEE", "EMPLOYEE", "ID"),
                new Column("DATE_IN", DbType.DateTime, ColumnProperty.NotNull),
                new Column("DATE_OUT", DbType.DateTime),
                new Column("COMMENT", DbType.String.WithSize(2000)),
                new Column("PROCESSING_RESULT", DbType.Int16),
                new Column("MOVEMENT_TYPE", DbType.Int16, ColumnProperty.NotNull),
                new Column("ARCHIVED_AT", DbType.DateTime),
                new Column("DELETED_AT", DbType.DateTime),
                new Column("STAGE", DbType.Int16, ColumnProperty.NotNull, 0));
        }

        public override void Revert()
        {
            Database.RemoveEntityTable("INVENTORY_ROUTE");
            Database.RemoveEntityTable("INVENTORY_MEMBER");
            Database.RemoveEntityTable("INVENTORY_PRODUCT");
            Database.RemoveEntityTable("INVENTORY");
        }
    }
}