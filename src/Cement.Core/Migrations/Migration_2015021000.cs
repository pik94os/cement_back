﻿using System.Data;
using Cement.Core.Migrations.Extensions;
using ECM7.Migrator.Framework;

namespace Cement.Core.Migrations
{
    [Migration(2015021000)]
    public class Migration_2015021000 : Migration
    {
        public override void Apply()
        {
            Database.RemoveRefColumn("PRODUCT_REQUEST", "PLIST_PRODUCT_ID", "RQST_PLIST_PRODUCT");
            Database.AddRefColumn("PRODUCT_REQUEST", new RefColumn("PRODUCT_ID", "RQST_PRODUCT", "PRODUCT", "ID"));
            Database.AddRefColumn("PRODUCT_REQUEST", new RefColumn("PRODUCT_UNIT_ID", "RQST_PRODUCT_UNIT", "DICT_MEASURE_UNIT", "ID"));
            Database.AddRefColumn("PRODUCT_REQUEST", new RefColumn("WAREHOUSE_ID", "RQST_PRODUCT_WRHS", "WAREHOUSE", "ID"));
            Database.AddColumn("PRODUCT_REQUEST", new Column("PRODUCT_PRICE", DbType.Decimal));
        }

        public override void Revert()
        {
            Database.RemoveColumn("PRODUCT_REQUEST", "PRODUCT_PRICE");
            Database.RemoveRefColumn("PRODUCT_REQUEST", "PRODUCT_UNIT_ID", "RQST_PRODUCT_UNIT");
            Database.RemoveRefColumn("PRODUCT_REQUEST", "PRODUCT_ID", "RQST_PRODUCT");
            Database.RemoveRefColumn("PRODUCT_REQUEST", "WAREHOUSE_ID", "RQST_PRODUCT_WRHS");
            Database.AddRefColumn("PRODUCT_REQUEST", new RefColumn("PLIST_PRODUCT_ID", "RQST_PLIST_PRODUCT", "PRICE_LIST_PRODUCT", "ID"));
        }
    }
}