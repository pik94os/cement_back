﻿using System.Data;
using Cement.Core.Enums;
using Cement.Core.Migrations.Extensions;
using ECM7.Migrator.Framework;

namespace Cement.Core.Migrations
{
    [Migration(2015031801)]
    public class Migration_2015031801 : Migration
    {
        public override void Apply()
        {
            Database.AddEntityTable("DOCUMENT",
                new Column("No", DbType.String),
                new Column("DOCUMENT_DATE", DbType.DateTime),
                new Column("NAME", DbType.String),
                new Column("TYPE", DbType.Int16, ColumnProperty.NotNull),
                new Column("STATE", DbType.Int16, ColumnProperty.NotNull, (int)DocumentState.Draft));

            Database.AddEntityTable("STOCKROOM_REQUEST_FOUNDATION",
                new RefColumn("REQUEST_ID", ColumnProperty.NotNull, "STRREQFOUNDATION_STRREQUEST", "STOCKROOM_REQUEST", "ID"),
                new RefColumn("DOCUMENT_ID", ColumnProperty.NotNull, "STRREQFOUNDATION_DOCUMENT", "DOCUMENT", "ID"));

            Database.AddColumn("STOCKROOM_REQUEST", new Column("USED_AT", DbType.DateTime));
            Database.AddColumn("PRODUCT_REQUEST", new Column("USED_AT", DbType.DateTime));
        }

        public override void Revert()
        {
            Database.RemoveEntityTable("STOCKROOM_REQUEST_FOUNDATION");
            Database.RemoveEntityTable("DOCUMENT");
            Database.RemoveColumn("STOCKROOM_REQUEST", "USED_AT");
            Database.RemoveColumn("PRODUCT_REQUEST", "USED_AT");
        }
    }
}