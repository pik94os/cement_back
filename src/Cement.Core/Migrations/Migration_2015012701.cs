﻿using System.Data;
using Cement.Core.Migrations.Extensions;
using ECM7.Migrator.Framework;

namespace Cement.Core.Migrations
{
    [Migration(2015012701)]
    public class Migration_2015012701 : Migration
    {
        public override void Apply()
        {
            Database.AddEntityTable("CONTRACT_MOVEMENT",
                new RefColumn("DOCUMENT_ID", ColumnProperty.NotNull, "CONTR_MVT_CONTR", "CONTRACT", "ID"),
                new RefColumn("EMPLOYEE_ID", "CONTR_MVT_EMPLOYEE", "EMPLOYEE", "ID"),
                new RefColumn("ORGANIZATION_ID", ColumnProperty.NotNull, "CONTR_MVT_ORGANIZATION", "ORGANIZATION", "ID"),
                new Column("TYPE", DbType.Int16, ColumnProperty.NotNull),
                new Column("IS_ARCHIVED", DbType.Boolean, ColumnProperty.Null, false));

            Database.AddEntityTable("CONTRACT_ANNEX_MOVEMENT",
                new RefColumn("DOCUMENT_ID", ColumnProperty.NotNull, "CONTRANNMVT_CONTRANN", "CONTRACT_ANNEX", "ID"),
                new RefColumn("EMPLOYEE_ID", "CONTRANNEX_MVT_EMPLOYEE", "EMPLOYEE", "ID"),
                new RefColumn("ORGANIZATION_ID", ColumnProperty.NotNull, "CONTRANNEX_MVT_ORG", "ORGANIZATION", "ID"),
                new Column("TYPE", DbType.Int16, ColumnProperty.NotNull),
                new Column("IS_ARCHIVED", DbType.Boolean, ColumnProperty.Null, false));
        }

        public override void Revert()
        {
            Database.RemoveEntityTable("CONTRACT_MOVEMENT");
            Database.RemoveEntityTable("CONTRACT_ANNEX_MOVEMENT");
        }
    }
}