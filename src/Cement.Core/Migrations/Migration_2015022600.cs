﻿using System.Data;
using Cement.Core.Entities;
using ECM7.Migrator.Framework;

namespace Cement.Core.Migrations
{
    [Migration(2015022600)]
    public class Migration_2015022600 : Migration
    {
        public override void Apply()
        {
            Database.AddColumn("USERS", new Column("ROLE", DbType.Int64, ColumnProperty.NotNull, (int)UserRole.Employee));
        }

        public override void Revert()
        {
            Database.RemoveColumn("USERS", "ROLE");
        }
    }
}