﻿using System.Data;
using Cement.Core.Migrations.Extensions;
using ECM7.Migrator.Framework;

namespace Cement.Core.Migrations
{
    [Migration(2015031900)]
    public class Migration_2015031900 : Migration
    {
        public override void Apply()
        {
            Database.AddEntityTable("STOCKROOM_PRODUCT_HISTORY",
                new RefColumn("STOCKROOM_PRODUCT_ID", ColumnProperty.NotNull, "STRPRODHIST_STRPRODUCT", "STOCKROOM_PRODUCT", "ID"),
                new Column("HISTORY_DATE", DbType.DateTime, ColumnProperty.NotNull),
                new Column("IS_EXPENSE", DbType.Boolean, ColumnProperty.NotNull, false),
                new Column("INITIAL_AMOUNT", DbType.Decimal, ColumnProperty.NotNull, 0),
                new Column("INITIAL_PRICE", DbType.Decimal, ColumnProperty.NotNull, 0),
                new Column("INITIAL_TAX", DbType.Decimal, ColumnProperty.NotNull, 0),
                new Column("CHANGE_AMOUNT", DbType.Decimal, ColumnProperty.NotNull, 0),
                new Column("CHANGE_PRICE", DbType.Decimal, ColumnProperty.NotNull, 0),
                new Column("CHANGE_TAX", DbType.Decimal, ColumnProperty.NotNull, 0));

            Database.AddIndex("IND_STRPRODHIST_DATE", false, "STOCKROOM_PRODUCT_HISTORY", "HISTORY_DATE");
        }

        public override void Revert()
        {
            Database.RemoveEntityTable("STOCKROOM_PRODUCT_HISTORY");
        }
    }
}