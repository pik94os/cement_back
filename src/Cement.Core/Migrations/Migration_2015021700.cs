﻿using System.Data;
using Cement.Core.Migrations.Extensions;
using ECM7.Migrator.Framework;

namespace Cement.Core.Migrations
{
    [Migration(2015021700)]
    public class Migration_2015021700 : Migration
    {
        public override void Apply()
        {
            Database.RemoveColumn("PRODUCT_REQUEST", "CHAIN_ID");
            Database.AddColumn("PRODUCT_REQUEST", new Column("CHAIN_GUID", DbType.String));
        }

        public override void Revert()
        {
            Database.AddColumn("PRODUCT_REQUEST", new Column("CHAIN_ID", DbType.Int64));
            Database.RemoveColumn("PRODUCT_REQUEST", "CHAIN_GUID");
        }
    }
}