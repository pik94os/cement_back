﻿using Cement.Core.Migrations.Extensions;
using ECM7.Migrator.Framework;

namespace Cement.Core.Migrations
{
    [Migration(2015031800)]
    public class Migration_2015031800 : Migration
    {
        public override void Apply()
        {
            Database.AddEntityTable("WAREHOUSE_EMPLOYEE",
                new RefColumn("WAREHOUSE_ID", ColumnProperty.NotNull, "WAREHOUSEEMP_WAREHOUSE", "WAREHOUSE", "ID"),
                new RefColumn("EMPLOYEE_ID", ColumnProperty.NotNull, "WAREHOUSEEMP_EMPLOYEE", "EMPLOYEE", "ID"));

            Database.AddUniqueConstraint("UNQ_WAREHOUSE_EMPLOYEE", "WAREHOUSE_EMPLOYEE", "WAREHOUSE_ID", "EMPLOYEE_ID");
        }

        public override void Revert()
        {
            Database.RemoveEntityTable("WAREHOUSE_EMPLOYEE");
        }
    }
}