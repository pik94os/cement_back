﻿using System.Data;
using ECM7.Migrator.Framework;

namespace Cement.Core.Migrations
{
    [Migration(2015021601)]
    public class Migration_2015021601 : Migration
    {
        public override void Apply()
        {
            Database.AddColumn("PRODUCT_REQUEST", new Column("SIGNED_AT", DbType.DateTime));
        }

        public override void Revert()
        {
            Database.RemoveColumn("PRODUCT_REQUEST", "SIGNED_AT");
        }
    }
}