﻿using System.Data;
using Cement.Core.Migrations.Extensions;
using ECM7.Migrator.Framework;

namespace Cement.Core.Migrations
{
    [Migration(2015012201)]
    public class Migration_2015012201 : Migration
    {
        public override void Apply()
        {
            Database.AddEntityTable("PERSONAL_CLIENT_EVENT",
                new RefColumn("PERSONAL_CLIENT_ID", ColumnProperty.NotNull, "PERS_CLI_EVT_PERS_CLI", "PERSONAL_CLIENT", "ID"),
                new Column("EVENT_DATE", DbType.DateTime),
                new Column("ARCHIVED_AT", DbType.DateTime),
                new Column("THEME", DbType.String),
                new Column("DESCRIPTION", DbType.String.WithSize(2000)),
                new Column("REMINDER_TYPE", DbType.Int16, ColumnProperty.NotNull));

            Database.AddEntityTable("PERS_CLI_EVENT_COMMENT",
                new RefColumn("PERS_CLI_EVENT_ID", ColumnProperty.NotNull, "PCLI_EVT_COM_PCLI_EVT", "PERSONAL_CLIENT_EVENT", "ID"),
                new RefColumn("EMPLOYEE_ID", "PCLI_EVT_COM_EMPLOYEE", "EMPLOYEE", "ID"),
                new Column("CREATED_AT", DbType.DateTime),
                new Column("TEXT", DbType.String));
        }

        public override void Revert()
        {
            Database.RemoveEntityTable("PERS_CLI_EVENT_COMMENT");
            Database.RemoveEntityTable("PERSONAL_CLIENT_EVENT");
        }
    }
}