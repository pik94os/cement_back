﻿using System.Data;
using Cement.Core.Migrations.Extensions;
using ECM7.Migrator.Framework;

namespace Cement.Core.Migrations
{
    [Migration(2015021701)]
    public class Migration_2015021701 : Migration
    {
        public override void Apply()
        {
            Database.AddEntityTable("CONTRACT_PRODUCT",
                new RefColumn("PRODUCT_ID", ColumnProperty.NotNull, "CONTR_PROD_PRODUCT", "PRODUCT", "ID"),
                new RefColumn("CONTRACT_ID", ColumnProperty.NotNull, "CONTR_PROD_CONTRACT", "CONTRACT", "ID"),
                new RefColumn("TAX_ID", ColumnProperty.NotNull, "CONTR_PROD_TAX", "DICT_TAX", "ID"),
                new RefColumn("MEASURE_UNIT_ID", ColumnProperty.NotNull, "CONTR_PROD_MEASUNIT", "DICT_MEASURE_UNIT", "ID"),
                new Column("PRICE", DbType.Decimal, ColumnProperty.NotNull));

//            Database.ExecuteNonQuery(@"INSERT INTO CONTRACT_PRODUCT(ID, PRODUCT_ID, CONTRACT_ID, TAX_ID,MEASURE_UNIT_ID, PRICE)
//SELECT
//    hibernate_sequence.nextval as Id,
//    prp.PRODUCT_ID,
//    cpr.CONTRACT_ID,    
//    prp.TAX_ID,
//    prp.MEASURE_UNIT_ID,
//    prp.PRICE    
//FROM
//    CONTRACT_ANNEX_PRICELIST  cpr
//JOIN PRICE_LIST_PRODUCT prp on prp.PRICELIST_ID = cpr.PRICELIST_ID");
        }

        public override void Revert()
        {
            Database.RemoveEntityTable("CONTRACT_PRODUCT");
        }
    }
}