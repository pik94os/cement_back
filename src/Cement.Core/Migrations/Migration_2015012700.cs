﻿using System.Data;
using Cement.Core.Migrations.Extensions;
using ECM7.Migrator.Framework;

namespace Cement.Core.Migrations
{
    [Migration(2015012700)]
    public class Migration_2015012700 : Migration
    {
        public override void Apply()
        {
            Database.RemoveRefColumn("PRODUCT", "ORGANIZATION_ID", "PRODUCT_ORGANIZATION");
            Database.AddColumn("PRODUCT", new Column("IS_MODERATED", DbType.Boolean, ColumnProperty.NotNull, false));

            Database.AddEntityTable("PRODUCT_PERSONAL", 
                new RefColumn("ORGANIZATION_ID", ColumnProperty.Null, "PRODUCT_PERS_ORG", "ORGANIZATION", "ID"),
                new RefColumn("PRODUCT_ID", ColumnProperty.Null, "PRODUCT_PERS_PRODUCT", "PRODUCT", "ID"),
                new Column("ARCHIVE_DATE", DbType.DateTime, ColumnProperty.Null));
        }

        public override void Revert()
        {
            Database.AddRefColumn("PRODUCT", new RefColumn("ORGANIZATION_ID", ColumnProperty.Null, "PRODUCT_ORGANIZATION", "ORGANIZATION", "ID"));
            Database.RemoveColumn("PRODUCT", "IS_MODERATED");

            Database.RemoveEntityTable("PRODUCT_PERSONAL");
        }
    }
}