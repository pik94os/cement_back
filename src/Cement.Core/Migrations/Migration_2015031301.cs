﻿using System.Data;
using Cement.Core.Enums;
using Cement.Core.Migrations.Extensions;
using ECM7.Migrator.Framework;

namespace Cement.Core.Migrations
{
    [Migration(2015031301)]
    public class Migration_2015031301 : Migration
    {
        public override void Apply()
        {
            Database.AddRefColumn("STOCKROOM_REQUEST", new RefColumn("WAREHOUSE_CONTACT_ID", "STOCKREQ_WRHSE_EMP", "EMPLOYEE", "ID"));
        }

        public override void Revert()
        {
            Database.RemoveRefColumn("STOCKROOM_REQUEST", "WAREHOUSE_CONTACT_ID", "STOCKREQ_WRHSE_EMP");
        }
    }
}