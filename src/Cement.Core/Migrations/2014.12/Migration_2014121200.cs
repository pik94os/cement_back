﻿using System.Data;
using Cement.Core.Migrations.Extensions;
using ECM7.Migrator.Framework;

namespace Cement.Core.Migrations
{
    [Migration(2014121200)]
    public class Migration_2014121200 : Migration
    {
        public override void Apply()
        {
            Database.AddRefColumn("PRICE_LIST", new RefColumn("MSG_DOCUMENT_ID", ColumnProperty.Null, "PRICE_MSG_DOC","INTERNAL_DOCUMENT","ID"));
        }

        public override void Revert()
        {
            Database.RemoveRefColumn("PRICE_LIST", "MSG_DOCUMENT_ID", "PRICE_MSG_DOC");
        }
    }
}