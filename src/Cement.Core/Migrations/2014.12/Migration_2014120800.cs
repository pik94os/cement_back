﻿using System.Data;
using Cement.Core.Migrations.Extensions;
using ECM7.Migrator.Framework;

namespace Cement.Core.Migrations
{
    [Migration(2014120800)]
    public class Migration_2014120800 : Migration
    {
        public override void Apply()
        {
            Database.AddEntityTable("CLIENT_GROUP",
                new RefColumn("ORGANIZATION_ID", "CLIGROUP_ORG", "ORGANIZATION", "ID"),
                new RefColumn("PARENT_ID", "CLIGROUP_CLIGROUP", "CLIENT_GROUP", "ID"),
                new Column("NAME", DbType.String),
                new Column("DESCRIPTION", DbType.String.WithSize(2000)));

            Database.AddEntityTable("CLIENT_STATE",
                new RefColumn("ORGANIZATION_ID", "CLISTATE_ORG", "ORGANIZATION", "ID"),
                new Column("NAME", DbType.String),
                new Column("DESCRIPTION", DbType.String.WithSize(2000)));

            Database.AddRefColumn("PERSONAL_CLIENT", new RefColumn("STATE_ID", "PERSCLI_CLISTATE", "CLIENT_STATE", "ID"));
            Database.AddRefColumn("PERSONAL_CLIENT", new RefColumn("GROUP_ID", "PERSCLI_CLIGROUP", "CLIENT_GROUP", "ID"));
            Database.AddRefColumn("PERSONAL_CLIENT", new RefColumn("SUBGROUP_ID", "PERSCLISUB_CLIGROUP", "CLIENT_GROUP", "ID"));
            Database.AddColumn("PERSONAL_CLIENT", new Column("CLIENT_FROM", DbType.String));
            Database.AddColumn("PERSONAL_CLIENT", new Column("COMMENT_TEXT", DbType.String));
        }

        public override void Revert()
        {
            Database.RemoveColumn("PERSONAL_CLIENT", "CLIENT_FROM");
            Database.RemoveColumn("PERSONAL_CLIENT", "COMMENT_TEXT");
            Database.RemoveRefColumn("PERSONAL_CLIENT", "STATE_ID", "PERSCLI_CLISTATE");
            Database.RemoveRefColumn("PERSONAL_CLIENT", "GROUP_ID", "PERSCLI_CLIGROUP");
            Database.RemoveRefColumn("PERSONAL_CLIENT", "SUBGROUP_ID", "PERSCLISUB_CLIGROUP");

            Database.RemoveEntityTable("CLIENT_STATE");
            Database.RemoveEntityTable("CLIENT_GROUP");
        }
    }
}