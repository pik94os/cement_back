﻿using System.Data;
using ECM7.Migrator.Framework;

namespace Cement.Core.Migrations
{
    [Migration(2014123000)]
    public class Migration_2014123000 : Migration
    {
        public override void Apply()
        {
            Database.AddColumn("PRODUCT_REQUEST", new Column("STATE", DbType.Int16, ColumnProperty.NotNull, 10));
            Database.AddColumn("PRODUCT_REQUEST", new Column("NAME", DbType.String));
        }

        public override void Revert()
        {
            Database.RemoveColumn("PRODUCT_REQUEST", "NAME");
            Database.RemoveColumn("PRODUCT_REQUEST", "STATE");
        }
    }
}