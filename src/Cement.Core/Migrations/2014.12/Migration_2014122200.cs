﻿using System.Data;
using Cement.Core.Migrations.Extensions;
using ECM7.Migrator.Framework;

namespace Cement.Core.Migrations
{
    [Migration(2014122200)]
    public class Migration_2014122200 : Migration
    {
        public override void Apply()
        {
            Database.AddEntityTable("PRODUCT_REQUEST",
                new RefColumn("ORGANIZATION_ID", "PRODUCT_REQUEST_ORG", "ORGANIZATION", "ID"),
                new Column("REQUEST_NUMBER", DbType.String),
                new Column("REQUEST_DATE", DbType.DateTime),
                new Column("IS_NO_REQUEST", DbType.Boolean),
                new RefColumn("REQUEST_ID", "PROD_RQST_RQST", "PRODUCT_REQUEST", "ID"),
                new Column("IS_NO_TEMPLATE", DbType.Boolean),
                new Column("SHIPMENT_TYPE", DbType.Int16),
                new Column("SHIPMENT_DATE", DbType.DateTime),
                new Column("UNSHIPMENT_DATE", DbType.DateTime),
                new Column("IS_NO_PROVIDER", DbType.Boolean),
                new RefColumn("PROVIDER_CONTRACT_ID", "RQST_PRVDR_CONTRACT", "CONTRACT", "ID"),
                new RefColumn("PROVIDER_PERSON_ID", "RQST_PRVDR_PERSON", "EMPLOYEE", "ID"),
                new RefColumn("PRODUCT_ID", "RQST_PRODUCT", "PRODUCT", "ID"),
                new Column("PRODUCT_COUNT", DbType.Int32),
                new Column("VEHICLE_TYPE", DbType.Int16),
                new RefColumn("VEH_BODY_TYPE_ID", "RQST_VEH_BODY", "DICT_VEHICLE_BODY_TYPE", "ID"),
                new Column("VEH_IS_REAR_LOADING", DbType.Boolean),
                new Column("VEH_IS_SIDE_LOADING", DbType.Boolean),
                new Column("VEH_IS_OVER_LOADING", DbType.Boolean),
                new RefColumn("VEHICLE_OPTION_ID", "RQST_VEH_OPTION", "DICT_VEHICLE_ADD_OPTIONS", "ID"),
                new Column("VEH_CHARACTERISTIC", DbType.String),
                new RefColumn("CONSIGNEE_ID", "REQUEST_CONSIGNEE", "ORGANIZATION", "ID"),
                new RefColumn("CONSIGNEE_PERSON_ID", "RQST_CONSIGNEE_PERSON", "EMPLOYEE", "ID"),
                new Column("IS_PAYER_CONSIGNEE", DbType.Boolean),
                new RefColumn("PAYER_ID", "REQUEST_PAYER", "ORGANIZATION", "ID"),
                new RefColumn("PAYER_PERSON_ID", "RQST_PAYER_PERSON", "EMPLOYEE", "ID"),
                new Column("CONTENT", DbType.Binary),
                new Column("IMPORTANCE", DbType.Int16),
                new Column("SECRECY", DbType.Int16));
        }

        public override void Revert()
        {
            Database.RemoveEntityTable("PRODUCT_REQUEST");
        }
    }
}