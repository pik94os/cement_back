﻿using System.Data;
using Cement.Core.Migrations.Extensions;
using ECM7.Migrator.Framework;

namespace Cement.Core.Migrations
{
    [Migration(2014120300)]
    public class Migration_2014120300 : Migration
    {
        public override void Apply()
        {
            Database.AddColumn("CORRESP_ROUTE", new Column("COMMENT_BYTES", DbType.Binary));
            Database.AddColumn("CORRESP_ROUTE", new Column("INITIAL_PROPERTY", DbType.Int32, ColumnProperty.NotNull, 0));
            Database.AddColumn("CORRESP_ROUTE", new Column("PROCESSING_RESULT", DbType.Int32, ColumnProperty.NotNull, 0));
            Database.AddRefColumn("CORRESP_MOVEMENT", new RefColumn("ROUTE_ID", "CORRESPMVT_CORROUTE", "CORRESP_ROUTE", "ID"));

            Database.AddColumn("INTERNAL_DOC_ROUTE", new Column("COMMENT_BYTES", DbType.Binary));
            Database.AddColumn("INTERNAL_DOC_ROUTE", new Column("INITIAL_PROPERTY", DbType.Int32, ColumnProperty.NotNull, 0));
            Database.AddColumn("INTERNAL_DOC_ROUTE", new Column("PROCESSING_RESULT", DbType.Int32, ColumnProperty.NotNull, 0));
            Database.AddRefColumn("INTERNAL_DOC_MOVEMENT", new RefColumn("ROUTE_ID", "INTDOCMVT_INTDOCROUTE", "INTERNAL_DOC_ROUTE", "ID"));
        }

        public override void Revert()
        {
            Database.RemoveColumn("CORRESP_ROUTE", "COMMENT_BYTES");
            Database.RemoveColumn("CORRESP_ROUTE", "INITIAL_PROPERTY");
            Database.RemoveColumn("CORRESP_ROUTE", "PROCESSING_RESULT");
            Database.RemoveRefColumn("CORRESP_MOVEMENT", "ROUTE_ID", "CORRESPMVT_CORROUTE");

            Database.RemoveColumn("INTERNAL_DOC_ROUTE", "COMMENT_BYTES");
            Database.RemoveColumn("INTERNAL_DOC_ROUTE", "INITIAL_PROPERTY");
            Database.RemoveColumn("INTERNAL_DOC_ROUTE", "PROCESSING_RESULT");
            Database.RemoveRefColumn("INTERNAL_DOC_MOVEMENT", "ROUTE_ID", "INTDOCMVT_INTDOCROUTE");
        }
    }
}