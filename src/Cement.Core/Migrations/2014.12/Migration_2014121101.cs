﻿using System.Data;
using ECM7.Migrator.Framework;

namespace Cement.Core.Migrations
{
    [Migration(2014121101)]
    public class Migration_2014121101 : Migration
    {
        public override void Apply()
        {
            Database.AddColumn("INTERNAL_DOCUMENT", new Column("SPECIFICITY", DbType.Int16, ColumnProperty.Null, 0));
        }

        public override void Revert()
        {
            Database.RemoveColumn("INTERNAL_DOCUMENT", "SPECIFICITY");
        }
    }
}