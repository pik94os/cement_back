﻿using System.Data;
using Cement.Core.Migrations.Extensions;
using ECM7.Migrator.Framework;

namespace Cement.Core.Migrations
{
    [Migration(2014120500)]
    public class Migration_2014120500 : Migration
    {
        public override void Apply()
        {
            Database.AddRefColumn("SIGNATURE", new RefColumn("REASON_DOC_ID", "SIGREASON_INTDOC", "INTERNAL_DOCUMENT", "ID"));
        }

        public override void Revert()
        {
            Database.RemoveRefColumn("SIGNATURE", "REASON_DOC_ID", "SIGREASON_INTDOC");
        }
    }
}