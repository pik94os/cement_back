﻿using System.Data;
using ECM7.Migrator.Framework;

namespace Cement.Core.Migrations
{
    [Migration(2014120400)]
    public class Migration_2014120400 : Migration
    {
        public override void Apply()
        {
            Database.ChangeColumn("CORRESP_ROUTE", "PROCESSING_RESULT", DbType.Int32, false);
            Database.ChangeColumn("INTERNAL_DOC_ROUTE", "PROCESSING_RESULT", DbType.Int32, false);
        }

        public override void Revert()
        {
            
        }
    }
}