﻿using System.Data;
using Cement.Core.Migrations.Extensions;
using ECM7.Migrator.Framework;

namespace Cement.Core.Migrations
{
    [Migration(2014120502)]
    public class Migration_2014120502 : Migration
    {
        public override void Apply()
        {
            Database.RemoveRefColumn("ORGANIZATION", "OWNERSHIP_ID", "ORGANIZATION_OWNERSHIP");
            Database.RemoveColumn("ORGANIZATION", "FULL_NAME");
            Database.RemoveColumn("ORGANIZATION", "JUR_ADDRESS_POSTAL_CODE");
            Database.RemoveColumn("ORGANIZATION", "JUR_ADDRESS_COUNTRY");
            Database.RemoveColumn("ORGANIZATION", "JUR_ADDRESS_REGION");
            Database.RemoveColumn("ORGANIZATION", "JUR_ADDRESS_AREA");
            Database.RemoveColumn("ORGANIZATION", "JUR_ADDRESS_LOCALITY");
            Database.RemoveColumn("ORGANIZATION", "JUR_ADDRESS_STREET");
            Database.RemoveColumn("ORGANIZATION", "JUR_ADDRESS_HOUSE");
            Database.RemoveColumn("ORGANIZATION", "JUR_ADDRESS_HOUSING");
            Database.RemoveColumn("ORGANIZATION", "JUR_ADDRESS_BUILDING");
            Database.RemoveColumn("ORGANIZATION", "JUR_ADDRESS_OFFICE");

            Database.RemoveColumn("ORGANIZATION", "PHONE");
            Database.RemoveColumn("ORGANIZATION", "FAX");
            Database.RemoveColumn("ORGANIZATION", "EMAIL");
            Database.RemoveColumn("ORGANIZATION", "WEB");


            Database.AddRefColumn("CLIENT", new RefColumn("OWNERSHIP_ID", "CLIENT_OWNERSHIP", "DICT_OWNERSHIP", "ID"));
            Database.AddColumn("CLIENT", new Column("FULL_NAME", DbType.String.WithSize(2000)));
            Database.AddColumn("CLIENT", new Column("OGRN", DbType.String));
            Database.AddColumn("CLIENT", new Column("OKVED", DbType.String));
            Database.AddColumn("CLIENT", new Column("OKATO", DbType.String));
            Database.AddColumn("CLIENT", new Column("OKONH", DbType.String));
            Database.AddColumn("CLIENT", new Column("BRAND", DbType.String));

            Database.AddColumn("CLIENT", new Column("JUR_ADDRESS_POSTAL_CODE", DbType.String));
            Database.AddColumn("CLIENT", new Column("JUR_ADDRESS_COUNTRY", DbType.String));
            Database.AddColumn("CLIENT", new Column("JUR_ADDRESS_REGION", DbType.String));
            Database.AddColumn("CLIENT", new Column("JUR_ADDRESS_AREA", DbType.String));
            Database.AddColumn("CLIENT", new Column("JUR_ADDRESS_LOCALITY", DbType.String));
            Database.AddColumn("CLIENT", new Column("JUR_ADDRESS_STREET", DbType.String));
            Database.AddColumn("CLIENT", new Column("JUR_ADDRESS_HOUSE", DbType.String));
            Database.AddColumn("CLIENT", new Column("JUR_ADDRESS_HOUSING", DbType.String));
            Database.AddColumn("CLIENT", new Column("JUR_ADDRESS_BUILDING", DbType.String));
            Database.AddColumn("CLIENT", new Column("JUR_ADDRESS_OFFICE", DbType.String));

            Database.AddColumn("CLIENT", new Column("FACT_ADDRESS", DbType.String));
            Database.AddColumn("CLIENT", new Column("FACT_ADDRESS_POSTAL_CODE", DbType.String));
            Database.AddColumn("CLIENT", new Column("FACT_ADDRESS_COUNTRY", DbType.String));
            Database.AddColumn("CLIENT", new Column("FACT_ADDRESS_REGION", DbType.String));
            Database.AddColumn("CLIENT", new Column("FACT_ADDRESS_AREA", DbType.String));
            Database.AddColumn("CLIENT", new Column("FACT_ADDRESS_LOCALITY", DbType.String));
            Database.AddColumn("CLIENT", new Column("FACT_ADDRESS_STREET", DbType.String));
            Database.AddColumn("CLIENT", new Column("FACT_ADDRESS_HOUSE", DbType.String));
            Database.AddColumn("CLIENT", new Column("FACT_ADDRESS_HOUSING", DbType.String));
            Database.AddColumn("CLIENT", new Column("FACT_ADDRESS_BUILDING", DbType.String));
            Database.AddColumn("CLIENT", new Column("FACT_ADDRESS_OFFICE", DbType.String));

            Database.AddColumn("CLIENT", new Column("PHONE", DbType.String));
            Database.AddColumn("CLIENT", new Column("FAX", DbType.String));
            Database.AddColumn("CLIENT", new Column("EMAIL", DbType.String));
            Database.AddColumn("CLIENT", new Column("WEB", DbType.String));
        }

        public override void Revert()
        {
            Database.AddRefColumn("ORGANIZATION", new RefColumn("OWNERSHIP_ID", "ORGANIZATION_OWNERSHIP", "DICT_OWNERSHIP", "ID"));
            Database.AddColumn("ORGANIZATION", new Column("FULL_NAME", DbType.String.WithSize(2000)));

            Database.AddColumn("ORGANIZATION", new Column("JUR_ADDRESS_POSTAL_CODE", DbType.String));
            Database.AddColumn("ORGANIZATION", new Column("JUR_ADDRESS_COUNTRY", DbType.String));
            Database.AddColumn("ORGANIZATION", new Column("JUR_ADDRESS_REGION", DbType.String));
            Database.AddColumn("ORGANIZATION", new Column("JUR_ADDRESS_AREA", DbType.String));
            Database.AddColumn("ORGANIZATION", new Column("JUR_ADDRESS_LOCALITY", DbType.String));
            Database.AddColumn("ORGANIZATION", new Column("JUR_ADDRESS_STREET", DbType.String));
            Database.AddColumn("ORGANIZATION", new Column("JUR_ADDRESS_HOUSE", DbType.String));
            Database.AddColumn("ORGANIZATION", new Column("JUR_ADDRESS_HOUSING", DbType.String));
            Database.AddColumn("ORGANIZATION", new Column("JUR_ADDRESS_BUILDING", DbType.String));
            Database.AddColumn("ORGANIZATION", new Column("JUR_ADDRESS_OFFICE", DbType.String));

            Database.AddColumn("ORGANIZATION", new Column("PHONE", DbType.String));
            Database.AddColumn("ORGANIZATION", new Column("FAX", DbType.String));
            Database.AddColumn("ORGANIZATION", new Column("EMAIL", DbType.String));
            Database.AddColumn("ORGANIZATION", new Column("WEB", DbType.String));

            Database.RemoveRefColumn("CLIENT", "OWNERSHIP_ID", "CLIENT_OWNERSHIP");
            Database.RemoveColumn("CLIENT", "FULL_NAME");
            Database.RemoveColumn("CLIENT", "OGRN");
            Database.RemoveColumn("CLIENT", "OKVED");
            Database.RemoveColumn("CLIENT", "OKATO");
            Database.RemoveColumn("CLIENT", "OKONH");
            Database.RemoveColumn("CLIENT", "BRAND");

            Database.RemoveColumn("CLIENT", "JUR_ADDRESS_POSTAL_CODE");
            Database.RemoveColumn("CLIENT", "JUR_ADDRESS_COUNTRY");
            Database.RemoveColumn("CLIENT", "JUR_ADDRESS_REGION");
            Database.RemoveColumn("CLIENT", "JUR_ADDRESS_AREA");
            Database.RemoveColumn("CLIENT", "JUR_ADDRESS_LOCALITY");
            Database.RemoveColumn("CLIENT", "JUR_ADDRESS_STREET");
            Database.RemoveColumn("CLIENT", "JUR_ADDRESS_HOUSE");
            Database.RemoveColumn("CLIENT", "JUR_ADDRESS_HOUSING");
            Database.RemoveColumn("CLIENT", "JUR_ADDRESS_BUILDING");
            Database.RemoveColumn("CLIENT", "JUR_ADDRESS_OFFICE");

            Database.RemoveColumn("CLIENT", "FACT_ADDRESS");
            Database.RemoveColumn("CLIENT", "FACT_ADDRESS_POSTAL_CODE");
            Database.RemoveColumn("CLIENT", "FACT_ADDRESS_COUNTRY");
            Database.RemoveColumn("CLIENT", "FACT_ADDRESS_REGION");
            Database.RemoveColumn("CLIENT", "FACT_ADDRESS_AREA");
            Database.RemoveColumn("CLIENT", "FACT_ADDRESS_LOCALITY");
            Database.RemoveColumn("CLIENT", "FACT_ADDRESS_STREET");
            Database.RemoveColumn("CLIENT", "FACT_ADDRESS_HOUSE");
            Database.RemoveColumn("CLIENT", "FACT_ADDRESS_HOUSING");
            Database.RemoveColumn("CLIENT", "FACT_ADDRESS_BUILDING");
            Database.RemoveColumn("CLIENT", "FACT_ADDRESS_OFFICE");
            
            Database.RemoveColumn("CLIENT", "PHONE");
            Database.RemoveColumn("CLIENT", "FAX");
            Database.RemoveColumn("CLIENT", "EMAIL");
            Database.RemoveColumn("CLIENT", "WEB");
        }
    }
}