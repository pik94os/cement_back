﻿using System.Data;
using Cement.Core.Migrations.Extensions;
using ECM7.Migrator.Framework;

namespace Cement.Core.Migrations
{
    [Migration(2014122900)]
    public class Migration_2014122900 : Migration
    {
        public override void Apply()
        {
            Database.AddEntityTable("REQUEST_ROUTE",
                new RefColumn("DOCUMENT_ID", ColumnProperty.NotNull, "REQUEST_INTDOC", "PRODUCT_REQUEST", "ID"),
                new RefColumn("EMPLOYEE_ID", ColumnProperty.NotNull, "RQSTROUTE_EMPLOYEE", "EMPLOYEE", "ID"),
                new Column("DATE_IN", DbType.DateTime),
                new Column("DATE_OUT", DbType.DateTime));

            Database.AddEntityTable("REQUEST_MOVEMENT",
                new RefColumn("DOCUMENT_ID", ColumnProperty.NotNull, "RQSTMVT_RQST", "PRODUCT_REQUEST", "ID"),
                new RefColumn("EMPLOYEE_ID", ColumnProperty.NotNull, "RQSTMVT_EMPLOYEE", "EMPLOYEE", "ID"),
                new RefColumn("MEETING_EMP_ID", "RQSTMVT_EMP2", "EMPLOYEE", "ID"),
                new Column("TYPE", DbType.Int16, ColumnProperty.NotNull),
                new Column("IS_ARCHIVED", DbType.Boolean, ColumnProperty.NotNull),
                new RefColumn("ROUTE_ID", "RQSTMVT_RQSTROUTE", "REQUEST_ROUTE", "ID"));
        }

        public override void Revert()
        {
            Database.RemoveEntityTable("REQUEST_MOVEMENT");
            Database.RemoveEntityTable("REQUEST_ROUTE");
        }
    }
}