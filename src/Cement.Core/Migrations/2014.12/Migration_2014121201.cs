﻿using System.Data;
using Cement.Core.Migrations.Extensions;
using ECM7.Migrator.Framework;

namespace Cement.Core.Migrations
{
    [Migration(2014121201)]
    public class Migration_2014121201 : Migration
    {
        public override void Apply()
        {
            Database.AddEntityTable("CATALOGUE",
                new RefColumn("ORGANIZATION_ID", "CATALOG_ORG", "ORGANIZATION", "ID"),
                new RefColumn("PRICE_LIST_ID", "CATALOG_PRICE", "PRICE_LIST", "ID"),
                new Column("DESCRIPTION", DbType.Binary));
        }

        public override void Revert()
        {
            Database.RemoveEntityTable("CATALOGUE");
        }
    }
}