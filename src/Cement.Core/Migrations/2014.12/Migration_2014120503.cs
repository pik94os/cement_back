﻿using System.Data;
using Cement.Core.Migrations.Extensions;
using ECM7.Migrator.Framework;

namespace Cement.Core.Migrations
{
    [Migration(2014120503)]
    public class Migration_2014120503 : Migration
    {
        public override void Apply()
        {
            Database.AddEntityTable("PERSONAL_CLIENT",
                new RefColumn("ORGANIZATION_ID", "PERSCLIENT_ORG", "ORGANIZATION", "ID"),
                new RefColumn("CLIENT_ID", "PERSCLIENT_CLIENT", "CLIENT", "ID"),
                new RefColumn("EMPLOYEE_ID", "PERSCLIENT_EMPLOYEE", "EMPLOYEE", "ID"),
                new Column("IS_CORPORATE", DbType.Boolean),
                new Column("DATE_FIRST_CONTRACT", DbType.DateTime),
                new Column("DATE_LAST_CONTRACT", DbType.DateTime),
                new Column("ARCHIVED_AT", DbType.DateTime));
        }

        public override void Revert()
        {
            Database.RemoveEntityTable("PERSONAL_CLIENT");
        }
    }
}