﻿using System.Data;
using Cement.Core.Migrations.Extensions;
using ECM7.Migrator.Framework;

namespace Cement.Core.Migrations
{
    [Migration(2014121100)]
    public class Migration_2014121100 : Migration
    {
        public override void Apply()
        {
            Database.AddEntityTable("ADVERT",
                new RefColumn("ORGANIZATION_ID", "ADVERT_ORG", "ORGANIZATION", "ID"),
                new Column("NAME", DbType.String),
                new Column("DATE_SHOW", DbType.DateTime),
                new Column("SUBJECT", DbType.String),
                new Column("CONTENT", DbType.Binary),
                new RefColumn("CREATOR_ID", "ADVERT_CREATOR_EMPLOYEE", "EMPLOYEE", "ID"),
                new RefColumn("AUTHOR_ID", "ADVERT_AUTHOR_EMPLOYEE", "EMPLOYEE", "ID"),
                new Column("IMPORTANCE", DbType.Int16),
                new Column("SECRECY", DbType.Int16),
                new Column("STATE", DbType.Int16),
                new RefColumn("FILE_ID", "ADVERT_ENTFILE", "ENTITY_FILE", "ID"));

            Database.AddEntityTable("ADVERT_RECIPIENT",
                new RefColumn("ADVERT_ID", ColumnProperty.NotNull, "ADVERTRECIPIENT_ADVERT", "ADVERT", "ID"),
                new RefColumn("RECIPIENT_ID", ColumnProperty.NotNull, "ADVERTRECIPIENT_EMPLOYEE", "EMPLOYEE", "ID"));

            Database.AddEntityTable("ADVERT_ROUTE",
                new RefColumn("DOCUMENT_ID", ColumnProperty.NotNull, "ADVERTROUTE_ADVERT", "ADVERT", "ID"),
                new RefColumn("EMPLOYEE_ID", ColumnProperty.NotNull, "ADVERTROUTE_EMPLOYEE", "EMPLOYEE", "ID"),
                new Column("DATE_IN", DbType.DateTime),
                new Column("DATE_OUT", DbType.DateTime),
                new Column("COMMENT_BYTES", DbType.Binary),
                new Column("PROCESSING_RESULT", DbType.Int32));

            Database.AddEntityTable("ADVERT_MOVEMENT",
                new RefColumn("DOCUMENT_ID", ColumnProperty.NotNull, "ADVERTMVT_ADVERT", "ADVERT", "ID"),
                new RefColumn("EMPLOYEE_ID", ColumnProperty.NotNull, "ADVERTMVT_EMPLOYEE", "EMPLOYEE", "ID"),
                new Column("TYPE", DbType.Int16, ColumnProperty.NotNull),
                new Column("PROPERTIES", DbType.Int16),
                new Column("IS_ARCHIVED", DbType.Boolean, ColumnProperty.Null, false),
                new RefColumn("ROUTE_ID", "ADVMVT_ADVROUTE", "ADVERT_ROUTE", "ID"));
        }

        public override void Revert()
        {
            Database.RemoveEntityTable("ADVERT_MOVEMENT");
            Database.RemoveEntityTable("ADVERT_ROUTE");
            Database.RemoveEntityTable("ADVERT_RECIPIENT");
            Database.RemoveEntityTable("ADVERT");
        }
    }
}