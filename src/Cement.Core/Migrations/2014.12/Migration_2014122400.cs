﻿using System.Data;
using Cement.Core.Migrations.Extensions;
using ECM7.Migrator.Framework;

namespace Cement.Core.Migrations
{
    [Migration(2014122400)]
    public class Migration_2014122400 : Migration
    {
        public override void Apply()
        {
            Database.RemoveRefColumn("PRODUCT_REQUEST", "PRODUCT_ID", "RQST_PRODUCT");
            Database.AddRefColumn("PRODUCT_REQUEST", new RefColumn("PLIST_PRODUCT_ID", "RQST_PLIST_PRODUCT", "PRICE_LIST_PRODUCT", "ID"));
            Database.AddColumn("PRODUCT_REQUEST", new Column("TYPE", DbType.Int16));
        }

        public override void Revert()
        {
            Database.RemoveColumn("PRODUCT_REQUEST", "TYPE");
            Database.RemoveRefColumn("PRODUCT_REQUEST", "PLIST_PRODUCT_ID", "RQST_PLIST_PRODUCT");
            Database.AddRefColumn("PRODUCT_REQUEST", new RefColumn("PRODUCT_ID", "RQST_PRODUCT", "PRODUCT", "ID"));
        }
    }
}