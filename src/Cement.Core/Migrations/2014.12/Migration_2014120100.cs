﻿using System.Data;
using Cement.Core.Migrations.Extensions;
using ECM7.Migrator.Framework;

namespace Cement.Core.Migrations
{
    [Migration(2014120100)]
    public class Migration_2014120100 : Migration
    {
        public override void Apply()
        {
            Database.AddEntityTable("CORRESPONDENCE",
                new RefColumn("ORGANIZATION_ID", "CORRESP_ORG", "ORGANIZATION", "ID"),
                new Column("NAME", DbType.String),
                new Column("NO", DbType.String),
                new Column("DOCUMENT_DATE", DbType.DateTime),
                new Column("TYPE", DbType.Int16),
                new RefColumn("NOMENCLATURE_ID", "CORRESP_NOMENCLATURE", "NOMENCLATURE", "ID"),
                new Column("SUBJECT", DbType.String),
                new Column("CONTENT", DbType.Binary),
                new RefColumn("ADDRESSEE_ID", "CORRESP_ADRSEE_EMPLOYEE", "EMPLOYEE", "ID"),
                new RefColumn("AUTHOR_ID", "CORRESP_AUTHOR_EMPLOYEE", "EMPLOYEE", "ID"),
                new RefColumn("SIGNER_ID", "CORRESP_SIGNER_EMPLOYEE", "EMPLOYEE", "ID"),
                new Column("REVIEW_BEFORE", DbType.DateTime),
                new Column("IMPORTANCE", DbType.Int16),
                new Column("SECRECY", DbType.Int16),
                new RefColumn("CREATOR_ID", "CORRESP_CREATOR_EMPLOYEE", "EMPLOYEE", "ID"),
                new Column("STATE", DbType.Int16, ColumnProperty.NotNull, 10),
                new RefColumn("FILE_ID", "CORRESP_ENTFILE", "ENTITY_FILE", "ID"));

            Database.AddEntityTable("CORRESP_MOVEMENT",
                new RefColumn("DOCUMENT_ID", ColumnProperty.NotNull, "CORRESPMVT_CORRESP", "CORRESPONDENCE", "ID"),
                new RefColumn("EMPLOYEE_ID", ColumnProperty.NotNull, "CORRESPMVT_EMPLOYEE", "EMPLOYEE", "ID"),
                new Column("TYPE", DbType.Int16, ColumnProperty.NotNull),
                new Column("PROPERTIES", DbType.Int16),
                new Column("IS_ARCHIVED", DbType.Boolean, ColumnProperty.NotNull));

            Database.AddEntityTable("CORRESP_ROUTE",
                new RefColumn("DOCUMENT_ID", ColumnProperty.NotNull, "CORRESPROUTE_INTDOC", "CORRESPONDENCE", "ID"),
                new RefColumn("EMPLOYEE_ID", ColumnProperty.NotNull, "CORRESPROUTE_EMPLOYEE", "EMPLOYEE", "ID"),
                new Column("DATE_IN", DbType.DateTime),
                new Column("DATE_OUT", DbType.DateTime));
        }

        public override void Revert()
        {
            Database.RemoveEntityTable("CORRESP_ROUTE");
            Database.RemoveEntityTable("CORRESP_MOVEMENT");
            Database.RemoveEntityTable("CORRESPONDENCE");
        }
    }
}