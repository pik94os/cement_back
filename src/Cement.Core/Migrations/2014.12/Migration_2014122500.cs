﻿using System.Data;
using Cement.Core.Enums;
using ECM7.Migrator.Framework;

namespace Cement.Core.Migrations
{
    [Migration(2014122500)]
    public class Migration_2014122500 : Migration
    {
        public override void Apply()
        {
            Database.AddColumn("DICT_PRODUCT_GROUP", new Column("TYPE", DbType.Int16, ColumnProperty.NotNull, (int)ProductType.Product));
        }

        public override void Revert()
        {
            Database.RemoveColumn("DICT_PRODUCT_GROUP", "TYPE");
        }
    }
}