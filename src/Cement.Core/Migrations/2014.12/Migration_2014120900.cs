﻿using System.Data;
using Cement.Core.Migrations.Extensions;
using ECM7.Migrator.Framework;

namespace Cement.Core.Migrations
{
    [Migration(2014120900)]
    public class Migration_2014120900 : Migration
    {
        public override void Apply()
        {
            Database.AddEntityTable("MESSAGE",
                new RefColumn("ORGANIZATION_ID", "MESSAGE_ORG", "ORGANIZATION", "ID"),
                new Column("NAME", DbType.String),
                new Column("MESSAGE_DATE", DbType.DateTime),
                new Column("TYPE", DbType.Int16),
                new Column("SUBJECT", DbType.String),
                new Column("CONTENT", DbType.Binary),
                new RefColumn("CREATOR_ID", "MESSAGE_CREATOR_EMPLOYEE", "EMPLOYEE", "ID"),
                new RefColumn("AUTHOR_ID", "MESSAGE_AUTHOR_EMPLOYEE", "EMPLOYEE", "ID"),
                new Column("IMPORTANCE", DbType.Int16),
                new Column("SECRECY", DbType.Int16),
                new Column("STATE", DbType.Int16),
                new RefColumn("FILE_ID", "MESSAGE_ENTFILE", "ENTITY_FILE", "ID"),
                new Column("REPEAT_DISPLAY", DbType.String),
                new Column("REPEAT_START_AT", DbType.DateTime),
                new Column("REPEAT_STOP_AT", DbType.DateTime),
                new Column("REPEAT_COUNT", DbType.Int32),
                new Column("REPEAT_DETAILS", DbType.String.WithSize(2000)));

            Database.AddEntityTable("MESSAGE_RECIPIENT",
                new RefColumn("MESSAGE_ID", ColumnProperty.NotNull, "MSGRECIPIENT_MESSAGE", "MESSAGE", "ID"),
                new RefColumn("RECIPIENT_ID", ColumnProperty.NotNull, "MSGRECIPIENT_EMPLOYEE", "EMPLOYEE", "ID"));

            Database.AddEntityTable("MESSAGE_ROUTE",
                new RefColumn("DOCUMENT_ID", ColumnProperty.NotNull, "MSGROUTE_MESSAGE", "MESSAGE", "ID"),
                new RefColumn("EMPLOYEE_ID", ColumnProperty.NotNull, "MSGROUTE_EMPLOYEE", "EMPLOYEE", "ID"),
                new Column("DATE_IN", DbType.DateTime),
                new Column("DATE_OUT", DbType.DateTime),
                new Column("COMMENT_BYTES", DbType.Binary),
                new Column("PROCESSING_RESULT", DbType.Int32));

            Database.AddEntityTable("MESSAGE_MOVEMENT",
                new RefColumn("DOCUMENT_ID", ColumnProperty.NotNull, "MSGMVT_MESSAGE", "MESSAGE", "ID"),
                new RefColumn("EMPLOYEE_ID", ColumnProperty.NotNull, "MSGMVT_EMPLOYEE", "EMPLOYEE", "ID"),
                new Column("TYPE", DbType.Int16, ColumnProperty.NotNull),
                new Column("PROPERTIES", DbType.Int16),
                new Column("IS_ARCHIVED", DbType.Boolean, ColumnProperty.Null, false),
                new RefColumn("ROUTE_ID", "MSGMVT_MSGROUTE", "MESSAGE_ROUTE", "ID"));
        }

        public override void Revert()
        {
            Database.RemoveEntityTable("MESSAGE_MOVEMENT");
            Database.RemoveEntityTable("MESSAGE_ROUTE");
            Database.RemoveEntityTable("MESSAGE_RECIPIENT");
            Database.RemoveEntityTable("MESSAGE");
        }
    }
}