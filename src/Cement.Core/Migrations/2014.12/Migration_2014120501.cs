﻿using System.Data;
using ECM7.Migrator.Framework;

namespace Cement.Core.Migrations
{
    [Migration(2014120501)]
    public class Migration_2014120501 : Migration
    {
        public override void Apply()
        {
            Database.ChangeColumn("CORRESPONDENCE", "STATE", DbType.Int16, false);
            Database.ChangeColumn("INTERNAL_DOCUMENT", "STATE", DbType.Int16, false);
        }

        public override void Revert()
        {
            
        }
    }
}