﻿using System.Data;
using ECM7.Migrator.Framework;

namespace Cement.Core.Migrations
{
    [Migration(2014123100)]
    public class Migration_2014123100 : Migration
    {
        public override void Apply()
        {
            Database.AddColumn("REQUEST_ROUTE", new Column("COMMENT_BYTES", DbType.Binary));
            Database.AddColumn("REQUEST_ROUTE", new Column("INITIAL_PROPERTY", DbType.Int32, ColumnProperty.NotNull, 0));
            Database.AddColumn("REQUEST_ROUTE", new Column("PROCESSING_RESULT", DbType.Int32, ColumnProperty.NotNull, 0));
        }

        public override void Revert()
        {
            Database.RemoveColumn("REQUEST_ROUTE", "COMMENT_BYTES");
            Database.RemoveColumn("REQUEST_ROUTE", "INITIAL_PROPERTY");
            Database.RemoveColumn("REQUEST_ROUTE", "PROCESSING_RESULT");
        }
    }
}