﻿using Cement.Core.Migrations.Extensions;
using ECM7.Migrator.Framework;

namespace Cement.Core.Migrations
{
    [Migration(2014122300)]
    public class Migration_2014122300 : Migration
    {
        public override void Apply()
        {
            Database.AddRefColumn("INTERNAL_DOC_MOVEMENT", new RefColumn("MEETING_EMP_ID", "INTDOCMVT_EMP2", "EMPLOYEE", "ID"));
            Database.AddRefColumn("CORRESP_MOVEMENT", new RefColumn("MEETING_EMP_ID", "CORRESPMVT_EMP2", "EMPLOYEE", "ID"));
        }

        public override void Revert()
        {
            Database.RemoveRefColumn("INTERNAL_DOC_MOVEMENT", "MEETING_EMP_ID", "INTDOCMVT_EMP2");
            Database.RemoveRefColumn("CORRESP_MOVEMENT", "MEETING_EMP_ID", "CORRESPMVT_EMP2");
        }
    }
}