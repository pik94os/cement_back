﻿using System.Data;
using ECM7.Migrator.Framework;

namespace Cement.Core.Migrations
{
    [Migration(2014121000)]
    public class Migration_2014121000 : Migration
    {
        public override void Apply()
        {
            Database.AddColumn("PRICE_LIST", new Column("STATUS", DbType.Int16, ColumnProperty.Null, 10));
        }

        public override void Revert()
        {
            Database.RemoveColumn("PRICE_LIST", "STATUS");
        }
    }
}