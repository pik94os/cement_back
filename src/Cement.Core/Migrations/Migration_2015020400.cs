﻿using System.Data;
using Cement.Core.Migrations.Extensions;
using ECM7.Migrator.Framework;

namespace Cement.Core.Migrations
{
    [Migration(2015020400)]
    public class Migration_2015020400 : Migration
    {
        public override void Apply()
        {
            Database.AddEntityTable("USER_PERMISSION",
                new RefColumn("USER_ID", ColumnProperty.NotNull, "USER_PERM_USER", "USERS", "ID"),
                new Column("ACCESS_TYPE", DbType.Int16, ColumnProperty.NotNull),
                new Column("PERMISSION", DbType.String, ColumnProperty.NotNull),
                new Column("DESCRIPTION", DbType.String));

            Database.AddUniqueConstraint("UNQ_USER_PERMISSION", "USER_PERMISSION", "USER_ID", "PERMISSION");
        }

        public override void Revert()
        {
            Database.RemoveConstraint("USER_PERMISSION", "UNQ_USER_PERMISSION");
            Database.RemoveEntityTable("USER_PERMISSION");
        }
    }
}