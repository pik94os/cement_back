﻿using System.Data;
using ECM7.Migrator.Framework;

namespace Cement.Core.Migrations.Extensions
{
    /// <summary>
    /// Колонка, содержащая индекс и внешний ключ
    /// </summary>
    public class RefColumn : Column
    {
        public string PrimaryTable { get; set; }

        public string PrimaryColumn { get; set; }

        public string IndexAndForeignKeyName { get; set; }

        /// <summary>
        /// Колонка, содержащая индекс и внешний ключ
        /// </summary>
        /// <param name="columnName">Имя колонки</param>
        /// <param name="property">Свойство колонки</param>
        /// <param name="indexAndForeignKeyName">Имя индекса и внешнего ключа</param>
        /// <param name="primaryTable">Таблица, содержащая первичный ключ</param>
        /// <param name="primaryColumn">Колонка, содержащая первичный ключ</param>
        public RefColumn(string columnName, ColumnProperty property, string indexAndForeignKeyName, string primaryTable, string primaryColumn)
            : base(columnName, DbType.Int64, property)
        {
            IndexAndForeignKeyName = indexAndForeignKeyName;
            PrimaryTable = primaryTable;
            PrimaryColumn = primaryColumn;
        }

        /// <summary>
        /// Колонка, содержащая индекс и внешний ключ
        /// </summary>
        /// <param name="columnName">Имя колонки</param>
        /// <param name="indexAndForeignKeyName">Имя индекса и внешнего ключа</param>
        /// <param name="primaryTable">Таблица, содержащая первичный ключ</param>
        /// <param name="primaryColumn">Колонка, содержащая первичный ключ</param>
        public RefColumn(string columnName, string indexAndForeignKeyName, string primaryTable, string primaryColumn)
            : base(columnName, DbType.Int64)
        {
            IndexAndForeignKeyName = indexAndForeignKeyName;
            PrimaryTable = primaryTable;
            PrimaryColumn = primaryColumn;
        }
    }
}

