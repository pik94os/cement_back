﻿using System.Collections.Generic;
using System.Data;
using System.Linq;
using ECM7.Migrator.Framework;

namespace Cement.Core.Migrations.Extensions
{
    public static class MigratorExtension
    {
        public static void AddEntityTable(this ITransformationProvider database, string tableName, params Column[] columns)
        {
            var dictColumns = new List<Column>
            {
                new Column("ID", DbType.Int64, ColumnProperty.PrimaryKeyWithIdentity)
            };

            if (columns.Any())
            {
                dictColumns.RemoveAll(x => columns.Any(y => y.Name == x.Name));
            }

            dictColumns.AddRange(columns);

            database.AddTable(tableName, dictColumns.ToArray());
            AddForeignKeys(database, tableName, columns);
        }

        public static void AddSubclassEntityTable(this ITransformationProvider database, string tableName, string baseTableName, string foreignKeyName, params Column[] columns)
        {
            var dictColumns = new List<Column>
            {
                new Column("ID", DbType.Int64, ColumnProperty.PrimaryKey)
            };

            if (columns.Any())
            {
                dictColumns.RemoveAll(x => columns.Any(y => y.Name == x.Name));
            }

            dictColumns.AddRange(columns);

            database.AddTable(tableName, dictColumns.ToArray());

            database.AddForeignKey("FK_" + foreignKeyName, tableName, "ID", baseTableName, "ID");

            AddForeignKeys(database, tableName, columns);
        }

        public static void AddRefColumn(this ITransformationProvider database, string tableName, RefColumn column)
        {
            database.AddColumn(tableName, column);
            
            AddForeignKeys(database, tableName, column);
        }

        public static void RemoveRefColumn(this ITransformationProvider database, string tableName, string columnName, string indexAndForeignKeyName)
        {
            var constraintName = "FK_" + indexAndForeignKeyName;
            var indexName = "IND_" + indexAndForeignKeyName;

            if (database.ConstraintExists(tableName, constraintName))
            {
                database.RemoveConstraint(tableName, constraintName);
            }

            if (database.IndexExists(tableName, indexName))
            {
                database.RemoveIndex(tableName, indexName);
            }

            database.RemoveColumn(tableName, columnName);
        }

        public static void RemoveEntityTable(this ITransformationProvider database, string tableName)
        {
            database.RemoveTable(tableName);
        }

        /// <summary>
        /// Добавить Index и ForegnKey для всех RefColumn
        /// </summary>
        /// <param name="database">ITransformationProvider</param>
        /// <param name="tableName">Имя таблицы</param>
        /// <param name="columns">Массив колонок таблицы</param>
        private static void AddForeignKeys(this ITransformationProvider database, string tableName, params Column[] columns)
        {
            foreach (var column in columns)
            {
                if (column is RefColumn)
                {
                    var refColumn = column as RefColumn;
                    database.AddIndex("IND_" + refColumn.IndexAndForeignKeyName, false, tableName, refColumn.Name);
                    database.AddForeignKey("FK_" + refColumn.IndexAndForeignKeyName, tableName, refColumn.Name, refColumn.PrimaryTable, refColumn.PrimaryColumn);
                }
            }
        }
    }
}