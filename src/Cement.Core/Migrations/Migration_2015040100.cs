﻿using System.Data;
using Cement.Core.Enums;
using Cement.Core.Migrations.Extensions;
using ECM7.Migrator.Framework;

namespace Cement.Core.Migrations
{
    [Migration(2015040100)]
    public class Migration_2015040100 : Migration
    {
        public override void Apply()
        {
            Database.AddEntityTable("STOCKROOM_RECEIPT_ORDER_ITEM",
                new Column("TYPE", DbType.Int16, ColumnProperty.NotNull),
                new Column("AMOUNT", DbType.Decimal, ColumnProperty.NotNull, 0),
                new Column("PRICE", DbType.Decimal),
                new Column("TAX", DbType.Decimal),
                new Column("USED_AT", DbType.DateTime),
                new RefColumn("WAREHOUSE_ID", ColumnProperty.NotNull, "STRECORDITEM_WAREHOUSE", "WAREHOUSE", "ID"),
                new RefColumn("PRODUCT_ID", ColumnProperty.NotNull, "STRECORDITEM_PRODUCT", "PRODUCT", "ID"),
                new RefColumn("MEASURE_UNIT_ID", ColumnProperty.NotNull, "STRECORDITEM_MEASUNIT", "DICT_MEASURE_UNIT", "ID"),
                new RefColumn("ACCOUNTING_INOUT_DOC_ID", "STRECORDITEM_ACCINOUTDOC", "ACCOUNTING_INOUT_DOC", "ID"),
                new RefColumn("ACCOUNTING_OTHER_ID", "STRECORDITEM_ACCOTHER", "ACCOUNTING_OTHER", "ID"),
                new RefColumn("WAREHOUSE_FROM_ID", "STRECORDITEM_WAREHOUSEFROM", "WAREHOUSE", "ID"));

            Database.AddEntityTable("STOCKROOM_RECEIPT_ORDER",
                new Column("TYPE", DbType.Int16, ColumnProperty.NotNull),
                new Column("NO", DbType.String),
                new Column("DOCUMENT_DATE", DbType.DateTime),
                new Column("STATE", DbType.Int16, ColumnProperty.NotNull, (int)DocumentState.Draft),
                new Column("AMOUNT", DbType.Decimal, ColumnProperty.NotNull),
                new Column("USED_AT", DbType.DateTime),
                new RefColumn("WAREHOUSE_ID", ColumnProperty.NotNull, "STRECORDER_WAREHOUSE", "WAREHOUSE", "ID"),
                new RefColumn("STOCKROOM_RECEIPT_ORDER_ITEM_ID", ColumnProperty.NotNull, "STRECORDER_STRECORDITEM", "STOCKROOM_RECEIPT_ORDER_ITEM", "ID"));

            Database.AddEntityTable("STOCKROOM_RECEIPT_ORDER_ROUTE",
                new RefColumn("STOCKROOM_RECEIPT_ORDER_ID", ColumnProperty.NotNull, "STRECORDERROUTE_STRECORDER", "STOCKROOM_RECEIPT_ORDER", "ID"),
                new RefColumn("EMPLOYEE_ID", ColumnProperty.NotNull, "STRECORDERROUTE_EMPLOYEE", "EMPLOYEE", "ID"),
                new Column("DATE_IN", DbType.DateTime, ColumnProperty.NotNull),
                new Column("DATE_OUT", DbType.DateTime),
                new Column("COMMENT", DbType.String.WithSize(2000)),
                new Column("PROCESSING_RESULT", DbType.Int16),
                new Column("MOVEMENT_TYPE", DbType.Int16, ColumnProperty.NotNull),
                new Column("ARCHIVED_AT", DbType.DateTime),
                new Column("DELETED_AT", DbType.DateTime),
                new Column("STAGE", DbType.Int16, ColumnProperty.NotNull, 0));
        }

        public override void Revert()
        {
            Database.RemoveEntityTable("STOCKROOM_RECEIPT_ORDER_ROUTE");
            Database.RemoveEntityTable("STOCKROOM_RECEIPT_ORDER");
            Database.RemoveEntityTable("STOCKROOM_RECEIPT_ORDER_ITEM");
        }
    }
}