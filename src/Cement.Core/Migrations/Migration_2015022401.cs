﻿using Cement.Core.Migrations.Extensions;
using ECM7.Migrator.Framework;

namespace Cement.Core.Migrations
{
    [Migration(2015022401)]
    public class Migration_2015022401 : Migration
    {
        public override void Apply()
        {
            Database.AddRefColumn("PRODUCT_REQUEST", new RefColumn("TRANSPORT_UNIT_ID", "REQUEST_TUNIT", "TRANSPORT_UNIT", "ID"));
        }

        public override void Revert()
        {
            Database.RemoveRefColumn("PRODUCT_REQUEST", "TRANSPORT_UNIT_ID", "REQUEST_TUNIT");
        }
    }
}