﻿using System.Data;
using Cement.Core.Migrations.Extensions;
using ECM7.Migrator.Framework;

namespace Cement.Core.Migrations
{
    [Migration(2015021600)]
    public class Migration_2015021600 : Migration
    {
        public override void Apply()
        {
            Database.AddRefColumn("PRODUCT_REQUEST", new RefColumn("INITIAL_REQUEST_ID", "REQUEST_REQUEST", "PRODUCT_REQUEST", "ID"));
            Database.AddColumn("PRODUCT_REQUEST", new Column("STAGE_NUMBER", DbType.Int32));
            Database.AddColumn("PRODUCT_REQUEST", new Column("CHAIN_ID", DbType.Int64));
        }

        public override void Revert()
        {
            Database.RemoveRefColumn("PRODUCT_REQUEST", "INITIAL_REQUEST_ID", "REQUEST_REQUEST");
            Database.RemoveColumn("PRODUCT_REQUEST", "STAGE_NUMBER");
            Database.RemoveColumn("PRODUCT_REQUEST", "CHAIN_ID");
        }
    }
}