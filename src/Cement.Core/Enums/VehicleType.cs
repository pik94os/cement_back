﻿namespace Cement.Core.Enums
{
    /// <summary>
    /// Тип транспортного средства
    /// </summary>
    public enum VehicleType
    {
        /// <summary>
        /// Одиночка
        /// </summary>
        Single = 1,

        /// <summary>
        /// Полуприцеп
        /// </summary>
        Semitrailer = 2,

        /// <summary>
        /// Сцепка
        /// </summary>
        Couple = 3,
    }
}