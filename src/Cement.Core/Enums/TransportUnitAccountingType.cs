﻿namespace Cement.Core.Enums
{
    public enum TransportUnitAccountingType
    {
        Available = 10,

        Buzy = 20,

        InRepair = 30,

        Missing = 40
    }
}