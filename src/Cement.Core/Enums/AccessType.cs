﻿namespace Cement.Core.Enums
{
    public enum AccessType
    {
        Denied = 10,

        Read = 20,

        ReadWrite = 30
    }
}