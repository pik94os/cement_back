﻿using System.ComponentModel.DataAnnotations;
using Cement.Core.Attributes;

namespace Cement.Core.Enums
{
    /// <summary>
    /// Тип документа, который может иметь связи
    /// </summary>
    public enum DocumentType
    {
        [Display(Name = "Внутренний документ")]
        InternalDocument = 10,

        [Display(Name = "Корреспонденция")]
        Correspondence = 20,

        [Display(Name = "Договор")]
        Contract = 30,

        [Display(Name = "Сообщение")]
        Message = 40,

        [Display(Name = "Прайс-лист")]
        PriceList = 50,
        
        [Display(Name = "Объявление")]
        Advert = 60,

        [Display(Name = "Заявка на товар (услугу)")]
        Request = 70,

        [Display(Name = "Складская заявка")]
        [RegistrationName("StockroomRequestService")]
        StockroomRequest = 80,

        [Display(Name = "Инвентаризация")]
        [RegistrationName("InventorServicey")]
        Inventory = 90,
        
        [Display(Name = "Расходный складской ордер")]
        [RegistrationName("StockroomExpenseOrderService")]
        StockroomExpenseOrder = 100,
        
        [Display(Name = "Приходный складской ордер")]
        [RegistrationName("StockroomReceiptOrderService")]
        StockroomReceiptOrder = 110,

        [Display(Name = "Иной бухгалтерский документ")]
        [RegistrationName("AccountingOtherService")]
        OtherAccountingDocument = 120,

        [Display(Name = "Документы (приход/расход)")]
        [RegistrationName("AccountingInOutDocumentService")]
        AccountingInOutDocument = 130
        
    }
}