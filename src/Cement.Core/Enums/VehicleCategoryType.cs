﻿namespace Cement.Core.Enums
{
    /// <summary>
    /// Тип категории транспортного средства
    /// </summary>
    public enum VehicleCategoryType
    {
        /// <summary>
        /// Прицеп
        /// </summary>
        Trailer = 20,

        /// <summary>
        /// Другое
        /// </summary>
        Other = 100,
    }
}