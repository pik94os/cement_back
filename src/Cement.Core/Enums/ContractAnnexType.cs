﻿namespace Cement.Core.Enums
{
    public enum ContractAnnexType
    {
        /// <summary>
        /// Приложение к договору
        /// </summary>
        Application = 10,

        /// <summary>
        /// Доп. соглашение
        /// </summary>
        Additional = 20,

        /// <summary>
        /// Спецификация
        /// </summary>
        Specification = 30
    }
}