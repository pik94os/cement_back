﻿using System.ComponentModel.DataAnnotations;

namespace Cement.Core.Enums
{
    /// <summary>
    /// Виды сообщений
    /// </summary>
    public enum MessageType
    {
#warning значения енама придуманы на шару
        [Display(Name = "Эл.письмо")]
        Type1 = 10,

        [Display(Name = "Телефонограмма")]
        Type2 = 20
    }
}