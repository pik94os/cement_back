﻿namespace Cement.Core.Enums
{
    public enum PositionType
    {
        /// <summary>
        /// Другое
        /// </summary>
        Other = 10,

        /// <summary>
        /// Водитель ТС
        /// </summary>
        Driver = 20
    }
}