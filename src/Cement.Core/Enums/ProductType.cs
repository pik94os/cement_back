﻿namespace Cement.Core.Enums
{
    public enum ProductType
    {
        /// <summary>
        /// Продукт
        /// </summary>
        Product = 10,

        /// <summary>
        /// Сервис
        /// </summary>
        Service = 20
    }
}