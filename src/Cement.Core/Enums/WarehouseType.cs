﻿namespace Cement.Core.Enums
{
    /// <summary>
    /// Тип склада
    /// </summary>
    public enum WarehouseType
    {
        /// <summary>
        /// Авто
        /// </summary>
        Auto = 1,

        /// <summary>
        /// Ж/Д
        /// </summary>
        Railway = 2,
    }
}