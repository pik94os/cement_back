﻿using System.ComponentModel.DataAnnotations;

namespace Cement.Core.Enums
{
    /// <summary>
    /// Тип заявки
    /// </summary>
    public enum RequestType
    {
        [Display(Name = "Заявка на товар")]
        Product = 10,

        [Display(Name = "Заявка на услугу")]
        Service = 20
    }
}