﻿using System.ComponentModel.DataAnnotations;

namespace Cement.Core.Enums
{
    /// <summary>
    /// Направление движения документа
    /// </summary>
    public enum DocumentMovementType
    {
        [Display(Name = "Входящий")]
        Incoming = 10,

        [Display(Name = "Исходящий")]
        Outcoming = 20
    }
}