﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Cement.Core.Enums
{
    /// <summary>
    /// Свойства документа
    /// </summary>
    //[Flags]
    public enum DocumentProperty
    {
        [Display(Name = "")]
        None = 0,

        [Display(Name = "На согласование")] 
        Agreement = 1,

        //[Display(Name = "На подпись")]
        //ForSign = 2,

        [Display(Name = "На регистрацию")]
        ForRegistration = 2,

        //Next = 4,
        //Next = 8,
        //Next = 16,
        //Next = 32,
        //Next = 64,
    }
}