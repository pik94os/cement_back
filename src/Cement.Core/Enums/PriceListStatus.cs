﻿namespace Cement.Core.Enums
{
    /// <summary>
    /// Статус прайс-листа
    /// </summary>
    public enum PriceListStatus
    {
        /// <summary>
        /// Новый
        /// </summary>
        New = 10,

        /// <summary>
        /// Действующий
        /// </summary>
        Working = 20,

        /// <summary>
        /// Архив
        /// </summary>
        Archive = 30
    }
}