﻿namespace Cement.Core.Enums.TermsOfPayment
{
    public enum DayNumberKind
    {
        Day = 10,
        Period = 20
    }
}