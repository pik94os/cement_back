﻿namespace Cement.Core.Enums.TermsOfPayment
{
    public enum DaysKind
    {
        WorkDays = 10,
        BankDays = 20
    }
}