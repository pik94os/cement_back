﻿namespace Cement.Core.Enums.TermsOfPayment
{
    public enum PayKind
    {
        Days = 10,
        NextMonthDay = 20,
        DayNumber = 30,
        Accredetive = 40,
        Prepayment = 50
    }
}