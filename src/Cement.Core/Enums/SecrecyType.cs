﻿using System.ComponentModel.DataAnnotations;

namespace Cement.Core.Enums
{
    public enum SecrecyType
    {
        [Display(Name = "Обычный")]
        Usual = 10,

        [Display(Name = "Средний")]
        Medium = 20,

        [Display(Name = "Высокий")]
        High = 30
    }
}