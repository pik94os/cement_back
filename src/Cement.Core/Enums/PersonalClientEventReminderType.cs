﻿using System.ComponentModel.DataAnnotations;

namespace Cement.Core.Enums
{
    /// <summary>
    /// Тип напоминания события персонального клиента
    /// </summary>
    public enum PersonalClientEventReminderType
    {
        [Display(Name = "При наступлении")]
        UponOccurrence,

        [Display(Name = "За 5 минут")]
        Before05Minute,

        [Display(Name = "За 15 минут")]
        Before15Minute,

        [Display(Name = "За 30 минут")]
        Before30Minute,

        [Display(Name = "За час")]
        Before60Minute,

        [Display(Name = "За 2 часа")]
        Before120Minute,

        [Display(Name = "Не напоминать")]
        DoNotRemind
    }
}