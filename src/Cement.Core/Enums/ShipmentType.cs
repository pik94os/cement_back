﻿using System.ComponentModel.DataAnnotations;

namespace Cement.Core.Enums
{
    /// <summary>
    /// Способ отгрузки
    /// </summary>
    public enum ShipmentType
    {
        /// <summary>
        /// Самовывоз
        /// </summary>
        [Display(Name = "Самовывоз")]
        Pickup = 1,

        /// <summary>
        /// Самовывоз по дговору
        /// </summary>
        [Display(Name = "Самовывоз по дговору")]
        PickupByContract = 2,

        /// <summary>
        /// Централизация
        /// </summary>
        [Display(Name = "Централизация")]
        Сentralization = 3
    }
}