﻿using System;

namespace Cement.Core.Enums
{
    /// <summary>
    /// Специфичность
    /// </summary>
    [Flags]
    public enum Specificity
    {
        /// <summary>
        /// Отстутсвует
        /// </summary>
        None = 0,

        /// <summary>
        /// Письмо с прайс-листом
        /// </summary>
        MessageWithPriceList = 1,
        
        //Next = 2,
        //Next = 4,
        //Next = 8,
        //Next = 16,
        //Next = 32,
        //Next = 64,
        //Next = 128,
    }
}