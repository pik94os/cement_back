﻿using System.ComponentModel.DataAnnotations;

namespace Cement.Core.Enums
{
    /// <summary>
    /// Вид корреспонденции
    /// </summary>
    public enum CorrespondenceType
    {
        [Display(Name = "Акт")]
        Act = 10,

        [Display(Name = "Анкета")]
        Anketa = 20,

        [Display(Name = "График")]
        Schedule = 30,

        [Display(Name = "Докладная записка")]
        Memorandum = 40,

        [Display(Name = "Должностная инструкция")]
        JobDescription = 50,

        [Display(Name = "Записка")]
        Note = 60,

        [Display(Name = "Заявление")]
        Statement = 70,

        [Display(Name = "Инструкция")]
        Instruction = 80,

        [Display(Name = "Объяснительная записка")]
        ExplanatoryNote = 90,

        [Display(Name = "Письмо")]
        Message = 100,

        [Display(Name = "Положение")]
        Thesis = 110,

        [Display(Name = "Постановление")]
        Resolution = 120,

        [Display(Name = "Правила")]
        Rules = 130,

        [Display(Name = "Представление")]
        Presentation = 140,

        [Display(Name = "Протокол")]
        Protocol = 150,

        //[Display(Name = "Распоряжение")]
        //Disposal = 160,

        [Display(Name = "Служебная записка")]
        ServiceNote = 170,

        [Display(Name = "Штатное расписание")]
        StaffList = 180
    }
}