﻿using System.ComponentModel.DataAnnotations;

namespace Cement.Core.Enums
{
    /// <summary>
    /// Результат рассмотрения документа
    /// </summary>
    public enum DocumentProcessingResult
    {
        [Display(Name = "На согласовании")]
        None = 0,

        [Display(Name = "Отклонено")]
        Rejected = 20,

        [Display(Name = "Согласовано")]
        Agreed = 30,

        [Display(Name = "Подписано")]
        Signed = 40,

        [Display(Name = "На ознакомлении")]
        OnAcquaintance = 50,

        [Display(Name = "Ознакомлено")]
        Acquainted = 60,
    }
}