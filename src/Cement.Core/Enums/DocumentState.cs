﻿using System.ComponentModel.DataAnnotations;

namespace Cement.Core.Enums
{
    /// <summary>
    /// Статус документа
    /// </summary>
    public enum DocumentState
    {
        [Display(Name = "Черновик/Новый")]
        Draft = 10,

        [Display(Name = "Отклонено")]
        Rejected = 20,

        [Display(Name = "На согласовании")]
        OnAgreement = 30,

        [Display(Name = "На согласовании")]
        OnSecondSideAgreement = 35,
        
        [Display(Name = "Подписано")]
        Signed = 40
    }
}