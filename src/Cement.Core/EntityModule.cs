﻿using System.Collections.Generic;
using System.Reflection;
using DataAccess;

namespace Cement.Core
{
    public class EntityModule : IEntityModule
    {
        public List<Assembly> GetAssemblies()
        {
            return new List<Assembly>{ Assembly.GetExecutingAssembly() };

        }
    }
}