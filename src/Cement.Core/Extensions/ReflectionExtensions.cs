﻿using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using Newtonsoft.Json;

namespace Cement.Core.Extensions
{
    public static class ReflectionExtensions
    {
        public static Dictionary<string, PropertyInfo> GetPropertiesDict(this List<PropertyInfo> properties)
        {
            return properties
                .Select(property =>
                {
                    var propertyName = property.Name;

                    var jsonPropertyAttribute =
                        property.CustomAttributes.FirstOrDefault(x => x.AttributeType == typeof(JsonPropertyAttribute));

                    if (jsonPropertyAttribute != null && jsonPropertyAttribute.NamedArguments != null)
                    {
                        var members =
                            jsonPropertyAttribute.NamedArguments.Where(x => x.MemberName == "PropertyName").ToList();

                        if (members.Any())
                        {
                            var val = members.First().TypedValue.Value;
                            propertyName = val != null ? val.ToString() : propertyName;
                        }
                    }

                    return new { propertyName, property };
                })
                .GroupBy(x => x.propertyName)
                .ToDictionary(x => x.Key, x => x.First().property);
        }
    }
}