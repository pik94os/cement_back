﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Reflection;
using System.Threading;
using System.Threading.Tasks;
using Castle.Core.Internal;
using Cement.Core.Entities;
using Cement.Core.Services.StoreParams;

namespace Cement.Core.Extensions
{
    public static class RequestExtension
    {
        public static T ConvertToType<T>(this HttpRequestMessage request) where T : class, new()
        {
            if (request.Content.IsFormData())
            {
                var formData = request.Content.ReadAsFormDataAsync().Result;

                return ProcessFormData<T>(formData);
            }

            if (request.Content.IsMimeMultipartContent())
            {
                return ProcessMultipartFormData<T>(request);
            }

            return default(T);
        }

        public static T ProcessFormData<T>(NameValueCollection nvc) where T : class, new()
        {
            var properties = typeof(T).GetProperties();

            var result = new T();

            var propertiesDict = properties.ToList().GetPropertiesDict();

            propertiesDict.ForEach(x =>
            {
                var propertyName = x.Key;
                var property = x.Value;

                var value = nvc.Get(propertyName);

                if (!property.CanWrite)
                {
                    return;
                }

                if (property.PropertyType.Name.ToLower() == "string")
                {
                    property.SetValue(result, value);
                }
                else if (!string.IsNullOrWhiteSpace(value))
                {
                    ProcessNonStringData(value, property, result);
                }
            });

            if (result is RawStoreParams)
            {
                var dictionary = (result as RawStoreParams).Dictionary;

                foreach (string key in nvc)
                {
                    if (!propertiesDict.ContainsKey(key))
                    {
                        dictionary[key] = nvc[key];
                    }
                }
            }

            return result;
        }

        private static void ProcessNonStringData<T>(object value, PropertyInfo property, T result)
        {
            if (property.PropertyType.Name == "Nullable`1")
            {
                var val = ChangeType(value, property.PropertyType.GenericTypeArguments.First());

                property.SetValue(result, val);
            }
            else if (property.PropertyType.IsEnum)
            {
                property.SetValue(result, Convert.ChangeType(value, typeof(int)));
            }
            //else if (property.PropertyType == typeof(Boolean))
            //{
            //    var strVal = value.ToString().ToLower();

            //    property.SetValue(result, strVal == "1" || strVal == "true");
            //}
            else if (property.PropertyType.IsSubclassOf(typeof(BaseEntity)))
            {
                var id = value.ToLong();

                if (id != 0)
                {
                    var entity = property.PropertyType.GetConstructors().First().Invoke(null);

                    property.PropertyType.GetProperty("Id").SetValue(entity, id);

                    property.SetValue(result, entity);
                }
            }
            else if (property.PropertyType.Name == "List`1")
            {
                ParseList(value, property, result);
            }
            else
            {
                property.SetValue(result, ChangeType(value, property.PropertyType));
            }
        }

        private static void ParseList<T>(object value, PropertyInfo property, T result)
        {
            var type = property.PropertyType.GenericTypeArguments.First();

            var strValues = value.SafeToString().Trim('[', ']').Split(new[] { ',' }, StringSplitOptions.RemoveEmptyEntries);

            if (type == typeof(long))
            {
                var list = strValues.Select(x => x.ToLong()).ToList();

                property.SetValue(result, list);
            }
            else if (type == typeof(string))
            {
                var list = strValues.Select(x => x.SafeToString().Trim('"').Trim()).ToList();

                property.SetValue(result, list);
            }
        }

        private static object ChangeType(object value, Type type)
        {
            if (type == typeof(decimal))
            {
                return value.ToDecimal();
            }

            if (type == typeof(long))
            {
                return value.ToLong();
            }

            if (type == typeof(int))
            {
                return value.ToInt();
            }

            if (type == typeof(bool))
            {
                return value.ToBool();
            }

            if (type == typeof(DateTime))
            {
                var strValue = value.ToString();

                var date = strValue.ToDateTimeFromCementString();

                date = date ?? strValue.ToDateTimeFromCementString("dd.MM.yyyy");

                date = date ?? strValue.ToDateTimeFromCementString("HH:mm");

                if (date == null)
                {
                    throw new Exception("Неверный формат даты: '" + strValue + "'");
                }

                return date.Value;
            }

            return Convert.ChangeType(value, type);
        }

        private static T ProcessMultipartFormData<T>(HttpRequestMessage request) where T : class, new()
        {
            var root = Path.GetTempPath();

            var provider = new MultipartFormDataStreamProvider(root);

            // request.Content.ReadAsMultipartAsync(provider).Result
            // ToDo Избавиться от магии

            Task.Factory
                .StartNew(() =>
                    request.Content.ReadAsMultipartAsync(provider).Wait(),
                    CancellationToken.None,
                    TaskCreationOptions.LongRunning,
                    TaskScheduler.Default)
                .Wait();

            var result = ProcessFormData<T>(provider.FormData);

            if (provider.FileData != null && provider.FileData.Count > 0)
            {
                var properties = typeof(T)
                    .GetProperties()
                    .Where(x => x.PropertyType == typeof(EntityFile))
                    .ToList();

                var propertiesDict = properties.GetPropertiesDict();

                foreach (var multipartFileData in provider.FileData)
                {
                    if (multipartFileData == null
                        || multipartFileData.Headers == null
                        || multipartFileData.Headers.ContentDisposition == null
                        || string.IsNullOrWhiteSpace(multipartFileData.Headers.ContentDisposition.Name)
                        || string.IsNullOrWhiteSpace(multipartFileData.Headers.ContentDisposition.Name.Trim('\"')))
                    {
                        continue;
                    }

                    var propertyName = multipartFileData.Headers.ContentDisposition.Name.Trim('\"');

                    if (propertiesDict.ContainsKey(propertyName))
                    {
                        var entityFileWithData = GetEntityFileWithData(multipartFileData);

                        propertiesDict[propertyName].SetValue(result, entityFileWithData);
#warning добавить доп проверки для update

                        if (entityFileWithData != null)
                        {

                        }
                    }
                }
            }

            return result;
        }

        private static EntityFileWithData GetEntityFileWithData(MultipartFileData multipartFileData)
        {
            if (string.IsNullOrWhiteSpace(multipartFileData.Headers.ContentDisposition.FileName)
                || string.IsNullOrWhiteSpace(multipartFileData.Headers.ContentDisposition.FileName.Trim('\"'))
                || string.IsNullOrWhiteSpace(multipartFileData.LocalFileName)
                || !File.Exists(multipartFileData.LocalFileName))
            {
                return null;
            }

            var fileName = multipartFileData.Headers.ContentDisposition.FileName.Trim('\"');
            var fileParts = fileName.Split('.');
            var extension = fileParts.Count() > 1 ? fileParts.Last() : string.Empty;

            var data = File.ReadAllBytes(multipartFileData.LocalFileName);

            var result = new EntityFileWithData
            {
                Data = data,
                Size = data.Length,
                CheckSum = Convert.ToBase64String(System.Security.Cryptography.MD5.Create().ComputeHash(data)),
                Name = fileName.Replace("." + extension, string.Empty),
                Extension = extension
            };

            return result;
        }

    }
}