﻿namespace Cement.Core.Extensions
{
    using System.Collections.Generic;

    public static class DictionaryExtension
    {
        public static TValue Get<TKey, TValue>(this IDictionary<TKey, TValue> dictionary, TKey key, TValue defValue = default (TValue))
        {
            TValue value;
            if (!dictionary.TryGetValue(key, out value))
            {
                value = defValue;
            }

            return value;
        }
    }
}