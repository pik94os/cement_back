﻿using System;
using NHibernate.Util;

namespace Cement.Core.Extensions
{
    public static class ObjectExtention
    {
        public static object GetPropValue(this object src, string propName)
        {
            return src.GetType().GetProperty(propName).GetValue(src, null);
        }

        public static string SafeToString(this object obj)
        {
            return obj == null ? string.Empty : obj.ToString();
        }

        /// <summary>
        /// Вычисление значения из переданного объекта.
        /// Если возникла ошибка выполнения (в Func), то метод выбросит исключение.
        /// </summary>
        /// <param name="o">Экземпляр.</param>
        /// <param name="evaluator">Функция вычисления значения.</param>
        /// <param name="failureValue">Значение, которое будет возвращено если в качестве экземпляра передан Null.</param>
        /// <typeparam name="TInput">Тип экземпляра.</typeparam>
        /// <typeparam name="TResult">Тип результата.</typeparam>
        /// <returns></returns>
        public static TResult Return<TInput, TResult>(
            this TInput o,
            Func<TInput, TResult> evaluator,
            TResult failureValue = default(TResult))
        {
            var type = typeof(TInput);
            if (type.IsValueType && !type.IsNullable())
                return evaluator(o);

            return (object)o == null ? failureValue : evaluator(o);
        }
    }
}