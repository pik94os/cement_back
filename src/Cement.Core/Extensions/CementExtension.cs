﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Globalization;
using System.Linq;
using System.Text;
using Cement.Core.Attributes;
using Cement.Core.Enums;

namespace Cement.Core.Extensions
{
    public static class CementExtension
    {
        public static CultureInfo Culture = new CultureInfo("ru-RU");

        public static string ToCementDateTime(this DateTime? obj)
        {
            if (obj == null)
            {
                return string.Empty;
            }

            return obj.Value.ToString("dd.MM.yy");
        }

        public static DateTime? ToDateTimeFromCementString(this string obj)
        {
            return obj.ToDateTimeFromCementString("dd.MM.yy");
        }

        public static DateTime? ToDateTimeFromCementString(this string obj, string format)
        {
            if (obj == null)
            {
                return null;
            }

            DateTime date;
            DateTime? result = null;

            if (DateTime.TryParseExact(obj, format, Culture, DateTimeStyles.None, out date))
            {
                result = date;
            }

            return result;
        }

        public static IEnumerable<Dictionary<string, object>> Extend<T>(this IEnumerable<T> enumerable, Action<Dictionary<string, object>, T> action)
        {
            var properties = typeof (T).GetProperties();

            return enumerable.Select(x =>
            {
                var dict = properties.ToDictionary(property => property.Name, property => property.GetValue(x));

                action(dict, x);

                return dict;
            });
        }

        public static Dictionary<string, object> Extend<T>(this T obj, Action<Dictionary<string, object>, T> action)
        {
            var properties = typeof(T).GetProperties();

            var dict = properties.ToDictionary(property => property.Name, property => property.GetValue(obj));

            action(dict, obj);

            return dict;
        }

        public static byte[] GetBytes(this string obj)
        {
            if (obj == null)
            {
                return null;
            }

            return Encoding.UTF8.GetBytes(obj);
        }

        public static string GetString(this byte[] byteArray)
        {
            if (byteArray == null)
            {
                return null;
            }

            return Encoding.UTF8.GetString(byteArray);
        }

        public static string GetDisplayValue(this Enum value)
        {
            var fieldInfo = value.GetType().GetField(value.ToString());

            if (fieldInfo == null)
            {
                return value.ToString();
            }

            var descriptionAttributes = fieldInfo.GetCustomAttributes(
                typeof(DisplayAttribute), false) as DisplayAttribute[];

            if (descriptionAttributes == null)
            {
                return value.ToString();
            }

            return (descriptionAttributes.Length > 0) ? descriptionAttributes[0].Name : value.ToString();
        }

        public static string GetRegistrationName(this Enum value)
        {
            const string defaultReturnValue = "не смогла я, не смогла";

            var fieldInfo = value.GetType().GetField(value.ToString());

            if (fieldInfo == null)
            {
                return defaultReturnValue;
            }

            var descriptionAttributes = fieldInfo.GetCustomAttributes(
                typeof(RegistrationNameAttribute), false) as RegistrationNameAttribute[];

            if (descriptionAttributes == null)
            {
                return defaultReturnValue;
            }

            return (descriptionAttributes.Length > 0) ? descriptionAttributes[0].Name : defaultReturnValue;
        }

        public static Dictionary<int, string> AsDictionary(this Type enumType)
        {
            var dict = new Dictionary<int, string>();

            if (!enumType.IsSubclassOf(typeof (Enum)))
            {
                return dict;
            }
            
            foreach (var type in Enum.GetValues(enumType))
            {
                dict[(int)type] = ((Enum)type).GetDisplayValue();
            }

            return dict;
        }

        public static List<long> ToLongList(this string ids)
        {
            var result = new List<long>();

            if (string.IsNullOrWhiteSpace(ids))
            {
                return result;
            }

            var strValues = ids.Trim('[', ']').Split(new[] { ',' }, StringSplitOptions.RemoveEmptyEntries);

            result = strValues.Select(x => x.ToLong()).ToList();
            
            return result;
        }

        public static Tuple<bool, DocumentType, long> GetDocumentDetails(this string idStr)
        {
            var result = new Tuple<bool, DocumentType, long>(false, 0, 0);

            if (string.IsNullOrWhiteSpace(idStr)|| idStr.Contains("\""))
            {
                return result;
            }
            
            var keyParts = idStr.Split(new[] { Constants.DocIdDelimeter }, StringSplitOptions.RemoveEmptyEntries);

            if (keyParts.Length != 2)
            {
                return result;
            }
            
            var docType = (DocumentType)keyParts.First().ToLong();
            var docId = keyParts.Last().ToLong();

            return new Tuple<bool, DocumentType, long>(true, docType, docId);
        }

        public static string TimeSpent(DateTime? dateOut, DateTime? dateIn)
        {
            if (dateOut.HasValue && dateIn.HasValue)
            {
                return (dateOut - dateIn).ToString();
            }

            return null;
        }
    }
}