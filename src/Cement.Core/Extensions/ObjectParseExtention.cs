﻿
using System;
using System.Globalization;

namespace Cement.Core.Extensions
{
    /// <summary>
    /// Набор методов-расширений для распарсивания объектов.
    /// </summary>
    public static class ObjectParseExtention
    {
        /// <summary>
        /// Приводит <paramref name="obj"/> к типу <typeparamref name="T"/>.
        /// Если не удалось, то возвращается default(<typeparamref name="T"/>).
        /// </summary>
        /// <typeparam name="T">
        /// Тип, к которому приводится объект.
        /// </typeparam>
        /// <param name="obj">
        /// Объект.
        /// </param>
        /// <returns>
        /// Приведенное значение.
        /// </returns>
        public static T To<T>(this object obj)
        {
            return To(obj, default(T));
        }

        /// <summary>
        /// Приводит <paramref name="obj"/> к типу <typeparamref name="T"/>.
        /// Если не удалось, то возвращается <paramref name="defaultValue"/>.
        /// </summary>
        /// <typeparam name="T">
        /// Тип, к которому приводится объект.
        /// </typeparam>
        /// <param name="obj">
        /// Объект.
        /// </param>
        /// <param name="defaultValue">
        /// Дефолтное значение.
        /// </param>
        /// <returns>
        /// Приведенное значение.
        /// </returns>
        public static T To<T>(this object obj, T defaultValue)
        {
            if (obj == null)
            {
                return defaultValue;
            }

            if (obj is T)
            {
                return (T)obj;
            }

            Type type = typeof(T);

            if (type == typeof(string))
            {
                return (T)(object)obj.ToString();
            }

            Type underlyingType = Nullable.GetUnderlyingType(type);

            if (underlyingType != null)
            {
                return To(obj, defaultValue, underlyingType);
            }

            return To(obj, defaultValue, type);
        }

        /// <summary>
        /// The to bool.
        /// </summary>
        /// <param name="obj">
        /// The obj. 
        /// </param>
        /// <returns>
        /// The to bool. 
        /// </returns>
        public static bool ToBool(this object obj)
        {
            if (obj == null)
            {
                return false;
            }

            if (obj is bool)
            {
                return (bool)obj;
            }

            bool boolValue;

            if (bool.TryParse(obj.ToString(), out boolValue))
            {
                return boolValue;
            }

            int intValue;

            if (int.TryParse(obj.ToString(), out intValue))
            {
                return intValue != 0;
            }

            return false;
        }

        /// <summary>
        /// The to date time.
        /// </summary>
        /// <param name="obj">
        /// The obj. 
        /// </param>
        /// <returns>
        /// </returns>
        public static DateTime ToDateTime(this object obj)
        {
            if (obj == null)
            {
                return DateTime.MinValue;
            }

            if (obj is DateTime)
            {
                return (DateTime)obj;
            }

            DateTime dateTimeValue;

            if (DateTime.TryParse(obj.ToString(), out dateTimeValue))
            {
                return dateTimeValue;
            }

            return DateTime.MinValue;
        }

        /// <summary>
        /// The to decimal.
        /// </summary>
        /// <param name="obj">
        /// The obj. 
        /// </param>
        /// <returns>
        /// The to decimal. 
        /// </returns>
        public static decimal ToDecimal(this object obj)
        {
            if (obj == null)
            {
                return 0M;
            }

            if (obj is decimal)
            {
                return (decimal)obj;
            }

            string decimalString = obj.ToString();

            if (decimalString == string.Empty)
            {
                return 0M;
            }

            string value = decimalString.Replace(
                CultureInfo.InvariantCulture.NumberFormat.CurrencyDecimalSeparator,
                CultureInfo.CurrentCulture.NumberFormat.CurrencyDecimalSeparator);

            decimal decValue;

            if (decimal.TryParse(value, out decValue))
            {
                return decValue;
            }

            return 0M;
        }

        /// <summary>
        /// The to double.
        /// </summary>
        /// <param name="obj">
        /// The obj. 
        /// </param>
        /// <returns>
        /// The to double. 
        /// </returns>
        public static double ToDouble(this object obj)
        {
            if (obj == null)
            {
                return 0;
            }

            if (obj is double)
            {
                return (double)obj;
            }

            double dbValue;

            string value = obj.ToString().Replace(
                CultureInfo.InvariantCulture.NumberFormat.CurrencyDecimalSeparator,
                CultureInfo.CurrentCulture.NumberFormat.CurrencyDecimalSeparator);

            if (double.TryParse(value, out dbValue))
            {
                return dbValue;
            }

            return 0;
        }

        /// <summary>
        /// To long
        /// </summary>
        /// <param name="obj"></param>
        /// <param name="defaultValue"></param>
        /// <returns></returns>
        public static long ToLong(this object obj, long defaultValue)
        {
            if (obj == null || obj == DBNull.Value)
            {
                return defaultValue;
            }

            if (obj is long)
            {
                return (long)obj;
            }

            long longValue;

            if (long.TryParse(obj.ToString(), out longValue))
            {
                return longValue;
            }

            return defaultValue;
        }

        /// <summary>
        /// To long
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public static long ToLong(this object obj)
        {
            if (obj == null || obj == DBNull.Value)
            {
                return 0;
            }

            if (obj is long)
            {
                return (long)obj;
            }

            if (obj is bool)
            {
                return (bool)obj ? 1 : 0;
            }

            long longValue;

            if (long.TryParse(obj.ToString(), out longValue))
            {
                return longValue;
            }

            return 0;
        }

        /// <summary>
        /// The to int.
        /// </summary>
        /// <param name="obj">
        /// The obj. 
        /// </param>
        /// <param name="defaultValue">
        /// The default value. 
        /// </param>
        /// <returns>
        /// The to int. 
        /// </returns>
        public static int ToInt(this object obj, int defaultValue)
        {
            if (obj == null || obj == DBNull.Value)
            {
                return defaultValue;
            }

            if (obj is int)
            {
                return (int)obj;
            }

            int intValue;

            if (int.TryParse(obj.ToString(), out intValue))
            {
                return intValue;
            }

            return defaultValue;
        }

        /// <summary>
        /// The to int.
        /// </summary>
        /// <param name="obj">
        /// The obj. 
        /// </param>
        /// <returns>
        /// The to int. 
        /// </returns>
        public static int ToInt(this object obj)
        {
            if (obj == null || obj == DBNull.Value)
            {
                return 0;
            }

            if (obj is int)
            {
                return (int)obj;
            }

            if (obj is bool)
            {
                return (bool)obj ? 1 : 0;
            }

            int intValue;

            if (int.TryParse(obj.ToString(), out intValue))
            {
                return intValue;
            }

            return 0;
        }

        /// <summary>
        /// Приводит объект к типу string.
        /// Если объект равен null, то возвращает пустую строку.
        /// </summary>
        /// <param name="obj">
        /// </param>
        /// <returns>
        /// </returns>
        public static string ToStr(this object obj)
        {
            return obj == null ? string.Empty : obj.ToString();
        }

        /// <summary>
        /// The to.
        /// </summary>
        /// <param name="obj">
        /// The obj. 
        /// </param>
        /// <param name="defaultValue">
        /// The default value. 
        /// </param>
        /// <param name="type">
        /// The type. 
        /// </param>
        /// <typeparam name="T">
        /// </typeparam>
        /// <returns>
        /// </returns>
        private static T To<T>(object obj, T defaultValue, Type type)
        {
            if (type.IsEnum)
            {
                if (obj is decimal)
                {
                    return (T)Enum.Parse(type, obj.ToString());
                }

                if (obj is string)
                {
                    return (T)Enum.Parse(type, (string)obj);
                }

                if (obj is long)
                {
                    return (T)Enum.Parse(type, obj.ToString());
                }

                if (Enum.IsDefined(type, obj))
                {
                    return (T)Enum.Parse(type, obj.ToString());
                }

                return defaultValue;
            }

            try
            {
                return (T)Convert.ChangeType(obj, type);
            }
            catch
            {
                return defaultValue;
            }
        }
    }
}