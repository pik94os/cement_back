﻿using System;
using Castle.MicroKernel.Registration;
using Castle.MicroKernel.SubSystems.Configuration;
using Castle.Windsor;
using Cement.Core.Auth;
using Cement.Core.Auth.Impl;
using Cement.Core.Entities;
using Cement.Core.Entities.Stockroom;
using Cement.Core.Enums;
using Cement.Core.Extensions;
using Cement.Core.Services;
using Cement.Core.Services.Contract;
using Cement.Core.Services.Dictionary;
using Cement.Core.Services.Dictionary.Impl;
using Cement.Core.Services.EntityService;
using Cement.Core.Services.EntityService.Dict;
using Cement.Core.Services.EntityService.Impl;
using Cement.Core.Services.Impl;
using Cement.Core.Services.Log;
using Cement.Core.Services.Log.Impl;
using Cement.Core.Services.Mail;
using Cement.Core.Services.Mail.Impl;
using DataAccess;

namespace Cement.Core
{
    public class ModuleInstaller : IWindsorInstaller
    {
        public void Install(IWindsorContainer container, IConfigurationStore store)
        {
            RegisterAuth(container);

            container.Register(Component.For<IEntityModule>().ImplementedBy<EntityModule>());

            container.Register(Component.For<IEntityFileService>().ImplementedBy<EntityFileService>());

            container.Register(Component.For(typeof(IEntityService<>)).ImplementedBy(typeof(BaseEntityService<>)).LifestyleTransient());
            
            container.Register(Component.For<IDictService>().ImplementedBy<DictService>());

            container.Register(Component.For<IMailSender>().ImplementedBy<MailSenderSmtp>());

            container.Register(Component.For<ILogger>().ImplementedBy<NLogLogger>());

            RegisterCustomEntityServices(container);

            RegisterFileEntityServices(container);

            RegisterServices(container);

            RegisterContractSavers(container);
        }

        private void RegisterAuth(IWindsorContainer container)
        {
            container.Register(Component.For<IUserCache>().ImplementedBy<UserCache>());
            
            container.Register(Component.For<IPasswordHashProvider>().ImplementedBy<PasswordHashProvider>().LifestyleTransient());

            container.Register(Component.For<IPasswordGenerator>().ImplementedBy<PasswordGenerator>().LifestyleTransient());

            container.Register(Component.For<IAuthenticationProvider>().ImplementedBy<AuthenticationProvider>());
            container.Register(Component.For<IAuthenticator, IAuthenticationState>().ImplementedBy<CookiesAuthenticationState>().LifestyleTransient());

            container.Register(Component.For<IRootUserManager>().ImplementedBy<RootUserManager>());
        }

        private void RegisterServices(IWindsorContainer container)
        {
            container.Register(Component.For<ISignAuthorityService>().ImplementedBy<SignAuthorityService>());

            container.Register(Component.For<IVehicleService>().ImplementedBy<VehicleService>().LifestyleTransient());
            container.Register(Component.For<ITransportUnitService>().ImplementedBy<TransportUnitService>().LifestyleTransient());
            container.Register(Component.For<IDocumentService>().ImplementedBy<DocumentService>().LifestyleTransient());
            container.Register(Component.For<IDocumentNumberProvider>().ImplementedBy<DocumentNumberProvider>().LifestyleSingleton());
            container.Resolve<IDocumentNumberProvider>(); // Для выполнения статического конструктора

            container.Register(Component.For<IInternalDocumentService>().ImplementedBy<InternalDocumentService>().LifestyleTransient());
            container.Register(Component.For<ICorrespondenceService>().ImplementedBy<CorrespondenceService>().LifestyleTransient());
            container.Register(Component.For<IMessageService>().ImplementedBy<MessageService>().LifestyleTransient());
            container.Register(Component.For<IAdvertService>().ImplementedBy<AdvertService>().LifestyleTransient());
            container.Register(Component.For<IProductRequestService<ProductRequest>>().ImplementedBy<ProductRequestService>().LifestyleTransient());
            container.Register(Component.For<IRoutedDocumentProcessingService>().ImplementedBy<StockroomRequestService>().LifestyleTransient().Named(DocumentType.StockroomRequest.GetRegistrationName()));
            container.Register(Component.For<IRoutedDocumentProcessingService>().ImplementedBy<StockroomExpenseOrderService>().LifestyleTransient().Named(DocumentType.StockroomExpenseOrder.GetRegistrationName()));
            container.Register(Component.For<IRoutedDocumentProcessingService>().ImplementedBy<InventoryService>().LifestyleTransient().Named(DocumentType.Inventory.GetRegistrationName()));
            container.Register(Component.For<IRoutedDocumentProcessingService>().ImplementedBy<AccountingOtherService>().LifestyleTransient().Named(DocumentType.OtherAccountingDocument.GetRegistrationName()));
            container.Register(Component.For<IRoutedDocumentProcessingService>().ImplementedBy<AccountingInOutDocService>().LifestyleTransient().Named(DocumentType.AccountingInOutDocument.GetRegistrationName()));
            container.Register(Component.For<IWarehouseService>().ImplementedBy<WarehouseService>().LifestyleTransient());
            container.Register(Component.For<IStockroomProductService>().ImplementedBy<StockroomProductService>().LifestyleTransient());
            container.Register(Component.For<IOrganizationService>().ImplementedBy<OrganizationService>().LifestyleTransient());
        }

        private void RegisterCustomEntityServices(IWindsorContainer container)
        {
            container.Register(Component.For<IEntityService<Vehicle>>().ImplementedBy<VehicleEntityService>().LifestyleTransient());
            container.Register(Component.For<IEntityService<Employee>>().ImplementedBy<EmployeeEntityService>().LifestyleTransient());
            container.Register(Component.For<IEntityService<User>>().ImplementedBy<UserEntityService>().LifestyleTransient());
            container.Register(Component.For<IEntityService<Product>>().ImplementedBy<ProductEntityService>().LifestyleTransient());
            container.Register(Component.For<IEntityService<PriceList>>().ImplementedBy<PriceListEntityService>().LifestyleTransient());
            container.Register(Component.For<IEntityService<VehicleArend>>().ImplementedBy<VehicleArendEntityService>().LifestyleTransient());
            container.Register(Component.For<IEntityService<TransportUnitArend>>().ImplementedBy<TransportUnitArendEntityService>().LifestyleTransient());
            container.Register(Component.For<IEntityService<PaymentAccount>>().ImplementedBy<PaymentAccountEntityService>().LifestyleTransient());
            container.Register(Component.For<IEntityService<Warehouse>>().ImplementedBy<WarehouseEntityService>().LifestyleTransient());
            container.Register(Component.For<IEntityService<PersonalClient>>().ImplementedBy<PersonalClientEntityService>().LifestyleTransient());
            container.Register(Component.For<IEntityService<InternalDocument>>().ImplementedBy<InternalDocumentEntityService>().LifestyleTransient());
            container.Register(Component.For<IEntityService<Correspondence>>().ImplementedBy<CorrespondenceEntityService>().LifestyleTransient());
            container.Register(Component.For<IEntityService<Message>>().ImplementedBy<MessageEntityService>().LifestyleTransient());
            container.Register(Component.For<IEntityService<Advert>>().ImplementedBy<AdvertEntityService>().LifestyleTransient());
            container.Register(Component.For<IEntityService<Catalogue>>().ImplementedBy<CatalogueEntityService>().LifestyleTransient());
          //  container.Register(Component.For<IEntityService<BaseRequest>>().ImplementedBy<BaseRequestEntityService>().LifestyleTransient());
            container.Register(Component.For<IEntityService<Signature>>().ImplementedBy<SignatureEntityService>().LifestyleTransient());
            container.Register(Component.For<IEntityService<PersonalClientEvent>>().ImplementedBy<PersonalClientEventEntityService>().LifestyleTransient());

            container.Register(Component.For<IEntityService<ProductRequest>>().ImplementedBy<ProductRequestEntityService>().LifestyleTransient());
            container.Register(Component.For<IEntityService<Organization>>().ImplementedBy<OrganizationEntityService>().LifestyleTransient());

            //container.Register(Component.For<IEntityService<IncomingCorrespondence>>().ImplementedBy<BaseDocumentMovementEntityService<IncomingCorrespondence>>().LifestyleTransient());
            //container.Register(Component.For<IEntityService<OutcomingCorrespondence>>().ImplementedBy<BaseDocumentMovementEntityService<OutcomingCorrespondence>>().LifestyleTransient());
            //container.Register(Component.For<IEntityService<CorrespondenceMovement>>().ImplementedBy<BaseDocumentMovementEntityService<CorrespondenceMovement>>().LifestyleTransient());

            //container.Register(Component.For<IEntityService<IncomingInternalDocument>>().ImplementedBy<BaseDocumentMovementEntityService<IncomingInternalDocument>>().LifestyleTransient());
            //container.Register(Component.For<IEntityService<OutcomingInternalDocument>>().ImplementedBy<BaseDocumentMovementEntityService<OutcomingInternalDocument>>().LifestyleTransient());
            //container.Register(Component.For<IEntityService<InternalDocumentMovement>>().ImplementedBy<BaseDocumentMovementEntityService<InternalDocumentMovement>>().LifestyleTransient());

            //container.Register(Component.For<IEntityService<IncomingMessage>>().ImplementedBy<BaseDocumentMovementEntityService<IncomingMessage>>().LifestyleTransient());
            //container.Register(Component.For<IEntityService<OutcomingMessage>>().ImplementedBy<BaseDocumentMovementEntityService<OutcomingMessage>>().LifestyleTransient());
            //container.Register(Component.For<IEntityService<MessageMovement>>().ImplementedBy<BaseDocumentMovementEntityService<MessageMovement>>().LifestyleTransient());

            container.Register(Component.For<IEntityService<VehicleModification>>().ImplementedBy<VehicleModificationEntityService>().LifestyleTransient());
            container.Register(Component.For<IEntityService<VehicleModel>>().ImplementedBy<VehicleModelEntityService>().LifestyleTransient());
            container.Register(Component.For<IEntityService<ProductSubGroup>>().ImplementedBy<ProductSubGroupEntityService>().LifestyleTransient());
            container.Register(Component.For<IEntityService<DangerSubClass>>().ImplementedBy<DangerSubClassEntityService>().LifestyleTransient());
            container.Register(Component.For<IEntityService<PackingType>>().ImplementedBy<PackingTypeEntityServiceEntityService>().LifestyleTransient());

            container.Register(Component.For<IEntityService<StockroomBaseRequest>>().ImplementedBy<StockroomRequestEntityService>().LifestyleTransient());
            container.Register(Component.For<IEntityService<StockroomExpenseOrderBase>>().ImplementedBy<StockroomExpenseOrderEntityService>().LifestyleSingleton());
            container.Register(Component.For<IEntityService<StockroomReceiptOrderBase>>().ImplementedBy<StockroomReceiptOrderEntityService>().LifestyleTransient());
            container.Register(Component.For<IEntityService<StockroomExpenseOrderRoute>>().ImplementedBy<StockroomExpenseOrderRouteEntityService>().LifestyleTransient());
            container.Register(Component.For<IEntityService<StockroomReceiptOrderRoute>>().ImplementedBy<StockroomReceiptOrderRouteEntityService>().LifestyleTransient());
            container.Register(Component.For<IEntityService<Inventory>>().ImplementedBy<InventoryEntityService>().LifestyleTransient());
            container.Register(Component.For<IEntityService<AccountingOtherBase>>().ImplementedBy<AccountingOtherEntityService>().LifestyleTransient());
            container.Register(Component.For<IEntityService<AccountingInOutDocBase>>().ImplementedBy<AccountingInOutDocEntityService>().LifestyleTransient());

        }

        private void RegisterFileEntityServices(IWindsorContainer container)
        {
            RegisterFileEntityService<VehicleInsurance>(container);
            RegisterFileEntityService<VehicleFuel>(container);
            RegisterFileEntityService<VehicleMaintenance>(container);
            RegisterFileEntityService<VehicleSparePartService>(container);
            RegisterFileEntityService<Product>(container);
        }

        private void RegisterFileEntityService<T>(IWindsorContainer container) where T : class, IEntity 
        {
            container.Register(Component.For<IEntityService<T>>().ImplementedBy<FileFieldEntityService<T>>().LifestyleTransient());
        }

        private void RegisterContractSavers(IWindsorContainer container)
        {
            container.Register(Component.For<IContractService>().ImplementedBy<Services.Contract.Impl.MordovCement.RailwayDelivery.ContractSaver>());
        }
    }
}