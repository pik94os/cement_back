﻿using System;
using System.Net;

namespace Cement.Core.Exceptions
{
    [Serializable]
    public class ValidationException : CementException
    {
        public ValidationException(string message) : base(message)
        {
        }

        public ValidationException(string message, HttpStatusCode httpStatusCode) : base(message, httpStatusCode)
        {
        }
    }
}