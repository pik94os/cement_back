﻿using System;
using System.Net;
using System.Runtime.Serialization;

namespace Cement.Core.Exceptions
{
    using System.Security.Authentication;

    [Serializable]
    public class CementException : Exception
    {
        public CementException(string message)
            : this(message, HttpStatusCode.BadRequest)
        {

        }

        public CementException(string message, HttpStatusCode httpStatusCode)
            : base(message)
        {
            HttpStatusCode = httpStatusCode;
        }

        public HttpStatusCode HttpStatusCode { get; protected set; }

        public virtual string GetLocalizedMessage(string template)
        {
            return template;
        }

        public override void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            base.GetObjectData(info, context);

            info.AddValue("httpStatusCode", HttpStatusCode);
        }
    }

    [Serializable]
    public class FailedThresholdExceededException : AuthenticationException
    {
        public TimeSpan TryAfter { get; private set; }

        public FailedThresholdExceededException(TimeSpan tryAfter)
        {
            TryAfter = tryAfter;
        }

        public override void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            base.GetObjectData(info, context);

            info.AddValue("tryAfter", TryAfter);
        }
    }

    [Serializable]
    public class UserIsBlockedException : AuthenticationException
    {
        public UserIsBlockedException(string login)
            : base(String.Format("User with login {0} is blocked", login))
        {
        }
    }
}