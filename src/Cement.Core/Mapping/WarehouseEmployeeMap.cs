﻿using Cement.Core.Entities;

namespace Cement.Core.Mapping
{
    public class WarehouseEmployeeMap : BaseEntityMap<WarehouseEmployee>
    {
        public WarehouseEmployeeMap() : base("WAREHOUSE_EMPLOYEE")
        {
            ManyToOne(x => x.Warehouse, m => m.Column("WAREHOUSE_ID"));
            ManyToOne(x => x.Employee, m => m.Column("EMPLOYEE_ID"));
        }
    }
}