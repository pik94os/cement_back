﻿using Cement.Core.Entities;

namespace Cement.Core.Mapping
{
    public class VehicleMap : BaseOrganizationBasedEntityMap<Vehicle>
    {
        public VehicleMap() 
        {
            Property(x => x.Name);
            ManyToOne(x => x.Model, m => m.Column("MODEL_ID"));
            ManyToOne(x => x.Modification, m => m.Column("MODIFICATION_ID"));
            ManyToOne(x => x.Category, m => m.Column("CATEGORY_ID"));
            ManyToOne(x => x.SubCategory, m => m.Column("SUBCATEGORY_ID"));
            Property(x => x.ReleaseYear, m => m.Column("RELEASE_YEAR"));
            Property(x => x.Number, m => m.Column("NUM"));
            ManyToOne(x => x.NumCountryCode, m => m.Column("NUM_COUNTRY_CODE_ID"));
            ManyToOne(x => x.NumRegionCode, m => m.Column("NUM_REGION_CODE_ID"));
            ManyToOne(x => x.Photo, m => m.Column("PHOTO_FILE_ID"));

            Property(x => x.Vin);
            Property(x => x.СhassisSerialNum, m => m.Column("CHASSIS_SERIAL_NUM"));
            Property(x => x.EngineSerialNum, m => m.Column("ENGINE_SERIAL_NUM"));
            Property(x => x.Body);
            ManyToOne(x => x.WheelBase, m => m.Column("WHEEL_BASE_ID"));
            ManyToOne(x => x.DriverCategory, m => m.Column("DRIVER_CAT_ID"));
            Property(x => x.EcoClass, m => m.Column("ECO_CLASS"));
            Property(x => x.Color);
            Property(x => x.Power);
            Property(x => x.EngineVolume, m => m.Column("ENGINE_VOLUME"));
            Property(x => x.MaxMass, m => m.Column("MAX_MASS"));
            Property(x => x.VehicleMass, m => m.Column("VEHICLE_MASS"));

            Property(x => x.PassportSerie, m => m.Column("PTS_SERIE"));
            Property(x => x.PassportNumber, m => m.Column("PTS_NUMBER"));
            Property(x => x.PassportManufacturer, m => m.Column("PTS_MANUFACTURER"));
            Property(x => x.PassportDate, m => m.Column("PTS_DATE"));
            ManyToOne(x => x.PasspostScan1, m => m.Column("PTS_SCAN1_FILE_ID"));
            ManyToOne(x => x.PasspostScan2, m => m.Column("PTS_SCAN2_FILE_ID"));

            Property(x => x.DocumentSerie, m => m.Column("DOC_SERIE"));
            Property(x => x.DocumentNumber, m => m.Column("DOC_NUMBER"));
            Property(x => x.DocumentOwner, m => m.Column("DOC_OWNER"));
            Property(x => x.DocumentDate, m => m.Column("DOC_DATE"));
            ManyToOne(x => x.DocumentScan1, m => m.Column("DOC_SCAN1_FILE_ID"));
            ManyToOne(x => x.DocumentScan2, m => m.Column("DOC_SCAN2_FILE_ID"));

            ManyToOne(x => x.Class, m => m.Column("CLASS_ID"));
            Property(x => x.Type);
            ManyToOne(x => x.BodyType, m => m.Column("BODYTYPE_ID"));
            Property(x => x.Length);
            Property(x => x.Width);
            Property(x => x.Height);
            Property(x => x.IsRearLoading, m => m.Column("IS_REAR_LOADING"));
            Property(x => x.IsSideLoading, m => m.Column("IS_SIDE_LOADING"));
            Property(x => x.IsOverLoading, m => m.Column("IS_OVER_LOADING"));
            Property(x => x.Capacity);
            Property(x => x.Volume);
            ManyToOne(x => x.Option, m => m.Column("OPTION_ID"));
            Property(x => x.ExtraCharacteristic, m => m.Column("EXTRA_CHAR"));

            ManyToOne(x => x.Division, m => m.Column("DIVISION_ID"));
            ManyToOne(x => x.Column, m => m.Column("COLUMN_ID"));
            Property(x => x.GarageNumber, m => m.Column("GARAGE_NUMBER"));

            Property(x => x.NumberText, m => m.Column("NUMBER_TEXT"));
            Property(x => x.SizeText, m => m.Column("SIZE_TEXT"));
            Property(x => x.GoText, m => m.Column("GO_TEXT"));
            Property(x => x.ModelFullName, m => m.Column("MODEL_TEXT"));
        }
    }
}