﻿using Cement.Core.Entities;

namespace Cement.Core.Mapping
{
    public class PersonalClientMap : BaseOrganizationBasedEntityMap<PersonalClient>
    {
        public PersonalClientMap() : base("PERSONAL_CLIENT")
        {
            ManyToOne(x => x.Client, m => m.Column("CLIENT_ID"));
            ManyToOne(x => x.Employee, m => m.Column("EMPLOYEE_ID"));
            Property(x => x.IsCorporate, m => m.Column("IS_CORPORATE"));
            Property(x => x.DateFirstContract, m => m.Column("DATE_FIRST_CONTRACT"));
            Property(x => x.DateLastContract, m => m.Column("DATE_LAST_CONTRACT"));
            Property(x => x.ArchivedAt, m => m.Column("ARCHIVED_AT"));

            ManyToOne(x => x.State, m => m.Column("STATE_ID"));
            ManyToOne(x => x.Group, m => m.Column("GROUP_ID"));
            ManyToOne(x => x.Subgroup, m => m.Column("SUBGROUP_ID"));
            Property(x => x.From, m => m.Column("CLIENT_FROM"));
            Property(x => x.Comment, m => m.Column("COMMENT_TEXT"));
        }
    }
}