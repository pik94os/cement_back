﻿using Cement.Core.Entities;

namespace Cement.Core.Mapping
{
    public class ProductPersonalMap : BaseOrganizationBasedEntityMap<ProductPersonal>
    {
        public ProductPersonalMap() : base("PRODUCT_PERSONAL")
        {
            Property(x => x.ArchiveDate, m => m.Column("ARCHIVE_DATE"));
            ManyToOne(x => x.Product, m => m.Column("PRODUCT_ID"));
        }
    }
}