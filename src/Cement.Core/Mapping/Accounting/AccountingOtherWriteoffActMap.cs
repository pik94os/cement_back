﻿using Cement.Core.Entities;
using NHibernate.Mapping.ByCode.Conformist;

namespace Cement.Core.Mapping.Accounting
{
    public class AccountingOtherWriteoffActMap : SubclassMapping<AccountingOtherWriteoffAct>
    {
        public AccountingOtherWriteoffActMap()
        {
            DiscriminatorValue((int)AccountingOtherDocumentType.WriteoffAct);
            Property(x => x.AccountingOtherWriteoffActKind, m => m.Column("KIND"));
        }
    }
}