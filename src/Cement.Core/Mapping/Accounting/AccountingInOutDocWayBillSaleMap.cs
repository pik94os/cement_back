﻿using Cement.Core.Entities;
using NHibernate.Mapping.ByCode.Conformist;

namespace Cement.Core.Mapping.Accounting
{
    public class AccountingInOutDocWayBillSaleMap : SubclassMapping<AccountingInOutDocWayBillSale>
    {
        public AccountingInOutDocWayBillSaleMap()
        {
            DiscriminatorValue((int)AccountingInOutDocType.WayBillSale);

            Property(x => x.UsedAt, m => m.Column("USED_AT"));
            Property(x => x.AccountingInOutDocWayBillSaleKind, m => m.Column("KIND"));
            Property(x => x.AccountingInOutDocWayBillSaleShippingKind, m => m.Column("SHIPPING_KIND"));
        }
    }
}