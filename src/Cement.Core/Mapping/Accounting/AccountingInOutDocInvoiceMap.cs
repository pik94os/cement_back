﻿using Cement.Core.Entities;
using NHibernate.Mapping.ByCode.Conformist;

namespace Cement.Core.Mapping.Accounting
{
    public class AccountingInOutDocInvoiceMap : SubclassMapping<AccountingInOutDocInvoice>
    {
        public AccountingInOutDocInvoiceMap()
        {
            DiscriminatorValue((int)AccountingInOutDocType.Invoice);

            ManyToOne(x => x.AccountingInOutDocWayBill, map =>
            {
                map.Column("ACCOUNTING_INOUT_DOC_ID");
                map.NotNullable(true);
            });
        }
    }
}