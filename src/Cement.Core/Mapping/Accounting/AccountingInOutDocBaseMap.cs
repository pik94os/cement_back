﻿using Cement.Core.Entities;

namespace Cement.Core.Mapping.Accounting
{
    public class AccountingInOutDocRouteMap : DocumentRouteMap<AccountingInOutDocBase, AccountingInOutDocRoute>
    {
        public AccountingInOutDocRouteMap() : base("ACCOUNTING_INOUT_DOC_ROUTE", "ACCOUNTING_INOUT_DOC_ID") { }
    }

    public class AccountingInOutDocBaseMap : BaseEntityMap<AccountingInOutDocBase>
    {
        public AccountingInOutDocBaseMap()
            : base("ACCOUNTING_INOUT_DOC")
        {
            Discriminator(x =>
            {
                x.Column("TYPE");
                x.NotNullable(true);
            });

            Property(x => x.AccountingInOutDocType,
                m =>
                {
                    m.Column("TYPE");
                    m.Insert(false);
                    m.Update(false);
                });

            Property(x => x.No);
            Property(x => x.Date, m => m.Column("DOCUMENT_DATE"));
            Property(x => x.DocumentState, m => m.Column("STATE"));
            Property(x => x.Sum, m => m.Column("TOTAL_SUM"));

            ManyToOne(x => x.OrganizationFrom, map =>
            {
                map.Column("ORGANIZATION_FROM_ID");
                map.Update(false);
                map.NotNullable(true);
            });

            ManyToOne(x => x.OrganizationTo, map =>
            {
                map.Column("ORGANIZATION_TO_ID");
                map.NotNullable(true);
            });
        }
    }
}