﻿using Cement.Core.Entities;
using NHibernate.Mapping.ByCode.Conformist;

namespace Cement.Core.Mapping.Accounting
{
    public class AccountingOtherPostingActMap : SubclassMapping<AccountingOtherPostingAct>
    {
        public AccountingOtherPostingActMap()
        {
            DiscriminatorValue((int)AccountingOtherDocumentType.PostingAct);
        }
    }
}