﻿using Cement.Core.Entities;
using NHibernate.Mapping.ByCode.Conformist;

namespace Cement.Core.Mapping.Accounting
{
    public class AccountingOtherMovementBillMap : SubclassMapping<AccountingOtherMovementBill>
    {
        public AccountingOtherMovementBillMap()
        {
            DiscriminatorValue((int)AccountingOtherDocumentType.MovementBill);

            Property(x => x.AccountingOtherMovementBillKind, m => m.Column("KIND"));
        }
    }
}