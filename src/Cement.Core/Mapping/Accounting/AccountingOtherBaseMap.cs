﻿using Cement.Core.Entities;

namespace Cement.Core.Mapping.Accounting
{

    public class AccountingOtherRouteMap : DocumentRouteMap<AccountingOtherBase, AccountingOtherRoute>
    {
        public AccountingOtherRouteMap() : base("ACCOUNTING_OTHER_ROUTE", "ACCOUNTING_OTHER_ID") { }
    }

    public class AccountingOtherBaseMap : BaseEntityMap<AccountingOtherBase>
    {
        public AccountingOtherBaseMap() : base("ACCOUNTING_OTHER")
        {
            Discriminator(x =>
            {
                x.Column("TYPE");
                x.NotNullable(true);
            });

            Property(x => x.AccountingOtherDocumentType,
                m =>
                {
                    m.Column("TYPE");
                    m.Insert(false);
                    m.Update(false);
                });

            Property(x => x.No);
            Property(x => x.Date, m => m.Column("DOCUMENT_DATE"));
            Property(x => x.DocumentState, m => m.Column("STATE"));
            Property(x => x.Sum, m => m.Column("TOTAL_SUM"));
        }
    }
}