﻿using Cement.Core.Entities;
using NHibernate.Mapping.ByCode.Conformist;

namespace Cement.Core.Mapping.Accounting
{
    public class AccountingInOutDocWayBillMap : SubclassMapping<AccountingInOutDocWayBill>
    {
        public AccountingInOutDocWayBillMap()
        {
            DiscriminatorValue((int)AccountingInOutDocType.WorkCompleteAct);  
        }
    }
}