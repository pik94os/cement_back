﻿using Cement.Core.Entities;

namespace Cement.Core.Mapping
{
    public class AdvertMap : BaseOrganizationBasedEntityMap<Advert>
    {
        public AdvertMap() 
        {
            Property(x => x.Name);
            Property(x => x.Date, m => m.Column("DATE_SHOW"));
            Property(x => x.Subject);
            Property(x => x.Content);
            ManyToOne(x => x.Author, m => m.Column("AUTHOR_ID"));
            ManyToOne(x => x.Creator, m => m.Column("CREATOR_ID"));
            Property(x => x.Importance);
            Property(x => x.Secrecy);
            ManyToOne(x => x.File, m => m.Column("FILE_ID"));
            Property(x => x.DocumentState, m => m.Column("STATE"));
        }
    }
}