﻿using Cement.Core.Entities;

namespace Cement.Core.Mapping
{
    public class VehicleArendMap : BaseEntityMap<VehicleArend>
    {
        public VehicleArendMap() : base("VEHICLE_AREND")
        {
            ManyToOne(x => x.Vehicle, m => m.Column("VEHICLE_ID"));
            ManyToOne(x => x.Arendator, m => m.Column("CLIENT_ID"));

            Property(x => x.DateStart, m => m.Column("DATE_START"));
            Property(x => x.DateEnd, m => m.Column("DATE_END"));
            Property(x => x.IsActive, m => m.Column("IS_ACTIVE"));
        }
    }
}