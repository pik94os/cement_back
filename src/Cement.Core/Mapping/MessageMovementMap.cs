﻿using Cement.Core.Entities;
using Cement.Core.Enums;
using NHibernate.Mapping.ByCode.Conformist;

namespace Cement.Core.Mapping
{
    public class MessageMovementMap : BaseEntityMap<MessageMovement>
    {
        public MessageMovementMap() : base("MESSAGE_MOVEMENT")
        {
            Discriminator(x =>
            {
                x.Column("TYPE");
                x.NotNullable(true);
            });

            Property(x => x.MovementType,
                m =>
                {
                    m.Column("TYPE");
                    m.Insert(false);
                    m.Update(false);
                });

            ManyToOne(x => x.Document, m => m.Column("DOCUMENT_ID"));
            ManyToOne(x => x.Employee, m => m.Column("EMPLOYEE_ID"));
            Property(x => x.IsArchived, m => m.Column("IS_ARCHIVED"));
            ManyToOne(x => x.Route, m => m.Column("ROUTE_ID"));
        }
    }

    public class IncomingMessageMap : SubclassMapping<IncomingMessage>
    {
        public IncomingMessageMap()
        {
            DiscriminatorValue((int)DocumentMovementType.Incoming);
        }
    }

    public class OutcomingMessageMap : SubclassMapping<OutcomingMessage>
    {
        public OutcomingMessageMap()
        {
            DiscriminatorValue((int)DocumentMovementType.Outcoming);
        }
    }
}