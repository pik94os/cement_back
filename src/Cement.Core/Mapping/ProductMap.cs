﻿using Cement.Core.Entities;

namespace Cement.Core.Mapping
{
    public class ProductMap : BaseEntityMap<Product>
    {
        public ProductMap() 
        {
            Property(x => x.Type);
            Property(x => x.Name);
            ManyToOne(x => x.Group, m => m.Column("GROUP_ID"));
            ManyToOne(x => x.SubGroup, m => m.Column("SUB_GROUP_ID"));
            Property(x => x.Brand);
            ManyToOne(x => x.Country, m => m.Column("COUNTRY_ID"));
            ManyToOne(x => x.Manufacturer, m => m.Column("MANUFACTURER_ID"));
            ManyToOne(x => x.Packer, m => m.Column("PACKER_ID"));
            ManyToOne(x => x.Importer, m => m.Column("IMPORTER_ID"));
            ManyToOne(x => x.Photo, m => m.Column("PHOTO_FILE_ID"));
            Property(x => x.Okp);
            Property(x => x.TnVed);
            Property(x => x.CargoTrainCode, m => m.Column("CARGO_TRAIN_CODE"));
            Property(x => x.Barcode);
            Property(x => x.Gost);
            Property(x => x.GostR);
            Property(x => x.Ost);
            Property(x => x.Stp);
            Property(x => x.Okun);
            Property(x => x.Tu);
            Property(x => x.Article);
            ManyToOne(x => x.MeasureUnit, m => m.Column("MEASURE_UNIT_ID"));
            Property(x => x.ConformityCertificateNumber, m => m.Column("CONFORM_CERT_NUM"));
            ManyToOne(x => x.ConformityCertificateScan, m => m.Column("CONFORM_CERT_FILE_ID"));
            ManyToOne(x => x.ConformityCertificateScan2, m => m.Column("CONFORM_CERT_FILE2_ID"));
            Property(x => x.ConformityCertificateStartDate, m => m.Column("CONFORM_CERT_START"));
            Property(x => x.ConformityCertificateEndDate, m => m.Column("CONFORM_CERT_END"));
            Property(x => x.ConformityDeclarationNumber, m => m.Column("CONFORM_DECL_NUM"));
            ManyToOne(x => x.ConformityDeclarationScan, m => m.Column("CONFORM_DECL_FILE_ID"));
            Property(x => x.ConformityDeclarationStartDate, m => m.Column("CONFORM_DECL_START"));
            Property(x => x.ConformityDeclarationEndDate, m => m.Column("CONFORM_DECL_END"));
            Property(x => x.HealthCertificateNumber, m => m.Column("HEALTH_CERT_NUM"));
            ManyToOne(x => x.HealthCertificateScan, m => m.Column("HEALTH_CERT_FILE_ID"));
            Property(x => x.HealthCertificateStartDate, m => m.Column("HEALTH_CERT_START"));
            Property(x => x.HealthCertificateEndDate, m => m.Column("HEALTH_CERT_END"));
            Property(x => x.FireSafetyCertificateNumber, m => m.Column("FIRE_CERT_NUM"));
            ManyToOne(x => x.FireSafetyCertificateScan, m => m.Column("FIRE_CERT_FILE_ID"));
            Property(x => x.FireSafetyCertificateStartDate, m => m.Column("FIRE_CERT_START"));
            Property(x => x.FireSafetyCertificateEndDate, m => m.Column("FIRE_CERT_END"));
            Property(x => x.FireSafetyDeclarationNumber, m => m.Column("FIRE_DECL_NUM"));
            ManyToOne(x => x.FireSafetyDeclarationScan, m => m.Column("FIRE_DECL_FILE_ID"));
            Property(x => x.FireSafetyDeclarationStartDate, m => m.Column("FIRE_DECL_START"));
            Property(x => x.FireSafetyDeclarationEndDate, m => m.Column("FIRE_DECL_END"));
            ManyToOne(x => x.DangerClass, m => m.Column("DNGR_CLASS_ID"));
            ManyToOne(x => x.DangerSubClass, m => m.Column("DNGR_SUBCLASS_ID"));
            ManyToOne(x => x.DangerUnCode, m => m.Column("DNGR_UN_CODE_ID"));
            Property(x => x.Kind);
            ManyToOne(x => x.TypeSingle, m => m.Column("TYPE_SINGLE_ID"));
            ManyToOne(x => x.MaterialSingle, m => m.Column("MATERIAL_SINGLE_ID"));
            Property(x => x.VolumeSingle, m => m.Column("VOLUME_SINGLE"));
            ManyToOne(x => x.VolumeUnitSingle, m => m.Column("VOLUME_UNIT_SINGLE_ID"));
            Property(x => x.WeightSingle, m => m.Column("WEIGHT_SINGLE"));
            ManyToOne(x => x.WeightUnitSingle, m => m.Column("WEIGHT_UNIT_SINGLE_ID"));
            Property(x => x.LengthSingle, m => m.Column("LENGTH_SINGLE"));
            Property(x => x.WidthSingle, m => m.Column("WIDTH_SINGLE"));
            Property(x => x.HeightSingle, m => m.Column("HEIGHT_SINGLE"));
            ManyToOne(x => x.SizeUnitSingle, m => m.Column("SIZE_UNIT_SINGLE_ID"));
            Property(x => x.UnitsCountGroup, m => m.Column("UNIT_COUNT_GROUP"));
            ManyToOne(x => x.TypeGroup, m => m.Column("TYPE_GROUP_ID"));
            ManyToOne(x => x.MaterialGroup, m => m.Column("MATERIAL_GROUP_ID"));
            Property(x => x.VolumeGroup, m => m.Column("VOLUME_GROUP"));
            ManyToOne(x => x.VolumeUnitGroup, m => m.Column("VOLUME_UNIT_GROUP_ID"));
            Property(x => x.WeightGroup, m => m.Column("WEIGHT_GROUP"));
            ManyToOne(x => x.WeightUnitGroup, m => m.Column("WEIGHT_UNIT_GROUP_ID"));
            Property(x => x.LengthGroup, m => m.Column("LENGTH_GROUP"));
            Property(x => x.WidthGroup, m => m.Column("WIDTH_GROUP"));
            Property(x => x.HeightGroup, m => m.Column("HEIGHT_GROUP"));
            ManyToOne(x => x.SizeUnitGroup, m => m.Column("SIZE_UNIT_GROUP_ID"));
            Property(x => x.UnitsCountTransport, m => m.Column("UNIT_COUNT_TRN"));
            ManyToOne(x => x.TypeTransport, m => m.Column("TYPE_TRN_ID"));
            ManyToOne(x => x.MaterialTransport, m => m.Column("MATERIAL_TRN_ID"));
            Property(x => x.VolumeTransport, m => m.Column("VOLUME_TRN"));
            ManyToOne(x => x.VolumeUnitTransport, m => m.Column("VOLUME_UNIT_TRN_ID"));
            Property(x => x.WeightTransport, m => m.Column("WEIGHT_TRN"));
            ManyToOne(x => x.WeightUnitTransport, m => m.Column("WEIGHT_UNIT_TRN_ID"));
            Property(x => x.LengthTransport, m => m.Column("LENGTH_TRN"));
            Property(x => x.WidthTransport, m => m.Column("WIDTH_TRN"));
            Property(x => x.HeightTransport, m => m.Column("HEIGHT_TRN"));
            ManyToOne(x => x.SizeUnitTransport, m => m.Column("SIZE_UNIT_TRN_ID"));
            Property(x => x.PackageDisplayString, m => m.Column("PKG_DISPLAY_STRING"));
            Property(x => x.ComplectString, m => m.Column("COMPLECT_STRING"));
            Property(x => x.PackageJsonBytes, m => m.Column("PACKAGE_JSON"));
            Property(x => x.IsModerated, m => m.Column("IS_MODERATED"));
        }
    }
}