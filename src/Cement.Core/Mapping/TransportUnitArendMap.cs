﻿using Cement.Core.Entities;

namespace Cement.Core.Mapping
{
    public class TransportUnitArendMap : BaseEntityMap<TransportUnitArend>
    {
        public TransportUnitArendMap() : base("TRANSPORT_UNIT_AREND")
        {
            ManyToOne(x => x.TransportUnit, m => m.Column("TRANSPORT_UNIT_ID"));
            ManyToOne(x => x.Arendator, m => m.Column("CLIENT_ID"));

            Property(x => x.DateStart, m => m.Column("DATE_START"));
            Property(x => x.DateEnd, m => m.Column("DATE_END"));
            Property(x => x.IsActive, m => m.Column("IS_ACTIVE"));
        }
    }
}