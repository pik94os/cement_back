﻿using Cement.Core.Entities;

namespace Cement.Core.Mapping
{
    class ClientStateMap : BaseOrganizationBasedEntityMap<ClientState>
    {
        public ClientStateMap() : base("CLIENT_STATE")
        {
            Property(x => x.Name);
            Property(x => x.Description);
        }
    }
}