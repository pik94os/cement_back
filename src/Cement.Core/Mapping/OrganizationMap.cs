﻿using Cement.Core.Entities;
using NHibernate.Mapping.ByCode.Conformist;

namespace Cement.Core.Mapping
{
    //public class OrganizationMap : BaseEntityMap<Organization>
    //{
    //    public OrganizationMap() 
    //    {
    //        ManyToOne(x => x.Client, m => m.Column("CLIENT_ID"));
    //        Property(x => x.Name);
    //    }
    //}

    public class OrganizationMap : ClassMapping<Organization>
    {
        public OrganizationMap()
        {
            Table("ORGANIZATION");

            // Делаем неявное наследование от клиента
            // Идентификаторы клиента и организации будут совпадать
            Id(x => x.Id, map =>
            {
                map.Column("ID");
                map.Generator(NHibernate.Mapping.ByCode.Generators.Assigned);
            });

            ManyToOne(x => x.Client, m => m.Column("CLIENT_ID"));
            Property(x => x.Name);
            ManyToOne(x => x.Logo, m => m.Column("LOGO_FILE_ID"));
        }
    }
}