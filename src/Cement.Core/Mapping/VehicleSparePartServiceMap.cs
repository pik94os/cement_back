﻿using Cement.Core.Entities;

namespace Cement.Core.Mapping
{
    public class VehicleSparePartServiceMap : BaseEntityMap<VehicleSparePartService>
    {
        public VehicleSparePartServiceMap() : base("VEHICLE_SPAREPART_SERVICE")
        {
            Property(x => x.Date, m => m.Column("DATE_FIX"));
            Property(x => x.Mileage);
            Property(x => x.Name);
            Property(x => x.Master, m => m.Column("REPAIR_MASTER"));
            Property(x => x.Amount);
            Property(x => x.Cost);
            Property(x => x.DateStart, m => m.Column("DATE_START"));
            Property(x => x.DateEnd, m => m.Column("DATE_END"));
            Property(x => x.DocName, m => m.Column("DOCUMENT"));

            ManyToOne(x => x.Driver, m => m.Column("DRIVER_ID"));
            ManyToOne(x => x.Vehicle, m => m.Column("VEHICLE_ID"));
            ManyToOne(x => x.CarService, m => m.Column("CAR_SERVICE_ID"));
            ManyToOne(x => x.MeasureUnit, m => m.Column("MEASURE_UNIT_ID"));
            ManyToOne(x => x.DocFile, m => m.Column("DOCUMENT_ID"));

        }
    }
}