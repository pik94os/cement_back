﻿using Cement.Core.Entities;

namespace Cement.Core.Mapping
{
    public class WarehouseMap : BaseOrganizationBasedEntityMap<Warehouse>
    {
        public WarehouseMap() 
        {
            ManyToOne(x => x.DrivewaysOwner, m => m.Column("WAYSOWNER_ID"));
            Property(x => x.Name);
            Property(x => x.Type);
            Property(x => x.TypeStr, m => m.Column("TYPE_STR"));
            Property(x => x.IsOwnDriveways, m => m.Column("IS_OWN_DRIVEWAYS"));

            Property(x => x.AddressIndex, m => m.Column("ADDRESS_POSTAL_CODE"));
            Property(x => x.AddressCountry, m => m.Column("ADDRESS_COUNTRY"));
            Property(x => x.AddressRegion, m => m.Column("ADDRESS_REGION"));
            Property(x => x.AddressArea, m => m.Column("ADDRESS_AREA"));
            Property(x => x.AddressLocality, m => m.Column("ADDRESS_LOCALITY"));
            Property(x => x.AddressStreet, m => m.Column("ADDRESS_STREET"));
            Property(x => x.AddressHouse, m => m.Column("ADDRESS_HOUSE"));
            Property(x => x.AddressHousing, m => m.Column("ADDRESS_HOUSING"));
            Property(x => x.AddressBuilding, m => m.Column("ADDRESS_BUILDING"));
            Property(x => x.AddressOffice, m => m.Column("ADDRESS_OFFICE"));
            Property(x => x.AddressComment, m => m.Column("ADDRESS_COMMENT"));
            Property(x => x.Address);
            Property(x => x.Latitude);
            Property(x => x.Longitude);

            Property(x => x.CompanyCode, m => m.Column("COMPANY_CODE"));
            ManyToOne(x => x.RailwayStation, m => m.Column("STATION_ID"));
        }
    }
}