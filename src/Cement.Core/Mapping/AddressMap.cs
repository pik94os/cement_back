﻿using Cement.Core.Entities;

namespace Cement.Core.Mapping
{
    public class AddressMap : BaseEntityMap<Address>
    {
        public AddressMap()
        {
            Property(x => x.Index, m => m.Column("POSTALCODE"));
            Property(x => x.Country);
            Property(x => x.Region);
            Property(x => x.Area);
            Property(x => x.Locality);
            Property(x => x.Street);
            Property(x => x.House);
            Property(x => x.Office);
            Property(x => x.Apartment);
            Property(x => x.Comment);
        }
    }
}