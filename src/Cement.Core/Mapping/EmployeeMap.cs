﻿using Cement.Core.Entities;

namespace Cement.Core.Mapping
{
    class EmployeeMap : BaseOrganizationBasedEntityMap<Employee>
    {
        public EmployeeMap()
        {
            #region Общие сведения

            Property(x => x.Name);
            Property(x => x.Surname);
            Property(x => x.FirstName);
            Property(x => x.Patronymic);
            ManyToOne(x => x.Position, m => m.Column("POSITION_ID"));
            ManyToOne(x => x.Department, m => m.Column("DEPARTMENT_ID"));
            Property(x => x.Inn);
            ManyToOne(x => x.Photo, m => m.Column("PHOTO_FILE_ID"));
            
            #endregion Общие сведения

            #region Паспорт

            Property(x => x.PassportSerie, m => m.Column("PASSPORT_SERIE"));
            Property(x => x.PassportNo, m => m.Column("PASSPORT_NO"));
            Property(x => x.PassportGivenPlace, m => m.Column("PASSPORT_GIVEN_PLACE"));
            Property(x => x.PassportDate, m => m.Column("PASSPORT_DATE"));
            Property(x => x.BirthDate, m => m.Column("BIRTH_DATE"));
            Property(x => x.BirthPlace, m => m.Column("BIRTH_PLACE"));
            
            #endregion Паспорт

            #region Водительское удостоверение

            Property(x => x.DriveLicSerie, m => m.Column("DRIVELIC_SERIE"));
            Property(x => x.DriveLicNo, m => m.Column("DRIVELIC_NO"));
            Property(x => x.DriveLicGivenPlace, m => m.Column("DRIVELIC_GIVEN_PLACE"));
            Property(x => x.DriveLicDate, m => m.Column("DRIVELIC_DATE"));
            Property(x => x.DriveLicValidDate, m => m.Column("DRIVELIC_VALID_DATE"));
            ManyToOne(x => x.DriveExperience, m => m.Column("DRIVE_EXPERIENCE_ID"));
            
            #endregion Водительское удостоверение

            #region Удостоверение тракториста

            Property(x => x.TractorDriveLicSerie, m => m.Column("TRACTDRIVELIC_SERIE"));
            Property(x => x.TractorDriveLicNo, m => m.Column("TRACTDRIVELIC_NO"));
            Property(x => x.TractorDriveLicCode, m => m.Column("TRACTDRIVELIC_CODE"));
            Property(x => x.TractorDriveLicDate, m => m.Column("TRACTDRIVELIC_DATE"));
            Property(x => x.TractorDriveLicValidDate, m => m.Column("TRACTDRIVELIC_VALID_DATE"));

            #endregion Удостоверение тракториста

            #region Прописка

            Property(x => x.Index, m => m.Column("POSTAL_CODE"));
            Property(x => x.Country);
            Property(x => x.Region);
            Property(x => x.Area);
            Property(x => x.Locality);
            Property(x => x.Street);
            Property(x => x.House);
            Property(x => x.Apartment);
            
            #endregion Прописка

            #region Связь

            Property(x => x.Phone1);
            Property(x => x.Phone2);
            Property(x => x.Phone3);
            Property(x => x.Fax);
            Property(x => x.MobilePhone1, m => m.Column("MOBILE_PHONE1"));
            Property(x => x.MobilePhone2, m => m.Column("MOBILE_PHONE2"));
            Property(x => x.MobilePhone3, m => m.Column("MOBILE_PHONE3"));
            Property(x => x.Email1);
            Property(x => x.Email2);
            Property(x => x.Email3);
            
            #endregion Связь
        }
    }
}
