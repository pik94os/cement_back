﻿using Cement.Core.Entities;

namespace Cement.Core.Mapping
{
    public class TransportUnitMap : BaseOrganizationBasedEntityMap<TransportUnit>
    {
        public TransportUnitMap() : base("TRANSPORT_UNIT")
        {
            Property(x => x.Name);
            Property(x => x.DateStart, m => m.Column("DATE_START"));

            ManyToOne(x => x.Vehicle, m => m.Column("VEHICLE_ID"));
            ManyToOne(x => x.Trailer, m => m.Column("TRAILER_ID"));
            ManyToOne(x => x.Driver, m => m.Column("DRIVER_ID"));
        }
    }
}