﻿using Cement.Core.Entities;

namespace Cement.Core.Mapping
{
    public class VehicleInsuranceMap : BaseEntityMap<VehicleInsurance>
    {
        public VehicleInsuranceMap() : base("VEHICLE_INSURANCE")
        {
            ManyToOne(x => x.Vehicle, m => m.Column("VEHICLE_ID"));
            ManyToOne(x => x.Document, m => m.Column("DOCUMENT_ID"));

            Property(x => x.Name);
            Property(x => x.No);
            ManyToOne(x => x.InsuranceCompany, m => m.Column("INSURANCE_COMPANY_ID"));
            Property(x => x.ContactPerson, m => m.Column("CONTACT_PERSON"));
            Property(x => x.Phone);
            Property(x => x.DateStart, m => m.Column("DATE_START"));
            Property(x => x.DateEnd, m => m.Column("DATE_END"));
            Property(x => x.InsuranceSum, m => m.Column("INSURANCE_SUM"));
        }
    }
}