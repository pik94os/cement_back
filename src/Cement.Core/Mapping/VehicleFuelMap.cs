﻿using Cement.Core.Entities;

namespace Cement.Core.Mapping
{
    public class VehicleFuelMap : BaseEntityMap<VehicleFuel>
    {
        public VehicleFuelMap() : base("VEHICLE_FUEL")
        {
            ManyToOne(x => x.Vehicle, m => m.Column("VEHICLE_ID"));
            ManyToOne(x => x.DocFile, m => m.Column("DOCUMENT_ID"));

            Property(x => x.Date, m => m.Column("DATE_FIX"));
            Property(x => x.Name);
            ManyToOne(x => x.FuelStation, m => m.Column("FUEL_STATION_ID"));
            ManyToOne(x => x.MeasureUnit, m => m.Column("MEASURE_UNIT_ID"));
            Property(x => x.Amount);
            Property(x => x.Cost);
            ManyToOne(x => x.Driver, m => m.Column("DRIVER_ID"));
            Property(x => x.DocName, m => m.Column("DOCUMENT"));
        }
    }
}