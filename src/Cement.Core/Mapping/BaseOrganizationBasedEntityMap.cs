﻿using Cement.Core.Entities;

namespace Cement.Core.Mapping
{
    public abstract class BaseOrganizationBasedEntityMap<T> : BaseEntityMap<T> where T : BaseOrganizationBasedEntity
    {
        protected BaseOrganizationBasedEntityMap(string tableName = null) : base(tableName)
        {
            ManyToOne(x => x.Organization, m => m.Column("ORGANIZATION_ID"));
        }
    }
}