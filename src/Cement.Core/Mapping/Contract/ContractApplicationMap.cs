﻿using Cement.Core.Entities;
using Cement.Core.Enums;
using NHibernate.Mapping.ByCode;

namespace Cement.Core.Mapping
{
    public class ContractApplicationMap : BaseJoinedSubclassMap<ContractApplication>
    {
        public ContractApplicationMap() : base((int)ContractAnnexType.Application, "CONTRACT_APPLICATION")
        {
            
        }

        protected override void SplitMapping(IJoinMapper<ContractApplication> map)
        {
            
        }
    }
}