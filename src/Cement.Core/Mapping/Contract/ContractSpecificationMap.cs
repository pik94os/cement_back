﻿using Cement.Core.Entities;
using Cement.Core.Enums;
using NHibernate.Mapping.ByCode;

namespace Cement.Core.Mapping
{
    public class ContractSpecificationMap : BaseJoinedSubclassMap<ContractSpecification>
    {
        public ContractSpecificationMap()
            : base((int)ContractAnnexType.Specification, "CONTRACT_SPECIFICATION")
        {

        }

        protected override void SplitMapping(IJoinMapper<ContractSpecification> map)
        {

        }
    }
}