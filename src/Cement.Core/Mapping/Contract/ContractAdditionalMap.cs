﻿using Cement.Core.Entities;
using Cement.Core.Enums;
using NHibernate.Mapping.ByCode;

namespace Cement.Core.Mapping
{
    public class ContractAdditionalMap : BaseJoinedSubclassMap<ContractAdditional>
    {
        public ContractAdditionalMap()
            : base((int)ContractAnnexType.Additional, "CONTRACT_ADDITIONAL")
        {

        }

        protected override void SplitMapping(IJoinMapper<ContractAdditional> map)
        {

        }
    }
}