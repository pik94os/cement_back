﻿using Cement.Core.Entities;

namespace Cement.Core.Mapping
{
    public class ContractMap : BaseEntityMap<Contract>
    {
        public ContractMap()
        {
            Property(x => x.Date, m => m.Column("DATE_CONTRACT"));
            Property(x => x.No);
            Property(x => x.Name);
            ManyToOne(x => x.Organization, m => m.Column("ORGANIZATION_ID"));
            ManyToOne(x => x.Client, m => m.Column("CLIENT_ID"));
        }
    }
}