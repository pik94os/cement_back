﻿using Cement.Core.Entities;

namespace Cement.Core.Mapping
{
    public class ContractAnnexBaseMap : BaseEntityMap<ContractAnnexBase>
    {
        public ContractAnnexBaseMap() : base("CONTRACT_ANNEX")
        {
            Discriminator(x =>
            {
                x.Column("TYPE");
                x.NotNullable(true);
            });

            Property(x => x.Date, m => m.Column("DATE_DOCUMENT"));
            Property(x => x.No);
            Property(x => x.Name);
            Property(x => x.SubType);
            ManyToOne(x => x.Contract, m => m.Column("CONTRACT_ID"));

            Property(x => x.Type, m =>
                {
                    m.Insert(false);
                    m.Update(false);
                });

            Property(x => x.TermsOfPaymentText, m => m.Column("PAY_TERM_TEXT"));
            Property(x => x.TermsOfPaymentPayKindVal, m => m.Column("PAY_TERM_KIND"));
            Property(x => x.TermsOfPaymentDaysKindVal, m => m.Column("PAY_TERM_DAYS_KIND"));
            Property(x => x.TermsOfPaymentWorkDays, m => m.Column("PAY_TERM_WORK_DAYS"));
            Property(x => x.TermsOfPaymentBankDays, m => m.Column("PAY_TERM_BANK_DAYS"));
            Property(x => x.TermsOfPaymentNextMonthDay, m => m.Column("PAY_TERM_NEXTMNTHDAY"));
            Property(x => x.TermsOfPaymentDayNumberKindVal, m => m.Column("PAY_TERM_DAYNUMKIND"));
            Property(x => x.TermsOfPaymentDayNumberDay, m => m.Column("PAY_TERM_DAYNUMDAY"));
            Property(x => x.TermsOfPaymentDayNumberFrom, m => m.Column("PAY_TERM_DAYNUMFROM"));
            Property(x => x.TermsOfPaymentDayNumberTo, m => m.Column("PAY_TERM_DAYNUMTO"));
        }
    }
}