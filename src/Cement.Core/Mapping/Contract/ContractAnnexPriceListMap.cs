﻿using Cement.Core.Entities;

namespace Cement.Core.Mapping
{
    public class ContractAnnexPriceListMap : BaseEntityMap<ContractAnnexPriceList>
    {
        public ContractAnnexPriceListMap() : base("CONTRACT_ANNEX_PRICELIST")
        {
            ManyToOne(x => x.Contract, m => m.Column("CONTRACT_ID"));
            ManyToOne(x => x.ContractAnnex, m => m.Column("CONTRACT_ANNEX_ID"));
            ManyToOne(x => x.PriceList, m => m.Column("PRICELIST_ID"));
            Property(x => x.Type);
        }
    }
}