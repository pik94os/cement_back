﻿using Cement.Core.Entities;
using Cement.Core.Enums;
using NHibernate.Mapping.ByCode.Conformist;

namespace Cement.Core.Mapping
{
    public class ContractAnnexMovementMap : BaseEntityMap<ContractAnnexMovement>
    {
        public ContractAnnexMovementMap()
            : base("CONTRACT_ANNEX_MOVEMENT")
        {
            Discriminator(x =>
            {
                x.Column("TYPE");
                x.NotNullable(true);
            });

            Property(x => x.MovementType,
                m =>
                {
                    m.Column("TYPE");
                    m.Insert(false);
                    m.Update(false);
                });

            ManyToOne(x => x.Document, m => m.Column("DOCUMENT_ID"));
            ManyToOne(x => x.Employee, m => m.Column("EMPLOYEE_ID"));
            ManyToOne(x => x.Organization, m => m.Column("ORGANIZATION_ID"));
            Property(x => x.IsArchived, m => m.Column("IS_ARCHIVED"));
        }
    }

    public class IncomingContractAnnexMap : SubclassMapping<IncomingContractAnnex>
    {
        public IncomingContractAnnexMap()
        {
            DiscriminatorValue((int)DocumentMovementType.Incoming);
        }
    }

    public class OutcomingContractAnnexMap : SubclassMapping<OutcomingContractAnnex>
    {
        public OutcomingContractAnnexMap()
        {
            DiscriminatorValue((int)DocumentMovementType.Outcoming);
        }
    }
}