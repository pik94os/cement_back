﻿using Cement.Core.Entities;

namespace Cement.Core.Mapping
{
    public class NomenclatureMap : BaseOrganizationBasedEntityMap<Nomenclature>
    {
        public NomenclatureMap()
        {
            Property(x => x.CaseIndex, m => m.Column("CASE_INDEX"));
            Property(x => x.CaseTitle, m => m.Column("CASE_TITLE"));
            Property(x => x.StorageUnitsCount, m => m.Column("STORAGE_UNITS_COUNT"));
            Property(x => x.StorageLifeAndArticleNumbers, m => m.Column("STORAGE_LIFE"));
            Property(x => x.Description);
            Property(x => x.Year, m => m.Column("NOMENCLATURE_YEAR"));
        }
    }
}