﻿using Cement.Core.Entities;

namespace Cement.Core.Mapping
{
    public class UserLoginLogMap : BaseEntityMap<UserLoginLog>
    {
        public UserLoginLogMap() : base("USER_LOGIN_LOG")
        {
            Property(x => x.LogedAt, map => map.Column("LOGED_AT"));
            Property(x => x.Success);

            ManyToOne(x => x.User, m => m.Column("USER_ID"));
        }
    }
}