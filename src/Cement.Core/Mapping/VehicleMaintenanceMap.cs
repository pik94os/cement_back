﻿using Cement.Core.Entities;

namespace Cement.Core.Mapping
{
    public class VehicleMaintenanceMap : BaseEntityMap<VehicleMaintenance>
    {
        public VehicleMaintenanceMap() : base("VEHICLE_MAINTENANCE")
        {
            Property(x => x.Date, m => m.Column("DATE_FIX"));
            Property(x => x.Mileage);
            Property(x => x.Name);
            Property(x => x.Master, m => m.Column("REPAIR_MASTER"));
            Property(x => x.WorkDate, m => m.Column("WORK_DATE"));
            Property(x => x.Cost);
            Property(x => x.Notes);
            Property(x => x.DocName, m => m.Column("DOCUMENT"));

            ManyToOne(x => x.Vehicle, m => m.Column("VEHICLE_ID"));
            ManyToOne(x => x.Driver, m => m.Column("DRIVER_ID"));
            ManyToOne(x => x.CarService, m => m.Column("CAR_SERVICE_ID"));
            ManyToOne(x => x.DocFile, m => m.Column("DOCUMENT_ID"));
        }
    }
}