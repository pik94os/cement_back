﻿using Cement.Core.Entities;

namespace Cement.Core.Mapping.Dict
{
    public class MeasureUnitMap : BaseEntityMap<MeasureUnit>
    {
        public MeasureUnitMap() : base("DICT_MEASURE_UNIT")
        {
            Property(x => x.Name);
            Property(x => x.ShortName, m => m.Column("SHORT_NAME"));
            Property(x => x.MeasureUnitGroup, m => m.Column("UNIT_GROUP"));
            Property(x => x.IsNational, m => m.Column("IS_NATIONAL"));
            Property(x => x.IsNotInEskk, m => m.Column("IS_NOT_IN_ESKK"));
        }
    }
}