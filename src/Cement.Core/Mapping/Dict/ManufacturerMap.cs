﻿using Cement.Core.Entities;

namespace Cement.Core.Mapping
{
    public class ManufacturerMap : BaseEntityMap<Manufacturer>
    {
        public ManufacturerMap() : base("DICT_MANUFACTURER")
        {
            Property(x => x.Name);
        }
    }
}