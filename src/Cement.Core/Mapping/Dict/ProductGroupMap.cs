﻿using Cement.Core.Entities;

namespace Cement.Core.Mapping
{
    public class ProductGroupMap : BaseEntityMap<ProductGroup>
    {
        public ProductGroupMap(): base("DICT_PRODUCT_GROUP")
        {
            Property(x => x.Name);
            Property(x => x.ProductType, m => m.Column("TYPE"));
        }
    }
}