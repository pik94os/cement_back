﻿using Cement.Core.Entities;

namespace Cement.Core.Mapping
{
    public class PackingMaterialMap : BaseEntityMap<PackingMaterial>
    {
        public PackingMaterialMap() : base("DICT_PACKING_MATERIAL")
        {
            Property(x => x.Name);
        }
    }
}