﻿using Cement.Core.Entities.Dict;

namespace Cement.Core.Mapping
{
    public class ExperienceMap : BaseEntityMap<Experience>
    {
        public ExperienceMap() : base("DICT_EXPERIENCE")
        {
            Property(x => x.Name);
            Property(x => x.Type);
        }
    }
}