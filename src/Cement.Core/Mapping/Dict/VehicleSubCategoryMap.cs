﻿using Cement.Core.Entities;

namespace Cement.Core.Mapping
{
    public class VehicleSubCategoryMap : BaseEntityMap<VehicleSubCategory>
    {
        public VehicleSubCategoryMap() : base("DICT_VEHICLE_SUBCATEGORY")
        {
            Property(x => x.Name);

            ManyToOne(x => x.Category, m => m.Column("CATEGORY_ID"));
        }
    }
}