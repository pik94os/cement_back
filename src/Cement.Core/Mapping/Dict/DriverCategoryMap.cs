﻿using Cement.Core.Entities;

namespace Cement.Core.Mapping
{
    public class DriverCategoryMap : BaseEntityMap<DriverCategory>
    {
        public DriverCategoryMap() : base("DICT_DRIVER_CATEGORY")
        {
            Property(x => x.Name);
            Property(x => x.ClientValue, m => m.Column("CLIENT_VALUE"));
            Property(x => x.Type);
        }
    }
}