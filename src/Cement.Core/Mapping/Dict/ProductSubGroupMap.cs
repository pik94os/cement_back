﻿using Cement.Core.Entities;

namespace Cement.Core.Mapping
{
    public class ProductSubGroupMap : BaseEntityMap<ProductSubGroup>
    {
        public ProductSubGroupMap() : base("DICT_PRODUCT_SUBGROUP")
        {
            Property(x => x.Name);
            ManyToOne(x => x.ProductGroup, m => m.Column("GROUP_ID"));
        }
    }
}