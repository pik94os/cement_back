﻿using Cement.Core.Entities;

namespace Cement.Core.Mapping
{
    public class VehicleBrandMap : BaseEntityMap<VehicleBrand>
    {
        public VehicleBrandMap() : base("DICT_VEHICLE_BRAND")
        {
            Property(x => x.Name);
        }
    }
}