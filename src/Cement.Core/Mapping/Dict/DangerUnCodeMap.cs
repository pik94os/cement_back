﻿using Cement.Core.Entities;

namespace Cement.Core.Mapping
{
    public class DangerUnCodeMap : BaseEntityMap<DangerUnCode>
    {
        public DangerUnCodeMap() : base("DICT_DANGER_UN_CODE")
        {
            Property(x => x.Name);
        }
    }
}