﻿using Cement.Core.Entities;

namespace Cement.Core.Mapping
{
    public class BankMap : BaseEntityMap<Bank>
    {
        public BankMap() : base("DICT_BANK")
        {
            Property(x => x.Name);
            Property(x => x.FullName, m => m.Column("FULL_NAME")); 
            Property(x => x.Bik); 
            Property(x => x.Ks);
        }
    }
}