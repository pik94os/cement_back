﻿using Cement.Core.Entities;

namespace Cement.Core.Mapping
{
    public class TaxMap : BaseEntityMap<Tax>
    {
        public TaxMap() : base("DICT_TAX")
        {
            Property(x => x.Rate);
        }
    }
}