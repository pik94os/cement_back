﻿using Cement.Core.Entities;

namespace Cement.Core.Mapping
{
    public class RailwayStationMap : BaseEntityMap<RailwayStation>
    {
        public RailwayStationMap() : base("DICT_RAILWAY_STATION")
        {
            Property(x => x.Code);
            Property(x => x.Name);
            Property(x => x.ShortName, m => m.Column("SHORT_NAME"));
            Property(x => x.RegionName, m => m.Column("REGION_NAME"));
            Property(x => x.RailwayName, m => m.Column("RAILWAY_NAME"));
            Property(x => x.RailwayShortName, m => m.Column("RAILWAY_SHORT_NAME"));
        }
    }
}