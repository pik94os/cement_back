﻿using Cement.Core.Entities;

namespace Cement.Core.Mapping
{
    public class PackerMap : BaseEntityMap<Packer>
    {
        public PackerMap() : base("DICT_PICKER")
        {
            Property(x => x.Name);
        }
    }
}