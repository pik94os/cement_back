﻿using Cement.Core.Entities;

namespace Cement.Core.Mapping
{
    public class ImporterMap : BaseEntityMap<Importer>
    {
        public ImporterMap() : base("DICT_IMPORTER")
        {
            Property(x => x.Name);
        }
    }
}