﻿using Cement.Core.Entities;

namespace Cement.Core.Mapping
{
    public class VehicleWheelBaseMap : BaseEntityMap<VehicleWheelBase>
    {
        public VehicleWheelBaseMap(): base("DICT_VEHICLE_WHEELBASE")
        {
            Property(x => x.Name);
        }
    }
}