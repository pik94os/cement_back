﻿using Cement.Core.Entities;

namespace Cement.Core.Mapping
{
    public class CountryCodeMap : BaseEntityMap<CountryCode>
    {
        public CountryCodeMap() : base("DICT_COUNTRY_CODE")
        {
            Property(x => x.Name);
        }
    }
}