﻿using Cement.Core.Entities;

namespace Cement.Core.Mapping
{
    public class OwnershipMap : BaseEntityMap<Ownership>
    {
        public OwnershipMap() : base("DICT_OWNERSHIP")
        {
            Property(x => x.Name);
            Property(x => x.ShortName, m => m.Column("SHORT_NAME"));
        }
    }
}