﻿using Cement.Core.Entities;

namespace Cement.Core.Mapping
{
    public class ServiceSupplierMap : BaseEntityMap<ServiceSupplier>
    {
        public ServiceSupplierMap() : base("DICT_SERVICE_SUPPLIER")
        {
            Property(x => x.Name);
        }
    }
}