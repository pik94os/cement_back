﻿using Cement.Core.Entities;

namespace Cement.Core.Mapping
{
    public class VehicleModificationMap : BaseEntityMap<VehicleModification>
    {
        public VehicleModificationMap() : base("DICT_VEHICLE_MODIFICATION")
        {
            Property(x => x.Name);
            ManyToOne(x => x.Model, m => m.Column("MODEL_ID"));
        }
    }
}