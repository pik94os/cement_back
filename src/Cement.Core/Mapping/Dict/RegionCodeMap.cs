﻿using Cement.Core.Entities;

namespace Cement.Core.Mapping
{
    public class RegionCodeMap : BaseEntityMap<RegionCode>
    {
        public RegionCodeMap() : base("DICT_REGION_CODE")
        {
            Property(x => x.Name);
        }
    }
}