﻿using Cement.Core.Entities.Dict;

namespace Cement.Core.Mapping.Dict
{
    public class PositionMap : BaseEntityMap<Position>
    {
        public PositionMap() : base("DICT_POSITION")
        {
            Property(x => x.Name);
            Property(x => x.Type, m => m.Update(false));
        }
    }
}