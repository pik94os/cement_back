﻿using Cement.Core.Entities;

namespace Cement.Core.Mapping
{
    public class PackingTypeMap : BaseEntityMap<PackingType>
    {
        public PackingTypeMap() : base("DICT_PACKING_TYPE")
        {
            Property(x => x.Name);
            Property(x => x.Description);
            ManyToOne(x => x.Photo, m => m.Column("PHOTO_FILE_ID"));
        }
    }
}