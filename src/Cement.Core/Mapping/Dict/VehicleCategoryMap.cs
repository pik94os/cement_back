﻿using Cement.Core.Entities;

namespace Cement.Core.Mapping
{
    public class VehicleCategoryMap : BaseEntityMap<VehicleCategory>
    {
        public VehicleCategoryMap() : base("DICT_VEHICLE_CATEGORY")
        {
            Property(x => x.Name);
            Property(x => x.HasСharacteristics, m => m.Column("HAS_CHARACTERISTICS"));
            Property(x => x.Type);
        }
    }
}