﻿using Cement.Core.Entities;

namespace Cement.Core.Mapping
{
    public class DangerClassMap : BaseEntityMap<DangerClass>
    {
        public DangerClassMap() : base("DICT_DANGER_CLASS")
        {
            Property(x => x.Num);
            
            Property(x => x.Name);
        }
    }
}