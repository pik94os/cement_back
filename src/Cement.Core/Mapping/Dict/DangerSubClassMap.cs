﻿using Cement.Core.Entities;

namespace Cement.Core.Mapping
{
    public class DangerSubClassMap : BaseEntityMap<DangerSubClass>
    {
        public DangerSubClassMap() : base("DICT_DANGER_SUB_CLASS")
        {
            Property(x => x.Num);
            Property(x => x.Name);
            ManyToOne(x => x.Class, m => m.Column("CLASS_ID"));
        }
    }
}