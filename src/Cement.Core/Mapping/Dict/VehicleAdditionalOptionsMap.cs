﻿using Cement.Core.Entities;

namespace Cement.Core.Mapping
{
    public class VehicleAdditionalOptionsMap : BaseEntityMap<VehicleAdditionalOptions>
    {
        public VehicleAdditionalOptionsMap() : base("DICT_VEHICLE_ADD_OPTIONS")
        {
            Property(x => x.Name);
        }
    }
}