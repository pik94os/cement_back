﻿using Cement.Core.Entities;

namespace Cement.Core.Mapping
{
    public class VehicleBodyTypeMap : BaseEntityMap<VehicleBodyType>
    {
        public VehicleBodyTypeMap() : base("DICT_VEHICLE_BODY_TYPE")
        {
            Property(x => x.Name);
        }
    }
}