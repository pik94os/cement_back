﻿using Cement.Core.Entities;

namespace Cement.Core.Mapping
{
    public class VehicleClassMap : BaseEntityMap<VehicleClass>
    {
        public VehicleClassMap() : base("DICT_VEHICLE_CLASS")
        {
            Property(x => x.Name);
        }
    }
}