﻿using Cement.Core.Entities;

namespace Cement.Core.Mapping
{
    public class VehicleModelMap : BaseEntityMap<VehicleModel>
    {
        public VehicleModelMap() : base("DICT_VEHICLE_MODEL")
        {
            Property(x => x.Name);
            ManyToOne(x => x.Brand, m => m.Column("BRAND_ID"));
            ManyToOne(x => x.Category, m => m.Column("CATEGORY_ID"));
            ManyToOne(x => x.SubCategory, m => m.Column("SUBCATEGORY_ID"));
        }
    }
}