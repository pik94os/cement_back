﻿using Cement.Core.Entities;

namespace Cement.Core.Mapping
{
    public class FeatureModelMap : BaseEntityMap<FeatureModel>
    {
        public FeatureModelMap() : base("DICT_FEATURE_MODEL")
        {
            Property(x => x.Name);
        }
    }
}