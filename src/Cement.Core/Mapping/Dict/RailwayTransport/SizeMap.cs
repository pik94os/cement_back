﻿using Cement.Core.Entities;

namespace Cement.Core.Mapping
{
    public class SizeMap : BaseEntityMap<Size>
    {
        public SizeMap() : base("DICT_SIZE")
        {
            Property(x => x.Name);
        }
    }
}