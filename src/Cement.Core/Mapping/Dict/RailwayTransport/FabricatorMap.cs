﻿using Cement.Core.Entities;

namespace Cement.Core.Mapping
{
    public class FabricatorMap : BaseEntityMap<Fabricator>
    {
        public FabricatorMap() : base("DICT_FABRICATOR")
        {
            Property(x => x.Name);
        }
    }
}