﻿using Cement.Core.Entities;

namespace Cement.Core.Mapping
{
    public class BogieMap : BaseEntityMap<Bogie>
    {
        public BogieMap() : base("DICT_BOGIE")
        {
            Property(x => x.Name);
            Property(x => x.Model);
        }
    }
}