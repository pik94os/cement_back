﻿using Cement.Core.Entities;

namespace Cement.Core.Mapping
{
    public class RailwayTransportMap : BaseEntityMap<RailwayTransport>
    {
        public RailwayTransportMap() : base("DICT_RAILWAY_TRANSPORT")
        {
            Property(x => x.Model);
            Property(x => x.Kind);
            Property(x => x.ReleaseYear, m => m.Column("RELEASE_YEAR"));
            Property(x => x.EndEditionYear, m => m.Column("END_EDITION_YEAR"));
            Property(x => x.Capacity);
            Property(x => x.TareWeightMin, m => m.Column("TARE_WEIGHT_MIN"));
            Property(x => x.TareWeightMax, m => m.Column("TARE_WEIGHT_MAX"));
            Property(x => x.AxesLength, m => m.Column("AXES_LENGTH"));
            Property(x => x.AxesCount, m => m.Column("AXES_COUNT"));
            Property(x => x.AxialLoad, m => m.Column("AXIAL_LOAD"));
            Property(x => x.HasTransitionPlatform, m => m.Column("HAS_TRSN_PLATFORM"));
            Property(x => x.BodyVolume, m => m.Column("BODY_VOLUME"));
            Property(x => x.BoilerCalibration, m => m.Column("BOILER_CALIBRATION"));
            ManyToOne(x => x.Fabricator, m => m.Column("FABRICATOR_ID"));
            ManyToOne(x => x.FeatureModel, m => m.Column("FEATURE_MODEL_ID"));
            ManyToOne(x => x.Bogie, m => m.Column("BOGIE_ID"));
            ManyToOne(x => x.Size, m => m.Column("SIZE_ID")); 
        }
    }
}