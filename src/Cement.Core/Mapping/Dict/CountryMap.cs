﻿using Cement.Core.Entities;

namespace Cement.Core.Mapping
{
    public class CountryMap : BaseEntityMap<Country>
    {
        public CountryMap() : base("DICT_COUNTRY")
        {
            Property(x => x.Name);
        }
    }
}