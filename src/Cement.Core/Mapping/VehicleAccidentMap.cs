﻿using Cement.Core.Entities;

namespace Cement.Core.Mapping
{
    public class VehicleAccidentMap : BaseEntityMap<VehicleAccident>
    {
        public VehicleAccidentMap() : base("VEHICLE_ACCIDENT")
        {
            Property(x => x.Date, m => m.Column("DATE_FIX"));
            Property(x => x.Count);
            Property(x => x.Damage);
            Property(x => x.Description);
            Property(x => x.Cost);

            ManyToOne(x => x.Vehicle, m => m.Column("VEHICLE_ID"));
            ManyToOne(x => x.Driver, m => m.Column("DRIVER_ID"));
        }
    }
}