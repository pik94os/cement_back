﻿using Cement.Core.Entities;

namespace Cement.Core.Mapping
{
    public class VehicleDepartmentMap : BaseOrganizationBasedEntityMap<VehicleDepartment>
    {
        public VehicleDepartmentMap() : base("DICT_VEHICLE_DEPARTMENT")
        {
            Property(x => x.Name);
        }
    }
}