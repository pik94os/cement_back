﻿using Cement.Core.Entities;

namespace Cement.Core.Mapping
{
    public class BaseRequestMap : BaseOrganizationBasedEntityMap<BaseRequest>
    {
        public BaseRequestMap() : base("PRODUCT_REQUEST")
        {
            Discriminator(x =>
            {
                x.Column("TYPE");
                x.NotNullable(true);
            });

            Property(x => x.RequestType,
                m =>
                {
                    m.Column("TYPE");
                    m.Insert(false);
                    m.Update(false);
                });

            ManyToOne(x => x.InitialRequest, m =>
            {
                m.Column("INITIAL_REQUEST_ID");
                m.Update(false);
            });

            Property(x => x.StageNumber, m =>
            {
                m.Column("STAGE_NUMBER");
                m.Update(false);
            });

            Property(x => x.ChainGuid, m =>
            {
                m.Column("CHAIN_GUID");
                m.Update(false);
            });

            Property(x => x.SignedAt, m => m.Column("SIGNED_AT"));
            
            Property(x => x.Number, m => m.Column("REQUEST_NUMBER"));
            Property(x => x.Date, m => m.Column("REQUEST_DATE"));
            Property(x => x.ShipmentDateTime, m => m.Column("SHIPMENT_DATE"));
            Property(x => x.UnshipmentDateTime, m => m.Column("UNSHIPMENT_DATE"));
            ManyToOne(x => x.ProviderContract, m => m.Column("PROVIDER_CONTRACT_ID"));
            ManyToOne(x => x.ProviderContactPerson, m => m.Column("PROVIDER_PERSON_ID"));
            ManyToOne(x => x.Product, m => m.Column("PRODUCT_ID"));
            ManyToOne(x => x.ProductMeasureUnit, m => m.Column("PRODUCT_UNIT_ID"));
            Property(x => x.ProductPrice, m => m.Column("PRODUCT_PRICE"));
            Property(x => x.ProductCount, m => m.Column("PRODUCT_COUNT"));
            Property(x => x.Content);
            Property(x => x.Importance);
            Property(x => x.Secrecy);
            Property(x => x.Name);
            Property(x => x.DocumentState, m => m.Column("STATE"));
            ManyToOne(x => x.Payer, m => m.Column("PAYER_ID"));
            ManyToOne(x => x.PayerContactPerson, m => m.Column("PAYER_PERSON_ID"));
            Property(x => x.ProductTax, m => m.Column("PRODUCT_TAX"));
            Property(x => x.ProductFactCount, m => m.Column("PRODUCT_FACT_COUNT"));
            ManyToOne(x => x.Consignee, m =>
            {
                m.Column("CONSIGNEE_ID");
                m.Update(false);
            });
            ManyToOne(x => x.ConsigneeContactPerson, m => m.Column("CONSIGNEE_PERSON_ID"));
            ManyToOne(x => x.Warehouse, m => m.Column("WAREHOUSE_ID"));
            ManyToOne(x => x.Provider, m => m.Column("PROVIDER_ID"));
            Property(x => x.IsLastInChain, m => m.Column("IS_LAST"));
            Property(x => x.UsedAt, m => m.Column("USED_AT"));
        }
    }
}