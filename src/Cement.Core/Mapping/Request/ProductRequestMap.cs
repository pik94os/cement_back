﻿using Cement.Core.Entities;
using Cement.Core.Enums;
using NHibernate.Mapping.ByCode.Conformist;

namespace Cement.Core.Mapping
{
    public class ProductRequestMap : SubclassMapping<ProductRequest>
    {
        public ProductRequestMap() 
        {
            this.DiscriminatorValue((int)RequestType.Product);

            Property(x => x.ShipmentType, m => m.Column("SHIPMENT_TYPE"));
            Property(x => x.VehicleType, m => m.Column("VEHICLE_TYPE"));
            ManyToOne(x => x.VehicleBodyType, m => m.Column("VEH_BODY_TYPE_ID"));
            Property(x => x.VehicleIsRearLoading, m => m.Column("VEH_IS_REAR_LOADING"));
            Property(x => x.VehicleIsSideLoading, m => m.Column("VEH_IS_SIDE_LOADING"));
            Property(x => x.VehicleIsOverLoading, m => m.Column("VEH_IS_OVER_LOADING"));
            ManyToOne(x => x.VehicleOption, m => m.Column("VEHICLE_OPTION_ID"));
            Property(x => x.VehicleExtraCharacteristic, m => m.Column("VEH_CHARACTERISTIC"));
            ManyToOne(x => x.TransportUnit, m => m.Column("TRANSPORT_UNIT_ID"));
        }
    }
}