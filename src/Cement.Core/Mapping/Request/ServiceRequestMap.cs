﻿using Cement.Core.Entities;
using Cement.Core.Enums;
using NHibernate.Mapping.ByCode.Conformist;

namespace Cement.Core.Mapping
{
    public class ServiceRequestMap : SubclassMapping<ServiceRequest>
    {
        public ServiceRequestMap() 
        {
            this.DiscriminatorValue((int)RequestType.Service);
        }
    }
}