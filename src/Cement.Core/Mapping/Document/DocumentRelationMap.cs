﻿using Cement.Core.Entities;

namespace Cement.Core.Mapping
{
    public class DocumentRelationMap : BaseEntityMap<DocumentRelation>
    {
        public DocumentRelationMap() : base("DOC_RELATION")
        {
            Property(x => x.BaseDocumentId, m => m.Column("BASE_DOC_ID"));
            Property(x => x.BaseDocumentType, m => m.Column("BASE_DOC_TYPE"));
            Property(x => x.JoinedDocumentType, m => m.Column("DOC_TYPE"));
            Property(x => x.JoinedDocId, m => m.Column("DOC_ID"));
            Property(x => x.Name, m => m.Column("NAME"));
            Property(x => x.No, m => m.Column("NO"));
            Property(x => x.Date, m => m.Column("DOC_DATE"));
            Property(x => x.State, m => m.Column("STATE"));
        }
    }
}