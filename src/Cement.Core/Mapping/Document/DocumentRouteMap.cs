﻿using Cement.Core.Entities;

namespace Cement.Core.Mapping
{
    public class DocumentRouteMap<T, TK> : BaseEntityMap<TK> where T: BaseEntity, IDocument where TK : DocumentRoute<T>  
    {
        public DocumentRouteMap(string tableName, string documentColumnId = "DOCUMENT_ID") : base(tableName)
        {
            ManyToOne(x => x.Document, m =>
            {
                m.Column(documentColumnId);
                m.Update(false);
            });

            ManyToOne(x => x.Employee, m =>
            {
                m.Column("EMPLOYEE_ID");
                m.Update(false);
            });

            Property(x => x.DateIn, m =>
            {
                m.Column("DATE_IN");
                m.Update(false);
            });

            Property(x => x.DateOut, m => m.Column("DATE_OUT"));
            Property(x => x.Comment);
            Property(x => x.ProcessingResult, m => m.Column("PROCESSING_RESULT"));
            Property(x => x.MovementType, m =>
            {
                m.Column("MOVEMENT_TYPE");
                m.Update(false);
            });

            Property(x => x.ArchivedAt, m => m.Column("ARCHIVED_AT"));
            Property(x => x.DeletedAt, m => m.Column("DELETED_AT"));
            Property(x => x.Stage, m => m.Update(false));
        }
    }
}