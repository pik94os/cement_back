﻿using Cement.Core.Entities;

namespace Cement.Core.Mapping
{
    public class DocumentMap : BaseEntityMap<Document>
    {
        public DocumentMap()
        {
            Property(x => x.Number, m => m.Column("NO"));
            Property(x => x.Name);
            Property(x => x.Date, m => m.Column("DOCUMENT_DATE"));
            Property(x => x.Type);
            Property(x => x.DocumentState, m => m.Column("STATE"));
        }
    }
}