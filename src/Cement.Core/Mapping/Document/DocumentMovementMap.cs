﻿using Cement.Core.Entities;
using NHibernate.Mapping.ByCode.Conformist;

namespace Cement.Core.Mapping
{
    public class IncomingDocumentMap : ClassMapping<IncomingDocument>
    {
        public IncomingDocumentMap()
        {
            Table("DOC_INCOMING");

            Id(x => x.Id, map =>
            {
                map.Column("ID");
                map.Generator(NHibernate.Mapping.ByCode.Generators.Assigned);
            });

            Property(x => x.DocumentId, m => m.Column("DOCUMENT_ID"));
            Property(x => x.EmployeeId, m => m.Column("EMPLOYEE_ID"));
            Property(x => x.OrganizationId, m => m.Column("ORGANIZATION_ID"));
            Property(x => x.IsArchived, m => m.Column("IS_ARCHIVED"));
        }
    }

    public class OutcomingDocumentMap : ClassMapping<OutcomingDocument>
    {
        public OutcomingDocumentMap()
        {
            Table("DOC_OUTCOMING");

            Id(x => x.Id, map =>
            {
                map.Column("ID");
                map.Generator(NHibernate.Mapping.ByCode.Generators.Assigned);
            });

            Property(x => x.DocumentId, m => m.Column("DOCUMENT_ID"));
            Property(x => x.EmployeeId, m => m.Column("EMPLOYEE_ID"));
            Property(x => x.OrganizationId, m => m.Column("ORGANIZATION_ID"));
            Property(x => x.IsArchived, m => m.Column("IS_ARCHIVED"));
        }
    }
}