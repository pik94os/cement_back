﻿using Cement.Core.Entities;

namespace Cement.Core.Mapping
{
    class ClientGroupMap : BaseOrganizationBasedEntityMap<ClientGroup>
    {
        public ClientGroupMap() : base("CLIENT_GROUP")
        {
            Property(x => x.Name);
            Property(x => x.Description);
            ManyToOne(x => x.Parent, m => m.Column("PARENT_ID"));
        }
    }
}