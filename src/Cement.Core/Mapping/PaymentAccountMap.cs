﻿using Cement.Core.Entities;

namespace Cement.Core.Mapping
{
    public class PaymentAccountMap : BaseOrganizationBasedEntityMap<PaymentAccount>
    {
        public PaymentAccountMap() : base("PAYMENT_ACCOUNT")
        {
            Property(x => x.Number, m => m.Column("ACCOUNT_NUMBER"));
            ManyToOne(x => x.Bank, m => m.Column("BANK_ID"));
        }
    }
}