﻿using Cement.Core.Entities;

namespace Cement.Core.Mapping
{
    public class MessageRecipientMap : BaseEntityMap<MessageRecipient>
    {
        public MessageRecipientMap() : base("MESSAGE_RECIPIENT")
        {
            ManyToOne(x => x.Message, m => m.Column("MESSAGE_ID"));
            ManyToOne(x => x.Recipient, m => m.Column("RECIPIENT_ID"));
        }
    }
}