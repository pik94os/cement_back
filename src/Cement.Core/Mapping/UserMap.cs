﻿using Cement.Core.Entities;
using NHibernate.Type;

namespace Cement.Core.Mapping
{
    public class UserMap : BaseEntityMap<User>
    {
        public UserMap() : base("USERS")
        {
            Property(x => x.Login, map =>
            {
                map.NotNullable(true);
                map.Update(false);
            });
            Property(x => x.Email, map => map.NotNullable(true));
            Property(x => x.Name, map => map.NotNullable(true));
            Property(x => x.IsBlocked, map => map.Column("IS_BLOCKED"));
            Property(x => x.CreatedAt, map =>
            {
                map.Column("CREATED_AT");
                map.Update(false);
            });
            Property("PasswordHash", map =>
                {
                    map.NotNullable(false);
                    map.Column("password_hash");
                    map.Type<BinaryBlobType>();
                    map.Length(64);
                });
            Property("Salt", map =>
                {
                    map.NotNullable(false);
                    map.Type<BinaryBlobType>();
                    map.Length(64);
                });

            Property(x => x.IsEmailConfirmed, map => map.Column("IS_EMAIL_CONFIRMED"));
            Property(x => x.SecretToken, map =>
                {
                    map.Column("SECRET_TOKEN");
                    map.Type<BinaryBlobType>();
                    map.NotNullable(false);
                });

            ManyToOne(x => x.Organization, m => m.Column("ORGANIZATION_ID"));
            ManyToOne(x => x.Employee, m => m.Column("EMPLOYEE_ID"));

            Property(x => x.Role, map =>
            {
                map.Column("ROLE");
                map.Update(false);
            });
        }
    }
}