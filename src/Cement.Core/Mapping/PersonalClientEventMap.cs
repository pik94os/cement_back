﻿using Cement.Core.Entities;

namespace Cement.Core.Mapping
{
    public class PersonalClientEventMap : BaseEntityMap<PersonalClientEvent>
    {
        public PersonalClientEventMap() : base("PERSONAL_CLIENT_EVENT")
        {
            Property(x => x.EventDateTime, m => m.Column("EVENT_DATE"));
            Property(x => x.ArchivedAt, m => m.Column("ARCHIVED_AT"));
            Property(x => x.Theme, m => m.Column("THEME"));
            Property(x => x.Description, m => m.Column("DESCRIPTION"));
            Property(x => x.ReminderType, m => m.Column("REMINDER_TYPE"));

            ManyToOne(x => x.PersonalClient, m =>
            {
                m.Column("PERSONAL_CLIENT_ID");
                m.NotNullable(true);
            });
        }
    }
}