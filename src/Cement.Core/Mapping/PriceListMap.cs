﻿using Cement.Core.Entities;

namespace Cement.Core.Mapping
{
    public class PriceListMap : BaseOrganizationBasedEntityMap<PriceList>
    {
        public PriceListMap() : base("PRICE_LIST")
        {
            Property(x => x.Name);
            Property(x => x.DateEnd, m => m.Column("DATE_END"));
            Property(x => x.DateStart, m => m.Column("DATE_START"));
            Property(x => x.DescriptionBytes, m => m.Column("DESCRIPTION"));
            Property(x => x.Status);
            ManyToOne(x => x.MessageDocument, m => m.Column("MSG_DOCUMENT_ID"));
            ManyToOne(x => x.ReplacementPriceList, m => m.Column("RPL_PRICELIST_ID"));
        }
    }
}