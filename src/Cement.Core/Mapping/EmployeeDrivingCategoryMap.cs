﻿using Cement.Core.Entities;

namespace Cement.Core.Mapping
{
    class EmployeeDrivingCategoryMap : BaseEntityMap<EmployeeDrivingCategory>
    {
        public EmployeeDrivingCategoryMap() : base("EMPLOYEE_DRIVE_CAT")
    {
        ManyToOne(x => x.Employee, m => m.Column("EMPLOYEE_ID"));
        ManyToOne(x => x.Category, m => m.Column("DRIVE_CATEGORY_ID"));
    }
    }
}
