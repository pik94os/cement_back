﻿using Cement.Core.Entities.Views;
using NHibernate.Mapping.ByCode.Conformist;

namespace Cement.Core.Mapping.Views
{
    public class ExternalIncomingMap : ClassMapping<ExternalIncoming>
    {
        public ExternalIncomingMap()
        {
            Table("VIEW_EXTERNAL_INCOMING");

            Id(x => x.Id, m => m.Column("MVT_ID"));
            Property(x => x.State, m => m.Column("DOC_STATE"));
            Property(x => x.No, m => m.Column("DOC_NO"));
            Property(x => x.Date, m => m.Column("DOC_DATE"));
            Property(x => x.Name, m => m.Column("DOC_NAME"));
            Property(x => x.CorrepondentName, m => m.Column("MEETING_NAME"));
            Property(x => x.DocumentType, m => m.Column("DOC_TYPE"));
            Property(x => x.DocumentKind, m => m.Column("DOC_KIND"));
            Property(x => x.SigneeId, m => m.Column("SIGNEE_ID"));
            Property(x => x.EmployeeId, m => m.Column("EMP_ID"));
        }
    }
}