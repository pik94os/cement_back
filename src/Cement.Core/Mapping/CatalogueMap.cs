﻿using Cement.Core.Entities;

namespace Cement.Core.Mapping
{
    public class CatalogueMap : BaseOrganizationBasedEntityMap<Catalogue>
    {
        public CatalogueMap() 
        {
            ManyToOne(x => x.PriceList, m => m.Column("PRICE_LIST_ID"));
            Property(x => x.DescriptionBytes, m => m.Column("DESCRIPTION"));
        }
    }
}