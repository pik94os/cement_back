﻿using Cement.Core.Entities;

namespace Cement.Core.Mapping
{
    public class AddressDictMap : BaseEntityMap<AddressDict>
    {
        public AddressDictMap() : base("ADDRESS_DICT")
        {
            Property(x => x.Index, m => m.Column("POSTALCODE"));
            Property(x => x.Country);
            Property(x => x.Region);
            Property(x => x.Area);
            Property(x => x.Locality);
            Property(x => x.Street);
        }
    }
}