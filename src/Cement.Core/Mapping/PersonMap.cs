﻿using Cement.Core.Entities;

namespace Cement.Core.Mapping
{
    public class PersonMap : BaseEntityMap<Person>
    {
        public PersonMap() 
        {
            Property(x => x.Name);
        }
    }
}