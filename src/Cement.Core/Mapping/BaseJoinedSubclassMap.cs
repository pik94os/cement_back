﻿using Cement.Core.Entities;
using NHibernate.Mapping.ByCode;
using NHibernate.Mapping.ByCode.Conformist;

namespace Cement.Core.Mapping
{
    public abstract class BaseJoinedSubclassMap<T> : SubclassMapping<T> where T : BaseEntity
    {
        protected abstract void SplitMapping(IJoinMapper<T> map);

        protected BaseJoinedSubclassMap(int discriminator, string tableName = null, string keyColumn = "ID")
        {
            DiscriminatorValue(discriminator);

            if (string.IsNullOrWhiteSpace(tableName))
            {
                tableName = typeof(T).Name.ToUpper();
            }

            Join(tableName,
                map =>
                {
                    map.Key(x =>
                    {
                        x.Column(keyColumn);
                        x.NotNullable(true);
                    });

                    SplitMapping(map);
                });
        }
    }
}