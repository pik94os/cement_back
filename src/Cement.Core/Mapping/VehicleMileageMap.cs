﻿using Cement.Core.Entities;

namespace Cement.Core.Mapping
{
    public class VehicleMileageMap : BaseEntityMap<VehicleMileage>
    {
        public VehicleMileageMap() : base("VEHICLE_MILEAGE")
        {
            Property(x => x.Mileage);
            Property(x => x.Date, m => m.Column("FIXATION_DATE"));
            ManyToOne(x => x.Vehicle, m => m.Column("VEHICLE_ID"));
        }
    }
}