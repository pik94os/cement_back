﻿using Cement.Core.Entities;
using NHibernate.Mapping.ByCode.Conformist;

namespace Cement.Core.Mapping
{
    public abstract class BaseEntityMap<T> : ClassMapping<T> where T : BaseEntity
    {
        public string TableName { get; private set; }

        protected BaseEntityMap(string tableName = null)
        {
            if (string.IsNullOrWhiteSpace(tableName))
            {
                tableName = typeof (T).Name.ToUpper();
            }

            TableName = tableName;
            Table(tableName);
            Id(x => x.Id,
                map =>
                {
                    map.Column("ID");
                    //map.Generator(NHibernate.Mapping.ByCode.Generators.Native);
                    map.Generator(NHibernate.Mapping.ByCode.Generators.HighLow, p => p.Params(new { max_lo = 100 }));
                });

        }

        //protected BaseEntityMap()
        //    : this(typeof(T).Name)
        //{

        //}
    }
}