﻿using Cement.Core.Entities;

namespace Cement.Core.Mapping
{
    public class AdvertRecipientMap : BaseEntityMap<AdvertRecipient>
    {
        public AdvertRecipientMap() : base("ADVERT_RECIPIENT")
        {
            ManyToOne(x => x.Advert, m => m.Column("ADVERT_ID"));
            ManyToOne(x => x.Recipient, m => m.Column("RECIPIENT_ID"));
        }
    }
}