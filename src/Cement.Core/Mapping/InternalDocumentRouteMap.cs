﻿using Cement.Core.Entities;

namespace Cement.Core.Mapping
{
    public class InternalDocumentRouteMap : BaseEntityMap<InternalDocumentRoute>
    {
        public InternalDocumentRouteMap() : base("INTERNAL_DOC_ROUTE")
        {
            ManyToOne(x => x.Document, m => m.Column("DOCUMENT_ID"));
            ManyToOne(x => x.Employee, m => m.Column("EMPLOYEE_ID"));
            Property(x => x.DateIn, m => m.Column("DATE_IN"));
            Property(x => x.DateOut, m => m.Column("DATE_OUT"));

            Property(x => x.Comment, m => m.Column("COMMENT_BYTES"));
            //Property(x => x.InitialProperty, m => m.Column("INITIAL_PROPERTY"));
            Property(x => x.ProcessingResult, m => m.Column("PROCESSING_RESULT"));
        }
    }
}