﻿using Cement.Core.Entities;
using Cement.Core.Enums;
using NHibernate.Mapping.ByCode.Conformist;

namespace Cement.Core.Mapping
{
    public class InternalDocumentMovementMap : BaseEntityMap<InternalDocumentMovement>
    {
        public InternalDocumentMovementMap() : base("INTERNAL_DOC_MOVEMENT")
        {
            Discriminator(x =>
            {
                x.Column("TYPE"); 
                x.NotNullable(true);
            });

            Property(x => x.MovementType,
                m =>
                {
                    m.Column("TYPE");
                    m.Insert(false);
                    m.Update(false);
                });

            ManyToOne(x => x.Document, m => m.Column("DOCUMENT_ID"));
            ManyToOne(x => x.Employee, m => m.Column("EMPLOYEE_ID"));
            ManyToOne(x => x.Meeting, m => m.Column("MEETING_EMP_ID"));
            Property(x => x.IsArchived, m => m.Column("IS_ARCHIVED"));
            ManyToOne(x => x.Route, m => m.Column("ROUTE_ID"));
        }
    }

    public class IncomingInternalDocumentMap : SubclassMapping<IncomingInternalDocument>
    {
        public IncomingInternalDocumentMap()
        {
            DiscriminatorValue((int)DocumentMovementType.Incoming);

            //Property(x => x.Properties);
        }
    }

    public class OutcomingInternalDocumentMap : SubclassMapping<OutcomingInternalDocument>
    {
        public OutcomingInternalDocumentMap()
        {
            DiscriminatorValue((int)DocumentMovementType.Outcoming);
        }
    }

}