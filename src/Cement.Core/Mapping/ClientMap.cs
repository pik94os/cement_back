﻿using Cement.Core.Entities;

namespace Cement.Core.Mapping
{
    class ClientMap : BaseEntityMap<Client>
    {
        public ClientMap()
        {
            Property(x => x.Name);
            Property(x => x.Address);
            Property(x => x.Inn);
            Property(x => x.Kpp);
            Property(x => x.Okpo);

            Property(x => x.Ogrn);
            Property(x => x.Okved);
            Property(x => x.Okato);
            Property(x => x.Okonh);
            Property(x => x.Brand);

            Property(x => x.FullName, m => m.Column("FULL_NAME"));
            ManyToOne(x => x.Ownership, m => m.Column("OWNERSHIP_ID"));

            Property(x => x.JurAddressIndex, m => m.Column("JUR_ADDRESS_POSTAL_CODE"));
            Property(x => x.JurAddressCountry, m => m.Column("JUR_ADDRESS_COUNTRY"));
            Property(x => x.JurAddressRegion, m => m.Column("JUR_ADDRESS_REGION"));
            Property(x => x.JurAddressArea, m => m.Column("JUR_ADDRESS_AREA"));
            Property(x => x.JurAddressLocality, m => m.Column("JUR_ADDRESS_LOCALITY"));
            Property(x => x.JurAddressStreet, m => m.Column("JUR_ADDRESS_STREET"));
            Property(x => x.JurAddressHouse, m => m.Column("JUR_ADDRESS_HOUSE"));
            Property(x => x.JurAddressHousing, m => m.Column("JUR_ADDRESS_HOUSING"));
            Property(x => x.JurAddressBuilding, m => m.Column("JUR_ADDRESS_BUILDING"));
            Property(x => x.JurAddressOffice, m => m.Column("JUR_ADDRESS_OFFICE"));

            Property(x => x.FactAddress, m => m.Column("FACT_ADDRESS"));
            Property(x => x.FactAddressIndex, m => m.Column("FACT_ADDRESS_POSTAL_CODE"));
            Property(x => x.FactAddressCountry, m => m.Column("FACT_ADDRESS_COUNTRY"));
            Property(x => x.FactAddressRegion, m => m.Column("FACT_ADDRESS_REGION"));
            Property(x => x.FactAddressArea, m => m.Column("FACT_ADDRESS_AREA"));
            Property(x => x.FactAddressLocality, m => m.Column("FACT_ADDRESS_LOCALITY"));
            Property(x => x.FactAddressStreet, m => m.Column("FACT_ADDRESS_STREET"));
            Property(x => x.FactAddressHouse, m => m.Column("FACT_ADDRESS_HOUSE"));
            Property(x => x.FactAddressHousing, m => m.Column("FACT_ADDRESS_HOUSING"));
            Property(x => x.FactAddressBuilding, m => m.Column("FACT_ADDRESS_BUILDING"));
            Property(x => x.FactAddressOffice, m => m.Column("FACT_ADDRESS_OFFICE"));

            Property(x => x.Phone);
            Property(x => x.Fax);
            Property(x => x.Email);
            Property(x => x.Web);
        }
    }
}
