﻿using Cement.Core.Entities;

namespace Cement.Core.Mapping
{
    public class SignatureMap : BaseOrganizationBasedEntityMap<Signature>
    {
        public SignatureMap()
        {
            Property(x => x.DateStart, m => m.Column("DATE_START"));
            Property(x => x.DateEnd, m => m.Column("DATE_END"));
            ManyToOne(x => x.Truster, m => m.Column("TRUSTER_ID"));
            ManyToOne(x => x.Recipient, m => m.Column("RECIPIENT_ID"));

            // Основание
            ManyToOne(x => x.Reason, m => m.Column("REASON_DOC_ID"));
        }
    }
}