﻿using Cement.Core.Entities;

namespace Cement.Core.Mapping
{
    public class DepartmentMap : BaseOrganizationBasedEntityMap<Department>
    {
        public DepartmentMap() : base("DICT_DEPARTMENT")
        {
            Property(x => x.Name);
        }
    }
}