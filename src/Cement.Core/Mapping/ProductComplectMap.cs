﻿using Cement.Core.Entities;

namespace Cement.Core.Mapping
{
    public class ProductComplectMap : BaseEntityMap<ProductComplect>
    {
        public ProductComplectMap() : base("PRODUCT_COMPLECT")
        {
            ManyToOne(x => x.Product, m => m.Column("PRODUCT_ID"));
            ManyToOne(x => x.ComplectProduct, m => m.Column("CMPL_PRODUCT_ID"));
        }
    }
}