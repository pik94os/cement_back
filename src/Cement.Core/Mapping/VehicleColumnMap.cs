﻿using Cement.Core.Entities;

namespace Cement.Core.Mapping
{
    public class VehicleColumnMap : BaseOrganizationBasedEntityMap<VehicleColumn>
    {
        public VehicleColumnMap() : base("DICT_VEHICLE_COLUMN")
        {
            Property(x => x.Name);
        }
    }
}