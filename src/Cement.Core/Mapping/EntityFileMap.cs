﻿using Cement.Core.Entities;

namespace Cement.Core.Mapping
{
    public class EntityFileMap : BaseEntityMap<EntityFile>
    {
        public EntityFileMap() : base("ENTITY_FILE")
        {
            Property(x => x.Name);
            Property(x => x.Extension);
            Property(x => x.CheckSum);
            Property(x => x.Size, m => m.Column("FILE_SIZE"));
        }
    }
}