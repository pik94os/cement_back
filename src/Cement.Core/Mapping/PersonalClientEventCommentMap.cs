﻿using Cement.Core.Entities;

namespace Cement.Core.Mapping
{
    public class PersonalClientEventCommentMap : BaseEntityMap<PersonalClientEventComment>
    {
        public PersonalClientEventCommentMap(): base("PERS_CLI_EVENT_COMMENT")
        {
            ManyToOne(x => x.PersonalClientEvent, m => m.Column("PERS_CLI_EVENT_ID"));
            ManyToOne(x => x.Employee, m => m.Column("EMPLOYEE_ID"));
            Property(x => x.DateTime, m => m.Column("CREATED_AT"));
            Property(x => x.Text);
        }
    }
}