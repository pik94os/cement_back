﻿using Cement.Core.Entities;

namespace Cement.Core.Mapping
{
    public class DisposalRecipientMap : BaseEntityMap<DisposalRecipient>
    {
        public DisposalRecipientMap()
            : base("DISPOSAL_RECIPIENT")
        {
            ManyToOne(x => x.Disposal, m => m.Column("DISPOSAL_ID"));
            ManyToOne(x => x.Recipient, m => m.Column("RECIPIENT_ID"));
        }
    }
}