﻿using Cement.Core.Entities;

namespace Cement.Core.Mapping
{
    public class UserPermissionMap : BaseEntityMap<UserPermission>
    {
        public UserPermissionMap() : base("USER_PERMISSION")
        {
            Property(x => x.Permission);
            Property(x => x.Description);
            Property(x => x.AccessType, m => m.Column("ACCESS_TYPE"));

            ManyToOne(x => x.User, m => m.Column("USER_ID"));
        }
    }
}