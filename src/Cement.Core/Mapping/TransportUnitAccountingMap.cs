﻿using Cement.Core.Entities;

namespace Cement.Core.Mapping
{
    public class TransportUnitAccountingMap : BaseOrganizationBasedEntityMap<TransportUnitAccounting>
    {
        public TransportUnitAccountingMap(): base("TRANSPORT_UNIT_ACCOUNTING")
        {
            ManyToOne(x => x.TransportUnit, m => m.Column("TRANSPORT_UNIT_ID"));

            Property(x => x.Date, m => m.Column("DATE_ACCOUNTING"));
            Property(x => x.AccountingType, m => m.Column("TYPE"));
        }
    }
}