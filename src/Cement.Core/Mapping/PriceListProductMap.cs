﻿using Cement.Core.Entities;

namespace Cement.Core.Mapping
{
    public class PriceListProductMap : BaseEntityMap<PriceListProduct>
    {
        public PriceListProductMap() : base("PRICE_LIST_PRODUCT")
        {
            Property(x => x.Price);
            ManyToOne(x => x.Product, m => m.Column("PRODUCT_ID"));
            ManyToOne(x => x.PriceList, m => m.Column("PRICELIST_ID"));
            ManyToOne(x => x.Tax, m => m.Column("TAX_ID"));
            ManyToOne(x => x.MeasureUnit, m => m.Column("MEASURE_UNIT_ID"));
        }
    }
}