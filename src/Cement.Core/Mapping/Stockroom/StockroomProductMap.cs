﻿using Cement.Core.Entities.Stockroom;

namespace Cement.Core.Mapping.Stockroom
{
    public class StockroomProductMap : BaseEntityMap<StockroomProduct>
    {
        public StockroomProductMap()
            : base("STOCKROOM_PRODUCT")
        {
            ManyToOne(x => x.Warehouse, m => m.Column("WAREHOUSE_ID"));
            ManyToOne(x => x.Product, m => m.Column("PRODUCT_ID"));
            ManyToOne(x => x.MeasureUnit, m => m.Column("MEASURE_UNIT_ID"));
            Property(x => x.Amount);
        }
    }
}