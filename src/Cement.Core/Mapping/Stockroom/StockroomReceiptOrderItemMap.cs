﻿using Cement.Core.Entities.Stockroom;
using NHibernate.Mapping.ByCode.Conformist;

namespace Cement.Core.Mapping.Stockroom
{
    public class StockroomReceiptOrderItemBaseMap : BaseEntityMap<StockroomReceiptOrderItemBase>
    {
        public StockroomReceiptOrderItemBaseMap()
            : base("STOCKROOM_RECEIPT_ORDER_ITEM")
        {
            Discriminator(x =>
            {
                x.Column("TYPE");
                x.NotNullable(true);
            });

            Property(x => x.StockroomReceiptOrderItemType,
                m =>
                {
                    m.Column("TYPE");
                    m.Insert(false);
                    m.Update(false);
                });

            ManyToOne(x => x.Warehouse, m =>
            {
                m.Column("WAREHOUSE_ID");
                m.NotNullable(true);
            });

            ManyToOne(x => x.Product, m =>
            {
                m.Column("PRODUCT_ID");
                m.NotNullable(true);
            });

            ManyToOne(x => x.MeasureUnit, m =>
            {
                m.Column("MEASURE_UNIT_ID");
                m.NotNullable(true);
            });
            
            Property(x => x.Amount);
            Property(x => x.Price);
            Property(x => x.Tax);
            Property(x => x.UsedAt, m => m.Column("USED_AT"));
        }
    }

    public class StockroomReceiptOrderItemInnerMoveMap : SubclassMapping<StockroomReceiptOrderItemInnerMove>
    {
        public StockroomReceiptOrderItemInnerMoveMap()
        {
            DiscriminatorValue((int)StockroomReceiptOrderItemType.InnerMove);

            ManyToOne(x => x.AccountingOtherDoc, m =>
            {
                m.Column("ACCOUNTING_OTHER_ID");
                m.NotNullable(true);
            });

            ManyToOne(x => x.WarehouseFrom, m =>
            {
                m.Column("WAREHOUSE_FROM_ID");
                m.NotNullable(true);
            });
        }
    }

    public class StockroomReceiptOrderItemInnerPostingMap : SubclassMapping<StockroomReceiptOrderItemInnerPosting>
    {
        public StockroomReceiptOrderItemInnerPostingMap()
        {
            DiscriminatorValue((int)StockroomReceiptOrderItemType.InnerPosting);

            ManyToOne(x => x.AccountingOtherDoc, m =>
            {
                m.Column("ACCOUNTING_OTHER_ID");
                m.NotNullable(true);
            });
        }
    }

    public class StockroomReceiptOrderItemOuterMap : SubclassMapping<StockroomReceiptOrderItemOuter>
    {
        public StockroomReceiptOrderItemOuterMap()
        {
            DiscriminatorValue((int)StockroomReceiptOrderItemType.Outer);

            ManyToOne(x => x.AccountingInOutDoc, m =>
            {
                m.Column("ACCOUNTING_INOUT_DOC_ID");
                m.NotNullable(true);
            });
        }
    }
}