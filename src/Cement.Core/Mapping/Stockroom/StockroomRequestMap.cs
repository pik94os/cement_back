﻿using Cement.Core.Entities.Stockroom;
using NHibernate.Mapping.ByCode.Conformist;

namespace Cement.Core.Mapping.Stockroom
{
    public class StockroomRequestRouteMap : DocumentRouteMap<StockroomBaseRequest, StockroomRequestRoute>
    {
        public StockroomRequestRouteMap() : base("STOCKROOM_REQUEST_ROUTE", "STOCKROOM_REQUEST_ID") {}
    }

    public class StockroomBaseRequestMap : BaseEntityMap<StockroomBaseRequest>
    {
        public StockroomBaseRequestMap()
            : base("STOCKROOM_REQUEST")
        {
            Discriminator(x =>
            {
                x.Column("TYPE");
                x.NotNullable(true);
            });

            Property(x => x.StockroomRequestType,
                m =>
                {
                    m.Column("TYPE");
                    m.Insert(false);
                    m.Update(false);
                });
            
            ManyToOne(x => x.Warehouse, m =>
            {
                m.Column("WAREHOUSE_ID");
                m.Update(false);
            });
            ManyToOne(x => x.StockroomProduct, m => m.Column("STOCKROOM_PRODUCT_ID"));
            Property(x => x.No);
            Property(x => x.Date, m => m.Column("REQUEST_DATE"));
            Property(x => x.Amount);
            Property(x => x.Foundation);
            Property(x => x.ExtraData, m => m.Column("EXTRA_DATA"));
            Property(x => x.DocumentState, m => m.Column("STATE"));
            Property(x => x.UsedAt, m => m.Column("USED_AT"));
        }
    }

    public class StockroomWriteoffRequestMap : SubclassMapping<StockroomWriteoffRequest>
    {
        public StockroomWriteoffRequestMap()
        {
            DiscriminatorValue((int)StockroomRequestType.Writeoff);

            Property(x => x.Reason);
        }
    }

    public class StockroomPostingRequestMap : SubclassMapping<StockroomPostingRequest>
    {
        public StockroomPostingRequestMap()
        {
            DiscriminatorValue((int)StockroomRequestType.Posting);

            ManyToOne(x => x.AccountingOtherPostingAct, m => m.Column("ACCOUNTING_OTHER_ID"));
        }
    }

    public class StockroomMovementRequestMap : SubclassMapping<StockroomMovementRequest>
    {
        public StockroomMovementRequestMap()
        {
            DiscriminatorValue((int)StockroomRequestType.Movement);

            ManyToOne(x => x.WarehouseTo, m =>
            {
                m.Column("WAREHOUSE_TO_ID");
                m.NotNullable(true);
            });

            ManyToOne(x => x.WarehouseContactPerson, m =>
            {
                m.Column("WAREHOUSE_CONTACT_ID");
                m.NotNullable(true);
            });
        }
    }

    public class StockroomMakingPostingRequestMap : SubclassMapping<StockroomMakingPostingRequest>
    {
        public StockroomMakingPostingRequestMap()
        {
            DiscriminatorValue((int)StockroomRequestType.MakingPosting);

            ManyToOne(x => x.PriceListProduct, m =>
            {
                m.Column("PRICELIST_PRODUCT_ID");
                m.NotNullable(true);
            });

            ManyToOne(x => x.AccountingOtherPostingAct, m => m.Column("ACCOUNTING_OTHER_ID"));
        }
    }
}