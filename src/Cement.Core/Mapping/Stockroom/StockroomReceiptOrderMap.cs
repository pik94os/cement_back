﻿using Cement.Core.Entities.Stockroom;
using NHibernate.Mapping.ByCode.Conformist;

namespace Cement.Core.Mapping.Stockroom
{
    public class StockroomReceiptOrderRouteMap : DocumentRouteMap<StockroomReceiptOrderBase, StockroomReceiptOrderRoute>
    {
        public StockroomReceiptOrderRouteMap() : base("STOCKROOM_RECEIPT_ORDER_ROUTE", "STOCKROOM_RECEIPT_ORDER_ID") { }
    }

    public class StockroomReceiptOrderMap : BaseEntityMap<StockroomReceiptOrderBase>
    {
        public StockroomReceiptOrderMap() : base("STOCKROOM_RECEIPT_ORDER")
        {
            Discriminator(x =>
            {
                x.Column("TYPE");
                x.NotNullable(true);
            });

            Property(x => x.StockroomReceiptOrderType,
                m =>
                {
                    m.Column("TYPE");
                    m.Insert(false);
                    m.Update(false);
                });

            ManyToOne(x => x.Warehouse, m =>
            {
                m.Column("WAREHOUSE_ID");
                m.NotNullable(true);
            });

            Property(x => x.No);
            Property(x => x.Date, m => m.Column("DOCUMENT_DATE"));
            Property(x => x.Amount);
            Property(x => x.DocumentState, m => m.Column("STATE"));
        }
    }

    public class StockroomReceiptOrderBuyMap : SubclassMapping<StockroomReceiptOrderBuy>
    {
        public StockroomReceiptOrderBuyMap()
        {
            DiscriminatorValue((int)StockroomReceiptOrderType.Buy);

            ManyToOne(x => x.StockroomReceiptOrderItemOuter, m =>
            {
                m.Column("STOCKROOM_RECEIPT_ORDER_ITEM_ID");
                m.NotNullable(true);
            });
        }
    }

    public class StockroomReceiptOrderPostingMap : SubclassMapping<StockroomReceiptOrderPosting>
    {
        public StockroomReceiptOrderPostingMap()
        {
            DiscriminatorValue((int)StockroomReceiptOrderType.Posting);

            ManyToOne(x => x.StockroomReceiptOrderItemPosting, m =>
            {
                m.Column("STOCKROOM_RECEIPT_ORDER_ITEM_ID");
                m.NotNullable(true);
            });
        }
    }

    public class StockroomReceiptOrderMovementMap : SubclassMapping<StockroomReceiptOrderMovement>
    {
        public StockroomReceiptOrderMovementMap()
        {
            DiscriminatorValue((int)StockroomReceiptOrderType.Movement);

            ManyToOne(x => x.StockroomReceiptOrderItemMove, m =>
            {
                m.Column("STOCKROOM_RECEIPT_ORDER_ITEM_ID");
                m.NotNullable(true);
            });
        }
    }
}