﻿using Cement.Core.Entities.Stockroom;

namespace Cement.Core.Mapping.Stockroom
{
    public class InventoryMap : BaseEntityMap<Inventory>
    {
        public InventoryMap() : base("INVENTORY")
        {
            ManyToOne(x => x.Warehouse, m =>
            {
                m.Column("WAREHOUSE_ID");
                m.Update(false);
            });

            ManyToOne(x => x.Chairman, m =>
            {
                m.Column("CHAIRMAN_ID");
                m.Update(false);
            });

            ManyToOne(x => x.Director, m =>
            {
                m.Column("DIRECTOR_ID");
                m.Update(false);
            });

            Property(x => x.No);
            Property(x => x.Date, m => m.Column("INVENTORY_DATE"));
            Property(x => x.DocumentState, m => m.Column("STATE"));
            Property(x => x.Kind);
        }
    }

    public class InventoryRouteMap : DocumentRouteMap<Inventory, InventoryRoute>
    {
        public InventoryRouteMap() : base("INVENTORY_ROUTE", "INVENTORY_ID") { }
    }
}