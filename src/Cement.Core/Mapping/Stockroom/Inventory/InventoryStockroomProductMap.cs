﻿using Cement.Core.Entities.Stockroom;
using NHibernate.Mapping.ByCode.Conformist;

namespace Cement.Core.Mapping.Stockroom
{
    public class InventoryStockroomProductMap : BaseEntityMap<InventoryStockroomProduct>
    {
        public InventoryStockroomProductMap() : base("INVENTORY_PRODUCT")
        {
            ManyToOne(x => x.Inventory, m => m.Column("INVENTORY_ID"));

            ManyToOne(x => x.StockroomProduct, m => m.Column("SR_PRODUCT_ID"));

            Property(x => x.Count, m => m.Column("FACT_COUNT"));
        }
    }
}