﻿using Cement.Core.Entities.Stockroom;
using NHibernate.Mapping.ByCode.Conformist;

namespace Cement.Core.Mapping.Stockroom
{
    public class InventoryMemberMap : BaseEntityMap<InventoryMember>
    {
        public InventoryMemberMap() : base("INVENTORY_MEMBER")
        {
            ManyToOne(x => x.Inventory, m => m.Column("INVENTORY_ID"));

            ManyToOne(x => x.Memeber, m => m.Column("EMPLOYEE_ID"));
        }
    }
}