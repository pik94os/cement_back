﻿using Cement.Core.Entities.Stockroom;
using NHibernate.Mapping.ByCode.Conformist;

namespace Cement.Core.Mapping.Stockroom
{
    public class StockroomExpenseOrderRouteMap : DocumentRouteMap<StockroomExpenseOrderBase, StockroomExpenseOrderRoute>
    {
        public StockroomExpenseOrderRouteMap() : base("STOCKROOM_EXPENSE_ORDER_ROUTE", "STOCKROOM_EXPENSE_ORDER_ID") { }
    }

    public class StockroomExpenseOrderMap : BaseEntityMap<StockroomExpenseOrderBase>
    {
        public StockroomExpenseOrderMap()
            : base("STOCKROOM_EXPENSE_ORDER")
        {
            Discriminator(x =>
            {
                x.Column("TYPE");
                x.NotNullable(true);
            });

            Property(x => x.StockroomExpenseOrderType,
                m =>
                {
                    m.Column("TYPE");
                    m.Insert(false);
                    m.Update(false);
                });
            
            ManyToOne(x => x.Warehouse, m =>
            {
                m.Column("WAREHOUSE_ID");
            });
            
            Property(x => x.No);
            Property(x => x.Date, m => m.Column("DOCUMENT_DATE"));
            Property(x => x.Amount);
            Property(x => x.DocumentState, m => m.Column("STATE"));
        }
    }

    public class StockroomExpenseOrderSaleMap : SubclassMapping<StockroomExpenseOrderSale>
    {
        public StockroomExpenseOrderSaleMap()
        {
            DiscriminatorValue((int)StockroomExpenseOrderType.Sale);

            ManyToOne(x => x.ProductRequest, m =>
            {
                m.Column("PRODUCT_REQUEST_ID");
                m.NotNullable(true);
            });

            ManyToOne(x => x.AccountingInOutDocWayBill, m => m.Column("ACCOUNTING_INOUT_DOC_ID"));
        }
    }

    public class StockroomExpenseOrderMovementMap : SubclassMapping<StockroomExpenseOrderMovement>
    {
        public StockroomExpenseOrderMovementMap()
        {
            DiscriminatorValue((int)StockroomExpenseOrderType.Movement);

            ManyToOne(x => x.StockroomMovementRequest, m =>
            {
                m.Column("STOCKROOM_REQUEST_ID");
                m.NotNullable(true);
            });

            ManyToOne(x => x.AccountingOtherMovementBill, m => m.Column("ACCOUNTING_OTHER_ID"));
        }
    }

    public class StockroomExpenseOrderWriteoffMap : SubclassMapping<StockroomExpenseOrderWriteoff>
    {
        public StockroomExpenseOrderWriteoffMap()
        {
            DiscriminatorValue((int)StockroomExpenseOrderType.Writeoff);

            ManyToOne(x => x.StockroomWriteoffRequest, m =>
            {
                m.Column("STOCKROOM_REQUEST_ID");
                m.NotNullable(true);
            });

            ManyToOne(x => x.AccountingOtherWriteoffAct, m => m.Column("ACCOUNTING_OTHER_ID"));
        }
    }


}