﻿using Cement.Core.Entities.Stockroom;

namespace Cement.Core.Mapping.Stockroom
{
    public class StockroomProductHistoryMap : BaseEntityMap<StockroomProductHistory>
    {
        public StockroomProductHistoryMap()
            : base("STOCKROOM_PRODUCT_HISTORY")
        {
            ManyToOne(x => x.StockroomProduct, m => m.Column("STOCKROOM_PRODUCT_ID"));
            Property(x => x.Date, m => m.Column("HISTORY_DATE"));
            Property(x => x.IsExpense, m => m.Column("IS_EXPENSE"));
            Property(x => x.InitialAmount, m => m.Column("INITIAL_AMOUNT"));
            Property(x => x.InitialPrice, m => m.Column("INITIAL_PRICE"));
            Property(x => x.InitialTax, m => m.Column("INITIAL_TAX"));
            Property(x => x.ChangeAmount, m => m.Column("CHANGE_AMOUNT"));
            Property(x => x.ChangePrice, m => m.Column("CHANGE_PRICE"));
            Property(x => x.ChangeTax, m => m.Column("CHANGE_TAX"));
        }
    }
}