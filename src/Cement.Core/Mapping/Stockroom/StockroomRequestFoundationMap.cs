﻿using Cement.Core.Entities.Stockroom;

namespace Cement.Core.Mapping.Stockroom
{
    public class StockroomRequestFoundationMap : BaseEntityMap<StockroomRequestFoundation>
    {
        public StockroomRequestFoundationMap() : base("STOCKROOM_REQUEST_FOUNDATION")
        {
            ManyToOne(x => x.StockroomRequest, m => m.Column("REQUEST_ID"));
            ManyToOne(x => x.Document, m => m.Column("DOCUMENT_ID"));
        }
    }
}