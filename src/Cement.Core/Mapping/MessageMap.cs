﻿using Cement.Core.Entities;

namespace Cement.Core.Mapping
{
    public class MessageMap : BaseOrganizationBasedEntityMap<Message>
    {
        public MessageMap() 
        {
            Property(x => x.Name);
            Property(x => x.Date, m => m.Column("MESSAGE_DATE"));
            Property(x => x.MessageType, m => m.Column("TYPE"));
            Property(x => x.Subject);
            Property(x => x.Content);
            ManyToOne(x => x.Author, m => m.Column("AUTHOR_ID"));
            ManyToOne(x => x.Creator, m => m.Column("CREATOR_ID"));
            Property(x => x.Importance);
            Property(x => x.Secrecy);
            ManyToOne(x => x.File, m => m.Column("FILE_ID"));
            Property(x => x.DocumentState, m => m.Column("STATE"));

            Property(x => x.RepeatDisplay, m => m.Column("REPEAT_DISPLAY"));
            Property(x => x.RepeatStartAt, m => m.Column("REPEAT_START_AT"));
            Property(x => x.RepeatStopAt, m => m.Column("REPEAT_STOP_AT"));
            Property(x => x.RepeatCount, m => m.Column("REPEAT_COUNT"));
            Property(x => x.RepeatDetails, m => m.Column("REPEAT_DETAILS"));
        }
    }
}