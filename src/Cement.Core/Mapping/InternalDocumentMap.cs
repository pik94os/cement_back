﻿using Cement.Core.Entities;

namespace Cement.Core.Mapping
{
    public class InternalDocumentMap : BaseOrganizationBasedEntityMap<InternalDocument>
    {
        public InternalDocumentMap() : base("INTERNAL_DOCUMENT")
        {
            Property(x => x.Name);
            Property(x => x.No);
            Property(x => x.Date, m => m.Column("DOCUMENT_DATE"));
            Property(x => x.InternalDocumentType, m => m.Column("TYPE"));
            ManyToOne(x => x.Nomenclature, m => m.Column("NOMENCLATURE_ID"));
            Property(x => x.Subject);
            Property(x => x.Content);
            ManyToOne(x => x.Author, m => m.Column("AUTHOR_ID"));
            ManyToOne(x => x.Signer, m => m.Column("SIGNER_ID"));
            Property(x => x.ReviewBefore, m => m.Column("REVIEW_BEFORE"));
            Property(x => x.Importance);
            Property(x => x.Secrecy);
            Property(x => x.Specificity);
            ManyToOne(x => x.File, m => m.Column("FILE_ID"));

            ManyToOne(x => x.Creator, m => m.Column("CREATOR_ID"));
            Property(x => x.DocumentState, m => m.Column("STATE"));
        }
    }
}