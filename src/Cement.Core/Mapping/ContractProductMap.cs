﻿using Cement.Core.Entities;

namespace Cement.Core.Mapping
{
    public class ContractProductMap : BaseEntityMap<ContractProduct>
    {
        public ContractProductMap() : base("CONTRACT_PRODUCT")
        {
            Property(x => x.Price);
            ManyToOne(x => x.Product, m => m.Column("PRODUCT_ID"));
            ManyToOne(x => x.Contract, m => m.Column("CONTRACT_ID"));
            ManyToOne(x => x.Tax, m => m.Column("TAX_ID"));
            ManyToOne(x => x.MeasureUnit, m => m.Column("MEASURE_UNIT_ID"));
        }
    }
}