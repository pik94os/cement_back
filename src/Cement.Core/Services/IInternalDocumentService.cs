﻿using System.Collections.Generic;
using Cement.Core.Entities;
using Cement.Core.Enums;

namespace Cement.Core.Services
{
    public interface IInternalDocumentService
    {
        void SendTo(long documentId, Employee from, Employee to, string comment);

        //void ProcessDocuments(List<long> documentIds, DocumentProcessingResult result, string comment);

        void ProcessDocument(long documentMovementId, DocumentProcessingResult result, string comment, Employee to = null);

        void Reject(long documentMovementId, string comment);

        void Sign(long documentMovementId, string comment);

        void SignOutcoming(long documentId, string comment);

        void MarkAsRead(long documentMovementId);
    }
}