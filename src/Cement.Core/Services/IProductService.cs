﻿using System;
using System.Collections.Generic;
using System.Linq;
using Cement.Core.Entities;

namespace Cement.Core.Services
{
    public interface IProductService
    {
        Tuple<IList<ProductDataProxy>, int> GetBottomList(IQueryable<Product> stockroomProductQuery, StoreParams.StoreParams storeParams, bool separateCountQuery = true);
    }

    public class ProductDataProxy
    {
        public long id { get; set; }

        public string p_name { get; set; }

        public string p_group { get; set; }

        public string p_subgroup { get; set; }

        public string p_trade_mark { get; set; }

        public string p_manufacturer { get; set; }

        public string p_unit_display { get; set; }
    }
}