﻿using System.IO;
using Cement.Core.Entities;
using DataAccess.Config;
using DataAccess.Repository;

namespace Cement.Core.Services
{
    public class EntityFileService : IEntityFileService
    {
        public ConfigProvider ConfigProvider { get; set; }
        
        public IRepository<EntityFile> EntityFileRepository { get; set; }
        
        public EntityFile SaveEntityFile(EntityFileWithData data)
        {
            var entityFile = new EntityFile
            {
                Name = data.Name,
                Extension = data.Extension,
                Size = data.Size,
                CheckSum = data.CheckSum
            };
            
            EntityFileRepository.Save(entityFile);

            var fileName = Path.Combine(ConfigProvider.FilesDirectory.FullName, string.Format("{0}.{1}", entityFile.Id, entityFile.Extension));

            File.WriteAllBytes(fileName, data.Data);

            return entityFile;
        }

        public virtual DownloadResult LoadFile(long id)
        {
            var entity = EntityFileRepository.Get(id);
            if (entity == null)
            {
                return new DownloadResult { ResultCode = ResultCode.FileNotFound };
            }

            var fileInfo = new System.IO.FileInfo(Path.Combine(ConfigProvider.FilesDirectory.FullName, string.Format("{0}.{1}", id, entity.Extension)));
            if (!fileInfo.Exists)
            {
                throw new FileNotFoundException();
            }

            return new DownloadResult
            {
                ResultCode = ResultCode.Success,
                FileDownloadName = string.Format("{0}.{1}", entity.Name, entity.Extension),
                Path = fileInfo.FullName
            };
        }
    }
}