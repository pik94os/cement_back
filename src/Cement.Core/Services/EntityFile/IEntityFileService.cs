﻿using Cement.Core.Entities;

namespace Cement.Core.Services
{
    public interface IEntityFileService
    {
        EntityFile SaveEntityFile(EntityFileWithData data);

        DownloadResult LoadFile(long id);
    }
}