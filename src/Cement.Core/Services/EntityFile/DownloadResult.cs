﻿using System;
using System.Web;
using System.Web.Mvc;

namespace Cement.Core.Services
{
    /// <summary>
    /// Код результата загрузки файла.
    /// </summary>
    public enum ResultCode
    {
        /// <summary>
        /// Успех.
        /// </summary>
        Success = 200,

        /// <summary>
        /// Файл не найден.
        /// </summary>
        FileNotFound = 404,

        /// <summary>
        /// Другая ошибка.
        /// </summary>
        OtherError = 500
    }

    /// <summary>
    /// Результат загрузки файла.
    /// </summary>
    public class DownloadResult : ActionResult
    {
        /// <summary>
        /// Создает экземпляр класса.
        /// </summary>
        public DownloadResult()
        {
        }

        /// <summary>
        /// Создает экземпляр класса.
        /// </summary>
        /// <param name="path">
        /// Путь.
        /// </param>
        public DownloadResult(string path)
        {
            this.Path = path;
        }

        /// <summary>
        /// Имя файла.
        /// </summary>
        public string FileDownloadName { get; set; }

        /// <summary>
        /// Путь.
        /// </summary>
        public string Path { get; set; }

        /// <summary>
        /// Код результата загрузки файла.
        /// </summary>
        public ResultCode ResultCode { get; set; }

        public static string GetBrowserName(HttpRequestBase request)
        {
            if (request == null)
            {
                throw new ArgumentNullException("request");
            }

            string userAgent = request.ServerVariables["HTTP_USER_AGENT"];

            if (string.IsNullOrEmpty(userAgent))
            {
                return "Unknown";
            }

            string result;

            if (userAgent.Contains("Chrome"))
            {
                result = "Chrome";
            }
            else if (userAgent.Contains("Avant Browser"))
            {
                result = "Avant Browser";
            }
            else if (userAgent.Contains("Googlebot"))
            {
                result = "Googlebot";
            }
            else if (userAgent.Contains("Yahoo! Slurp"))
            {
                result = "Yahoo! Slurp";
            }
            else if (userAgent.Contains("Mediapartners-Google"))
            {
                result = "Mediapartners-Google";
            }
            else if (userAgent.Contains("msnbot"))
            {
                result = "msnbot";
            }
            else if (userAgent.Contains("SurveyBot"))
            {
                result = "SurveyBot/2.3 (Whois Source)";
            }
            else if (userAgent.Contains("Baiduspider"))
            {
                result = "Baiduspider";
            }
            else if (userAgent.Contains("FeedFetcher-Google"))
            {
                result = "FeedFetcher-Google";
            }
            else if (userAgent.Contains("ia_archiver"))
            {
                result = "ia_archiver";
            }
            else
            {
                result = request.Browser.Browser + " " + request.Browser.Version;
            }

            return result;
        }

        public override void ExecuteResult(ControllerContext context)
        {
            var fileName = this.FileDownloadName ?? string.Empty;

            fileName = fileName.Replace("\"", "'");

            string agent = GetBrowserName(context.HttpContext.Request);

            // Хак для отображения русских имен файлов
            if (agent.StartsWith("IE"))
            {
                fileName = context.HttpContext.Server.UrlEncode(this.FileDownloadName);
                fileName = fileName.Replace("+", "%20");
            }

            switch (this.ResultCode)
            {
                case ResultCode.Success:
                    var extention = System.IO.Path.GetExtension(fileName) ?? string.Empty;
                    switch (extention)
                    {
                        /*case ".bmp":
                            context.HttpContext.Response.ContentType = "Image/bmp";
                            break;
                        case ".gif":
                            context.HttpContext.Response.ContentType = "Image/gif";
                            break;
                        case ".jpg":
                            context.HttpContext.Response.ContentType = "Image/jpeg";
                            break;
                        case ".png":
                            context.HttpContext.Response.ContentType = "Image/png";
                            break;
                        case ".tif":
                            context.HttpContext.Response.ContentType = "Image/tiff";
                            break;
                        case ".pdf":
                            context.HttpContext.Response.ContentType = "application/pdf";
                            break;*/
                        default:
                            context.HttpContext.Response.ContentType = "application/octet-stream";
                            context.HttpContext.Response.AddHeader(
                                "Content-Disposition", "attachment; filename=\"" + fileName + "\"");
                            break;
                    }

                    context.HttpContext.Response.StatusCode = 200;
                    context.HttpContext.Response.TransmitFile(this.Path);
                    break;

                case ResultCode.FileNotFound:
                    context.HttpContext.Response.StatusCode = 404;
                    context.HttpContext.Response.Write("File not found");
                    break;
            }
        }
    }
}