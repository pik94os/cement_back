﻿using System;
using System.Collections.Generic;
using System.Linq;
using Cement.Core.Entities.Stockroom;

namespace Cement.Core.Services
{
    public interface IStockroomProductService
    {
        Tuple<IList<StockroomProductDataProxy>, int> GetBottomList(IQueryable<StockroomProduct> stockroomProductQuery, StoreParams.StoreParams storeParams, bool separateCountQuery = true);
    }

    public class StockroomProductDataProxy
    {
        public long id { get; set; }

        public string p_name { get; set; }

        public string p_group { get; set; }

        public string p_subgroup { get; set; }

        public string p_trade_mark { get; set; }

        public string p_manufacturer { get; set; }

        public string p_unit_display { get; set; }

        public decimal? p_tax { get; set; }

        public decimal? p_price { get; set; }
    }
}