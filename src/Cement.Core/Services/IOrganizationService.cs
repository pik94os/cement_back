﻿using System;
using System.Collections.Generic;
using System.Linq;
using Cement.Core.Entities;

namespace Cement.Core.Services
{
    public interface IOrganizationService
    {
        Tuple<IList<OrganizationDataProxy>, int> GetBottomList(IQueryable<Organization> organizationQuery, StoreParams.StoreParams storeParams, bool separateCountQuery = true);
    }

    public class OrganizationDataProxy
    {
        public long id { get; set; }

        public string p_name { get; set; }

        public string p_group { get; set; }

        public string p_inn { get; set; }

        public string p_kpp { get; set; }

        public string p_address { get; set; }

        public string p_ogrn { get; set; }

        public string p_kind { get; set; }
    }
}