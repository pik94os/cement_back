﻿using Cement.Core.Entities;

namespace Cement.Core.Services
{
    public interface ISignAuthorityService
    {
        bool CanSignFor(long? trusterEmployeeId, Employee employee);

        void UpdateCache();
    }
}