﻿using System.Net.Mail;
using System.Text;

namespace Cement.Core.Services.Mail.Impl
{
    public class MailSenderSmtp : IMailSender
    {
        public static readonly Encoding MailEncoding = Encoding.UTF8;

        public void Send(string fromName, string fromMail, string subject, string body, string email)
        {
            using (var client = new SmtpClient())
            {
                var from = new MailAddress(fromMail, fromName, MailEncoding);
                // Set destinations for the e-mail message.
                var to = new MailAddress(email);
                // Specify the message content.
                var mail = new MailMessage(from, to);
                mail.Body = body;
                mail.IsBodyHtml = true;
                // Include some non-ASCII characters in body and subject.
                mail.BodyEncoding = MailEncoding;
                mail.Subject = subject;
                mail.SubjectEncoding = MailEncoding;

                //client.Host = Config.MailServerHost;
                //client.Port = Config.MailServerPort;
                //client.EnableSsl = true;
                //client.Credentials = new NetworkCredential(Config.MailFrom.Split('@')[0], Config.MailPassword);
                //client.DeliveryMethod = SmtpDeliveryMethod.Network;
                client.Send(mail);
            }
        }

        public void Send(string subject, string body, string email)
        {
            using (var client = new SmtpClient())
            {
                var mail = new MailMessage();
                mail.To.Add(new MailAddress(email));
                mail.Body = body;
                mail.IsBodyHtml = true;
                // Include some non-ASCII characters in body and subject.
                mail.BodyEncoding = MailEncoding;
                mail.Subject = subject;
                mail.SubjectEncoding = MailEncoding;
                client.Send(mail);
            }
        }
    }
}