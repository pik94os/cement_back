﻿namespace Cement.Core.Services.Mail
{
    public interface IMailSender
    {
        void Send(string fromName, string fromMail, string subject, string body, string email);

        void Send(string subject, string body, string email);
    }
}