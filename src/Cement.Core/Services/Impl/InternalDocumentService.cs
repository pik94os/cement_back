﻿using System;
using System.Collections.Generic;
using System.Linq;
using Castle.Core.Internal;
using Cement.Core.Auth;
using Cement.Core.Entities;
using Cement.Core.Enums;
using Cement.Core.Extensions;
using Cement.Core.Services.EntityService;
using NHibernate.Linq;

namespace Cement.Core.Services.Impl
{
    public class InternalDocumentService : BaseService, IInternalDocumentService
    {
        public IEntityService<IncomingInternalDocument> IncomingInternalDocumentEntityService { get; set; }

        public IEntityService<OutcomingInternalDocument> OutcomingInternalDocumentEntityService { get; set; }

        public IEntityService<InternalDocumentMovement> InternalDocumentMovementEntityService { get; set; }

        public IEntityService<InternalDocumentRoute> InternalDocumentRouteEntityService { get; set; }

        public IEntityService<InternalDocument> InternalDocumentEntityService { get; set; }

        public IEntityService<DocumentRelation> DocumentRelationEntityService { get; set; }

        public IEntityService<DisposalRecipient> DisposalRecipientEntityService { get; set; }

        public IEntityService<PriceList> PriceListEntityService { get; set; }


        public void SendTo(long documentId, Employee from, Employee to, string comment)
        {
            InTransaction(() => SendToInternal(documentId, from, to, comment));
        }

        /// <summary>
        /// Начало движения документа
        /// </summary>
        /// <param name="documentId"></param>
        /// <param name="from"></param>
        /// <param name="to"></param>
        /// <param name="comment"></param>
        protected void SendToInternal(long documentId, Employee from, Employee to, string comment)
        {
            var internalDoc = InternalDocumentEntityService.Load(documentId);

            // Если документ еще в "новых", то убираем его оттуда
            if (internalDoc.DocumentState == DocumentState.Draft)
            {
                internalDoc.DocumentState = DocumentState.OnAgreement;

                //InternalDocumentEntityService.Update(internalDoc);
            }

            // Добавляем документ в исходящие отправителя
            var outcomingDoc = new OutcomingInternalDocument
            {
                Document = internalDoc,
                Employee = from,
                Meeting = to
            };
            OutcomingInternalDocumentEntityService.Save(outcomingDoc);

            // Создаем запись маршрута - от инициатора
            var route = new InternalDocumentRoute
            {
                DateIn = DateTime.Now,
                DateOut = DateTime.Now,
                Document = internalDoc,
                Employee = from,
                Comment = comment.GetBytes(),
                ProcessingResult = DocumentProcessingResult.Agreed
            };

            InternalDocumentRouteEntityService.Save(route);

            MoveDocument(internalDoc, from, to);
        }

        protected void MoveDocument(InternalDocument internalDoc, Employee from, Employee to)
        {
            // Добавляем запись в маршрут документа
            var docRoute = new InternalDocumentRoute
            {
                DateIn = DateTime.Now,
                Document = internalDoc,
                Employee = to
            };

            // Пока эту логику убрали
            //// Добавляем документ в исходящие отправителя
            //var outcomingDoc = new OutcomingInternalDocument
            //{
            //    Document = internalDoc,
            //    Employee = from
            //};
            // OutcomingInternalDocumentEntityService.Save(outcomingDoc);

            // Добавляем документ во входящие получателя
            var incomingDoc = new IncomingInternalDocument
            {
                Document = internalDoc,
                Employee = to,
                Meeting = from,
                Route = docRoute
            };

            InternalDocumentRouteEntityService.Save(docRoute);
            IncomingInternalDocumentEntityService.Save(incomingDoc);
        }

        public void ProcessDocument(long documentMovementId, DocumentProcessingResult result, string comment, Employee to = null)
        {
            var documentMovement = InternalDocumentMovementEntityService.GetAll()
                .Where(x => x.Id == documentMovementId)
                .Fetch(x => x.Document)
                .Fetch(x => x.Employee)
                .Fetch(x => x.Route)
                .FirstOrDefault();

            if (documentMovement.Route.ProcessingResult == DocumentProcessingResult.None)
            {
                InTransaction(() =>
                {
                    switch (result)
                    {
                        case DocumentProcessingResult.Signed:
                            Sign(documentMovement, comment);
                            break;

                        case DocumentProcessingResult.Rejected:
                            Reject(documentMovement, comment);
                            break;

                        case DocumentProcessingResult.Agreed:
                            AgreeAndSendNext(documentMovement, to, comment);
                            break;
                    }
                });
            }
        }

        public void Reject(long documentMovementId, string comment)
        {
            var documentMovement = InternalDocumentMovementEntityService.GetAll()
                .Where(x => x.Id == documentMovementId)
                .Fetch(x => x.Document)
                .Fetch(x => x.Employee)
                .Fetch(x => x.Route)
                .FirstOrDefault();
            
            if (documentMovement.Route.ProcessingResult == DocumentProcessingResult.None)
            {
                InTransaction(() => Reject(documentMovement, comment));
            }
        }

        public void Sign(long documentMovementId, string comment)
        {
            var documentMovement = InternalDocumentMovementEntityService.GetAll()
                .Where(x => x.Id == documentMovementId)
                .Fetch(x => x.Document)
                .Fetch(x => x.Employee)
                .Fetch(x => x.Route)
                .FirstOrDefault();

            if (documentMovement.Route.ProcessingResult == DocumentProcessingResult.None)
            {
                InTransaction(() => Sign(documentMovement, comment));
            }
        }

        public void SignOutcoming(long documentId, string comment)
        {
            var internalDoc = InternalDocumentEntityService.Load(documentId);

            InTransaction(() => SignOutcoming(internalDoc, comment));
        }

        protected void SignOutcoming(InternalDocument internalDoc, string comment)
        {
            var currentEmployee = Container.Resolve<IUserPrincipal>().Employee;

            // Добавляем документ в исходящие отправителя
            var outcomingDoc = new OutcomingInternalDocument
            {
                Document = internalDoc,
                Employee = currentEmployee
            };
            OutcomingInternalDocumentEntityService.Save(outcomingDoc);

            // Создаем запись маршрута - от инициатора
            var route = new InternalDocumentRoute
            {
                DateIn = DateTime.Now,
                DateOut = DateTime.Now,
                Document = internalDoc,
                Employee = currentEmployee,
                Comment = comment.GetBytes(),
                ProcessingResult = DocumentProcessingResult.Signed
            };

            InternalDocumentRouteEntityService.Save(route);

            internalDoc.DocumentState = DocumentState.Signed;
            //InternalDocumentEntityService.Update(internalDoc);

            SendToRecipients(internalDoc);
        }

        /// <summary>
        /// Отправить документ на дальнейшее согласование (либо подпись)
        /// </summary>
        protected void AgreeAndSendNext(InternalDocumentMovement docMovement, Employee to, string comment)
        {
            var route = docMovement.Route;

            // Обновляем значение исходящего времени и статус обработки текущего этапа маршрута
            if (route != null)
            {
                route.ProcessingResult = DocumentProcessingResult.Agreed;
                route.DateOut = DateTime.Now;
                route.Comment = comment.GetBytes();
                //InternalDocumentRouteEntityService.Update(route);
            }

            MoveDocument(docMovement.Document, docMovement.Employee, to);
        }

        /// <summary>
        /// Подписать документ
        /// </summary>
        protected void Sign(InternalDocumentMovement docMovement, string comment)
        {
            var route = docMovement.Route;
            var document = docMovement.Document;

            // Обновляем значение исходящего времени и статус обработки текущего этапа маршрута
            if (route != null)
            {
                route.ProcessingResult = DocumentProcessingResult.Signed;
                route.DateOut = DateTime.Now;
                route.Comment = comment.GetBytes();
                //InternalDocumentRouteEntityService.Update(route);
            }

            //Обновляем статус документа на "Подписано"
            if (document != null)
            {
                document.DocumentState = DocumentState.Signed;
                //InternalDocumentEntityService.Update(document);

                ChangePriceListStatus(document);
                SendToRecipients(document);
            }
        }

        /// <summary>
        /// Помечает прайс-лист как "Действующий" при подписании родительского письма
        /// </summary>
        private void ChangePriceListStatus(InternalDocument document)
        {
            if (document.Type == DocumentType.Message && document.Specificity == Specificity.MessageWithPriceList)
            {
                DocumentRelationEntityService.GetAll()
                    .Join(PriceListEntityService.GetAll(),
                        x => x.JoinedDocId,
                        y => y.Id,
                        (a, b) => new
                        {
                            priceList = b,
                            a.BaseDocumentId,
                            a.JoinedDocumentType
                        }
                    )
                    .Where(x => x.BaseDocumentId == document.Id)
                    .Where(x => x.JoinedDocumentType == DocumentType.PriceList)
                    .Where(x => x.priceList.Status == PriceListStatus.New)
                    .Select(x => x.priceList)
                    .AsEnumerable()
                    .ForEach(priceList =>
                    {
                        priceList.Status = PriceListStatus.Working;

                        PriceListEntityService.Update(priceList);
                    });
            }
        }

        /// <summary>
        /// Отклонить документ
        /// </summary>
        protected void Reject(InternalDocumentMovement docMovement, string comment)
        {
            var route = docMovement.Route;
            var document = docMovement.Document;

            // Обновляем значение исходящего времени и статус обработки текущего этапа маршрута
            if (route != null)
            {
                route.ProcessingResult = DocumentProcessingResult.Rejected;
                route.DateOut = DateTime.Now;
                route.Comment = comment.GetBytes();
                //InternalDocumentRouteEntityService.Update(route);
            }

            //Обновляем статус документа на "Отклонено"
            if (document != null)
            {
                document.DocumentState = DocumentState.Rejected;
                //InternalDocumentEntityService.Update(document);
            }
        }

        protected void SendToRecipients(InternalDocument disposal)
        {
            if (disposal.InternalDocumentType != InternalDocumentType.Disposal)
            {
                return;
            }

            // получатели
            var recipientIds = DisposalRecipientEntityService.GetAll()
                .Where(x => x.Disposal == disposal)
                .Select(x => x.Recipient.Id)
                .ToList();

            // Добавляем приказ во входящие получателя
            recipientIds.ForEach(x =>
            {
                var employee = new Employee { Id = x };

                var route = new InternalDocumentRoute
                {
                    DateIn = DateTime.Now,
                    Document = disposal,
                    Employee = employee,
                    ProcessingResult = DocumentProcessingResult.OnAcquaintance
                };

                var incomingDoc = new IncomingInternalDocument
                {
                    Document = disposal,
                    Employee = employee,
                    Route = route
                };

                InternalDocumentRouteEntityService.Save(route);
                IncomingInternalDocumentEntityService.Save(incomingDoc);
            });
        }

        public void MarkAsRead(long disposalMovementId)
        {
            var disposalMovement = InternalDocumentMovementEntityService.Get(disposalMovementId);

            if (disposalMovement.Document.InternalDocumentType != InternalDocumentType.Disposal)
            {
                return;
            }

            var route = disposalMovement.Route;

            if (route != null && route.ProcessingResult == DocumentProcessingResult.OnAcquaintance)
            {
                route.DateOut = DateTime.Now;
                route.ProcessingResult = DocumentProcessingResult.Acquainted;
                InternalDocumentRouteEntityService.Update(route);
            }
        }
    }
}