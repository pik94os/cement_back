﻿using System;
using Cement.Core.Entities.Stockroom;
using Cement.Core.Enums;

namespace Cement.Core.Services.Impl
{
    public class StockroomRequestService : RoutedDocumentProcessingService<StockroomBaseRequest, StockroomRequestRoute>
    {
        protected override void OnSign(StockroomBaseRequest document, StockroomRequestRoute docRoute)
        {
            if (document is StockroomMovementRequest)
            {
                if (document.DocumentState == DocumentState.OnSecondSideAgreement)
                {
                    document.DocumentState = DocumentState.Signed;
                }
                else
                {
                    // Заявка отправляется на второй склад на подписание
                    document.DocumentState = DocumentState.OnSecondSideAgreement;

                    // Добавляем запись в маршрут документа (во входящие получателя)
                    var newRoute = new StockroomRequestRoute
                    {
                        DateIn = DateTime.Now,
                        Document = document,
                        Employee = (document as StockroomMovementRequest).WarehouseContactPerson,
                        Stage = docRoute.Stage + 1,
                        MovementType = DocumentMovementType.Incoming
                    };

                    DocumentRouteEntityService.Save(newRoute);
                }
            }
            else
            {
                document.DocumentState = DocumentState.Signed;
            }
        }
    }
}