﻿using System;
using System.Linq;
using Cement.Core.Entities;
using Cement.Core.Enums;
using Cement.Core.Services.EntityService;
using NHibernate.Linq;

namespace Cement.Core.Services.Impl
{
    public class RoutedDocumentProcessingService<T, TK> : BaseService, IRoutedDocumentProcessingService
        where T : BaseEntity, IDocument
        where TK : DocumentRoute<T> , new ()
    {
        public IEntityService<T> DocumentEntityService { get; set; }
        public IEntityService<TK> DocumentRouteEntityService { get; set; }

        protected virtual bool IsCorrespondence { get { return false; }}

        public void ProcessDocument(long documentRouteId, DocumentProcessingResult result, string comment, Employee to = null)
        {
            var documentMovement = DocumentRouteEntityService.GetAll()
                .Where(x => x.Id == documentRouteId)
                .Fetch(x => x.Document)
                .Fetch(x => x.Employee)
                .FirstOrDefault();

            InTransaction(() =>
            {
                switch (result)
                {
                    case DocumentProcessingResult.Signed:
                        Sign(documentMovement, comment);
                        break;

                    case DocumentProcessingResult.Rejected:
                        Reject(documentMovement, comment);
                        break;

                    case DocumentProcessingResult.Agreed:
                        AgreeAndSendNext(documentMovement, to, comment);
                        break;
                }
            });
        }

        protected virtual void AgreeAndSendNext(TK docRoute, Employee to, string comment)
        {
            docRoute.ProcessingResult = DocumentProcessingResult.Agreed;
            docRoute.DateOut = DateTime.Now;
            docRoute.Comment = comment;

            // Добавляем запись в маршрут документа 
            // Если это корреспонденция, то тип направления документа сохраняется
            // иначе создаем во входящих получателя
            var newRoute = new TK
            {
                DateIn = DateTime.Now,
                Document = docRoute.Document,
                Employee = to,
                Stage = docRoute.Stage + 1,
                MovementType = IsCorrespondence ? docRoute.MovementType : DocumentMovementType.Incoming
            };

            DocumentRouteEntityService.Save(newRoute);
        }

        public void Reject(long documentRouteId, string comment)
        {
            ProcessDocument(documentRouteId, DocumentProcessingResult.Rejected, comment);
        }

        protected virtual void Reject(TK docRoute, string comment)
        {
            // Обновляем значение исходящего времени и статус обработки текущего этапа маршрута
            docRoute.ProcessingResult = DocumentProcessingResult.Rejected;
            docRoute.DateOut = DateTime.Now;
            docRoute.Comment = comment;

            //Обновляем статус документа на "Отклонено"
            var document = docRoute.Document;
            if (document != null)
            {
                OnReject(document);
            }
        }

        protected virtual void OnReject(T document)
        {
            document.DocumentState = DocumentState.Rejected;
        }

        public virtual void Sign(long documentRouteId, string comment)
        {
            ProcessDocument(documentRouteId, DocumentProcessingResult.Signed, comment);
        }

        protected virtual void Sign(TK docRoute, string comment)
        {
            // Обновляем значение исходящего времени и статус обработки текущего этапа маршрута
            docRoute.ProcessingResult = DocumentProcessingResult.Signed;
            docRoute.DateOut = DateTime.Now;
            docRoute.Comment = comment;

            //Обновляем статус документа на "Подписано"
            var document = docRoute.Document;
            if (document != null)
            {
                OnSign(document, docRoute);
            }
        }

        protected virtual void OnSign(T document, TK docRoute)
        {
            document.DocumentState = DocumentState.Signed;
        }
    }
}