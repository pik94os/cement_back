﻿using System;
using System.Linq;
using Cement.Core.Entities;
using Cement.Core.Services.EntityService;

namespace Cement.Core.Services.Impl
{
    public class VehicleService : IVehicleService
    {
        public IEntityService<VehicleArend> VehicleArendEntityService { get; set; }
        public IEntityService<Vehicle> VehicleEntityService { get; set; }

        public IQueryable<Vehicle> GetVehicleQuery(Organization organization)
        {
            return GetVehicleQuery(organization, DateTime.Now);
        }

        public IQueryable<Vehicle> GetVehicleQuery(Organization organization, DateTime dateTime)
        {
            if (organization == null)
            {
                return VehicleEntityService.GetAll().Where(x => false);
            }

            var date = dateTime.Date;

            // Запрос активных аренд
            var activeArendQuery = VehicleArendEntityService.GetAll()
                .Where(x => x.DateStart == null || x.DateStart <= date)
                .Where(x => x.DateEnd == null || x.DateEnd >= date)
                .Where(x => x.IsActive);

            // ТС, арендованные организацией
            var activeArendVehiclesQuery = activeArendQuery
                .Where(x => x.Arendator == organization.Client)
                .Select(x => x.Vehicle.Id);

            // Запрос ТС, доступных для организации
            var availableVehicleQuery = VehicleEntityService.GetAll()
                .Where(x => activeArendVehiclesQuery.Contains(x.Id) ||
                     (x.Organization.Id == organization.Id && !activeArendQuery.Any(y => y.Vehicle == x)));

            return availableVehicleQuery;
        }

        public IQueryable<Vehicle> GetOwnVehicleQuery(Organization organization, DateTime dateTime)
        {
            if (organization == null)
            {
                return VehicleEntityService.GetAll().Where(x => false);
            }

            var date = dateTime.Date;

            // Запрос активных аренд
            var activeArendQuery = VehicleArendEntityService.GetAll()
                .Where(x => x.DateStart == null || x.DateStart <= date)
                .Where(x => x.DateEnd == null || x.DateEnd >= date)
                .Where(x => x.IsActive);
            
            // Запрос собственных ТС, доступных для организации (в аренду не переданы)
            var availableVehicleQuery = VehicleEntityService.GetAll()
                .Where(x => x.Organization.Id == organization.Id)
                .Where(x => !activeArendQuery.Any(y => y.Vehicle == x));

            return availableVehicleQuery;
        }

        public IQueryable<Vehicle> GetOwnVehicleQuery(Organization organization)
        {
            return GetOwnVehicleQuery(organization, DateTime.Now);
        }

        public IQueryable<VehicleArend> GetArendedVehicleQuery(Organization organization, DateTime dateTime)
        {
            if (organization == null)
            {
                return VehicleArendEntityService.GetAll().Where(x => false);
            }

            var date = dateTime.Date;

            // Активные аренды организации
            var query = VehicleArendEntityService.GetAll()
                .Where(x => x.DateStart == null || x.DateStart <= date)
                .Where(x => x.DateEnd == null || x.DateEnd >= date)
                .Where(x => x.IsActive)
                .Where(x => x.Arendator == organization.Client);

            return query;
        }

        public IQueryable<VehicleArend> GetArendedVehicleQuery(Organization organization)
        {
            return GetArendedVehicleQuery(organization, DateTime.Now);
        }

        public IQueryable<VehicleArend> GetGivenToArendVehicleQuery(Organization organization, DateTime dateTime)
        {
            if (organization == null)
            {
                return VehicleArendEntityService.GetAll().Where(x => false);
            }

            var date = dateTime.Date;
            
            // Запрос ТС переданнных в аренду
            var query = VehicleArendEntityService.GetAll()
                .Where(x => x.DateStart == null || x.DateStart <= date)
                .Where(x => x.DateEnd == null || x.DateEnd >= date)
                .Where(x => x.IsActive)
                .Where(x => x.Vehicle.Organization.Id == organization.Id);

            return query;
        }

        public IQueryable<VehicleArend> GetGivenToArendVehicleQuery(Organization organization)
        {
            return GetGivenToArendVehicleQuery(organization, DateTime.Now);
        }

    }
}