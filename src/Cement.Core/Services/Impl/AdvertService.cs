﻿using System;
using System.Linq;
using Cement.Core.Auth;
using Cement.Core.Entities;
using Cement.Core.Enums;
using Cement.Core.Extensions;
using Cement.Core.Services.EntityService;
using DataAccess.Repository;

namespace Cement.Core.Services.Impl
{
    public class AdvertService : BaseService, IAdvertService
    {
        public IRepository<Advert> AdvertRepository { get; set; }

        public IEntityService<AdvertRecipient> AdvertRecipientEntityService { get; set; }

        public IEntityService<IncomingAdvert> IncomingAdvertEntityService { get; set; }

        public IEntityService<OutcomingAdvert> OutcomingAdvertEntityService { get; set; }

        public IEntityService<AdvertMovement> AdvertMovementEntityService { get; set; }

        public IEntityService<AdvertRoute> AdvertRouteEntityService { get; set; }

        ///// <summary>
        ///// Согласование из вкладки "Новые"
        ///// </summary>
        //public void Process(long advertId, bool send, long? employeeId, string comment)
        //{
        //    var advert = AdvertRepository.Load(advertId);

        //    var currentEmployee = Container.Resolve<IUserPrincipal>().Employee;

        //    InTransaction(() =>
        //    {
        //        // Создаем запись маршрута - от инициатора
        //        var route = new AdvertRoute
        //        {
        //            DateIn = DateTime.Now,
        //            DateOut = DateTime.Now,
        //            Document = advert,
        //            Employee = currentEmployee,
        //            Comment = comment.GetBytes(),
        //            ProcessingResult = DocumentProcessingResult.Agreed
        //        };

        //        AdvertRouteEntityService.Save(route);
                
        //        advert.DocumentState = DocumentState.OnAgreement;

        //        AdvertRepository.Update(advert);

        //        if (send)
        //        {
        //            if (employeeId == null)
        //            {
        //                throw new ValidationException("Не указан согласующий");
        //            }

        //            SendToAgree(advert, currentEmployee, new Employee { Id = employeeId.Value });
        //        }
        //        else
        //        {
        //            // Добавляем документ в исходящие отправителя
        //            var outcomingDoc = new OutcomingAdvert
        //            {
        //                Document = advert,
        //                Employee = currentEmployee
        //            };

        //            OutcomingAdvertEntityService.Save(outcomingDoc);

        //            SendAdvertToRecipients(advert);
        //        }
        //    });
        //}

        ///// <summary>
        ///// Согласование из вкладки "Входящие"
        ///// </summary>
        //public void ProcessFromMovement(long advertMovementId, bool send, long? employeeId, string comment)
        //{
        //    var advertMovement = AdvertMovementEntityService.Get(advertMovementId);
        //    var currentEmployee = Container.Resolve<IUserPrincipal>().Employee;
        //    var advert = advertMovement.Document;
        //    var route = advertMovement.Route;

        //    if (route != null)
        //    {
        //        route.ProcessingResult = DocumentProcessingResult.Agreed;
        //        route.DateOut = DateTime.Now;
        //        route.Comment = comment.GetBytes();
        //        AdvertRouteEntityService.Update(route);
        //    }

        //    if (send)
        //    {
        //        if (employeeId == null)
        //        {
        //            throw new ValidationException("Не указан согласующий");
        //        }

        //        SendToAgree(advert, currentEmployee, new Employee { Id = employeeId.Value });
        //    }
        //    else
        //    {
        //        SendAdvertToRecipients(advert);
        //    }
        //}

        //protected void SendToAgree(Advert advert, Employee from, Employee to)
        //{
        //    // Добавляем запись в маршрут документа
        //    var docRoute = new AdvertRoute
        //    {
        //        DateIn = DateTime.Now,
        //        Document = advert,
        //        Employee = to
        //    };

        //    // Добавляем документ в исходящие отправителя
        //    var outcomingDoc = new OutcomingAdvert
        //    {
        //        Document = advert,
        //        Employee = from
        //    };

        //    // Добавляем документ во входящие получателя
        //    var incomingDoc = new IncomingAdvert
        //    {
        //        Document = advert,
        //        Employee = to,
        //        Route = docRoute
        //    };

        //    AdvertRouteEntityService.Save(docRoute);
        //    OutcomingAdvertEntityService.Save(outcomingDoc);
        //    IncomingAdvertEntityService.Save(incomingDoc);
        //}

        public void Sign(long advertId, string comment)
        {
            var advert = AdvertRepository.Load(advertId);

            var currentEmployee = Container.Resolve<IUserPrincipal>().Employee;

            InTransaction(() =>
            {
                // Создаем запись маршрута - от инициатора
                var route = new AdvertRoute
                {
                    DateIn = DateTime.Now,
                    DateOut = DateTime.Now,
                    Document = advert,
                    Employee = currentEmployee,
                    ProcessingResult = DocumentProcessingResult.Signed,
                    Comment = comment.GetBytes()
                };

                AdvertRouteEntityService.Save(route);

                advert.DocumentState = DocumentState.OnAgreement;

                AdvertRepository.Update(advert);

                // Добавляем документ в исходящие отправителя
                var outcomingDoc = new OutcomingAdvert
                {
                    Document = advert,
                    Employee = currentEmployee
                };

                OutcomingAdvertEntityService.Save(outcomingDoc);

                SendAdvertToRecipients(advert);
            });
        }

        protected void SendAdvertToRecipients(Advert advert)
        {
            // получатели
            var recipientIds = AdvertRecipientEntityService.GetAll()
                .Where(x => x.Advert == advert)
                .Select(x => x.Recipient.Id)
                .ToList();
            
            // Добавляем сообщение во входящие получателя
            recipientIds.ForEach(x =>
            {
                var employee = new Employee { Id = x };

                var route = new AdvertRoute
                {
                    DateIn = DateTime.Now,
                    Document = advert,
                    Employee = employee,
                    ProcessingResult = DocumentProcessingResult.OnAcquaintance
                };

                var incomingDoc = new IncomingAdvert
                {
                    Document = advert,
                    Employee = new Employee{ Id = x},
                    Route = route
                };

                AdvertRouteEntityService.Save(route);
                IncomingAdvertEntityService.Save(incomingDoc);
            });
        }

        public void MarkAsRead(long movementId)
        {
            var movement = AdvertMovementEntityService.Get(movementId);

            var route = movement.Route;

            if (route != null)
            {
                route.DateOut = DateTime.Now;
                route.ProcessingResult = DocumentProcessingResult.Acquainted;
                AdvertRouteEntityService.Update(route);
            }
        }
    }
}