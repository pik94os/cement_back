﻿using System;
using System.Collections.Generic;
using System.Linq;
using Castle.Core.Internal;
using Cement.Core.Auth;
using Cement.Core.Entities;
using Cement.Core.Enums;
using Cement.Core.Extensions;
using Cement.Core.Services.EntityService;
using DataAccess;
using DataAccess.SessionProvider;
using NHibernate;
using NHibernate.Linq;

namespace Cement.Core.Services.Impl
{
    public class BaseRequestService<T> : BaseService, IProductRequestService<T>  where T: BaseRequest, new()
    {
        public IEntityService<IncomingRequest> IncomingRequestEntityService { get; set; }

        public IEntityService<OutcomingRequest> OutcomingRequestEntityService { get; set; }

        public IEntityService<RequestMovement> RequestMovementEntityService { get; set; }

        public IEntityService<RequestRoute> RequestRouteEntityService { get; set; }

        //public IEntityService<BaseRequest> RequestEntityService { get; set; }
        public IEntityService<T> RequestEntityService { get; set; }

        #region Подписать исходящую заявку

        public void SignOutcoming(long documentMovementId, string comment, Employee employee)
        {
            var documentMovement = RequestMovementEntityService.GetAll()
                .Where(x => x.Id == documentMovementId)
                .Fetch(x => x.Document)
                .Fetch(x => x.Employee)
                .Fetch(x => x.Route)
                .FirstOrDefault();

            InTransaction(() => SignOutcoming(documentMovement, comment, employee));
        }

        protected void SignOutcoming(RequestMovement docMovement, string comment, Employee employee)
        {
            var route = docMovement.Route;
            var document = docMovement.Document as T;

            // Обновляем значение исходящего времени и статус обработки текущего этапа маршрута
            if (route != null)
            {
                route.ProcessingResult = DocumentProcessingResult.Signed;
                route.DateOut = DateTime.Now;
                route.Comment = comment.GetBytes();
                RequestRouteEntityService.Update(route);
            }

            // При подписании исходящей заявки, появляется запись во входящих заявках у контактного лица поставщика

            document.SignedAt = DateTime.Now;
            document.DocumentState = DocumentState.OnSecondSideAgreement;
            RequestEntityService.Update(document);

            // Добавляем запись в маршрут документа
            var docRoute = new RequestRoute
            {
                DateIn = DateTime.Now,
                Document = document,
                Employee = document.ProviderContactPerson
            };

            RequestRouteEntityService.Save(docRoute);

            // Добавляем документ получателю 
            var docMvtNew = new IncomingRequest
            {
                Document = document,
                Employee = document.ProviderContactPerson,
                Meeting = employee,
                Route = docRoute
            };

            IncomingRequestEntityService.Save(docMvtNew);
        }
        
        #endregion Подписать исходящую заявку

        #region Подписать входящую заявку

        public void SignIncoming(long documentMovementId, string comment)
        {
            var documentMovement = RequestMovementEntityService.GetAll()
                .Where(x => x.Id == documentMovementId)
                .Fetch(x => x.Document)
                .ThenFetch(x => x.InitialRequest)
                .Fetch(x => x.Employee)
                .Fetch(x => x.Route)
                .FirstOrDefault();

            InTransaction(() => SignIncoming(documentMovement, comment));
        }

        protected void SignIncoming(RequestMovement docMovement, string comment)
        {
            var route = docMovement.Route;
            var document = docMovement.Document;
            document.DocumentState = DocumentState.Signed;

            // Обновляем значение исходящего времени и статус обработки текущего этапа маршрута
            if (route != null)
            {
                route.ProcessingResult = DocumentProcessingResult.Signed;
                route.DateOut = DateTime.Now;
                route.Comment = comment.GetBytes();
                RequestRouteEntityService.Update(route);
            }

            // При подписании входящей заявки на её основе формируется новая заявка (вкладка НОВЫЕ)
            CreateChainNewRequest(document);
        }

        /// <summary>
        /// Создание посреднической заявки в цепочке
        /// </summary>
        protected void CreateChainNewRequest(BaseRequest request)
        {
            var newRequest = new T
            {
                StageNumber = request.StageNumber + 1,
                ChainGuid = request.ChainGuid,
                Consignee = request.Consignee,
                ConsigneeContactPerson = request.ConsigneeContactPerson,
                //Product = request.Product,
                RequestType = request.RequestType,
                Warehouse = request.Warehouse,
                Date = request.Date,
                UnshipmentDateTime = request.UnshipmentDateTime
            };

            if (request.StageNumber == 0)
            {
                // Исходная заявка === request

                newRequest.InitialRequest = request;
            }
            else
            {
                // Исходная заявка === request.InitialRequest

                newRequest.InitialRequest = request.InitialRequest;
            }

            CreateChainNewRequestInternal(request, newRequest);

            RequestEntityService.Save(newRequest);

#warning реализовать логику
        }

        protected virtual void CreateChainNewRequestInternal(BaseRequest request, T newRequest)
        {

        }

        #endregion Подписать входящую заявку

        #region Согласование
        
        public void AgreeAndSendNext(long documentMovementId, string comment, Employee to)
        {
            var documentMovement = RequestMovementEntityService.GetAll()
                .Where(x => x.Id == documentMovementId)
                .Fetch(x => x.Document)
                .Fetch(x => x.Employee)
                .Fetch(x => x.Route)
                .FirstOrDefault();

            InTransaction(() => AgreeAndSendNext(documentMovement, to, comment));
        }

        protected void AgreeAndSendNext(RequestMovement docMovement, Employee to, string comment)
        {
            var route = docMovement.Route;
            var document = docMovement.Document;

            // Обновляем значение исходящего времени и статус обработки текущего этапа маршрута
            if (route != null)
            {
                route.ProcessingResult = DocumentProcessingResult.Agreed;
                route.DateOut = DateTime.Now;
                route.Comment = comment.GetBytes();
                RequestRouteEntityService.Update(route);
            }

            // Добавляем запись в маршрут документа
            var docRoute = new RequestRoute
            {
                DateIn = DateTime.Now,
                Document = document,
                Employee = to
            };

            RequestRouteEntityService.Save(docRoute);

            // Добавляем документ получателю 

            var docMvtNew = docMovement.MovementType == DocumentMovementType.Incoming
                ? (RequestMovement) new IncomingRequest()
                : (RequestMovement) new OutcomingRequest();
            
            docMvtNew.Document = document;
            docMvtNew.Employee = to;
            docMvtNew.Meeting = docMovement.Employee;
            docMvtNew.Route = docRoute;

            RequestMovementEntityService.Save(docMvtNew);
        }

        #endregion Согласование

        #region Отклонение

        public void Reject(long documentMovementId, string comment)
        {
            var documentMovement = RequestMovementEntityService.GetAll()
                .Where(x => x.Id == documentMovementId)
                .Fetch(x => x.Document)
                .Fetch(x => x.Employee)
                .Fetch(x => x.Route)
                .FirstOrDefault();

            InTransaction(() => Reject(documentMovement, comment));
        }
    
        protected void Reject(RequestMovement docMovement, string comment)
        {
            var route = docMovement.Route;
            
            // Обновляем значение исходящего времени и статус обработки текущего этапа маршрута
            if (route != null)
            {
                route.ProcessingResult = DocumentProcessingResult.Rejected;
                route.DateOut = DateTime.Now;
                route.Comment = comment.GetBytes();
                RequestRouteEntityService.Update(route);
            }
        }

        #endregion Отклонение


        /// <summary>
        /// Добавить движение документа, если отсутствует
        /// </summary>
        protected void AddMovementIfNone(BaseRequest request, Employee from)
        {
            if (RequestMovementEntityService.GetAll().Count(x => x.Document.Id == request.Id) == 0)
            {
                // Создаем запись маршрута - от инициатора
                var route = new RequestRoute
                {
                    DateIn = DateTime.Now,
                    Document = request,
                    Employee = from,
                };

                RequestRouteEntityService.Save(route);

                // Добавляем заявку в исходящие себе же
                var outcomingDoc = new OutcomingRequest
                {
                    Document = request,
                    Employee = from,
                    Route = route
                };
                OutcomingRequestEntityService.Save(outcomingDoc);
            }
        }

        /// <summary>
        /// Сохранение начальной заявки цепочки
        /// </summary>
        public long SaveChainInitialRequest(T request, Employee from)
        {
            InTransaction(() =>
            {
                request.ChainGuid = Guid.NewGuid().ToString();
                RequestEntityService.Save(request);
                AddMovementIfNone(request, from);
            });

            return request.Id;
        }

        /// <summary>
        /// Сохранение посреднической заявки цепочки
        /// </summary>
        public long SaveChainNonInitialRequest(T request, Employee from)
        {
            InTransaction(() =>
            {
                RequestEntityService.Save(request);
                AddMovementIfNone(request, from);
            });

            return request.Id;
        }

        /// <summary>
        /// Завершение цепочки заявок
        /// </summary>
        public void SetChainEnd(long requestId, DateTime shipmentDateTime)
        {
            var requestToDel = RequestEntityService.GetAll().First(x => x.Id == requestId);

            if (requestToDel.StageNumber == 0)
            {
                // Ибо нефиг страдать фигней! Короче, нельзя так!
                throw new Exception();
            }

            var chainRequests = RequestEntityService.GetAll().Where(x => x.ChainGuid == requestToDel.ChainGuid).ToList();

            InTransaction(() =>
            {
                foreach (var chainRequest in chainRequests)
                {
                    chainRequest.ShipmentDateTime = shipmentDateTime;
                }
                
                // конечная заявка в цепочке
                var lastRequest = chainRequests.First(x => x.StageNumber == requestToDel.StageNumber - 1);
                lastRequest.IsLastInChain = true;

                // на основании входящей заявки и наличия на складе товара сформировать складской расходный ордер 
                
                // Удаляем заявку
                RequestEntityService.Delete(requestId);
            });
        }

        /// <summary>
        /// Возвращает идентификатр товара/услуги исходной заявки
        /// </summary>
        public long GetInitialProductId(long requestId)
        {
            var request = RequestEntityService.Get(requestId);

            if (request.InitialRequest == null)
            {
                return 0;
            }

            return request.InitialRequest.Product.Id;
        }
    }

    public class ProductRequestService : BaseRequestService<ProductRequest>
    {
        protected override void CreateChainNewRequestInternal(BaseRequest request, ProductRequest newRequest)
        {
            var productRequest = request as ProductRequest;
            if (productRequest == null)
            {
                return;
            }

            newRequest.VehicleBodyType = productRequest.VehicleBodyType;
            newRequest.VehicleExtraCharacteristic = productRequest.VehicleExtraCharacteristic;
            newRequest.VehicleIsOverLoading = productRequest.VehicleIsOverLoading;
            newRequest.VehicleIsRearLoading = productRequest.VehicleIsRearLoading;
            newRequest.VehicleIsSideLoading = productRequest.VehicleIsSideLoading;
            newRequest.VehicleOption = productRequest.VehicleOption;
            newRequest.VehicleType = productRequest.VehicleType;
        }
    }
}