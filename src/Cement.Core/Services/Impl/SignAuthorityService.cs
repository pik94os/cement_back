﻿using System;
using System.Collections.Generic;
using System.Linq;
using Cement.Core.Entities;
using Cement.Core.Services.EntityService;

namespace Cement.Core.Services.Impl
{
    public class SignAuthorityService : ISignAuthorityService
    {
        public IEntityService<Signature> SignatureEntityService { get; set; }

        private DateTime CacheUpdatedAt;

        /// <summary>
        /// Время жизни кэша в минутах
        /// </summary>
        private int CacheLifetime = 60;

        private Dictionary<long, List<long>> _cache;

        private object lockObject = new object();

        private Dictionary<long, List<long>> Cache
        {
            get
            {
                lock (lockObject)
                {
                    if (_cache == null || CacheUpdatedAt.AddMinutes(CacheLifetime) < DateTime.Now)
                    {
                        UpdateCache();
                    }

                    return _cache;
                }
            }
        }

        public bool CanSignFor(long? trusterEmployeeId, Employee employee)
        {
            if (trusterEmployeeId == null || employee == null)
            {
                return false;
            }

            if (!Cache.ContainsKey(employee.Id))
            {
                return false;
            }

            return Cache[employee.Id].Contains(trusterEmployeeId.Value);
        }


        /// <summary>
        /// Обновление кэша. доступно из вне, т.к. кэш необходимо обновлять при изменении передач подписи.
        /// </summary>
        public void UpdateCache()
        {
            var now = DateTime.Now;

            _cache = SignatureEntityService.GetAll()
                .Where(x => x.Truster != null)
                .Where(x => x.Recipient != null)
                .Where(x => x.DateStart <= now)
                .Where(x => x.DateEnd >= now)
                .Select(x => new
                {
                    trusterId = x.Truster.Id,
                    recipientId = x.Recipient.Id
                })
                .AsEnumerable()
                .GroupBy(x => x.recipientId)
                .ToDictionary(x => x.Key, x => x.Select(y => y.trusterId).Distinct().ToList());

            CacheUpdatedAt = now;
        }
    }
}