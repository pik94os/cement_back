﻿using System;
using System.Linq;
using Cement.Core.Entities;
using Cement.Core.Entities.Stockroom;
using Cement.Core.Enums;
using Cement.Core.Services.EntityService;

namespace Cement.Core.Services.Impl
{
    public class AccountingInOutDocService : RoutedDocumentProcessingService<AccountingInOutDocBase, AccountingInOutDocRoute>
    {
        public IEntityService<StockroomExpenseOrderSale> StockroomExpenseOrderSaleEntityService { get; set; }

        public IEntityService<StockroomReceiptOrderItemOuter> StockroomReceiptOrderItemOuterEntityService { get; set; }

        protected override bool IsCorrespondence { get { return true; } }
        
        protected override void OnSign(AccountingInOutDocBase document, AccountingInOutDocRoute docRoute)
        {
            if (document.DocumentState == DocumentState.OnSecondSideAgreement)
            {
                document.DocumentState = DocumentState.Signed;

                OnDocFinalSign(document);
            }
            else
            {
                // Документ отправляется во вторую организацию
                document.DocumentState = DocumentState.OnSecondSideAgreement;

                // Добавляем запись в маршрут документа (во входящие получателя)
                var newRoute = new AccountingInOutDocRoute
                {
                    DateIn = DateTime.Now,
                    Document = document,
                    Employee = GetEmployee(document),
                    Stage = docRoute.Stage + 1,
                    MovementType = DocumentMovementType.Incoming
                };

                DocumentRouteEntityService.Save(newRoute);
            }
        }

        private Employee GetEmployee(AccountingInOutDocBase document)
        {
            var docId = document.Id;

            if (document is AccountingInOutDocInvoice)
            {
                docId = (document as AccountingInOutDocInvoice).AccountingInOutDocWayBill.Id;
            }

            var employeeId = StockroomExpenseOrderSaleEntityService.GetAll()
                .Where(x => x.AccountingInOutDocWayBill.Id == docId)
                .Select(x => x.ProductRequest.PayerContactPerson.Id)
                .First();

            return new Employee { Id = employeeId };
        }

        private void OnDocFinalSign(AccountingInOutDocBase document)
        {
            if (document is AccountingInOutDocInvoice)
            {
                return;
            }

            var products = StockroomExpenseOrderSaleEntityService.GetAll()
                .Where(x => x.AccountingInOutDocWayBill.Id == document.Id)
                .Select(x => new
                {
                    warehouseId = x.ProductRequest.Warehouse.Id,
                    productId = x.ProductRequest.Product.Id,
                    measureUnitId = x.ProductRequest.ProductMeasureUnit.Id,
                    x.ProductRequest.ProductPrice,
                    x.ProductRequest.ProductTax,
                    x.Amount
                })
                .ToList();

            var groupedProducts = products.GroupBy(x => new { x.productId, x.measureUnitId, x.warehouseId })
                .Select(x =>
                {
                    var amount = x.Sum(y => y.Amount);
                    var totalSum = x.Sum(y => y.Amount * y.ProductPrice);
                    var price = amount > 0 ? totalSum / amount : 0;
                    var tax = totalSum > 0 ? x.Sum(y => y.Amount * y.ProductPrice * y.ProductTax) / totalSum : 0;

                    return new
                    {
                        x.Key.warehouseId,
                        x.Key.productId,
                        x.Key.measureUnitId,
                        amount,
                        price,
                        tax,
                    };
                })
                .ToList();

            groupedProducts.ForEach(x =>
            {
                var stockroomReceiptOrderItemOuter = new StockroomReceiptOrderItemOuter
                {
                    AccountingInOutDoc = document,
                    Warehouse = new Warehouse{ Id = x.warehouseId },
                    Product = new Product { Id = x.productId },
                    MeasureUnit = new MeasureUnit { Id = x.measureUnitId },
                    Amount = x.amount,
                    Price = x.price,
                    Tax = x.tax
                };

                StockroomReceiptOrderItemOuterEntityService.Save(stockroomReceiptOrderItemOuter);
            });
        }
    }
}