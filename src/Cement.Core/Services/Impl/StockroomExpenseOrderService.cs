﻿using System;
using System.Linq;
using Cement.Core.Entities;
using Cement.Core.Entities.Stockroom;
using Cement.Core.Enums;
using Cement.Core.Exceptions;
using Cement.Core.Services.EntityService;

namespace Cement.Core.Services.Impl
{
    public class StockroomExpenseOrderService : RoutedDocumentProcessingService<StockroomExpenseOrderBase, StockroomExpenseOrderRoute>
    {
        public IEntityService<StockroomMovementRequest> StockroomMovementRequestEntityService { get; set; }
        public IEntityService<StockroomWriteoffRequest> StockroomWriteoffRequestEntityService { get; set; }
        public IEntityService<StockroomProduct> StockroomProductEntityService { get; set; }
        public IEntityService<StockroomProductHistory> StockroomProductHistoryEntityService { get; set; }
        public IEntityService<ProductRequest> ProductRequestEntityService { get; set; }

        private static readonly object LockObject = new object();

        public override void Sign(long documentRouteId, string comment)
        {
            lock (LockObject)
            {
                ProcessDocument(documentRouteId, DocumentProcessingResult.Signed, comment);
            }
        }

        protected override void OnSign(StockroomExpenseOrderBase document, StockroomExpenseOrderRoute docRoute)
        {
            base.OnSign(document, docRoute);

            var stockroomProduct = GetStockroomProduct(document);

            if (stockroomProduct == null)
            {
                throw new ValidationException("На складе отсутствует данный товар");
            }

            if (stockroomProduct.Amount < document.Amount)
            {
                throw new ValidationException("На складе отсутствует необходимое количество товара");
            }

            // Пишем изменения в "Движение товаров"
            var stockroomProductHistory = new StockroomProductHistory
            {
                StockroomProduct = stockroomProduct,
                Date = DateTime.Now,
                IsExpense = true,
                InitialAmount = stockroomProduct.Amount,
                InitialPrice = 0, //stockroomProduct.Price,
                InitialTax = 0, //stockroomProduct.Tax,
                ChangeAmount = document.Amount,
                ChangePrice = 0, //stockroomProduct.Price,
                ChangeTax = 0 //stockroomProduct.Tax,
            };

            StockroomProductHistoryEntityService.Save(stockroomProductHistory);

            // Вычитаем объем товаров из склада
            stockroomProduct.Amount -= document.Amount;
            
            // Пересчитываем стоимостную оценку
            //stockroomProduct.Sum = stockroomProduct.Amount * stockroomProduct.Price;
        }

        /// <summary>
        /// Получение товара склада из расходного складского ордера
        /// </summary>
        private StockroomProduct GetStockroomProduct(StockroomExpenseOrderBase document)
        {
            StockroomProduct stockroomProduct = null;

            if (document is StockroomExpenseOrderMovement)
            {
                stockroomProduct = StockroomMovementRequestEntityService.GetAll()
                    .Where(x => x.Id == (document as StockroomExpenseOrderMovement).StockroomMovementRequest.Id)
                    .Select(x => x.StockroomProduct)
                    .First();
            }
            else if (document is StockroomExpenseOrderWriteoff)
            {
                stockroomProduct = StockroomWriteoffRequestEntityService.GetAll()
                    .Where(x => x.Id == (document as StockroomExpenseOrderWriteoff).StockroomWriteoffRequest.Id)
                    .Select(x => x.StockroomProduct)
                    .First();
            }
            else if (document is StockroomExpenseOrderSale)
            {
                var productDataQuery = ProductRequestEntityService.GetAll()
                    .Where(x => x.Id == (document as StockroomExpenseOrderSale).ProductRequest.Id)
                    .Select(x => new
                    {
                        productId = x.Product.Id,
                        measureUnitId = x.ProductMeasureUnit.Id
                    });

                stockroomProduct = StockroomProductEntityService.GetAll()
                    .Where(x => productDataQuery.Any(y => y.productId == x.Product.Id && y.measureUnitId == x.MeasureUnit.Id))
                    .FirstOrDefault();
            }

            return stockroomProduct;
        }
    }
}