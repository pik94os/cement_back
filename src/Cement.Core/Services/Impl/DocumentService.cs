﻿using System;
using System.Collections.Generic;
using System.Linq;
using Cement.Core.Entities;
using Cement.Core.Entities.Stockroom;
using Cement.Core.Enums;
using Cement.Core.Extensions;
using Cement.Core.Services.EntityService;

namespace Cement.Core.Services.Impl
{
    public class DocumentService : IDocumentService
    {
        public IEntityService<DocumentRelation> DocumentRelationEntityService { get; set; }

        public IEntityService<InternalDocument> InternalDocumentEntityService { get; set; }

        public IEntityService<Correspondence> CorrespondenceEntityService { get; set; }

        public IEntityService<Entities.Contract> ContractEntityService { get; set; }

        /// <summary>
        /// Связывание двух документов
        /// Так как таблица связей документов обобщенная и не имеет внешних ключей
        /// необходимо создавать 2 записи для одной связи (для упрощения определения входищим является документ или нет)
        /// </summary>
        public void BindDocuments(IDocument document1, IDocument document2)
        {
            CreateDocumentBind(document1, document2);
            CreateDocumentBind(document2, document1);
        }

        protected void CreateDocumentBind(IDocument document1, IDocument document2)
        {
            var documentRelation = new DocumentRelation
            {
                BaseDocumentId = document1.Id,
                BaseDocumentType = document1.Type,
                JoinedDocId = document2.Id,
                JoinedDocumentType = document2.Type,
                Name = document2.Name,
                Date = document2.Date,
                No = document2.No,
                State = document2.State
            };

            DocumentRelationEntityService.Save(documentRelation);
        }

        public void AddDocumentBinds(IDocument document1, List<string> documentKeys)
        {
            if (documentKeys == null || documentKeys.Count == 0)
            {
                return;
            }

            foreach (var documentKey in documentKeys)
            {
                var keyParts = documentKey.Split(Constants.DocIdDelimeter);

                if (keyParts.Length != 2)
                {
                    continue;
                }

                var docType = (DocumentType)keyParts.First().ToLong();
                var docId = keyParts.Last().ToLong();

                var docToBind = GetDocument(docType, docId);

                if (docToBind != null)
                {
                    BindDocuments(document1, docToBind);
                }
            }
        }

        public void AddRelatedDocumentsToEntity(long entityId, List<string> documentKeys)
        {
            if (documentKeys == null || documentKeys.Count == 0)
            {
                return;
            }

            foreach (var documentKey in documentKeys)
            {
                var keyParts = documentKey.Split(Constants.DocIdDelimeter);

                if (keyParts.Length != 2)
                {
                    continue;
                }

                var docType = (DocumentType)keyParts.First().ToLong();
                var docId = keyParts.Last().ToLong();

                var docToBind = GetDocument(docType, docId);

                if (docToBind != null)
                {
                    var documentRelation = new DocumentRelation
                    {
                        BaseDocumentId = entityId,
                        JoinedDocId = docToBind.Id,
                        JoinedDocumentType = docToBind.Type,
                        Name = docToBind.Name,
                        Date = docToBind.Date,
                        No = docToBind.No,
                        State = docToBind.State
                    };

                    DocumentRelationEntityService.Save(documentRelation);
                }
            }
        }

        protected IDocument GetDocument(DocumentType type, long docId)
        {
            switch (type)
            {
                case DocumentType.InternalDocument: 
                    return InternalDocumentEntityService.GetAll().FirstOrDefault(x => x.Id == docId);

                case DocumentType.Correspondence:
                    return CorrespondenceEntityService.GetAll().FirstOrDefault(x => x.Id == docId);

                case DocumentType.Contract:
                    return ContractEntityService.GetAll().FirstOrDefault(x => x.Id == docId);
            }

            return null;
        }

        public Dictionary<long, RelatedDocumentsProxy> GetRelatedDocumentsDict(List<long> documentIds)
        {
            var result = DocumentRelationEntityService.GetAll()
                .Where(x => documentIds.Contains(x.BaseDocumentId))
                .Select(x => new
                {
                    x.BaseDocumentId,
                    x.JoinedDocId,
                    x.JoinedDocumentType,
                    x.Name
                })
                .AsEnumerable()
                .GroupBy(x => x.BaseDocumentId)
                .ToDictionary(x => x.Key,
                    x =>
                    {
                        var relDocProxy = new RelatedDocumentsProxy
                        {
                            Ids = x.Select(y => GetDocIdString(y.JoinedDocumentType, y.JoinedDocId)).ToList(),
                            Display = string.Join(", ", x.Select(y => y.Name))
                        };

                        return relDocProxy;
                    });

            return result;
        }

        public string GetDocIdString(DocumentType type, long id)
        {
            return string.Format("{0}{1}{2}", (int)type, Constants.DocIdDelimeter, id);
        }

        public string GetBranchIdString(DocumentType type, int key)
        {
            return string.Format("{0}{1}{2}", (int)type, Constants.DocBranchIdDelimeter, key);
        }

        public string GetDocTypeString(DocumentType type, int docKind)
        {
            var typeDict = typeof(DocumentType).AsDictionary();

            switch (type)
            {
                case DocumentType.Message:
                    var msgKindDict = typeof(MessageType).AsDictionary();
                    return string.Format("{0} - {1}", typeDict.Get((int)type), msgKindDict.Get(docKind));

                case DocumentType.InternalDocument:
                    var intDocKindDict = typeof(InternalDocumentType).AsDictionary();
                    return string.Format("{0} - {1}", typeDict.Get((int)type), intDocKindDict.Get(docKind));

                case DocumentType.Correspondence:
                    var correspondenceKindDict = typeof(CorrespondenceType).AsDictionary();
                    return string.Format("{0} - {1}", typeDict.Get((int)type), correspondenceKindDict.Get(docKind));

                case DocumentType.StockroomRequest:
                    var stockroomRequestKindDict = typeof(StockroomRequestType).AsDictionary();
                    return string.Format("Склад - {0}", stockroomRequestKindDict.Get(docKind));

                case DocumentType.StockroomExpenseOrder:
                    var stockroomExpenseOrderKindDict = typeof(StockroomExpenseOrderType).AsDictionary();
                    return string.Format("{0} ({1})", typeDict.Get((int)type), stockroomExpenseOrderKindDict.Get(docKind));

                case DocumentType.OtherAccountingDocument:
                    var accountingOtherDocumentTypeDict = typeof(AccountingOtherDocumentType).AsDictionary();
                    return string.Format("{0} - {1}", typeDict.Get((int)type), accountingOtherDocumentTypeDict.Get(docKind));

                case DocumentType.AccountingInOutDocument:
                    var accountingInOutDocumentTypeDict = typeof(AccountingInOutDocType).AsDictionary();
                    return string.Format("{0} - {1}", typeDict.Get((int)type), accountingInOutDocumentTypeDict.Get(docKind));

                default:
                    return typeDict.Get((int)type);
            }
            
            return string.Empty;
        }

        
    }
}