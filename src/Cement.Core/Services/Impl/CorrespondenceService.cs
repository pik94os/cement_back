﻿using System;
using System.Linq;
using Cement.Core.Entities;
using Cement.Core.Enums;
using Cement.Core.Extensions;
using Cement.Core.Services.EntityService;
using NHibernate.Linq;

namespace Cement.Core.Services.Impl
{
    public class CorrespondenceService : BaseService, ICorrespondenceService
    {
        public IEntityService<IncomingCorrespondence> IncomingCorrespondenceEntityService { get; set; }

        public IEntityService<OutcomingCorrespondence> OutcomingCorrespondenceEntityService { get; set; }

        public IEntityService<CorrespondenceMovement> CorrespondenceMovementEntityService { get; set; }

        public IEntityService<CorrespondenceRoute> CorrespondenceRouteEntityService { get; set; }

        public IEntityService<Correspondence> CorrespondenceEntityService { get; set; }

        public void SendTo(long documentId, Employee from, Employee to, string comment)
        {
            InTransaction(() => SendToInternal(documentId, from, to, comment));
        }

        protected void SendToInternal(long documentId, Employee from, Employee to, string comment)
        {
            var correspondence = CorrespondenceEntityService.Load(documentId);

            // Если документ еще в "новых", то убираем его оттуда
            if (correspondence.DocumentState == DocumentState.Draft)
            {
                correspondence.DocumentState = DocumentState.OnAgreement;

                CorrespondenceEntityService.Update(correspondence);
            }

            // Добавляем документ в исходящие отправителя
            var outcomingDoc = new OutcomingCorrespondence
            {
                Document = correspondence,
                Employee = from,
                Meeting = to
            };

            OutcomingCorrespondenceEntityService.Save(outcomingDoc);

            // Создаем запись маршрута - от инициатора
            var route = new CorrespondenceRoute
            {
                DateIn = DateTime.Now,
                DateOut = DateTime.Now,
                Document = correspondence,
                Employee = from,
                Comment = comment.GetBytes(),
                ProcessingResult = DocumentProcessingResult.Agreed
            };

            CorrespondenceRouteEntityService.Save(route);

            MoveDocument(correspondence, from, to);
        }

        protected void MoveDocument(Correspondence correspondence, Employee from, Employee to)
        {
            // Добавляем запись в маршрут документа
            var docRoute = new CorrespondenceRoute
            {
                DateIn = DateTime.Now,
                Document = correspondence,
                Employee = to
            };

            // Пока эту логику убрали
            //// Добавляем документ в исходящие отправителя
            //var outcomingDoc = new OutcomingCorrespondence
            //{
            //    Document = correspondence,
            //    Employee = from
            //};

            // Добавляем документ во входящие получателя
            var incomingDoc = new IncomingCorrespondence
            {
                Document = correspondence,
                Employee = to,
                Meeting = from,
                Route = docRoute
            };

            CorrespondenceRouteEntityService.Save(docRoute);
            IncomingCorrespondenceEntityService.Save(incomingDoc);
        }

        public void ProcessDocument(long documentMovementId, DocumentProcessingResult result, string comment, Employee to = null)
        {
            var documentMovement = CorrespondenceMovementEntityService.GetAll()
                .Where(x => x.Id == documentMovementId)
                .Fetch(x => x.Document)
                .Fetch(x => x.Employee)
                .Fetch(x => x.Route)
                .FirstOrDefault();

            InTransaction(() =>
            {
                switch (result)
                {
                    case DocumentProcessingResult.Signed:
                        Sign(documentMovement, comment);
                        break;

                    case DocumentProcessingResult.Rejected:
                        Reject(documentMovement, comment);
                        break;

                    case DocumentProcessingResult.Agreed:
                        AgreeAndSendNext(documentMovement, to, comment);
                        break;
                }
            });
        }

        /// <summary>
        /// Отправить документ на дальнейшее согласование (либо подпись)
        /// </summary>
        protected void AgreeAndSendNext(CorrespondenceMovement docMovement, Employee to, string comment)
        {
            var route = docMovement.Route;
            var document = docMovement.Document;

            // Обновляем значение исходящего времени и статус обработки текущего этапа маршрута
            if (route != null)
            {
                route.ProcessingResult = DocumentProcessingResult.Agreed;
                route.DateOut = DateTime.Now;
                route.Comment = comment.GetBytes();
            }

            // Добавляем запись в маршрут документа
            var docRoute = new CorrespondenceRoute
            {
                DateIn = DateTime.Now,
                Document = document,
                Employee = to
            };

            CorrespondenceRouteEntityService.Save(docRoute);

            // Добавляем документ получателю 

            var docMvtNew = docMovement.MovementType == DocumentMovementType.Incoming
                ? (CorrespondenceMovement)new IncomingCorrespondence()
                : (CorrespondenceMovement)new OutcomingCorrespondence();

            docMvtNew.Document = document;
            docMvtNew.Employee = to;
            docMvtNew.Meeting = docMovement.Employee;
            docMvtNew.Route = docRoute;
            
            CorrespondenceMovementEntityService.Save(docMvtNew);
        }

        /// <summary>
        /// Подписать документ
        /// </summary>
        protected void Sign(CorrespondenceMovement docMovement, string comment)
        {
            var route = docMovement.Route;
            var document = docMovement.Document;

            // Обновляем значение исходящего времени и статус обработки текущего этапа маршрута
            if (route != null)
            {
                route.ProcessingResult = DocumentProcessingResult.Signed;
                route.DateOut = DateTime.Now;
                route.Comment = comment.GetBytes();
            }

            //Обновляем статус документа на "Подписано"
            document.DocumentState = DocumentState.Signed;

            // При подписании исходящей корреспонденции
            // Создаем входящую корреспонденцию для адресата
            if (docMovement.MovementType == DocumentMovementType.Outcoming)
            {
                document.SignedAt = DateTime.Now;

                MoveDocument(document, document.Creator, document.Addressee);
            }
        }

        /// <summary>
        /// Отклонить документ
        /// </summary>
        protected void Reject(CorrespondenceMovement docMovement, string comment)
        {
            var route = docMovement.Route;
            var document = docMovement.Document;

            // Обновляем значение исходящего времени и статус обработки текущего этапа маршрута
            if (route != null)
            {
                route.ProcessingResult = DocumentProcessingResult.Rejected;
                route.DateOut = DateTime.Now;
                route.Comment = comment.GetBytes();
            }

            // Обновляем статус документа на "Отклонено"
            if (document != null)
            {
                document.DocumentState = DocumentState.Rejected;
            }
        }
    }
}