﻿using System;
using System.Collections.Generic;
using System.Linq;
using Cement.Core.Auth;
using Cement.Core.Entities;
using Cement.Core.Enums;
using Cement.Core.Extensions;
using Cement.Core.Services.EntityService;
using DataAccess.Repository;

namespace Cement.Core.Services.Impl
{
    public class WarehouseService : BaseService, IWarehouseService
    {
        public Tuple<IList<WarehouseDataProxy>, int> GetBottomList(IQueryable<Warehouse> warehousesQuery, StoreParams.StoreParams storeParams, bool separateCountQuery = true)
        {
            var query = warehousesQuery.Select(x => new WarehouseDataProxy
            {
                id = x.Id,
                p_group = "Склады",
                p_name = x.Name,
                p_address = x.Address,
                p_inn = x.Organization.Client.Inn,
                p_kpp = x.Organization.Client.Kpp,
                p_ogrn = x.Organization.Client.Ogrn,
                p_kind = ""
            })
            .Filter(storeParams);

            var data = query
               .Order(storeParams)
               .ToList();

            var count = separateCountQuery ? query.Count() : data.Count;

            return new Tuple<IList<WarehouseDataProxy>, int>(data, count); 
        }
    }
}