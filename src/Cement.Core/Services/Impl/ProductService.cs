﻿using System;
using System.Collections.Generic;
using System.Linq;
using Cement.Core.Entities;
using Cement.Core.Extensions;

namespace Cement.Core.Services.Impl
{
    public class ProductService : BaseService, IProductService
    {
        public Tuple<IList<ProductDataProxy>, int> GetBottomList(IQueryable<Product> productQuery, StoreParams.StoreParams storeParams, bool separateCountQuery = true)
        {
            var query = productQuery.Select(x => new ProductDataProxy
            {
                id = x.Id,
                p_name = x.Name,
                p_group = x.Group.Name,
                p_subgroup = x.SubGroup.Name,
                p_trade_mark = x.Brand,
                p_manufacturer = x.Manufacturer.Name,
                p_unit_display = x.MeasureUnit.ShortName
            })
            .Filter(storeParams);

            var data = query
               .Order(storeParams)
               .ToList();

            var count = separateCountQuery ? query.Count() : data.Count;

            return new Tuple<IList<ProductDataProxy>, int>(data, count); 
        }
    }
}