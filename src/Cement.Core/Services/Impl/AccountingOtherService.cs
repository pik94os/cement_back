﻿using System;
using System.Linq;
using Cement.Core.Entities;
using Cement.Core.Entities.Stockroom;
using Cement.Core.Enums;
using Cement.Core.Services.EntityService;

namespace Cement.Core.Services.Impl
{
    public class AccountingOtherService : RoutedDocumentProcessingService<AccountingOtherBase, AccountingOtherRoute>
    {
        public IEntityService<StockroomExpenseOrderMovement> StockroomExpenseOrderMovementEntityService { get; set; }

        public IEntityService<StockroomBaseRequest> StockroomBaseRequestEntityService { get; set; }

        public IEntityService<StockroomReceiptOrderItemInnerMove> StockroomReceiptOrderItemInnerMoveEntityService { get; set; }

        public IEntityService<StockroomReceiptOrderItemInnerPosting> StockroomReceiptOrderItemInnerPostingEntityService { get; set; }

        protected override void OnSign(AccountingOtherBase document, AccountingOtherRoute docRoute)
        {
            if (document is AccountingOtherMovementBill)
            {
                if (document.DocumentState == DocumentState.OnSecondSideAgreement)
                {
                    document.DocumentState = DocumentState.Signed;

                    OnDocFinalSignMovementBill(document);
                }
                else
                {
                    // Заявка отправляется на второй склад на подписание
                    document.DocumentState = DocumentState.OnSecondSideAgreement;

                    // Документ отправляем автору внут.заявки на перемещение
#warning Определить по бизнес-логике кому отправить бух.док
                    var employee = new Employee();
                    throw new Exception("Определить по бизнес-логике кому отправить бух.док");

                    // Добавляем запись в маршрут документа (во входящие получателя)
                    var newRoute = new AccountingOtherRoute
                    {
                        DateIn = DateTime.Now,
                        Document = document,
                        Employee = employee,
                        Stage = docRoute.Stage + 1,
                        MovementType = DocumentMovementType.Incoming
                    };

                    DocumentRouteEntityService.Save(newRoute);
                }
            }
            else
            {
                document.DocumentState = DocumentState.Signed;

                OnDocFinalSign(document);
            }
        }

        private void OnDocFinalSignMovementBill(AccountingOtherBase document)
        {
            var products = StockroomExpenseOrderMovementEntityService.GetAll()
                .Where(x => x.AccountingOtherMovementBill.Id == document.Id)
                .Select(x => new
                {
                    warehouseId = x.StockroomMovementRequest.WarehouseTo.Id,
                    warehouseFromId = x.StockroomMovementRequest.Warehouse.Id,
                    productId = x.StockroomMovementRequest.StockroomProduct.Product.Id,
                    measureUnitId = x.StockroomMovementRequest.StockroomProduct.MeasureUnit.Id,
                    ProductPrice = 0m,
                    ProductTax = 0m,
                    x.Amount
                })
                .ToList();

            var groupedProducts = products.GroupBy(x => new { x.productId, x.measureUnitId, x.warehouseId, x.warehouseFromId })
                .Select(x =>
                {
                    var amount = x.Sum(y => y.Amount);
                    var totalSum = x.Sum(y => y.Amount * y.ProductPrice);
                    var price = amount > 0 ? totalSum / amount : 0;
                    var tax = totalSum > 0 ? x.Sum(y => y.Amount * y.ProductPrice * y.ProductTax) / totalSum : 0;

                    return new
                    {
                        x.Key.warehouseFromId,
                        x.Key.warehouseId,
                        x.Key.productId,
                        x.Key.measureUnitId,
                        amount,
                        price,
                        tax,
                    };
                })
                .ToList();

            groupedProducts.ForEach(x =>
            {
                var stockroomReceiptOrderItemInner = new StockroomReceiptOrderItemInnerMove
                {
                    AccountingOtherDoc = document,
                    Warehouse = new Warehouse { Id = x.warehouseId },
                    WarehouseFrom = new Warehouse { Id = x.warehouseFromId },
                    Product = new Product { Id = x.productId },
                    MeasureUnit = new MeasureUnit { Id = x.measureUnitId },
                    Amount = x.amount,
                    Price = x.price,
                    Tax = x.tax
                };

                StockroomReceiptOrderItemInnerMoveEntityService.Save(stockroomReceiptOrderItemInner);
            });
        }

        private void OnDocFinalSign(AccountingOtherBase document)
        {
            if (!(document is AccountingOtherPostingAct))
            {
                return;
            }

            var products = StockroomBaseRequestEntityService.GetAll()
                .Where(x => x.StockroomRequestType == StockroomRequestType.MakingPosting || x.StockroomRequestType == StockroomRequestType.Posting)
                .Where(x => (x as StockroomPostingRequest).AccountingOtherPostingAct.Id == document.Id)
                .Select(x => new
                {
                    warehouseId = x.Warehouse.Id,
                    productId = x.StockroomProduct.Product.Id,
                    measureUnitId = x.StockroomProduct.MeasureUnit.Id,
                    ProductPrice = (decimal?)(x as StockroomMakingPostingRequest).PriceListProduct.Price,
                    ProductTax = (decimal?)(x as StockroomMakingPostingRequest).PriceListProduct.Tax.Rate,
                    x.Amount
                })
                .ToList();

            var groupedProducts = products.GroupBy(x => new { x.productId, x.measureUnitId, x.warehouseId })
                .Select(x =>
                {
                    var amount = x.Sum(y => y.Amount);
                    var totalSum = x.Sum(y => y.Amount * y.ProductPrice);
                    var price = amount > 0 ? totalSum / amount : 0;
                    var tax = totalSum > 0 ? x.Sum(y => y.Amount * y.ProductPrice * y.ProductTax) / totalSum : 0;

                    return new
                    {
                        x.Key.warehouseId,
                        x.Key.productId,
                        x.Key.measureUnitId,
                        amount,
                        price,
                        tax,
                    };
                })
                .ToList();

            groupedProducts.ForEach(x =>
            {
                var stockroomReceiptOrderItemInner = new StockroomReceiptOrderItemInnerPosting
                {
                    AccountingOtherDoc = document,
                    Warehouse = new Warehouse { Id = x.warehouseId },
                    Product = new Product { Id = x.productId },
                    MeasureUnit = new MeasureUnit { Id = x.measureUnitId },
                    Amount = x.amount,
                    Price = x.price,
                    Tax = x.tax
                };

                StockroomReceiptOrderItemInnerPostingEntityService.Save(stockroomReceiptOrderItemInner);
            });
        }
    }
}