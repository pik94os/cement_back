﻿using System;
using System.Linq;
using Cement.Core.Auth;
using Cement.Core.Entities;
using Cement.Core.Enums;
using Cement.Core.Exceptions;
using Cement.Core.Extensions;
using Cement.Core.Services.EntityService;
using DataAccess.Repository;

namespace Cement.Core.Services.Impl
{
    public class MessageService : BaseService, IMessageService
    {
        public IRepository<Message> MessageRepository { get; set; }

        public IEntityService<MessageRecipient> MessageRecipientEntityService { get; set; }

        public IEntityService<IncomingMessage> IncomingMessageEntityService { get; set; }

        public IEntityService<OutcomingMessage> OutcomingMessageEntityService { get; set; }

        public IEntityService<MessageMovement> MessageMovementEntityService { get; set; }

        public IEntityService<MessageRoute> MessageRouteEntityService { get; set; }

        ///// <summary>
        ///// Согласование из вкладки "Новые"
        ///// </summary>
        //public void Process(long messageId, bool send, long? employeeId, string comment)
        //{
        //    var message = MessageRepository.Load(messageId);

        //    var currentEmployee = Container.Resolve<IUserPrincipal>().Employee;

        //    InTransaction(() =>
        //    {
        //        // Создаем запись маршрута - от инициатора
        //        var route = new MessageRoute
        //        {
        //            DateIn = DateTime.Now,
        //            DateOut = DateTime.Now,
        //            Document = message,
        //            Employee = currentEmployee,
        //            Comment = comment.GetBytes(),
        //            ProcessingResult = DocumentProcessingResult.Agreed
        //        };

        //        MessageRouteEntityService.Save(route);
                
        //        message.DocumentState = DocumentState.OnAgreement;

        //        MessageRepository.Update(message);

        //        if (send)
        //        {
        //            if (employeeId == null)
        //            {
        //                throw new ValidationException("Не указан согласующий");
        //            }

        //            SendToAgree(message, currentEmployee, new Employee { Id = employeeId.Value });
        //        }
        //        else
        //        {
        //            // Добавляем документ в исходящие отправителя
        //            var outcomingDoc = new OutcomingMessage
        //            {
        //                Document = message,
        //                Employee = currentEmployee
        //            };

        //            OutcomingMessageEntityService.Save(outcomingDoc);

        //            SendMessageToRecipients(message);
        //        }
        //    });
        //}

        ///// <summary>
        ///// Согласование из вкладки "Входящие"
        ///// </summary>
        //public void ProcessFromMovement(long messageMovementId, bool send, long? employeeId, string comment)
        //{
        //    var messageMovement = MessageMovementEntityService.Get(messageMovementId);
        //    var currentEmployee = Container.Resolve<IUserPrincipal>().Employee;
        //    var message = messageMovement.Document;
        //    var route = messageMovement.Route;

        //    if (route != null)
        //    {
        //        route.ProcessingResult = DocumentProcessingResult.Agreed;
        //        route.DateOut = DateTime.Now;
        //        route.Comment = comment.GetBytes();
        //        MessageRouteEntityService.Update(route);
        //    }

        //    if (send)
        //    {
        //        if (employeeId == null)
        //        {
        //            throw new ValidationException("Не указан согласующий");
        //        }

        //        SendToAgree(message, currentEmployee, new Employee { Id = employeeId.Value });
        //    }
        //    else
        //    {
        //        SendMessageToRecipients(message);
        //    }
        //}

        //protected void SendToAgree(Message message, Employee from, Employee to)
        //{
        //    // Добавляем запись в маршрут документа
        //    var docRoute = new MessageRoute
        //    {
        //        DateIn = DateTime.Now,
        //        Document = message,
        //        Employee = to
        //    };

        //    // Добавляем документ в исходящие отправителя
        //    var outcomingDoc = new OutcomingMessage
        //    {
        //        Document = message,
        //        Employee = from
        //    };

        //    // Добавляем документ во входящие получателя
        //    var incomingDoc = new IncomingMessage
        //    {
        //        Document = message,
        //        Employee = to,
        //        Route = docRoute
        //    };

        //    MessageRouteEntityService.Save(docRoute);
        //    OutcomingMessageEntityService.Save(outcomingDoc);
        //    IncomingMessageEntityService.Save(incomingDoc);
        //}

        public void Sign(long messageId, string comment)
        {
            var message = MessageRepository.Load(messageId);

            var currentEmployee = Container.Resolve<IUserPrincipal>().Employee;

            InTransaction(() =>
            {
                // Создаем запись маршрута - от инициатора
                var route = new MessageRoute
                {
                    DateIn = DateTime.Now,
                    DateOut = DateTime.Now,
                    Document = message,
                    Employee = currentEmployee,
                    ProcessingResult = DocumentProcessingResult.Signed,
                    Comment = comment.GetBytes()
                };

                MessageRouteEntityService.Save(route);

                message.DocumentState = DocumentState.OnAgreement;

                MessageRepository.Update(message);

                // Добавляем документ в исходящие отправителя
                var outcomingDoc = new OutcomingMessage
                {
                    Document = message,
                    Employee = currentEmployee
                };

                OutcomingMessageEntityService.Save(outcomingDoc);

                SendMessageToRecipients(message);
            });
        }

        protected void SendMessageToRecipients(Message message)
        {
            // получатели
            var recipientIds = MessageRecipientEntityService.GetAll()
                .Where(x => x.Message == message)
                .Select(x => x.Recipient.Id)
                .ToList();

            // Добавляем сообщение во входящие получателя
            recipientIds.ForEach(x =>
            {
                var employee = new Employee {Id = x};

                var route = new MessageRoute
                {
                    DateIn = DateTime.Now,
                    Document = message,
                    Employee = employee,
                    ProcessingResult = DocumentProcessingResult.OnAcquaintance
                };
                
                var incomingDoc = new IncomingMessage
                {
                    Document = message,
                    Employee = employee,
                    Route = route
                };

                MessageRouteEntityService.Save(route);
                IncomingMessageEntityService.Save(incomingDoc);
            });
        }

        public void MarkAsRead(long messageMovementId)
        {
            var messageMovement = MessageMovementEntityService.Get(messageMovementId);
            
            var route = messageMovement.Route;

            if (route != null)
            {
                route.DateOut = DateTime.Now;
                route.ProcessingResult = DocumentProcessingResult.Acquainted;
                MessageRouteEntityService.Update(route);
            }
        }
    }
}