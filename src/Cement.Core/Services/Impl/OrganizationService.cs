﻿using System;
using System.Collections.Generic;
using System.Linq;
using Cement.Core.Entities;
using Cement.Core.Extensions;

namespace Cement.Core.Services.Impl
{
    public class OrganizationService : BaseService, IOrganizationService
    {
        public Tuple<IList<OrganizationDataProxy>, int> GetBottomList(IQueryable<Organization> organizationQuery, StoreParams.StoreParams storeParams, bool separateCountQuery = true)
        {
            var query = organizationQuery.Select(x => new OrganizationDataProxy
            {
                id = x.Id,
                p_name = x.Name,
                p_group = "",
                p_address = x.Client.Address,
                p_inn = x.Client.Inn,
                p_kpp = x.Client.Kpp,
                p_ogrn = x.Client.Ogrn,
                p_kind = ""
            })
            .Filter(storeParams);

            var data = query
               .Order(storeParams)
               .ToList();

            var count = separateCountQuery ? query.Count() : data.Count;

            return new Tuple<IList<OrganizationDataProxy>, int>(data, count); 
        }
    }
}