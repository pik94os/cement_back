﻿using System;
using System.Linq;
using System.Linq.Expressions;
using Cement.Core.Entities;
using Cement.Core.Extensions;
using Cement.Core.Services.EntityService;

namespace Cement.Core.Services.Impl
{
    public class TransportUnitService : ITransportUnitService
    {
        public IVehicleService VehicleService { get; set; }

        public IEntityService<TransportUnitArend> TransportUnitArendEntityService { get; set; }
        public IEntityService<TransportUnit> TransportUnitEntityService { get; set; }

        public IQueryable<TransportUnit> GetTransportUnitQuery(Organization organization, DateTime dateTime)
        {
            if (organization == null)
            {
                return TransportUnitEntityService.GetAll().Where(x => false);
            }

            var date = dateTime.Date;

            // Запрос активных аренд
            var activeArendQuery = TransportUnitArendEntityService.GetAll()
                .Where(x => x.DateStart == null || x.DateStart <= date)
                .Where(x => x.DateEnd == null || x.DateEnd >= date)
                .Where(x => x.IsActive);

            // Транспортные единицы, арендованные организацией
            var activeArendTranspotUnitsQuery = activeArendQuery
                .Where(x => x.Arendator == organization.Client)
                .Select(x => x.TransportUnit.Id);
            
            // Запрос доступных ТС организации
            var activeVehicleIdQuery = VehicleService.GetVehicleQuery(organization, date).Select(x => x.Id);
            
            // Если ТрЕд Собственный, то 
            // 1. ТрЕд не должна быть кому-либо передана в аренду
            // 2. ТС ТрЕд должна быть доступна текущей организации
            // 3. Прицепное ТС ТрЕд должна быть доступна текущей организации
            Expression<Func<TransportUnit, bool>> own =
                trUnit => trUnit.Organization.Id == organization.Id
                          && !activeArendQuery.Any(y => y.TransportUnit == trUnit) // 1
                          && activeVehicleIdQuery.Any(x => x == trUnit.Vehicle.Id && (trUnit.Trailer == null || trUnit.Trailer.Id == x)); // 2 + 3

            // Если ТрЕд В Управлении, то 
            // 1. ТрЕд должна быть "получена в аренду" от организации А (условно)
            // 2. ТС ТрЕд должна быть доступна организации А  ??
            // 3. Прицепное ТС ТрЕд должна быть доступна организации А ??
            Expression<Func<TransportUnit, bool>> arended =
                trUnit => activeArendTranspotUnitsQuery.Contains(trUnit.Id); //1 + допусать доп логику


            var availableVehicleQuery = TransportUnitEntityService.GetAll()
                .WhereAny(arended, own);

            return availableVehicleQuery;
        }

        public IQueryable<TransportUnit> GetTransportUnitQuery(Organization organization)
        {
            return GetTransportUnitQuery(organization, DateTime.Now);
        }

        public IQueryable<TransportUnit> GetOwnTransportUnitQuery(Organization organization, DateTime dateTime)
        {
            if (organization == null)
            {
                return TransportUnitEntityService.GetAll().Where(x => false);
            }

            var date = dateTime.Date;

            // Запрос активных аренд
            var activeArendQuery = TransportUnitArendEntityService.GetAll()
                .Where(x => x.DateStart == null || x.DateStart <= date)
                .Where(x => x.DateEnd == null || x.DateEnd >= date)
                .Where(x => x.IsActive);
            
            // Запрос доступных ТС организации
            var activeVehicleIdQuery = VehicleService.GetVehicleQuery(organization, date).Select(x => x.Id);

            // Если ТрЕд Собственный, то 
            // 1. ТрЕд не должна быть кому-либо передана в аренду
            // 2. ТС ТрЕд должна быть доступна текущей организации
            // 3. Прицепное ТС ТрЕд должна быть доступна текущей организации
            Expression<Func<TransportUnit, bool>> own =
                trUnit => trUnit.Organization.Id == organization.Id
                          && !activeArendQuery.Any(y => y.TransportUnit == trUnit) // 1
                          && activeVehicleIdQuery.Any(x => x == trUnit.Vehicle.Id) // 2
                          && (trUnit.Trailer == null || activeVehicleIdQuery.Any(x => x == trUnit.Trailer.Id)); // 3
            
            var availableVehicleQuery = TransportUnitEntityService.GetAll()
                .Where(own);

            return availableVehicleQuery;
        }

        public IQueryable<TransportUnit> GetOwnTransportUnitQuery(Organization organization)
        {
            return GetOwnTransportUnitQuery(organization, DateTime.Now);
        }

        public IQueryable<TransportUnitArend> GetArendedTransportUnitQuery(Organization organization, DateTime dateTime)
        {
            if (organization == null)
            {
                return TransportUnitArendEntityService.GetAll().Where(x => false);
            }

            var date = dateTime.Date;

            // Активные аренды организации
            var query = TransportUnitArendEntityService.GetAll()
                .Where(x => x.DateStart == null || x.DateStart <= date)
                .Where(x => x.DateEnd == null || x.DateEnd >= date)
                .Where(x => x.IsActive)
                .Where(x => x.Arendator == organization.Client);

            return query;
        }

        public IQueryable<TransportUnitArend> GetArendedTransportUnitQuery(Organization organization)
        {
            return GetArendedTransportUnitQuery(organization, DateTime.Now);
        }

        public IQueryable<TransportUnitArend> GetGivenToArendTransportUnitQuery(Organization organization, DateTime dateTime)
        {
            if (organization == null)
            {
                return TransportUnitArendEntityService.GetAll().Where(x => false);
            }

            var date = dateTime.Date;

            // Запрос ТС переданнных в аренду
            var query = TransportUnitArendEntityService.GetAll()
                .Where(x => x.DateStart == null || x.DateStart <= date)
                .Where(x => x.DateEnd == null || x.DateEnd >= date)
                .Where(x => x.IsActive)
                .Where(x => x.TransportUnit.Organization.Id == organization.Id);

            return query;
        }

        public IQueryable<TransportUnitArend> GetGivenToArendTransportUnitQuery(Organization organization)
        {
            return GetGivenToArendTransportUnitQuery(organization, DateTime.Now);
        }
    }
}