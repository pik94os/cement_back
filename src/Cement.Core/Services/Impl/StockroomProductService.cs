﻿using System;
using System.Collections.Generic;
using System.Linq;
using Cement.Core.Entities.Stockroom;
using Cement.Core.Extensions;

namespace Cement.Core.Services.Impl
{
    public class StockroomProductService : BaseService, IStockroomProductService
    {
        public Tuple<IList<StockroomProductDataProxy>, int> GetBottomList(IQueryable<StockroomProduct> stockroomProductQuery, StoreParams.StoreParams storeParams, bool separateCountQuery = true)
        {
            var query = stockroomProductQuery.Select(x => new StockroomProductDataProxy
            {
                id = x.Product.Id,
                p_name = x.Product.Name,
                p_group = x.Product.Group.Name,
                p_subgroup = x.Product.SubGroup.Name,
                p_trade_mark = x.Product.Brand,
                p_manufacturer = x.Product.Manufacturer.Name,
                p_unit_display = x.MeasureUnit.ShortName,
                p_price = null, // брать со склада
                p_tax = null // брать со склада
            })
            .Filter(storeParams);

            var data = query
               .Order(storeParams)
               .ToList();

            var count = separateCountQuery ? query.Count() : data.Count;

            return new Tuple<IList<StockroomProductDataProxy>, int>(data, count); 
        }
    }
}