﻿using System;
using System.Linq;
using System.Reflection;
using Castle.Core.Internal;
using Castle.Windsor;
using Cement.Core.Extensions;
using DataAccess.SessionProvider;
using NHibernate.Mapping.ByCode;

namespace Cement.Core.Services.Impl
{
    public class DocumentNumberProvider : IDocumentNumberProvider
    {
        public IWindsorContainer Container { get; set; }

        protected static System.Collections.Generic.Dictionary<Type, string> SequenceQueryDict { get; set; }

        static DocumentNumberProvider()
        {
            SequenceQueryDict = Assembly.GetExecutingAssembly().GetTypes()
                .Where(x => x.Is<IEntityAttributesMapper>())
                .Where(x => x.IsClass && !x.IsAbstract && !x.IsInterface)
                .Where(x => x.BaseType != null)
                .Where(x => x.BaseType.IsGenericType)
                .Where(x => x.BaseType.GenericTypeArguments != null)
                .Where(x => x.BaseType.GenericTypeArguments.Count() == 1)
                .Select(x =>
                {
                    var query = string.Empty;

                    var constructor = x.GetConstructor(Type.EmptyTypes);

                    if (constructor != null)
                    {
                        var map = constructor.Invoke(null);
                        var prop = x.GetProperty("TableName");

                        if (prop != null)
                        {
                            query = string.Format("SELECT nextval('{0}_id_seq')", prop.GetValue(map));
                        }
                    }

                    return new
                    {
                        entityClass = x.BaseType.GenericTypeArguments.First(),
                        query
                    };
                })
                .Where(x => !string.IsNullOrWhiteSpace(x.query))
                .ToDictionary(x => x.entityClass, x => x.query);
        }

        public string GetNewNumber(Type type)
        {
            var query = SequenceQueryDict.Get(type);

            if (string.IsNullOrWhiteSpace(query))
            {
                throw new Exception("empty query");
            }

            var newNum = Container.Resolve<ISessionProvider>().GetCurrentSession().CreateSQLQuery(query).UniqueResult<long>();

            return newNum.ToString("D8");
        }
    }
}