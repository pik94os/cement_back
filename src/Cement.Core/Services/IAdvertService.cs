﻿namespace Cement.Core.Services
{
    public interface IAdvertService
    {
        void Sign(long messageId, string comment);

        void MarkAsRead(long messageMovementId);

        //void Process(long advertId, bool send, long? employeeId, string comment);

        //void ProcessFromMovement(long advertMovementId, bool send, long? employeeId, string comment); 
    }
}