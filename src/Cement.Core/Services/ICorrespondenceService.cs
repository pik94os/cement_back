﻿using Cement.Core.Entities;
using Cement.Core.Enums;

namespace Cement.Core.Services
{
    public interface ICorrespondenceService
    {
        //void SendTo(long documentId, Employee from, Employee to, string comment);

        void ProcessDocument(long documentMovementId, DocumentProcessingResult result, string comment, Employee to = null);
    }
}