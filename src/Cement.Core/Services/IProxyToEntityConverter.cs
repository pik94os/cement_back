﻿namespace Cement.Core.Services
{
    public interface IProxyToEntityConverter
    {
        T ConvertToEntity<T, TP>(TP proxy);
    }
}