﻿using System.Collections.Generic;
using Cement.Core.Entities;
using Cement.Core.Enums;

namespace Cement.Core.Services
{
    public interface IDocumentService
    {
        void BindDocuments(IDocument document1, IDocument document2);

        void AddDocumentBinds(IDocument document1, List<string> documentKeys);

        void AddRelatedDocumentsToEntity(long entityId, List<string> documentKeys);

        Dictionary<long, RelatedDocumentsProxy> GetRelatedDocumentsDict(List<long> documentIds);

        string GetDocIdString(DocumentType type, long id);

        string GetBranchIdString(DocumentType type, int key);

        string GetDocTypeString(DocumentType type, int docKind);
    }

    public class RelatedDocumentsProxy
    {
        public List<string> Ids { get; set; }

        public string Display { get; set; }
    }
}