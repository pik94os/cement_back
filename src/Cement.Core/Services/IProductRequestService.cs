﻿using System;
using Cement.Core.Entities;

namespace Cement.Core.Services
{
    public interface IProductRequestService<T>
    {
        void Reject(long documentMovementId, string comment);

        void SignIncoming(long documentMovementId, string comment);

        void SignOutcoming(long documentMovementId, string comment, Employee employee);

        void AgreeAndSendNext(long documentMovementId, string comment, Employee to);

        long SaveChainInitialRequest(T request, Employee from);

        long SaveChainNonInitialRequest(T request, Employee from);

        void SetChainEnd(long requestId, DateTime shipmentDateTime);

        long GetInitialProductId(long requestId);
    }
}