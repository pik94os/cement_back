﻿using System;
using Castle.Windsor;
using NHibernate;

namespace Cement.Core.Services
{
    public class BaseService
    {
        public IWindsorContainer Container { get; set; }

        /// <summary>Выполнить действие в транзакции</summary>
        /// <param name="action">Действие</param>
        protected virtual void InTransaction(Action action)
        {
            using (var transaction = BeginTransaction())
            {
                try
                {
                    action();

                    transaction.Commit();
                }
                catch (Exception exception)
                {
                    transaction.Rollback();
                    throw;
                }
            }
        }

        /// <summary>Открыть транзакцию</summary>
        /// <returns>Экземпляр ITransaction</returns>
        protected virtual ITransaction BeginTransaction()
        {
            return Container.Resolve<ITransaction>();
        } 
    }
}