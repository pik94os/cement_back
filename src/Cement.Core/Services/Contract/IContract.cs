﻿using System;

namespace Cement.Core.Services.Contract
{
    public interface IContract
    {
        long Id { get; set; }

        string No { get; set; }

        DateTime? Date { get; set; }
    }
}