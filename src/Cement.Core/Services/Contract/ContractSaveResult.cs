﻿namespace Cement.Core.Services.Contract
{
    public class ContractSaveResult
    {
        public long Id { get; set; }

        public bool Success { get; set; }

        public string Message { get; set; } 
    }
}