﻿using System;
using Newtonsoft.Json;

namespace Cement.Core.Services.Contract
{
    public class BaseContract : IContract
    {
        [JsonProperty(PropertyName = "id")]
        public long Id { get; set; }

        [JsonProperty(PropertyName = "p_number")]
        public string No { get; set; }

        [JsonProperty(PropertyName = "p_date")]
        public DateTime? Date { get; set; }
    }
}