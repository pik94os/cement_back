﻿using Cement.Core.Auth;

namespace Cement.Core.Services.Contract
{
    public interface IContractService
    {
        ContractSaveResult Save(System.Net.Http.HttpRequestMessage requestMessage, IUserPrincipal userPrincipal);

        IContract Get(long id);

        int FormKind { get; }
    }
}