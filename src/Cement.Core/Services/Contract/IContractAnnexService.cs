﻿using Cement.Core.Auth;

namespace Cement.Core.Services.Contract
{
    public interface IContractAnnexService
    {
        ContractSaveResult Save(System.Net.Http.HttpRequestMessage requestMessage, IUserPrincipal userPrincipal);
    }
}