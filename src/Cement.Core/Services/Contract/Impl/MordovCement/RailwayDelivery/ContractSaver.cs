﻿using System.Linq;
using Cement.Core.Auth;
using Cement.Core.Entities;
using Cement.Core.Enums;
using Cement.Core.Enums.TermsOfPayment;
using Cement.Core.Extensions;
using Cement.Core.Services.EntityService;
using Newtonsoft.Json;

namespace Cement.Core.Services.Contract.Impl.MordovCement.RailwayDelivery
{
    public class ContractSaver : BaseSaver<ContractProxy>, IContractService
    {
        public IEntityService<ContractApplication> ContractApplicationEntityService { get; set; }
        public IEntityService<ContractAdditional> ContractAdditionalEntityService { get; set; }
        public IEntityService<ContractSpecification> ContractSpecificationEntityService { get; set; }

        public string ContractTypeName {
            get { return "Договор поставки Ж/Д"; }
        }

        public int FormKind {
            get { return 1; }
        }

        protected override ContractSaveResult Save(ContractProxy incomeProxy, IUserPrincipal userPrincipal)
        {
            return incomeProxy.Id > 0 
                ? UpdateInternal(incomeProxy, userPrincipal) 
                : SaveInternal(incomeProxy, userPrincipal);
        }

        protected ContractSaveResult UpdateInternal(ContractProxy incomeProxy, IUserPrincipal userPrincipal)
        {
            var contract = ContractEntityService.Get(incomeProxy.Id);

            contract.Name = GetContractName(incomeProxy);
            contract.No = incomeProxy.No;
            contract.Date = incomeProxy.Date;
            contract.Client = incomeProxy.Client;
            
            ContractEntityService.Update(contract);
            
            var additional = ContractAdditionalEntityService.GetAll().First(x => x.Contract == contract);

            SetTermsOfPayment(additional, incomeProxy);

            ContractAdditionalEntityService.Update(additional);
            
#warning с прайсами что-нибудь надо делать или нет пока непонятно

            return new ContractSaveResult
            {
                Id = contract.Id,
                Success = true
            };
        }

        protected ContractSaveResult SaveInternal(ContractProxy incomeProxy, IUserPrincipal userPrincipal)
        {
            var dateStr = incomeProxy.Date.HasValue
                ? string.Format(" от {0}", incomeProxy.Date.Value.ToString("dd.MM.yyyy"))
                : string.Empty;

            var contractName = GetContractName(incomeProxy);

            var annexNameTemplate = "{0} №{1}" + dateStr; // + " к " + contractName;

            var contract = new Entities.Contract
            {
                Client = incomeProxy.Client,
                Date = incomeProxy.Date,
                No = incomeProxy.No,
                Name = contractName,
                Organization = userPrincipal.Organization
            };

            ContractEntityService.Save(contract);

            var addon1 = new ContractApplication
            {
                Contract = contract,
                Date = incomeProxy.Date,
                No = "1",
                Name = string.Format(annexNameTemplate, ContractApplication.DocumentName, "1"),
                SubType = 1,
            };
            var addon2 = new ContractApplication
            {
                Contract = contract,
                Date = incomeProxy.Date,
                No = "2",
                Name = string.Format(annexNameTemplate, ContractApplication.DocumentName, "2"),
                SubType = 2,
            };
            var addon3 = new ContractApplication
            {
                Contract = contract,
                Date = incomeProxy.Date,
                No = "3",
                Name = string.Format(annexNameTemplate, ContractApplication.DocumentName, "3"),
                SubType = 3,
            };

            ContractApplicationEntityService.Save(addon1);
            ContractApplicationEntityService.Save(addon2);
            ContractApplicationEntityService.Save(addon3);

            SaveContractAnnexPriceList(addon3, incomeProxy.AddonPriceList1, 1);
            SaveContractAnnexPriceList(addon3, incomeProxy.AddonPriceList2, 2);

            var additional = new ContractAdditional
            {
                Contract = contract,
                Date = incomeProxy.Date,
                No = "1",
                Name = string.Format(annexNameTemplate, ContractAdditional.DocumentName, "1")
            };

            SetTermsOfPayment(additional, incomeProxy);

            ContractAdditionalEntityService.Save(additional);

            var spec = new ContractSpecification
            {
                Contract = contract,
                Date = incomeProxy.Date,
                No = "1",
                Name = string.Format(annexNameTemplate, ContractSpecification.DocumentName, "1")
            };

            ContractSpecificationEntityService.Save(spec);

            SaveContractAnnexPriceList(spec, incomeProxy.SpecPriceList, 0);

            return new ContractSaveResult
            {
                Id = contract.Id,
                Success = true
            };
        }

        protected void SaveContractAnnexPriceList(ContractAnnexBase annex, PriceList priceList, int type)
        {
            if (priceList != null)
            {
                var contractAnnexPriceList = new ContractAnnexPriceList
                {
                    Contract = annex.Contract,
                    ContractAnnex = annex,
                    PriceList = priceList,
                    Type = type
                };

                ContractAnnexPriceListEntityService.Save(contractAnnexPriceList);
            }
        }

        public IContract Get(long id)
        {
            var contract = ContractEntityService.GetAll()
                .Where(x => x.Id == id)
                .Select(x => new
                {
                    x.Id,
                    x.No,
                    x.Date,
                    clientId = (long?)x.Client.Id,
                    clientName = x.Client.Name,
                    clientInn = x.Client.Inn,
                    clientKpp = x.Client.Kpp,
                    clientAddress = x.Client.Address
                })
                .FirstOrDefault();

            if (contract == null)
            {
                return null;
            }

            var spec = ContractAdditionalEntityService.GetAll()
                .Where(x => x.Contract.Id == contract.Id)
                .Where(x => x.Type == ContractAnnexType.Additional)
                .Select(x => new
                {
                    x.TermsOfPaymentBankDays,
                    x.TermsOfPaymentDayNumberDay,
                    x.TermsOfPaymentDayNumberFrom,
                    x.TermsOfPaymentDayNumberKindVal,
                    x.TermsOfPaymentDayNumberTo,
                    x.TermsOfPaymentDaysKindVal,
                    x.TermsOfPaymentNextMonthDay,
                    TermsOfPaymentPayKindVal = (PayKind?) x.TermsOfPaymentPayKindVal ?? 0,
                    x.TermsOfPaymentText,
                    x.TermsOfPaymentWorkDays
                })
                .FirstOrDefault();


            var termsOfPaymentText = string.Empty;
            var termsOfPayment = new TermsOfPayment();

            if (spec != null)
            {
                termsOfPaymentText = spec.TermsOfPaymentText;
                termsOfPayment = new TermsOfPayment
                {
                    BankDays = spec.TermsOfPaymentBankDays,
                    DayNumberDay = spec.TermsOfPaymentDayNumberDay,
                    DayNumberFrom = spec.TermsOfPaymentDayNumberFrom.ToCementDateTime(),
                    DayNumberKindVal = spec.TermsOfPaymentDayNumberKindVal,
                    DayNumberTo = spec.TermsOfPaymentDayNumberTo.ToCementDateTime(),
                    DaysKindVal = spec.TermsOfPaymentDaysKindVal,
                    NextMonthDay = spec.TermsOfPaymentNextMonthDay,
                    PayKindVal = spec.TermsOfPaymentPayKindVal,
                    WorkDays = spec.TermsOfPaymentWorkDays,
                };
            }

            var termsOfPaymentJson = JsonConvert.SerializeObject(termsOfPayment);

            var contractPrices = ContractAnnexPriceListEntityService.GetAll()
                .Where(x => x.Contract.Id == contract.Id)
                .Select(x => new
                {
                    x.Id,
                    priceId = x.PriceList.Id,
                    x.PriceList.Name,
                    annexType = x.ContractAnnex.Type,
                    x.Type
                })
                .ToList();

            var addon3Price1 = contractPrices.FirstOrDefault(x => x.annexType == ContractAnnexType.Application && x.Type == 1);
            var addon3Price2 = contractPrices.FirstOrDefault(x => x.annexType == ContractAnnexType.Application && x.Type == 2);
            var specPrice = contractPrices.FirstOrDefault(x => x.annexType == ContractAnnexType.Specification);

            return new ContractProxyOut
            {
                Id = contract.Id,
                Date = contract.Date,
                No = contract.No,
                ClientId = contract.clientId,
                ClientName = contract.clientName,
                ClientInn = contract.clientInn,
                ClientKpp = contract.clientKpp,
                ClientAddress = contract.clientAddress,
                PaymentRulesDisplays = termsOfPaymentText,
                PaymentRulesJson = termsOfPaymentJson,
                AddonPriceList1Id = addon3Price1.Return(x => x.Id),
                AddonPriceList1Name = addon3Price1.Return(x => x.Name),
                AddonPriceList2Id = addon3Price2.Return(x => x.Id),
                AddonPriceList2Name = addon3Price2.Return(x => x.Name),
                SpecPriceListId = specPrice.Return(x => x.Id),
                SpecPriceListName = specPrice.Return(x => x.Name)
            };
        }

        protected void SetTermsOfPayment(ContractAdditional additional, ContractProxy incomeProxy)
        {
            if (!string.IsNullOrWhiteSpace(incomeProxy.PaymentRulesJson))
            {
                var termsOfPaymentRaw = JsonConvert.DeserializeObject<TermsOfPayment>(incomeProxy.PaymentRulesJson);

                if (termsOfPaymentRaw != null)
                {
                    additional.TermsOfPaymentText = incomeProxy.PaymentRulesDisplays;
                    additional.TermsOfPaymentBankDays = termsOfPaymentRaw.BankDays;
                    additional.TermsOfPaymentDayNumberDay = termsOfPaymentRaw.DayNumberDay;
                    additional.TermsOfPaymentDayNumberFrom = termsOfPaymentRaw.DayNumberFrom.ToDateTimeFromCementString();
                    additional.TermsOfPaymentDayNumberKindVal = termsOfPaymentRaw.DayNumberKindVal;
                    additional.TermsOfPaymentDayNumberTo = termsOfPaymentRaw.DayNumberTo.ToDateTimeFromCementString();
                    additional.TermsOfPaymentDaysKindVal = termsOfPaymentRaw.DaysKindVal;
                    additional.TermsOfPaymentNextMonthDay = termsOfPaymentRaw.NextMonthDay;
                    additional.TermsOfPaymentPayKindVal = termsOfPaymentRaw.PayKindVal;
                    additional.TermsOfPaymentWorkDays = termsOfPaymentRaw.WorkDays;
                }
            }
        }

        protected string GetContractName(ContractProxy incomeProxy)
        {
            var type = ContractTypeName + " №";
            var dateStr = incomeProxy.Date.HasValue
                ? string.Format(" от {0}", incomeProxy.Date.Value.ToString("dd.MM.yyyy"))
                : string.Empty;

            return string.Format("{0}{1}{2}", type, incomeProxy.No, dateStr);
        }
    }
}