﻿using System;
using Cement.Core.Entities;
using Newtonsoft.Json;

namespace Cement.Core.Services.Contract.Impl.MordovCement.RailwayDelivery
{
    public class ContractProxy : BaseContract
    {
        [JsonProperty(PropertyName = "p_client")]
        public virtual Client Client { get; set; }

        [JsonProperty(PropertyName = "p_addon_price1")]
        public virtual PriceList AddonPriceList1 { get; set; }

        [JsonProperty(PropertyName = "p_addon_price2")]
        public virtual PriceList AddonPriceList2 { get; set; }

        [JsonProperty(PropertyName = "p_spec_price")]
        public virtual PriceList SpecPriceList { get; set; }

        [JsonProperty(PropertyName = "p_payment_rules")]
        public string PaymentRulesJson { get; set; }

        [JsonProperty(PropertyName = "p_payment_rules_display")]
        public string PaymentRulesDisplays { get; set; }
    }

    public class ContractProxyOut : BaseContract
    {
        [JsonProperty(PropertyName = "p_client")]
        public long? ClientId { get; set; }

        [JsonProperty(PropertyName = "p_client_display")]
        public string ClientName { get; set; }

        [JsonProperty(PropertyName = "p_client_inn")]
        public string ClientInn { get; set; }

        [JsonProperty(PropertyName = "p_client_kpp")]
        public string ClientKpp { get; set; }

        [JsonProperty(PropertyName = "p_client_address")]
        public string ClientAddress { get; set; }

        [JsonProperty(PropertyName = "p_addon_price1")]
        public long? AddonPriceList1Id { get; set; }

        [JsonProperty(PropertyName = "p_addon_price1_display")]
        public string AddonPriceList1Name { get; set; }

        [JsonProperty(PropertyName = "p_addon_price2")]
        public long? AddonPriceList2Id { get; set; }

        [JsonProperty(PropertyName = "p_addon_price2_display")]
        public string AddonPriceList2Name { get; set; }

        [JsonProperty(PropertyName = "p_spec_price")]
        public long? SpecPriceListId { get; set; }

        [JsonProperty(PropertyName = "p_spec_price_display")]
        public string SpecPriceListName { get; set; }

        [JsonProperty(PropertyName = "p_payment_rules")]
        public string PaymentRulesJson { get; set; }

        [JsonProperty(PropertyName = "p_payment_rules_display")]
        public string PaymentRulesDisplays { get; set; }
    }
}