﻿using System;
using System.Linq;
using System.Net.Http;
using Castle.Core.Internal;
using Cement.Core.Auth;
using Cement.Core.Entities;
using Cement.Core.Extensions;
using Cement.Core.Services.EntityService;
using DataAccess;

namespace Cement.Core.Services.Contract.Impl
{
    public abstract class BaseSaver<T> : BaseService where T : class, new()
    {
        public IEntityService<Entities.Contract> ContractEntityService { get; set; }

        public IEntityService<ContractAnnexPriceList> ContractAnnexPriceListEntityService { get; set; }

        public IEntityService<PriceListProduct> PriceListProductEntityService { get; set; }

        public IEntityService<ContractProduct> ContractProductEntityService { get; set; }

        public virtual ContractSaveResult Save(HttpRequestMessage requestMessage, IUserPrincipal userPrincipal)
        {
            var entityProxy = requestMessage.ConvertToType<T>();

            ContractSaveResult result = null;

            InTransaction(() =>
            {
                result = Save(entityProxy, userPrincipal);

                if (result.Success)
                {
                    AfterSave(result.Id);
                }
            });

            return result;
        }

        protected abstract ContractSaveResult Save(T proxy, IUserPrincipal userPrincipal);

        protected virtual void AfterSave(long contractId)
        {
            ContractProductEntityService.GetAll()
                .Where(x => x.Contract.Id == contractId)
                .ForEach(x => ContractProductEntityService.Delete(x.Id));

            var products = ContractAnnexPriceListEntityService.GetAll()
                .Where(x => x.Contract.Id == contractId)
                .Join(PriceListProductEntityService.GetAll(),
                    x => x.PriceList.Id,
                    y => y.PriceList.Id,
                    (x, y) => new
                    {
                        y.Product,
                        y.MeasureUnit,
                        y.Tax,
                        y.Price
                    });

            products.ForEach(x =>
                ContractProductEntityService.Save(new ContractProduct
                {
                    Contract = new Entities.Contract { Id = contractId},
                    Product = x.Product,
                    MeasureUnit = x.MeasureUnit,
                    Tax = x.Tax,
                    Price = x.Price
                })
            );
        }
    }

    public abstract class BaseSaver<T, TP> : BaseSaver<TP> 
        where T : class, IEntity
        where TP : class, new()
    {
        public IProxyToEntityConverter ProxyToEntityConverter { get; set; }

        public IEntityService<T> EntityService { get; set; }
        
        protected override ContractSaveResult Save(TP entityProxy, IUserPrincipal userPrincipal)
        {
            T entity;

            try
            {
                entity = ProxyToEntityConverter.ConvertToEntity<T, TP>(entityProxy);
            }
            catch (Exception exception)
            {
                return new ContractSaveResult
                {
                    Success = false,
                    Message = exception.Message
                };
            }

            if (entity == null)
            {
                return new ContractSaveResult
                {
                    Success = false,
                    Message = "Null entity"
                };
            }

            EntityService.Save(entity);

            return new ContractSaveResult
            {
                Id = entity.Id,
                Success = true
            }; 
        }

        protected override void AfterSave(long contractId)
        {
        }
    }
}