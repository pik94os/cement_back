﻿using Cement.Core.Enums.TermsOfPayment;
using Newtonsoft.Json;

namespace Cement.Core.Services.Contract.Impl
{
    public class TermsOfPayment
    {
        [JsonProperty(PropertyName = "p_pay_kind")]
        public PayKind PayKindVal { get; set; }

        [JsonProperty(PropertyName = "p_pay_days_kind")]
        public DaysKind? DaysKindVal { get; set; }

        [JsonProperty(PropertyName = "p_pay_days_work")]
        public int? WorkDays { get; set; }

        [JsonProperty(PropertyName = "p_pay_days_bank")]
        public int? BankDays { get; set; }

        [JsonProperty(PropertyName = "p_pay_next_day_value")]
        public int? NextMonthDay { get; set; }

        [JsonProperty(PropertyName = "p_pay_day_number_kind")]
        public DayNumberKind? DayNumberKindVal { get; set; }

        [JsonProperty(PropertyName = "p_pay_day_number_day")]
        public int? DayNumberDay { get; set; }

        [JsonProperty(PropertyName = "p_pay_day_number_from")]
        public string DayNumberFrom { get; set; }

        [JsonProperty(PropertyName = "p_pay_day_number_to")]
        public string DayNumberTo { get; set; }
    }
}