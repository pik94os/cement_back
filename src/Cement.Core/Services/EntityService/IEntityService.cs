﻿using System.Linq;

namespace Cement.Core.Services.EntityService
{
    public interface IEntityService<T> 
    {
        void Delete(long id);

        T Get(long id);

        IQueryable<T> GetAll();

        T Load(long id);

        void Save(T value);

        void Update(T value);
    }
}