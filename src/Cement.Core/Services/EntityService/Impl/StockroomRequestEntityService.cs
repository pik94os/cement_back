﻿using System;
using System.Linq;
using Cement.Core.Auth;
using Cement.Core.Entities;
using Cement.Core.Entities.Stockroom;
using Cement.Core.Enums;
using Cement.Core.Exceptions;

namespace Cement.Core.Services.EntityService.Impl
{
    public class StockroomRequestEntityService : BaseEntityService<StockroomBaseRequest>
    {
        public IEntityService<StockroomProduct> StockroomProductEntityService { get; set; }

        protected override void BeforeSave(StockroomBaseRequest value)
        {
            if (value.Warehouse == null)
            {
                throw new ValidationException("Ошибка определения склада");
            }

            base.BeforeSave(value);
            
            BeforeSaveInternal(value);

            value.DocumentState = DocumentState.Draft;
        }

        protected override void BeforeUpdate(StockroomBaseRequest value)
        {
            base.BeforeUpdate(value);

            if (value.DocumentState == 0)
            {
                value.DocumentState = DocumentState.Draft;
            }

            BeforeSaveInternal(value);
        }

        protected void BeforeSaveInternal(StockroomBaseRequest value)
        {
            if (value is StockroomPostingRequest || value is StockroomMakingPostingRequest)
            {
                GetStockroomProductOnProduct(value);
            }
        }

        private void GetStockroomProductOnProduct(StockroomBaseRequest value)
        {
            if (value.ProductId == null)
            {
                throw new ValidationException("Не задан товар");
            }

            var stockroomProduct = StockroomProductEntityService.GetAll()
                .Where(x => x.Warehouse == value.Warehouse)
                .Where(x => x.Product.Id == value.ProductId)
                .Where(x => x.MeasureUnit.Id == value.MeasureUnit.Id)
                .FirstOrDefault();

            if (stockroomProduct == null)
            {
                stockroomProduct = new StockroomProduct
                {
                    Warehouse = value.Warehouse,
                    MeasureUnit = value.MeasureUnit,
                    Product = new Product { Id = value.ProductId.Value }
                };

                StockroomProductEntityService.Save(stockroomProduct);
            }

            value.StockroomProduct = stockroomProduct;
        }

        protected override void AfterSave(StockroomBaseRequest value)
        {
            var route = new StockroomRequestRoute
            {
                Employee = Container.Resolve<IUserPrincipal>().Employee,
                MovementType = DocumentMovementType.Outcoming,
                Document = value,
                DateIn = DateTime.Now
            };

            Container.Resolve<IEntityService<StockroomRequestRoute>>().Save(route);

            base.AfterSave(value);
        }
    }
}