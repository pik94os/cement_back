﻿using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using Cement.Core.Auth;
using Cement.Core.Entities;
using Cement.Core.Enums;

namespace Cement.Core.Services.EntityService.Impl
{
    public class WarehouseEntityService : BaseEntityService<Warehouse>
    {
        public IEntityService<WarehouseEmployee> WarehouseEmployeeEntityService { get; set; }

        protected override void BeforeSave(Warehouse entity)
        {
            base.BeforeSave(entity);

            entity.Address = GenerateAddress(entity);
            SetTypeStr(entity);
        }

        protected override void BeforeUpdate(Warehouse value)
        {
            base.BeforeUpdate(value);

            value.Address = GenerateAddress(value);
            SetTypeStr(value);
        }

        protected override void AfterSave(Warehouse entity)
        {
            SaveOrUpdateEmployes(entity.Employes, entity);

            base.AfterSave(entity);
        }

        protected override void AfterUpdate(Warehouse entity)
        {
            SaveOrUpdateEmployes(entity.Employes, entity);

            base.AfterUpdate(entity);
        }

        private void SetTypeStr(Warehouse entity)
        {
            switch (entity.Type)
            {
                case WarehouseType.Auto:
                    entity.TypeStr = "Авто";
                    break;
                case WarehouseType.Railway:
                    entity.TypeStr = "Ж/Д";
                    break;
            }
        }

        private void SaveOrUpdateEmployes(List<long> employeeIds, Warehouse warehouse)
        {
            var existingList = WarehouseEmployeeEntityService.GetAll()
                .Where(x => x.Warehouse == warehouse)
                .ToDictionary(x => x.Employee.Id);

            var anyChange = false;

            foreach (var existing in existingList)
            {
                if (!employeeIds.Contains(existing.Key))
                {
                    WarehouseEmployeeEntityService.Delete(existing.Value.Id);
                    anyChange = true;
                }
            }

            foreach (var employeeId in employeeIds)
            {
                if (!existingList.ContainsKey(employeeId))
                {
                    WarehouseEmployeeEntityService.Save(new WarehouseEmployee { Warehouse = warehouse, Employee = new Employee { Id = employeeId } });
                    anyChange = true;
                }
            }

            if (anyChange)
            {
                Container.Resolve<IUserCache>().UpdateCache();
            }
        }

        private string GenerateAddress(Warehouse entity)
        {
            var strList = new List<string>()
            {
                entity.AddressIndex,
                entity.AddressCountry,
                entity.AddressRegion,
                entity.AddressArea,
                entity.AddressLocality,
                entity.AddressStreet,
                entity.AddressHouse,
                entity.AddressHousing,
                entity.AddressBuilding,
                entity.AddressOffice
            };
            
            return string.Join(", ", strList.Where(x => !string.IsNullOrWhiteSpace(x)));
        }
    }
}