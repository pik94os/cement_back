﻿using System.Linq;
using Cement.Core.Entities.Stockroom;

namespace Cement.Core.Services.EntityService.Impl
{
    public class StockroomExpenseOrderRouteEntityService : BaseEntityService<StockroomExpenseOrderRoute>
    {
        public IEntityService<StockroomExpenseOrderMovement> StockroomExpenseOrderMovementEntityService { get; set; }
        public IEntityService<StockroomExpenseOrderWriteoff> StockroomExpenseOrderWriteoffEntityService { get; set; }
        public IEntityService<StockroomExpenseOrderSale> StockroomExpenseOrderSaleEntityService { get; set; }
        
        protected override void AfterUpdate(StockroomExpenseOrderRoute value)
        {
            if (value.ArchivedAt == null && value.DeletedAt.HasValue)
            {
                UpdateRequestUsage(value.Document);
            }
            
            base.AfterUpdate(value);
        }

        protected void UpdateRequestUsage(StockroomExpenseOrderBase value)
        {
            switch (value.StockroomExpenseOrderType)
            {
                case StockroomExpenseOrderType.Movement:
                {
                    var stockroomMovementRequest = StockroomExpenseOrderMovementEntityService.GetAll()
                        .Where(x => x.Id == value.Id)
                        .Select(x => x.StockroomMovementRequest)
                        .First();

                    stockroomMovementRequest.UsedAt = null;

                    break;
                }

                case StockroomExpenseOrderType.Writeoff:
                {
                    var stockroomWriteoffRequest = StockroomExpenseOrderWriteoffEntityService.GetAll()
                        .Where(x => x.Id == value.Id)
                        .Select(x => x.StockroomWriteoffRequest)
                        .First();

                    stockroomWriteoffRequest.UsedAt = null;

                    break;
                }

                case StockroomExpenseOrderType.Sale:
                {
                    var productRequest = StockroomExpenseOrderSaleEntityService.GetAll()
                        .Where(x => x.Id == value.Id)
                        .Select(x => x.ProductRequest)
                        .First();

                    productRequest.UsedAt = null;

                    break;
                }
            }
        }
    }
}