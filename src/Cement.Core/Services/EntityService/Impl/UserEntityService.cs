﻿using System.Linq;
using Cement.Core.Auth;
using Cement.Core.Entities;
using Cement.Core.Exceptions;
using Cement.Core.Extensions;
using Cement.Core.Services.Log;
using Cement.Core.Services.Mail;

namespace Cement.Core.Services.EntityService.Impl
{
    public class UserEntityService : BaseEntityService<User>
    {
        public IMailSender MailSender { get; set; }

        public ILogger Logger { get; set; }

        protected override void AfterDelete(long id)
        {
            UpdateUserCache();

            base.AfterDelete(id);
        }

        protected override void AfterSave(User value)
        {
            UpdateUserCache();

            SendPasswordMail(value);

            base.AfterSave(value);
        }

        protected override void AfterUpdate(User value)
        {
            UpdateUserCache();

            if (!string.IsNullOrWhiteSpace(value.Password))
            {
                SendPasswordMail(value);
            }

            base.AfterUpdate(value);
        }

        private void UpdateUserCache()
        {
            var userCache = Container.Resolve<IUserCache>();

            try
            {
                userCache.UpdateCache();
            }
            finally
            {
                Container.Release(userCache);
            }
        }

        protected override void BeforeSave(User value)
        {
            if (Repository.GetAll().Count(x => x.Login.ToLower() == value.Login.ToLower()) > 0)
            {
                throw new ValidationException(string.Format("Пользователь с логином '{0}' уже существует", value.Login));
            }
        }

        protected void SendPasswordMail(User value)
        {
            var subject = "Реквизиты доступа к системе";
            var mailBody = string.Format("Логин: {0}<br />Пароль: {1}", value.Email, value.Password);

            MailSender.Send(subject, mailBody, value.Email);
            
#warning на время тестирования логины/пароли будем дублировать в файл
            Logger.Log("passwd", string.Format("{0}@{1} ({2})", value.Email, value.Password, value.Organization.Return(x => x.Name) ?? "System"));
        }
    }
}