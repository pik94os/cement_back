﻿using System;
using System.Collections.Generic;
using System.Linq;
using Cement.Core.Auth;
using Cement.Core.Auth.Impl;
using Cement.Core.Entities;
using Cement.Core.Enums;
using Cement.Core.Extensions;
using DataAccess.Repository;
using Newtonsoft.Json;

namespace Cement.Core.Services.EntityService.Impl
{
    public class BaseRequestEntityService<T> : FileFieldEntityService<T> where T: BaseRequest
    {
        protected override void BeforeSave(T entity)
        {
            base.BeforeSave(entity);

            SetParams(entity);
        }

        protected override void BeforeUpdate(T entity)
        {
            base.BeforeSave(entity);

            SetParams(entity);
        }

        private void SetParams(BaseRequest entity)
        {
            entity.Content = entity.ContentString.GetBytes();
            entity.Name = entity.Number;

            if (!Enum.IsDefined(typeof(ImportanceType), entity.Importance))
            {
                entity.Importance = ImportanceType.Usual;
            }

            if (!Enum.IsDefined(typeof(SecrecyType), entity.Secrecy))
            {
                entity.Secrecy = SecrecyType.Usual;
            }
            
            if (entity.UnshipmentDateTime > DateTime.MinValue && entity.UnshipmentTime > DateTime.MinValue)
            {
                entity.UnshipmentDateTime = new DateTime(
                    entity.UnshipmentDateTime.Value.Year,
                    entity.UnshipmentDateTime.Value.Month,
                    entity.UnshipmentDateTime.Value.Day,
                    entity.UnshipmentTime.Value.Hour,
                    entity.UnshipmentTime.Value.Minute,
                    entity.UnshipmentTime.Value.Second);
            }

            var userPrincipal = Container.Resolve<IUserPrincipal>();
            entity.Payer = userPrincipal.Organization;
            entity.PayerContactPerson = userPrincipal.Employee;
        }
    }
}