﻿using Cement.Core.Entities;
using Cement.Core.Exceptions;

namespace Cement.Core.Services.EntityService.Impl
{
    public class VehicleArendEntityService : BaseEntityService<VehicleArend>
    {
        protected override void BeforeUpdate(VehicleArend value)
        {
            base.BeforeUpdate(value);

            Validate(value);
        }

        protected override void BeforeSave(VehicleArend value)
        {
            base.BeforeSave(value);

            Validate(value);

            value.IsActive = true;
        }

        private void Validate(VehicleArend value)
        {
            if (value.Vehicle == null)
            {
                throw new ValidationException("Не задан ТС");
            }
        }
    }
}