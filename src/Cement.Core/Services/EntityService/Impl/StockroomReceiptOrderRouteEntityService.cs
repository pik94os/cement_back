﻿using System.Linq;
using Cement.Core.Entities.Stockroom;

namespace Cement.Core.Services.EntityService.Impl
{
    public class StockroomReceiptOrderRouteEntityService : BaseEntityService<StockroomReceiptOrderRoute>
    {
        public IEntityService<StockroomReceiptOrderBuy> StockroomReceiptOrderBuyEntityService { get; set; }
        public IEntityService<StockroomReceiptOrderMovement> StockroomReceiptOrderMovementEntityService { get; set; }
        public IEntityService<StockroomReceiptOrderPosting> StockroomReceiptOrderPostingEntityService { get; set; }

        protected override void AfterUpdate(StockroomReceiptOrderRoute value)
        {
            if (value.ArchivedAt == null && value.DeletedAt.HasValue)
            {
                UpdateRequestUsage(value.Document);
            }
            
            base.AfterUpdate(value);
        }

        protected void UpdateRequestUsage(StockroomReceiptOrderBase value)
        {
            switch (value.StockroomReceiptOrderType)
            {
                case StockroomReceiptOrderType.Buy:
                {
                    var stockroomReceiptOrderItem = StockroomReceiptOrderBuyEntityService.GetAll()
                        .Where(x => x.Id == value.Id)
                        .Select(x => x.StockroomReceiptOrderItemOuter)
                        .First();

                    stockroomReceiptOrderItem.UsedAt = null;

                    break;
                }

                case StockroomReceiptOrderType.Movement:
                {
                    var stockroomReceiptOrderItem = StockroomReceiptOrderMovementEntityService.GetAll()
                        .Where(x => x.Id == value.Id)
                        .Select(x => x.StockroomReceiptOrderItemMove)
                        .First();

                    stockroomReceiptOrderItem.UsedAt = null;

                    break;
                }

                case StockroomReceiptOrderType.Posting:
                {
                    var stockroomReceiptOrderItem = StockroomReceiptOrderPostingEntityService.GetAll()
                        .Where(x => x.Id == value.Id)
                        .Select(x => x.StockroomReceiptOrderItemPosting)
                        .First();

                    stockroomReceiptOrderItem.UsedAt = null;

                    break;
                }
            }
        }
    }
}