﻿using System.Collections.Generic;
using System.Linq;
using Castle.Core.Internal;
using Cement.Core.Entities;
using Cement.Core.Extensions;
using DataAccess;

namespace Cement.Core.Services.EntityService.Impl
{
    public class DocumentEntityService<T> : FileFieldEntityService<T> where T : class, IEntity, IDocument 
    {
        public IEntityService<DocumentRelation> DocumentRelationEntityService { get; set; }

        public IDocumentService DocumentService { get; set; }

        protected override void BeforeDelete(long id)
        {
            base.BeforeDelete(id);

            DocumentRelationEntityService.GetAll()
                .Where(x => x.JoinedDocId == id || x.BaseDocumentId == id)
                .ForEach(x => DocumentRelationEntityService.Delete(x.Id));
        }

        protected override void AfterUpdate(T value)
        {
            var idocument = (IDocument) value;

            ProcessRelatedDocuments(idocument);

            var documents = DocumentRelationEntityService.GetAll()
                .Where(x => x.JoinedDocId == idocument.Id)
                .ToList();

            documents.ForEach(document =>
            {
                document.Name = idocument.Name;
                document.No = idocument.No;
                document.Date = idocument.Date;
                document.State = idocument.State;
                
                DocumentRelationEntityService.Update(document);
            });

            base.AfterUpdate(value);
        }

        private void ProcessRelatedDocuments(IDocument document)
        {
            if (document.RelatedDocumentIds == null)
            {
                return;
            }

            var existingRelations = DocumentRelationEntityService.GetAll()
                .Where(x => x.JoinedDocId == document.Id || x.BaseDocumentId == document.Id)
                .ToList();

            var existingLeftJoinsDict = existingRelations
                .Where(x => x.BaseDocumentId == document.Id)
                .ToDictionary(x => DocumentService.GetDocIdString(x.JoinedDocumentType, x.JoinedDocId));

            var existingRightJoinsDict = existingRelations
                .Where(x => x.JoinedDocId == document.Id)
                .ToDictionary(x => DocumentService.GetDocIdString(x.BaseDocumentType, x.BaseDocumentId));

            var listToDel = existingLeftJoinsDict
                .Where(x => !document.RelatedDocumentIds.Contains(x.Key))
                .Select(x => x.Key)
                .ToList();

            listToDel.ForEach(x =>
            {
                var valToDel = existingLeftJoinsDict.Get(x);

                if (valToDel != null)
                {
                    DocumentRelationEntityService.Delete(valToDel.Id);
                }

                valToDel = existingRightJoinsDict.Get(x);

                if (valToDel != null)
                {
                    DocumentRelationEntityService.Delete(valToDel.Id);
                }
            });

            foreach (var doc in document.RelatedDocumentIds.Where(x => !existingLeftJoinsDict.Keys.Contains(x)))
            {
                DocumentService.AddDocumentBinds(document, new List<string> {doc});
            }
        }
    }
}