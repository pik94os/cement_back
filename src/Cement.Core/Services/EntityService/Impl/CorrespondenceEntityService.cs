﻿using System;
using System.Linq;
using Castle.Core.Internal;
using Cement.Core.Auth;
using Cement.Core.Entities;
using Cement.Core.Enums;
using Cement.Core.Extensions;

namespace Cement.Core.Services.EntityService.Impl
{
    public class CorrespondenceEntityService : DocumentEntityService<Correspondence>
    {
        public IEntityService<CorrespondenceMovement> CorrespondenceMovementEntityService { get; set; }
        
        public IEntityService<CorrespondenceRoute> CorrespondenceRouteEntityService { get; set; }

        protected override void BeforeUpdate(Correspondence value)
        {
            base.BeforeUpdate(value);

            ProcessName(value);

            if (value.DocumentState == 0)
            {
                value.DocumentState = DocumentState.Draft;
            }
            value.Content = value.ContentString.GetBytes();
        }

        protected override void BeforeSave(Correspondence value)
        {
            base.BeforeSave(value);

            ProcessName(value);

            value.DocumentState = DocumentState.Draft;
            value.Content = value.ContentString.GetBytes();
            value.Creator = Container.Resolve<IUserPrincipal>().Employee;
        }

        protected override void AfterSave(Correspondence value)
        {
            DocumentService.AddDocumentBinds(value, value.RelatedDocumentIds);

            // Создаваемая корреспонденция сразу должна быть задана как исходящая
            SetOutcoming(value);

            base.AfterSave(value);
        }

        protected override void BeforeDelete(long id)
        {
            base.BeforeDelete(id);

            CorrespondenceMovementEntityService.GetAll()
                .Where(x => x.Document.Id == id)
                .ForEach(x => CorrespondenceMovementEntityService.Delete(x.Id));

            CorrespondenceRouteEntityService.GetAll()
                .Where(x => x.Document.Id == id)
                .ForEach(x => CorrespondenceRouteEntityService.Delete(x.Id));
        }

        private void ProcessName(Correspondence correspondence)
        {
            var dateStr = correspondence.Date.HasValue
                ? " от " + correspondence.Date.Value.ToString("dd.MM.yyyy")
                : string.Empty;

            var docType = correspondence.CorrespondenceType.GetDisplayValue();

            correspondence.Name = string.Format("{0} №{1}{2}", docType, correspondence.No, dateStr).Trim();
        }

        /// <summary>
        /// Установить корреспонденцию как исходящую
        /// </summary>
        private void SetOutcoming(Correspondence correspondence)
        {
            // Добавляем запись в маршрут документа
            var docRoute = new CorrespondenceRoute
            {
                DateIn = DateTime.Now,
                Document = correspondence,
                Employee = correspondence.Creator
            };

            // Добавляем документ в исходящие отправителя
            var outcomingDoc = new OutcomingCorrespondence
            {
                Document = correspondence,
                Employee = correspondence.Creator,
                Route = docRoute
            };

            CorrespondenceRouteEntityService.Save(docRoute);
            CorrespondenceMovementEntityService.Save(outcomingDoc);
        }
    }
}