﻿using System.Collections.Generic;
using System.Linq;
using Castle.Core.Internal;
using Cement.Core.Auth;
using Cement.Core.Entities;
using Cement.Core.Enums;
using Cement.Core.Extensions;
using Newtonsoft.Json;

namespace Cement.Core.Services.EntityService.Impl
{
    public class MessageEntityService : DocumentEntityService<Message>
    {
        public IEntityService<MessageRecipient> MessageRecipientEntityService { get; set; }

        protected override void BeforeUpdate(Message value)
        {
            base.BeforeUpdate(value);

            Process(value);
        }

        protected override void BeforeSave(Message value)
        {
            base.BeforeSave(value);

            Process(value);
        }

        protected override void AfterSave(Message value)
        {
            //DocumentService.AddDocumentBinds(value, value.RelatedDocumentIds);

            if (value.RecipientIds != null)
            {
                value.RecipientIds.ForEach(x =>
                {
                    if (x > 0)
                    {
                        MessageRecipientEntityService.Save(new MessageRecipient
                        {
                            Message = value,
                            Recipient = new Employee { Id = x }
                        });
                    }
                });
            }

            base.AfterSave(value);
        }

        protected override void AfterUpdate(Message value)
        {
            var existing = MessageRecipientEntityService.GetAll()
                .Where(x => x.Message.Id == value.Id)
                .Select(x => new
                {
                    x.Id,
                    EmployeeId = x.Recipient.Id
                })
                .ToList();

            var currentRecipientList = value.RecipientIds ?? new List<long>();

            var listToDel = existing
                .Where(x => !currentRecipientList.Contains(x.EmployeeId))
                .Select(x => x.Id)
                .ToList();

            listToDel.ForEach(x => MessageRecipientEntityService.Delete(x));


            currentRecipientList.Where(x => !existing.Any(y => y.EmployeeId == x))
                .ForEach(x =>
                {
                    if (x > 0)
                    {
                        MessageRecipientEntityService.Save(new MessageRecipient
                        {
                            Message = value,
                            Recipient = new Employee { Id = x }
                        });
                    }
                });

            base.AfterUpdate(value);
        }

        private void Process(Message message)
        {
            var dateStr = message.Date.HasValue
                ? " от " + message.Date.Value.ToString("dd.MM.yyyy")
                : string.Empty;

            var docType = message.MessageType.GetDisplayValue();

            message.Name = (docType + dateStr).Trim();

            message.DocumentState = DocumentState.Draft;
            message.Content = message.ContentString.GetBytes();
            message.Creator = Container.Resolve<IUserPrincipal>().Employee;

            if (string.IsNullOrWhiteSpace(message.RepeatDetails))
            {
                return;
            }

            try
            {
                var repeatProxy = JsonConvert.DeserializeObject<MessageRepeatProxy>(message.RepeatDetails);

                message.RepeatStartAt = repeatProxy.BeginDate.ToDateTime();

                switch (repeatProxy.EndKind)
                {
                    case RepeatEndKind.Date:
                        message.RepeatStopAt = repeatProxy.EndDate.ToDateTime();
                        message.RepeatCount = null;
                        break;

                    case RepeatEndKind.AfterNRepeats:
                        message.RepeatStopAt = null;
                        message.RepeatCount = repeatProxy.EndCount;
                        break;

                    default:
                        message.RepeatStopAt = null;
                        message.RepeatCount = null;
                        break;
                }
            }
            catch
            {
            }
        }

        protected override void BeforeDelete(long id)
        {
            base.BeforeDelete(id);

            // Сразу вытягиваем объектами, иначе хибер для каждого объекта будет по-отдельности делать запрос к БД
            var existing = MessageRecipientEntityService.GetAll()
                .Where(x => x.Message.Id == id)
                .ToList();

            existing.ForEach(x => MessageRecipientEntityService.Delete(x.Id));
        }
    }
}