﻿using System;
using System.Linq;
using System.Linq.Expressions;
using Castle.Windsor;
using Cement.Core.Auth;
using Cement.Core.Entities;
using DataAccess;
using DataAccess.Repository;
using NHibernate;

namespace Cement.Core.Services.EntityService.Impl
{
    public class BaseEntityService<T> : IEntityService<T> where T : IEntity 
    {
        public IWindsorContainer Container { get; set; }

        public IRepository<T> Repository { get; set; }

        /// <summary>Выполнить действие в транзакции</summary>
        /// <param name="action">Действие</param>
        protected virtual void InTransaction(Action action)
        {
            using (var transaction = BeginTransaction())
            {
                try
                {
                    action();

                    transaction.Commit();
                }
                catch(Exception exception)
                {
                    transaction.Rollback();
                    throw;
                }
            }
        }

        /// <summary>Открыть транзакцию</summary>
        /// <returns>Экземпляр ITransaction</returns>
        protected virtual ITransaction BeginTransaction()
        {
            return Container.Resolve<ITransaction>();
        }
        
        public virtual void Delete(long id)
        {
            DeleteInternal(id);
        }

        public virtual T Get(long id)
        {
            return Repository.Get(id);
        }

        public virtual IQueryable<T> GetAll()
        {
            return Repository.GetAll();
        }

        public virtual T Load(long id)
        {
            return Repository.Load(id);
        }

        public virtual void Save(T value)
        {
            if (value.Id > 0)
            {
                Update(value);
            }
            else
            {
                SaveInternal(value);
            }
        }

        public virtual void Update(T value)
        {
            UpdateInternal(value);
        }

        protected virtual void DeleteInternal(long id)
        {
            InTransaction(() =>
            {
                BeforeDelete(id);
                
                Repository.Delete(id);

                AfterDelete(id);
            });
        }

        protected virtual void SaveInternal(T value)
        {
            InTransaction(() =>
            {
                BeforeSave(value);

                Repository.Save(value);

                AfterSave(value);
            });
        }

        protected virtual void UpdateInternal(T value)
        {
            InTransaction(() =>
            {
                BeforeUpdate(value);

                Repository.Update(value);

                AfterUpdate(value);
            });
        }

        protected virtual void BeforeSave(T value)
        {
            BeforeOrganizationBasedEntitySave(value);
        }

        protected virtual void AfterSave(T value)
        {

        }

        protected virtual void BeforeUpdate(T value)
        {
            BeforeOrganizationBasedEntityUpdate(value);
        }

        protected virtual void AfterUpdate(T value)
        {

        }

        protected virtual void BeforeDelete(long id)
        {

        }

        protected virtual void AfterDelete(long id)
        {

        }

        protected virtual void BeforeOrganizationBasedEntitySave(T value)
        {
            if (value is BaseOrganizationBasedEntity)
            {
                var userPrincipal = Container.Resolve<IUserPrincipal>();

                (value as BaseOrganizationBasedEntity).Organization = userPrincipal.Organization;
            }
        }

        protected virtual void BeforeOrganizationBasedEntityUpdate(T value)
        {
            if (value is BaseOrganizationBasedEntity)
            {
                var userPrincipal = Container.Resolve<IUserPrincipal>();

                var organization = userPrincipal.Organization;

                if (organization == null)
                {                    
                    var parameter = Expression.Parameter(typeof(T), "x");

                    var propertyExpression = Expression.Property(parameter, "Organization");

                    var lambda = Expression.Lambda<Func<T, Organization>>(propertyExpression, parameter);

                    organization = Repository.GetAll().Where(x => x.Id == value.Id).Select(lambda).FirstOrDefault();
                }
                
                (value as BaseOrganizationBasedEntity).Organization = organization;
            }
        }
    }
}