﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Castle.Core.Internal;
using Cement.Core.Auth;
using Cement.Core.Auth.Impl;
using Cement.Core.Entities;
using Cement.Core.Enums;
using DataAccess.Repository;

namespace Cement.Core.Services.EntityService.Impl
{
    public class CatalogueEntityService : FileFieldEntityService<Catalogue>
    {
        public IDocumentService DocumentService { get; set; }

        protected override void BeforeSave(Catalogue entity)
        {
            base.BeforeSave(entity);

            entity.DescriptionBytes = Encoding.UTF8.GetBytes(entity.Description);

            var organizationId = Container.Resolve<IUserPrincipal>().Organization.Id;

            // удаляем предыдущие каталоги организации
            Repository.GetAll()
            .Where(x => x.Organization.Id == organizationId)
            .AsEnumerable()
            .ForEach(x => Repository.Delete(x.Id));
        }
    }
}