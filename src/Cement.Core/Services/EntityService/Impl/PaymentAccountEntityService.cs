﻿using System.Linq;
using Cement.Core.Entities;
using DataAccess.Repository;

namespace Cement.Core.Services.EntityService.Impl
{
    public class PaymentAccountEntityService : BaseEntityService<PaymentAccount>
    {
        protected override void BeforeUpdate(PaymentAccount value)
        {
            base.BeforeUpdate(value);

            GetBankByBik(value);
        }

        protected override void BeforeSave(PaymentAccount value)
        {
            base.BeforeSave(value);

            GetBankByBik(value);
        }

        private void GetBankByBik(PaymentAccount value)
        {
            var bank = Container.Resolve<IRepository<Bank>>().GetAll().FirstOrDefault(x => x.Bik == value.Bik);
            if (bank != null)
            {
                value.Bank = bank;
            }
        }
    }
}