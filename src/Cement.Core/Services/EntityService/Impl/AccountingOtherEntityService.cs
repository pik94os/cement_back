﻿using System;
using System.Linq;
using Castle.Core.Internal;
using Cement.Core.Auth;
using Cement.Core.Entities;
using Cement.Core.Entities.Stockroom;
using Cement.Core.Enums;
using Cement.Core.Exceptions;

namespace Cement.Core.Services.EntityService.Impl
{
    public class AccountingOtherEntityService : BaseEntityService<AccountingOtherBase>
    {
        private static object _lockObject = new object();

        public IEntityService<StockroomExpenseOrderMovement> StockroomExpenseOrderMovementEntityService { get; set; }
        public IEntityService<StockroomExpenseOrderWriteoff> StockroomExpenseOrderWriteoffEntityService { get; set; }
        public IEntityService<StockroomBaseRequest> StockroomBaseRequestEntityService { get; set; }
        public IEntityService<AccountingOtherRoute> AccountingOtherRouteEntityService { get; set; }

        protected override void BeforeSave(AccountingOtherBase value)
        {
            base.BeforeSave(value);
            
            value.DocumentState = DocumentState.Draft;
        }

        protected override void BeforeUpdate(AccountingOtherBase value)
        {
            base.BeforeUpdate(value);

            if (value.DocumentState == 0)
            {
                value.DocumentState = DocumentState.Draft;
            }
        }

        protected override void AfterSave(AccountingOtherBase value)
        {
            var route = new AccountingOtherRoute
            {
                Employee = Container.Resolve<IUserPrincipal>().Employee,
                MovementType = DocumentMovementType.Outcoming,
                Document = value,
                DateIn = DateTime.Now
            };

            Container.Resolve<IEntityService<AccountingOtherRoute>>().Save(route);

            ProcessRelatedDocs(value);

            base.AfterSave(value);
        }

        protected override void AfterUpdate(AccountingOtherBase value)
        {
            ProcessRelatedDocs(value);

            base.AfterUpdate(value);
        }

        public override void Save(AccountingOtherBase value)
        {
            lock (_lockObject)
            {
                base.Save(value);
            }
        }

        protected void ProcessRelatedDocs(AccountingOtherBase value)
        {
            if (value is AccountingOtherMovementBill)
            {
                var accountingOtherMovementBill = value as AccountingOtherMovementBill;

                var data = StockroomExpenseOrderMovementEntityService.GetAll()
                    .Where(x => value.DstItems.Contains(x.Id) || x.AccountingOtherMovementBill.Id == value.Id)
                    .ToList();

                if (!data.All(x => x.AccountingOtherMovementBill == null || x.AccountingOtherMovementBill.Id == value.Id))
                {
                    throw new ValidationException("Ордер используется в другой накладной");
                }

                foreach (var expense in data)
                {
                    expense.AccountingOtherMovementBill = value.DstItems.Contains(expense.Id) ? accountingOtherMovementBill : null;
                }
            }
            else if (value is AccountingOtherWriteoffAct)
            {
                var accountingOtherWriteoffAct = value as AccountingOtherWriteoffAct;

                var data = StockroomExpenseOrderWriteoffEntityService.GetAll()
                    .Where(x => value.DstItems.Contains(x.Id) || x.AccountingOtherWriteoffAct.Id == value.Id)
                    .ToList();

                if (!data.All(x => x.AccountingOtherWriteoffAct == null || x.AccountingOtherWriteoffAct.Id == value.Id))
                {
                    throw new ValidationException("Ордер используется в другом акте");
                }

                foreach (var expense in data)
                {
                    expense.AccountingOtherWriteoffAct = value.DstItems.Contains(expense.Id) ? accountingOtherWriteoffAct : null;
                }
            }
            else if (value is AccountingOtherPostingAct)
            {
                var accountingOtherPostingAct = value as AccountingOtherPostingAct;

                var data = StockroomBaseRequestEntityService.GetAll()
                    .Where(x => x.StockroomRequestType == StockroomRequestType.Posting || x.StockroomRequestType == StockroomRequestType.MakingPosting)
                    .Where(x => value.DstItems.Contains(x.Id) || (x as StockroomPostingRequest).AccountingOtherPostingAct.Id == value.Id)
                    .ToList();

                if (!data.All(x => (
                        (x is StockroomPostingRequest) &&
                            ((x as StockroomPostingRequest).AccountingOtherPostingAct == null
                            || (x as StockroomPostingRequest).AccountingOtherPostingAct.Id == value.Id))
                       || (
                        (x is StockroomMakingPostingRequest) &&
                            ((x as StockroomMakingPostingRequest).AccountingOtherPostingAct == null
                            || (x as StockroomMakingPostingRequest).AccountingOtherPostingAct.Id == value.Id))))
                {
                    throw new ValidationException("Заявка используется в другом акте");
                }

                foreach (var expense in data)
                {
                    if (expense is StockroomPostingRequest)
                    {
                        (expense as StockroomPostingRequest).AccountingOtherPostingAct = value.DstItems.Contains(expense.Id) ? accountingOtherPostingAct : null;
                    }
                    else if (expense is StockroomMakingPostingRequest)
                    {
                        (expense as StockroomMakingPostingRequest).AccountingOtherPostingAct = value.DstItems.Contains(expense.Id) ? accountingOtherPostingAct : null;
                    }
                }
            }
        }

        protected override void BeforeDelete(long id)
        {
            /// Бухгалтерские документы в общем не удаляются, но можно удалить некорректно созданный, т.е. тот, который в новых

            base.BeforeDelete(id);

            AccountingOtherRouteEntityService.GetAll().Where(x => x.Document.Id == id)
                .ForEach(x => AccountingOtherRouteEntityService.Delete(x.Id));

            var entity = Repository.Load(id);

            ReleaseRelatedDocs(entity);
        }

        protected void ReleaseRelatedDocs(AccountingOtherBase value)
        {
            // тут намеренно используется switch по enum, т.к. не получается использовать оператор is
            // потому что NHibernate из базы laod-ит proxy-объекты

            switch (value.AccountingOtherDocumentType)
            {
                case AccountingOtherDocumentType.MovementBill:
                {
                    var data = StockroomExpenseOrderMovementEntityService.GetAll()
                        .Where(x => x.AccountingOtherMovementBill.Id == value.Id)
                        .ToList();

                    foreach (var expense in data)
                    {
                        expense.AccountingOtherMovementBill = null;
                    }

                    break;
                }

                case AccountingOtherDocumentType.WriteoffAct:
                {
                    var data = StockroomExpenseOrderWriteoffEntityService.GetAll()
                        .Where(x => x.AccountingOtherWriteoffAct.Id == value.Id)
                        .ToList();

                    foreach (var expense in data)
                    {
                        expense.AccountingOtherWriteoffAct = null;
                    }

                    break;
                }

                case AccountingOtherDocumentType.PostingAct:
                {
                    var data = StockroomBaseRequestEntityService.GetAll()
                        .Where(x => x.StockroomRequestType == StockroomRequestType.Posting || x.StockroomRequestType == StockroomRequestType.MakingPosting)
                        .Where(x => (x as StockroomPostingRequest).AccountingOtherPostingAct.Id == value.Id)
                        .ToList();

                    foreach (var expense in data)
                    {
                        if (expense is StockroomPostingRequest)
                        {
                            (expense as StockroomPostingRequest).AccountingOtherPostingAct = null;
                        }
                        else if (expense is StockroomMakingPostingRequest)
                        {
                            (expense as StockroomMakingPostingRequest).AccountingOtherPostingAct = null;
                        }
                    }

                    break;
                }
            }
        }
    }
}