﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using Cement.Core.Auth;
using Cement.Core.Entities;
using Cement.Core.Entities.Stockroom;
using Cement.Core.Enums;
using Cement.Core.Exceptions;
using Newtonsoft.Json;
using NHibernate.Linq;
using NHibernate.Mapping;

namespace Cement.Core.Services.EntityService.Impl
{
    public class InventoryEntityService : BaseEntityService<Inventory>
    {
        public IEntityService<InventoryStockroomProduct> InventoryStockroomProductEntityService { get; set; }
        public IEntityService<InventoryMember> InventoryMemberEntityService { get; set; }

        protected override void BeforeSave(Inventory value)
        {
            base.BeforeSave(value);
            
            value.DocumentState = DocumentState.Draft;
        }

        protected override void BeforeUpdate(Inventory value)
        {
            base.BeforeUpdate(value);

            if (value.DocumentState == 0)
            {
                value.DocumentState = DocumentState.Draft;
            }
        }

        protected override void AfterSave(Inventory value)
        {
            SaveOrUpdateMemebers(value.MemberIds, value);
            SaveOrUpdateProducts(value.StockRoomProducts, value);
            
            var route = new InventoryRoute
            {
                Employee = Container.Resolve<IUserPrincipal>().Employee,
                MovementType = DocumentMovementType.Outcoming,
                Document = value,
                DateIn = DateTime.Now
            };

            Container.Resolve<IEntityService<InventoryRoute>>().Save(route);

            base.AfterSave(value);
        }

        protected override void AfterUpdate(Inventory value)
        {
            SaveOrUpdateMemebers(value.MemberIds, value);
            SaveOrUpdateProducts(value.StockRoomProducts, value);

            base.AfterSave(value);
        }

        private void SaveOrUpdateMemebers(List<long> membersIds, Inventory inventory)
        {
            var existingMembers = InventoryMemberEntityService.GetAll()
                .Where(x => x.Inventory.Id == inventory.Id)
                .ToDictionary(x => x.Memeber.Id);

            foreach (var existingMember in existingMembers)
            {
                if (!membersIds.Contains(existingMember.Key))
                {
                    InventoryMemberEntityService.Delete(existingMember.Value.Id);
                }
            }

            foreach (var memberId in membersIds)
            {
                if (!existingMembers.Keys.Contains(memberId))
                {
                    InventoryMemberEntityService.Save(new InventoryMember { Inventory = inventory, Memeber = new Employee { Id = memberId } });
                }
            }
        }

        private void SaveOrUpdateProducts(string stockRoomProductsString, Inventory inventory)
        {
            var stockRoomProducts = JsonConvert.DeserializeObject<List<StockRoomProductProxy>>(stockRoomProductsString);
            var stockRoomProductsDict = stockRoomProducts.ToDictionary(x => x.Id, x => x.Count);

            var existingInventoryStockRoomProductsDict = InventoryStockroomProductEntityService.GetAll()
                .Where(x => x.Inventory.Id == inventory.Id)
                //.Fetch(x => x.StockroomProduct)
                .ToDictionary(x => x.StockroomProduct.Id);

            foreach (var existingInventoryStockRoomProducts in existingInventoryStockRoomProductsDict)
            {
                if (!stockRoomProductsDict.Keys.Contains(existingInventoryStockRoomProducts.Key))
                {
                    InventoryStockroomProductEntityService.Delete(existingInventoryStockRoomProducts.Value.Id);
                }
            }

            foreach (var stockRoomProduct in stockRoomProductsDict)
            {
                if (existingInventoryStockRoomProductsDict.ContainsKey(stockRoomProduct.Key))
                {
                    var existing = existingInventoryStockRoomProductsDict[stockRoomProduct.Key];
                    if (existing.Count != stockRoomProduct.Value)
                    {
                        existing.Count = stockRoomProduct.Value;

                        InventoryStockroomProductEntityService.Update(existing);
                    }
                }
                else
                {
                    InventoryStockroomProductEntityService.Save(new InventoryStockroomProduct
                    {
                        Count = stockRoomProduct.Value, 
                        Inventory = inventory,
                        StockroomProduct = new StockroomProduct { Id = stockRoomProduct.Key }
                    });
                }
            }
        }
    }

    class StockRoomProductProxy
    {
        public long Id { get; set; }

        public int Count { get; set; }
    }
}