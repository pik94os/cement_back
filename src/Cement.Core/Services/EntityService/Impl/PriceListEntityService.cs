﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using Cement.Core.Auth;
using Cement.Core.Auth.Impl;
using Cement.Core.Entities;
using Cement.Core.Enums;
using Cement.Core.Extensions;
using DataAccess.Repository;
using Newtonsoft.Json;

namespace Cement.Core.Services.EntityService.Impl
{
    public class PriceListEntityService : FileFieldEntityService<PriceList>
    {
        public IDocumentService DocumentService { get; set; }
        public IRepository<PriceListProduct> PriceListProductRepository { get; set; }

        protected override void BeforeDelete(long id)
        {
            base.BeforeDelete(id);

            var priceListProduct = PriceListProductRepository.GetAll()
                .Where(x => x.PriceList.Id == id)
                .Select(x => x.Id)
                .ToList();

            priceListProduct.ForEach(x => PriceListProductRepository.Delete(x));
        }
        
        protected override void BeforeSave(PriceList entity)
        {
            base.BeforeSave(entity);

            entity.DescriptionBytes = entity.Description.GetBytes();
            entity.Status = PriceListStatus.New;
            
            if (entity.ReplacementPriceList != null && entity.ReplacementPriceList.Id > 0)
            {
                var replacementPriceList = Repository.Load(entity.ReplacementPriceList.Id);
                
                replacementPriceList.DateEnd = entity.DateStart;
                replacementPriceList.Status = PriceListStatus.Archive;

                Repository.Update(replacementPriceList);
            }
        }

        protected override void AfterSave(PriceList entity)
        {
            var employee = Container.Resolve<IUserPrincipal>().Employee;
            
            var priceMessage = new InternalDocument
            {
                InternalDocumentType = InternalDocumentType.Message,
                Date = DateTime.Now,
                Author = entity.Author,
                Signer = entity.Signer,
                Importance = entity.Importance,
                Secrecy = entity.Secrecy,
                Creator = employee,
                Specificity = Specificity.MessageWithPriceList,
                RelatedDocumentIds = entity.RelatedDocumentIds,
                File = entity.File,
                ReviewBefore = entity.ReviewBefore,
                DocumentState = DocumentState.Draft
            };

            Container.Resolve<IEntityService<InternalDocument>>().Save(priceMessage);

            entity.MessageDocument = priceMessage;
            Repository.Update(entity);

            DocumentService.BindDocuments(entity, priceMessage);

            ProcessProducts(entity);

            base.AfterSave(entity);
        }

        protected override void AfterUpdate(PriceList entity)
        {
            ProcessProducts(entity);

            base.AfterUpdate(entity);
        }

        private void ProcessProducts(PriceList entity)
        {
            var products = new List<PriceListProductProtocol>();
            if (!string.IsNullOrEmpty(entity.ProductsJsonString))
            {
                products = JsonConvert.DeserializeObject<List<PriceListProductProtocol>>(entity.ProductsJsonString);
            }
            
            var existingProducts = PriceListProductRepository.GetAll()
                .Where(x => x.PriceList.Id == entity.Id)
                .Select(x => new { x.Id, ProductId = x.Product.Id, PriceListId = x.PriceList.Id })
                .ToList();

            if (products.Count > 0)
            {
                foreach (var existingProduct in existingProducts)
                {
                    PriceListProductRepository.Delete(existingProduct.Id);
                }

                foreach (var product in products)
                {
                    PriceListProductRepository.Save(new PriceListProduct
                    {
                        MeasureUnit = product.MeasureUnitId.HasValue ? new MeasureUnit { Id = product.MeasureUnitId.Value } : null,
                        Price = product.Price,
                        Tax = product.TaxId.HasValue ? new Tax { Id = product.TaxId.Value } : null,
                        PriceList = entity,
                        Product = new Product { Id = product.Id }
                    });
                }
            }
        }
    }

    public struct PriceListProductProtocol
    {
        public long Id { get; set; }

        public long? MeasureUnitId { get; set; }

        public decimal Price { get; set; }

        public long? TaxId { get; set; }
    }
}