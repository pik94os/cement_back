﻿using System;
using System.Linq;
using Cement.Core.Auth;
using Cement.Core.Entities;
using Cement.Core.Entities.Stockroom;
using Cement.Core.Enums;
using Cement.Core.Exceptions;

namespace Cement.Core.Services.EntityService.Impl
{
    public class StockroomExpenseOrderEntityService : BaseEntityService<StockroomExpenseOrderBase>
    {
        private object _lockObject = new object();

        public IEntityService<StockroomExpenseOrderMovement> StockroomExpenseOrderMovementEntityService { get; set; }
        public IEntityService<StockroomMovementRequest> StockroomMovementRequestEntityService { get; set; }
        public IEntityService<StockroomExpenseOrderWriteoff> StockroomExpenseOrderWriteoffEntityService { get; set; }
        public IEntityService<StockroomWriteoffRequest> StockroomWriteoffRequestEntityService { get; set; }
        public IEntityService<StockroomExpenseOrderSale> StockroomExpenseOrderSaleEntityService { get; set; }
        public IEntityService<ProductRequest> ProductRequestEntityService { get; set; }

        protected override void BeforeSave(StockroomExpenseOrderBase value)
        {
            base.BeforeSave(value);
            
            TestForRequestUsage(value);

            if (value.Warehouse == null)
            {
                throw new ValidationException("Ошибка определения склада");
            }

            value.DocumentState = DocumentState.Draft;
        }

        protected override void BeforeUpdate(StockroomExpenseOrderBase value)
        {
            base.BeforeUpdate(value);

            TestForRequestUsage(value);

            if (value.DocumentState == 0)
            {
                value.DocumentState = DocumentState.Draft;
            }
        }

        protected override void AfterSave(StockroomExpenseOrderBase value)
        {
            var route = new StockroomExpenseOrderRoute
            {
                Employee = Container.Resolve<IUserPrincipal>().Employee,
                MovementType = DocumentMovementType.Outcoming,
                Document = value,
                DateIn = DateTime.Now
            };

            Container.Resolve<IEntityService<StockroomExpenseOrderRoute>>().Save(route);

            UpdateRequestUsage(value);

            base.AfterSave(value);
        }

        protected override void AfterUpdate(StockroomExpenseOrderBase value)
        {
            UpdateRequestUsage(value);

            base.AfterUpdate(value);
        }

        public override void Save(StockroomExpenseOrderBase value)
        {
            lock (_lockObject)
            {
                base.Save(value);
            }
        }
        
        protected void TestForRequestUsage(StockroomExpenseOrderBase value)
        {
            if (value.Id > 0)
            {
                UpdateRequestUsage(value, false);
            }

            if (value is StockroomExpenseOrderMovement)
            {
                var data = StockroomMovementRequestEntityService.GetAll()
                    .Where(x => x.Id == (value as StockroomExpenseOrderMovement).StockroomMovementRequest.Id)
                    .Select(x => new { WarehouseId = x.WarehouseTo.Id, x.UsedAt})
                    .First();

                if (data.UsedAt != null)
                {
                    throw new ValidationException("Заявка уже используется в другом ордере");
                }

                value.Warehouse = new Warehouse {Id = data.WarehouseId };
            }
            else if (value is StockroomExpenseOrderWriteoff)
            {
                var data = StockroomWriteoffRequestEntityService.GetAll()
                    .Where(x => x.Id == (value as StockroomExpenseOrderWriteoff).StockroomWriteoffRequest.Id)
                    .Select(x => new { WarehouseId = x.Warehouse.Id, x.UsedAt })
                    .First();

                if (data.UsedAt != null)
                {
                    throw new ValidationException("Заявка уже используется в другом ордере");
                }

                value.Warehouse = new Warehouse { Id = data.WarehouseId };
            }
            else if (value is StockroomExpenseOrderSale)
            {
                if (ProductRequestEntityService.GetAll()
                    .Where(x => x.Id == (value as StockroomExpenseOrderSale).ProductRequest.Id)
                    .Where(x => x.UsedAt != null)
                    .Count() > 0)
                {
                    throw new ValidationException("Заявка уже используется в другом ордере");
                }
            }

        }

        protected void UpdateRequestUsage(StockroomExpenseOrderBase value, bool isUsed = true)
        {
            if (value is StockroomExpenseOrderMovement)
            {
                var stockroomMovementRequest = value.Id > 0
                    ? StockroomExpenseOrderMovementEntityService.GetAll()
                        .Where(x => x.Id == value.Id)
                        .Select(x => x.StockroomMovementRequest)
                        .First()
                    : StockroomMovementRequestEntityService.Get((value as StockroomExpenseOrderMovement).StockroomMovementRequest.Id);

                stockroomMovementRequest.UsedAt = isUsed ? DateTime.Now : (DateTime?)null;
            }
            else if (value is StockroomExpenseOrderWriteoff)
            {
                var stockroomWriteoffRequest = value.Id > 0
                    ? StockroomExpenseOrderWriteoffEntityService.GetAll()
                        .Where(x => x.Id == value.Id)
                        .Select(x => x.StockroomWriteoffRequest)
                        .First()
                    : StockroomWriteoffRequestEntityService.Get((value as StockroomExpenseOrderWriteoff).StockroomWriteoffRequest.Id);

                stockroomWriteoffRequest.UsedAt = isUsed ? DateTime.Now : (DateTime?)null;
            }
            else if (value is StockroomExpenseOrderSale)
            {
                var productRequest = value.Id > 0
                    ? StockroomExpenseOrderSaleEntityService.GetAll()
                        .Where(x => x.Id == value.Id)
                        .Select(x => x.ProductRequest)
                        .First()
                    : ProductRequestEntityService.Get((value as StockroomExpenseOrderSale).ProductRequest.Id);

                productRequest.UsedAt = isUsed ? DateTime.Now : (DateTime?)null;
            }

        }
    }
}