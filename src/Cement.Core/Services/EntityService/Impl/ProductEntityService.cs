﻿using System.Collections.Generic;
using System.Linq;
using Cement.Core.Entities;
using Cement.Core.Enums;
using Cement.Core.Extensions;
using DataAccess.Repository;
using Newtonsoft.Json;

namespace Cement.Core.Services.EntityService.Impl
{
    public class ProductEntityService : FileFieldEntityService<Product>
    {
        public IRepository<ProductComplect> ProductComplectRepository { get; set; }

        protected override void BeforeDelete(long id)
        {
            base.BeforeDelete(id);

            var productComplect = ProductComplectRepository.GetAll()
                .Where(x => x.Product.Id == id)
                .Select(x => x.Id)
                .ToList();

            productComplect.ForEach(x => ProductComplectRepository.Delete(x));
        }

        protected override void BeforeSave(Product entity)
        {
            base.BeforeSave(entity);

            if (entity.Type == ProductType.Service || entity.PackageJsonString == null)
            {
                return;
            }

            entity.PackageJsonBytes = entity.PackageJsonString.GetBytes();
            var productPackageProxy = JsonConvert.DeserializeObject<ProductPackageProxy>(entity.PackageJsonString);

            if (productPackageProxy != null)
            {
                entity.Kind = productPackageProxy.PackingKind;
                entity.TypeSingle = productPackageProxy.PackingTypeSingleId.HasValue && productPackageProxy.PackingTypeSingleId.Value > 0 ? new PackingType { Id = productPackageProxy.PackingTypeSingleId.Value } : null;
                entity.MaterialSingle = productPackageProxy.PackingMaterialSingleId.HasValue && productPackageProxy.PackingMaterialSingleId.Value > 0 ? new PackingMaterial { Id = productPackageProxy.PackingMaterialSingleId.Value } : null;
                entity.VolumeSingle = productPackageProxy.VolumeSingle;
                entity.VolumeUnitSingle = productPackageProxy.VolumeUnitSingleId.HasValue && productPackageProxy.VolumeUnitSingleId.Value > 0 ? new MeasureUnit { Id = productPackageProxy.VolumeUnitSingleId.Value } : null;
                entity.WeightSingle = productPackageProxy.WeightSingle;
                entity.WeightUnitSingle = productPackageProxy.WeightUnitSingleId.HasValue && productPackageProxy.WeightUnitSingleId.Value > 0 ? new MeasureUnit { Id = productPackageProxy.WeightUnitSingleId.Value } : null;
                entity.LengthSingle = productPackageProxy.LengthSingle;
                entity.WidthSingle = productPackageProxy.WidthSingle;
                entity.HeightSingle = productPackageProxy.HeightSingle;
                entity.SizeUnitSingle = productPackageProxy.SizeUnitSingleId.HasValue && productPackageProxy.SizeUnitSingleId.Value > 0 ? new MeasureUnit { Id = productPackageProxy.SizeUnitSingleId.Value } : null;
                entity.UnitsCountGroup = productPackageProxy.UnitsCountGroup;
                entity.TypeGroup = productPackageProxy.PackingTypeGroupId.HasValue && productPackageProxy.PackingTypeGroupId.Value > 0 ? new PackingType { Id = productPackageProxy.PackingTypeGroupId.Value } : null;
                entity.MaterialGroup = productPackageProxy.PackingMaterialGroupId.HasValue && productPackageProxy.PackingMaterialGroupId.Value > 0 ? new PackingMaterial { Id = productPackageProxy.PackingMaterialGroupId.Value } : null;
                entity.VolumeGroup = productPackageProxy.VolumeGroup;
                entity.VolumeUnitGroup = productPackageProxy.VolumeUnitGroupId.HasValue && productPackageProxy.VolumeUnitGroupId.Value > 0 ? new MeasureUnit { Id = productPackageProxy.VolumeUnitGroupId.Value } : null;
                entity.WeightGroup = productPackageProxy.WeightGroup;
                entity.WeightUnitGroup = productPackageProxy.WeightUnitGroupId.HasValue && productPackageProxy.WeightUnitGroupId.Value > 0 ? new MeasureUnit { Id = productPackageProxy.WeightUnitGroupId.Value } : null;
                entity.LengthGroup = productPackageProxy.LengthGroup;
                entity.WidthGroup = productPackageProxy.WidthGroup;
                entity.HeightGroup = productPackageProxy.HeightGroup;
                entity.SizeUnitGroup = productPackageProxy.SizeUnitGroupId.HasValue && productPackageProxy.SizeUnitGroupId.Value > 0 ? new MeasureUnit { Id = productPackageProxy.SizeUnitGroupId.Value } : null;
                entity.UnitsCountTransport = productPackageProxy.UnitsCountTransport;
                entity.TypeTransport = productPackageProxy.PackingTypeTransportId.HasValue ? new PackingType { Id = productPackageProxy.PackingTypeTransportId.Value } : null;
                entity.MaterialTransport = productPackageProxy.PackingMaterialTransportId.HasValue ? new PackingMaterial { Id = productPackageProxy.PackingMaterialTransportId.Value } : null;
                entity.VolumeTransport = productPackageProxy.VolumeTransport;
                entity.VolumeUnitTransport = productPackageProxy.VolumeUnitTransportId.HasValue && productPackageProxy.VolumeUnitTransportId.Value > 0 ? new MeasureUnit { Id = productPackageProxy.VolumeUnitTransportId.Value } : null;
                entity.WeightTransport = productPackageProxy.WeightTransport;
                entity.WeightUnitTransport = productPackageProxy.WeightUnitTransportId.HasValue && productPackageProxy.WeightUnitTransportId.Value > 0 ? new MeasureUnit { Id = productPackageProxy.WeightUnitTransportId.Value } : null;
                entity.LengthTransport = productPackageProxy.LengthTransport;
                entity.WidthTransport = productPackageProxy.WidthTransport;
                entity.HeightTransport = productPackageProxy.HeightTransport;
                entity.SizeUnitTransport = productPackageProxy.SizeUnitTransportId.HasValue && productPackageProxy.SizeUnitTransportId.Value > 0 ? new MeasureUnit { Id = productPackageProxy.SizeUnitTransportId.Value } : null;
            }
            
        }

        protected override void BeforeUpdate(Product value)
        {
            base.BeforeUpdate(value);
        }

        protected override void AfterSave(Product product)
        {
            ProcessComplectProducts(product);

            base.AfterSave(product);
        }

        protected override void AfterUpdate(Product product)
        {
            ProcessComplectProducts(product);

            base.AfterUpdate(product);
        }

        private void ProcessComplectProducts(Product product)
        {
            var existingComplectProducts = ProductComplectRepository.GetAll()
                .Where(x => x.Product.Id == product.Id)
                .Select(x => new { x.Id, ProductId = x.Product.Id, ComplectProductId = x.ComplectProduct.Id })
                .ToList();

            var requestProductComplectIds = (product.ComplectProductsIds ?? new List<long>());

            existingComplectProducts.ForEach(x =>
                {
                    if (!requestProductComplectIds.Contains(x.ComplectProductId))
                    {
                        ProductComplectRepository.Delete(x.Id);
                    }
                });

            requestProductComplectIds.ForEach(x =>
                {
                    if (!existingComplectProducts.Any(y => y.ComplectProductId == x))
                    {
                        ProductComplectRepository.Save(new ProductComplect
                        {
                            Product = product,
                            ComplectProduct = new Product { Id = x }
                        });
                    }
                });
        }
        
    }
}