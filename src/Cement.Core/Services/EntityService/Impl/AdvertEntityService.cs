﻿using System.Collections.Generic;
using System.Linq;
using Castle.Core.Internal;
using Cement.Core.Auth;
using Cement.Core.Entities;
using Cement.Core.Enums;
using Cement.Core.Extensions;

namespace Cement.Core.Services.EntityService.Impl
{
    public class AdvertEntityService : DocumentEntityService<Advert>
    {
        public IEntityService<AdvertRecipient> AdvertRecipientEntityService { get; set; }

        protected override void BeforeUpdate(Advert value)
        {
            base.BeforeUpdate(value);

            Process(value);
        }

        protected override void BeforeSave(Advert value)
        {
            base.BeforeSave(value);

            Process(value);
        }

        protected override void AfterSave(Advert value)
        {
            //DocumentService.AddDocumentBinds(value, value.RelatedDocumentIds);

            if (value.RecipientIds != null)
            {
                value.RecipientIds.ForEach(x =>
                {
                    if (x > 0)
                    {
                        AdvertRecipientEntityService.Save(new AdvertRecipient
                        {
                            Advert = value,
                            Recipient = new Employee { Id = x }
                        });
                    }
                });
            }

            base.AfterSave(value);
        }

        protected override void AfterUpdate(Advert value)
        {
            var existing = AdvertRecipientEntityService.GetAll()
                .Where(x => x.Advert.Id == value.Id)
                .Select(x => new
                {
                    x.Id,
                    EmployeeId = x.Recipient.Id
                })
                .ToList();

            var currentRecipientList = value.RecipientIds ?? new List<long>();

            var listToDel = existing
                .Where(x => !currentRecipientList.Contains(x.EmployeeId))
                .Select(x => x.Id)
                .ToList();

            listToDel.ForEach(x => AdvertRecipientEntityService.Delete(x));


            currentRecipientList.Where(x => !existing.Any(y => y.EmployeeId == x))
                .ForEach(x =>
                {
                    if (x > 0)
                    {
                        AdvertRecipientEntityService.Save(new AdvertRecipient
                        {
                            Advert = value,
                            Recipient = new Employee { Id = x }
                        });
                    }
                });

            base.AfterUpdate(value);
        }

        private void Process(Advert advert)
        {
            var dateStr = advert.Date.HasValue
                ? " для показа " + advert.Date.Value.ToString("dd.MM.yyyy")
                : string.Empty;

            var docType = DocumentType.Advert.GetDisplayValue();

            advert.Name = (docType + dateStr).Trim();

            advert.DocumentState = DocumentState.Draft;
            advert.Content = advert.ContentString.GetBytes();
            advert.Creator = Container.Resolve<IUserPrincipal>().Employee;
        }

        protected override void BeforeDelete(long id)
        {
            base.BeforeDelete(id);

            // Сразу вытягиваем объектами, иначе хибер для каждого объекта будет по-отдельности делать запрос к БД
            var existing = AdvertRecipientEntityService.GetAll()
                .Where(x => x.Advert.Id == id)
                .ToList();

            existing.ForEach(x => AdvertRecipientEntityService.Delete(x.Id));
        }
    }
}