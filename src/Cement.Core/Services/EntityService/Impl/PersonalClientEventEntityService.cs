﻿using System;
using System.Linq;
using Castle.Core.Internal;
using Cement.Core.Entities;
using Cement.Core.Extensions;

namespace Cement.Core.Services.EntityService.Impl
{
    public class PersonalClientEventEntityService : BaseEntityService<PersonalClientEvent>
    {
        public IEntityService<PersonalClientEventComment> PersonalClientEventCommentEntityService { get; set; }
        public IEntityService<DocumentRelation> DocumentRelationEntityService { get; set; }
        public IDocumentService DocumentService { get; set; }
        
        protected override void BeforeDelete(long id)
        {
            base.BeforeDelete(id);

            PersonalClientEventCommentEntityService.GetAll()
                .Where(x => x.PersonalClientEvent.Id == id)
                .ForEach(x => PersonalClientEventCommentEntityService.Delete(x.Id));

            var relatedDocuments = DocumentRelationEntityService.GetAll()
                .Where(x => x.BaseDocumentId == id)
                .ToList();

            relatedDocuments.ForEach(x => DocumentRelationEntityService.Delete(x.Id));
        }

        protected override void BeforeUpdate(PersonalClientEvent entity)
        {
            base.BeforeUpdate(entity);

            ProcessDateTime(entity);
        }

        protected override void BeforeSave(PersonalClientEvent entity)
        {
            base.BeforeSave(entity);

            ProcessDateTime(entity);
        }

        private void ProcessDateTime(PersonalClientEvent entity)
        {
            if (entity.EventDateTime == null)
            {
                return;
            }

            var hours = 0;
            var minutes = 0;

            if (!string.IsNullOrWhiteSpace(entity.TimeString))
            {
                var parts = entity.TimeString.Split(new[] {':'}, StringSplitOptions.RemoveEmptyEntries);

                if (parts.Length == 2)
                {
                    hours = parts.First().ToInt();
                    minutes = parts.Last().ToInt();
                }

                hours = hours > 23 ? 0 : hours;
                minutes = minutes > 59 ? 0 : minutes;
            }

            var dt = entity.EventDateTime.Value;

            entity.EventDateTime = new DateTime(dt.Year, dt.Month, dt.Day, hours, minutes, 0);
        }

        protected override void AfterSave(PersonalClientEvent value)
        {
            DocumentService.AddRelatedDocumentsToEntity(value.Id, value.RelatedDocumentIds);

            base.AfterSave(value);
        }
    }
}