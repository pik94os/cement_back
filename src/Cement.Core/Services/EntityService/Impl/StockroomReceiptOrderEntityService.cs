﻿using System;
using System.Linq;
using Cement.Core.Auth;
using Cement.Core.Entities;
using Cement.Core.Entities.Stockroom;
using Cement.Core.Enums;
using Cement.Core.Exceptions;

namespace Cement.Core.Services.EntityService.Impl
{
    public class StockroomReceiptOrderEntityService : BaseEntityService<StockroomReceiptOrderBase>
    {
        private static object _lockObject = new object();

        public IEntityService<StockroomReceiptOrderRoute> StockroomReceiptOrderRouteEntityService { get; set; }
        public IEntityService<StockroomReceiptOrderItemBase> StockroomReceiptOrderItemBaseEntityService { get; set; }
        public IEntityService<StockroomReceiptOrderItemOuter> StockroomReceiptOrderItemOuterEntityService { get; set; }
        public IEntityService<StockroomReceiptOrderItemInnerMove> StockroomReceiptOrderItemInnerMoveEntityService { get; set; }
        public IEntityService<StockroomReceiptOrderItemInnerPosting> StockroomReceiptOrderItemInnerPostingEntityService { get; set; }
        public IEntityService<StockroomReceiptOrderBuy> StockroomReceiptOrderBuyEntityService { get; set; }
        public IEntityService<StockroomReceiptOrderMovement> StockroomReceiptOrderMovementEntityService { get; set; }
        public IEntityService<StockroomReceiptOrderPosting> StockroomReceiptOrderPostingEntityService { get; set; }

        protected override void BeforeSave(StockroomReceiptOrderBase value)
        {
            base.BeforeSave(value);
            
            TestForRequestUsage(value);

            if (value.Warehouse == null)
            {
                throw new ValidationException("Ошибка определения склада");
            }

            value.DocumentState = DocumentState.Draft;
        }

        protected override void BeforeUpdate(StockroomReceiptOrderBase value)
        {
            base.BeforeUpdate(value);

            TestForRequestUsage(value);

            if (value.DocumentState == 0)
            {
                value.DocumentState = DocumentState.Draft;
            }
        }

        protected override void AfterSave(StockroomReceiptOrderBase value)
        {
            var route = new StockroomReceiptOrderRoute
            {
                Employee = Container.Resolve<IUserPrincipal>().Employee,
                MovementType = DocumentMovementType.Outcoming,
                Document = value,
                DateIn = DateTime.Now
            };

            StockroomReceiptOrderRouteEntityService.Save(route);

            UpdateRequestUsage(value);

            base.AfterSave(value);
        }

        protected override void AfterUpdate(StockroomReceiptOrderBase value)
        {
            UpdateRequestUsage(value);

            base.AfterUpdate(value);
        }

        public override void Save(StockroomReceiptOrderBase value)
        {
            lock (_lockObject)
            {
                base.Save(value);
            }
        }

        protected void TestForRequestUsage(StockroomReceiptOrderBase value)
        {
            if (value.Id > 0)
            {
                UpdateRequestUsage(value, false);
            }

            var stockroomReceiptOrderItemId = 0L;

            if (value is StockroomReceiptOrderBuy)
            {
                stockroomReceiptOrderItemId = (value as StockroomReceiptOrderBuy).StockroomReceiptOrderItemOuter.Id;
            }
            else if (value is StockroomReceiptOrderMovement)
            {
                stockroomReceiptOrderItemId = (value as StockroomReceiptOrderMovement).StockroomReceiptOrderItemMove.Id;
            }
            else if (value is StockroomReceiptOrderPosting)
            {
                stockroomReceiptOrderItemId = (value as StockroomReceiptOrderPosting).StockroomReceiptOrderItemPosting.Id;
            }

            var data = StockroomReceiptOrderItemBaseEntityService.GetAll()
                .Where(x => x.Id == stockroomReceiptOrderItemId)
                .Select(x => new { WarehouseId = x.Warehouse.Id, x.UsedAt })
                .First();

            if (data.UsedAt != null)
            {
                throw new ValidationException("Документ уже используется в другом ордере");
            }

            value.Warehouse = new Warehouse { Id = data.WarehouseId };
        }

        protected void UpdateRequestUsage(StockroomReceiptOrderBase value, bool isUsed = true)
        {
            if (value is StockroomReceiptOrderBuy)
            {
                var stockroomReceiptOrderItem = value.Id > 0
                    ? StockroomReceiptOrderBuyEntityService.GetAll()
                        .Where(x => x.Id == value.Id)
                        .Select(x => x.StockroomReceiptOrderItemOuter)
                        .First()
                    : StockroomReceiptOrderItemOuterEntityService.Get((value as StockroomReceiptOrderBuy).StockroomReceiptOrderItemOuter.Id);

                stockroomReceiptOrderItem.UsedAt = isUsed ? DateTime.Now : (DateTime?)null;
            }
            else if (value is StockroomReceiptOrderMovement)
            {
                var stockroomReceiptOrderItem = value.Id > 0
                    ? StockroomReceiptOrderMovementEntityService.GetAll()
                        .Where(x => x.Id == value.Id)
                        .Select(x => x.StockroomReceiptOrderItemMove)
                        .First()
                    : StockroomReceiptOrderItemInnerMoveEntityService.Get((value as StockroomReceiptOrderMovement).StockroomReceiptOrderItemMove.Id);

                stockroomReceiptOrderItem.UsedAt = isUsed ? DateTime.Now : (DateTime?)null;
            }
            else if (value is StockroomReceiptOrderPosting)
            {
                var stockroomReceiptOrderItem = value.Id > 0
                    ? StockroomReceiptOrderPostingEntityService.GetAll()
                        .Where(x => x.Id == value.Id)
                        .Select(x => x.StockroomReceiptOrderItemPosting)
                        .First()
                    : StockroomReceiptOrderItemInnerPostingEntityService.Get((value as StockroomReceiptOrderPosting).StockroomReceiptOrderItemPosting.Id);

                stockroomReceiptOrderItem.UsedAt = isUsed ? DateTime.Now : (DateTime?)null;
            }
        }
    }
}