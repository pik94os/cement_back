﻿using System;
using System.Linq;
using Cement.Core.Auth;
using Cement.Core.Entities;
using Cement.Core.Entities.Stockroom;
using Cement.Core.Enums;
using Cement.Core.Exceptions;
using NHibernate.Util;

namespace Cement.Core.Services.EntityService.Impl
{
    public class AccountingInOutDocEntityService : BaseEntityService<AccountingInOutDocBase>
    {
        private static object _lockObject = new object();

        public IEntityService<StockroomExpenseOrderSale> StockroomExpenseOrderSaleEntityService { get; set; }
        public IEntityService<AccountingInOutDocInvoice> AccountingInOutDocInvoiceEntityService { get; set; }
        public IEntityService<AccountingInOutDocWayBillSale> AccountingInOutDocWayBillSaleEntityService { get; set; }
        public IEntityService<AccountingInOutDocRoute> AccountingInOutDocRouteEntityService { get; set; }
        
        protected override void BeforeSave(AccountingInOutDocBase value)
        {
            base.BeforeSave(value);

            TestForDocUsage(value);

            value.OrganizationFrom = Container.Resolve<IUserPrincipal>().Organization;
            
            value.DocumentState = DocumentState.Draft;
        }

        protected override void BeforeUpdate(AccountingInOutDocBase value)
        {
            base.BeforeUpdate(value);

            TestForDocUsage(value);

            if (value.DocumentState == 0)
            {
                value.DocumentState = DocumentState.Draft;
            }
        }

        protected override void AfterSave(AccountingInOutDocBase value)
        {
            var route = new AccountingInOutDocRoute
            {
                Employee = Container.Resolve<IUserPrincipal>().Employee,
                MovementType = DocumentMovementType.Outcoming,
                Document = value,
                DateIn = DateTime.Now
            };

            Container.Resolve<IEntityService<AccountingInOutDocRoute>>().Save(route);

            ProcessRelatedDocs(value);

            base.AfterSave(value);
        }

        protected override void AfterUpdate(AccountingInOutDocBase value)
        {
            ProcessRelatedDocs(value);

            base.AfterUpdate(value);
        }

        public override void Save(AccountingInOutDocBase value)
        {
            lock (_lockObject)
            {
                base.Save(value);
            }
        }

        protected void ProcessRelatedDocs(AccountingInOutDocBase value)
        {
            if (value is AccountingInOutDocWayBill)
            {
                var accountingInOutDocWayBill = value as AccountingInOutDocWayBill;

                var data = StockroomExpenseOrderSaleEntityService.GetAll()
                    .Where(x => value.DstItems.Contains(x.Id) || x.AccountingInOutDocWayBill.Id == value.Id)
                    .ToList();

                if (!data.All(x => x.AccountingInOutDocWayBill == null || x.AccountingInOutDocWayBill.Id == value.Id))
                {
                    throw new ValidationException("Ордер используется в другой накладной");
                }

                foreach (var expense in data)
                {
                    expense.AccountingInOutDocWayBill = value.DstItems.Contains(expense.Id) ? accountingInOutDocWayBill : null;
                }
            }
        }

        protected void TestForDocUsage(AccountingInOutDocBase value)
        {
            if (value is AccountingInOutDocInvoice)
            {
                var accountingInOutDocInvoice = value as AccountingInOutDocInvoice;

                RelatedDocSetUnused(value.Id);

                var data = AccountingInOutDocWayBillSaleEntityService.Get(accountingInOutDocInvoice.DstItems.First());

                if (data.UsedAt != null)
                {
                    throw new ValidationException("Накладная уже используется в другом счете-фактуре");
                }

                value.OrganizationTo = data.OrganizationTo;
                value.Sum = data.Sum;
                (value as AccountingInOutDocInvoice).AccountingInOutDocWayBill = data;
                data.UsedAt = DateTime.Now;
            }
            else
            {
                var data = StockroomExpenseOrderSaleEntityService.GetAll()
                    .Where(x => value.DstItems.Contains(x.Id))
                    .Select(x =>  new
                    {
                        payerId = (long?)x.ProductRequest.Payer.Id, 
                        count = x.Amount,
                        price = x.ProductRequest.ProductPrice
                    })
                    .ToList();

                if (data.Count == 0 || data.First().payerId == null)
                {
                    throw new ValidationException("Не удалось определить контрагента"); 
                }

                value.OrganizationTo = new Organization { Id = data.First().payerId.Value };
                value.Sum = data.Sum(x => x.count * x.price) ?? 0;
            }
        }

        protected void RelatedDocSetUnused(long id)
        {
            if (id <= 0)
            {
                return;
            }
            
            var accountingInOutDoc = AccountingInOutDocInvoiceEntityService.GetAll()
                .Where(x => x.Id == id)
                .Select(x => x.AccountingInOutDocWayBill as AccountingInOutDocWayBillSale)
                .First();

            accountingInOutDoc.UsedAt = null;
        }

        protected override void BeforeDelete(long id)
        {
            /// Бухгалтерские документы в общем не удаляются, но можно удалить некорректно созданный, т.е. тот, который в новых

            base.BeforeDelete(id);

            AccountingInOutDocRouteEntityService.GetAll().Where(x => x.Document.Id == id)
                .ForEach(x => AccountingInOutDocRouteEntityService.Delete(x.Id));

            var entity = Repository.Load(id);

            ReleaseRelatedDocs(entity);
        }

        protected void ReleaseRelatedDocs(AccountingInOutDocBase value)
        {
            // тут намеренно используется switch по enum, т.к. не получается использовать оператор is
            // потому что NHibernate из базы laod-ит proxy-объекты

            switch (value.AccountingInOutDocType)
            {
                case AccountingInOutDocType.WorkCompleteAct:
                case AccountingInOutDocType.WayBillSale:
                    {
                        var data = StockroomExpenseOrderSaleEntityService.GetAll()
                            .Where(x => x.AccountingInOutDocWayBill.Id == value.Id)
                            .ToList();

                        foreach (var expense in data)
                        {
                            expense.AccountingInOutDocWayBill = null;
                        }

                        break;
                    }

                case AccountingInOutDocType.Invoice:
                    {
                        RelatedDocSetUnused(value.Id);

                        break;
                    }
            }
        }
    }
}