﻿using System.Collections.Generic;
using System.Linq;
using Cement.Core.Auth;
using Cement.Core.Entities;
using Cement.Core.Enums;
using Cement.Core.Extensions;
using NHibernate.Util;

namespace Cement.Core.Services.EntityService.Impl
{
    public class InternalDocumentEntityService : DocumentEntityService<InternalDocument>
    {
        public IEntityService<DisposalRecipient> DisposalRecipientEntityService { get; set; }

        protected override void BeforeUpdate(InternalDocument value)
        {
            base.BeforeUpdate(value);

            ProcessName(value);

            if (value.DocumentState == 0)
            {
                value.DocumentState = DocumentState.Draft;
            }
            value.Content = value.ContentString.GetBytes();
            value.Creator = Container.Resolve<IUserPrincipal>().Employee;
        }

        protected override void BeforeSave(InternalDocument value)
        {
            base.BeforeSave(value);

            ProcessName(value);

            value.DocumentState = DocumentState.Draft;
            value.Content = value.ContentString.GetBytes();
            value.Creator = Container.Resolve<IUserPrincipal>().Employee;
        }

        protected override void AfterSave(InternalDocument value)
        {
            DocumentService.AddDocumentBinds(value, value.RelatedDocumentIds);

            if (value.InternalDocumentType == InternalDocumentType.Disposal && value.RecipientIds != null)
            {
                value.RecipientIds.ForEach(x =>
                {
                    if (x > 0)
                    {
                        DisposalRecipientEntityService.Save(new DisposalRecipient
                        {
                            Disposal = value,
                            Recipient = new Employee { Id = x }
                        });
                    }
                });
            }

            base.AfterSave(value);
        }

        protected override void AfterUpdate(InternalDocument value)
        {
            if (value.InternalDocumentType == InternalDocumentType.Disposal)
            {
                var existing = DisposalRecipientEntityService.GetAll()
                .Where(x => x.Disposal.Id == value.Id)
                .Select(x => new
                {
                    x.Id,
                    EmployeeId = x.Recipient.Id
                })
                .ToList();

                var currentRecipientList = value.RecipientIds ?? new List<long>();

                var listToDel = existing
                    .Where(x => !currentRecipientList.Contains(x.EmployeeId))
                    .Select(x => x.Id)
                    .ToList();

                listToDel.ForEach(x => DisposalRecipientEntityService.Delete(x));


                currentRecipientList.Where(x => !existing.Any(y => y.EmployeeId == x))
                    .ForEach(x =>
                    {
                        if (x > 0)
                        {
                            DisposalRecipientEntityService.Save(new DisposalRecipient
                            {
                                Disposal = value,
                                Recipient = new Employee { Id = x }
                            });
                        }
                    });
            }
            
            base.AfterUpdate(value);
        }
        
        private void ProcessName(InternalDocument internalDocument)
        {
            var dateStr = internalDocument.Date.HasValue
                ? " от "+ internalDocument.Date.Value.ToString("dd.MM.yyyy")
                : string.Empty;

            var docType = internalDocument.InternalDocumentType.GetDisplayValue();

            internalDocument.Name = string.Format("{0} №{1}{2}", docType, internalDocument.No, dateStr).Trim();
        }
    }
}