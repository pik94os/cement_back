﻿using System.Linq;
using Cement.Core.Entities;
using Cement.Core.Extensions;
using DataAccess.Repository;

namespace Cement.Core.Services.EntityService.Impl
{
    public class VehicleEntityService : FileFieldEntityService<Vehicle>
    {
        protected override void BeforeUpdate(Vehicle value)
        {
            base.BeforeUpdate(value);

            SetName(value);
        }

        protected override void BeforeSave(Vehicle value)
        {
            base.BeforeSave(value);

            SetName(value);
        }

        private void SetName(Vehicle value)
        {
            var modificationName = string.Empty;
            var modelName = string.Empty;
            var brandName = string.Empty;
            var numRegionCode = string.Empty;
            var numCountryCode = string.Empty;

            if (value.Modification != null)
            {
                modificationName = Container.Resolve<IRepository<VehicleModification>>().GetAll()
                    .Where(x => x.Id == value.Modification.Id)
                    .Select(x => x.Name)
                    .FirstOrDefault();
            }

            if (value.Model != null)
            {
                var modelBrand = Container.Resolve<IRepository<VehicleModel>>().GetAll()
                    .Where(x => x.Id == value.Model.Id)
                    .Select(x => new {x.Name, brandName = x.Brand.Name })
                    .FirstOrDefault();

                if (modelBrand != null)
                {
                    brandName = modelBrand.brandName;
                    modelName = modelBrand.Name;
                }
            }

            if (value.NumCountryCode != null)
            {
                numCountryCode = Container.Resolve<IRepository<CountryCode>>().GetAll()
                    .Where(x => x.Id == value.NumCountryCode.Id)
                    .Select(x => x.Name)
                    .FirstOrDefault();
            }

            if (value.NumRegionCode != null)
            {
                numRegionCode = Container.Resolve<IRepository<RegionCode>>().GetAll()
                    .Where(x => x.Id == value.NumRegionCode.Id)
                    .Select(x => x.Name)
                    .FirstOrDefault();
            }

            value.NumberText = string.Format("{0} {1}{2}", value.Number, numRegionCode, numCountryCode).Trim();

            value.ModelFullName = string.Format("{0} {1} {2}", brandName, modelName, modificationName.SafeToString().Replace(modelName, string.Empty)).Trim();

            value.Name = string.Format("{0} {1}", value.ModelFullName, value.NumberText).Trim();

            value.SizeText =  string.Format("{0}x{1}x{2}", value.Length, value.Width, value.Height).Trim();

            value.GoText = string.Format("{0}/{1}", value.Capacity, value.Volume).Trim();
        }
    }
}