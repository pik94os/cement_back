﻿using System;
using System.Linq;
using System.Reflection;
using Cement.Core.Entities;
using DataAccess;

namespace Cement.Core.Services.EntityService.Impl
{
    public class FileFieldEntityService<T> : BaseEntityService<T> where T : class, IEntity 
    {
        private readonly Type _fileInfoType = typeof(EntityFile);

        public IEntityFileService EntityFileService { get; set; }

        private PropertyInfo[] _fileProperties;

        /// <summary>
        /// Свойства сущности с типом EntityFile
        /// </summary>
        protected PropertyInfo[] FileProperties
        {
            get
            {
                return _fileProperties ??
                    (_fileProperties = typeof(T)
                        .GetProperties()
                        .Where(x => x.PropertyType == _fileInfoType)
                        .ToArray());
            }
        }

        protected override void SaveInternal(T value)
        {
            InTransaction(() =>
            {
                BeforeSave(value);

                foreach (var fileProperty in FileProperties)
                {
                    ProcessEntityFile(value, fileProperty);
                }
                Repository.Save(value);

                AfterSave(value);
            });
        }

        protected override void UpdateInternal(T value)
        {
            InTransaction(() =>
            {
                BeforeUpdate(value);

                // Перед обновлением объекта проверяем наличие файлов
                // Если есть сохраненные файлы, а от клиента новых нет, то перепривязываем старые файлы
                var savedData = Repository.Load(value.Id);

                foreach (var fileProperty in FileProperties)
                {
                    ProcessEntityFile(value, fileProperty, savedData);
                }

                Repository.Evict(savedData);
                Repository.Update(value);

                AfterUpdate(value);
            });
        }
        
        protected void ProcessEntityFile(T value, PropertyInfo propertyInfo, T savedValue = null)
        {
            var data = propertyInfo.GetValue(value);

            var existingValue = savedValue != null ? propertyInfo.GetValue(savedValue) as IEntity : null;

            if (!(data is EntityFileWithData))
            {
                propertyInfo.SetValue(value, existingValue != null ? new EntityFile {Id = existingValue.Id} : null);

                return;
            }

            var entityFileWithData = data as EntityFileWithData;
            EntityFile entityFile = null;

            if (entityFileWithData.Data != null)
            {
                entityFile = EntityFileService.SaveEntityFile(entityFileWithData);
            }
            else if (entityFileWithData.Id > 0)
            {
                // Значит файл не изменен
                entityFile = new EntityFile { Id = entityFileWithData.Id };
            }

            propertyInfo.SetValue(value, entityFile);
        }
    }
}