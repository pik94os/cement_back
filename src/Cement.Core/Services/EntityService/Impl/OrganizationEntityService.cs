﻿using Cement.Core.Entities;

namespace Cement.Core.Services.EntityService.Impl
{
    public class OrganizationEntityService : FileFieldEntityService<Organization>
    {
        protected override void BeforeSave(Organization value)
        {
            base.BeforeSave(value);

            value.Id = value.Client.Id;
        }
    }
}