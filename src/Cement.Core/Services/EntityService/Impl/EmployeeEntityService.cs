﻿using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using Cement.Core.Auth;
using Cement.Core.Entities;
using Cement.Core.Exceptions;
using Remotion.Linq.Parsing;

namespace Cement.Core.Services.EntityService.Impl
{
    public class EmployeeEntityService : FileFieldEntityService<Employee>
    {
        public IEntityService<EmployeeDrivingCategory> EmployeeDrivingCategoryEntityService { get; set; }
        public IPasswordHashProvider PasswordHashProvider { get; set; }
        public IPasswordGenerator PasswordGenerator { get; set; }
        public IEntityService<User> UserEntityService { get; set; }

        protected override void BeforeDelete(long id)
        {
            base.BeforeDelete(id);

            var employeeDriveCategories = EmployeeDrivingCategoryEntityService.GetAll()
                .Where(x => x.Employee.Id == id)
                .Select(x => x.Id)
                .ToList();

            employeeDriveCategories.ForEach(x => EmployeeDrivingCategoryEntityService.Delete(x));
        }

        protected override void BeforeSave(Employee value)
        {
            base.BeforeSave(value);

            CheckEmail(value.Email1);

            ProcessFullName(value);
        }

        protected override void BeforeUpdate(Employee value)
        {
            base.BeforeUpdate(value);

            CheckEmail(value.Email1);

            ProcessFullName(value);
        }

        private void ProcessFullName(Employee employee)
        {
            employee.Name = string.Format("{0} {1} {2}", employee.Surname, employee.FirstName, employee.Patronymic).Trim();

            if (employee.DriveLicDate.HasValue)
            {
                employee.DriveLicValidDate = employee.DriveLicDate.Value.AddYears(10);
            }

            if (employee.TractorDriveLicDate.HasValue)
            {
                employee.TractorDriveLicValidDate = employee.TractorDriveLicDate.Value.AddYears(10);
            }
        }

        protected override void AfterSave(Employee employee)
        {
            ProcessEmployeeDrivingCategories(employee);

            var user = new User
            {
                Name = employee.Name,
                Email = employee.Email1,
                Login = employee.Email1,
                IsBlocked = false,
                Employee = employee,
                Organization = employee.Organization
            };

            user.SetNewPassword(PasswordHashProvider, PasswordGenerator.Generate());
            UserEntityService.Save(user);

            base.AfterSave(employee);
        }

        protected override void AfterUpdate(Employee employee)
        {
            ProcessEmployeeDrivingCategories(employee);

            base.AfterUpdate(employee);
        }

        private void ProcessEmployeeDrivingCategories(Employee employee)
        {
            var existingDriveCategories = EmployeeDrivingCategoryEntityService.GetAll()
                .Where(x => x.Employee.Id == employee.Id)
                .Select(x => new {x.Id, categoryId = x.Category.Id, type = x.Category.Type })
                .ToList();

            var categories = (employee.DriveCategories ?? new List<long>())
                .Concat(employee.TractorCategories ?? new List<long>())
                .ToList();
            
            existingDriveCategories.ForEach(x =>
                {
                    if (!categories.Contains(x.categoryId))
                    {
                        EmployeeDrivingCategoryEntityService.Delete(x.Id);
                    }
                });

            categories.ForEach(x =>
                {
                    if (!existingDriveCategories.Any(y => y.categoryId == x))
                    {
                        EmployeeDrivingCategoryEntityService.Save(new EmployeeDrivingCategory
                        {
                            Employee = employee,
                            Category = new DriverCategory { Id = x }
                        });
                    }
                });
        }

        private void CheckEmail(string email)
        {
            var emailRegex = new Regex(@"^(?!\.)(""([^""\r\\]|\\[""\r\\])*""|"
                + @"([-a-z0-9!#$%&'*+/=?^_`{|}~]|(?<!\.)\.)*)(?<!\.)"
                + @"@[a-z0-9][\w\.-]*[a-z0-9]\.[a-z][a-z\.]*[a-z]$",
                RegexOptions.IgnoreCase);

            if (!emailRegex.IsMatch(email))
            {
                throw new ValidationException("Введеный адрес электронной почты имеет неверный формат");
            }
        }
    }
}