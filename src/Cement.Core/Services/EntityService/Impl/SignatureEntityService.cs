﻿using Cement.Core.Entities;

namespace Cement.Core.Services.EntityService.Impl
{
    public class SignatureEntityService : BaseEntityService<Signature>
    {
        protected override void AfterDelete(long id)
        {
            UpdateUserCache();

            base.AfterDelete(id);
        }

        protected override void AfterSave(Signature value)
        {
            UpdateUserCache();

            base.AfterSave(value);
        }

        protected override void AfterUpdate(Signature value)
        {
            UpdateUserCache();

            base.AfterUpdate(value);
        }

        private void UpdateUserCache()
        {
            var signAuthorityService = Container.Resolve<ISignAuthorityService>();

            try
            {
                signAuthorityService.UpdateCache();
            }
            finally
            {
                Container.Release(signAuthorityService);
            }
        }

    }
}