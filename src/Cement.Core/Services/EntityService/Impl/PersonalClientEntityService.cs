﻿using System.Linq;
using Cement.Core.Auth;
using Cement.Core.Entities;
using Cement.Core.Exceptions;
using Cement.Core.Extensions;

namespace Cement.Core.Services.EntityService.Impl
{
    public class PersonalClientEntityService : BaseEntityService<PersonalClient>
    {
        protected override void BeforeUpdate(PersonalClient value)
        {
            base.BeforeUpdate(value);

            value.Employee = Container.Resolve<IUserPrincipal>().Employee;
        }

        protected override void BeforeSave(PersonalClient entity)
        {
            base.BeforeSave(entity);

            var userPrincipal = Container.Resolve<IUserPrincipal>();

            var clientIsAlreadyPersonal = Repository.GetAll().OrganizationFilter(userPrincipal).Count(x => x.Client == entity.Client) > 0;

            if (clientIsAlreadyPersonal)
            {
                throw new ValidationException("Есть дублирующая запись клиента для организации");
            }

            entity.Employee = userPrincipal.Employee;
        }
    }
}