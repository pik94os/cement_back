﻿using Cement.Core.Entities;
using Cement.Core.Exceptions;

namespace Cement.Core.Services.EntityService.Impl
{
    public class TransportUnitArendEntityService : BaseEntityService<TransportUnitArend>
    {
        protected override void BeforeUpdate(TransportUnitArend value)
        {
            base.BeforeUpdate(value);

            Validate(value);
        }

        protected override void BeforeSave(TransportUnitArend value)
        {
            base.BeforeSave(value);

            Validate(value);

            value.IsActive = true;
        }

        private void Validate(TransportUnitArend value)
        {
            if (value.TransportUnit == null)
            {
                throw new ValidationException("Не задана Транспортная единица");
            }
        }
    }
}