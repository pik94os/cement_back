﻿using System;
using Cement.Core.Entities;
using Cement.Core.Enums;
using Cement.Core.Services.EntityService.Impl;
using DataAccess;
using DataAccess.Repository;

namespace Cement.Core.Services.EntityService
{
    [Obsolete]
    public class BaseDocumentMovementEntityService<T> : BaseEntityService<T> where T : IDocumentMovement, IEntity
    {
        public IRepository<IncomingDocument> IncomingDocumentRepository { get; set; }
        public IRepository<OutcomingDocument> OutcomingDocumentRepository { get; set; }

        protected override void AfterSave(T value)
        {
            if (value.MovementType == DocumentMovementType.Incoming)
            {
                var incomingDocument = new IncomingDocument
                {
                    DocumentId = value.DocumentId,
                    EmployeeId = value.EmployeeId,
                    IsArchived = value.IsArchived,
                    OrganizationId = value.OrganizationId,
                    Id = value.Id
                };

                IncomingDocumentRepository.Save(incomingDocument);
            }
            else if (value.MovementType == DocumentMovementType.Outcoming)
            {
                var outcomingDocument = new OutcomingDocument
                {
                    DocumentId = value.DocumentId,
                    EmployeeId = value.EmployeeId,
                    IsArchived = value.IsArchived,
                    OrganizationId = value.OrganizationId,
                    Id = value.Id
                };

                OutcomingDocumentRepository.Save(outcomingDocument);
            }

            base.AfterSave(value);
        }

        protected override void BeforeDelete(long id)
        {
            base.BeforeDelete(id);

            var entity = Repository.Load(id);

            if (entity.MovementType == DocumentMovementType.Incoming)
            {
                IncomingDocumentRepository.Delete(id);
            }
            else if (entity.MovementType == DocumentMovementType.Outcoming)
            {
                OutcomingDocumentRepository.Delete(id);
            }

            base.AfterDelete(id);
        }
        
        protected override void AfterUpdate(T value)
        {
            if (value.MovementType == DocumentMovementType.Incoming)
            {
                var entity = IncomingDocumentRepository.Get(value.Id);

                entity.IsArchived = value.IsArchived;

                IncomingDocumentRepository.Update(entity);
            }
            else if (value.MovementType == DocumentMovementType.Outcoming)
            {
                var entity = OutcomingDocumentRepository.Get(value.Id);

                entity.IsArchived = value.IsArchived;

                OutcomingDocumentRepository.Update(entity);
            }

            base.AfterUpdate(value);
        }
    }
}