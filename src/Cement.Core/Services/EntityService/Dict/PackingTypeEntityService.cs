﻿using System.Linq;
using Cement.Core.Entities;
using Cement.Core.Services.EntityService.Impl;
using NHibernate.Linq;

namespace Cement.Core.Services.EntityService.Dict
{
    public class PackingTypeEntityServiceEntityService : FileFieldEntityService<PackingType>
    {
        public override IQueryable<PackingType> GetAll()
        {
            return base.GetAll().Fetch(x => x.Photo);
        }
    }
}