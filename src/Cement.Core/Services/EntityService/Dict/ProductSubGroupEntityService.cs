﻿using System.Linq;
using Cement.Core.Entities;
using Cement.Core.Services.EntityService.Impl;
using NHibernate.Linq;

namespace Cement.Core.Services.EntityService.Dict
{
    public class ProductSubGroupEntityService : BaseEntityService<ProductSubGroup>
    {
        public override IQueryable<ProductSubGroup> GetAll()
        {
            return base.GetAll()
                .Fetch(x => x.ProductGroup);
        }
    }
}