﻿using System.Linq;
using Cement.Core.Entities;
using Cement.Core.Services.EntityService.Impl;
using NHibernate.Linq;

namespace Cement.Core.Services.EntityService.Dict
{
    public class DangerSubClassEntityService : BaseEntityService<DangerSubClass>
    {
        public override IQueryable<DangerSubClass> GetAll()
        {
            return base.GetAll()
                .Fetch(x => x.Class);
        }
    }
}