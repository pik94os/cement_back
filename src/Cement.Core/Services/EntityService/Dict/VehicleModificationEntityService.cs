﻿using System.Linq;
using Cement.Core.Entities;
using Cement.Core.Services.EntityService.Impl;
using NHibernate.Linq;

namespace Cement.Core.Services.EntityService.Dict
{
    public class VehicleModificationEntityService : BaseEntityService<VehicleModification>
    {
        public override IQueryable<VehicleModification> GetAll()
        {
            return base.GetAll()
                .Fetch(x => x.Model)
                .ThenFetch(x => x.Brand)
                .Fetch(x => x.Model)
                .ThenFetch(x => x.Category)
                .Fetch(x => x.Model)
                .ThenFetch(x => x.SubCategory);
        }
    }
}