﻿using System.Linq;
using Cement.Core.Entities;
using Cement.Core.Services.EntityService.Impl;
using NHibernate.Linq;

namespace Cement.Core.Services.EntityService.Dict
{
    public class VehicleModelEntityService : BaseEntityService<VehicleModel>
    {
        public override IQueryable<VehicleModel> GetAll()
        {
            return base.GetAll()
                .Fetch(x => x.Brand)
                .Fetch(x => x.Category)
                .Fetch(x => x.SubCategory);
        }
    }
}