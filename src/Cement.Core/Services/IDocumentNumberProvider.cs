﻿using System;

namespace Cement.Core.Services
{
    public interface IDocumentNumberProvider
    {
        string GetNewNumber(Type type);
    }
}