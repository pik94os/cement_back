﻿namespace Cement.Core.Services
{
    public interface IMessageService
    {
        void Sign(long messageId, string comment);

        void MarkAsRead(long messageMovementId);

        //void Process(long messageId, bool send, long? employeeId, string comment);

        //void ProcessFromMovement(long messageMovementId, bool send, long? employeeId, string comment);
    }
}