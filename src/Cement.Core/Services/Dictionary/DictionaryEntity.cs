﻿using Cement.Core.Entities;
using Cement.Core.Enums;
using Newtonsoft.Json;

namespace Cement.Core.Services.Dictionary
{
    public class DictionaryEntity
    {
        [JsonProperty(PropertyName = "id")]
        public long Id { get; set; }

        [JsonProperty(PropertyName = "entityType")]
        public string EntityType { get; set; }

        public string Name { get; set; }

        public string ShortName { get; set; }

        public string Description { get; set; }

        public string FullName { get; set; }

        public string Bik { get; set; }

        public string Ks { get; set; }

        public VehicleModel Model { get; set; }

        public VehicleBrand Brand { get; set; }

        public VehicleCategory Category { get; set; }

        public VehicleSubCategory SubCategory { get; set; }

        public ProductType ProductType  { get; set; }

        public ProductGroup ProductGroup  { get; set; }

        public decimal Rate { get; set; }

        public string Num { get; set; }

        public DangerClass Class { get; set; }

        public string Code { get; set; }

        public string RegionName { get; set; }

        public string RailwayName { get; set; }

        public string RailwayShortName { get; set; }

        public MeasureUnitGroup MeasureUnitGroup { get; set; }

        public bool IsNational { get; set; }

        public bool IsNotInEskk { get; set; }

        public EntityFile Photo { get; set; }
    }
}