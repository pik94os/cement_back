﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using Castle.Windsor;
using Cement.Core.Entities;
using Cement.Core.Entities.Dict;
using Cement.Core.Enums;
using Cement.Core.Exceptions;
using Cement.Core.Extensions;
using Cement.Core.Services.EntityService;
using DataAccess;
using DataAccess.SessionProvider;
using Newtonsoft.Json;

namespace Cement.Core.Services.Dictionary.Impl
{
    public class DictService : IDictService
    {
        #region Injections

        public IEntityService<VehicleBrand> VehicleBrandService { get; set; }
        public IEntityService<VehicleModel> VehicleModelService { get; set; }
        public IEntityService<VehicleModification> VehicleModificationService { get; set; }
        public IEntityService<VehicleClass> VehicleClassService { get; set; }
        public IEntityService<VehicleBodyType> VehicleBodyTypeService { get; set; }
        public IEntityService<VehicleAdditionalOptions> VehicleAdditionalOptionsService { get; set; }
        public IEntityService<VehicleCategory> VehicleCategoryService { get; set; }
        public IEntityService<VehicleSubCategory> VehicleSubCategoryService { get; set; }
        public IEntityService<VehicleWheelBase> VehicleWheelBaseService { get; set; }
        public IEntityService<DriverCategory> DriverCategoryService { get; set; }
        public IEntityService<Experience> ExperienceService { get; set; }
        public IEntityService<Country> CountryService { get; set; }
        public IEntityService<DangerSubClass> DangerSubClassService { get; set; }
        public IEntityService<DangerClass> DangerClassService { get; set; }
        public IEntityService<DangerUnCode> DangerUnCodeService { get; set; }
        public IEntityService<PackingMaterial> PackingMaterialService { get; set; }
        public IEntityService<PackingType> PackingTypeService { get; set; }
        public IEntityService<ProductGroup> ProductGroupService { get; set; }
        public IEntityService<ProductSubGroup> ProductSubGroupService { get; set; }
        public IEntityService<MeasureUnit> MeasureUnitService { get; set; }
        public IEntityService<Ownership> OwnershipService { get; set; }
        public IEntityService<Position> PositionService { get; set; }
        public IEntityService<Tax> TaxService { get; set; }
        public IEntityService<RailwayStation> RailwayStationService { get; set; }
        public IEntityService<Bogie> BogieService { get; set; }
        public IEntityService<Fabricator> FabricatorService { get; set; }
        public IEntityService<FeatureModel> FeatureModelService { get; set; }
        public IEntityService<Size> SizeService { get; set; }
        public IEntityService<RailwayTransport> RailwayTransportService { get; set; }
        public IEntityService<Bank> BankService { get; set; }

        public IWindsorContainer Container { get; set; }

        #endregion Injections

        public void Populate(string appPath)
        {
            PopulateVehicleClasses();
            PopulateVehicleBodyType();
            PopulateVehicleAdditionalOptionsService();
            PopulateVehicleCategory();
            PopulateVehicleWheelBase();
            PopulateDriverCategory();
            PopulateExperience();
            PopulateCountry();
            PopulateDangerClasses();
            PopulateDangerSubClasses();
            PopulateDangerUnCodes();
            PopulatePackingMaterial(appPath);
            PopulatePackingType(appPath);
            PopulateProductGroupsSubGroups(appPath);
            PopulateMeasureUnits(appPath);
            PopulateOwnership(appPath);
            PopulatePosition(appPath);
            PopulateTax();
            PopulateRailwayStations(appPath);
            PopulateRailwayTransport(appPath);
            PopulateBank(appPath);
            PopulateCarModels(appPath);
        }

        private void PopulateVehicleClasses()
        {
            var classes = new List<string>
            {
                "до 650 кг.",
                "до 1000 кг.",
                "до 1500 кг.",
                "до 3000 кг.",
                "до 5000 кг.",
                "до 10000 кг.",
                "до 20000 кг и выше"
            };

            var existing = VehicleClassService.GetAll().Select(x => x.Name)
                .AsEnumerable()
                .GroupBy(x => x)
                .ToDictionary(x => x.Key);
            
            classes.ForEach(x =>
            {
                if (!existing.ContainsKey(x))
                {
                    VehicleClassService.Save(new VehicleClass { Name = x });
                }
            });
        }

        private void PopulateVehicleBodyType()
        {
            var bodytypes = new List<string>
            {
                "Тент",
                "Открытый борт",
                "Фургон (обычный)",
                "Изотермический фургон",
                "Рефрижератор",
                "Самосвал",
                "Бункеровоз",
                "Автоцистерна",
                "Стекловоз",
                "Цементовоз",
                "Контейнеровоз",
                "Опен Тор",
                "Скотовоз",
                "Автотранспортер",
                "Муковоз",
                "Панелевоз",
                "Миксер",
                "Лесовоз",
                "Роспуск",
                "Эвакуатор"
            };

            var existing = VehicleBodyTypeService.GetAll().Select(x => x.Name)
                .AsEnumerable()
                .GroupBy(x => x)
                .ToDictionary(x => x.Key);

            bodytypes.OrderBy(x => x).ToList().ForEach(x =>
            {
                if (!existing.ContainsKey(x))
                {
                    VehicleBodyTypeService.Save(new VehicleBodyType { Name = x });
                }
            });
        }

        private void PopulateVehicleAdditionalOptionsService()
        {
            var values = new List<string>
            {
                "Гидролифт",
                "Кран-манипулятор",
                "Пирамида",
                "Коники",
                "Компрессор дизельный",
                "Компрессор электрический",
                "Без компрессора",
                "Санпаспорт",
                "Фаркоп",
                "Боковая шторка",
                "Верхняя шторка",
                "Без бортов",
                "Борта открываются",
                "Борта снимаются",
                "Разборная крыша",
                "Деревянный пол",
                "Алюминиевый пол",
                "Пластиковый пол",
                "Пневмоподвеска",
                "Мягкая обивка внутри",
                "Крепления к полу",
                "Крепления в стенках",
                "Крепежные ремни",
                "Крючки для туш",
                "Сзади ворота-створки",
                "Сзади верхняя дверь",
                "Сзади клапан-тент",
                "Боковая дверь справа",
                "Боковая дверь слева",
                "Агрегат на «минус»",
                "Агрегат на «плюс»",
                "Тент укрыть груз",
                "Подогрев кузова",
            };

            var existing = VehicleAdditionalOptionsService.GetAll().Select(x => x.Name)
                .AsEnumerable()
                .GroupBy(x => x)
                .ToDictionary(x => x.Key);

            values.OrderBy(x => x).ToList().ForEach(x =>
            {
                if (!existing.ContainsKey(x))
                {
                    VehicleAdditionalOptionsService.Save(new VehicleAdditionalOptions { Name = x });
                }
            });
        }

        private void PopulateVehicleCategory()
        {
            var values = new []
            {
                new
                {
                     name = "Легковые",
                     hasСharacteristics = false,
                     subcategories = new List<string>()
                },
                new
                {
                     name = "Грузовые",
                     hasСharacteristics = true,
                     subcategories = new List<string>{ "Легкие коммерческие", "Седельный тягач", "Грузовик" }
                },
                new
                {
                     name = "Прицепные устройства",
                     hasСharacteristics = true,
                     subcategories = new List<string>{ "Прицеп", "Полуприцеп" }
                },
                new
                {
                     name = "Автобус",
                     hasСharacteristics = false,
                     subcategories = new List<string>()
                },
                new
                {
                     name = "Спецтехника",
                     hasСharacteristics = false,
                     subcategories = new List<string>
                     {
                        "Автокраны",
                        "Автопогрузчики",
                        "Бульдозеры",
                        "Коммунальная",
                        "Самопогрузчики",
                        "Сельхозтехника",
                        "Строительная техника",
                        "Экскаваторы"
                     }
                }
            };

            var existing = VehicleCategoryService.GetAll().ToList();

            var existingSubCats = VehicleSubCategoryService.GetAll()
                .Where(x => x.Category != null)
                .Select(x => new {x.Id, x.Name, catId = x.Category.Id})
                .AsEnumerable()
                .GroupBy(x => x.catId)
                .ToDictionary(x => x.Key, x => x.ToList());

            values.ToList().ForEach(x =>
            {
                var category = existing.FirstOrDefault(y => y.Name == x.name);

                if (category == null)
                {
                    category = new VehicleCategory { Name = x.name, HasСharacteristics = x.hasСharacteristics};

                    VehicleCategoryService.Save(category);

                    x.subcategories.ForEach(y => VehicleSubCategoryService.Save(new VehicleSubCategory{ Name = y, Category = category}));
                }
                else
                {
                    var subcats = existingSubCats.Get(category.Id);

                    x.subcategories.ForEach(y =>
                    {
                        if (!subcats.Any(z => z.Name == y))
                        {
                            VehicleSubCategoryService.Save(new VehicleSubCategory {Name = y, Category = category});
                        }
                    });

                }
            });
        }

        private void PopulateVehicleWheelBase()
        {
            var values = new List<string>
            {
                "4x2/2",
                "4x2/1",
                "4x4/1",
                "6x4/2",
                "6x6/2",
                "6x6/1",
                "6x2/1",
                "8x4/2",
                "8x4/1",
                "8x8/2",
                "8x8/1"
            };

            var existing = VehicleWheelBaseService.GetAll().Select(x => x.Name)
                .AsEnumerable()
                .GroupBy(x => x)
                .ToDictionary(x => x.Key);

            values.OrderBy(x => x).ToList().ForEach(x =>
            {
                if (!existing.ContainsKey(x))
                {
                    VehicleWheelBaseService.Save(new VehicleWheelBase { Name = x });
                }
            });
        }

        private void PopulateDriverCategory()
        {
            var values = new []
            {
                new { name = "A", clientValue = 1, type = DriverCategoryType.AutoMoto },
                new { name = "B", clientValue = 2, type = DriverCategoryType.AutoMoto },
                new { name = "C", clientValue = 3, type = DriverCategoryType.AutoMoto },
                new { name = "D", clientValue = 4, type = DriverCategoryType.AutoMoto },
                new { name = "E", clientValue = 5, type = DriverCategoryType.AutoMoto },
                new { name = "BE", clientValue = 6, type = DriverCategoryType.AutoMoto },
                new { name = "CE", clientValue = 7, type = DriverCategoryType.AutoMoto },
                new { name = "DE", clientValue = 8, type = DriverCategoryType.AutoMoto },
                new { name = "Трамвай", clientValue = 9, type = DriverCategoryType.AutoMoto },
                new { name = "Троллейбус", clientValue = 10, type = DriverCategoryType.AutoMoto },

                new { name = "A I", clientValue = 21, type = DriverCategoryType.Tractor },
                new { name = "A II", clientValue = 22, type = DriverCategoryType.Tractor },
                new { name = "A III", clientValue = 23, type = DriverCategoryType.Tractor },
                new { name = "A IV", clientValue = 24, type = DriverCategoryType.Tractor },
                new { name = "B", clientValue = 25, type = DriverCategoryType.Tractor },
                new { name = "C", clientValue = 26, type = DriverCategoryType.Tractor },
                new { name = "D", clientValue = 27, type = DriverCategoryType.Tractor },
                new { name = "E", clientValue = 28, type = DriverCategoryType.Tractor },
                new { name = "F", clientValue = 29, type = DriverCategoryType.Tractor }
            };

            var existing = DriverCategoryService.GetAll()
                .Select(x => new { x.Type, x.Name })
                .AsEnumerable()
                .GroupBy(x => x.Type)
                .ToDictionary(x => x.Key, x => x.Select(y => y.Name).ToList());

            values.GroupBy(x => x.type).ToList().ForEach(x =>
            {
                if (existing.ContainsKey(x.Key))
                {
                    var existingNamesInType = existing[x.Key];

                    x.ToList().ForEach(y =>
                    {
                        if (!existingNamesInType.Contains(y.name))
                        {
                            DriverCategoryService.Save(new DriverCategory { Name = y.name , ClientValue = y.clientValue, Type = y.type});
                        }

                    });
                }
                else
                {
                    x.ToList().ForEach(y => DriverCategoryService.Save(new DriverCategory { Name = y.name , ClientValue = y.clientValue, Type = y.type}));
                }
            });
        }

        private void PopulateExperience()
        {
            var values = new[]
            {
                new { name = "До 3 лет", type = ExperienceType.Driving },
                new { name = "От 3 до 5 лет", type = ExperienceType.Driving },
                new { name = "Свыше 5 лет", type = ExperienceType.Driving }
            };

            var existing = ExperienceService.GetAll()
                .Select(x => new {x.Type, x.Name})
                .AsEnumerable()
                .GroupBy(x => x.Type)
                .ToDictionary(x => x.Key, x => x.Select(y => y.Name).ToList());

            values.GroupBy(x => x.type).ToList().ForEach(x =>
            {
                if (existing.ContainsKey(x.Key))
                {
                    var existingNamesInType = existing[x.Key];

                    x.ToList().ForEach(y =>
                    {
                        if (!existingNamesInType.Contains(y.name))
                        {
                            ExperienceService.Save(new Experience { Name = y.name, Type = y.type });
                        }
                        
                    });
                }
                else
                {
                    x.ToList().ForEach(y => ExperienceService.Save(new Experience { Name = y.name, Type = y.type }));
                }
            });
        }

        private void PopulateCountry()
        {
            var countryList = new List<string>
            {
                "Абхазия",
                "Австралия",
                "Австрия",
                "Азербайджан",
                "Албания",
                "Алжир",
                "Американское Самоа",
                "Ангилья",
                "Ангола",
                "Андорра",
                "Антарктида",
                "Антигуа и Барбуда",
                "Аргентина",
                "Армения",
                "Аруба",
                "Афганистан",
                "Багамы",
                "Бангладеш",
                "Барбадос",
                "Бахрейн",
                "Беларусь",
                "Белиз",
                "Бельгия",
                "Бенин",
                "Бермуды",
                "Болгария",
                "Боливия, Многонациональное Государство",
                "Бонайре, Саба и Синт-Эстатиус",
                "Босния и Герцеговина",
                "Ботсвана",
                "Бразилия",
                "Британская территория в Индийском океане",
                "Бруней-Даруссалам",
                "Буркина-Фасо",
                "Бурунди",
                "Бутан",
                "Вануату",
                "Венгрия",
                "Венесуэла Боливарианская Республика",
                "Виргинские острова, Британские",
                "Виргинские острова, США",
                "Вьетнам",
                "Габон",
                "Гаити",
                "Гайана",
                "Гамбия",
                "Гана",
                "Гваделупа",
                "Гватемала",
                "Гвинея",
                "Гвинея-Бисау",
                "Германия",
                "Гернси",
                "Гибралтар",
                "Гондурас",
                "Гонконг",
                "Гренада",
                "Гренландия",
                "Греция",
                "Грузия",
                "Гуам",
                "Дания",
                "Джерси",
                "Джибути",
                "Доминика",
                "Доминиканская Республика",
                "Египет",
                "Замбия",
                "Западная Сахара",
                "Зимбабве",
                "Израиль",
                "Индия",
                "Индонезия",
                "Иордания",
                "Ирак",
                "Иран, Исламская Республика",
                "Ирландия",
                "Исландия",
                "Испания",
                "Италия",
                "Йемен",
                "Кабо-Верде",
                "Казахстан",
                "Камбоджа",
                "Камерун",
                "Канада",
                "Катар",
                "Кения",
                "Кипр",
                "Киргизия",
                "Кирибати",
                "Китай",
                "Кокосовые (Килинг) острова",
                "Колумбия",
                "Коморы",
                "Конго",
                "Конго, Демократическая Республика",
                "Корея, Народно-Демократическая Республика",
                "Корея, Республика",
                "Коста-Рика",
                "Кот д'Ивуар",
                "Куба",
                "Кувейт",
                "Кюрасао",
                "Лаос",
                "Латвия",
                "Лесото",
                "Ливан",
                "Ливийская Арабская Джамахирия",
                "Либерия",
                "Лихтенштейн",
                "Литва",
                "Люксембург",
                "Маврикий",
                "Мавритания",
                "Мадагаскар",
                "Майотта",
                "Макао",
                "Малави",
                "Малайзия",
                "Мали",
                "Малые Тихоокеанские отдаленные острова Соединенных Штатов",
                "Мальдивы",
                "Мальта",
                "Марокко",
                "Мартиника",
                "Маршалловы острова",
                "Мексика",
                "Микронезия, Федеративные Штаты",
                "Мозамбик",
                "Молдова, Республика",
                "Монако",
                "Монголия",
                "Монтсеррат",
                "Мьянма",
                "Намибия",
                "Науру",
                "Непал",
                "Нигер",
                "Нигерия",
                "Нидерланды",
                "Никарагуа",
                "Ниуэ",
                "Новая Зеландия",
                "Новая Каледония",
                "Норвегия",
                "Объединенные Арабские Эмираты",
                "Оман",
                "Остров Буве",
                "Остров Мэн",
                "Остров Норфолк",
                "Остров Рождества",
                "Остров Херд и острова Макдональд",
                "Острова Кайман",
                "Острова Кука",
                "Острова Теркс и Кайкос",
                "Пакистан",
                "Палау",
                "Палестинская территория, оккупированная",
                "Панама",
                "Папский Престол (Государство — город Ватикан)",
                "Папуа-Новая Гвинея",
                "Парагвай",
                "Перу",
                "Питкерн",
                "Польша",
                "Португалия",
                "Пуэрто-Рико",
                "Республика Македония",
                "Реюньон",
                "Россия",
                "Руанда",
                "Румыния",
                "Самоа",
                "Сан-Марино",
                "Сан-Томе и Принсипи",
                "Саудовская Аравия",
                "Свазиленд",
                "Святая Елена, Остров вознесения, Тристан-да-Кунья",
                "Северные Марианские острова",
                "Сен-Бартельми",
                "Сен-Мартен",
                "Сенегал",
                "Сент-Винсент и Гренадины",
                "Сент-Люсия",
                "Сент-Китс и Невис",
                "Сент-Пьер и Микелон",
                "Сербия",
                "Сейшелы",
                "Сингапур",
                "Синт-Мартен",
                "Сирийская Арабская Республика",
                "Словакия",
                "Словения",
                "Соединенное Королевство",
                "Соединенные Штаты",
                "Соломоновы острова",
                "Сомали",
                "Судан",
                "Суринам",
                "Сьерра-Леоне",
                "Таджикистан",
                "Таиланд",
                "Тайвань (Китай)",
                "Танзания, Объединенная Республика",
                "Тимор-Лесте",
                "Того",
                "Токелау",
                "Тонга",
                "Тринидад и Тобаго",
                "Тувалу",
                "Тунис",
                "Туркмения",
                "Турция",
                "Уганда",
                "Узбекистан",
                "Украина",
                "Уоллис и Футуна",
                "Уругвай",
                "Фарерские острова",
                "Фиджи",
                "Филиппины",
                "Финляндия",
                "Фолклендские острова (Мальвинские)",
                "Франция",
                "Французская Гвиана",
                "Французская Полинезия",
                "Французские Южные территории",
                "Хорватия",
                "Центрально-Африканская Республика",
                "Чад",
                "Черногория",
                "Чешская Республика",
                "Чили",
                "Швейцария",
                "Швеция",
                "Шпицберген и Ян Майен",
                "Шри-Ланка",
                "Эквадор",
                "Экваториальная Гвинея",
                "Эландские острова",
                "Эль-Сальвадор",
                "Эритрея",
                "Эстония",
                "Эфиопия",
                "Южная Африка",
                "Южная Джорджия и Южные Сандвичевы острова",
                "Южная Осетия",
                "Южный Судан",
                "Ямайка",
                "Япония"
            };

            var existing = CountryService.GetAll().Select(x => x.Name.ToLower())
                .AsEnumerable()
                .GroupBy(x => x)
                .ToDictionary(x => x.Key);

            countryList.OrderBy(x => x).ToList().ForEach(x =>
            {
                if (!existing.ContainsKey(x.ToLower()))
                {
                    CountryService.Save(new Country { Name = x });
                }
            });
        }

        private void PopulateDangerClasses()
        {
            var values = new[]
            {
                new { classNum="1", className="Взрывчатые материалы (ВМ)"},
                new { classNum="2", className="Газы сжатые, сжиженные и растворенные под давлением"},
                new { classNum="3", className="Легковоспламеняющиеся жидкости (ЛВЖ)"},
                new { classNum="4", className="Легковоспламеняющиеся твердые грузы и вещества (ЛВТ);самовозгорающиеся грузы и вещества (СВ);грузы и вещества, выделяющие воспламеняющиеся газы при взаимодействии с водой;"},
                new { classNum="5", className="Окисляющие вещества (ОК) и органические пероксиды (ОП)"},
                new { classNum="6", className="Ядовитые вещества (ЯВ) и инфекционные грузы и вещества (ИВ)"},
                new { classNum="7", className="Радиоактивные материалы (РМ)"},
                new { classNum="8", className="Едкие и (или) коррозионные грузы и вещества (ЕК)"},
                new { classNum="9", className="Прочие опасные грузы и вещества"},
            };

            var existing = DangerClassService.GetAll()
                .Select(x => x.Num)
                .AsEnumerable()
                .GroupBy(x => x)
                .ToDictionary(x => x.Key);

            values.ToList().ForEach(x =>
            {
                if (!existing.ContainsKey(x.classNum))
                {
                    DangerClassService.Save(new DangerClass {Name = x.className, Num = x.classNum});
                }
            });
        }

        private void PopulateDangerSubClasses()
        {
            var values = new[]
            {
                new { classNum="1", subClassNum="1.1", subClassName="Взрывчатые материалы с опасностью взрыва массой"},
                new { classNum="1", subClassNum="1.2", subClassName="Взрывчатые материалы, не взрывающиеся массой"},
                new { classNum="1", subClassNum="1.3", subClassName="Взрывчатые материалы пожароопасные, не взрывающиеся массой"},
                new { classNum="1", subClassNum="1.4", subClassName="Взрывчатые материалы, не представляющие значительной опасности"},
                new { classNum="1", subClassNum="1.5", subClassName="Очень нечувствительные взрывчатые материалы"},
                new { classNum="1", subClassNum="1.6", subClassName="Изделия чрезвычайно низкой чувствительности"},
                new { classNum="2", subClassNum="2.1", subClassName="Невоспламеняющиеся неядовитые газы"},
                new { classNum="2", subClassNum="2.2", subClassName="Ядовитые газы"},
                new { classNum="2", subClassNum="2.3", subClassName="Воспламеняющиеся (горючие) газы"},
                new { classNum="2", subClassNum="2.4", subClassName="Ядовитые и воспламеняющиеся газы"},
                new { classNum="3", subClassNum="3.1", subClassName="Легковоспламеняющиеся жидкости с температурой вспышки менее минус 18 °C в закрытом тигле"},
                new { classNum="3", subClassNum="3.2", subClassName="Легковоспламеняющиеся жидкости с температурой вспышки не менее минус 18 °C, но менее 23 °C, в закрытом тигле"},
                new { classNum="3", subClassNum="3.3", subClassName="Легковоспламеняющиеся жидкости с температурой вспышки не менее 23 °C, но не более 61 °C, в закрытом тигле"},
                new { classNum="4", subClassNum="4.1", subClassName="Легковоспламеняющиеся твердые вещества"},
                new { classNum="4", subClassNum="4.2", subClassName="Самовозгорающиеся вещества"},
                new { classNum="4", subClassNum="4.3", subClassName="Вещества, выделяющие воспламеняющиеся газы при взаимодействии с водой"},
                new { classNum="5", subClassNum="5.1", subClassName="Окисляющие вещества"},
                new { classNum="5", subClassNum="5.2", subClassName="Органические пероксиды"},
                new { classNum="6", subClassNum="6.1", subClassName="Ядовитые вещества"},
                new { classNum="6", subClassNum="6.2", subClassName="Инфекционные вещества"},
                new { classNum="8", subClassNum="8.1", subClassName="Едкие и (или) коррозионные вещества, обладающие кислотными свойствами"},
                new { classNum="8", subClassNum="8.2", subClassName="Едкие и (или) коррозионные вещества, обладающие основными свойствами"},
                new { classNum="8", subClassNum="8.3", subClassName="Разные едкие и (или) коррозионные вещества"},
                new { classNum="9", subClassNum="9.1", subClassName="Грузы, не отнесенные к классам 1 - 8"},
                new { classNum="9", subClassNum="9.2", subClassName="Грузы, обладающие видами опасности, проявление которых представляет опасность только при их транспортировании навалом водным транспортом"}
            };

            var existingSubClasses = DangerSubClassService.GetAll()
                .Select(x => x.Num)
                .AsEnumerable()
                .GroupBy(x => x)
                .ToDictionary(x => x.Key);

            var existingClasses = DangerClassService.GetAll()
                .Select(x => new {x.Num, x.Id})
                .AsEnumerable()
                .GroupBy(x => x.Num)
                .ToDictionary(x => x.Key, x => x.Select(y => y.Id).First());

            values.ToList().ForEach(x =>
            {
                if (existingClasses.ContainsKey(x.classNum))
                {
                    if (!existingSubClasses.ContainsKey(x.subClassNum))
                    {
                        DangerSubClassService.Save(new DangerSubClass { Name = x.subClassName, Num = x.subClassNum, Class = new DangerClass { Id = existingClasses[x.classNum] } });
                    }
                }
            });
        }

        private void PopulateDangerUnCodes()
        {
            var existing = DangerUnCodeService.GetAll()
                .Select(x => x.Name)
                .AsEnumerable()
                .GroupBy(x => x)
                .ToDictionary(x => x.Key);

            for (var i = 1; i <= 3500; i++)
            {
                var stringCode = i.ToString("D4");
                if (!existing.ContainsKey(stringCode))
                {
                    DangerUnCodeService.Save(new DangerUnCode { Name = stringCode });
                }
            }
        }

        private void PopulatePackingMaterial(string appPath)
        {
            var path = appPath + @"\Content\dictionary-sources\PackingMaterial.json";

            if (!File.Exists(path))
            {
                return;
            }

            var jsonString = File.ReadAllText(path);

            var newMaterials = JsonConvert.DeserializeObject<List<PackingMaterial>>(jsonString);
            
            var existing = PackingMaterialService.GetAll()
                .Select(x => x.Name)
                .AsEnumerable()
                .GroupBy(x => x)
                .ToDictionary(x => x.Key);

            foreach (var newMaterial in newMaterials)
            {
                if (!existing.ContainsKey(newMaterial.Name))
                {
                    PackingMaterialService.Save(newMaterial);
                }
            }
        }

        private void PopulatePackingType(string appPath)
        {
            var path = appPath + @"\Content\dictionary-sources\PackingType.json";

            if (!File.Exists(path))
            {
                return;
            }

            var jsonString = File.ReadAllText(path);
           
            var newTypes = JsonConvert.DeserializeObject<List<PackingType>>(jsonString);

            var existing = PackingTypeService.GetAll()
                .Select(x => x.Name)
                .AsEnumerable()
                .GroupBy(x => x)
                .ToDictionary(x => x.Key);

            foreach (var newType in newTypes)
            {
                if (!existing.ContainsKey(newType.Name))
                {
                    PackingTypeService.Save(newType);
                }
            }
        }

        private void PopulateProductGroupsSubGroups(string appPath)
        {
            var path = appPath + @"\Content\dictionary-sources\ProductsGroup.json";

            if (!File.Exists(path))
            {
                return;
            }

            var jsonString = File.ReadAllText(path);

            var newTypes = JsonConvert.DeserializeObject<List<ProductGroupsSubGroupsNames>>(jsonString)
                .GroupBy(x => x.ProductGroupName)
                .ToDictionary(x => x.Key, x => x.Select(y => y.ProductSubGroupName).ToList());

            var existingGroups = ProductGroupService.GetAll()
                .AsEnumerable()
                .GroupBy(x => x.Name)
                .ToDictionary(x => x.Key, x => x.Select(y => y.Id).First());

            var existingSubGroups = ProductSubGroupService.GetAll()
                .Select(x => new { x.Name, x.ProductGroup.Id })
                .AsEnumerable()
                .GroupBy(x => x.Id)
                .ToDictionary(x => x.Key, x => x.Select(y => y.Name).ToList());

            foreach (var newType in newTypes)
            {
                if (existingGroups.ContainsKey(newType.Key))
                {
                    var groupId = existingGroups[newType.Key];
                    var existingSubGroupsInCurrentGroup = existingSubGroups.ContainsKey(groupId) ? existingSubGroups[groupId] : new List<string>();
                    foreach (var newSubGroupName in newType.Value)
                    {
                        if (!existingSubGroupsInCurrentGroup.Contains(newSubGroupName))
                        {
                            ProductSubGroupService.Save(new ProductSubGroup { Name = newSubGroupName, ProductGroup = new ProductGroup { Id = groupId } });
                        }
                    }
                }
                else
                {
                    var newGroup = new ProductGroup {Name = newType.Key};
                    ProductGroupService.Save(newGroup);
                    foreach (var newSubGroupName in newType.Value)
                    {
                        ProductSubGroupService.Save(new ProductSubGroup { Name = newSubGroupName, ProductGroup = newGroup });
                    }
                }
            }
        }

        private void PopulateMeasureUnits(string appPath)
        {
            var path = appPath + @"\Content\dictionary-sources\MeasureUnit.json";

            if (!File.Exists(path))
            {
                return;
            }

            var jsonString = File.ReadAllText(path);

            var newTypes = JsonConvert.DeserializeObject<List<MeasureUnit>>(jsonString);

            var existing = MeasureUnitService.GetAll()
                .Select(x => x.Name)
                .AsEnumerable()
                .GroupBy(x => x)
                .ToDictionary(x => x.Key);

            foreach (var newType in newTypes)
            {
                if (!existing.ContainsKey(newType.Name))
                {
                    MeasureUnitService.Save(newType);
                }
            }
        }

        private void PopulateOwnership(string appPath)
        {
            var path = appPath + @"\Content\dictionary-sources\Ownership.json";

            if (!File.Exists(path))
            {
                return;
            }

            var jsonString = File.ReadAllText(path);

            var newTypes = JsonConvert.DeserializeObject<List<Ownership>>(jsonString);

            var existing = OwnershipService.GetAll()
                .Select(x => x.Name)
                .AsEnumerable()
                .GroupBy(x => x)
                .ToDictionary(x => x.Key);

            foreach (var newType in newTypes)
            {
                if (!existing.ContainsKey(newType.Name))
                {
                    OwnershipService.Save(newType);
                }
            }
        }

        private void PopulateTax()
        {
            var existing = TaxService.GetAll()
                .Select(x => x.Rate)
                .AsEnumerable()
                .GroupBy(x => x)
                .ToDictionary(x => x.Key);
            
            for (decimal i = 0; i <= 1; i += 0.01M)
            {
                if (!existing.ContainsKey(i))
                {
                    TaxService.Save(new Tax { Rate = i });
                }
            }
        }

        private void PopulatePosition(string appPath)
        {
            var path = appPath + @"\Content\dictionary-sources\Position.json";

            if (!File.Exists(path))
            {
                return;
            }

            var jsonString = File.ReadAllText(path);

            var newTypes = JsonConvert.DeserializeObject<List<Position>>(jsonString);

            var existing = PositionService.GetAll()
                .Select(x => x.Name)
                .AsEnumerable()
                .GroupBy(x => x)
                .ToDictionary(x => x.Key);

            foreach (var newType in newTypes)
            {
                if (!existing.ContainsKey(newType.Name))
                {
                    PositionService.Save(newType);
                }
            }
        }

        private void PopulateRailwayStations(string appPath)
        {
            var path = appPath + @"\Content\dictionary-sources\RailwayStation.json";

            if (!File.Exists(path))
            {
                return;
            }

            var jsonString = File.ReadAllText(path);

            var newTypes = JsonConvert.DeserializeObject<List<RailwayStation>>(jsonString);

            var existing = RailwayStationService.GetAll()
                .Select(x => x.Name)
                .AsEnumerable()
                .GroupBy(x => x)
                .ToDictionary(x => x.Key);

            foreach (var newType in newTypes)
            {
                if (!existing.ContainsKey(newType.Name))
                {
                    RailwayStationService.Save(newType);
                }
            }
        }

        private void PopulateRailwayTransport(string appPath)
        {
            
        }

        private void PopulateBank(string appPath)
        {
            var path = appPath + @"\Content\dictionary-sources\bank.json";

            if (!File.Exists(path))
            {
                return;
            }

            var jsonString = File.ReadAllText(path);

            var newTypes = JsonConvert.DeserializeObject<List<Bank>>(jsonString);

            var existing = BankService.GetAll()
                .Select(x => x.Name)
                .AsEnumerable()
                .GroupBy(x => x)
                .ToDictionary(x => x.Key);

            foreach (var newType in newTypes)
            {
                if (!existing.ContainsKey(newType.Name))
                {
                    BankService.Save(newType);
                }
            }
        }
        
        #region Загрузка марок - моделей - модификаций авто
        private void PopulateCarModels(string appPath)
        {
            var dir = appPath + "App_Data/";

            var mapping = new[]
            {
                new {fileName = "cars.json", categoryName = "Легковые", subcategoryName = (string) null},
                new {fileName = "автобусы.json", categoryName = "Автобус", subcategoryName = (string) null},
                new {fileName = "легкие коммерческие.json", categoryName = "Грузовые", subcategoryName = "Легкие коммерческие"},
                new {fileName = "седельные тягачи.json", categoryName = "Грузовые", subcategoryName = "Седельный тягач"},
                new {fileName = "грузовики.json", categoryName = "Грузовые", subcategoryName = "Грузовик"},
                new {fileName = "автокраны.json", categoryName = "Спецтехника", subcategoryName = "Автокраны"},
                new {fileName = "автопогрузчики.json", categoryName = "Спецтехника", subcategoryName = "Автопогрузчики"},
                new {fileName = "бульдозеры.json", categoryName = "Спецтехника", subcategoryName = "Бульдозеры"},
                new {fileName = "коммунальная.json", categoryName = "Спецтехника", subcategoryName = "Коммунальная"},
                new {fileName = "самопогрузчики.json", categoryName = "Спецтехника", subcategoryName = "Самопогрузчики"},
                new {fileName = "сельскохозяйственная.json", categoryName = "Спецтехника", subcategoryName = "Сельхозтехника"},
                new {fileName = "строительная.json", categoryName = "Спецтехника", subcategoryName = "Строительная техника"},
                new {fileName = "экскаваторы.json", categoryName = "Спецтехника", subcategoryName = "Экскаваторы"},
                new {fileName = "прицепы.json", categoryName = "Прицепные устройства", subcategoryName =(string) null}
            };

            var categoryDict = VehicleCategoryService.GetAll()
                .Select(x => new { x.Id, x.Name })
                .AsEnumerable()
                .Where(x => !string.IsNullOrWhiteSpace(x.Name))
                .Select(x => new { x.Id, Name = x.Name.ToLower() })
                .GroupBy(x => x.Name)
                .ToDictionary(x => x.Key, x => x.First().Id);

            var subcategoryDict = VehicleSubCategoryService.GetAll()
                .Select(x => new { x.Id, x.Name, categoryId = x.Category.Id })
                .AsEnumerable()
                .Where(x => !string.IsNullOrWhiteSpace(x.Name))
                .GroupBy(x => x.categoryId)
                .ToDictionary(x => x.Key,
                    x => x.Select(y => new { y.Id, Name = y.Name.ToLower() })
                          .GroupBy(y => y.Name)
                          .ToDictionary(y => y.Key, y => y.First().Id));

            foreach (var cartype in mapping)
            {
                var categoryId = categoryDict.Get(cartype.categoryName.ToLower());

                if (categoryId == 0)
                {
                    continue;
                }

                long? subcategoryId = null;

                if (!string.IsNullOrWhiteSpace(cartype.subcategoryName) && subcategoryDict.ContainsKey(categoryId))
                {
                    var id = subcategoryDict[categoryId].Get(cartype.subcategoryName.ToLower());

                    if (id > 0)
                    {
                        subcategoryId = id;
                    }
                }

                var fileName = dir + cartype.fileName;

                PopulateCarsModels(fileName, categoryId, subcategoryId);
            }
        }

        private void PopulateCarsModels(string fileName, long categoryId, long? subcategoryId)
        {
            if (!File.Exists(fileName))
            {
                return;
            }

            var jsonString = File.ReadAllText(fileName);

            var autosFromExist = JsonConvert.DeserializeObject<List<AutoFromExist>>(jsonString);

            if (autosFromExist == null)
            {
                return;
            }

            var brandDictionary = VehicleBrandService.GetAll()
                .Select(x => new { x.Id, x.Name })
                .AsEnumerable()
                .Where(x => !string.IsNullOrWhiteSpace(x.Name))
                .Select(x => new { x.Id, Name = x.Name.ToLower() })
                .GroupBy(x => x.Name)
                .ToDictionary(x => x.Key, x => x.First().Id);

            var modelDictionary = VehicleModelService.GetAll()
                .Where(x => x.Category.Id == categoryId)
                .Where(x => x.SubCategory.Id == subcategoryId)
                .Select(x => new
                {
                    x.Id,
                    x.Name,
                    brandId = x.Brand.Id
                })
                .AsEnumerable()
                .Where(x => !string.IsNullOrWhiteSpace(x.Name))
                .GroupBy(x => x.brandId)
                .ToDictionary(x => x.Key,
                    x => x.Select(y => new { y.Id, Name = y.Name.ToLower() })
                          .GroupBy(y => y.Name)
                          .ToDictionary(y => y.Key, y => y.First().Id));

            var modificationDict = VehicleModificationService.GetAll()
                .Select(x => new
                {
                    x.Id,
                    x.Name,
                    modelId = x.Model.Id
                })
                .AsEnumerable()
                .Where(x => !string.IsNullOrWhiteSpace(x.Name))
                .GroupBy(x => x.modelId)
                .ToDictionary(
                    x => x.Key,
                    x => x.Select(y => new { y.Id, Name = y.Name.ToLower() })
                        .GroupBy(y => y.Name)
                        .ToDictionary(y => y.Key)
                );

            var brandsToCreate = new List<VehicleBrand>();
            var modelsToCreate = new List<VehicleModel>();
            var modificationsToCreate = new List<VehicleModification>();

            autosFromExist = autosFromExist
                .Where(x => !string.IsNullOrWhiteSpace(x.Brand))
                .Select(x => new AutoFromExist
                {
                    Brand = x.Brand.Trim(),
                    Model = x.Model.SafeToString().Trim(),
                    Modification = x.Modification.SafeToString().Trim()
                })
                .ToList();

            foreach (var brandGroup in autosFromExist.GroupBy(x => x.Brand))
            {
                VehicleBrand brand;

                var existingBrandId = brandDictionary.Get(brandGroup.Key.ToLower());

                if (existingBrandId > 0)
                {
                    brand = new VehicleBrand { Id = existingBrandId };
                }
                else
                {
                    brand = new VehicleBrand { Name = brandGroup.Key };
                    brandsToCreate.Add(brand);
                }

                var brandModels = modelDictionary.Get(brand.Id);

                foreach (var modelGroup in brandGroup.Where(x => !string.IsNullOrWhiteSpace(x.Model)).GroupBy(x => x.Model))
                {
                    VehicleModel model = null;

                    if (brandModels != null && brandModels.ContainsKey(modelGroup.Key.ToLower()))
                    {
                        var modelId = brandModels[modelGroup.Key.ToLower()];

                        model = new VehicleModel { Id = modelId };
                    }

                    if (model == null)
                    {
                        model = new VehicleModel
                        {
                            Name = modelGroup.Key,
                            Brand = brand,
                            Category = new VehicleCategory { Id = categoryId },
                            SubCategory = subcategoryId != null ? new VehicleSubCategory { Id = subcategoryId.Value } : null
                        };
                        modelsToCreate.Add(model);
                    }

                    var modelModifications = modificationDict.Get(model.Id);

                    foreach (var modification in modelGroup.Where(x => !string.IsNullOrWhiteSpace(x.Modification)).GroupBy(x => x.Modification))
                    {
                        if (modelModifications == null ||
                            !modelModifications.ContainsKey(modification.Key.ToLower()))
                        {
                            var mod = new VehicleModification
                            {
                                Model = model,
                                Name = modification.Key
                            };

                            modificationsToCreate.Add(mod);
                        }
                    }
                }
            }

            using (var statelessSession = Container.Resolve<ISessionProvider>().OpenStatelessSession())
            using (var transaction = statelessSession.BeginTransaction())
            {
                try
                {
                    brandsToCreate.ForEach(x => statelessSession.Insert(x));
                    modelsToCreate.ForEach(x => statelessSession.Insert(x));
                    modificationsToCreate.ForEach(x => statelessSession.Insert(x));

                    transaction.Commit();
                }
                catch (Exception)
                {
                    transaction.Rollback();
                    throw;
                }
            }
        }

        #endregion Загрузка марок - моделей - модификаций авто


        public List<EntityDescription> ListEntities()
        {
            return new List<EntityDescription>
                {
                    GetRow<Position>("Должность"),
                    GetRow<PackingType>("Тип упаковки"),
                    GetRow<Country>("Страна"),
                    GetRow<Bank>("Банк"),
                    GetRow<VehicleWheelBase>("Автотранспорт - Колесная база"),
                    GetRow<VehicleBrand>("Автотранспорт - Марка"),
                    GetRow<VehicleModel>("Автотранспорт - Модель"),
                    GetRow<VehicleModification>("Автотранспорт - Модификация"),
                    GetRow<VehicleAdditionalOptions>("Автотранспорт - Опция автомобиля"),
                    GetRow<VehicleBodyType>("Автотранспорт - Тип кузова"),
                    GetRow<VehicleClass>("Автотранспорт - Класс грузоподъемности"),
                    GetRow<ProductGroup>("Товары/услуги - Группа"),
                    GetRow<ProductSubGroup>("Товары/услуги - Подгруппа"),
                    GetRow<Tax>("Налог"),
                    GetRow<DangerClass>("Опасные грузы - класс опасности"),
                    GetRow<DangerSubClass>("Опасные грузы - подкласс опасности"),
                    GetRow<DangerUnCode>("Опасные грузы - код ООН"),
                    GetRow<RegionCode>("Код региона"),
                    GetRow<CountryCode>("Код страны"),
                    GetRow<Experience>("Водительский стаж"),
                    GetRow<MeasureUnit>("Единица измерения"),
                    GetRow<Ownership>("Форма собственности"),
                    GetRow<PackingMaterial>("Материал упаковки"),
                    GetRow<RailwayStation>("Железнодорожная станция"),
                }
                .OrderBy(x => x.HumanName)
                .ToList();
        }

        private EntityDescription GetRow<T>(string name) where T : BaseEntity
        {
            var type = typeof (T);

            var result = new EntityDescription
            {
                EntityName = type.Name, 
                HumanName = name,
                Properties = type.GetProperties(BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic).Select(x => x.Name).Where(x => x != "Id").ToList()
            };
            
            return result;
        }

        public Tuple<object, int> ListByType(StoreParams.StoreParams storeParams)
        {
            var typeName = storeParams.GetFilterPropertyByName("entityType");

            var entityService = GetEntityService(typeName);

            if (entityService == null)
            {
                return new Tuple<object, int>(null, 0);
            }

            var type = GetEntityType(typeName);
            var queryableExtensionType = typeof(QueryableExtension);

            var query = entityService.GetType().GetMethod("GetAll").Invoke(entityService, null);
            var filtered = queryableExtensionType.GetMethod("Filter").MakeGenericMethod(type).Invoke(null, new[] { query, storeParams });
            var ordered = queryableExtensionType.GetMethod("Order").MakeGenericMethod(type).Invoke(null, new[] { filtered, storeParams });
            var paged = queryableExtensionType.GetMethod("Paging").MakeGenericMethod(type).Invoke(null, new[] { ordered, storeParams });
            
            var data = (paged as IQueryable<object>).ToList();
            var count = (filtered as IQueryable<object>).Count();

            return new Tuple<object, int>(data, count);
        }

        public long Save(DictionaryEntity dictionaryEntity)
        {
            var entityService = GetEntityService(dictionaryEntity.EntityType);
            
            if (entityService == null)
            {
                throw new Exception();
            }

            var save = entityService.GetType().GetMethod("Save");

            var type = GetEntityType(dictionaryEntity.EntityType);

            var entity = type.GetConstructor(Type.EmptyTypes).Invoke(null);

            var dictionaryEntityType = typeof (DictionaryEntity);
            var dictionaryEntityProperties = dictionaryEntityType.GetProperties(BindingFlags.Instance | BindingFlags.Public).Where(ø => ø.CanRead).ToList();

            foreach (var property in type.GetProperties(BindingFlags.Instance | BindingFlags.Public).Where(ø => ø.CanWrite))
            {
                var dictionaryEntityProperty = dictionaryEntityProperties.FirstOrDefault(x => x.Name == property.Name);

                if (dictionaryEntityProperty != null && dictionaryEntityProperty.PropertyType == property.PropertyType)
                {
                    property.SetValue(entity, dictionaryEntityProperty.GetValue(dictionaryEntity));
                }
            }

            save.Invoke(entityService, new []{ entity});

            return dictionaryEntity.Id;
        }

        public void Delete(string entityType, long id)
        {
            var entityService = GetEntityService(entityType);

            if (entityService == null)
            {
                throw new Exception();
            }

            var delete = entityService.GetType().GetMethod("Delete");

            try
            {
                delete.Invoke(entityService, new object[] {id});
            }
            catch (ValidationException)
            {
                throw;
            }
            catch (Exception exception)
            {
                throw exception.InnerException ?? exception;
            }
        }

        private Type GetEntityType(string typeName)
        {
            if (string.IsNullOrWhiteSpace(typeName))
            {
                return null;
            }

            var type = Assembly.GetExecutingAssembly().GetTypes()
                .Where(t => t.Namespace != null && t.Namespace.Contains("Cement.Core.Entities"))
                .Where(x => x.Name == typeName)
                .Where(x => x.IsSubclassOf(typeof(BaseEntity)))
                .FirstOrDefault();

            return type;
        }

        private object GetEntityService(string typeName)
        {
            var type = GetEntityType(typeName);

            if (type == null)
            {
                throw new Exception();
            }

            var entityServiceType = typeof(IEntityService<>).MakeGenericType(type);

            return Container.Resolve(entityServiceType);
        }
    }

    public class EntityDescription
    {
        [JsonProperty(PropertyName = "entity")]
        public string EntityName { get; set; }

        [JsonProperty(PropertyName = "name")]
        public string HumanName { get; set; }

        [JsonProperty(PropertyName = "properties")]
        public List<string> Properties { get; set; }
    }

    class ProductGroupsSubGroupsNames
    {
        public string ProductGroupName { get; set; }

        public string ProductSubGroupName { get; set; }
    }

    public class AutoFromExist
    {
        [JsonProperty(PropertyName = "c")]
        public string Category { get; set; }

        [JsonProperty(PropertyName = "s")]
        public string SubCategory { get; set; }

        [JsonProperty(PropertyName = "b")]
        public string Brand { get; set; }

        [JsonProperty(PropertyName = "m")]
        public string Model { get; set; }

        [JsonProperty(PropertyName = "f")]
        public string Modification { get; set; }
    }
}