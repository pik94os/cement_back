﻿using System;
using System.Collections.Generic;
using Cement.Core.Services.Dictionary.Impl;

namespace Cement.Core.Services.Dictionary
{
    public interface IDictService
    {
        void Populate(string appPath);

        Tuple<object, int> ListByType(StoreParams.StoreParams storeParams);

        List<EntityDescription> ListEntities();

        long Save(DictionaryEntity dictionaryEntity);

        void Delete(string entityType, long id);
    }
}