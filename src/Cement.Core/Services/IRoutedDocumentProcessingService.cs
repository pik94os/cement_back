﻿using Cement.Core.Entities;
using Cement.Core.Enums;

namespace Cement.Core.Services
{
    public interface IRoutedDocumentProcessingService
    {
        void ProcessDocument(long documentRouteId, DocumentProcessingResult result, string comment, Employee to = null);

        void Reject(long documentRouteId, string comment);

        void Sign(long documentRouteId, string comment); 
    }
}