﻿using NLog;

namespace Cement.Core.Services.Log.Impl
{
    public class NLogLogger: ILogger
    {
        public void Log(string loggerName, string log)
        {
            var logger = LogManager.GetLogger(loggerName);
            logger.Info(log);
        }
    }
}