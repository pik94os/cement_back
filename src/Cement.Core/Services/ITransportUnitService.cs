﻿using System;
using System.Linq;
using Cement.Core.Entities;

namespace Cement.Core.Services
{
    public interface ITransportUnitService
    {
        IQueryable<TransportUnit> GetTransportUnitQuery(Organization organization, DateTime dateTime);

        IQueryable<TransportUnit> GetTransportUnitQuery(Organization organization);

        IQueryable<TransportUnit> GetOwnTransportUnitQuery(Organization organization, DateTime dateTime);

        IQueryable<TransportUnit> GetOwnTransportUnitQuery(Organization organization);

        IQueryable<TransportUnitArend> GetArendedTransportUnitQuery(Organization organization, DateTime dateTime);

        IQueryable<TransportUnitArend> GetArendedTransportUnitQuery(Organization organization);

        IQueryable<TransportUnitArend> GetGivenToArendTransportUnitQuery(Organization organization, DateTime dateTime);

        IQueryable<TransportUnitArend> GetGivenToArendTransportUnitQuery(Organization organization); 
    }
}