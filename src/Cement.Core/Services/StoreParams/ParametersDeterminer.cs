﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Reflection;
using Cement.Core.Extensions;

namespace Cement.Core.Services.StoreParams
{
    public static class ParametersDeterminer
    {
        static ParametersDeterminer()
        {
            _filterOperatorsDict = new Dictionary<string, FilterOperator>();

            _filterOperatorsDict["_eql"] = FilterOperator.Equals;
            _filterOperatorsDict["_not_eql"] = FilterOperator.NotEquals;
            _filterOperatorsDict["_starts_with"] = FilterOperator.StartsWith;
            _filterOperatorsDict["_ends_with"] = FilterOperator.EndsWith;
            _filterOperatorsDict["_contains"] = FilterOperator.Contains;
            _filterOperatorsDict["_not_contains"] = FilterOperator.NotContains;
            _filterOperatorsDict["_much"] = FilterOperator.GreaterThan;
            _filterOperatorsDict["_much_or_eql"] = FilterOperator.GreaterThanOrEqual;
            _filterOperatorsDict["_less"] = FilterOperator.LessThan;
            _filterOperatorsDict["_less_or_eql"] = FilterOperator.LessThanOrEqual;

            _nullSortTypes = new Dictionary<string, SortOrder>();

            _nullSortTypes["_null_first"] = SortOrder.Descending;
            _nullSortTypes["_null_last"] = SortOrder.Ascending;
            _nullSortTypes["_null_ignore"] = SortOrder.Unspecified;
        }

        /// <summary>
        /// Определение полей и операторов фильтрации
        /// </summary>
        public static List<ExtendedFilterParam> DetermineFilterOperators(this StoreParams storeParams, PropertyInfo[] properties)
        {
            var res = new List<ExtendedFilterParam>();

            if (storeParams.FilterParams != null)
            {
                res.AddRange(storeParams.FilterParams
                    .Select(x =>
                    {
                        var operators = FilterOperatorsDict.Where(y => x.PropertyName.EndsWith(y.Key)).ToList();

                        if (operators.Any())
                        {
                            var op = operators.OrderByDescending(y => y.Key.Length).First();
                            var property =
                                properties.FirstOrDefault(y => (y.Name.ToLower() + op.Key) == x.PropertyName.ToLower());

                            if (property != null)
                            {
                                return new ExtendedFilterParam
                                {
                                    PropertyName = property.Name,
                                    Value = x.Value,
                                    Operator = op.Value
                                };
                            }
                        }
                        else if (properties.Any(y => y.Name.ToLower() == x.PropertyName.ToLower()))
                        {
                            return new ExtendedFilterParam
                            {
                                PropertyName = x.PropertyName,
                                Value = x.Value
                            };
                        }

                        return null;
                    })
                    .Where(x => x != null)
                    .ToList());
            }

            if (storeParams.ComplexFilterParams != null)
            {
                var complexFilterParams = storeParams.ComplexFilterParams
                    .Select(x =>
                    {
                        var propertyName = x.PropertyName;
                        var subProperties = string.Empty;

                        if (propertyName.Contains("."))
                        {
                            var parts = propertyName.Split('.').ToList();

                            propertyName = parts[0];

                            parts.RemoveAt(0);

                            subProperties = string.Join(".", parts);
                        }

                        if (properties.Any(y => y.Name.ToLower() == propertyName.ToLower()))
                        {
                            return new ExtendedFilterParam
                            {
                                PropertyName = propertyName,
                                Value = x.Value,
                                Operator = x.Operator,
                                SubProperties = subProperties
                            };
                        }

                        return null;
                    })
                    .Where(x => x != null)
                    .ToList();

                res.AddRange(complexFilterParams);
            }

            return res;
        }

        private static readonly Dictionary<string, FilterOperator> _filterOperatorsDict;

        public static FilterOperator GetOperator(string operand, FilterOperator def = FilterOperator.Contains)
        {
            return _filterOperatorsDict.Get(operand, def);
        }

        private static Dictionary<string, FilterOperator> FilterOperatorsDict
        {
            get
            {
                return _filterOperatorsDict;
            }
        }

        private static readonly Dictionary<string, SortOrder> _nullSortTypes;

        private static Dictionary<string, SortOrder> NullSortTypes
        {
            get
            {
                return _nullSortTypes;
            }
        }

        /// <summary>
        /// Определение полей и параметров подсветки
        /// </summary>
        public static List<HighlightParam> DetermineHighlightOptions(this List<FilterParam> highlightParams, List<string> propertyNames)
        {
            var res = highlightParams
                .Select(x =>
                {
                    var parts = x.PropertyName.Split(new []{Constants.HighLight}, StringSplitOptions.RemoveEmptyEntries).ToList();

                    if (parts.Count != 2)
                    {
                        return null;
                    }

                    var propertyAndOperator = parts.First().ToLower();
                    var style = parts.Last().Replace('_', ' ');

                    var operators = FilterOperatorsDict.Where(y => propertyAndOperator.EndsWith(y.Key)).ToList();

                    if (operators.Any())
                    {
                        var op = operators.OrderByDescending(y => y.Key.Length).First();
                        var propertyName = propertyNames.FirstOrDefault(y => (y.ToLower() + op.Key) == propertyAndOperator);

                        if (propertyName != null)
                        {
                            return new HighlightParam
                            {
                                PropertyName = propertyName,
                                Value = x.Value,
                                Operator = op.Value,
                                Style = style
                            };
                        }
                    }

                    return null;
                })
                .Where(x => x != null)
                .GroupBy(x => new
                {
                    x.PropertyName,
                    x.Operator,
                    x.Value
                })
                .Select(x => new HighlightParam
                {
                    PropertyName = x.Key.PropertyName,
                    Value = x.Key.Value,
                    Operator = x.Key.Operator,
                    Style = x.First().Style
                })
                .ToList();

            return res;
        }

        /// <summary>
        /// Определение полей и операторов фильтрации
        /// </summary>
        public static List<SortParam> DetermineSortFields(this List<SortParam> sortParams, PropertyInfo[] properties)
        {
            var res = sortParams
                .Select(x =>
                {
                    var nullSorts = NullSortTypes.Where(y => x.PropertyName.EndsWith(y.Key)).ToList();

                    if (nullSorts.Any())
                    {
                        var op = nullSorts.First();
                        var property = properties.FirstOrDefault(y => (y.Name.ToLower() + op.Key) == x.PropertyName.ToLower());

                        if (property != null)
                        {
                            return new SortParam
                            {
                                PropertyName = property.Name,
                                PropertyType = property.PropertyType,
                                Direction = x.Direction,
                                NullSort = op.Value
                            };
                        }
                    }
                    else
                    {
                        var propertyName = x.PropertyName;
                        var subProperties = string.Empty;

                        if (propertyName.Contains("."))
                        {
                            var parts = propertyName.Split('.').ToList();

                            propertyName = parts[0];

                            parts.RemoveAt(0);

                            subProperties = string.Join(".", parts);
                        }

                        var property = properties.FirstOrDefault(y => y.Name.ToLower() == propertyName.ToLower());

                        if (property != null)
                        {
                            return new SortParam
                            {
                                PropertyName = propertyName,
                                PropertyType = property.PropertyType,
                                Direction = x.Direction,
                                NullSort = SortOrder.Unspecified,
                                SubProperties = subProperties
                            };
                        }
                    }
                    
                    return null;
                })
                .Where(x => x != null)
                .ToList();

            return res;
        }
    }
}