﻿using System.Data.SqlClient;
using Cement.Core.Extensions;
using Newtonsoft.Json;

namespace Cement.Core.Services.StoreParams
{
    public class RawSortParam
    {
        [JsonProperty(PropertyName = "property")]
        public string PropertyName { get; set; }

        [JsonProperty(PropertyName = "direction")]
        public string Direction { get; set; }

        public SortParam ToSortParam()
        {
            return new SortParam
            {
                PropertyName = PropertyName,
                Direction = Direction.SafeToString().ToLower() == "desc" ? SortOrder.Descending : SortOrder.Ascending
            };
        }
    }
}