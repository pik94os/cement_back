﻿using System.Collections.Generic;
using System.Linq;

namespace Cement.Core.Services.StoreParams
{
    public class StoreParams
    {
        public int Offset { get; set; }

        public int Limit { get; set; }

        public List<SortParam> SortParams { get; set; }

        public List<FilterParam> FilterParams { get; set; }

        public List<ExtendedFilterParam> ComplexFilterParams { get; set; }

        public Dictionary<string, string> Data { get; set; }

        public string GetFilterPropertyByName(string propertyName)
        {
            var property = FilterParams.FirstOrDefault(x => x.PropertyName == propertyName);

            if (property != null)
            {
                return property.Value;
            }

            return null;
        }

        public string GetDataPropertyByName(string propertyName)
        {
            var property = Data.ContainsKey(propertyName) ? Data[propertyName] : null; 

            if (property != null)
            {
                return property;
            }

            return null;
        }
    }
}