﻿using System.Collections.Generic;
using System.Linq;
using Cement.Core.Extensions;
using Newtonsoft.Json;

namespace Cement.Core.Services.StoreParams
{
    public class RawStoreParams
    {
        [JsonProperty(PropertyName = "p_page")]
        public int Page { get; set; }

        [JsonProperty(PropertyName = "p_start")]
        public int Offset { get; set; }

        [JsonProperty(PropertyName = "p_limit")]
        public int Limit { get; set; }

        [JsonProperty(PropertyName = "p_sort")]
        public string Sort { get; set; }

        [JsonProperty(PropertyName = "p_filter")]
        public string Filter { get; set; }

        [JsonProperty(PropertyName = "complexFilter")]
        public string ComplexFilter { get; set; }

        public Dictionary<string, string> Dictionary = new Dictionary<string, string>();

        public StoreParams ToStoreParams()
        {
            var listRawSortParams = new List<RawSortParam>();
            var listFilterParams = new List<FilterParam>();
            var listExtendedFilterParams = new List<ExtendedFilterParam>();

            if (!string.IsNullOrWhiteSpace(Sort))
            {
                try
                {
                    listRawSortParams = JsonConvert.DeserializeObject<List<RawSortParam>>(Sort);
                }
                catch
                {
                    listRawSortParams = new List<RawSortParam>();
                }
            }

            if (!string.IsNullOrWhiteSpace(Filter))
            {
                try
                {
                    listFilterParams = JsonConvert.DeserializeObject<List<FilterParam>>(Filter);
                }
                catch
                {
                    listFilterParams = new List<FilterParam>();
                }
            }

            if (!string.IsNullOrWhiteSpace(ComplexFilter))
            {
                Dictionary<string, ComplexFilterParam> complexFilterItems = null;

                try
                {
                    complexFilterItems = JsonConvert.DeserializeObject<Dictionary<string, ComplexFilterParam>>(ComplexFilter);
                }
                catch 
                {
                    
                }

                if (complexFilterItems != null)
                {
                    listExtendedFilterParams = complexFilterItems
                    .Select(x => new ExtendedFilterParam
                    {
                        Operator = ParametersDeterminer.GetOperator(x.Value.Operand),
                        PropertyName = x.Key,
                        Value = x.Value.Value
                    })
                    .ToList();
                }
            }

            return new StoreParams
            {
                Limit = Limit <= 0 ? 15 : Limit,
                Offset = Offset,
                SortParams = listRawSortParams.Select(x => x.ToSortParam()).Where(x => !string.IsNullOrWhiteSpace(x.PropertyName)).ToList(),
                FilterParams = listFilterParams.Where(x => !string.IsNullOrWhiteSpace(x.PropertyName)).ToList(),
                ComplexFilterParams = listExtendedFilterParams.Where(x => !string.IsNullOrWhiteSpace(x.PropertyName)).ToList(),
                Data = Dictionary
            };
        }
    }
}