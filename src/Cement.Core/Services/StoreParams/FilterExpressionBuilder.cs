﻿using System;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using Cement.Core.Entities;
using Cement.Core.Extensions;

namespace Cement.Core.Services.StoreParams
{
    public static class FilterExpressionBuilder
    {
        /// <summary>
        /// Получение выражения фильтрации поля
        /// </summary>
        public static Expression<Func<T, bool>> GetFilterExpression<T>(PropertyInfo propertyInfo, string filterValue, FilterOperator op, string subProperties = null)
        {
            var parameter = Expression.Parameter(typeof(T), "x");

            var propertyExpression = Expression.Property(parameter, propertyInfo.Name);

            var type = propertyInfo.PropertyType;

            if (type.IsGenericType)
            {
                type = type.GenericTypeArguments.First();
            }
            else if(type.IsSubclassOf(typeof(BaseEntity)))
            {
                if (string.IsNullOrWhiteSpace(subProperties))
                {
                    var propName = op == FilterOperator.Equals || op == FilterOperator.None ? "Id" : "Name";

                    var prop = type.GetProperty(propName);
                    if (prop != null)
                    {
                        propertyExpression = Expression.PropertyOrField(propertyExpression, prop.Name);
                        type = prop.PropertyType;
                    }
                }
                else
                {
                    var parts = subProperties.Split('.').ToList();

                    foreach (var part in parts)
                    {
                        var prop = type.GetProperty(part);
                        if (prop != null)
                        {
                            propertyExpression = Expression.PropertyOrField(propertyExpression, prop.Name);
                            type = prop.PropertyType;
                        }
                    }
                }
            }

            TypeCode typeCode = Type.GetTypeCode(type);

            Expression expr = null;

            switch (typeCode)
            {
                case TypeCode.String:
                    expr = GetStringFilterExpression(propertyExpression, filterValue, op);
                    break;

                case TypeCode.Decimal:
                    expr = GetDecimalFilterExpression(propertyExpression, filterValue, op);
                    break;

                case TypeCode.Boolean:
                    expr = GetBooleanFilterExpression(propertyExpression, filterValue);
                    break;


                case TypeCode.Byte:
                case TypeCode.Int16:
                case TypeCode.Int32:
                case TypeCode.Int64:
                case TypeCode.SByte:
                case TypeCode.UInt16:
                case TypeCode.UInt32:
                    //case TypeCode.UInt64:
                    expr = GetIntegerFilterExpression(propertyExpression, filterValue, op);
                    break;
                case TypeCode.DateTime:

                    expr = GetDateTimeFilterExpression(propertyExpression, filterValue, op);
                    break;
            }

            return expr == null ? null : Expression.Lambda<Func<T, bool>>(expr, parameter);
        }

        /// <summary>
        /// Получение выражения фильтрации текстового поля
        /// </summary>
        private static Expression GetStringFilterExpression(MemberExpression propertyExpression, string filterValue, FilterOperator op)
        {
            var loweredExpr = Expression.Call(propertyExpression, typeof(string).GetMethod("ToLower", new Type[0]));
            var exprValue = Expression.Constant(filterValue.ToLower());

            switch (op)
            {
                case FilterOperator.StartsWith:
                    return Expression.Call(loweredExpr, typeof(string).GetMethod("StartsWith", new[] { typeof(string) }), exprValue);

                case FilterOperator.EndsWith:
                    return Expression.Call(loweredExpr, typeof(string).GetMethod("EndsWith", new[] { typeof(string) }), exprValue);

                case FilterOperator.Equals:
                    return Expression.Equal(propertyExpression, Expression.Constant(filterValue));

                case FilterOperator.NotEquals:
                    return Expression.NotEqual(propertyExpression, Expression.Constant(filterValue));

                case FilterOperator.NotContains:
                    return Expression.Not(Expression.Call(loweredExpr, typeof(string).GetMethod("Contains"), exprValue));

                case FilterOperator.Contains:
                default:
                    return Expression.Call(loweredExpr, typeof(string).GetMethod("Contains"), exprValue);
            }
        }

        /// <summary>
        /// Получение выражения фильтрации целочисленного поля
        /// </summary>
        private static Expression GetIntegerFilterExpression(MemberExpression propertyExpression, string filterValue, FilterOperator op)
        {
            var longValue = filterValue.ToLong();
            //var valExpr = Expression.Convert(Expression.Constant(intValue), propertyExpression.Type);
            var valExpr = Expression.Convert(Expression.Constant(longValue), typeof(Int64));
            var propExpression = Expression.Convert(propertyExpression, typeof(Int64));

            switch (op)
            {
                case FilterOperator.NotEquals:
                    return Expression.NotEqual(propExpression, valExpr);

                case FilterOperator.GreaterThan:
                    return Expression.GreaterThan(propExpression, valExpr);

                case FilterOperator.GreaterThanOrEqual:
                    return Expression.GreaterThanOrEqual(propExpression, valExpr);

                case FilterOperator.LessThan:
                    return Expression.LessThan(propExpression, valExpr);

                case FilterOperator.LessThanOrEqual:
                    return Expression.LessThanOrEqual(propExpression, valExpr);

                case FilterOperator.Equals:
                default:
                    return Expression.Equal(propExpression, valExpr);
            }
        }


        /// <summary>
        /// Получение выражения фильтрации логического поля
        /// </summary>
        private static Expression GetBooleanFilterExpression(MemberExpression propertyExpression, string filterValue)
        {
            var boolValue = filterValue.ToBool();
            var valExpr = Expression.Convert(Expression.Constant(boolValue), typeof(Boolean));
            var propExpression = Expression.Convert(propertyExpression, typeof(Boolean)); 
            
            return Expression.Equal(propExpression, valExpr);
        }

        /// <summary>
        /// Получение выражения фильтрации нецелочисленного поля
        /// </summary>
        private static Expression GetDecimalFilterExpression(MemberExpression propertyExpression, string filterValue, FilterOperator op)
        {
            var decimalValue = filterValue.ToDecimal();
            //var valExpr = Expression.Convert(Expression.Constant(intValue), propertyExpression.Type);
            var valExpr = Expression.Convert(Expression.Constant(decimalValue), typeof(Decimal));
            var propExpression = Expression.Convert(propertyExpression, typeof(Decimal));

            switch (op)
            {
                case FilterOperator.NotEquals:
                    return Expression.NotEqual(propExpression, valExpr);

                case FilterOperator.GreaterThan:
                    return Expression.GreaterThan(propExpression, valExpr);

                case FilterOperator.GreaterThanOrEqual:
                    return Expression.GreaterThanOrEqual(propExpression, valExpr);

                case FilterOperator.LessThan:
                    return Expression.LessThan(propExpression, valExpr);

                case FilterOperator.LessThanOrEqual:
                    return Expression.LessThanOrEqual(propExpression, valExpr);

                case FilterOperator.Equals:
                default:
                    return Expression.Equal(propExpression, valExpr);
            }
        }

        /// <summary>
        /// Получение выражения фильтрации поля с типом DateTime
        /// </summary>
        private static Expression GetDateTimeFilterExpression(MemberExpression propertyExpression, string filterValue, FilterOperator op)
        {
            Expression dateValueExpr;

            try
            {
                var date = (DateTime)Convert.ChangeType(filterValue, typeof(DateTime));

                dateValueExpr = Expression.Convert(Expression.Constant(date), propertyExpression.Type);
            }
            catch
            {
                return null;
            }

            switch (op)
            {
                case FilterOperator.NotEquals:
                    return Expression.NotEqual(propertyExpression, dateValueExpr);

                case FilterOperator.GreaterThan:
                    return Expression.GreaterThan(propertyExpression, dateValueExpr);

                case FilterOperator.GreaterThanOrEqual:
                    return Expression.GreaterThanOrEqual(propertyExpression, dateValueExpr);

                case FilterOperator.LessThan:
                    return Expression.LessThan(propertyExpression, dateValueExpr);

                case FilterOperator.LessThanOrEqual:
                    return Expression.LessThanOrEqual(propertyExpression, dateValueExpr);

                case FilterOperator.Equals:
                default:
                    return Expression.Equal(propertyExpression, dateValueExpr);
            }
        } 
    }
}