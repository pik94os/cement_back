﻿using System;
using Cement.Core.Extensions;

namespace Cement.Core.Services.StoreParams
{
    public static class Highlight
    {
        public static bool Test(HighlightParam highlightOption, System.Collections.Generic.Dictionary<string, object> row)
        {
            var value = row[highlightOption.PropertyName];

            if (value == null)
            {
                return false;
            }

            var type = value.GetType();

            TypeCode typeCode = Type.GetTypeCode(type);

            switch (typeCode)
            {
                case TypeCode.String:
                    return TestString(value.ToString(), highlightOption.Value, highlightOption.Operator);

                case TypeCode.Byte:
                case TypeCode.Int16:
                case TypeCode.Int32:
                case TypeCode.Int64:
                case TypeCode.SByte:
                case TypeCode.UInt16:
                case TypeCode.UInt32:
                    //case TypeCode.UInt64:
                    return TestInteger(value.ToLong(), highlightOption.Value, highlightOption.Operator);

                case TypeCode.DateTime:
                    return TestDateTime(value, highlightOption.Value, highlightOption.Operator);
            }

            return false;
        }

        /// <summary>
        /// Проверка текстового поля
        /// </summary>
        private static bool TestString(string value, string filterValue, FilterOperator op)
        {
            switch (op)
            {
                case FilterOperator.StartsWith:
                    return value.ToLower().StartsWith(filterValue.ToLower());

                case FilterOperator.EndsWith:
                    return value.ToLower().EndsWith(filterValue.ToLower());

                case FilterOperator.Equals:
                    return value.Equals(filterValue);

                case FilterOperator.NotEquals:
                    return !value.Equals(filterValue);

                case FilterOperator.NotContains:
                    return !value.ToLower().Contains(filterValue.ToLower());

                case FilterOperator.Contains:
                default:
                    return value.ToLower().Contains(filterValue.ToLower());
            }
        }

        /// <summary>
        /// Проверка целочисленного поля
        /// </summary>
        private static bool TestInteger(long value, string filterValue, FilterOperator op)
        {
            var filterLongValue = filterValue.ToLong();
            
            switch (op)
            {
                case FilterOperator.NotEquals:
                    return value != filterLongValue;

                case FilterOperator.GreaterThan:
                    return value > filterLongValue;

                case FilterOperator.GreaterThanOrEqual:
                    return value >= filterLongValue;

                case FilterOperator.LessThan:
                    return value < filterLongValue;

                case FilterOperator.LessThanOrEqual:
                    return value <= filterLongValue;

                case FilterOperator.Equals:
                default:
                    return value == filterLongValue;
            }
        }

        /// <summary>
        /// Проверка поля с типом DateTime
        /// </summary>
        private static bool TestDateTime(object value, string filterValue, FilterOperator op)
        {
            DateTime date1, date2;

            try
            {
                date1 = (DateTime)Convert.ChangeType(value, typeof(DateTime));
                date2 = (DateTime)Convert.ChangeType(filterValue, typeof(DateTime));
            }
            catch
            {
                return false;
            }

            switch (op)
            {
                case FilterOperator.NotEquals:
                    return date1 != date2;

                case FilterOperator.GreaterThan:
                    return date1 > date2;

                case FilterOperator.GreaterThanOrEqual:
                    return date1 >= date2;

                case FilterOperator.LessThan:
                    return date1 < date2;

                case FilterOperator.LessThanOrEqual:
                    return date1 <= date2;

                case FilterOperator.Equals:
                default:
                    return date1 == date2;
            }
        } 
    }
}