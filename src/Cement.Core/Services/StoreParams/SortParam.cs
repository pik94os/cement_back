﻿using System;
using System.Data.SqlClient;

namespace Cement.Core.Services.StoreParams
{
    public class SortParam
    {
        public string PropertyName { get; set; }

        public SortOrder Direction { get; set; }

        public SortOrder NullSort { get; set; }

        public Type PropertyType { get; set; }

        public string SubProperties { get; set; }
    }
}