﻿using Newtonsoft.Json;

namespace Cement.Core.Services.StoreParams
{
    public class FilterParam
    {
        [JsonProperty(PropertyName = "property")]
        public string PropertyName { get; set; }

        [JsonProperty(PropertyName = "value")]
        public string Value { get; set; }
    }

    public class ComplexFilterParam : FilterParam
    {
        [JsonProperty(PropertyName = "operand")]
        public string Operand { get; set; }
    }

    public class ExtendedFilterParam : FilterParam
    {
        public FilterOperator Operator { get; set; }

        public string SubProperties { get; set; }
    }

    public class HighlightParam : ExtendedFilterParam
    {
        public string Style { get; set; }
    }

    public enum FilterOperator
    {
        None,

        Equals,

        NotEquals,

        StartsWith,

        EndsWith,

        Contains,

        NotContains,

        GreaterThan,

        GreaterThanOrEqual,

        LessThan,

        LessThanOrEqual,
    }
}