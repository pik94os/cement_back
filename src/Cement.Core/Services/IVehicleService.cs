﻿using System;
using System.Linq;
using Cement.Core.Entities;

namespace Cement.Core.Services
{
    public interface IVehicleService
    {
        IQueryable<Vehicle> GetVehicleQuery(Organization organization, DateTime dateTime);

        IQueryable<Vehicle> GetVehicleQuery(Organization organization);

        IQueryable<Vehicle> GetOwnVehicleQuery(Organization organization, DateTime dateTime);

        IQueryable<Vehicle> GetOwnVehicleQuery(Organization organization);

        IQueryable<VehicleArend> GetArendedVehicleQuery(Organization organization, DateTime dateTime);

        IQueryable<VehicleArend> GetArendedVehicleQuery(Organization organization);

        IQueryable<VehicleArend> GetGivenToArendVehicleQuery(Organization organization, DateTime dateTime);

        IQueryable<VehicleArend> GetGivenToArendVehicleQuery(Organization organization);
    }
}