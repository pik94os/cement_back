﻿using Cement.Core.Enums;
using ECM7.Migrator.Framework;

namespace Cement.Core
{
    public static class DbViewCollection
    {
        public static void Update(ITransformationProvider database)
        {
            database.ExecuteNonQuery(InternalIncomingView());
            database.ExecuteNonQuery(InternalOutcomingView());
            database.ExecuteNonQuery(ExternalIncomingView());
            database.ExecuteNonQuery(ExternalOutcomingView());
        }

        private static string False { get { return "false"; } }
        
        private static string InternalIncomingView()
        {
            var internalDocQuery = string.Format(@"select 
                    mvt.ID as mvt_id, 
                    doc.STATE as doc_state, 
                    doc.No as doc_no, 
                    doc.DOCUMENT_DATE as doc_date, 
                    doc.Name as doc_name, 
                    employee.Name as author_name, 
                    '{0}' as doc_type,
                    doc.TYPE as doc_kind, 
                    doc.SIGNER_ID as signee_id, 
                    mvt.EMPLOYEE_ID as emp_id

                from INTERNAL_DOC_MOVEMENT mvt 
                inner join INTERNAL_DOC_ROUTE route on mvt.ROUTE_ID=route.ID 
                inner join INTERNAL_DOCUMENT doc on mvt.DOCUMENT_ID=doc.ID 
                left outer join EMPLOYEE employee on doc.AUTHOR_ID=employee.ID 
                where mvt.TYPE='{1}' 
                and mvt.IS_ARCHIVED={2}
                and (route.PROCESSING_RESULT={3} or route.PROCESSING_RESULT={4})", 
                (int)DocumentType.InternalDocument, 
                (int)DocumentMovementType.Incoming,
                False,
                (int)DocumentProcessingResult.None,
                (int)DocumentProcessingResult.OnAcquaintance);

            var messageQuery = string.Format(@"select 
                    mvt.ID as mvt_id, 
                    msg.STATE as doc_state,
                    null as doc_no,
                    msg.MESSAGE_DATE as doc_date, 
                    msg.Subject as doc_name,
                    employee.Name as author_name, 
                    '{0}' as doc_type,
                    msg.TYPE as doc_kind, 
                    null as signee_id,
                    mvt.EMPLOYEE_ID as emp_id

                from MESSAGE_MOVEMENT mvt 
                left outer join MESSAGE msg on mvt.DOCUMENT_ID=msg.ID 
                left outer join MESSAGE_ROUTE route on mvt.ROUTE_ID=route.ID 
                left outer join EMPLOYEE employee on msg.AUTHOR_ID=employee.ID 
                where mvt.TYPE='{1}' 
                and mvt.IS_ARCHIVED={3} 
                and route.PROCESSING_RESULT={2}",
                (int)DocumentType.Message,
                (int)DocumentMovementType.Incoming,
                (int)DocumentProcessingResult.OnAcquaintance,
                False);

            var advertQuery = string.Format(@"select 
                    mvt.ID as mvt_id, 
                    ad.STATE as doc_state,
                    null as doc_no,
                    ad.DATE_SHOW as doc_date, 
                    ad.Subject as doc_name,
                    employee.Name as author_name,  
                    '{0}' as doc_type,
                    0 as doc_kind, 
                    null as signee_id,
                    mvt.EMPLOYEE_ID as emp_id

                from ADVERT_MOVEMENT mvt 
                left outer join ADVERT ad on mvt.DOCUMENT_ID=ad.ID 
                left outer join ADVERT_ROUTE route on mvt.ROUTE_ID=route.ID 
                left outer join EMPLOYEE employee on ad.AUTHOR_ID=employee.ID 
                where mvt.TYPE='{1}' 
                and mvt.IS_ARCHIVED={3} 
                and route.PROCESSING_RESULT={2}",
                    (int)DocumentType.Advert,
                    (int)DocumentMovementType.Incoming,
                    (int)DocumentProcessingResult.OnAcquaintance,
                    False);

            var stockRequestQuery = string.Format(@"select 
                    route.ID as mvt_id, 
                    request.STATE as doc_state,
                    request.NO as doc_no,
                    request.REQUEST_DATE as doc_date, 
                    CAST(null AS character varying(255)) as doc_name,
                    CAST(null AS character varying(255)) as author_name,  
                    '{0}' as doc_type,
                    request.type as doc_kind, 
                    null as signee_id,
                    route.EMPLOYEE_ID as emp_id
                from STOCKROOM_REQUEST_ROUTE route 
                left outer join STOCKROOM_REQUEST request on route.STOCKROOM_REQUEST_ID=request.ID 
                where route.MOVEMENT_TYPE='{1}' 
                and route.ARCHIVED_AT is null
                and route.DELETED_AT is null
                and route.PROCESSING_RESULT={2}",
                    (int)DocumentType.StockroomRequest,
                    (int)DocumentMovementType.Incoming,
                    (int)DocumentProcessingResult.None);

            var stockExpenseOrderQuery = string.Format(@"select 
                    route.ID as mvt_id, 
                    doc.STATE as doc_state,
                    doc.NO as doc_no,
                    doc.DOCUMENT_DATE as doc_date, 
                    CAST(null AS character varying(255)) as doc_name,
                    CAST(null AS character varying(255)) as author_name,  
                    '{0}' as doc_type,
                    doc.type as doc_kind, 
                    null as signee_id,
                    route.EMPLOYEE_ID as emp_id
                from STOCKROOM_EXPENSE_ORDER_ROUTE route 
                left outer join STOCKROOM_EXPENSE_ORDER doc on route.STOCKROOM_EXPENSE_ORDER_ID=doc.ID 
                where route.MOVEMENT_TYPE='{1}' 
                and route.ARCHIVED_AT is null
                and route.DELETED_AT is null
                and route.PROCESSING_RESULT={2}",
                    (int)DocumentType.StockroomExpenseOrder,
                    (int)DocumentMovementType.Incoming,
                    (int)DocumentProcessingResult.None);

            var inventoryQuery = string.Format(@"select 
                    route.ID as mvt_id, 
                    doc.STATE as doc_state,
                    doc.NO as doc_no,
                    doc.INVENTORY_DATE as doc_date, 
                    CAST(null AS character varying(255)) as doc_name,
                    CAST(null AS character varying(255)) as author_name,  
                    '{0}' as doc_type,
                    0 as doc_kind, 
                    null as signee_id,
                    route.EMPLOYEE_ID as emp_id
                from INVENTORY_ROUTE route 
                left outer join INVENTORY doc on route.INVENTORY_ID=doc.ID 
                where route.MOVEMENT_TYPE='{1}' 
                and route.ARCHIVED_AT is null
                and route.DELETED_AT is null
                and route.PROCESSING_RESULT={2}",
                    (int)DocumentType.Inventory,
                    (int)DocumentMovementType.Incoming,
                    (int)DocumentProcessingResult.None);

            var accountingOtherQuery = string.Format(@"select 
                    route.ID as mvt_id, 
                    doc.STATE as doc_state,
                    doc.NO as doc_no,
                    doc.DOCUMENT_DATE as doc_date, 
                    CAST(null AS character varying(255)) as doc_name,
                    CAST(null AS character varying(255)) as author_name,  
                    '{0}' as doc_type,
                    doc.type as doc_kind, 
                    null as signee_id,
                    route.EMPLOYEE_ID as emp_id
                from ACCOUNTING_OTHER_ROUTE route 
                left outer join ACCOUNTING_OTHER doc on route.ACCOUNTING_OTHER_ID=doc.ID 
                where route.MOVEMENT_TYPE='{1}' 
                and route.ARCHIVED_AT is null
                and route.DELETED_AT is null
                and route.PROCESSING_RESULT={2}",
                    (int)DocumentType.OtherAccountingDocument,
                    (int)DocumentMovementType.Incoming,
                    (int)DocumentProcessingResult.None);

            var resultQuery = string.Format(@"CREATE OR REPLACE VIEW VIEW_INTERNAL_INCOMING AS {0}
                UNION {1}
                UNION {2}
                UNION {3}
                UNION {4}
                UNION {5}
                UNION {6}
                ", internalDocQuery
                 , messageQuery
                 , advertQuery
                 , stockRequestQuery
                 , stockExpenseOrderQuery
                 , inventoryQuery
                 , accountingOtherQuery);

            return resultQuery;
        }

        private static string InternalOutcomingView()
        {
            var internalDocQuery = string.Format(@"select 
                    doc.ID as id, 
                    doc.STATE as doc_state, 
                    doc.No as doc_no, 
                    doc.DOCUMENT_DATE as doc_date, 
                    doc.Name as doc_name, 
                    employee.Name as author_name,  
                    '{0}' as doc_type,
                    doc.TYPE as doc_kind, 
                    doc.SIGNER_ID as signee_id, 
                    doc.CREATOR_ID as emp_id

                from INTERNAL_DOCUMENT doc
                left outer join EMPLOYEE employee on doc.AUTHOR_ID=employee.ID 
                where doc.STATE='{1}'",
                (int)DocumentType.InternalDocument,
                (int)DocumentState.Draft);

            var messageQuery = string.Format(@"select 
                    msg.ID as id, 
                    msg.STATE as doc_state,
                    null as doc_no,
                    msg.MESSAGE_DATE as doc_date, 
                    msg.Subject as doc_name,
                    employee.Name as author_name, 
                    '{0}' as doc_type,
                    msg.TYPE as doc_kind, 
                    null as signee_id,
                    msg.CREATOR_ID as emp_id

                from MESSAGE msg
                left outer join EMPLOYEE employee on msg.AUTHOR_ID=employee.ID
                where msg.STATE='{1}' ",
                (int)DocumentType.Message,
                (int)DocumentState.Draft);

            var advertQuery = string.Format(@"select 
                    ad.ID as id, 
                    ad.STATE as doc_state,
                    null as doc_no,
                    ad.DATE_SHOW as doc_date, 
                    ad.Subject as doc_name,
                    employee.Name as author_name, 
                    '{0}' as doc_type,
                    0 as doc_kind, 
                    null as signee_id,
                    ad.CREATOR_ID as emp_id

                from ADVERT ad
                left outer join EMPLOYEE employee on ad.AUTHOR_ID=employee.ID
                where ad.STATE='{1}'",
                    (int)DocumentType.Advert,
                    (int)DocumentState.Draft);

            var stockRequestQuery = string.Format(@"select 
                    route.ID as id, 
                    request.STATE as doc_state,
                    request.NO as doc_no,
                    request.REQUEST_DATE as doc_date, 
                    CAST(null AS character varying(255)) as doc_name,
                    CAST(null AS character varying(255)) as author_name,   
                    '{0}' as doc_type,
                    request.type as doc_kind, 
                    null as signee_id,
                    route.EMPLOYEE_ID as emp_id
                from STOCKROOM_REQUEST_ROUTE route 
                left outer join STOCKROOM_REQUEST request on route.STOCKROOM_REQUEST_ID=request.ID 
                where route.MOVEMENT_TYPE='{1}' 
                and route.ARCHIVED_AT is null
                and route.DELETED_AT is null
                and route.PROCESSING_RESULT={2}",
                    (int)DocumentType.StockroomRequest,
                    (int)DocumentMovementType.Outcoming,
                    (int)DocumentProcessingResult.None);

            var stockExpenseOrderQuery = string.Format(@"select 
                    route.ID as id, 
                    doc.STATE as doc_state,
                    doc.NO as doc_no,
                    doc.DOCUMENT_DATE as doc_date, 
                    CAST(null AS character varying(255)) as doc_name,
                    CAST(null AS character varying(255)) as author_name,  
                    '{0}' as doc_type,
                    doc.type as doc_kind, 
                    null as signee_id,
                    route.EMPLOYEE_ID as emp_id
                from STOCKROOM_EXPENSE_ORDER_ROUTE route 
                left outer join STOCKROOM_EXPENSE_ORDER doc on route.STOCKROOM_EXPENSE_ORDER_ID=doc.ID 
                where route.MOVEMENT_TYPE='{1}' 
                and route.ARCHIVED_AT is null
                and route.DELETED_AT is null
                and route.PROCESSING_RESULT={2}",
                    (int)DocumentType.StockroomExpenseOrder,
                    (int)DocumentMovementType.Outcoming,
                    (int)DocumentProcessingResult.None);

            var inventoryQuery = string.Format(@"select 
                    route.ID as id, 
                    doc.STATE as doc_state,
                    doc.NO as doc_no,
                    doc.INVENTORY_DATE as doc_date, 
                    CAST(null AS character varying(255)) as doc_name,
                    CAST(null AS character varying(255)) as author_name,  
                    '{0}' as doc_type,
                    0 as doc_kind, 
                    null as signee_id,
                    route.EMPLOYEE_ID as emp_id
                from INVENTORY_ROUTE route 
                left outer join INVENTORY doc on route.INVENTORY_ID=doc.ID 
                where route.MOVEMENT_TYPE='{1}' 
                and route.ARCHIVED_AT is null
                and route.DELETED_AT is null
                and route.PROCESSING_RESULT={2}",
                    (int)DocumentType.Inventory,
                    (int)DocumentMovementType.Outcoming,
                    (int)DocumentProcessingResult.None);

            var accountingOtherQuery = string.Format(@"select 
                    route.ID as id, 
                    doc.STATE as doc_state,
                    doc.NO as doc_no,
                    doc.DOCUMENT_DATE as doc_date, 
                    CAST(null AS character varying(255)) as doc_name,
                    CAST(null AS character varying(255)) as author_name,  
                    '{0}' as doc_type,
                    doc.type as doc_kind, 
                    null as signee_id,
                    route.EMPLOYEE_ID as emp_id
                from ACCOUNTING_OTHER_ROUTE route 
                left outer join ACCOUNTING_OTHER doc on route.ACCOUNTING_OTHER_ID=doc.ID 
                where route.MOVEMENT_TYPE='{1}' 
                and route.ARCHIVED_AT is null
                and route.DELETED_AT is null
                and route.PROCESSING_RESULT={2}",
                    (int)DocumentType.OtherAccountingDocument,
                    (int)DocumentMovementType.Outcoming,
                    (int)DocumentProcessingResult.None);

            var resultQuery = string.Format(@"CREATE OR REPLACE VIEW VIEW_INTERNAL_OUTCOMING AS {0}
                UNION {1}
                UNION {2}
                UNION {3}
                UNION {4}
                UNION {5}
                UNION {6}
                ", internalDocQuery
                 , messageQuery
                 , advertQuery
                 , stockRequestQuery
                 , stockExpenseOrderQuery
                 , inventoryQuery
                 , accountingOtherQuery);

            return resultQuery;
        }

        private static string ExternalIncomingView()
        {
            var correspondenceQuery = string.Format(@"select 
                    mvt.ID as mvt_id, 
                    corr.STATE as doc_state,
                    null as doc_no,
                    corr.DOCUMENT_DATE as doc_date, 
                    corr.SUBJECT as doc_name,
                    null as meeting_name, 
                    '{0}' as doc_type,
                    corr.TYPE as doc_kind, 
                    null as signee_id,
                    mvt.EMPLOYEE_ID as emp_id

                from CORRESP_MOVEMENT mvt 
                left outer join CORRESPONDENCE corr on mvt.DOCUMENT_ID=corr.ID 
                left outer join CORRESP_ROUTE route on mvt.ROUTE_ID=route.ID 
                where mvt.TYPE='{1}' 
                and mvt.IS_ARCHIVED={3} 
                and route.PROCESSING_RESULT={2}",
                (int)DocumentType.Correspondence,
                (int)DocumentMovementType.Incoming,
                (int)DocumentProcessingResult.None,
                False);

            var productRequestQuery = string.Format(@"select 
                    mvt.ID as mvt_id, 
                    doc.STATE as doc_state, 
                    doc.REQUEST_NUMBER as doc_no, 
                    doc.REQUEST_DATE as doc_date, 
                    doc.Name as doc_name, 
                    null as meeting_name, 
                    '{0}' as doc_type,
                    doc.TYPE as doc_kind, 
                    null as signee_id, 
                    mvt.EMPLOYEE_ID as emp_id
                from REQUEST_MOVEMENT mvt 
                inner join REQUEST_ROUTE route on mvt.ROUTE_ID=route.ID 
                inner join PRODUCT_REQUEST doc on mvt.DOCUMENT_ID=doc.ID 
                --left outer join EMPLOYEE employee on doc.AUTHOR_ID=employee.ID 
                where mvt.TYPE='{1}'
                and mvt.IS_ARCHIVED={3}
                and route.PROCESSING_RESULT={2}",
                (int)DocumentType.Request,
                (int)DocumentMovementType.Incoming,
                (int)DocumentProcessingResult.None,
                False);

            var accountingInOutDocQuery = string.Format(@"select 
                    route.ID as mvt_id, 
                    doc.STATE as doc_state,
                    doc.NO as doc_no,
                    doc.DOCUMENT_DATE as doc_date, 
                    CAST(null AS character varying(255)) as doc_name,
                    CAST(null AS character varying(255)) as meeting_name,  
                    '{0}' as doc_type,
                    doc.type as doc_kind, 
                    null as signee_id,
                    route.EMPLOYEE_ID as emp_id
                from ACCOUNTING_INOUT_DOC_ROUTE route 
                left outer join ACCOUNTING_INOUT_DOC doc on route.ACCOUNTING_INOUT_DOC_ID=doc.ID 
                where route.MOVEMENT_TYPE='{1}' 
                and route.ARCHIVED_AT is null
                and route.DELETED_AT is null
                and route.PROCESSING_RESULT={2}",
                    (int)DocumentType.AccountingInOutDocument,
                    (int)DocumentMovementType.Incoming,
                    (int)DocumentProcessingResult.None);

            var resultQuery = string.Format(@"CREATE OR REPLACE VIEW VIEW_EXTERNAL_INCOMING AS {0}
                UNION {1}
                UNION {2}
                ", correspondenceQuery
                 , productRequestQuery
                 , accountingInOutDocQuery);

            return resultQuery;
        }

        private static string ExternalOutcomingView()
        {
            var correspondenceQuery = string.Format(@"select 
                    mvt.ID as id, 
                    doc.STATE as doc_state, 
                    doc.NO as doc_no, 
                    doc.DOCUMENT_DATE as doc_date, 
                    doc.Name as doc_name, 
                    '{0}' as doc_type,
                    doc.TYPE as doc_kind, 
                    CAST(null AS bigint) as signee_id, 
                    mvt.EMPLOYEE_ID as emp_id
                from CORRESP_MOVEMENT mvt 
                inner join CORRESPONDENCE doc on mvt.DOCUMENT_ID=doc.ID 
                inner join CORRESP_ROUTE route on route.ID = mvt.ROUTE_ID
                where mvt.TYPE='{1}'
                and mvt.IS_ARCHIVED={2}
                and route.PROCESSING_RESULT={3}",
                (int)DocumentType.Correspondence,
                (int)DocumentMovementType.Outcoming,
                False,
                (int)DocumentProcessingResult.None);

            var productRequestQuery = string.Format(@"select 
                    mvt.ID as id, 
                    doc.STATE as doc_state, 
                    doc.REQUEST_NUMBER as doc_no, 
                    doc.REQUEST_DATE as doc_date, 
                    doc.Name as doc_name, 
                    '{0}' as doc_type,
                    doc.TYPE as doc_kind, 
                    CAST(null AS bigint) as signee_id, 
                    mvt.EMPLOYEE_ID as emp_id
                from REQUEST_MOVEMENT mvt 
                inner join REQUEST_ROUTE route on mvt.ROUTE_ID=route.ID 
                inner join PRODUCT_REQUEST doc on mvt.DOCUMENT_ID=doc.ID 
                --left outer join EMPLOYEE employee on doc.AUTHOR_ID=employee.ID 
                where mvt.TYPE='{1}'
                and mvt.IS_ARCHIVED={3}
                and route.PROCESSING_RESULT={2}",
                (int)DocumentType.Request,
                (int)DocumentMovementType.Outcoming,
                (int)DocumentProcessingResult.None,
                False);

            var accountingInOutDocQuery = string.Format(@"select 
                    route.ID as id, 
                    doc.STATE as doc_state,
                    doc.NO as doc_no,
                    doc.DOCUMENT_DATE as doc_date, 
                    CAST(null AS character varying(255)) as doc_name,
                    '{0}' as doc_type,
                    doc.type as doc_kind, 
                    null as signee_id,
                    route.EMPLOYEE_ID as emp_id
                from ACCOUNTING_INOUT_DOC_ROUTE route 
                left outer join ACCOUNTING_INOUT_DOC doc on route.ACCOUNTING_INOUT_DOC_ID=doc.ID 
                where route.MOVEMENT_TYPE='{1}' 
                and route.ARCHIVED_AT is null
                and route.DELETED_AT is null
                and route.PROCESSING_RESULT={2}",
                    (int)DocumentType.AccountingInOutDocument,
                    (int)DocumentMovementType.Outcoming,
                    (int)DocumentProcessingResult.None);

            var resultQuery = string.Format(@"CREATE OR REPLACE VIEW VIEW_EXTERNAL_OUTCOMING AS {0}
                UNION {1}
                UNION {2}
                ", correspondenceQuery
                 , productRequestQuery
                 , accountingInOutDocQuery);

            return resultQuery;
        }
    }
}