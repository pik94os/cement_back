﻿using System;
using System.Collections.Generic;
using System.Security.Principal;
using Cement.Core.Entities;

namespace Cement.Core.Auth.Impl
{

    internal class UserPrincipal : IUserPrincipal
    {
        private readonly User _user;
        private IIdentity _identity;

        public UserPrincipal(User user)
        {
            if (user == null)
            {
                throw new ArgumentNullException("user");
            }

            _user = user;
        }

        public long UserId { get { return _user.Id; } }

        public string Login { get { return _user.Login; } }
        
        public bool IsActive { get { return !_user.IsBlocked; } }

        public Organization Organization { get { return _user.Organization; } }

        public Employee Employee { get { return _user.Employee; } }
        
        public List<long> WarehouseIds { get { return _user.GetWarehouses(); } }

        public bool IsInRole(UserRole role)
        {
            return _user.IsInRole(role);
        }

        public bool IsVerified()
        {
            return _user.IsEmailConfirmed;
        }
        IIdentity IPrincipal.Identity
        {
            get { return _identity ?? (_identity = new UserIdentity(_user)); }
        }

        bool IPrincipal.IsInRole(string role)
        {
            UserRole userRole;

            if (Enum.TryParse(role, out userRole))
            {
                return IsInRole(userRole);
            }

            return false;
        }

        internal class UserIdentity : IIdentity
        {
            private readonly User _user;

            public UserIdentity(User user)
            {
                _user = user;
            }

            public string Name { get { return _user != null ? _user.Login : null; } }

            public string AuthenticationType { get { return "database"; } }

            public bool IsAuthenticated { get { return _user != null; } }
        }
    }
}