﻿using System;
using System.Linq;
using System.Security.Principal;
using Cement.Core.Entities;
using Cement.Core.Exceptions;
using DataAccess.Repository;

namespace Cement.Core.Auth.Impl
{
    public class AuthenticationProvider : IAuthenticationProvider
    {
        #region Injections

        public IPasswordHashProvider PasswordHashProvider { get; set; }
        public IUserCache UserCache { get; set; }
        public IRepository<UserLoginLog> UserLoginLogRepository { get; set; }

        #endregion Injections

        public TimeSpan AuthenticationIdle = TimeSpan.FromMinutes(5);
        public int FailedAuthenticationStep = 5;

        #region UserAuth

        public IUserPrincipal AuthenticateUser(string login, string password)
        {
            var user = UserCache.GetList().FirstOrDefault(x => x.Login == login);

            if (user == null)
            {
                return null;
            }

            CheckUserIsBlocked(user);
            CheckFailedCount(user);

            var userLoginLog = new UserLoginLog { User = user, LogedAt = DateTime.Now };

            var success = !String.IsNullOrEmpty(password) && user.Authenticate(PasswordHashProvider, password);
            userLoginLog.Success = success;
            UserLoginLogRepository.Save(userLoginLog);

            if (!success)
            {
                return null;
            }

            return new UserPrincipal(user);
        }
        
        private void CheckFailedCount(User user)
        {
            var failedAuthCount = UserLoginLogRepository.GetAll()
                .Where(x => x.User == user)
                .Where(x => !x.Success)
                .Where(x => x.LogedAt > (DateTime.Now - AuthenticationIdle))
                .Count();

            if (failedAuthCount >= FailedAuthenticationStep)
            {
                throw new FailedThresholdExceededException(AuthenticationIdle);
            }
        }

        private void CheckUserIsBlocked(User user)
        {
            if (user.IsBlocked)
            {
                throw new UserIsBlockedException(user.Login);
            }
        }

        #endregion UserAuth

        public IUserPrincipal GetUser(IIdentity identity)
        {
            if (!identity.IsAuthenticated)
            {
                return null;
            }

            string login = identity.Name;

            var user = UserCache.GetList().FirstOrDefault(x => x.Login == login);
            
            if (user != null)
            {
                CheckUserIsBlocked(user);

                return new UserPrincipal(user);
            }

            return null;
        }
    }
}