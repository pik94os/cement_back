﻿using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using Castle.Windsor;
using Cement.Core.Entities;
using Cement.Core.Extensions;
using DataAccess.Repository;
using DataAccess.SessionProvider;
using NHibernate.Linq;

namespace Cement.Core.Auth.Impl
{
    public class UserCache : IUserCache
    {
        private List<User> _userList;

        private List<User> UserList {
            get { 
                if (_userList == null)
                {
                    UpdateCache();
                }

                return _userList;
            }
        }

        public IWindsorContainer Container { get; set; }

        public List<User> GetList()
        {
            return UserList.ToList();
        }
        
        public void UpdateCache()
        {
            // Чистим кэш сессии Nhibernate чтобы все объекты целиком обновились
            var session = Container.Resolve<ISessionProvider>().GetCurrentSession();
            session.Flush();
            session.Clear();
            
            _userList = Container.Resolve<IRepository<User>>().GetAll()
                .Fetch(x => x.Organization)
                .ThenFetch(x => x.Client)
                .ThenFetch(x => x.Ownership)
                .Fetch(x => x.Employee)
                .ToList();
            
            var empWarehouses = Container.Resolve<IRepository<WarehouseEmployee>>().GetAll()
                .Select(x => new { employeeId = x.Employee.Id, warehouseId = x.Warehouse.Id })
                .AsEnumerable()
                .GroupBy(x => x.employeeId)
                .ToDictionary(x => x.Key, x => x.Select(y => y.warehouseId).ToList());

            var field = typeof (User).GetField("_warehouseIds",BindingFlags.NonPublic |BindingFlags.Instance);
            foreach (var user in _userList)
            {
                if (user.Employee != null)
                {
                    field.SetValue(user, empWarehouses.Get(user.Employee.Id));
                }
            }
        }
    }
}