﻿using System;
using System.Linq;
using System.Security.Cryptography;
using System.Text;

namespace Cement.Core.Auth.Impl
{
    public class PasswordHashProvider : IPasswordHashProvider
    {
        private static readonly Encoding DefaultEncoding = Encoding.UTF8;

        private readonly HashAlgorithm _hashAlgorithm = SHA512.Create();

        private const int IterationCount = 20000;

        public byte[] CreateSolt()
        {
            var solt = new byte[64];

            new Random().NextBytes(solt);

            return solt;
        }

        public byte[] CalcPasswordHash(string password, byte[] solt)
        {
            if (password == null)
            {
                throw new ArgumentNullException("password");
            }

            password = password.Trim();

            var passwordBytes = DefaultEncoding.GetBytes(password);

            var hash = passwordBytes;

            for (var i = 0; i < IterationCount; i++)
            {
                solt = this.IterationHash(solt, hash);
                hash = this.IterationHash(hash, solt);
            }

            return hash;
        }

        public bool IsValid(string password, byte[] solt, byte[] origHash)
        {
            var passHash = this.CalcPasswordHash(password, solt);

            return this.IsEquial(passHash, origHash);
        }

        public bool IsEquial(byte[] bytes1, byte[] bytes2)
        {
            if (bytes1.Length != bytes2.Length)
            {
                return false;
            }

            return !bytes1.Where((t, i) => t != bytes2[i]).Any();
        }

        private byte[] IterationHash(byte[] password, byte[] solt)
        {
            var soltedPassword = solt.Concat(password).ToArray();
            return this._hashAlgorithm.ComputeHash(soltedPassword);
        }
    }
}