﻿using System.Linq;
using Cement.Core.Entities;
using DataAccess.Repository;

namespace Cement.Core.Auth.Impl
{
    public class RootUserManager : IRootUserManager
    {
        public IRepository<User> UserRepository { get; set; }

        public IPasswordHashProvider PasswordHashProvider { get; set; }

        public const string RootUserLogin = "root"; 

        public void ManageRootUser(bool rootUserIsActive, string password)
        {
            var root = UserRepository.GetAll().FirstOrDefault(x => x.Login == RootUserLogin);

            if (rootUserIsActive && !string.IsNullOrWhiteSpace(password))
            {
                if (root == null)
                {
                    root = new User(UserRole.Root)
                    {
                        Login = RootUserLogin,
                        Email = RootUserLogin,
                        Name = RootUserLogin
                    };

                    root.SetNewPassword(PasswordHashProvider, password);
                    UserRepository.Save(root);
                }
                else
                {
                    root.SetNewPassword(PasswordHashProvider, password);
                    root.IsBlocked = false;
                    UserRepository.Update(root);
                }
            }
            else if (root != null)
            {
                root.IsBlocked = true;
                UserRepository.Update(root);
            }
        }
    }
}