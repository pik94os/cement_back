﻿using System.Collections.Generic;
using System.Security.Principal;
using System.Threading;
using System.Web;
using System.Web.Security;
using Cement.Core.Entities;

namespace Cement.Core.Auth.Impl
{
    public class CookiesAuthenticationState : IAuthenticator, IAuthenticationState
    {
        private readonly IPrincipal _httpPrincipal;
        private readonly IAuthenticationProvider _authenticationProvider;

        public CookiesAuthenticationState(IAuthenticationProvider authenticationProvider) : this(null, authenticationProvider)
        {
        }

        public CookiesAuthenticationState(IPrincipal principal, IAuthenticationProvider authenticationProvider)
        {
            _authenticationProvider = authenticationProvider;

            _httpPrincipal = principal;
            
            CheckAuthenticationState();
            
            Thread.CurrentPrincipal = HttpContext.Current.User = CurrentUser ?? new NotAuthorizedPrincipal();
        }
        
        public bool SignIn(string login, string password)
        {
            var user = _authenticationProvider.AuthenticateUser(login, password);

            if (user == null)
            {
                SignOut();
                return false;
            }

            SetAuthentication(user);

            return true;
        }

        private void SetAuthentication(IUserPrincipal user)
        {
            SignOut();

            CurrentUser = user;

            FormsAuthentication.SetAuthCookie(user.Login, false /*persistent*/);
        }

        
        public void SignOut()
        {
            CurrentUser = null;

            FormsAuthentication.SignOut();
        }
        
        public bool IsAuthenticated
        {
            get
            {
                return CurrentUser != null;
            }
        }

        public IUserPrincipal CurrentUser { get; private set; }

        private void CheckAuthenticationState()
        {
            CurrentUser = _httpPrincipal == null ? null : _authenticationProvider.GetUser(_httpPrincipal.Identity);
        }
    }

    public class NotAuthorizedPrincipal : IUserPrincipal
    {
        private readonly IIdentity _identity = new NotAuthorizedIdentity();

        public bool IsInRole(string role)
        {
            return false;
        }

        public IIdentity Identity
        {
            get { return _identity; }
        }

        private class NotAuthorizedIdentity : IIdentity
        {
            public string Name { get { return string.Empty; } }
            public string AuthenticationType { get { return string.Empty; } }
            public bool IsAuthenticated { get { return false; } }
        }

        public long UserId { get { return 0; }}
        public string Login { get { return string.Empty; } }
        public bool IsActive { get { return false; } }
        public Organization Organization { get { return null; } }
        public Employee Employee { get { return null; } }
        public List<long> WarehouseIds { get { return null; } }

        public bool IsInRole(UserRole role)
        {
            return false;
        }
    }
}
