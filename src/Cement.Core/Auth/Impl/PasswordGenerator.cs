﻿using System;

namespace Cement.Core.Auth.Impl
{
    public class PasswordGenerator : IPasswordGenerator
    {
        public string Generate(int length = 4)
        {
            var md5 = System.Security.Cryptography.MD5.Create();
            var inputBytes = System.Text.Encoding.UTF8.GetBytes(Guid.NewGuid().ToString());
            var hash = md5.ComputeHash(inputBytes);
            var sb = new System.Text.StringBuilder();
            for (var i = 0; i < (hash.Length < length ? hash.Length : length); i++)
            {
                sb.Append(hash[i].ToString("x2"));
            }

            return sb.ToString();
        }
    }
}