﻿namespace Cement.Core.Auth
{
    public interface IAuthenticationState
    {
        bool IsAuthenticated { get; }

        IUserPrincipal CurrentUser { get; }
    }
}