﻿using System.Collections.Generic;
using System.Security.Principal;
using Cement.Core.Entities;

namespace Cement.Core.Auth
{
    public interface IUserPrincipal : IPrincipal
    {
        long UserId { get; }

        string Login { get; }

        bool IsActive { get; }

        Organization Organization { get; }

        Employee Employee { get; }
        
        List<long> WarehouseIds { get; }

        bool IsInRole(UserRole role);
    }
}