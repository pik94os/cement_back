﻿namespace Cement.Core.Auth
{
    public interface IAuthenticator
    {
        bool SignIn(string login, string password);

        void SignOut();
    }
}