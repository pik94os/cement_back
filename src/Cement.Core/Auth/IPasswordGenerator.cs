﻿namespace Cement.Core.Auth
{
    public interface IPasswordGenerator
    {
        string Generate(int length = 4);
    }
}