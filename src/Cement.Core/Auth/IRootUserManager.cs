﻿namespace Cement.Core.Auth
{
    public interface IRootUserManager
    {
        void ManageRootUser(bool rootUserIsActive, string password);
    }
}