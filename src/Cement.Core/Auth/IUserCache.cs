﻿using System.Collections.Generic;
using Cement.Core.Entities;

namespace Cement.Core.Auth
{
    public interface IUserCache
    {
        List<User> GetList();

        void UpdateCache();
    }
}