﻿namespace Cement.Core.Auth
{
    public interface IPasswordHashProvider
    {
        byte[] CalcPasswordHash(string password, byte[] solt);

        byte[] CreateSolt();

        bool IsValid(string password, byte[] solt, byte[] origHash);
    }
}