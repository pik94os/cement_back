﻿using System.Security.Principal;

namespace Cement.Core.Auth
{
    public interface IAuthenticationProvider
    {
        IUserPrincipal AuthenticateUser(string login, string password);

        IUserPrincipal GetUser(IIdentity identity);
    }
}